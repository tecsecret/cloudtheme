//////////////////////////////////////////////////////////////
// Tabber.js
// By Alons
// Please Read the Terms of use at http://www.softaculous.com
// (c)Softaculous Inc.
// You cannot remove the copyrights.
//////////////////////////////////////////////////////////////

function tabber(){

	this.tabs = new Array();//The tabs
	
	this.tabwindows = new Array();//The tab windows
	
	this.tabclass = 'tab';//A tab button which is not yet tabbed
	
	this.tabbedclass = 'tabbed';//The tabbed button class
	
	this.inittab = false;
	
	this.active = false;
	
	this.tab = function(id){
		for(x in this.tabs){			
			if(this.tabs[x] == id){		
				$_(this.tabs[x]).className = this.tabbedclass;
				setopacity($_(this.tabs[x]), 100);
				setopacity($_(this.tabwindows[x]), 10);
				$_(this.tabwindows[x]).style.display = 'block';
				smoothopaque(this.tabwindows[x], 0, 100, 5);
			}else{
				$_(this.tabs[x]).className = this.tabclass;
				setopacity($_(this.tabs[x]), 50);
				$_(this.tabwindows[x]).style.display = 'none';			
			}
		}
	};
	
	//Will set the first tab to Tabbed
	this.init = function(){
		if(this.inittab){
			this.active = this.inittab;
		}else{
			this.active = this.tabs[0];
		}
		this.tab(this.active);
	};
	
};