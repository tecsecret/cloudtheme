//////////////////////////////////////////////////////////////
// universal.js - Simple JS functions that make JS easy
// Inspired by Alons
// ----------------------------------------------------------
// Please Read the Terms of use at http://www.softaculous.com
// ----------------------------------------------------------
// (c)Softaculous Inc.
//////////////////////////////////////////////////////////////

ua = navigator.userAgent.toLowerCase();
isIE = ((ua.indexOf("msie") != -1) && (ua.indexOf("opera") == -1) && (ua.indexOf("webtv") == -1));
IE_version = 0;
if(isIE){
	var _match = /(msie) ([\w.]+)/.exec( ua );
	IE_version = parseInt(_match[2]);
}
isFF = (ua.indexOf("firefox") != -1);
isGecko = (ua.indexOf("gecko") != -1);
isSafari = (ua.indexOf("safari") != -1);
isKonqueror = (ua.indexOf("konqueror") != -1);

aefonload = '';

//Element referencer - We use $ because we love PHP
function $_(id){
	//DOM
	if(document.getElementById){
		return document.getElementById(id);
	//IE
	}else if(document.all){
		return document.all[id];
	//NS4
	}else if(document.layers){
		return document.layers[id];
	}
};

//Trims a string
function trim(str){
	return str.replace(/^[\s]+|[\s]+$/, "");
};

//Give a random integer
function AEFrand(min, max){
	return Math.floor(Math.random() * (max - min + 1) + min);
};

//To clear a time out
function AEFclear(timer){
	clearTimeout(timer);
	clearInterval(timer);
	return null;
};

//Changes the opacity
function setopacity(el, opacity){
	el.style.opacity = (opacity/100);
	if(isIE){ el.style.zoom = 1; }
	el.style.filter = 'alpha(opacity=' + opacity + ')';
};

//Hides an element
function hideel(elid){
	$_(elid).style.visibility="hidden";
};

//Shows an element
function showel(elid){
	$_(elid).style.visibility="visible";
};

function isvisible(elid){
	if($_(elid).style.visibility == "visible"){
		return true;
	}else{
		return false;
	}
}

//Checks the entire range of checkboxes
function check(field, checker){
	if(checker.checked == true){
		for(i = 0; i < field.length; i++){
			field[i].checked = true;
		}
	}else{
		for(i = 0; i < field.length; i++){  
			field[i].checked = false;
		}
	}
};
//The page width
function getwidth(){
	return document.body.clientWidth;
};
//The page height
function getheight(){
	return document.body.clientHeight;
};

//Get the scrolled height
function scrolledy(){
	//Netscape compliant
	if(typeof(window.pageYOffset) == 'number'){
		return window.pageYOffset;
	//DOM compliant
	}else if(document.body && document.body.scrollTop){
		return document.body.scrollTop;
	//IE6 standards compliant mode
	}else if(document.documentElement && typeof(document.documentElement.scrollTop)!='undefined'){
		return document.documentElement.scrollTop;
	}else{
		return 0;	
	}
};

//Gradually increases the opacity
function smoothopaque(elid, startop, endop, inc){
	if(typeof(elid) == 'object'){
		var el = elid;
	}else{
		var el = $_(elid);
	}
	op = startop;
	//Initial opacity
	setopacity(el, op);
	//Start the opacity timeout that makes it more visible
	setTimeout(slowopacity, 1);
	function slowopacity(){
		if(startop < endop){
			op = op + inc;
			if(op < endop){
				setTimeout(slowopacity, 1);
			}
		}else{
			op = op - inc;
			if(op > endop){
				setTimeout(slowopacity, 1);
			}
		}
		setopacity(el, op);		
	};
};

//Cookie setter
function setcookie(name, value, duration){
	value = escape(value);
	if(duration){
		var date = new Date();
		date.setTime(date.getTime() + (duration * 86400000));
		value += "; expires=" + date.toGMTString();
	}
	document.cookie = name + "=" + value;
};

//Gets the cookie value
function getcookie(name){
	value = document.cookie.match('(?:^|;)\\s*'+name+'=([^;]*)');
	return value ? unescape(value[1]) : false;
};

//Removes the cookies
function removecookie(name){
	setcookie(name, '', -1);
};

function AJAX(url, evalthis, met, send){
	var send = (send || null);
	var met = (met || "GET");
	var req = false;
	var toeval = evalthis;
    // branch for native XMLHttpRequest object
    if(window.XMLHttpRequest){
    	try{
			req = new XMLHttpRequest();
        }catch(e){
			req = false;
        }
    // branch for IE/Windows ActiveX version
    }else if(window.ActiveXObject){
       	try{
	        req = new ActiveXObject("Msxml2.XMLHTTP");
      	}catch(e){
        	try{
          		req = new ActiveXObject("Microsoft.XMLHTTP");
        	}catch(e){
          		req = false;
        	}
		}
    }
	
	if(req){
		try{
			req.onreadystatechange = function(){				
    			// only if req shows "loaded"
				if (req.readyState==4) {
					//only if OK
					if (req.status == 200) {
						// only if "OK"...processing statements go here..
						var re = req.responseText // result of the req object
						if(re.length > 0){
							return eval(toeval);
						}else{
							return false;
						}
					}
				}
			};
			req.open(met, url, true);
			if(met.toLowerCase() == "post"){req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); }
			req.send(send);
		}catch(e){
			return false;
		}
	}else{
		return false;
	}
	return true;
};

//Finds the position of the element
function findelpos(ele){
	var curleft = 0;
	var curtop = 0;
	if(ele.offsetParent){
		while(1){
			curleft += ele.offsetLeft;
			curtop += ele.offsetTop;
			if(!ele.offsetParent){
				break;
			}
			ele = ele.offsetParent;
		}
	}else if(ele.x){
		curleft += ele.x;
		curtop += ele.y;
	}
	return [curleft,curtop];
};

function getAttributeByName(node, attribute){
	if(typeof NamedNodeMap != "undefined"){
		if(node.attributes.getNamedItem(attribute)){
			return node.attributes.getNamedItem(attribute).value;
		}
	}else{
		return node.getAttribute(attribute);
	}
};

function AttributeByName(node, attribute){
	if(typeof NamedNodeMap != "undefined"){
		if(node.attributes.getNamedItem(attribute)){
			return node.attributes.getNamedItem(attribute);
		}
	}else{
		return node.getAttribute(attribute);
	}
};

//With ';'
function addonload(js){
	aefonload += js;
};

function randstr(length, special, strength){
	
	var special = special || 0;
	var strength = strength || 0;
	$randstr = "";
	$specialchars = new Array('&', '#', '$', '%', '@');
	//alert(special);
	for($i = 0; $i < length; $i++){	
		$randnum = Math.floor(Math.random()*61);
		if($randnum < 10){		
			$randstr = $randstr + String.fromCharCode($randnum+48);			
		}else if($randnum < 36){		
			$randstr = $randstr + String.fromCharCode($randnum+55);			
		}else if(special > 0){
			var tmp = Math.floor(Math.random() * $specialchars.length);
			$randstr = $randstr + $specialchars[tmp];
		}else{		
			$randstr = $randstr + String.fromCharCode($randnum+61);			
		}
	}
	
	// Do we have to meet the strength set by admin ?
	if(strength > 0){
		$cur_strength = passwordStrength($randstr);
		if($cur_strength[1] < strength){
			while ($cur_strength[1] < strength) {
				$randnum = Math.floor(Math.random()*61);
				$randstr = $randstr + String.fromCharCode($randnum+55);
				$cur_strength = passwordStrength($randstr);
			}
		}
	}
	
	if(special > 0){
		return $randstr;
	}else{
		return $randstr.toLowerCase();	
	}
};

// Random password String with Special characters
function rand_pass(length){

	var $string="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%&*";
	var $randpass = "";
	
	for ($i = 0; $i < length; $i++){
		
		$randpass += $string.charAt(Math.floor(Math.random() * $string.length));
	}
	
	return $randpass;

};

function sub_but(el, c){
	var oldclass = el.className;
	el.className = c || 'submiton';
	el.onmouseout= function(){
						el.className = oldclass;
					};
};

function getFormValues(id){
	var vals = new Array();
	var elem = $_(id).elements;
	for(var i = 0; i < elem.length; i++){
		vals[i] = elem[i].name+"="+escape(elem[i].value);
	}
	return vals.join('&');
};

function disable_submit(el){
	el.disabled = 1;
	el.className = 'submitoff';	
	tform = el.form;
	var thisname  = el.name;
	el.name = thisname+'_temp';
	var inp = document.createElement("INPUT");
	inp.type = "hidden";
	inp.value = "Sub";
	inp.name = thisname;
	tform.appendChild(inp);
	tform.submit();
};

// For showing the search form on page
function showsearch(){
	if($_("showsearch").style.display == ""){
		$_("showsearch").style.display = "none";
	}else{
		$_("showsearch").style.display = "";
	}
};


$(document).ready(function() {
	
	$(".altrowstable tr").mouseover(function(){
		var old_class = $(this).attr("class");
		//alert(old_class);
		$(this).attr("class", "tr_bgcolor");
	
		$(this).mouseout(function(){
			$(this).attr("class", old_class);
		});
	});
	
	// MENU Hiding / Showing for screens
	var shown=false;
	$('#pull').on('click', function(e) {
		shown=$('#left-menu').is(':visible');
		e.preventDefault();
		$('#left-menu').toggle('1000');
		if(!shown){
			$('#pull').animate({
				left: '220px'
			},'1000');
		}else{
			$('#pull').animate({
				left: '0px'
			},'1000');
		}

	});

	$(window).resize(function(){
	    	var w = $(window).width();
	    	if(w >= 700 && $('#left-menu').is(':hidden')){
	    		$('#left-menu').removeAttr("style");
	    	}
	});
	
	
	// Allow only numbers in a HTML Textbox
	$(".numbersonly").keydown(function(event){
		
		if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 
			|| event.keyCode == 27 || event.keyCode == 13 
			|| (event.keyCode == 65 && event.ctrlKey === true) 
			|| (event.keyCode >= 35 && event.keyCode <= 39)){
				return;
		}else {
			// If its not a number stop the keypress
			if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
				event.preventDefault(); 
			}   
		}
	});
	
	try{
	
		$('.select_all').on('change', function(){
			var checkAll = $('.select_all');
			var checkboxes = $('.ios');
	
			var is_checked = $_('select_all').checked;
			
			checkboxes.each(function(){
				
				if(is_checked){
					$(this).prop("checked", true);
				}else{
					$(this).prop("checked", false);
				}
			});
		});
	}catch(e){}

	// Chosen Select Lib Init
	if($(".chosen-select").length){
		$(".chosen-select").chosen({width: "100%"});
		$(".chosen-choices").addClass("form-control");
		$(".chosen-choices").css("margin-top","2px");
		$(".chosen-choices").css("padding","2px 5px");
		$(".chosen-choices").css("border","1px solid #ccc");
	}

});

// The page jumper box
function pagejump(ele, len, urlto, call_func){
	
	var offset = $(ele).offset();
	offset.left -= 5;
	offset.top += ele.offsetHeight + 2;
	//alert(offset.top+" - "+offset.left);
	call_func = call_func || 'loadpage';
	
	// Is there an ONSHOW function
	var isit_fn = window[call_func];
	
	var pageJumpTimer;
	
	$(ele).mouseout(function() {		
		pageJumpTimer = setTimeout("$('#pagejump').hide();", 100);
	});
	
	$("#pagejump :text").val('');
	
	$("#pagejump form").submit(function(event) {
		event.stopImmediatePropagation();
		val = $("#pagejump :text").val();
		val = parseInt(val);
		if(val > 0){
			
			if(typeof isit_fn === 'function'){
				isit_fn(urlto+(val-1));
			}else{
				loadpage(urlto+(val-1));
			}
			
			$('#pagejump').hide();
		}
		return false;	
	});
	
	$("#pagejump").mouseout(function() {		
		pageJumpTimer = setTimeout("$('#pagejump').hide();", 100);
	});
	
	$("#pagejump").mouseover(function() {		
		clearTimeout(pageJumpTimer);
	});
	
	// Set the offset
	$_('pagejump').style.left=Math.ceil(offset.left)+"px";
	$_('pagejump').style.top=Math.ceil(offset.top)+"px";
	
	$("#pagejump").show();
	return false;
};

// Builds the page links
function pageLinks(id, urlto, pages, call_func){
	
	$("#"+id+" .pagination-top").hide();
	$("#"+id+" .pagination-bottom").hide();
	
	pageInfo = pages || (typeof(N["page"]) == "undefined" ? false : N["page"]);
	call_func = call_func || 'loadpage';
	
	// Is there a pagination ?
	if(!pageInfo){
		return;
	}
	
	// Make the URL
	var urlto = (urlto || windowHASH()).toString();
	var final = urlto.replace(/(&?)start\=(\d{1,4})/gi,"")+"&page=";
	
	// Number of Pages
	var $pages = Math.ceil(pageInfo["maxNum"] / pageInfo["len"]);
	
	// Current Page
	var $pg = (pageInfo["start"]/pageInfo["len"]) + 1;
	
	var $_pages = new Object();
	
	if($pages > 1){
		
		// Show th Back Links if required
		if($pg != 1){
			$_pages['&lt;&lt;'] = 1;
			$_pages['&lt;'] = ($pg - 1);
		}
		
		for($i = ($pg - 4); $i < $pg; $i++){
			if($i >= 1){
				$_pages["i"+$i] = $i;
			}
		}
		
		$_pages["i"+$pg] = $pg;
				
		for($i = ($pg + 1); $i <= ($pg + 4); $i++){
			if($i <= $pages){
				$_pages["i"+$i] = $i;
			}
		}
		
		if($pg != $pages){
			$_pages['&gt;'] = ($pg + 1);
			$_pages['&gt;&gt;'] = $pages;
		}
		
	}
	
	// Make the table
	var str = '<table class="cbgbor" cellspacing="1" cellpadding="0" border="0" align="right">'+
'<tr>'+
'<td class="pagelinks"><a href="javascript:void(0)" onclick="'+call_func+'(\''+final+1+'\');" > '+ page_page +$pg+ page_of +$pages+'</a></td>';

	for(x in $_pages){
		var i = x.substring(0, 1) == "i" ? x.substring(1) : x;
		str += '<td class="' + (i == $pg ? 'activepage' : 'pagelinks' ) + '"><a href="javascript:void(0);" onclick="'+call_func+'(\''+final+$_pages[x]+'\');">'+i+'</a></td>';
	};
	
	str += '</tr>'+
'</table>';
	
	$("#"+id+" .pagination-top").html(str);
	$("#"+id+" .pagination-top").show();
	
	$("#"+id+" .pagination-bottom").html(str);
	$("#"+id+" .pagination-bottom").show();
	return false;
};

//Get parameter from URL by name
function getParameterByName(name, inHash) {
	
	inHash = inHash || 0;
	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
		results = regex.exec(inHash ? "?"+windowHASH() : location.search);
	return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
};

function is_checked(id){
	
	if($("#"+ id).is(":checked")){
		return true;
	}else{
		return false;
	}
};

// PHP equivalent empty()
function empty(mixed_var) {

  var undef, key, i, len;
  var emptyValues = [undef, null, false, 0, '', '0'];

  for (i = 0, len = emptyValues.length; i < len; i++) {
    if (mixed_var === emptyValues[i]) {
      return true;
    }
  }

  if (typeof mixed_var === 'object') {
    for (key in mixed_var) {
      // TODO: should we check for own properties only?
      //if (mixed_var.hasOwnProperty(key)) {
      return false;
      //}
    }
    return true;
  }

  return false;
}

function multiselect(id, num){
	try{
		for(i=0; i < num; i++) {
			$_(id).options[i].selected = true;
		}
	}catch(e){}
};

//field function to get/set input values of any type of input
(function () {
    $.fn.field = function (inputName, value){
        if (typeof inputName !== "string") return false;
        var $inputElement = $(this).find("[name='" + inputName + "']");
        // var $inputElement = $(this); //direct mapping with no form context
		
        if (typeof value === "undefined" && $inputElement.length >= 1){
            switch ($inputElement.prop("type")){
                case "checkbox":
                    return $inputElement.is(":checked");
                    break;
                case "radio":
                    var result;
                    $inputElement.each(function (i, val) {
                        if ($(this).is(":checked")) result = $(this).val()
                    });
                    return result;
                    break;
				default:
                    return $inputElement.val();
                    break;
            }
        }else{
			switch ($inputElement.prop("type")){
                case "checkbox":
					$inputElement.prop("checked", !empty(value) ? 1 : 0);
                    break;
                case "radio":
                    $inputElement.each(function (i) {
                        if ($(this).val() == value) $(this).attr({
                            checked: true
                        })
                    });
                    break;
				/*case 'select-multiple':
					$.each(value, function( ind, val ){
						
						$(this).attr("selected", false);
						if($(this).val() == val){
							$(this).attr("selected", "selected");
						}
					});
                    break;*/
                case undefined:
                    $(this).append('');
                    break;
                default:
					$inputElement.val(value);
                    break;
            }
            return $inputElement;
        }
    }
})();

/* Monitor page related code*/

// Format the date
function nDate(timestamp, format){
	format = format || "";
	if(!timestamp){
		return "<i>Never</i>";
	}
	var d = new Date(timestamp * 1000);
	
	if(format == ""){
		var ret = d.toUTCString();
		return ret.replace(" GMT", "");
	}
	
	var ret = format;
	ret = ret.replace("Y", d.getUTCFullYear());
	ret = ret.replace("m", (d.getUTCMonth()+1).toString().pad(2, "0"));
	ret = ret.replace("d", d.getUTCDate().toString().pad(2, "0"));
	ret = ret.replace("H", d.getUTCHours().toString().pad(2, "0"));
	ret = ret.replace("i", d.getUTCMinutes().toString().pad(2, "0"));
	ret = ret.replace("s", d.getUTCSeconds().toString().pad(2, "0"));
	return ret;
};

// Show tooltip for graphs
function showTooltip(x, y, contents) {
	$('<div id="tooltip">' + contents + '</div>').css( {
		position: "absolute",
		display: "none",
		top: y + 20,
		left: x - 20,
		border: "1px solid #CCCCCC",
		padding: "2px",
		"background-color": "#EFEFEF",
		"z-index" : 10000,
		opacity: 0.80
	}).appendTo("body").fadeIn(200);
};

// Draw Live status graphs
function live_resource_graph(id, data, options, show_in, show_time){
	
	var plot = $.plot($("#"+id), data, options);
	
	if(!('tooltip' in options)){
		var previousPoint = null;
		$("#"+id).bind("plothover", function (event, pos, item) {
			$("#x").text(pos.x.toFixed(2));
			$("#y").text(pos.y.toFixed(2));
			if (item) {
				
				if (previousPoint != item.dataIndex) {
					previousPoint = item.dataIndex;
					$("#tooltip").remove();
					var x = item.datapoint[0].toFixed(2),
						y = item.datapoint[1].toFixed(2),
						time = '';
						
					if(show_time){
						time = nDate(x, 'd/m/Y H:i:s');
					}		
					
					if(id == "ntw" || id == "io_read_plot" || id == "io_write_plot"){
					
						var yval = parseInt(y);
						var show_ntw_in;
						
						if(yval <= 1024){
							show_ntw_in = 'B/s';
						}else if(yval > 1024 && yval <= (1024*1024)){
							yval = (yval/1024).toFixed(2);
							show_ntw_in = 'KB/s';
						}else if(yval > (1024*1024) && yval <= (1024*1024*1024)){
							yval = (yval/1024/1024).toFixed(2);
							show_ntw_in = 'MB/s';
						}else if(yval > (1024*1024*1024)){
							yval = (yval/1024/1024/1024).toFixed(2);
							show_ntw_in = 'GB/s';
						}
						
						showTooltip(item.pageX, item.pageY,
									item.series.label + " " + yval + " "+ show_ntw_in + "&nbsp; at &nbsp;" + time);
					}else{
						showTooltip(item.pageX, item.pageY,
									parseFloat(y) + " "+ show_in + " " + time);
					}	
					
				}
			} else {
				$("#tooltip").remove();
				previousPoint = null;
			}
		});
	}
};
	
String.prototype.pad = function(l, s, t){
    return s || (s = " "), (l -= this.length) > 0 ? (s = new Array(Math.ceil(l / s.length)
        + 1).join(s)).substr(0, t = !t ? l : t == 1 ? 0 : Math.ceil(l / 2))
        + this + s.substr(0, l - t) : this;
};

// progress bar function
function makeProgress(target, start, stop, steps, hide_status_text){
	
	hide_status_text = hide_status_text || 0;

	if(isNaN(stop)){
		stop = 0;
	}
	
	var step = (stop - start)/steps;
	
	if(step < 0){
		step = 0;
	}
	
	var increment = function(){
		
		var val = target.progressbar("value");
		var increase = val + step;
		var parsed_increase = parseInt(val + Math.round(step));
		
		if(parsed_increase > stop){
			parsed_increase = stop;
		}
		
		
		if(empty(hide_status_text)){
			if(parsed_increase < 100){
				$("#pbar").html(progress_update + " (" + parsed_increase + "%) ");
			}else{
				$("#pbar").html(progress_update + " (100%) ");
			}
		}
		
		target.progressbar("option", "value", increase);
		
		if (increase < stop) {
			setTimeout(increment,1000);
		}
	};
	
	increment();
};

function get_progress(current_act){

	//current_act = current_act || "";
	var url_path = window.location.origin+window.location.pathname+"?act=";
	
	// We have to set it zero to complete the progress normally
	hide_status_text = 0;
	
	if(status == "-1"){
		setTimeout(function(){
			$.ajax({type: "GET",
				url: url_path + current_act + "&jsnohf=1",
				data: "error=1&actid="+actid,
				success: function(response){
					
					if(current_act == "vs"){
						action_by_id();
						//$("#softcontent").html(response);
					}else{
						var error_box = $(response).find("#error_box");
						if(current_act == "addvs"){
							$("#form-container").html(error_box);
						}else{
							$("#form-container").prepend(error_box);
						}
						$("#form-container").show();
						$("#progress-cont").hide();
						if(current_act == "migrate" || current_act == "clone"){
							$("#running").hide();
						}
						if(current_act == "letsencrypt"){
							$("#ajax_err").html(error_box);
						}
					}
					
					if(current_act == "enable_rescuevs" || current_act == "disable_rescuevs") {
						rescuedonefunc(current_act, 1);
					}
					
					// Override status, once error processing is completed
					status = '';
				}
			});
		}, 1000);
	
	}else if(progress == 100){
		
		$("#pbar").html(progress_update + " (100% ) ");
		$("#progressbar").progressbar("option", "value", 100);
		if(current_act == "letsencrypt"){
			setTimeout(donecert(), 3000);
			return;
		}
		setTimeout(function(){
			$.ajax({type: "GET",
				url: url_path+current_act+"&jsnohf=1",
				data:"done=1&actid="+actid,
				success:function(response){
					
					if(current_act == "vs"){
						//alert("progress 100 : "+ delvpsid)
						done_vps_list.push(delvpsid);
						action_by_id();
					}
					
					if(current_act == "migrate" || current_act == "clone"){
						donefunc(1);
					}
					
					if(current_act == "createtemplate"){
						$("#softcontent").html(response);
						changeplan();
					}
					
					if(current_act == "rebuild"){
						
						var url = window.location.href;						
						
						// If the current act is managevs
						if(url.match(/act=managevps/)){
							$("#rebuild_content").html(response);
							$("#newvs_rootpass").html(newvs_rootpass);	
							return;
						}
						
						$("#softcontent").html(response);
						$("#newvs_rootpass").html(newvs_rootpass);
					}
					
					if(current_act == "addvs"){
						if(ubc_redirect > 0){
							$("#softcontent").html("<div class=\'bg\'>"+response+"</div>");
							setTimeout(function(){
								window.location  = url_path+"ubc&vpsid="+vpsid;
							}, 1000);
						}else{
							$("#softcontent").html("<div class=\'bg\'>"+response+"</div>");
							
							$("#newvs_rootpass").html(newvs_rootpass);
							if(!empty(newvs_vncpass)){
								$("#newvs_vncpass").html(newvs_vncpass);
							}
						}
						
					}
					
					if(current_act == "enable_rescuevs" || current_act == "disable_rescuevs"){
						rescuedonefunc(current_act);
					}
					
				}
			});
		}, 1000);
	}else{
		
		$.ajax({
			type: "GET",
			url: url_path+"tasks",
			data:"api=json&actid="+actid,
			dataType : "json",
			success:function(response){
				var tasks = response.tasks;
				status = tasks[actid]["status"];
				progress = tasks[actid]["progress"];
				progress_update = tasks[actid]["status_txt"];
				vpsid = tasks[actid]["vpsid"];
				
				// We have to create the table in case of migrate and clone (make_mig_table is in migrate_theme)
				if(tasks[actid]["action"] == "migrate2" || tasks[actid]["action"] == "clone2"){
					hide_status_text = 1;
					make_mig_table("#pbar", tasks[actid]["status_txt"]);
				}
				
				makeProgress($("#progressbar"), $("#progressbar").progressbar("value"), progress, 20, hide_status_text);
				
				setTimeout(function(){
					get_progress(current_act);
				}, 1000);
			}
		});
	}
};

//load progress bar
function progress_onload(is_vs){
	
	is_vs = is_vs || 0;
	
	var for_vs = "";
	if(is_vs == 1){
		for_vs = "<div style='font-size:14px;text-align:center;' id='pbar_id'></div>";
	}
	
	document.getElementById("progress_onload").innerHTML = "<br/><div style='display:none;' id='progress-cont'><br/><br/><center><div style='font-size:18px;text-align:center;' id='pbar'>0%</div>"+for_vs+"</center><br /><center><div style='max-width:600px;width:100%;height:18px;' id='progressbar'></div></center><br/><center>"+pbar_notice+" "+pbar_tip+"</center></div>";
};

// Function to show LOGS if any error occurs
function loadlogs(actid){
	
	actid = actid || 0;
	var url_path = window.location.origin+window.location.pathname+"?act=";
	$.ajax({
			
		url: url_path+"tasks&api=json",
		data: "show_logs="+actid,
		dataType : "json",
		method : "post",
		success:function(data){
			
			logs_tab_head = "";
			logs_tab_data = "";
			var j = 0;
			active = "";
			
			common_logs = data["logs_data"]["common_logs"] || "";
			
			if(typeof(data["logs_data"]["logs"]) === "object"){
				
				$.each(data["logs_data"]["logs"], function(i, item){
					
					if(j == 0){
						active = "active";
					}else{
						active = "";
					}
					
					logs_tab_head += "<li class="+active+"><a data-toggle=\"tab\" href=\"#"+i+"_tab\">"+i+"</a></li>";
					logs_tab_data += "<div id=\""+i+"_tab\" class=\"tab-pane in fade "+active+"\"><pre style=\"max-height: 450px;\" >"+item+"</pre></div>";
					j++;
				});
				
				common_logs_html = "";
				if(common_logs != ""){
					common_logs_html = "<div class=\"notice\"><img src='../themes/default/images/notice.gif' /> &nbsp; "+data["l_common_logs"]+"</div>";
				}
				
				logs_html = common_logs_html+"<ul class=\"nav nav-tabs\">"+logs_tab_head+"</ul><div class=\"tab-content\">"+logs_tab_data+"</div>";
				
			}else{
				logs_html = "<pre style=\"max-height: 500px;\" >"+data["logs_data"]["logs"]+"</pre>";				
			}		
			
			$("#logs_modal").modal("show");
			$("#logs_modal_body").html(logs_html);
		}
	});

}

// Password strentgh related functions
function passwordStrength(password1) {
	
	var shortPass = 1, badPass = 2, goodPass = 3, strongPass = 4, mismatch = 5, symbolSize = 0, natLog, score = 0;
	var pass_strength = Array();
	//password < 4
	if ( password1.length < 4 ){
		score = 9;
		pass_strength = [shortPass, parseInt(score)];
		return pass_strength;
	}

	var strength = 0;
	
	if (password1.length > 7) strength += 1.5;
			
	//If password contains both lower and uppercase characters, increase strength value.
	if (password1.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))  strength += 1.5;	
	
	//If it has numbers and characters, increase strength value.
	if (password1.match(/([a-zA-Z])/) && password1.match(/([0-9])/))  strength += 1.5;	
	
	//If it has one special character, increase strength value.
	if (password1.match(/([!,%,&,@,#,$,^,*,?,_,~])/) && password1.match(/([a-zA-Z])/))  strength += 1.5;
	
	//if it has two special characters, increase strength value.
	if (password1.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/) && password1.match(/([a-zA-Z])/)) strength += 1.5;
	
	//If value is less than 2
	if( strength < 2 ){
		pass_strength = [badPass, parseInt((strength/5)*100)];	
		return pass_strength;	
	}
	
	if (strength == 2 ){
		pass_strength = [goodPass, parseInt((strength/5)*100)];	
		return pass_strength;	
	}else{
		pass_strength = [strongPass, parseInt((strength/5)*100)];
		return pass_strength;	
	}
}


function check_pass_strength(id) {
	
	var pass = $("#"+id).val();
	var strength = Array();
	
	$("#"+id+"_pass-strength-result").removeClass("short bad good strong");
	
	if (!pass) {
		display_pass_strength("strength_indicator", 0, id);
		return;
	}
	
	try{
		
		strength = passwordStrength(pass);
		
		if(strength[1] > 100) strength[1] = 100;
		
		switch(strength[0]){
				case 1:
				score  = "bad";// For short password 
				display_pass_strength(score, strength[1], id);
				break;
			case 2:
				score = "bad"; // For bad password 
				display_pass_strength(score, strength[1], id);
				break;
			case 3:
				score = "good";// For good password 
				display_pass_strength(score, strength[1], id);
				break;
			case 4:     		 
				score = "strong";// For Strong password 
				display_pass_strength(score, strength[1], id);
				break;
		}
		
	}catch(e){}
}

function display_pass_strength(score, per, id){
	
	if(typeof per == "undefined") per = 0;
	
	$("#"+id+"_pass-strength-result").addClass(score).html(lang[score] + " ("+per+"/100)");
}

function handle_capping(){
	$("#speed_cap_down").prop("min", 0);
	$("#speed_cap_up").prop("min", 0);
	
	var band_val = $("#band").val();
	if(empty(band_val)){
		band_val = $("#bandwidth").val();
	}
	
	if(!empty(band_val) && !$("#band_suspend").prop("checked")){
		$("#speed_cap_limit").css("display", "block");
		if($("#network_speed").val() > 0){
			$("#speed_cap_down").prop("max", $("#network_speed").val());
			$("#speed_cap_up").prop("max", $("#network_speed").val());
		}
		if($("#upload_speed").val() > 0){
			$("#speed_cap_up").prop("max", $("#upload_speed").val());
		}
	}else{
		$("#speed_cap_limit").css("display", "none");
	}
}	
// Speed Mbits select options
function fillspeedmbits(){
	var str = "";
	str += '<option value="0" selected="selected">'+lang_no_limit+'</option>';
	for(var i = 1; i <= 10; i++){
		str += '<option value="'+(i*128)+'">'+(i*128)+' KB/s ('+i+' Mbit)</option>';
	}
	str += '<option value="'+(15*128)+'">'+(15*128)+' KB/s ('+15+' Mbit)</option>';
	
	for(var i = 20; i <= 100; i += 10){
		str += '<option value="'+(i*128)+'">'+(i*128)+' KB/s ('+i+' Mbit)</option>';
	}
	str += '<option value="'+(1000*128)+'">'+(1000*128)+' KB/s ('+1000+' Mbit)</option>';
	str += '<option value="'+(10000*128)+'">'+(10000*128)+' KB/s ('+10000+' Mbit)</option>';
	$(".speedmbits").html(str).trigger("chosen:updated");	
}


// Submits a FORM
function POST(obj, submitresponse){
	
	submitresponse = submitresponse || 'DOESNT_EXIST';
	
	//Loading(1); // Show the loading text
	
	obj['type'] = "POST";
	obj['dataType'] = "json";
	obj['success'] = function(data, textStatus, jqXHR) {
	
		//Loading(0); // Hide the loading text
				
		// Is there an submitresponse function
		var fn = (typeof submitresponse != 'function' ? window[submitresponse]: submitresponse);
		
		// If its there, then call it
		if(typeof fn === 'function'){
			if(fn(data) == -1){
				return;
			}
		}
		
		// Handles the responses
		handleResponseData(data, obj['extradata']);
		
	};
	
	obj['xhrFields'] = {
		withCredentials: true
	};
		
	obj['crossDomain'] = true;
	
	$.ajax(obj);
	
	return true;
	
};

function handleResponseData(data){	

	// Are there any errors ?
	if(typeof(data["error"]) != 'undefined'){
		error(data["error"]);
	}

	// Are we to show a success message ?
	if(typeof(data["done"]) != 'undefined'){
		done(data["done"]);
	}
	
};

// Shows the error
function error(er){
	
	var count = 0;
	for (k in er) count++;

	// If count is 0 then no error was there
	if(count < 1) return;
	
	var err_str = "";
	// Show the errors
	var count = 1;
	for (k in er) {
		err_str += count++ +") " + er[k] + "\n";
	}

	alert(err_str);
};

// Shows a success message
function done(success){

	var count = 0;
	var goto = "";
	for (k in success) count++;

	// If count is 0 then no success message was there
	if(count < 1) return;

	if(typeof(success["msg"]) != 'undefined'){
		alert(success["msg"]);
	}
	
};

// Creates the TABLE
function table(props, cols, data){

	var elid = props['id'];

	// Final Properties
	var fp = {"width" : '100%',
			"class" : 'table table-hover tablesorter', //shadow altrowstable gridtable
			"border" : '0',
			"cellspacing" : '1',
			"cellpadding" : '8',
			"align" : 'center',
			"tid" : ''
		};

	for (x in props){
		fp[x] = props[x];
	}

	// Create the TABLE
	var table = '<table id="'+fp["tid"]+'" border="'+fp["border"]+'" cellspacing="'+fp["cellspacing"]+'" cellpadding="'+fp["cellpadding"]+'" class="'+fp["class"]+'" align="'+fp["align"]+'" width="'+fp["width"]+'"><thead><tr>';

	// Add the headers
	for(x in cols){
		table += '<th '+(cols[x]["width"] ? 'width="'+cols[x]["width"]+'"' : '')+' '+(cols[x]["class"] ? 'class="'+cols[x]["class"]+'"' : '')+'>'+cols[x]["l"]+'</th>';
	}

	table += '</tr></thead>';

	var $i = 0; // For color

	for(d in data){
		$i++;
		table += '<tr>';

		for(x in cols){
			table += '<td '+(cols[x]["centered"] ? 'align="center"' : '')+'>'+data[d][x]+'</td>';
		}

		table += '</tr>';
	}

	table += '</table>';

	$('#'+elid).html(table);


};

// Auto select IPs from Pool if user specifies the number
function ipchoose(name="ips_int", num=0){
	var label = name;
	num = parseInt(num);
	if(name=="ipv6count"){
		label = "ipv6";
	}
	try{
		for(i=0; i < $_(label).options.length; i++) {
			if(num > 0){
				$_(label).options[i].selected = true;
				num = num - 1;
			}else{
				$_(label).options[i].selected = false;
			}
		}
	}catch(e){	}
}

function processing_symb(show){
	if(show == 1){
		$("#processing_symb").css('display','block');
	}else{
		$("#processing_symb").css('display','none');
	}
}