<?php

//////////////////////////////////////////////////////////////
//===========================================================
// restartservices_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function restartservices_theme(){

global $theme, $globals, $cluster, $user, $l, $getservices, $do, $done;

softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-services icon-head"></i>&nbsp; '.$l['<title>'].'</center>

<script language="javascript" type="text/javascript"><!-- // --><![CDATA[

function restartservice(){
	
	// Hide the question
	$("#question").hide();
	
	// Show the image
	$("#prog_img").show();
	
	$.ajax({
		type: "GET",
		dataType: "json",
		url: "'.$globals['ind'].'act=restartservices&service='.$getservices.'&do=true&api=json",
		success: function(data, textStatus, jqXHR){	
	
			// Hide the image
			$("#prog_img").hide();
			
			// Show the done message
			$("#done").show();
			
		},
		
		// If it aborts, it must be due to a webserver restart. Hence recheck with index
		error: function(jqXHR, textStatus, errorThrown){	
			
			$.ajax({
				type: "GET",
				dataType: "json",
				url: "'.$globals['ind'].'act=manageserver&api=json",
				success: function(data, textStatus, jqXHR){	
			
					// Hide the image
					$("#prog_img").hide();
					
					// Show the done message
					$("#done").show();
					
				}
			});
			
		}
	});
	
}

// ]]></script>

<div class="notice" id="done" style="display:none;"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';

// Are there any services ?
if(empty($getservices)){

	echo '<br /><br />
	<center class="val">'.$l['no_serv'].'</center>';
	
}elseif(!empty($getservices) && empty($do)){

	echo '<br /><br />
	<center>
	<div id="question">
	<span class="val">Are you sure you want to restart the  '.$l["$getservices"].' ?</span>
	<br /><br /><br />
	<a href="javascript:restartservice();" class="btn">'.$l['yes'].'</a>
	<a href="'.$globals['ind'].'" class="btn">'.$l['no'].'</a>
	</div>
	<img src="'.$theme['images'].'loading_50.gif" id="prog_img" style="display:none;" />
	</center>';
	
}

echo '</div>';
softfooter();

}

?>
