<?php

//////////////////////////////////////////////////////////////
//===========================================================
// updates_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function updates_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $info, $report, $updated;

softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-config icon-head"></i> &nbsp;'.$l['heading'].'</center>';

error_handle($error, '100%');

if(!empty($updated)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['updated_successfully'].'</div>';
}else{

if(!empty($report)){
	echo '<table width="100%" cellpadding="8" cellspacing="0" border="0">
	<tr>
		<td colspan="2" class="head">'.$l['update_logs'].'</td>
	</tr>
	<tr>
		<td valign="top">'.implode('<br />', $report['log']).'</td>
		<td width="20%"><img src="'.$theme['images'].'admin/'.(empty($report['status']) ? 'softerror.gif' : 'softok.gif').'" /></td>
	</tr>
	</table>
	<br />';
}

echo '<form accept-charset="'.$globals['charset'].'" name="updatevirtualizor" method="post" action="" class="form-horizontal">

<div class="form-group">
	<div class="col-sm-6 right">
		<label class="control-label">'.$l['cur_ver'].'</label>
	</div>
	<div class="col-sm-6">'.$globals['version'].'</div>
</div>
<div class="form-group">
	<div class="col-sm-6 right">
		<label class="control-label">'.$l['cur_patch'].'</label>
	</div>
	<div class="col-sm-6">'.$globals['patch'].'</div>
</div>
<div class="form-group">
	<div class="col-sm-6 right">
		<label class="control-label">'.$l['latest_ver'].'</label>
	</div>
	<div class="col-sm-6">'.(empty($info['version']) ? $l['not_connect_soft'] : $info['version']).'</div>
</div>
<div class="form-group">
	<div class="col-sm-6 right">
		<label class="control-label">'.$l['latest_patch'].'</label>
	</div>
	<div class="col-sm-6">'.($globals['patch'] < $info['patch'] ? $info['patch'] : $l['no_new_patch']).'</div>
</div>
<br/>
<div style="padding:5px;">'.$info['message'].'</div>
<br/><br/>
<p align="center"><input type="submit" class="btn" name="update" value="'.$l['updatevirtualizor'].'" '.(empty($info['link']) ? 'disabled="disabled"' : '').' /></p>
</form>';

}

echo '</div>';
softfooter();

}

?>