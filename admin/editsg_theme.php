<?php

//////////////////////////////////////////////////////////////
//===========================================================
// editsg_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function editsg_theme(){

global $theme, $globals, $kernel, $user, $l , $cluster, $error, $done, $servergroup, $sgid;

softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-servers icon-head"></i>&nbsp; '.$l['add_sg'].'</center>';

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['edited'].'</div><br />';
}else{

error_handle($error);

echo '<div id="form-container">
<form accept-charset="'.$globals['charset'].'" name="editsg" method="post" action="" class="form-horizontal">
	<div class="row">
		<div class="col-sm-5">
			<label class="control-label">'.$l['sg_name'].'</label>
			<span class="help-block">'.$l['sg_name_exp'].'</span>
		</div>
		<div class="col-sm-6">
			<input type="text" class="form-control" name="sg_name" size="30" value="'.POSTval('sg_name', $servergroup['sg_name']).'" />
		</div>
	</div>
	<div class="row">
		<div class="col-sm-5">
			<label class="control-label">'.$l['sg_reseller_name'].'</label>
			<span class="help-block">'.$l['sg_reseller_name_exp'].'</span>
		</div>
		<div class="col-sm-6">
			<input type="text" class="form-control" name="sg_reseller_name" size="30" value="'.POSTval('sg_reseller_name', $servergroup['sg_reseller_name']).'" />
		</div>
	</div>
	<div class="row">
		<div class="col-sm-5">
			<label class="control-label">'.$l['sg_desc'].'</label>
			<span class="help-block">&nbsp;</span>
		</div>
		<div class="col-sm-6">
			<textarea class="form-control" name="sg_desc" rows="5" cols="40" >'.POSTval('sg_desc', $servergroup['sg_desc']).'</textarea>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-5">
			<label class="control-label">'.$l['sg_select'].'</label>
			<span class="help-block">&nbsp;</span>
		</div>
		<div class="col-sm-6">
			<input type="radio" name="sg_select" value="0" '.POSTradio('sg_select', '0', $servergroup['sg_select']).' /> '.$l['least_util'].' &nbsp; &nbsp; <input type="radio" name="sg_select" value="1" '.POSTradio('sg_select', '1', $servergroup['sg_select']).' /> '.$l['first_avl'].'
		</div>
	</div>
	<div class="row">
		<div class="col-sm-5">
			<label class="control-label">'.$l['sg_ha_enable'].'</label>
			<span class="help-block">'.$l['sg_ha_enable_exp'].'</span>
		</div>
		<div class="col-sm-6">
			<input type="checkbox" class="ios" name="sg_ha" '.POSTchecked('sg_ha', $servergroup['sg_ha']).' value="1"/>
		</div>
	</div>
<br /><br />
<center><input type="submit" name="editsg" class="btn" value="'.$l['sub_but'].'" /></center>
</form>
</div>
</div>';

}

echo '</div>';
softfooter();

}

?>