<?php

//////////////////////////////////////////////////////////////
//===========================================================
// ips_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function ips_theme(){	

global $theme, $globals, $servers, $user, $l, $alerts, $ips, $ippools, $error, $done;

softheader($l['<title>']);
	
echo '
<div class="bg" style="width: 99%">
<center class="tit">
<i class="icon icon-ippool icon-head"></i>&nbsp; '.$l['ip_list'].' 
<span style="float:right"><a href="javascript:showsearch();"><img src="'.$theme['images'].'admin/search.gif" /></a></span>
</center>';
	
error_handle($error);

echo '<script language="javascript" type="text/javascript"><!-- // --><![CDATA[
var from_moveips = 0;
function show_confirm(ipid){
	
	ipid = ipid || 0;
	
	ipsids = new Array();
	
	selopts = $("#selected_do_options option");
	selectvals = new Array();

	for(i=0; i<selopts.length; i++){
		val = selopts.eq(i).val();
		if(val != 0){
			selectvals.push(val);
		}
	}
	
	confirmmsg = ["'.$l["del_conf"].'","'.$l["lockip_conf"].'","'.$l["unlockip_conf"].'","'.$l["moveips_conf"].'"];
	donemsg = ["'.$l["done"].'","'.$l["done_lockip"].'","'.$l["done_unlockip"].'","'.$l["done_moveips"].'"];
	selectIndx = selectvals.indexOf($_("selected_do_options").value);

	if(ipid < 1){

		if(selectIndx == -1){
			alert("'.$l['no_action'].'");
			return false;
		}
	
		$(".ios:checked").each(function() {
			ipsids.push($(this).val());
		});
	
	}else{
		ipsids.push(ipid);
		selectIndx = 0;
	}
	
	if(ipsids.length < 1){
		alert("'.$l['nothing_selected'].'");
		return false;
	}
	
	var finalips = new Object();
	
	// Show select pool dialog if this is for moveips action and this call is not from this dialog
	if(selectvals[selectIndx] == "moveips"){
		if(from_moveips != 1){
		
		$("#moveips_modal").modal({keyboard: true});
		$("#moveips_modal").modal({show:true});
		return;
		
		// This call is from moveips dialog, reset from_moveips flag, and proceed
		}else{
			from_moveips = 0; // reset flag
			
			// Check we got the ippool id, if not return false with alert
			if($("#moveips_pool").val() == undefined){
				alert("'.$l['moveips_noipp_sel'].'");
				return false;
			}
			finalips["ippid"] = $("#moveips_pool").val();
			
		}
	}
	
	var r = confirm(confirmmsg[selectIndx]);
	if(r != true){
		return false;
	}
	
	finalips["do"] = selectvals[selectIndx];
	finalips["ips"] = ipsids.join(",");
	
	// API backward compatibility
	if(selectvals[selectIndx] == "del"){
		finalips["delete"] = finalips["ips"];
	}
	
	$("#progress_bar").show();
	
	$.ajax({
		type: "POST",
		url: "'.$globals['index'].'act=ips&api=json",
		data: finalips,
		dataType : "json",
		success:function(response){
			$("#progress_bar").hide();
			if("done" in response){
				alert(donemsg[selectIndx]);
				location.reload(true);
			}
			if("error" in response){
				alert(response["error"]);
				//location.reload(true);
			}
		},
		error: function(data) {
			$("#progress_bar").hide();
			alert("'.$l['error_occurred'].'");
			return false;
		}
	});
	
	return false;
};

$(document).ready(function(){
    $(\'[data-toggle="tooltip"]\').tooltip();   
});
// ]]></script>

<div id="showsearch" style="display:'.(optREQ('search') || (!empty($ips) && !empty($globals['showsearch'])) ? "" : "none").';">
<form accept-charset="'.$globals['charset'].'" name="ips" method="GET" action="" class="form-horizontal">
<input type="hidden" name="act" value="ips">
		
<div class="form-group_head">
  <div class="row">
	<div class="col-sm-1"></div>
    <div class="col-sm-2"><label>'.$l['sbyip'].'</label></div>
    <div class="col-sm-2"><input type="text" class="form-control" name="ipsearch" id="ipsearch" size="30" value="'.POSTval('ipsearch', '').'"/></div>
    <div class="col-sm-1"><label>'.$l['sbyippool'].'</label></div>
    <div class="col-sm-2"><input type="text" class="form-control" name="ippoolsearch" id="ippoolsearch" size="30" value="'.POSTval('ippoolsearch','').'"/></div>
    <div class="col-sm-1"><label>'.$l['mac'].'</label></div>
    <div class="col-sm-2"><input type="text" class="form-control" name="macsearch" id="macsearch" size="30" value="'.POSTval('macsearch','').'"/></div>
    <div class="col-sm-1"></div>		
  </div>
  <div class="row">
    <div class="col-sm-1"></div>
    <div class="col-sm-2"><label>'.$l['sbvps_name'].'</label></div>
    <div class="col-sm-2"><input type="text" class="form-control" name="vps_search" id="vps_search" size="30" value="'.POSTval('vps_search','').'"/></div>
    <div class="col-sm-1"><label>'.$l['sbservers'].'</label></div>
    <div class="col-sm-2 server-select-lg">
      <select name="servers_search" id="servers_search" class="form-control virt-select" style="width:100%">
        <option value="-1" '.(REQval('servers_search') == -1 ? 'selected="selected"' : '').'>'.$l['status_none'].'</option>';
      
          foreach($servers as $key => $id){
            echo '<option value="'.$key.'" '.(isset($_REQUEST['servers_search']) && REQval('servers_search') == $key ? 'selected="selected"' : '').'>'.$id['server_name'].'</option>';
          }
      echo '</select>
    </div>
		<div class="col-sm-1"><label>'.$l['locked_label'].'</label></div>
		<div class="col-sm-2">
			<select name="lockedsearch" id="lockedsearch"	 class="form-control">
				<option value="-1" '.(REQval('lockedsearch') == -1 ? 'selected="selected"' : '').'>'.$l['show_all'].'</option>
				<option value="showlocked" '.(REQval('lockedsearch') == "showlocked" ? 'selected="selected"' : '').'>'.$l['show_locked'].'</option>
				<option value="hidelocked" '.(REQval('lockedsearch') == "hidelocked" ? 'selected="selected"' : '').'>'.$l['show_unlocked'].'</option>
			</select>
		</div>
    <div class="col-sm-1"></div>
	</div>
	<div class="row">
		<div class="col-sm-12" style="text-align: center;"><button type="submit" name="search" class="go_btn" value="Search" style="margin-left:30px;">'.$l['submit'].'</button></div>
	</div>
</div>
<br />
<br />
</form>
</div>';

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
}

if(empty($ips)){

	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.(optREQ('search') ? $l['no_res'] : $l['no_ips']).'</div>';
	
}else{

page_links($globals['num_res'], $globals['cur_page'], $globals['reslen']);
echo '<br /><br />
<form name="ips" id="ips" method="post" action="" accept-charset="'.$globals['charset'].'" class="form-horizontal">
<table class="table table-hover tablesorter">
	<tr>
		<th align="center">'.$l['id'].'</th>
		<th align="center" width="52px"></th>
		<th align="center">'.$l['ip'].'</th>
		<th align="center">'.$l['assigned_to'].'</th>
		<th align="center">'.$l['ip_mac'].'</th>
		<th align="center">'.$l['ip_pool'].' / '.$l['ip_server'].'</th>
		<th align="center" colspan="2">'.$l['manage'].'</th>
		<th><input type="checkbox" class="select_all" name="select_all" id="select_all"></th>
	</tr>';

$i = 1;

foreach($ips as $k => $v){
	
	$editact = (!empty($v['ipr_netmask']) ? 'editiprange' : 'editips');
	
	echo '<tr>
		<td align="left">'.$k.'</td>
		<td>'.(!empty($v['locked']) ? '<span><img src="'.$theme['images'].'admin/lock.png"  title="'.$l['locked_title'].'"></span> ' : '').(!empty($v['note']) ? '<span><i class="icon-notes" data-toggle="tooltip" title="'.$v['note'].'" style="font-size:15px;vertical-align: middle;;"></i></span>' : '').'</td>
		<td>'.$v['ip'].(!empty($v['ipr_netmask']) ? ' / '.$v['ipr_netmask'] : '').'</td>
		<td>'.(empty($v['vpsid']) ? $l['none'] : $v['hostname'].' (<a href="'.$globals['ind'].'act=editvs&vpsid='.$v['vpsid'].'">'.$v['vpsid'].'&nbsp;&nbsp<img src="'.$theme['images'].'admin/svs_login.gif" /></a>)').'</td>
		<td>'.$v['mac_addr'].'</td>
		<td>'.(empty($v['ippool_name']) ? ($v['ip_serid'] == -1 ? $l['all_servers'] : '[Server] '.$servers[$v['ip_serid']]['server_name']) : $v['ippool_name']).'</td>
		<td align="center" width="24" >
			<a href="'.$globals['ind'].'act='.$editact.'&ipid='.$k.'" title="'.$l['edit_ip'].'"><img src="'.$theme['images'].'admin/edit.png" /></a>
		</td>
		<td align="center" width="24" >
			<a href="javascript:void(0);" onclick="return show_confirm('.$k.');"  title="'.$l['del_ip'].'"><img src="'.$theme['images'].'admin/delete.png" /></a>
		</td>
		<td align="center" width="24" >
				<input type="checkbox" class="ios" name="ips_id[]" id="ips_'.$k.'" value="'.$k.'" />
		</td>
	</tr>';

	
	$i++;
}

echo '</table><br />';
		
}	

echo '
<div class="row bottom-menu">
		
	<div class="col-sm-7"></div>
	<div class="col-sm-5">
	
		<label>'.$l['with_selected'].'</label>
		<select name="selected_do_options" id="selected_do_options" class="form-control">
			<option value="0">---</option>
			<option value="del">'.$l['ms_delete'].'</option>
			<option value="lockip">'.$l['ms_lockip'].'</option>
			<option value="unlockip">'.$l['ms_unlockip'].'</option>
			<option value="moveips">'.$l['ms_moveips'].'</option>
		</select>&nbsp;
		<input type="submit" class="go_btn" value="'.$l['go'].'" onclick="show_confirm(); return false;" />
	
	</div>
</div>
</form>';

page_links($globals['num_res'], $globals['cur_page'], $globals['reslen']);

echo '<center><input type="button" value="'.$l['add_ips'].'" class="link_btn" onclick="window.location =\''.$globals['ind'].'act=addips\';"></center>
<div id="progress_bar" style="height:125px; display:none">
	<br />
	<center>
		<font id="progress_txt" size="4" color="#222222">'.$l['action_msg'].'</font>
		<br>
		<br>
	</center>
	<table id="table_progress" width="500" height="28" cellspacing="0" cellpadding="0" border="0" align="center" style="border:1px solid #CCC; -moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px;background-color:#efefef;">
		<tbody>
			<tr>
				<td id="progress_color" width="100%" style="background-image: url(themes/default/images/bar.gif); -moz-border-radius: 4px; -webkit-border-radius: 4px; border-radius: 4px;"></td>
				<td id="progress_nocolor"> </td>
			</tr>
		</tbody>
	</table>
	<br />
</div>

</div>
';

softfooter();

echo '
<div class="modal fade" role="dialog" id="moveips_modal">
	<div class="modal-dialog" style="width: 60%;">
		<div class="modal-content">
			<div class="modal-header text-center" id="moveips_modal_head">
				<button class="close" data-dismiss="modal">&times;</button>
				<span class="fhead">'.$l['ms_moveips'].'</span>
			</div>
			<div class="modal-body" id="moveips_modal_body" >
				<div class="row">
					<div class="col-sm-6"><label class="control-label">'.$l['select'].' '.$l['ip_pool'].'</label></div>
					<div class="col-sm-6" valign="top">
						<select class="form-control" id="moveips_pool" name="moveips_pool" style="font-size:13px">';
						foreach($ippools as $k => $v){
							echo '<option value="'.$v['ippid'].'">'.$v['ippid'].' - '.$v['ippool_name'].'</option>';
						}	
						echo '</select>
					</div>
				</div>
				<div class="row text-center" style="padding: 10px;">
					<div class="col-sm-12">
					<input type="submit" class="btn btn-primary" id="moveips_sub" value="'.$l['ms_moveips'].'" onclick="from_moveips=1; show_confirm(); return false;" />
				</div>
			</div>
		</div>
	</div>
</div>	
';

}

?>

