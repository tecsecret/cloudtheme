<?php

//////////////////////////////////////////////////////////////
//===========================================================
// multivirt_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function multivirt_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $info, $report, $started, $install_options, $servers;

softheader($l['<title>']);

echo '
<script>

function enable_conf(){
	var y = confirm("'.$l['multi_conf'].'");
	if(y != true){
		return false;
	}
}

</script>
<div class="bg" style="width:100%">
<center class="tit">'.$l['heading'].'<span style="float:right;" ><a href="'.$globals['docs'].'Multi_Virtualization" target="_blank" class="wiki_help" title="'.$l['wiki_help'].'"><i class="icon-help" ></i></a></span></center><br />
<div class="row">
	<div class="col-sm-6">
		<center><img src="'.$theme['images'].'/admin/kvm_100.gif" /> <p style="font-size:20px;display:inline-block;margin:5px;">+</p> <img src="'.$theme['images'].'/admin/openvz_100.gif" /></center>
	</div>
	<div class="col-sm-6">
		<center><img src="'.$theme['images'].'/admin/kvm_100.gif" /> <p style="font-size:20px;display:inline-block;margin:5px;">+</p>  <img src="'.$theme['images'].'/admin/lxc_100.gif" /></center>
	</div>
</div><br />';

error_handle($error, '100%');

if(!empty($started)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['started'].'</div>';
}else{

if(!preg_match('/ /', $servers[$globals['server']]['virt'])){
	
		echo '<form accept-charset="'.$globals['charset'].'" name="enable_multivirt" method="post" action="" class="form-horizontal">
		<div class="row"><br /><br />
			<div class="col-sm-offset-3 col-sm-3">
				<label class="control-label">'.$l['choose_multi'].'</label>
				<span class="help-block">'.$l['choose_multi_exp'].'</span>
			</div>
			<div class="col-sm-4">
				<select class="form-control" name="install_virt" >';
				foreach($install_options as $k => $v){
					echo '<option value="'.$k.'" '.(POSTval('install_virt') == $k ? 'selected="selected"' : '').' >'.$v.'</option>';
				}
				echo '</select>
			</div>
			<div class="col-sm-2">
			</div>
		</div><br />
		
		<p align="center"><input type="submit" class="btn" name="start" value="'.$l['enable_multi'].'" onclick="return enable_conf();" /></p>
	</form>';
}

echo '<div class="notebox" align="left" style="font-size:12px; line-height:150%"><img src="'.$theme['images'].'/notice.gif" align=left/> &nbsp; '.$l['multivirt_exp'].'</div>

<center class="notebox">'.$l['warning'].'</center>';

}
echo '</div>';
softfooter();

}

?>