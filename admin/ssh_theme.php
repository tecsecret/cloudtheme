<?php

//////////////////////////////////////////////////////////////
//===========================================================
// ssh_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function ssh_theme(){

global $theme, $globals, $kernel, $user, $l, $info;

softheader($l['<title>']);

//////////////////////////////////////////////////////////////////////
// Used JCTerm applet for SSH terminal : http://www.jcraft.com/jcterm/
//////////////////////////////////////////////////////////////////////

echo '
<div class="bg">
<center class="tit"><i class="icon icon-ssh icon-head"></i>&nbsp; '.$l['<title>'].'<span style="float:right;" ><a href="'.$globals['docs'].'SSH_Tool" target="_blank" class="wiki_help" title="'.$l['wiki_help'].'"><i class="icon-help" ></i></a></span></center>

<center>
	<applet code="com.jcraft.jcterm.JCTermApplet.class"
            archive="jcterm-0.0.10.jar?'.rand_num(3).',jsch-0.1.46.jar?'.rand_num(3).',jzlib-1.1.1.jar?'.rand_num(3).'"
			codebase="'.$theme['url'].'/java/jcterm/"
			width="650" height="480">
		   
    <param name="jcterm.font_size"  value="13">
	
    <!--
    <param name="jcterm.fg_bg"
		   value="#000000:#ffffff,#ffffff:#000000,#00ff00:#000000">
    -->
    <!--
    <param name="jcterm.config.repository"
           value="com.jcraft.jcterm.ConfigurationRepositoryFS">
    -->
	
	<param name="jcterm.destinations"
          value="root@'.$info['ip'][0].'">
		  
   </applet>
</center>

<br />
<br />
<br />
<br />

<p align="center"><img src="'.$theme['images'].'notice.gif" />&nbsp;'.$l['notice'].'</p>'; 

echo '</div>';
softfooter();

}

?>