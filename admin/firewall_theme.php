<?php

//////////////////////////////////////////////////////////////
//===========================================================
// firewall_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function firewall_theme(){

global $theme,$globals,$cluster,$user,$l, $getservices,$do,$done,$kernel,$output,$error;

if(optGET('ajax')){
	if(!empty($output)){
		foreach ($output as $key => $value){
			echo '<pre>'.$value.'</pre>';
			
			if(!empty($output['port_exist'])){
				echo '<pre><font color="RED">'.$l['port_already_exist'].'</font></pre>';
				return;
			}
			if(!empty($output['ip_exist'])){
				echo '<pre><font color="RED">'.$l['ip_already_exist'].'</font></pre>';
				return;
			}			
		}
	}
	
	if(isset($error)){
		error_handle($error);
		
	}
	return;
}

softheader($l['<title>']);

// Is it offline ?
$hypervisor_status = $cluster->statewise($globals['server']);
if($hypervisor_status == 0 || $hypervisor_status == 2){

	echo '<div class="e_notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['server_status_'.$hypervisor_status].'</div>';
	
}else{
	 $span_id = array('start','stop','restart','status','view','search','version','block','setpolicy','allow','ip_block','ip_allow');	
	echo '<script language="javascript" type="text/javascript"><!-- // --><![CDATA[
	function ipvalidation(ip){	
	
	  if ((ip.length==0) || (ip == null)) {
		  return false;	
	  }
	  if(!ip.match(/^(([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]).){3}([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/ ) || ip=="0"){
		  return false;	
	  }
	  return true;
	};
	
	function portvalidation(port){
		if ((port.length==0) || (port==null)){
			return false;	
		}
		if(!port.match(/^\d+$/ )  || port=="0")	{		// check port number is numeric or not
			return false;	
		}
		return true;
	};
		
	function firewall_onclick(id){
		if(id=="'.$l['onfirewall'].'"){																		//enable firewall
			AJAX("'.$globals['index'].'act=firewall&ajax=true&start=true", "firewall_button(re,\"start\")")	
		}else if(id=="'.$l['restartfirewall'].'"){															// restart firewall
			AJAX("'.$globals['index'].'act=firewall&ajax=true&restart=true", "firewall_button(re,\"restart\")")
		}else if(id=="'.$l['stopfirewall'].'"){																//stop firewall
			AJAX("'.$globals['index'].'act=firewall&ajax=true&stop=true", "firewall_button(re,\"stop\")")
		}else if(id=="'.$l['statusfirewall'].'"){   														// status firewall
			AJAX("'.$globals['index'].'act=firewall&ajax=true&status=true", "firewall_button(re,\"status\")")	
		}else if(id=="'.$l['policyfirewall'].'"){															//setpolicy firewall
			AJAX("'.$globals['index'].'act=firewall&ajax=true&setpolicy=true", "firewall_button(re,\"setpolicy\")")
		}else if(id=="'.$l['versionfirewall'].'"){   															// version firewall
			AJAX("'.$globals['index'].'act=firewall&ajax=true&version=true", "firewall_button(re,\"version\")")
		}else if(id=="'.$l['viewfirewall'].'"){																	//view firewall
			AJAX("'.$globals['index'].'act=firewall&ajax=true&view=true", "firewall_button(re,\"view\")")
		}else if(id=="'.$l['blockfirewall'].'"){				
			
			var port=$_("txtblockport").value;
			if(port=="4082" || port=="4083" || port=="4084" || port=="4085" ){		// Access is denied to block port
				alert("'.$l['port_default'].'")			
				return false;
			}
			
			if( portvalidation(port)){
				AJAX("'.$globals['index'].'act=firewall&ajax=true&block=true&port="+port, "firewall_button(re,\"block\")")
				$_("txtblockport").value="";
			}else {
				$_("block_td").style.display="none";
				$_("txtblockport").focus();
				alert("'.$l['port_error'].'");			
				return false;
			}
		}else if(id=="'.$l['allowfirewall'].'"){													//allow portfirewall
			var port=$_("txtallowport").value;
			if( portvalidation(port)){
				AJAX("'.$globals['index'].'act=firewall&ajax=true&allow=true&port="+port, "firewall_button(re,\"allow\")")
				$_("txtallowport").value="";
			}else{
				$_("allow_td").style.display="none";
				$_("txtallowport").focus();
				alert("'.$l['port_error'].'");			
				return false;	
			}	
		}else if(id=="'.$l['ip_blockfirewall'].'"){													//ipblock firewall
			var ip = $_("txtblockip").value;
			if(ipvalidation(ip)){
				AJAX("'.$globals['index'].'act=firewall&ajax=true&ip_block=true&ip="+ip, "firewall_button(re,\"ip_block\")")
				$_("txtblockip").value="";
			}else{
				$_("ip_block_td").style.display="none";
				$_("txtblockip").focus();
				alert("'.$l['ip_error'].'");			
				return false;		
			}
		}else if(id=="'.$l['ip_allowfirewall'].'"){   												// ipallow firewall
			var ip = $_("txtallowip").value;
			if(ipvalidation(ip))
			{
				AJAX("'.$globals['index'].'act=firewall&ajax=true&ip_allow=true&ip="+ip, "firewall_button(re,\"ip_allow\")")
				$_("txtallowip").value="";
			}else{
				$_("ip_allow_td").style.display="none";
				$_("txtallowip").focus();
				alert("'.$l['ip_error'].'");
				return false;	
			}
		}else if(id=="'.$l['searchip'].'"){ 												// search  firewall
			var ip = $_("txtsearchip").value;
			if(ipvalidation(ip)){
				AJAX("'.$globals['index'].'act=firewall&ajax=true&search=true&ip="+ip, "firewall_button(re,\"search\")")
				$_("txtsearchip").value="";
			}else{
				$_("search_td").style.display="none";
				$_("txtsearchip").focus();
				alert("'.$l['ip_error'].'");			
				return false;	
			}
		}			
	}
	
	function firewall_button(re,opt)
	{';
		foreach($span_id AS $k => $v){
			echo'if(opt=="'.$v.'"){
					$_("'.$v.'_td").innerHTML=re;
					$_("'.$v.'_td").style.display="block";						
				}else{
					$_("'.$v.'_td").style.display="none";														
				}';
		}	// foreach close.
echo '}
// ]]></script>';

echo '
<div class="bg firewall">
<center class="tit"><i class="icon icon-firewall icon-head"></i>&nbsp;<strong> '.$l['<title>'].'</strong></center>';
  
if(!empty($done)){
  echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
}
	
echo '<form accept-charset="'.$globals['charset'].'" name ="firewallform" method="post" action="" class="form-horizontal">
	  
		<div class="row">
			<div class="col-sm-3">
				<input type="button" name="onfirewall" value="'.$l['onfirewall'].'" 
				style=" cursor:pointer" class ="btn" onclick="firewall_onclick(this.value);">
			</div>
			<div class="col-sm-9" valign="top">
				<label class="control-label">'.$l['onfirewall_detail'].'</label><br/><br/>
				<span class="output" id="start_td" style="display: none"></span >	  
			</div>	
		</div>		
		<div class="row">
			<div class="col-sm-3">
				<input type="button" name="restartfirewall" value="'.$l['restartfirewall'].'" 
				style=" cursor:pointer" class ="btn" onclick="firewall_onclick(this.value);" >
			</div>
			<div class="col-sm-9">
				<label class="control-label">'.$l['restartfirewall_detail'].'</label><br/><br/>
				<span class="output" id="restart_td" style="display: table-cell"></span >
			</div>	
		</div>	
		<div class="row">
			<div class="col-sm-3">
				<input type="button" name="stopfirewall" value="'.$l['stopfirewall'].'" 
				style=" cursor:pointer" class ="btn" onclick="firewall_onclick(this.value);" >
			</div>
			<div class="col-sm-9">
				<label class="control-label">'.$l['stopfirewall_detail'].'</label><br/><br/>
				<span class="output" id="stop_td" style="display: none"></span >
			</div>
		</div>
		<div class="row">
			<div class="col-sm-3">
				<input type="button" name="statusfirewall" value="'.$l['statusfirewall'].'" 
				style=" cursor:pointer" class ="btn" onclick="firewall_onclick(this.value)">
			</div>
			<div class="col-sm-9">
				<label class="control-label">'.$l['statusfirewall_detail'].'</label ><br/><br/>
				<span class="output" id="status_td" style="display: none"></span >
			</div>
		</div>
		<div class="row">
			<div class="col-sm-3" width="15%" valign="top" >
				<input type="button" name="setfirewallpolicy" value="'.$l['policyfirewall'].'" 
				style=" cursor:pointer" class ="btn" onclick="firewall_onclick(this.value);">
			</div>
			<div class="col-sm-9" valign="top">
				<label class="control-label"> '.$l['policyfirewall_detail'].'</label><br/><br/>
				<span class="output" id="setpolicy_td" style="display: none"></span >
			</div>
		</div>
		<div class="row">
			<div class="col-sm-3" width="15%" valign="top">
				<input type="button" name="versionfirewall" value="'.$l['versionfirewall'].'"
				style=" cursor:pointer" class ="btn" onclick="firewall_onclick(this.value);">
			</div>
			<div class="col-sm-9" valign="top"> 
				<label class="control-label">'.$l['versionfirewall_detail'].'</label><br/><br/>
				<span class="output" id="version_td" style="display: none"></span >
			</div>
		</div>
		<div class="row">
			<div class="col-sm-3">
				<input type="button" name="viewfirewall" value="'.$l['viewfirewall'].'" 
				style=" cursor:pointer" class ="btn" onclick="firewall_onclick(this.value);">
			</div>
			<div class="col-sm-9" valign="top">	 
				<label class="control-label">'.$l['viewfirewall_detail'].'</label><br/><br/>
				<span class="output" id="view_td" style="display: none"></span >				
			</div>
		</div>		
		<div class="row">
			<div class="col-sm-3">
				<input type="button" name="blockfirewall" value="'.$l['blockfirewall'].'" 
				style=" cursor:pointer" class ="btn" onclick="firewall_onclick(this.value);">
			</div>
			<div class="col-sm-9">
				<label class="control-label">'.$l['blockfirewall_detail_1'].'</label>
				<input type="text" id="txtblockport"  class="form-control" name="txtblockport" style="width : 80px;"> <label>'.$l['blockfirewall_detail'].'</label>
				<br/><br/>
				<span class="output" id="block_td" style="display: none"></span >
			</div>
		</div>		
		<div class="row">
			<div class="col-sm-3" width="15%" valign="top">
				<input type="button" name="allowfirewall" value="'.$l['allowfirewall'].'" 
				style=" cursor:pointer" class ="btn" onclick="firewall_onclick(this.value);">
			</div>
			<div class="col-sm-9" valign="top">
				<label class="control-label">'.$l['allowfirewall_detail_1'].'</label>
				<input type="text" id="txtallowport"  class="form-control" name="txtallowport" style="width : 80px;"> <label>'.$l['allowfirewall_detail'].'</label>
				<br/><br/>
				<span class="output" id="allow_td" style="display: none"></span >
			</div>
		</div>		
		<div class="row">
			<div class="col-sm-3">
				<input type="button" name="ip_blockfirewall" value="'.$l['ip_blockfirewall'].'" 
				style=" cursor:pointer" class ="btn" onclick="firewall_onclick(this.value);">
			</div>
			<div class="col-sm-9" valign="top">
				<label class="control-label">'.$l['ip_blockfirewall_detail'].'</label>
				<input type="text" id="txtblockip" class="form-control" name="txtblockip" style="width : 150px;"><label>'.$l['blockfirewall_detail'].'</label>
				<br/><br/>
				<span class="output" id="ip_block_td" style="display: none"></span >
			</div>
		</div>		
		<div class="row">
			<div class="col-sm-3" width="15%" valign="top">
				<input type="button" name="ip_allowfirewall" value="'.$l['ip_allowfirewall'].'" 
				style=" cursor:pointer" class ="btn" onclick="firewall_onclick(this.value);">
			</div>
			<div class="col-sm-9">
				<label class="control-label">'.$l['ip_allowfirewall_detail'].'</label>
				<input type="text" id="txtallowip" class="form-control" name="txtallowip" style="width : 150px;"><label>'.$l['allowfirewall_detail'].'</label>
				<br/><br/>
				<span class="output" id="ip_allow_td" style="display: none"></span >
			</div>
		</div>
		<div class="row">
			<div class="col-sm-3">
				<input type="button" name="searchip" value="'.$l['searchip'].'" 
				style=" cursor:pointer" class ="btn" onclick="firewall_onclick(this.value);">
			</div>
			<div class="col-sm-9" valign="top">
				 <label class="control-label">'.$l['searchip_detail'].' </label>
				 <input type="text" id="txtsearchip" class="form-control" name="txtsearchip" style="width : 150px;">
				 <br/><br/>
				<span class="output" id="search_td" style="display: none"></span >
			</div>
		</div>
	
	</form> ';
}
 
 echo '</div>';
softfooter();

}

?>
