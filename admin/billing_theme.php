<?php

//////////////////////////////////////////////////////////////
//===========================================================
// billing_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function billing_theme(){

global $theme, $servers, $globals, $kernel, $cluster, $user, $l, $error, $done, $info, $user_plans, $gateways;

softheader($l['<title>']);

echo '
<div class="bg" style="width:99%">
<center class="tit"><i class="icon icon-config icon-head"></i> &nbsp;'.$l['heading'].'</center>';

echo '<div class="alert alert-info">
	<b>'.$l['note'].' : </b> '.$l['bill_note'].'
</div>';

error_handle($error);

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
}

echo '
<div class="divroundshad">
<div class="roundheader">'.$l['bill_set'].'</div>
<br/>
<form accept-charset="'.$globals['charset'].'" action="" method="post" name="config" class="form-horizontal">';
	
echo '
<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['inhouse_billing'].'</label><br />
		<span class="help-block">'.$l['inhouse_billing_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="checkbox" name="inhouse_billing" '.POSTchecked('inhouse_billing', $info['inhouse_billing']).' />
	</div>
</div>
<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['cp_url'].'</label><br />
		<span class="help-block">'.$l['cp_url_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="cp_url" size="30" value="'.POSTval('cp_url', $info['cp_url']).'" />
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['billing_day'].'</label><br />
		<span class="help-block">'.$l['billing_day_exp'].'</span>
	</div>
	<div class="col-sm-6">		
		<select class="form-control" name="billing_day">
			<option value="" '.ex_POSTselect('billing_day', '', '').'>'.$l['bill_last_day'].'</option>';
	
	for($i = 1; $i <= 31; $i++){

		echo '<option value="'.$i.'" '.(empty($_POST['billing_day']) && $info['billing_day'] == $i ? 'selected="selected"' : (@trim($_POST['billing_day']) == $i ? 'selected="selected"' : '') ).'>'.$i.'</option>';
		
	}
	
	echo '</select>
	</div>
</div>

<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['billing_currency'].'</label><br />
		<span class="help-block">'.$l['billing_currency_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="text" class="form-control" name="billing_currency" size="30" value="'.POSTval('billing_currency', @$info['billing_currency']).'" />
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['billing_symbol'].'</label><br />
		<span class="help-block">'.$l['billing_symbol_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="billing_symbol" size="30" value="'.POSTval('billing_symbol', $info['billing_symbol']).'" />
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['billing_warn_bal'].'</label><br />
		<span class="help-block">'.$l['billing_warn_bal_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="billing_warn_bal" size="30" value="'.POSTval('billing_warn_bal', @$info['billing_warn_bal']).'" />
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['billing_min'].'</label><br />
		<span class="help-block">'.$l['billing_min_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="billing_min" size="30" value="'.POSTval('billing_min', $info['billing_min']).'" />
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['billing_max'].'</label><br />
		<span class="help-block">'.$l['billing_max_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="billing_max" size="30" value="'.POSTval('billing_max', $info['billing_max']).'" />
	</div>
</div>

<br/>
<div class="roundheader">'.$l['reg_set'].'</div>
<br/>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['enable_registration'].'</label><br />
		<span class="help-block">'.$l['enable_registration_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="checkbox" name="enable_registration" '.POSTchecked('enable_registration', $info['enable_registration']).' />
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['default_uplid'].'</label><br />
		<span class="help-block">'.$l['default_uplid_exp'].'</span>
	</div>
	<div class="col-sm-6">		
		<select class="form-control" name="default_uplid">';
	
	foreach($user_plans as $k => $v){

		echo '<option value="'.$k.'" '.(empty($_POST['default_uplid']) && $info['default_uplid'] == $k ? 'selected="selected"' : (@trim($_POST['default_uplid']) == $k ? 'selected="selected"' : '') ).'>'.$v['plan_name'].'</option>';
		
	}
	
	echo '</select>
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['user_pass_min_char'].'</label><br />
		<span class="help-block">'.$l['user_pass_min_char_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="user_pass_min_char" size="30" value="'.POSTval('user_pass_min_char', $info['user_pass_min_char']).'" />
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['captcha_secret'].'</label><br />
		<span class="help-block">'.$l['captcha_secret_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="captcha_secret" size="30" value="'.POSTval('captcha_secret', $info['captcha_secret']).'" />
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['captcha_sitekey'].'</label><br />
		<span class="help-block">'.$l['captcha_sitekey_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="captcha_sitekey" size="30" value="'.POSTval('captcha_sitekey', $info['captcha_sitekey']).'" />
	</div>
</div>

<br/>
<div class="roundheader">'.$l['com_set'].'</div>
<br/>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['support_email'].'</label><br />
		<span class="help-block">'.$l['support_email_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="support_email" size="30" value="'.POSTval('support_email', $info['support_email']).'" />
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['billing_comp'].'</label><br />
		<span class="help-block">'.$l['billing_comp_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="billing_comp" size="30" value="'.POSTval('billing_comp', $info['billing_comp']).'" />
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['billing_addr1'].'</label><br />
		<span class="help-block">'.$l['billing_addr1_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="billing_addr1" size="30" value="'.POSTval('billing_addr1', $info['billing_addr1']).'" />
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['billing_addr2'].'</label><br />
		<span class="help-block">'.$l['billing_addr2_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="billing_addr2" size="30" value="'.POSTval('billing_addr2', $info['billing_addr2']).'" />
	</div>
</div>

<br/>
<div class="roundheader">'.$l['payment_gateways'].'</div>
<br/>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['payment_gateways'].'</label><br />
		<span class="help-block">'.$l['payment_gateways_exp'].'</span>
	</div>
	<div class="col-sm-6">';
		foreach ($gateways as $key => $value){
			echo '<div class="col-md-4 checkbox">';
			echo '<label><input type="checkbox" name="gateways[]" value="'.$key.'" '.(!empty($value['status']) || !empty($globals[$key.'_enable']) ? 'checked="checked"' : '').'>'.$value['name'].'</label>';
			echo '</div>';
		}
	echo '</div>
</div>';

foreach($gateways as $key => $value){
	if(!empty($value['status']) || !empty($globals[$key.'_enable'])){
		echo '<br/>
			<div class="roundheader">'.$value['name'].'</div>
		<br/>';
		foreach($value['fields'] as $k => $v){
			
			// Just to handle OLD code
			if($k == 'paypal_sandbox' && !empty($globals['paypal_env']) && $globals['paypal_env'] == 'sandbox'){
				$info[$k] = 1;
			}
			
			echo '<div class="row">
				<div class="col-sm-6">
					<label class="control-label">'.(array_key_exists($l[$k], $l) ? $l[$k] : $v['friendly_name']).'</label><br />
					<span class="help-block">'.(array_key_exists($l[$k.'_exp'], $l) ? $l[$k.'_exp'] : $v['description']).'</span>
				</div>
				<div class="col-sm-6">	
					<input type="'.$v['type'].'" '.($v['type'] == 'checkbox' ? '' : 'class="form-control"').' name="'.$k.'" size="30" '.($v['type'] == 'checkbox' ? POSTchecked($k, $info[$k]) : 'value="'.POSTval($k, $info[$k]).'"').' />
				</div>
			</div>';
		}
	}
}	

echo '</div>
<br /><br />
	
<center>
<input type="submit" name="editsettings" value="'.$l['saveconfig'].'" class="btn"/>
</center>
<br /><br />
</form>';

echo '</div>';

softfooter();

}



