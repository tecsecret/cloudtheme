<?php

//////////////////////////////////////////////////////////////
//===========================================================
// editinvoice_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function editinvoice_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $done, $users, $invoice;

softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-file icon-head"></i>&nbsp; '.$l['_head'].'</center>';

error_handle($error);

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
}

echo '<div id="form-container">
<form accept-charset="'.$globals['charset'].'" name="editinvoice" method="post" action="" class="form-horizontal">

	<div class="row">
		<div class="col-sm-5">
			<label class="control-label">'.$l['select_user'].'</label>
			<span class="help-block">'.$l['exp_select_user'].'</span>
		</div>
		<div class="col-sm-6">
			<select class="form-control" name="uid" id="uid">
			<option value="0" '.(POSTval('uid') == 0 ? 'selected="selected"' : '').'>'.$l['select_user'].'</option>';	
			foreach($users as $k => $v){		 
				echo '<option value="'.$k.'" '.POSTselect('uid', $k, $k == $invoice['uid']).'>'.$v['email'].'</option>';
			}
	
		echo '</select>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-5">
			<label class="control-label">'.$l['invodate'].'</label>
			<span class="help-block">'.$l['exp_invodate'].'</span>
		</div>
		<div class="col-sm-6">
			<input type="text" class="form-control" placeholder="DD/MM/YYYY" name="invodate" id="invodate" size="30" value="'.POSTval('invodate', datetime($invoice['invodate'])).'" />
		</div>
	</div>
	<div class="row">
		<div class="col-sm-5">
			<label class="control-label">'.$l['duedate'].'</label>
			<span class="help-block">'.$l['exp_duedate'].'</span>
		</div>
		<div class="col-sm-6">
			<input type="text" class="form-control" placeholder="DD/MM/YYYY" name="duedate" id="duedate" size="30" value="'.POSTval('duedate', datetime($invoice['duedate'])).'" />
		</div>
	</div>
	<div class="row">
		<div class="col-sm-5">
			<label class="control-label">'.$l['paydate'].'</label>
			<span class="help-block">'.$l['exp_paydate'].'</span>
		</div>
		<div class="col-sm-6">
			<input type="text" class="form-control" placeholder="DD/MM/YYYY" name="paydate" id="paydate" size="30" value="'.POSTval('paydate', datetime($invoice['paydate'])).'" />
		</div>
	</div>
	<div class="row">
		<div class="col-sm-5">
			<label class="control-label">'.$l['item'].'</label>
			<span class="help-block">'.$l['exp_item'].'</span>
		</div>
		<div class="col-sm-6">
			<input type="text" class="form-control" name="item" id="item" value="'.POSTval('item', $invoice['item']).'" />
		</div>
	</div>
	<div class="row">
		<div class="col-sm-5">
			<label class="control-label">'.$l['item_desc'].'</label>
			<span class="help-block">'.$l['exp_item_desc'].'</span>
		</div>
		<div class="col-sm-6">
			<input type="text" class="form-control" name="item_desc" id="item_desc" value="'.POSTval('item_desc', $invoice['item_desc']).'" />
		</div>
	</div>
	<div class="row">
		<div class="col-sm-5">
			<label class="control-label">'.$l['amt'].'</label>
			<span class="help-block">'.$l['exp_amt'].'</span>
		</div>
		<div class="col-sm-6">
			<input type="text" class="form-control" name="amt" id="amt" value="'.POSTval('amt', $invoice['amt']).'" onchange="calc_net()" />
		</div>
	</div>
	<div class="row">
		<div class="col-sm-5">
			<label class="control-label">'.$l['disc'].'</label>
			<span class="help-block">'.$l['exp_disc'].'</span>
		</div>
		<div class="col-sm-6">
			<input type="text" class="form-control" name="disc" id="disc" value="'.POSTval('disc', $invoice['disc']).'" onchange="calc_net()" />
		</div>
	</div>
	<div class="row">
		<div class="col-sm-5">
			<label class="control-label">'.$l['net'].'</label>
			<span class="help-block">'.$l['exp_net'].'</span>
		</div>
		<div class="col-sm-6">
			<input type="text" disabled="disabled" class="form-control" name="net" id="net" value="" />
		</div>
	</div>
	<div class="row">
		<div class="col-sm-5">
			<label class="control-label">'.$l['token'].'</label>
			<span class="help-block">'.$l['exp_token'].'</span>
		</div>
		<div class="col-sm-6">
			<input type="text" class="form-control" name="token" id="token" value="'.POSTval('token', $invoice['token']).'" />
		</div>
	</div>
		
</div>

<br /><br />
<center><input type="submit" class="btn" name="editinvoice" value="'.$l['submit'].'"></center>

</form>

<script language="javascript" type="text/javascript">
function calc_net(){
	var amt = parseFloat($_("amt").value);
	var disc = parseFloat($_("disc").value);
	if(amt > 0){
		$_("net").value = amt - (disc > 0 ? disc : 0);
	}
};

calc_net();
</script>
</div>
</div>
';


softfooter();

}


