<?php

//////////////////////////////////////////////////////////////
//===========================================================
// emailtemp_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function editemailtemp_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $emailtemp, $ll, $done, $notice, $tempname;

softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><img src="'.$theme['images'].'admin/edit.png" />&nbsp; '.$l['edittemp'].'</center>';

error_handle($error);

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
}

// We will load the tiny MCE if html is enabled
echo '
<script type="text/javascript" src="'.$theme['url'].'/js/tinymce/tinymce.min.js"></script>
';

// Are we allowed to edit
if(!empty($notice)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['action_not_applicable'].'</div>';
}else{

echo '<form accept-charset="'.$globals['charset'].'" name="editemailtemp" method="post" action="" class="form-horizontal">

<div class="row">
	<div class="col-sm-3"><label class="control-label">'.$l['subject'].'</label></div>
	<div class="col-sm-9"><input type="text" class="form-control" name="tempsub" value="'.htmlizer($ll['title']).'" size="73"></div>
</div>
<br/>
<div class="row">
	<div class="col-sm-3"><label class="control-label">'.$l['content'].'</label></div>
	<div class="col-sm-9">
		<textarea  cols="70" class="form-control" name="tempcontent" rows="15" id="tempcontent">'.htmlizer($ll['body']).'</textarea>
	</div>
</div>
<br/>
<div class="row">
	<div class="col-sm-3">
		<label class="control-label">'.$l['mail_type'].'</label>
		<span class="help-block">'.$l['mail_type_exp'].'</span>
	</div>
	
	<div class="col-sm-9">
		<input type="checkbox" name="mail_type" '.POSTchecked('mail_type').' '.($ll['mail_type'] == 'html' ? 'checked=checked' : '').' value="1" onchange="load_tinymce();" id="mail_type">
	</div>
</div>
<br/>
';

echo '<script language="javascript" type="text/javascript"><!-- // --><![CDATA[
	
	function confirm_reset(){
		var r = confirm("'.$l['reset_confirm'].'");
		if(r != true){
			return false;
		}else{
			window.location = window.location+"&reset='.$tempname.'";
		}
	}
	
	function load_tinymce(){
		
		if($("#mail_type").prop("checked") == true){
			
			var data = $("#tempcontent").val().replace(/\n/g, "<br>");
			$("#tempcontent").val(data);
			tinymce.init({
				selector:"textarea",
				plugins: [
					"advlist autolink lists link image charmap print preview hr anchor pagebreak",
					"searchreplace wordcount visualblocks visualchars code fullscreen",
					"insertdatetime nonbreaking save table contextmenu directionality",
					"emoticons template paste textcolor"
				],
				toolbar1: "insertfile undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent blockquote| link image",
				toolbar2: "preview media | forecolor backcolor emoticons | formatselect fontselect fontsizeselect ",
				force_br_newlines: false,
				force_p_newlines: false,
				forced_root_block: false,
			});
			
		}else{
			//console.log(tinymce.get("tempcontent").getContent())
			var data = tinymce.get("tempcontent").getContent().replace(/(<br>|<br \/>)/g, "\n");
			tinymce.remove("textarea");
			$("#tempcontent").val(data);
		}
		
	};
	
	$(document).ready(function(){
		load_tinymce();
	});
	
	// ]]></script>';
echo '

<div class="row">
	<div class="col-sm-3"><label class="control-label"></label></div>
	<div class="col-sm-9">
		'.$l['mail_'.$tempname.'_info'].'
	</div>
</div>

<br/><br/>
<center>
	<input type="submit" name="savetemplate" value="'.$l['savetemp'].'" class="btn"/> &nbsp;
	<input type="button" name="resettemplate" value="'.$l['reset_template'].'" class="btn" onClick="confirm_reset();"/><br /><br />
	<a href="'.$globals['index'].'act=emailtemp" class="link_btn">'.$l['temp_overview'].'</a>
</center>

	</br>';
	
	echo '<br /><br />
		
</form>';					
			
}

echo '</div>';
softfooter();

}

?>