<?php

//////////////////////////////////////////////////////////////
//===========================================================
// editippool_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function editippool_theme(){

global $theme, $globals, $kernel, $user, $l , $error, $servers, $error, $ippool, $ippid, $done, $ippool_servers, $servergroups;

softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-ippool icon-head"></i>'.$l['editippool'].'</center>';

error_handle($error);

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
}

// Check the ippid belongs to which servers
foreach($ippool_servers as $p => $q){
	if($q['serid'] == -1){
		$ipgp[$q['ippid']][] = $l['all_servers'];
	}elseif($q['serid'] == -2){
		$ipgp[$q['ippid']][] = '[Group] '.$servergroups[$q['sgid']]['sg_name'];
	}else{
		foreach($servers as $x => $y){
			if($q['serid'] == $y['serid'] || $q['sgid'] == $y['sgid']){
				$ipgp[$q['ippid']][] = $y['server_name'];
			}
		}
	}
}

echo '<script language="javascript" type="text/javascript">
	
	function changeDNS(ips){
		ip = ips.split(\',\');
		$("input[type=text][name=ns1]").val(ip[0]);
		$("input[type=text][name=ns2]").val(ip[1]);
	}
	
	$(document).ready(function(){
		var nat = "'.$ippool['nat'].'";
		if(nat != 0){
			$("#routing").attr("disabled", "disabled");
		}
	});

</script>

<div id="form-container">
<form accept-charset="'.$globals['charset'].'" name="editippool" method="post" action="" class="form-horizontal">

<div class="row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['ippool_name'].'</label>
		<span class="help-block">'.$l['nameofip'].'</span>
	</div>
	<div class="col-sm-4">
		<input type="text" class="form-control" name="ippool_name" id="ippool_name" size="30" value="'.POSTval('ippool_name', $ippool['ippool_name']).'" />
	</div>
	<div class="col-sm-4"></div>
</div>

<div class="row">
	<div class="col-sm-4"><label class="control-label">'.$l['gateway'].'</label></div>
	<div class="col-sm-4"><input type="text" class="form-control" name="gateway" value="'.POSTval('gateway', $ippool['gateway']).'" size="30"></div>
	<div class="col-sm-4"></div>
</div>

<br/>
<div class="row">
	<div class="col-sm-4"><label class="control-label">'.$l['netmask'].'</label></div>
	<div class="col-sm-4">'.(empty($ippool['ipv6']) ? '' : '<label style="float:left; font-size: 25px; padding: 0 22px 0 0;">/</label>').'<input type="text" class="form-control" name="netmask" value="'.POSTval('netmask', $ippool['netmask']).'" size="30" style="width:90%;"></div>
	<div class="col-sm-4"></div>
</div>

<span class="help-block"></span>
<div class="row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['nameserver'].' 1</label>
		<span class="help-block">'.(empty($ippool['ipv6']) ? $l['unsure_1'] : $l['unsure_1_6']).'</span>
	</div>
	<div class="col-sm-4">
		<input type="text" class="form-control" name="ns1" value="'.POSTval('ns1', $ippool['ns1']).'" size="30">
	</div>
	<div class="col-sm-4">
		'.(empty($ippool['ipv6']) ? '<span id="ipv4_ns"><a href="javascript:changeDNS(\'8.8.8.8,8.8.4.4\');">[Google]</a>&nbsp;<a href="javascript:changeDNS(\'208.67.222.222,208.67.220.220\');">[Open DNS]</a>&nbsp;<a href="javascript:changeDNS(\'8.26.56.26,8.20.247.20\');">[Comodo]</a><a href="javascript:changeDNS(\'208.76.50.50,208.76.51.51\');">[Smart Viper]</a></span>' :
		'<span id="ipv6_ns"><a href="javascript:changeDNS(\'2001:4860:4860::8888,2001:4860:4860::8844\');">[Google]</a></span>').'
	</div>
</div>

<div class="row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['nameserver'].' 2</label>
		<span class="help-block">'.(empty($ippool['ipv6']) ? $l['unsure_2'] : $l['unsure_2_6']).'</span>
	</div>
	<div class="col-sm-4">
		<input type="text" class="form-control" name="ns2" value="'.POSTval('ns2', $ippool['ns2']).'" size="30">
	</div>
	<div class="col-sm-4"></div>
</div>

<div class="row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['pool_servers'].'</label><br />
		<span class="help-block">'.$l['pool_servers_exp'].'</span>
	</div>
	<div class="col-sm-4">
		';	
	
		$POSTserid = @$_POST['serid'];
		$POSTserid = empty($POSTserid) ? array() : $POSTserid;
		
		echo '<select class="form-control" name="serid[]" id="serid" size="8" multiple="multiple">
			<option value="-1" '.(in_array(-1, $POSTserid) || (in_array('All Servers', $ipgp[$ippid], true)) ? 'selected="selected"' : '').'>'.$l['all_servers'].'</option>';
			
			foreach ($servergroups as $k => $v){
				echo '<option class="fhead" value="'.$k.'_group" '.(in_array($k.'_group', $POSTserid) || (in_array('[Group] '.$v['sg_name'], $ipgp[$ippid], true)) ? 'selected="selected"' : '').'>[Group]&nbsp;'.$v['sg_name'].'</option>';
				
				foreach ($servers as $m => $n){
					if($n['sgid'] == $k){
						echo '<option value="'.$n['serid'].'" '.(in_array((string)$n['serid'], $POSTserid) || (in_array((string)$n['server_name'], $ipgp[$ippid], true)) ? 'selected="selected"' : '').'>&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;'.$n['server_name'].'</option>';
					}
				}
			}
		echo '</select>
		<span class="help-block"></span>
	</div>
	<div class="col-sm-4"></div>
</div>';

if(empty($ippool['nat']) && empty($ippool['vlan'])){
  echo '<div class="row" id="routing_tr">
			<div class="col-sm-4">
				<label class="control-label">'.$l['add_route'].'</label>
				<span class="help-block">'.$l['exp_add_route'].'</span>
			</div>
			<div class="col-sm-4">
				<input type="checkbox" id="routing" class="ios" name="routing" '.POSTchecked('routing', $ippool['routing']).' size="30">
			</div>
			<div class="col-sm-4"></div>
	</div>';
}

if(!empty($ippool['internal'])){
  echo '<div class="row" id="internal_tr">
			<div class="col-sm-4">
				<label class="control-label">'.$l['is_internal'].'</label>
				<span class="help-block">&nbsp;</span>
			</div>
			<div class="col-sm-4">'.$l['yes'].' ('.$l['bridge'].' : <b>'.$ippool['bridge'].'</b>)
				<span class="help-block">&nbsp;</span>
			</div>
			<div class="col-sm-4"></div>
	</div>';
}

if(!empty($ippool['vlan'])){
  echo '<div class="row" id="vlan_tr">
			<div class="col-sm-4">
				<label class="control-label">'.$l['is_vlan'].'</label>
				<span class="help-block">&nbsp;</span>
			</div>
			<div class="col-sm-4">'.$l['yes'].' ('.$l['bridge'].' : <b>'.$ippool['bridge'].'</b>)
				<span class="help-block">&nbsp;</span>
			</div>
			<div class="col-sm-4"></div>
	</div>';
}

echo '<div class="row" id="mtu">
	<div class="col-sm-4">
		<label class="control-label">'.$l['mtu'].'</label><br />
		<span class="help-block">'.$l['exp_mtu'].'</span>
	</div>
	<div class="col-sm-4">
		<input type="text" class="form-control" name="mtu" value="'.POSTval('mtu', $ippool['mtu']).'" size="10">
	</div>	
	<div class="col-sm-4" style="padding-top:8px;">Bytes</div>
</div>

</div>
<br /><br />
<center><input type="submit" value="'.$l['sub_but'].'" class="btn" name="editippool"></center>

</form>

</div>
';

softfooter();

}

?>