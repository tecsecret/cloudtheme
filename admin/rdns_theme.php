<?php

//////////////////////////////////////////////////////////////
//===========================================================
// rdns_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function rdns_theme(){

global $theme, $globals, $kernel, $user, $l, $cluster, $error, $servers, $done, $ips, $pdns, $pdnsid;

softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-pdns icon-head"></i> '.$l['add_rdns'].'<span style="float:right;" ><a href="'.$globals['docs'].'Configure_Reverse_DNS" target="_blank" class="wiki_help" title="'.$l['wiki_help'].'"><i class="icon-help" ></i></a></span></center>';

error_handle($error);

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
}

echo '<div id="form-container">
<form accept-charset="'.$globals['charset'].'" name="rdns" method="post" action="" class="form-horizontal">

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['zone_name'].'</label><br />
		<span class="help-block">'.$l['exp_zone_name'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="zone_name" value="'.POSTval('zone_name', '').'" size="30">
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['ns'].' 1</label><br />
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="ns1" value="'.POSTval('ns1', '').'" size="30">
	</div>
</div>
<br/>
<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['ns'].' 2</label><br />
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="ns2" value="'.POSTval('ns2', '').'" size="30">
	</div>
</div>
<br/>
<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['ns'].' 3</label><br />
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="ns3" value="'.POSTval('ns3', '').'" size="30">
	</div>
</div>
<br/>
<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['ns'].' 4</label><br />
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="ns4" value="'.POSTval('ns4', '').'" size="30">
	</div>
</div>
<br/>
<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['hostmasteremail'].'</label><br />
		<span class="help-block">'.$l['exp_hostmasteremail'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="hostmasteremail" value="'.POSTval('hostmasteremail', '').'" size="30">
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['pdns_server'].'</label><br />
		<span class="help-block">'.$l['exp_pdns_server'].'</span>
	</div>
	<div class="col-sm-6 server-select-lg">
		<select class="form-control virt-select" name="pdnsid" style="width:100%">';
	foreach($pdns as $k => $v){
		echo '<option value="'.$v['id'].'" '.ex_POSTselect('pdnsid', $pdnsid).' >'.$v['name'].'</option>';
	}
echo '</select>
	</div>
</div>

</div>
<br /><br />
<center><input type="submit" value="'.$l['sub_but'].'" class="btn" name="rdns"/></center>

</form>
</div>
';

softfooter();

}

?>