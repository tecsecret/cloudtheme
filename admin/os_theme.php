<?php

//////////////////////////////////////////////////////////////
//===========================================================
// ostemplates_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function os_theme(){

global $theme, $globals, $kernel, $user, $l, $oslist, $offlist, $ostemplates, $error, $done, $flag_getos, $actid;


softheader($l['<title>']);
echo '
<div class="bg" style="width: 99%">
<center class="tit"><i class="icon icon-ostemplates icon-head"></i> &nbsp; '.$l['tit_ostmp'].'<span style="float:right"><a href="javascript:showsearch();"><img src="'.$theme['images'].'admin/search.gif" /></a><a href="'.$globals['docs'].'Ready_Made_OS_Templates" target="_blank" class="wiki_help" title="'.$l['wiki_help'].'"><i class="icon-help" ></i></a></span></center>';

error_handle($error);

$getos = (int) optGET('getos');

if(!empty($flag_getos)){
	echo '<script language="javascript" type="text/javascript">
		var actid = '.$actid.';
		var progress = 0;
		var oses = new Object();';
		
		if(empty($ostemplates[$getos])){
			$list = $ostemplates;
		}else{
			$list = array($getos => $ostemplates[$getos]);
		}
		
		foreach($list as $k => $v){
			echo 'oses['.$k.'] = new Array(0, 0, "'.$k.'", "'.$v['size'].'", "'.$v['filename'].'", 0);';
		}
	
	echo '
	function get_osprogress(){
		setTimeout(function(){
			$.ajax({type: "GET",
				url: "'.$globals['index'].'jsnohf=1&act=tasks",
				data: "api=json&actid="+actid,
				dataType : "json",
				success: function(response){
					var tasks = response.tasks;
					
					var os = tasks[actid]["os"];
					var dlos = new Array();
					$.each(os, function(index, value){
						dlos[index]=[];
						dlos[index]["osid"] = os[index]["osid"];
						dlos[index]["inprogress"] = os[index]["inprogress"];
						dlos[index]["done"] = os[index]["done"];
						dlos[index]["failed"] = os[index]["failed"];
						dlos[index]["err_msg"] = os[index]["err_msg"];
						if(dlos[index]["inprogress"] != ""){
							dlos[index]["progress"] = os[index]["progress"]
						}
					});
					
					for(x in dlos){
						if(dlos[x]["inprogress"] == 1){
							$("#prog_"+dlos[x]["osid"]).html(dlos[x]["progress"]+"%");
						}
						else if(dlos[x]["done"] == 1){
							$("#prog_"+dlos[x]["osid"]).html(\'<img src="'.$theme['images'].'admin/softok.gif" />\');
							$("#err_"+dlos[x]["osid"]).html(dlos[x]["err_msg"]);
						}
						else if(dlos[x]["failed"] == 1){
							$("#prog_"+dlos[x]["osid"]).html(\'<img src="'.$theme['images'].'admin/softerror.gif" />\');
							$("#err_"+dlos[x]["osid"]).html(dlos[x]["err_msg"]);
						}
					}
					if(tasks[actid]["progress"] == 100){
						$("#done_notice").show();
						return false;
					}
					setTimeout("get_osprogress()", 100);
				}
			});
		}, 1000);
	};
		
	$(document).ready(function(){
		for(x in oses){
			if(oses[x][0] == 0){
				$("#getostable").append(\'<tr><td>File : \'+oses[x][4]+\'</td><td>Size : \'+Math.round(oses[x][3]/1024/1024)+\' MB</td><td id="prog_\'+oses[x][2]+\'" class="val" width="100">'.$l['pending'].'</td></tr><tr><td height="20px"colspan="3" align="right" id="err_\'+oses[x][2]+\'"></td></tr>\');
			}
		}
		
	setTimeout("get_osprogress()", 100);
	
});

	</script>
	<div id="doing_div">
		<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>
		<center><table id="getostable" border="0" width="100%" cellpadding="4" ></table></center>
	</div>
	<br />
	<div class="notice" style="display:none;" id="done_notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['down_done'].'</div>';
}
else{

	echo '
	<div id="showsearch">
			
		<div class="form-group_head">
		  <div class="row">
		    <div class="col-sm-3"></div>
		    <div class="col-sm-1"><label>'.$l['vstype'].'</label></div>
		    <div class="col-sm-2">
		      <select name="vstype" style="width:99%" id="vstype" class="form-control">
				<option value="0" >'.$l['status_none'].'</option>
				<option value="xen" >'.$l['osxen'].'</option>
				<option value="xenhvm" >'.$l['osxenhvm'].'</option>
				<option value="xcp" >'.$l['osxcp'].'</option>
				<option value="xcphvm" >'.$l['osxcphvm'].'</option>
				<option value="openvz" >'.$l['osopenvz'].'</option>
				<option value="kvm" >'.$l['oskvm'].'</option>
				<option value="lxc" >'.$l['oslxc'].'</option>
				<option value="vzo" >'.$l['osvzo'].'</option>
				<option value="vzk" >'.$l['osvzk'].'</option>
				<option value="proxo" >'.$l['osproxo'].'</option>
				<option value="proxk" >'.$l['osproxk'].'</option>
				<option value="proxl" >'.$l['osproxl'].'</option>
		      </select>
		    </div>
		    <div class="col-sm-1"><label>'.$l['distrotype'].'</label></div>
		    <div class="col-sm-2">
		      <select name="ostype" style="width:99%" id="ostype" class="form-control">
		        <option value="0"></option>
		        <option value="centos" >'.$l['oscentos'].'</option>
		        <option value="ubuntu" >'.$l['osubuntu'].'</option>
		        <option value="debian" >'.$l['osdebian'].'</option>
		        <option value="fedora" >'.$l['osfedora'].'</option>
		        <option value="suse" >'.$l['osopensuse'].'</option>
		        <option value="scientific" >'.$l['osscientific'].'</option>
			<option value="webuzo" >'.$l['oswebuzo'].'</option>
		      </select>
		    </div>
			<div class="col-sm-3"></div>
		  </div>
		</div>
		
		<br />
		<br />
	</div>
	<form accept-charset="'.$globals['charset'].'" name="os" method="post" action="" class="form-horizontal">
	<table id="oslist" class="table table-hover tablesorter" style="  width: 50%;margin: auto;">

	<tr>
		<th align="center" width="65">'.$l['head_os'].'</th>
		<th align="center" width="60">'.$l['head_type'].'</th>
		<th align="center" width="60">'.$l['head_name'].'</th>
		<th align="center" width="60">'.$l['head_size'].'</th>
		<th align="center" width="20">&nbsp;</th>
	</tr>';
	//r_print($oslist);
	
	foreach($offlist as $k => $v){

		foreach($v as $kv => $vv){
		
			foreach($vv as $kvv => $vs){
				$class_os = (($vs['type'] == 'xcp' || $vs['type'] == 'xen') && !empty($vs['hvm'])) ?  $vs['type'].'hvm' : $vs['type'];
				
				echo'<tr class="'.$class_os.' '.$vs['distro'].' template_rows">
				<td align="center"><img src="'.distro_logo($kv).'" /></td>
				<td align="center"><img src="'.$theme['images'].'admin/'.$vs['type'].(!empty($vs['hvm']) ? 'hvm' : '').'_42.gif" /></td>
				<td align="left">'.$vs['filename'].'</td>
				<td align="center">'.round($vs['size']/1024/1024).' MB</td>
				<td align="center"> <input type="checkbox" class="ios" name="osids[]" value="'.$kvv.'" '.(isset($_POST['osids']) ? (@in_array($kvv, $_POST['osids']) ? 'checked="checked"' : '') : (!empty($ostemplates[$kvv]) ? 'checked="checked"' : '')).' /></td>
				</tr>';
				
			}
					
		}
		
	}

	echo '</table><br /><br />
	<center><input id="savesettings" type="submit" name="savesettings" value="'.$l['submit'].'" class="btn"></center>

	</form>
	
	<script language="javascript" type="text/javascript">
	
		function showsearch(){
			if($_("showsearch").style.display == ""){
				$_("showsearch").style.display="none";
			}else{
				$_("showsearch").style.display="";
			}
		};
		
		$("select#vstype").on("change", function(){
			var value = $(this).val();
			$("select#ostype").attr("value", 0);
			if(value == 0){
				$(".template_rows").show();
			}else{
				$(".template_rows").hide();
				$("."+value).show();
			}
		});
		
		$("select#ostype").on("change", function(){
			
			var vs = $("select#vstype").val(),
				os = $(this).val();
			
			if(vs == 0 && os != 0){
				$(".template_rows").hide();
				$("."+os).show();
			}else if(os != 0){
				$(".template_rows").hide();
				$("."+vs+"."+os).show();
			}else if(os == 0 && vs == 0){
				$(".template_rows").show();
			}else{
				$(".template_rows").hide();
				$("."+vs).show();
			}
			
		});
	</script>';
}
echo '</div>';
softfooter();

}

?>