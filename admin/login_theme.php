<?php

//////////////////////////////////////////////////////////////
//===========================================================
// login_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function login_theme(){

global $theme, $globals, $kernel, $user, $l, $error;

echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset='.$globals['charset'].'" />
<meta name="keywords" content="softaculous, software" />
<title>'.$l['<title>'].'</title>
<link rel="stylesheet" type="text/css" href="'.css_url('bootstrap.min.css', 'style.css').'" />
<link rel="shortcut icon" href="'.$theme['images'].'favicon.ico" />
<script language="javascript" src="'.js_url('jquery.js', 'TweenLite.min.js', 'EasePack.min.js', 'bg-animate.js').'" type="text/javascript"></script>
<style>
.alert{
border:none;
}
.login {
margin: 168px 0;
position: relative;
}
.login label {
color: #707478;
}
.login .login-header {
position: absolute;
top: -110px;
left: 50%;
right: 0;
padding: 0 40px;
margin-left: -50%;
font-weight: 300;
}
.login .login-header .brand {
padding: 0;
font-size: 28px;
}
.login .login-header .brand .logo {
border: 14px solid transparent;
border-color: #4DCACA #31A3A3 #1D8888;
width: 28px;
height: 28px;
position: relative;
font-size: 0;
margin-right: 10px;
top: -9px;
}
.login .login-header .brand small {
font-size: 14px;
display: block;
}
.login .login-header .icon {
position: absolute;
right: 40px;
top: -2px;
opacity: .1;
filter: alpha(opacity=10);
}
.login .login-header .icon i {
font-size: 70px;
}
.login .login-content {
padding: 30px 40px;
color: #999;
margin: 0 auto;
}
.login-v2 {
background: #787878;
color: #ccc;
margin: 168px auto;
position: relative;
-webkit-border-radius: 4px;
-moz-border-radius: 4px;
border-radius: 4px;
}
.login-v2 .login-content {
padding: 40px;
}
.login-v2 .form-control1 {
background: rgba(0, 0, 0, 0.4);
border: none;
color: #fff;
}
.form-control1 {
border: 1px solid #ccd0d4;
-webkit-box-shadow: none;
box-shadow: none;
font-size: 12px;
border-radius: 3px;
-webkit-border-radius: 3px;
-moz-border-radius: 3px;
}
.login-cover, .login-cover-bg, .login-cover-image {
position: fixed;
top: 0;
left: 0;
right: 0;
bottom: 0;
}
.login-cover-image img {
max-width: 100%;
top: 0;
left: 0;
position: absolute;
}
.login-cover-bg {
background: #666;
background: -moz-radial-gradient(center, ellipse cover, #666 0, #000 100%);
background: -webkit-gradient(radial, center center, 0, center center, 100%, color-stop(0, #666), color-stop(100%, #000));
background: -webkit-radial-gradient(center, ellipse cover, #666 0, #000 100%);
background: -o-radial-gradient(center, ellipse cover, #666 0, #000 100%);
background: -ms-radial-gradient(center, ellipse cover, #666 0, #000 100%);
background: radial-gradient(ellipse at center, #666 0, #000 100%);
filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#666666",  endColorstr="#000000",  GradientType=1);
opacity: .6;
filter: alpha(opacity=60);
}
.fade {
opacity: 0;
-webkit-transition: opacity .3s linear;
transition: opacity .3s linear;
}
.btn {
font-weight: 300;
-webkit-border-radius: 3px;
-moz-border-radius: 3px;
border-radius: 3px;
}
.btn.active:focus, .btn:active:focus, .btn:focus {
outline: 0;
}
.login .login-content {
color: #999;
}.btn-block {
padding-left: 12px;
padding-right: 12px;
}
.btn.active, .btn:active {
-webkit-box-shadow: inset 0 3px 5px rgba(0, 0, 0, .1);
box-shadow: inset 0 3px 5px rgba(0, 0, 0, .1);
}
.btn.btn-default {
color: #fff;
background: #b6c2c9;
border-color: #b6c2c9;
}
.btn-default.active, .btn-default:active, .btn-default:focus, .btn-default:hover, .open .dropdown-toggle.btn-default {
background: #929ba1;
border-color: #929ba1;
}
.btn.btn-success {
background: #00acac none repeat scroll 0 0;
border-color: #00acac;
color: #fff;
}
.btn.btn-success.active, .btn.btn-success:active, .btn.btn-success:focus, .btn.btn-success:hover, .open .dropdown-toggle.btn-success {
background: #008a8a;
border-color: #008a8a;
}
.error{
background-color: #DF7000;
color: #FFF;
padding: 15px;
}
</style>
</head>
<body style="overflow:hidden">
<div class="login-cover" style="z-index:0">
	<div class="login-cover-image"></div>
	<div class="login-cover-bg"></div>
	<div id="large-header" class="large-header" style="position:absolute;z-index:1">
		<canvas id="demo-canvas"></canvas>	
	</div>
</div>';

// Normal Login form
if(empty($_REQUEST['ltoken'])){
	echo '
		<form accept-charset="'.$globals['charset'].'" action="" method="post" name="loginform" class="form-horizontal">
		<!-- begin #page-container -->
		<div id="page-container" class="fade in">
			<!-- begin login -->
			<div class="col-sm-4 col-md-4 col-lg-4"></div>
			<div class="login login-v2 animated fadeIn col-sm-4 col-md-4 col-lg-4" data-pageload-addclass="animated fadeIn">
				<!-- begin brand -->
				<div class="login-header">
					<div class="brand">
						<a href="'.$globals['ind'].'"><img src="'.(empty($globals['logo_login_url']) ? $theme['images'].'loginlogo.png' : $globals['logo_login_url']).'" alt="" class="img-responsive"/></a>
						<!--<span class="logo"></span>-->
						<!--<small>Tag line</small>-->
					</div>
					
					<!--<div class="icon">
						<i class="fa fa-sign-in"></i>
					</div>-->
				</div>
				
				<!-- end brand -->
				<div class="login-content">
				'.error_handle($error, '', 0, 1).'
					<div class="form-group m-b-20">
						<input class="form-control form-control1 input-lg" placeholder="'.$l['log_user'].'"  type="text" name="username" id="username" value="'.POSTval('username', '').'">
					</div>
					<div class="form-group m-b-20">
						<input class="form-control form-control1 input-lg" placeholder="'.$l['log_pass'].'" type="password" name="password" value="">
					</div><br />
					<div class="login-buttons form-group">
						<input type="submit" name="login" value="'.$l['sub_but'].'" class="btn btn-success btn-block btn-lg" />
					</div>
				</div>
			</div>
			<!-- end login -->
			<div class="col-sm-4 col-md-4 col-lg-4"></div>
		</div>
		</form>';
		
// 2FA login form for OTP
}else{
	echo '	
		<!-- Two Factor OTP form -->

		<form accept-charset="'.$globals['charset'].'" action="" method="post" name="twofactform" class="form-horizontal">
		<!-- begin #page-container -->
		<div id="page-container" class="fade in">
			<!-- begin login -->
			<div class="col-sm-4 col-md-4 col-lg-4"></div>
			<div class="login login-v2 animated fadeIn col-sm-4 col-md-4 col-lg-4" data-pageload-addclass="animated fadeIn">
				<!-- begin brand -->
				<div class="login-header">
					<div class="brand">
						<a href="'.$globals['ind'].'"><img src="'.(empty($globals['logo_login_url']) ? $theme['images'].'loginlogo.png' : $globals['logo_login_url']).'" alt="" class="img-responsive"/></a>
					</div>					
				</div>
				
				<!-- end brand -->
				<div class="login-content">
				'.error_handle($error, '', 0, 1).'
					<div class="form-group m-b-20">
						<input class="form-control form-control1 input-lg" placeholder="'.$l['log_otp'].'"  type="password" name="otp" id="otp" value="'.POSTval('otp', '').'">
					</div>
					<div class="login-buttons form-group">
						<input type="hidden" name="submitotp" value="1" />
						<input type="submit" name="submitotp" value="'.$l['login_sub_email'].'" class="btn btn-success btn-block btn-lg" />
					</div>
				</div>
			</div>
			<!-- end login -->
			<div class="col-sm-4 col-md-4 col-lg-4"></div>
		</div>
		</form>
	
	';
}

echo '
<script language="javascript" type="text/javascript"><!-- // --><![CDATA[
window.onload = function(){ 
	$("#username").focus();
}
// ]]></script>
<br />
</body>
</html>';
	
}

?>