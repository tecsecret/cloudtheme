<?php

//////////////////////////////////////////////////////////////
//===========================================================
// listrecipes_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function recipes_theme(){

global $theme, $globals, $kernel, $users, $l, $error, $done, $SESS, $recipe;

softheader($l['<title>']);

echo '
<div class="bg" style="width:99%">
<center class="tit">
<i class="icon icon-recipes icon-head"></i>&nbsp; '.$l['usr_tit'].'<span style="float:right"><a href="javascript:showsearch();"><img src="'.$theme['images'].'admin/search.gif" /></a></span></center>';

error_handle($error);

echo '<script language="javascript" type="text/javascript"><!-- // --><![CDATA[

function Delrecipe(rid){

	rid = rid || 0;
	
	// List of ids to delete
	var usr_list = new Array();
	
	if(rid < 1){
		
		if($("#user_task_select").val() == 0){
			alert("'.$l['no_action'].'");
			return false;
		}
		// Multi action
		var multi_action = $_("user_task_select").value;
		var act_val = $("#user_task_select :selected").text();
		
		$(".ios:checked").each(function() {
			usr_list.push($(this).val());
		});
	
	}else{
		
		usr_list.push(rid);
		
		// Single action
		var multi_action = "delete";
		var act_val = "'.$l['ms_delete'].'";
		
		if(usr_list.length < 1){
			alert("'.$l['nothing_selected'].'");
			return false;
		}
	}
	
	if(usr_list.length < 1){
		alert("'.$l['nothing_selected'].'");
		return false;
	}
	
	var usr_conf = confirm("'.$l['conf1'].'"+ act_val + "'.$l['conf2'].'");
	
	if(usr_conf == false){
		return false;
	}
	
	var finalData = new Object();
	finalData[multi_action] = usr_list.join(",");
	
	//alert(finalData);
	//return false;
	
	$("#progress_bar").show();
	
	$.ajax({
		type: "POST",
		url: "'.$globals['index'].'act=recipes&api=json",
		data : finalData,
		dataType : "json",
		success: function(data, a, b){
			$("#progress_bar").hide();
			if("done" in data){
				alert("'.$l['action_completed'].'");
				location.reload(true);
			}
		},
		error: function(data) {
			$("#progress_bar").hide();
			//alert(data.description);
			return false;
		}
	});
	
	return false;
};

// ]]></script>
<div id="showsearch" style="display:'.(optREQ('search') || (!empty($recipe) && !empty($globals['showsearch'])) ? "" : "none").';">
<form accept-charset="'.$globals['charset'].'" name="recipes" method="GET" action="" class="form-horizontal">
<input type="hidden" name="act" value="recipes">
<div class="form-group_head">
<div class="row">
	<div class="col-sm-2"></div>
	<div class="col-sm-1">
		<label>'.$l['search_id'].'</label>
	</div>
	<div class="col-sm-2">
		<input type="text" class="form-control" name="rid" id="rid" size="15" value="'.REQval('rid','').'" />
	</div>
	<div class="col-sm-2">
		<label>'.$l['search_recipe'].'</label> 
	</div>
	<div class="col-sm-2">
		<input type="text" class="form-control" name="rname" id="rname" size="30" value="'.REQval('rname', '').'" style="float: left; width: 80%;"/> &nbsp;<img class="wiki_help" title="'.$l['wiki_help'].'" style="height:22px; width:22px;" src="'.$theme['images'].'admin/information.gif" />
	</div>
	<div class="col-sm-1"></div>
	<div class="col-sm-1 sm_space"><input type="submit" class="go_btn" name="search" value="'.$l['submit'].'"/></div>
</div>
</div>
</form>
<br />
<br />
</div>';

if($done){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['saved'].'</div><br />';
}

if(empty($recipe)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.(optREQ('search') ? $l['no_res'] : $l['no_users']).'</div>';
}
else{

page_links($globals['num_res'], $globals['cur_page'], $globals['reslen']);
echo '<br /><br />
<form accept-charset="'.$globals['charset'].'" name="multi_recipes" id="multi_recipes" method="post" action="">
<table align="center" cellpadding="8" cellspacing="1" border="0" width="95%" class="table table-hover tablesorter" class="form-horizontal">
<tr>
	<th width="10">'.$l['usr_id'].'</th>
	<th width="100">'.$l['rec_logo'].'</th>
	<th width="500">'.$l['usr_type'].'</th>
	<th>'.$l['rec_desc'].'</th>
	<th width="5%">'.$l['status'].'</th>
	<th colspan="2" width="10">'.$l['manage'].'</th>
	<th><input type="checkbox" class="select_all" name="select_all" id="select_all"></th>
</tr>';

$i = 1;

foreach($recipe as $k => $v){	
	if(!empty($v['status'])){  	 	 
		$status_img = '<img src="'.$theme['images'].'online.png" title="'.$l['activated'].'" alt="'.$l['activated'].'">';  	 	 
	}else{  	 	 
		$status_img = '<img src="'.$theme['images'].'offline.png" title="'.$l['deactivated'].'" alt="'.$l['deactivated'].'">';  	 	 
	}
	echo '<tr>
			<td align="left">'.$v['rid'].'</td>
			<td align="center"><img src="'.(empty($v['logo']) ? $theme['images'].'recipes.png' : $v['logo']).'" title="'.$v['name'].'" width="32px"></td>
			<td>'.$v['name'].'</td>
			<td>'.$v['desc'].'</td>
			<td align="center">'.$status_img.'</td>
			<td width="20" valign="middle" align="center">
				<a href="'.$globals['ind'].'act=editrecipe&rid='.$k.'" title="'.$l['edit_usr'].'"><img src="'.$theme['images'].'admin/edit.png" /></a>
			</td>
			<td width="2" valign="middle" align="center">
				<a href="javascript:void(0);" onclick="return Delrecipe('.$k.');"  title="'.$l['del_usr'].'"><img src="'.$theme['images'].'admin/delete.png" /></a>
			</td>
			<td width="2" valign="middle" align="center">
				<input type="checkbox" class="ios" name="recipe_list[]" value="'.$k.'"/>
			</td>
		</tr>';
	
	$i++;
}

echo '</table>
<div class="row bottom-menu">
		
	<div class="col-sm-7"></div>
	<div class="col-sm-5"><label>'.$l['with_selected'].'</label>
		<select name="user_task_select" id="user_task_select" class="form-control">
			<option value="0">---</option>
			<option value="delete">'.$l['ms_delete'].'</option>
			<option value="activate">'.$l['ms_active'].'</option>
			<option value="deactivate">'.$l['ms_deactive'].'</option>
		</select>&nbsp;
		<input type="submit" id ="user_submit" class="go_btn" name="user_submit" value="Go" onclick="Delrecipe(); return false;">
	</div>
</div>
</form>

<div id="progress_bar" style="height:125px; display:none">
	<br />
	<center>
		<font id="progress_txt" size="4" color="#222222">'.$l['action_msg'].'</font>
		<br>
		<br>
	</center>
	<table id="table_progress" width="500" height="28" cellspacing="0" cellpadding="0" border="0" align="center" style="border:1px solid #CCC; -moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px;background-color:#efefef;">
		<tbody>
			<tr>
				<td id="progress_color" width="100%" style="background-image: url(themes/default/images/bar.gif); -moz-border-radius: 4px; -webkit-border-radius: 4px; border-radius: 4px;"></td>
				<td id="progress_nocolor"> </td>
			</tr>
		</tbody>
	</table>
	<br>
	<center>
		'.$l['notify_msg'].'
	</center>
</div>';

}// End of else

page_links($globals['num_res'], $globals['cur_page'], $globals['reslen']);

echo '<br />
<center><a href="'.$globals['ind'].'act=addrecipe" title="'.$l['add_usr'].'" class="link_btn">'.$l['add_usr'].'</a></center>';

echo '</div>';

softfooter();

}
?>