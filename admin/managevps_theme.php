<?php

//////////////////////////////////////////////////////////////
//===========================================================
// managevps_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function managevps_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $cluster, $servers, $oslist, $done, $virt;
global $plans, $ips, $ips6, $ips6_subnet, $ips_int, $ipools, $vps, $servers, $ostemplates, $users, $isos, $resources, $mgs, $servergroups, $scripts, $iscripts, $storages, $stid, $disk_space, $os_check, $storage_uuids, $disks, $primary_ip, $dnsplans, $common_logs, $supported_nics, $cpu_modes, $SESS, $backup_plans, $bus_driver;
global $server_config, $iscripts_allowed, $webuzo_apps, $webuzo_templates, $ha_enabled;

//Is Ajax Mode on?
if(optGET('ajax')){	
	$planid = optGET('plan');
	$uid  = (int) optGET('uid');
	$vpsid = (int) optGET('vpsid');
	$vps = getvps($vpsid);
	if(!empty($uid)){
		useriso($uid);
		$options = '';		
		$iso = array('iso', 'sec_iso');
		$res = array();
		foreach($iso as $isov){
			$options_eu_iso = $options_iso = '';
			$options = '<option value="0">'.$l['none'].'</option>';
			foreach($isos as $k => $v){
				if(!empty($v['isuseriso'])){
					$options_eu_iso .= '<option value="'.$k.'" '.($vps[$isov] == $k ? 'selected="selected"' : "").'>'.$v['name'].'</option>';
				}else{
					$options_iso .= '<option value="'.$k.'" '.($vps[$isov] == $k ? 'selected="selected"' : "").'>'.$v['name'].'</option>';
				}
			}

			if(!empty($options_iso)){
				$options .= '<optgroup label="'.$l['admin_iso'].'">'.$options_iso.'</optgroup>';
			}			
			if(!empty($options_eu_iso)){
				$options .= '<optgroup label="'.$l['eu_iso'].'">'.$options_eu_iso.'</optgroup>';
			}
			$res[$isov] = $options;
		}		
		if(!empty($res)){
			echo json_encode($res);
		}
		return true;
	}
	
	echo 'var $plan = new Object();';
	
	foreach($plans[$planid] as $k => $v){
		
		if($k == 'dns_nameserver'){
			$tmp_dns = unserialize($v);
			echo '$plan["dns_nameserver"] = '.json_encode($tmp_dns).';';
			continue;
		}
		
		if($k == 'mgs'){
			$tmp_mgs = cexplode(',', $v);
			echo '$plan["mgs"] = '.json_encode($tmp_mgs).';';
			continue;
		}
		
		if($k == 'speed_cap' && !empty($v)){
			$v = _unserialize($v);
			echo '$plan["speed_cap_down"] = '.$v['down'].';
				$plan["speed_cap_up"] = '.$v['up'].';
			';
		}

		if($k == 'ippoolid'){
			echo '$plan["plan_ips"] = '.json_encode($ips).';';
			echo '$plan["internal_ips"] = '.json_encode($ips_int).';';
			echo '$plan["ipsv6"] = '.json_encode($ips6).';';
			echo '$plan["ipsv6_subnet"] = '.json_encode($ips6_subnet).';';
			continue;
		}

		echo '$plan["'.$k.'"] = "'.str_replace('"', '\"', $v).'";';
	}
	
	return true;
}

if(optGET('api') == 'json'){
	
	if(optGET('enable_rescue') || optGET('disable_rescue') || optGET('install_script') || optGET('loadscripts') || optGET('scriptreqid')){
		
		if(!empty($error)){
			$response['error'] = $error;
		}
		
		if(!empty($done['rescue_enabled']) || !empty($done['rescue_disabled']) || 
			!empty($done['webuzo_enabled']) || !empty($done['allowed_scripts']) || 
			!empty($done['webuzo_apps'])){
			$response['done'] = $done;
		}
		
		echo array2json($response);
	}
	
}

softheader($l['<title>']);

// Is it offline ?
$hypervisor_status = $cluster->statewise($globals['server']);
if($hypervisor_status == 0 || $hypervisor_status == 2){
	echo '<div class="bg">';
	echo '<center class="tit"><i class="icon icon-vs icon-head"></i>&nbsp; '.$l['add_vs'].'</center>';
	echo '<div class="e_notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['server_status_'.$hypervisor_status].'</div>';
	echo '</div>';
}else{

echo '
<div class="bg">
<div class="tit" style="display:block;text-align:center;"><i class="icon icon-vs icon-head"></i>'.$l['heading'].'<div class="status"><span class="status_txt" id="vps_status"></span></div></div>';

error_handle($error);

// Rescume mode checking. VPS edit not allowed if Rescue Mode is ON
if(!empty($vps['rescue'])){
	
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '. $l['rescue_edit_no_allowed'].'</div>';	
}

if(!empty($vps['locked'])){
	
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '. $l['err_vps_locked'].'</div>';
}

echo '<div class="notice" id="done_msg" style="display:none"></div><div id="working_div" class="working_div" style="display:none"></div><div style="display:none" id="display_error" class="error alert alert-danger"></div>';

if(!empty($done)){
		
	$msg= '';
	$msg .= (!empty($done['done']) ? $l['done'] : '');
	
	// if any changes done then reboot msg dispaly
	if(!empty($done['mac']) || !empty($done['dns'])){
		$msg .= '<BR />'.$l['reboot'];
	}
	
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$msg.'</div>';	
}

echo '<link rel="stylesheet" type="text/css" href="'.$theme['url'].'/css2/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="'.$theme['url'].'/js2/jquery.dataTables.min.js"></script>';

echo '<script language="javascript" type="text/javascript">';

// Parse and include haproxy.js file
echo parse_lang(file_get_contents($theme['url'].'/js/haproxy.js'));

echo '

// For rescue mode operation
var actid = null;
var progress = 0;
var progress_update = "";

var ips = new Object();';

foreach($ips as $k => $v){
	echo 'ips['.$k.'] = "'.$v['ip'].'";';
}
foreach($vps['ips'] as $k => $v){
	echo 'ips['.$k.'] = "'.$v.'";';
}

echo 'function addrow(id, val){

	var t = $_(id);
	var lastRow = (t.rows.length);
	var x= t.insertRow(lastRow);
	var y = x.insertCell(0);
	y.innerHTML = \'<input type="text" style="width:80%;float:left" class="form-control" name="ips[]" value="\'+(val || "")+\'" onblur="checkippool()" size="20" /></td><td><a class="delip" title="'.$l['rem_from_ips'].'"><img style="padding-top:10px;" src="'.$theme['images'].'admin/delete.png" width="12"/></a><br/><span class="help-block">&nbsp;</span>\';
	
	ipdel();	

};

function changestatus(action){
		
	var status = '.$vps["stats"]["status"].';
	
	if(action){
		if(action == "start" ||  action == "unsuspend" || action == "restart"){
			status = 1;
		}else if(action == "stop" ||  action == "poweroff"){
			status = 0;
		}else if (action == "suspend"){
			status = 2;
		}
		$("#vps_status").removeClass();
		$("#vps_status").addClass("status_txt");
	}
	
	if(status == 0){
		$("#vps_status").html("'.$l['status_offline'].'").addClass("offline");
	}else if (status == 1){
		$("#vps_status").html("'.$l['status_online'].'").addClass("online");
	}else if (status == 2){
		$("#vps_status").html("'.$l['status_suspended'].'").addClass("suspended");
	}
};

function cputopology(){
	if(is_checked("enable_cpu_topology")){
		$("#cpu_topology_option").show();
	}else{
		$("#cpu_topology_option").hide();
		$("#topology_sockets").val(0);
		$("#topology_cores").val(0);
		$("#topology_threads").val(0);
	}
}

function adduser(){
	var uid = parseInt($_("uid").value);
	
	if(uid < 1){
		$(".user_details_f").show();
		$(".user_details").show();
	}else{
		$(".user_details").hide();
		$(".user_details_f").hide();
	}
	loadiso(uid);
}

function loadiso(uid){
	
	if(uid == 0){
		return false;
	}	
	if(AJAX("'.$globals['index'].'act=managevps&vpsid='.$vps['vpsid'].'&ajax=true&uid="+uid, "isodetails(re)")){
		return false;
	}else{
		return true;
	}
};

function isodetails(re){
	var re = $.parseJSON(re);
	$.each(re, function(k,v){
		if((k == "iso") && ($("#iso").length > 0)){
			$("#iso").empty().append(v).trigger("chosen:updated");
		}
		if((k == "sec_iso") && ($("#sec_iso").length > 0)){
			$("#sec_iso").empty().append(v).trigger("chosen:updated");
		}
	});
};

function load_scripts(){
	processing_symb(1);
	if(AJAX("'.$globals['index'].'act=managevps&vpsid='.$vps['vpsid'].'&loadscripts=1&api=json", "scriptdetails(re)")){
		return false;
	}else{
		return true;
	}
}

function install_webuzo(){
	if(!confirm("'.$l['webuzo_install_data_lost_warn'].'")){
		return false;
	}
	var iscript_data = $("#install_script").serialize();
	$("#webuzo_operation").hide();
	processing_symb(1);	
	$.ajax({
		type: "POST",
		url: "'.$globals['index'].'act=managevps&install_script=1&vpsid='.$vps['vpsid'].'&api=json",
		dataType : "json",
		data : iscript_data,
		success:function(response){
			processing_symb();
			if("done" in response){
			alert("'.$l['webuzo_install_start'].'");
			var htm = "<center><label class=\"control-label\">'.$l['webuzo_install_start'].'</label></center>";
				$("#webuzo_content").html(htm);
				$("#webuzo_operation").hide();
				location.reload();
				return;
			}
			
			// Are there any errors ?
			if(typeof(response["error"]) != "undefined"){
				var errors = "";
				for(x in response["error"]){
					errors = errors + "\n" + response["error"][x];
				}
				$("#webuzo_operation").show();
				alert(errors);
			}
		}
	});
}

function webuzo_apps(stack){
	stack = stack || 0;
	if($("#stack1").data("demo") === "1"){
		$("#webuzo_stack_tr").hide();
		return;
	}
	$("#webuzo_stack_tr").slideDown("slow");	
	if(stack == \'lamp\'){
		$("#webuzo_webserver_tr").show();
	}else{
		$("#webuzo_webserver_tr").hide();
	}
}

function script_req(sid){
	
	if(sid == 0 || $("#script_isfree").val() > 0){
		return;
	}
	$("#stack1, #stack2, #stack3").prop("checked", false);
	processing_symb(1);
	if(AJAX("'.$globals['index'].'act=managevps&vpsid='.$vps['vpsid'].'&api=json&scriptreqid="+sid, "scriptdetails(re, true)")){
		return false;
	}else{
		return true;
	}
};

function scriptdetails(re, scriptreq){
	
	processing_symb();
	
	var re = $.parseJSON(re);
	scriptreq = scriptreq || false;
	
	if("error" in re){
		alert(re["error"]);
		$("#webuzo_operation").hide();
		$("#webuzo_unsupport").show().text(re["error"]);
	}else{
		$("#webuzo_operation").show();
		$("#webuzo_unsupport").hide().text("");
	}
		
	if("webuzo_apps" in re["done"]){
		for(x in re["done"][\'webuzo_apps\']){
			var id = \'webuzo_\'+x;
			var str = "";
			var val = re["done"][\'webuzo_apps\'][x];
			for(k in val){
				str += \'<div class="col-sm-4"><table><tr><td><input type="radio" name="\'+x+\'" id="serverver_\'+k+\'" value="\'+val[k][\'softname\']+\'" /></td><td width="12%"></td><td><label class="_label" for="serverver_\'+k+\'">\'+val[k][\'fullname\']+\'</label></td></tr></table></div>\';
			}
			$("#"+id).html(str);
		}
	}
	
	if("isfree" in re["done"]){
		$("#stack2_tr, #stack3_tr").hide();
		$("#stack1").prop("checked", true).data("demo", "1");
		$("#script_isfree").val(1);
	}else{
		$("#stack2_tr, #stack3_tr").show();
		$("#stack1").data("demo", "0");
	}
	
	if(scriptreq == true){
		return;
	}
	
	var options = \'<option value="0">'.$l['none'].'</option>\';
	if("allowed_scripts" in re["done"]){	
		for(x in re["done"][\'allowed_scripts\']){
			options += \'<optgroup label="\'+x+\'">\';
			var val = re["done"][\'allowed_scripts\'][x];
			for(k in val){
				options += \'<option value="\'+k+\'">\'+val[k][\'name\']+\'</option>\';
			}
			options += \'</optgroup>\';
		}
	}
	$("#webuzo_scriptlist").html(options).trigger("chosen:updated");
}


function navigateto(window, div, tab){
	
	tab = tab || "";
	div = div || "";
	window = window || "";
	
	// If there is no window passed we will return as we do not have anything to process
	if(window == ""){
		return false;
	}	
	
	if ($("#"+window).is(":hidden")){
		$("#"+window).slideDown("fast");
		$("#"+window+"_toggle_indicator").text(" - ");
	}
	
	// If it is advanced tab we will first need to show the tab
	if(window == "advanced" && tab != ""){	
		$(".nav-tabs a[href=\"#"+tab+"\"]").tab("show");
	}
	
	// If there is not div passed we assume we have to navigate to the window itself
	if(div == ""){
		div = window;
	}
	
	setTimeout("showdiv(\'"+div+"\');", 200);
}

function showdiv(navdiv){
		
	$("body, html").animate({ 
        scrollTop: $(\'#\'+navdiv).offset().top - ($(window).height() / 2)
	}, "fast");
	
	var originalColor = $("#"+navdiv).css("background");
	
	$("#"+navdiv).css("background", "#ffffb7");
	$("#"+navdiv).css("padding", "5px");
	
	setTimeout(function(){
		$("#"+navdiv).css("background", originalColor);
	}, 3000);
}

// Flag to determine whether to apply plan changes if plan is changed
// 1: apply, 0: dont apply
var f_apply_plan = 0;

$(document).ready(function(){
	
	$("#editvs input, #editvs select, #editvs textarea").each(function(){
			
		var elem = $(this);
		
	   // Save current value of element
	   elem.data("oldVal", elem.val());
	  	     
	});
	
	$("input[type=checkbox]").each(function () {
			
		var elem = $(this);
		
		// Save current value of element
		if(elem.is(":checked")){
		   elem.data("ischecked", 1);
		}else{
			elem.data("ischecked", 0);
		}
		
	});
	
	var numdisks = $(".size").length;
	var numips = $(".ips").length;
	
	$("#hdd").data("numdisks", numdisks);
	$("#iplist").data("numips", numips);	
	
	$("#editvs").submit(function(){
		
		
		$("#working_div").show();
		var space = new Array();
		var size = new Array();
		var storage = new Array();
		var disk_uuid = new Array();
		var tmp_space = new Object();
		
		size = $(".size").map(function(){
			return this.value;
		}).get();
		
		storage = $(".storages_list").map(function(){
			return this.value;
		}).get();
		
		disk_uuid = $(".disk_uuids").map(function(){
			return this.value;
		}).get();
		
		if($(".bus_driver")){
			bus_driver = $(".bus_driver").map(function(){
				return this.value;
			}).get();
		
			bus_driver_num = $(".bus_driver_num").map(function(){
				return this.value;
			}).get();
		}
		
		 for(i = 0; i < $(".size").length; i++){
			 
			 if($(".bus_driver")){
			 	tmp_space[i] = {size: size[i], st_uuid: storage[i], disk_uuid : disk_uuid[i], bus_driver: bus_driver[i], bus_driver_num: bus_driver_num[i]};
			 }else{
			 	tmp_space[i] = {size: size[i], st_uuid: storage[i], disk_uuid : disk_uuid[i]};
			 }
			space.push(tmp_space[i]);
		}
			
		var json_data = JSON.stringify(space);
		$("#hdd").val(json_data);
		
		var newnumdisks = $(".size").length;
		var newnumips = $(".ips").length;
		
		var ips = document.getElementsByName("ips[]");
		
		try{
			
			if($("#hdd").data("numdisks") != newnumdisks){
				$("#hdd").addClass("post_data");	
			}
			
			handle_multiselect_post($(".ips"), $("#iplist").data("numips"), newnumips, "ips[]");
								
		}catch(ere){
			
		}
		
		// This variable is used as a flag, if true (any one IP changed), then we have to post all IPs
		var toPostAllIPs = false;
		
		// This variable is used as a flag, if true (any one topology param, or cores changed),
		// then we will have to post all topology parameters
		var toPostTopology = false;
		
		$("#editvs input, #editvs select, #editvs textarea").each(function(){
			var elem = $(this);
			
			if (elem.data("oldVal") != elem.val()){
						
				var newval = elem.val();
				var oldval = elem.data("oldVal");
				
				// Is it an Object ? We need a separate handle for objects
				if(typeof(newval) == "object" || typeof(oldval) == "object"){
					
					if(handle_multiselect_post(elem, oldval, newval, elem.attr("name"))){
						
						if(["ips[]", "ipv6[]", "ipv6_subnet[]", "ips_int[]"].indexOf(elem.attr("name")) != -1){
							toPostAllIPs = true;
						}
					}
									
				}else {
					
					
					if(["ips[]", "ipv6[]", "ipv6_subnet[]", "ips_int[]"].indexOf(elem.attr("name")) != -1){
						toPostAllIPs = true;
					}else if(["topology_sockets", "topology_cores", "topology_threads", "cores"].indexOf(elem.attr("name")) != -1){
						toPostTopology = true;
					}
					
					
					
					if(elem.attr("name") == "tmp_space[]"){				
						$("#hdd").addClass("post_data");
					} else if(elem.attr("name") == "space" || elem.attr("name") == "ipv6count") {
						// Do nothing
					} else {
						elem.addClass("post_data");
					}
				}
			}
		});
		
		// If any one IP has changed, post all IPs (IPv4, IPv6, IPv6 Subnets and Internal IPs)
		if(toPostAllIPs){
			$("input[name=\'ips[]\']").each(function(i,t){
				$(t).addClass("post_data");
			});
			$("select[name=\'ipv6[]\'], select[name=\'ipv6_subnet[]\'], select[name=\'ips_int[]\']").addClass("post_data");
		}
		
		// If any one of topology paramters including CPU core has changed, then submit "topology_sockets", "topology_cores",
		// "topology_threads" and "cores" data too
		if(toPostTopology){
			$("input[name=\'topology_sockets\'], input[name=\'topology_cores\'], input[name=\'topology_threads\'], input[name=\'cores\']").addClass("post_data");
		}
		
		var params = $(".post_data").serialize();
		
		$("input[type=checkbox]").each(function(){
			
			var elem = $(this);
			
			var ischecked = elem.is(":checked") ? 1 : 0;
			
			if((elem.data("ischecked") != ischecked)){
				
				if(elem.hasClass("cpupin") && ischecked > 0){
					ischecked = elem.attr("value");
				}
				
				params = params + "&" + elem.attr("name") + "=" + ischecked;
			}	
			
		});
		$.ajax({
			type : "POST",
			url : "'.$globals['index'].'act=managevps&api=json&vpsid='.$vps['vpsid'].'",
			data : params + "&editvps=1&theme_edit=1"+(f_apply_plan == 1 ? "&apply_plan=1" : ""),
			dataType : "json",
			success: function(data){
				if("done" in data){
					var msg = \'<img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'\';
					$("#done_msg").css("display", "").html(msg);
					location.reload(true);
				}
				
				if("error" in data){

					var errors = "'.$l['following_errors_occured'].'" + "<ul>";
					for(x in data["error"]){
							errors = errors + "<li>" + data["error"][x] + "</li>";		
					}
					errors =  errors + "</ul>";
					$("#display_error").css("display", "").html(errors);
				}
				$("#working_div").css("display", "none");
			}
		});
		
		window.scrollTo(0, 0);
		$("#display_error").css("display", "none");
		return false;
	});
	
	$(".chosen").chosen({width: "100%"});	
});

// Constant Types, used in v_ObjCompare and v_compare
var OBJ_TYPE = "[object Object]";
var ARR_TYPE = "[object Array]";

// Compare two objects or arrays
function v_obj_compare(v1, v2){
	
	// Get both arguments type
	var typeV1 = Object.prototype.toString.call(v1);
	var typeV2 = Object.prototype.toString.call(v2);
	
	// Compare types
	if(typeV1 !== typeV2){
		return false;
	}
	
	// Check they are comparable objects or array
	if([OBJ_TYPE, ARR_TYPE].indexOf(typeV1) < 0 || [OBJ_TYPE, ARR_TYPE].indexOf(typeV2) < 0){
		return false;
	}
	
	// Compare their lengths
	var v1Len = typeV1 == ARR_TYPE ? v1.length : Object.keys(v1).length;
	var v2Len = typeV2 == ARR_TYPE ? v2.length : Object.keys(v2).length;
	
	if(v1Len !== v2Len){
		return false;
	}
	
	// Compare their values
	if(typeV1 == ARR_TYPE){
		
		for(var i = 0; i < v1.length; i++){
			if(v_compare(v1[i], v2[i]) == false){
				return false;
			}
		}
		
	}else{
		
		for(var i in v1){
			if(v_compare(v1[i], v2[i]) == false){
				return false;
			}
		}
		
	}
	return true;
	
}

// Compare any two objects or arrays or values
// Two passed arguments are equal only if their type, number of elements and values matches.
// Returns true if equal and false otherwise
// This function calls v_obj_compare() if arguments passed are array or object
function v_compare(v1, v2){
	
	// Get type
	var typeV1 = Object.prototype.toString.call(v1);
	
	// If type is Object or Array
	if([ARR_TYPE, OBJ_TYPE].indexOf(typeV1) >= 0){
		// make call to array/obj compare
		return v_obj_compare(v1,v2);
	}
	
	if(v1 !== v2){
		return false;
	}
	
	return true;
	
}

function handle_multiselect_post(ele, oldvalue, newvalue, ele_name){
	
	var changed = 0;
	
	if((oldvalue && newvalue && !v_compare(oldvalue, newvalue)) || (!oldvalue && newvalue)){
		ele.addClass("post_data");
		changed = 1;
	}else if(oldvalue && !newvalue){
		$("#editvs").append(\'<input type="text" name="\'+ele_name+\'" style="display:none" class="post_data" value="" />\');
		changed = 1;
	}
	return changed;
	
}

function add_storage_list(id, val){
	
	var y = id.insertCell(2);
	var z = id.insertCell(3);

	var str = \'<select class="form-control storages_list" name="storages[]">\';';
	
	foreach($storages as $sk => $sv){
		echo 'str += \'<option value="'.$sv['st_uuid'].'" '.($sv['st_uuid'] == $storages[$stid]['st_uuid'] ? 'selected="selected"' : '').'>'.$sv['name'].'&nbsp;('.$sv['disk_space'].' GB '.(empty($sv['oversell']) ? $l['free'] : $l['oversell_free']).')</option>\';';
	}
	
	echo 'str += \'</select>\';
	y.innerHTML = str;
	y.style.paddingTop = "10px";
	z.style.paddingTop = "10px";
	z.innerHTML = \'<a class="delstorage" title="'.$l['rem_storage'].'"><img  src="'.$theme['images'].'admin/delete.png" width="12" /></a>\';
	storagedel();

	
};

function add_storage(id, val){

	var t = $_(id);
	var lastRow = (t.rows.length);
	var x = t.insertRow(lastRow);
	var y = x.insertCell(0);
	var z = x.insertCell(1);

	y.innerHTML = \'<input name="size" class="form-control size" type="text" size="15" value="" />\';
	z.innerHTML = \'&nbsp;'.$l['space_gb'].'\';

	y.style.paddingTop = "10px";
	z.style.paddingTop = "10px";
	add_storage_list(x);
	
};

function add_storage_bus(id, val){

	var t = $_(id);
	var lastRow = (t.rows.length);
	var x = t.insertRow(lastRow);
	var y = x.insertCell(0);
	var p = x.insertCell(1);
	var r = x.insertCell(2);
	var s = x.insertCell(3);
	var j = x.insertCell(4);
	var k = x.insertCell(5);
	
	var str1 = \'<select class="form-control bus_driver" name="bus_driver[]" onchange="toggle_driver_num(this.value)" >\';';
						
	foreach($bus_driver as $bdk => $bdv){
		echo 'str1 += \'<option value="'.$bdk.'">'.$bdv.'</option>\';';
	}
							
	echo 'str1 += \'</select>\';

	var str2 = \'<select class="form-control bus_driver_num" name="bus_driver_num[]">\';';
	
	echo 'str2 += \'<option class = "sata virtio scsi" value="0" >0</option>\';';
	echo 'str2 += \'<option class = "sata virtio scsi" value="1" >1</option>\';';
	echo 'str2 += \'<option class = "ide sata virtio scsi" value="2" >2</option>\';';
	echo 'str2 += \'<option class = "ide sata virtio scsi" value="3" >3</option>\';';
	echo 'str2 += \'<option class = "virtio scsi" value="4" >4</option>\';';
	echo 'str2 += \'<option class = "sata virtio scsi" value="5" >5</option>\';';
	echo 'str2 += \'<option class = "virtio scsi" value="6" >6</option>\';';
	echo 'str2 += \'<option class = "virtio scsi" value="7" >7</option>\';';
	echo 'str2 += \'<option class = "virtio scsi" value="8" >8</option>\';';
	echo 'str2 += \'<option class = "virtio scsi" value="9" >9</option>\';';
	echo 'str2 += \'<option class = "virtio scsi" value="10" >10</option>\';';
	echo 'str2 += \'<option class = "virtio scsi" value="11" >11</option>\';';
	echo 'str2 += \'<option class = "virtio scsi" value="12" >12</option>\';';
	echo 'str2 += \'<option class = "virtio scsi" value="13" >13</option>\';';
	echo 'str2 += \'<option class = "virtio" value="14" >14</option>\';';
	echo 'str2 += \'<option class = "virtio" value="15" >15</option>\';';


	echo 'str2 += \'</select>\';
	
	r.innerHTML = \'<input name="size" class="form-control size" type="text" size="15" value="" />\';
	s.innerHTML = \'&nbsp;'.$l['space_gb'].'\';
	
	r.style.paddingTop = "10px";
	s.style.paddingTop = "10px";
					
	var str4 = \'<select class="form-control storages_list" name="storages[]" style="width:100%">\';';
	
	foreach($storages as $sk => $sv){
		echo 'str4 += \'<option value="'.$sv['st_uuid'].'" '.($sv['st_uuid'] == $storages[$stid]['st_uuid'] ? 'selected="selected"' : '').'>'.$sv['name'].'&nbsp;('.$sv['disk_space'].' GB '.(empty($sv['oversell']) ? $l['free'] : $l['oversell_free']).')</option>\';';
	}
	
	echo 'str4 += \'</select>\';
	
	y.innerHTML = str1;
	y.style.paddingTop = "10px";
	p.innerHTML = str2;
	p.style.paddingTop = "10px";
	j.innerHTML = str4;
	j.style.paddingTop = "10px";
	k.style.paddingTop = "10px";
	k.innerHTML = \'<a class="delstorage" title="'.$l['rem_storage'].'"><img  src="'.$theme['images'].'admin/delete.png" width="12"/></a>\';
	toggle_driver_num("sata");
	storagedel();
};

function ipdel(){
	$(".delip").each(function(){
		$(this).unbind("click");
		$(this).click(function(){
			var parent = $(this).parent();
			parent.remove();
			checkippool();
		});
	});
}

function enable_accel(){
	
	if(is_checked("kvm_vga")){
		$("#enable_acceleration").show();
	}else{
		$("#enable_acceleration").hide();
	}
	
}

function storagedel(){
	$(".delstorage").each(function(){
		$(this).unbind("click");
		$(this).click(function(){
			var parent = $(this).parent();
			
			if($(this).prev().prev().attr("disabled") == "disabled"){
				var y = confirm("'.$l['del_storage_warn'].'");
				if(y == false){
					return;
				}
			}
			var granparent = parent.parent();
			parent.remove();
			granparent.remove();
		});
	});
}

function toggle_driver_num(driver_type){
	$(".ide, .sata, .virtio, .scsi").hide();
	$("." + driver_type).show();
};


$(document).ready(function(){
	ipdel();
	storagedel();
});

function adddnsrow(id, val){
	var t = $_(id);
	var ele = document.createElement("div");
    ele.setAttribute("class","row");
    ele.innerHTML=\'<div class="col-sm-12"><input type="text" class="form-control" name="dns[]"  value="\'+(val || "")+\'"  size="20" /></div>\';
    t.appendChild(ele);

    var sp = document.createElement("span");
    sp.setAttribute("class","help-block");
    t.appendChild(sp);
	
};

function checkippool(){
	try{
	// Clear the Select
	while($_("iplist").options.length != 0){
		$_("iplist").remove(0);
	}
	
	vpsips = curips();
	
	for(x in ips){
		is_cur = 0;	
		for(xx in vpsips){
			if(vpsips[xx] == ips[x]){
				is_cur = 1;
			}
		}
		if(is_cur == 0){
			createOption(x, ips[x]);
		}
	}
	}catch(e){	}
	
	if($("#iplist option").length === 0){
		$("#iplist_parent").hide();
	}else{
		$("#iplist_parent").show();
	}
};

function addtoips(){
	try{
	for(i=0; i < $_("iplist").options.length; i++) {
		if ($_("iplist").options[i].selected) {
			addrow("iptable", ips[$_("iplist").options[i].value]);
		}
	}
	
	checkippool();
	
	// Make the Table again
	while($_("iptable").rows.length != 0){
		$_("iptable").deleteRow(0);
	}
	
	for(x in vpsips){
		addrow("iptable", vpsips[x]);
	}
	}catch(e){	}
};

function createOption(val, txt){
	var opt = document.createElement("option");
	opt.text = txt;
	opt.value = val;
	
	if(document.all && !window.opera){
		$_("iplist").add(opt);
	}else{
		$_("iplist").add(opt, null);
	}
};

function curips(){
	var vpsips = new Object();
	var cur_ips = document.getElementsByName("ips[]");
	for(i=0; i < cur_ips.length; i++){
		if(cur_ips.item(i).value != ""){
			vpsips[i] = cur_ips.item(i).value;
		}
	}
	return vpsips;
};

function planips(num){
	if(num > 0){
		try{
			for(i=0; i < num; i++) {
				$_("iplist").options[i].selected = true;
			}
		}catch(e){	}
		addtoips();
	}
};

function countipv6(){
	try{
		var num = 0;
		for(i=0; i < $_("ipv6").options.length; i++) {
			if($_("ipv6").options[i].selected){
				$_("ipv6").options[i].selected = true;
				num = num + 1;
			}
		}
		$_("ipv6count").value = num;
	}catch(e){	}
};

function loadplan(planid){
	if(planid == 0){
		return false;
	}
	
	if(AJAX("'.$globals['index'].'act=managevps&vpsid='.$vps['vpsid'].'&ajax=true&plan="+planid+"&plid="+planid, "plandetails(re)")){
		return false;
	}else{
		return true;	
	}
};

function plandetails(re){
	
	// Initially dont apply plan changes
	f_apply_plan = 0;

	// Change resources only as per plan
	eval(re);
	$.each($plan, function( index, value ){
		if(index == "plan_ips" && (typeof value == "object")){
			$("#iplist").html("");
			ips = [];
			$.each(value, function( ind, val ){
				let row = "<option value="+ind+">"+val.ip+"</option>";
				$("#iplist").append(row);
				ips[ind] = val.ip;
			});			
		}else if(index == "internal_ips" && (typeof value == "object")){
			$("#ips_int").html("");
			ips_int = [];
			$.each(value, function( ind, val ){
				let row = "<option value="+val.ip+">"+val.ip+"</option>";
				$("#ips_int").append(row);
			});
		}else if(index == "ipsv6" && (typeof value == "object")){
			$("#ipv6").html("");
			ipv6 = [];
			$.each(value, function( ind, val ){
				let row = "<option value="+val.ip+">"+val.ip+"</option>";
				$("#ipv6").append(row);
			});
		}else if(index == "ipsv6_subnet" && (typeof value == "object")){
			$("#ipv6_subnet").html("");
			ipv6_subnet = [];
			$.each(value, function( ind, val ){
				let row = "<option value="+val.ip+">"+val.ip+"</option>";
				$("#ipv6_subnet").append(row);
			});
		}
	});
	
	if(!confirm("'.$l['conf_apply_plan'].'")){
		// No changes to apply, just change plid
		return false;
	}
	
	// We need to apply plan changes too
	f_apply_plan = 1;
	var topology_enabled = 0;
	try{
		if($plan["virtio"] == 1){
			if(!$_("virtio").checked){
				virtio_warning();
			}
		}else{
			if($_("virtio").checked){
				virtio_warning();
			}
		}
	}catch(e){
		// Do not do anything !
	}
	
	$.each($plan, function( index, value ){
		
		if(index == "io") index = "priority";
		if(index == "swap") index = "swapram";
		
		if(index == "mgs"){
			$("#mg option:selected").removeAttr("selected");
			for(var i=0; i<$_("mg").options.length; i++){
				$_("mg").options[i].style.display = "";
				$($_("mg").options[i]).prop("selected", true);
				if($_("mg").options[i].value != value[i]){
					$_("mg").options[i].style.display = "none";
				}
			}
		}else if(index == "dns_nameserver" && (typeof value == "object")){
			$("#dnstable").html("");
			$.each(value, function( ind, val ){
				adddnsrow(\'dnstable\', val);
			});
		}else if(index == "plan_ips" && (typeof value == "object")){
			$("#iplist").html("");
			ips = [];
			$.each(value, function( ind, val ){
				let row = "<option value="+ind+">"+val.ip+"</option>";
				$("#iplist").append(row);
				ips[ind] = val.ip;
			});			
		}else if(index == "internal_ips" && (typeof value == "object")){
			$("#ips_int").html("");
			ips_int = [];
			$.each(value, function( ind, val ){
				let row = "<option value="+val.ip+">"+val.ip+"</option>";
				$("#ips_int").append(row);
			});
		}else if(index == "ipsv6" && (typeof value == "object")){
			$("#ipv6").html("");
			ipv6 = [];
			$.each(value, function( ind, val ){
				let row = "<option value="+val.ip+">"+val.ip+"</option>";
				$("#ipv6").append(row);
			});
		}else if(index == "ipsv6_subnet" && (typeof value == "object")){
			$("#ipv6_subnet").html("");
			ipv6_subnet = [];
			$.each(value, function( ind, val ){
				let row = "<option value="+val.ip+">"+val.ip+"</option>";
				$("#ipv6_subnet").append(row);
			});
		}else{	
			$("#editvs").field( index, value);
			
			if((index == "topology_sockets" || index == "topology_cores" || index == "cpu_threads") && (!empty(value)) && (empty(topology_enabled))){
				$("#enable_cpu_topology").prop("checked", true);
				topology_enabled = 1;
			}
		}
	});
	
	$_("hdd_0").value = $plan["space"];

	$("#ipv6count").val(parseInt($plan["ips6"]));
	ipchoose("ipv6", $plan["ips6"]);
	ipchoose("ips_int", $plan["ips_int"]);
	ipchoose("ipv6_subnet", $plan["ips6_subnet"]);
	
	if(document.getElementsByName("ips[]").length < $plan["ips"]){
		var diff = $plan["ips"] - document.getElementsByName("ips[]").length;
		planips(diff);
	}
	
	checkvnc();
	cputopology();
	handle_capping();
};

function changepriority(priority){
	if(!$_("prior")){
		return false;
	}
	$_("prior").value = priority;
};

function pincheck(){
	if(!$_("allcores")){
		return false;
	}
	if(is_checked("allcores")){';
		for($i=0; $i < $resources['cpucores']; $i++){
			echo '$_("pin'.$i.'").checked = 0;';
		}
		echo '$("#pincores").hide();
	}else{
		$("#pincores").show();
	}
};

function toggle_option(option){
	if ($("#"+option).is(":hidden")){
		$("#"+option).slideDown("slow");
		$("#"+option+"_toggle_indicator").text(" - ");
	}else{
		$("#"+option).slideUp("slow");
		$("#"+option+"_toggle_indicator").text(" + ");
	}
}

function setpwd(size){
	var pwd = randstr(size, 1, '.(!empty($globals['pass_strength']) ? $globals['pass_strength'] : 0).');
	$("#newpass").val(pwd);
	$("#conf").val(pwd);
}

// Match the passwords
function pass_match(){
	var newpass = $("#newpass").val();
	var conf = $("#conf").val();
	
	if(newpass != conf){
		$("#message").text("'.$l['pass_match'].'");
		$("#message").css("color", "red");
	}else{
		$("#message").text("");
	}
}

function plus_onmouseover(){
	$("#plus").attr("src", "'.$theme['images'].'admin/plus_hover.gif");
}

function plus_onmouseout(){
	$("#plus").attr("src", "'.$theme['images'].'admin/plus.gif");
}

function checkvnc(){
	
	if(!$("#vnc")){
		return false;
	}
	
	if(is_checked("vnc")){
		$("#vncpassrow").show();
		$("#launchvnc").show();
		$("#vnckeymap").show();
		
	}else{
		$("#vncpassrow").hide();
		$("#launchvnc").hide();
		$("#vnckeymap").hide();
	}
};

function ispvonhvm(){
	
	var pv_on_hvm = false;
	if(!$("#pv_on_hvm")){
		pv_on_hvm = false;
	}else{
		if(is_checked("pv_on_hvm")){
			pv_on_hvm =  true;
		}
	}
	
	if($("#tr_viftype")){
		if(pv_on_hvm){
			$("#tr_viftype").hide();
		}else{
			$("#tr_viftype").show();
		}
	}
	return true;
};

var lang = Array();
lang["bad"] = "'.$l['bad'].'";
lang["good"] = "'.$l['good'].'";
lang["strong"] = "'.$l['strong'].'";
lang["short"] = "'.$l['short'].'";
lang["strength_indicator"] = "'.$l['strength_indicator'].'";

function virtio_warning(){
	
	var isenabled = $_("virtio").checked;
	
	var r = confirm("'.$l['virtio_warning'].'");
	
	if(!r){
		
		// preserve the state if user click on cancel
		// we do this invertly as isenabled get the state as after change bcoz event is called on onchange.
		if(isenabled){
			$_("virtio").checked = 0;
		}else{
			$_("virtio").checked = 1;
		}
	}
	
};

function custom_valert(output_msg, title_msg)
{
	if (!title_msg)
		title_msg = "Success!";

	if (!output_msg)
		output_msg = "No Message to Display.";

	$("<div></div>").html(output_msg).dialog({
		title: title_msg,
		resizable: true,
		buttons: {
            "Close": function() 
            {
                $(this).dialog( "close" );
            }
        },
		modal: true,
		width: 350,
		maxHeight: 250
	});
}


function edit_action(action){
	var id = '.$vps['vpsid'].'; 
	
	$("#"+action).attr("src","'.$theme['images'].'loading_50.gif");
	
	if(action == "start" || action == "stop" || action == "restart" || action == "poweroff" || action == "ssh" || action == "suspend" || action == "unsuspend" || action == "suspend_net" || action == "unsuspend_net"){
	
		$.ajax({type: "POST",
			url: "'.$globals['index'].'act=vs&action="+action+"&vpsid="+id+"&api=json&random="+Math.random(),
			success:function(response){
				response_data = JSON.parse(response);
				
				if(response_data.error_msg){
					custom_valert(response_data.error_msg, "'.$l['error'].'");
				}else if(response_data.done_msg){
					changestatus(action);
					custom_valert(response_data.done_msg);
				}else{
					custom_valert("'.$l['alert_failure'].'", "'.$l['error'].'");
				}
				$("#"+action).attr("src","'.$theme['images'].'"+action+".png");
			}
		});
	}
};

function bwresert(){
	
	var id = '.$vps['vpsid'].';
	
	var r = confirm("'.$l['conf_bwreset'].'");
	
	if(r != true){
		return false;
	}
	
	$("#bwresert_img").attr("src","'.$theme['images'].'loading_50.gif");
	
	$.ajax({type: "POST",
		url: "'.$globals['index'].'act=managevps&bwreset="+id+"&api=json&vpsid="+id,
		success:function(response){
			response_data = JSON.parse(response);
			
			if(response_data.error_msg){
				custom_valert(response_data.error_msg, "'.$l['error'].'");
			}else if(response_data.done_msg){
				custom_valert(response_data.done_msg);
			}else{
				custom_valert("'.$l['alert_failure'].'", "'.$l['error'].'");
			}
			$("#bwresert_img").attr("src","'.$theme['images'].'bandwidth.png");
		}
	});	
}

function launchvnc(type){
	if(type === 1){
		var thisURL = window.location.href;
		thisURL = thisURL.toString();
		thisURL = thisURL.replace("http:", "https:");
		thisURL = thisURL.replace(":4084", ":4085");
		window.open("'.$globals['index'].'act=vnc&novnc='.$vps['vpsid'].'", "_blank", "height=400,width=720");
	}else if(type === 2){
		AJAX("'.$globals['index'].'act=vnc&ajax='.$vps['vpsid'].'", "vnchandle(re)");
	}
};

function netspeed(r){
	$_("network_speed").value = r;
	handle_capping();
}
function upspeed(r){
	$_("upload_speed").value = r;
	handle_capping();
}

function vnchandle(resp){
	$("#vnc_launcher").html(resp);
}

function update_progress(id, per){
	
	$("#"+id).css("width", per+"%");
	if(per < 40){
		$("#"+id).addClass("progress-bar-success");
	}else if(per < 60){
		$("#"+id).addClass("progress-bar-info");
	}else if(per < 80){
		$("#"+id).addClass("progress-bar-warning");
	}else{
		$("#"+id).addClass("progress-bar-danger");
	}
	
	$("#"+id).html(per+"%");
}

function rescue_ops(){
	var vpsid = '.$vps['vpsid'].';
	
	var rescue = '.$vps['rescue'].';
	
	// Disable rescue
	if(rescue == 1){
		
		var content = $("#rescue_content").html();
		var loading = \'<center><br /><br /><br /><img height="70px" width="70px" src="'.$theme['images'].'loading_50.gif" /><br /><br /><br /><label class="val">'.$l['disabling_rescue'].'</label></center>\';
		
		$("#rescue_content").empty();
		$("#rescue_content").append(loading);
		$("#rescue_operation").attr("disabled","disabled");	
		
		$.ajax({type: "POST",
				url: "'.$globals['index'].'act=managevps&disable_rescue="+vpsid+"&vpsid="+vpsid+"&api=json",
				dataType : "json",
				success:function(response){
					
					if("done" in response){
						actid = response["actid"];
						progress_update = "'.$l['disabling_rescue'].'";
						
						$("#rescue_content").html("<div id=\"progress_onload\"></div><br /><br />");
						progress_onload();
						
						$("#progress-cont").show();
						$("#progressbar").progressbar({value: 0});
						
						setTimeout("get_progress(\'disable_rescuevs\')", 500);
						
						return;
					}
					
					// Are there any errors ?
					if(typeof(response["error"]) != "undefined"){
						var errors = "";
						for(x in response["error"]){
							errors = errors + response["error"][x];
						}
						alert(errors);
					}
					
					$("#rescue_content").empty();
					$("#rescue_content").append(content);
					$("#rescue_operation").removeAttr("disabled");
					
				}
		});
	
		
	// Enable rescue	
	}else{
		
		var loading = \'<center><br /><br /><br /><img height="70px" width="70px" src="'.$theme['images'].'loading_50.gif" /><br /><br /><br /><label class="val">'.$l['enabling_rescue'].'</label></center>\';
		
		var content = $("#rescue_content").html();
		
		var rescuedata = new Object();
		
		rescuedata["rescue_pass"] = $("#rescue_pass").val();
		rescuedata["conf_rescue_pass"] = $("#conf_rescue_pass").val();
		
		$("#rescue_content").empty();
		$("#rescue_content").append(loading);
		$("#rescue_operation").attr("disabled","disabled");
		
		$.ajax({type: "POST",
				url: "'.$globals['index'].'act=managevps&enable_rescue="+vpsid+"&vpsid="+vpsid+"&api=json",
				dataType : "json",
				data : rescuedata,
				success:function(response){
					
					if("done" in response){
						actid = response["actid"];
						progress_update = "'.$l['enabling_rescue'].'";
						
						$("#rescue_content").html("<div id=\"progress_onload\"></div><br /><br />");
						progress_onload();
						
						$("#progress-cont").show();
						$("#progressbar").progressbar({value: 0});
						
						setTimeout("get_progress(\'enable_rescuevs\')", 500);
						
						return;
					}
					
					// Are there any errors ?
					if(typeof(response["error"]) != "undefined"){
						var errors = "";
						for(x in response["error"]){
							errors = errors + response["error"][x];
						}
						alert(errors);
					}
					
					$("#rescue_content").empty();
					$("#rescue_content").append(content);
					$("#rescue_operation").removeAttr("disabled");
				}
		});
		
	}
	
}

function rescuedonefunc(act, error){
	
	error = error || 0;
	
	if(act == "enable_rescuevs"){
		
		if(empty(error)){
			alert("'.$l['success_enable_rescue'].'");
		}else{
			alert("'.$l['err_enable_rescue'].'");
		}
		
	}else if(act == "disable_rescuevs"){
		
		if(empty(error)){
			alert("'.$l['success_disable_rescue'].'");
		}else{
			alert("'.$l['err_disable_rescue'].'");
		}
	}
	
	location.reload();
}

function show_loading(id) {
	$("#" + id).html("<div style=\"text-align:center; padding:96px 0\"><img src=\"'.$theme['images'].'progress_bar.gif\" /></div>");
}

function data_table(selector) {
	$(selector).dataTable({
		"language": {
			"lengthMenu": "'.$l['show'].' _MENU_ '._ucfirst($l['entries']).'",
			"search": "'.$l['search'].':",
			"info": "'.$l['showing'].' _START_ '.$l['to'].' _END_ '.$l['of'].' _TOTAL_ '.$l['entries'].'",
			"infoEmpty": "'.$l['showing'].' 0 '.$l['to'].' 0 '.$l['of'].' 0 '.$l['entries'].'",
			"zeroRecords": "'.$l['no_data_avail'].'",
			"emptyTable": "'.$l['no_data_avail'].'",
			"infoFiltered": "('.$l['filtered_from'].' _MAX_ '.$l['total_entries'].')",
			"paginate": {
				"first":      "'.$l['first'].'",
				"last":       "'.$l['last'].'",
				"next":       "'.$l['next'].'",
				"previous":   "'.$l['previous'].'"
			}
		}
	});
}

function show_subnets() {
	show_loading("manage_subnets_body");
	
	$.ajax({
		type : "GET",
		url : "'.$globals['index'].'act=managevps&managesubnets=1&api=json&vpsid='.$vps['vpsid'].'",
		dataType : "json",
		success: function(data){
			$("#manage_subnets_body").html("");
			
			if("error" in data){
				// TODO display error
				return;
			}
			
			var subnet_table = "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" class=\"table table-hover tablesorter data-table\" width=\"100%\"><thead><tr><th width=\"70%\">'.$l['ips6_subnet'].'</th><th>'.$l['edit'].'</th></tr></thead>";

			// Prepare the list
			for(x in data["ips2"]){

				$v = data["ips2"][x];

				subnet_table += "<tr><td align=\"center\">" + $v["ip"] + "/" + $v["ipr_netmask"] + "</td><td align=\"center\" id=\"data-subnet\" data-subnet=\"" + $v["ip"] + "/" + $v["ipr_netmask"] + "\" class=\"manage_subnet\"><img width=\"16\" height=\"16\" src=\"'.$theme['images'].'edit.png\" onclick=\"get_subnet_ips(this);\" style=\"cursor:pointer;\"/></td></tr>";

			}

			subnet_table += "</table>";

			$("#manage_subnets_body").html(subnet_table);
			
			data_table(".data-table");
		}
	});
}

function get_subnet_ips(el) {
	var subnet = $(el).closest("td").data("subnet");
	
	show_loading("manage_subnets_body");
	
	$.ajax({
		type : "POST",
		url : "'.$globals['index'].'act=managevps&managesubnets=1&api=json&vpsid='.$vps['vpsid'].'",
		data : "subnet="+subnet,
		dataType : "json",
		success: function(data){
			show_subnet_ips(data);
		}
	});
}

function show_subnet_ips(data) {
	if("error" in data) {
		// TODO show error;
		return;
	}
	
	$("#manage_subnets_body").html("");
	
	if(data["ipr_ips"]) {
		var html = "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" class=\"table table-hover tablesorter data-table\" width=\"100%\"><thead><tr><th width=\"70%\">'.$l['ips6_subnet'].'</th><th>'.$l['delete'].'</th></tr></thead>";
		
		var inputs = "";
		
		if($.isArray(data["ipr_ips"]["ipv6"])) {
			var ipv6 = data["ipr_ips"]["ipv6"];

			// Prepare the list
			for(x in ipv6){
				
				$v = ipv6[x];

				html += "<tr><td align=\"center\">" + $v + "</td><td align=\"center\" id=\"data-subnet\" data-ip=\"" + $v + "\" class=\"manage_subnet\"><img width=\"16\" height=\"16\" src=\"'.$theme['images'].'delete.png\" onclick=\"delete_ip(\'" + $v + "\');\" style=\"cursor:pointer;\"/></td></tr>";
				
				inputs += "<input type=\"hidden\" name=\"new_ipv6[]\" value=\"" + $v + "\" />";
			}
		}

		html += "</table><form id=\"manage_subnets_form\">" + inputs + "<input type=\"hidden\" name=\"new_ipv6[]\" value=\"\" id=\"input_new_ipv6\" disabled/><input type=\"hidden\" name=\"ipv6_addr\" value=\"" + data["ipr_ips"]["ipv6_addr"] + "\" /><input type=\"hidden\" name=\"ipv6_subnet_mask\" value=\"" + data["ipr_ips"]["ipv6_subnet_mask"] + "\" /></form>";
	
		// Show add ip form
		var ipv6_parts_arr = data["ipr_ips"]["ipv6_addr"].split(":");
		var subnet_length = 8 - data["ipr_ips"]["ipv6_subnet_mask_value"];

		var ipv6_input_boxes = new Array();
		
		for(var i = 0; i < subnet_length; i++){
			ipv6_input_boxes[i] = "<input type=\"text\" class=\"form-control input-sm ipv6_parts\" name=\"ipv6_parts[]\" value=\"" + ipv6_parts_arr[i] + "\" disabled=\"disabled\" size=\"4\" maxlength=\"4\" style=\"display:inline-block; width:auto;\"></input>";
		}
		
		for(i = i; i < 8; i++){
			ipv6_input_boxes[i] = "<input type=\"text\" class=\"form-control input-sm ipv6_parts\" name=\"ipv6_parts[]\" value=\"\" size=\"4\" maxlength=\"4\" style=\"display:inline-block; width:auto;\"></input>";
		}
		
		html += "<br /><table><tr><td width=\"150\"><label>'.$l['add_an_ipv6_addr'].'</label></td><td>";
		
		html += ipv6_input_boxes.join("&nbsp;:&nbsp;");
		
		html += "</td></tr></table><br /><center><button class=\"btn\" onclick=\"add_ipv6(); return false;\">'.$l['manage_subnets_add_ip'].'</button>&nbsp;&nbsp;<button class=\"btn\" onclick=\"show_subnets(); return false;\">'.$l['back'].'</button></center>";

		$("#manage_subnets_body").html(html);
		
		data_table(".data-table");
	}
}

function add_ipv6() {
	var parts = new Array();
	
	$(".ipv6_parts").each(function() {
		parts.push($(this).val());
	});
	
	$("#input_new_ipv6").prop("disabled", false);
	$("#input_new_ipv6").val(parts.join(":"));
	
	$.ajax({
		type : "POST",
		url : "'.$globals['index'].'act=managevps&managesubnets=1&api=json&vpsid='.$vps['vpsid'].'",
		data : $("#manage_subnets_form").serialize(),
		dataType : "json",
		success: function(data){
			show_subnet_ips(data);
		}
	});
}

function delete_ip(ip) {
	$("#manage_subnets_form input[value=\'" + ip + "\']").remove();
	
	$.ajax({
		type : "POST",
		url : "'.$globals['index'].'act=managevps&managesubnets=1&api=json&vpsid='.$vps['vpsid'].'",
		data : $("#manage_subnets_form").serialize(),
		dataType : "json",
		success: function(data){
			show_subnet_ips(data);
		}
	});
}

//-----------------------------------MANAGE VDF------------------------------

vdf_vpsuuid = "'.$vps['uuid'].'";
vdf_serid = '.$vps['serid'].';
var is_editvs = 1;
vdf_edit_ico = "fa icon-edit";
vdf_save_ico = "fa icon-save";
vdf_delete_ico = "fa icon-delete";
vdf_revert_ico = "fa icon-undo";

function show_managevdf_window(){
	//show_loading("managevdf_body");
	vdf_url = "'.$globals['index'].'act=managevps&managevdf=1&api=json&vpsid='.$vps['vpsid'].'";
	processing_symb(1);
	$.ajax({
		type : "GET",
		url : vdf_url,
		method : "post",
		dataType : "json",
		success: function(data){
			processing_symb(0);
			$arr_haproxy_src_ips = data["arr_haproxy_src_ips"];
			$supported_protocols = data["supported_protocols"];
			$haproxydata = data["haproxydata"];
			$vpses = data["vpses"];
			$arr_haproxy_src_ips = data["arr_haproxy_src_ips"];
			$server_haconfigs = data["server_haconfigs"];
			
			//List current VDF entries
			listvpsforwardertbl();
			
			// Hide add VDF form if it was visible previously and show add vdf form button
			$("#addvdf_form_div").hide();
			$("#showaddvdfformbtn").show();
			// Adjust button margin
			$("#showaddvdfformbtn button").css("margin-right", "10px");
			
		}
	});
}

//---------------------------MANAGE VDF ENDS------------------------------
function copy_password(){
	
	var conf = prompt("'.$l['copy_pass'].'", $("#newpass").val());
	if (conf == null) {
		return false;
	}
};

$(document).ready(function(){
	
	var band_per = '.(!empty($vps['band_per']) ? $vps['band_per'] : 0).';
	update_progress("bandwidth_bar", band_per);
	
	var disk_per = '.(!empty($vps['disk_per']) ? $vps['disk_per'] : 0).';
	update_progress("disk_bar", disk_per);
	
	var cpu_per = '.(!empty($vps['stats']['used_cpu']) ? $vps['stats']['used_cpu'] : 0).';
	update_progress("cpu_bar", cpu_per);
	
	var ram_per = '.(!empty($vps['stats']['used_ram_per']) ? $vps['stats']['used_ram_per'] : 0).';
	update_progress("ram_bar", ram_per);
	
	if($("#iplist option").length === 0){
		$("#iplist_parent").hide();
	}
	
	$("#manage_subnets").on("show.bs.modal", function (event) {
		show_subnets();
	});
	$("#managevdf").on("show.bs.modal", function (event) {
		show_managevdf_window();
	});	
	
	$("#form-container").submit(function(){
	
		$("#rebuild_error_row").hide();
		
		if(!confirm("'.$l['rebuild_data_lost_warn'].'")){
			return false;		
		}
		
		progress = 0;
		var params = "";
		newvs_rootpass = $("#newpass").val();
	
		conf = $("#conf").val();
		
		if(newvs_rootpass != conf){
			alert("'.$l['pass_match'].'");
			return false;
		}
		
		var vpsid = '.$vps['vpsid'].';
		
		var serid = '.$vps['serid'].';
		
		var osid = $("#osid option:selected").val();		
		
		eu_send_rebuild_email = 0;
		
		if(document.getElementById("eu_send_rebuild_email").checked){
			eu_send_rebuild_email = 1;
		}
		
		params = "osid="+osid+"&newpass="+encodeURIComponent(newvs_rootpass)+"&conf="+encodeURIComponent(conf)+"&reos=1&vpsid="+vpsid+"&eu_send_rebuild_email="+eu_send_rebuild_email;
		
		//var content = $("#form-container").html();
		var loading = \'<center><br /><br /><br /><img height="70px" width="70px" src="'.$theme['images'].'loading_50.gif" /><br /><br /><br /><label class="val">'.$l['checking_data'].'</label></center>\';
		
		$("#form-container").hide();
		$("#rebuild_content").html(loading);
		//$("#reos").attr("disabled","disabled");
	
		$.ajax({type: "POST",
			url: "'.$globals['index'].'jsnohf=1&act=rebuild&changeserid="+serid,
			data:params,
			success:function(response){
				if(!isNaN(response)){	
					actid = response;					
					$("#rebuild_content").html("<div id=\"progress_onload\"></div>");
					progress_onload();
					$("#pbar").html("'.$l['checking_data'].'" + " ( 0% ) ");
					$("#progress-cont").show();
					$("#progress-cont").append("<br /><br />");
					$("#progressbar").progressbar({value: 0});
					
					setTimeout("get_progress(\'rebuild\')", 500);
					
					return;
				}else{
					
					$("#rebuild_content").empty();			
					$("#form-container").show();
					var error_box_html =  $($.parseHTML(response)).find("#error_box");					
					$("#form-container").prepend(error_box_html);
				}
			}
		});	
		
		return false;
	});

});

var lang_no_limit = "'.$l['no_limit'].'";

addonload("handle_capping(); fillspeedmbits(); planips(0); checkippool(); adduser(); changepriority(\''.POSTval('priority', $vps['io']).'\'); checkvnc(); countipv6(); pincheck(); ispvonhvm();check_pass_strength(\'rootpass\');enable_accel();cputopology();changestatus();check_pass_strength();");

</script>
<!--<div class="alert alert-warning" style="text-align:center;font-size:14px;">'.$l['managevps_beta_warn'].'</div>-->
<div id="vnc_launcher" style="position:absolute;width:0px;left:-100%"></div>
<div class="modal" id="vncModal">
    <div class="modal-dialog">
		<div class="modal-content" >
			<div class="modal-header">
			  <button type="button" class="close" data-dismiss="modal">&times;</button>
			  <img width="20" src="'.$theme['images'].'vnc.png" />&nbsp;<span class="modal-title"><label class="control-label">VNC</label></span>
			</div>
			<div class="modal-body" >
				<div class="row">
					<div class="col-sm-12">
						<center><label class="h3">'.$l['vnc_info'].'</label></center>
					</div>
				</div><br /><br />
				<div class="row">
					<div class="col-sm-6">
						<label class="h4">'.$l['vnc_ip'].'&nbsp;:</label>&nbsp;&nbsp;<span class="val">'.$vps['vnc_ip'].'</span>
					</div>
					<div class="col-sm-6">
						<label class="h4">'.$l['vnc_port'].'&nbsp;:</label>&nbsp;&nbsp;<span class="val">'.$vps['vncport'].'</span>
					</div>
				</div><br /><br />
				<div class="row">
					<div class="col-sm-4">
						<label class="h4">'.$l['launch_vnc'].'</label>
					</div>
					<div class="col-sm-4">
					  <center><a href="javascript:void(0)" id="java_vnc" class="vncButton" onclick="launchvnc(2)"/><img src="'.$theme['images'].'vnc.png" title="'.$l['vpmenu_javavnc'].'"/><br />'.$l['title_javavnc'].'</a></center>
					</div>
					<div class="col-sm-4">'
						.(!empty($globals['novnc']) ? '<center><a href="javascript:void(0)" id="novncURL" class="vncButton" onclick="launchvnc(1)"><img src="'.$theme['images'].'vnc.png" title="'.(!empty($l['vpmenu_novnc'])?$l['vpmenu_novnc']:'').'"/><br />'.$l['title_novnc'].'</a></center>' : '').'
					</div>
				</div><br />
			</div>
		</div>
    </div>
</div>
<div class="modal" id="rescuemodal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				 <button type="button" class="close" data-dismiss="modal">&times;</button>
				<img width="20" src="'.$theme['images'].'rescue.png" />&nbsp;<span class="modal-title"><label class="control-label">'.$l['rescue_mode'].'</label></span>
			</div>
			<div class="modal-body" id="rescue_content">
				<span class="help-block">'.$l['rescue_exp'].'</span><br />
				<div class="row">
					<center><label class="control-label">'.(empty($vps['rescue']) ? $l['rescue_disabled'] : $l['rescue_enabled']).'</label></center>
				</div><br /><br /><br />
				'.(empty($vps['rescue']) ? '<div class="row">
						<div class="col-sm-4">
							<label class="control-label">'.$l['rootpass'].'</label><br />
						</div>
						<div class="col-sm-6">
							<input type="password" class="form-control" name="rescue_pass" id="rescue_pass" size="30" value="'.POSTval('rescue_pass', '').'" />
						</div>
						<div class="col-sm-2"></div>
				</div><br />
				<div class="row">
					<div class="col-sm-4">
						<label class="control-label">'.$l['conf_pass'].'</label><br />
					</div>
					<div class="col-sm-6">
						<input type="password" class="form-control" name="conf_rescue_pass" id="conf_rescue_pass" size="30" value="'.POSTval('conf_rescue_pass', '').'" />
					</div>
					<div class="col-sm-2"></div>
				</div>' : '' ).'
			</div>
			<div class="modal-footer">
				<center><a href="javascript:void(0)" class="btn" target="" name="rescue_operation" id="rescue_operation" onclick="rescue_ops();">'.(empty($vps['rescue']) ? $l['enable_rescue'] : $l['disable_rescue']).'</a></center>
			</div>
		</div>
	</div>
</div>

<div class="modal" id="webuzomodal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				 <button type="button" class="close" data-dismiss="modal">&times;</button>
				<img width="20" src="'.$theme['images'].'webuzo_50.gif" />&nbsp;<span class="modal-title"><label class="control-label">'.$l['webuzo_mode'].'</label></span>
			</div>
			<div class="modal-body" id="webuzo_content">
				<span class="help-block">'.$l['webuzo_exp'].'</span><br />';
				if(empty($vps['locked'])){
				echo '<form accept-charset="'.$globals['charset'].'" action="" method="post" id="install_script" name="install_script" class="form-horizontal">
				<div class="row">					
				</div>			
				<div class="row">
					<div class="col-sm-4">
						<label class="control-label">'.$l['webuzo_spasswd'].'</label><br />
					</div>
					<div class="col-sm-6">							
						<input class="form-control" type="text" name="webuzo_spasswd" id="webuzo_spasswd" onkeyup="check_pass_strength(\'webuzo_spasswd\');"  placeholder="'.$l['webuzo_spasswd_p'].'" 
						value="'.POSTval('webuzo_spasswd', '').'">
						<div id="webuzo_spasswd_pass-strength-result" class="">'.$l['strength_indicator'].'</div>							
					</div>
					<div class="col-sm-2">
						<a href="javascript: void(0);" onclick="$_(\'webuzo_spasswd\').value=randstr(12, 1, '.(!empty($globals['pass_strength']) ? $globals['pass_strength'] : 0).');check_pass_strength(\'webuzo_spasswd\');return false;" title="'.$l['randpass'].'"><img src="'.$theme['images'].'randpass.gif" /></a>
					</div>
				</div><br />
				<div class="row">
					<div class="col-sm-4">
						<label class="control-label">'.$l['webuzo_pd'].'</label><br />
					</div>
					<div class="col-sm-6">							
						<input class="form-control" type="textbox" name="webuzo_pd" id="webuzo_pd" placeholder="'.$l['webuzo_pd_p'].'" 
						value="'.POSTval('webuzo_pd', '').'">			
					</div>
					<div class="col-sm-2"></div>
				</div><br />
				<div class="row">
					<div class="col-sm-4">
						<label class="control-label">'.$l['webuzo_scriptlist'].'</label><br />
					</div>
					<div class="col-sm-6">						
						<select id="webuzo_scriptlist" class="chosen" onchange="script_req(this.value);" name="webuzo_script">
						</select>
					</div>
					<div class="col-sm-2"></div>
				</div><br />
				<div class="row">
					<div class="col-sm-4">
						<label class="control-label">'.$l['webuzo_os'].'</label><br />
					</div>
					<div class="col-sm-8">';
					
						foreach($webuzo_templates as $k => $v){
							echo '
							<div class="col-sm-12">
								<input for="input radio" type="radio" name="webuzo_os" id="webuzoos_'.$k.'" value="'.$k.'" />
								<label for="webuzoos_'.$k.'">'.$v['name'].'</label>
							</div>';
						}
				echo '</div>
				</div><br />
				<div class="row">
					<div class="col-sm-4">
						<label class="control-label">'.$l['webuzo_appstack'].'</label><br />
					</div>
					<div class="col-sm-8">
						<div class="col-sm-3" id="stack1_tr">
							<input type="radio" name="webuzo_stack" id="stack1" value="lamp" onclick="webuzo_apps(this.value);" />
							<label for="stack1">LAMP</label>
						</div>
						<div class="col-sm-3" id="stack2_tr" style="display:none;">
							<input type="radio" name="webuzo_stack" id="stack2" value="lemp" onclick="webuzo_apps(this.value);" />
							<label for="stack2">LEMP</label>
						</div>
						<div class="col-sm-3" id="stack3_tr" style="display:none;">
							<input type="radio" name="webuzo_stack" id="stack3" value="llmp" onclick="webuzo_apps(this.value);" />
							<label for="stack3">LLMP</label>
						</div>
					</div>
				</div><br />
				<div class="adv_border" id="webuzo_stack_tr" style="padding:1.2%;display:none;">
					<input type="hidden" id="script_isfree" value="0" />
					<div class="row" id="webuzo_webserver_tr">
						<div class="col-sm-4">
							<label class="control-label" style="margin:8px;">'.$l['webuzo_apache'].'</label><br />
						</div>
						<div class="col-sm-8" id="webuzo_webserver"></div>
					</div><br />
					<div class="row">
						<div class="col-sm-4">
							<label class="control-label" style="margin:8px;">'.$l['webuzo_mysql'].'</label><br />
						</div>						
						<div class="col-sm-8" id="webuzo_mysql"></div>				
					</div><br />
					<div class="row">
						<div class="col-sm-4">
							<label style="margin:8px;" class="control-label">'.$l['webuzo_php'].'</label><br />
						</div>	
						<div class="col-sm-8" id="webuzo_php"></div>
					</div><br />
				</div><br />
				<div class="row">
					<div class="roundheader" 
						onclick="toggle_option(\'webuzo_advance\');return false;" style="cursor:pointer;height:30px;">
						<label id="webuzo_advance_toggle_indicator" style="width:10px;">-</label>
						<label for="input text">'.$l['webuzo_advanced'].'</label>
					</div>';
					$vps['dns_nameserver'] = unserialize($vps['dns_nameserver']);					
					$ns1 = $ns2 = '';
					if(!empty($vps['dns_nameserver'])){
						$ns1 = (!empty($vps['dns_nameserver'][0]) ? $vps['dns_nameserver'][0] : '');
						$ns2 = (!empty($vps['dns_nameserver'][1]) ? $vps['dns_nameserver'][1] : '');
					}
					echo '<div id="webuzo_advance" class="bgaddv">
						<div class="row" id="webuzo_ns1_tr">
							<div class="col-sm-4">
								<label for="input text">'.$l['webuzo_ns1'].'</label>
							</div>
							<div class="col-sm-7">
								<input class="form-control" type="textbox" name="webuzo_ns1" id="webuzo_ns1" placeholder="'.$l['webuzo_ns1_p'].'"
								value="'.POSTval('webuzo_ns1', $ns1).'" />
							</div>
						</div><br />
						<div class="row" id="webuzo_ns2_tr">
							<div class="col-sm-4">
								<label for="input text">'.$l['webuzo_ns2'].'</label>
							</div>
							<div class="col-sm-7">
								<input class="form-control" type="textbox" name="webuzo_ns2" id="webuzo_ns2" placeholder="'.$l['webuzo_ns2_p'].'" 
								value="'.POSTval('webuzo_ns2', $ns2).'" />
							</div>
						</div><br />
						</div>
					</div>
					</form>';
				}else{
					echo $l['vm_locked_install_script'];
				}
				echo '</div>
			<div class="modal-footer">';
				echo (empty($vps['locked'])) ? '<center><a href="javascript:void(0)" class="btn" target="" name="webuzo_operation" id="webuzo_operation" onclick="install_webuzo();">'.$l['webuzo_submit'].'</a></center>' : '';
				
				echo '<div id="webuzo_unsupport" class="notice" style="display:none;">'.$l['webuzo_unsupport'].'</div>
			</div>
		</div>
	</div>
</div>

<div class="modal" id="rebuildmodal">
	<div class="modal-dialog">		
		<div class="modal-content">
			<div class="modal-header">
				 <button type="button" class="close" data-dismiss="modal">&times;</button>
				<img width="20" src="'.$theme['images'].'os.png" />&nbsp;<span class="modal-title"><label class="control-label">'.$l['rebuildvs'].'</label></span>
			</div>
			<div class="modal-body">
			<div id="rebuild_content"></div>
			<form id="form-container" accept-charset="'.$globals['charset'].'" action="" method="post" name="rebuild" class="form-horizontal">
				<div class="row" id="rebuild_error_row" style="display:none;">
					<div id="rebuild_error" class="col-sm-10 col-sm-offset-1"></div>
				</div>
				<div class="row">
					<div class="col-sm-4">
						<label class="control-label">'.$l['vsos'].'</label>
						<span class="help-block">'.$l['select_os'].'</span>
					</div>
					<div class="col-sm-6">
						<select class="form-control" id="osid" name="osid">
							<option value="0" '.(POSTval('osid')== 0 ?  'selected="selected"' :($vps['osid']== 0 ? 'selected="selected"' : '')).'>'.$l['select_os'].'</option>';
							
							foreach($oslist as $_virt => $vvvv){
							
								// Does this VPS support this $_virt ?
								if($vps['virt'] != $_virt){
									continue;
								}
							
								foreach($oslist[$_virt] as $kk => $vv){
								
									foreach($vv as $k => $v){
								
										echo '<option value="'.$k.'" '.(POSTval('osid') == $k ? 'selected="selected"' : ($vps['osid'] == $k ? 'selected="selected"' : '')).' '.(!empty($v['hvm']) ? 'hvm="1"' : '').' virt="'.$_virt.(!empty($v['hvm']) ? 'hvm' : '').'">'.(!empty($v['hvm']) ? 'HVM - ' : '').''.$v['name'].'</option>';
									
									}
									
								}
							}
					echo'</select>
					</div>
					<div class="col-sm-2"></div>
				</div>
				<div class="row">
					<div class="col-sm-4">
						<label class="control-label">'.$l['new_pass'].'</label>
					</div>
					<div class="col-sm-6">
						<input type="password" class="form-control" name="newpass" onkeyup="check_pass_strength(\'newpass\');" id="newpass" size="30" value="" />
						<div id="newpass_pass-strength-result" class="">'.$l['strength_indicator'].'</div>
						<span class="help-block"></span>
					</div>
					<div class="col-sm-2"><a href="javascript:void(0);" onclick="setpwd(12);check_pass_strength(\'newpass\');pass_match();copy_password();return false;" title="'.$l['randpass'].'"><img src="'.$theme['images'].'randpass.gif" /></a></div>
				</div>
				<div class="row">
					<div class="col-sm-4"><label class="control-label">'.$l['retype_pass'].'</label></div>
					<div class="col-sm-6"><input type="password" class="form-control" onblur="pass_match();" name="conf" id="conf" size="30" value="" /><div id="message"></div></div>
					<div class="col-sm-2"></div>
				</div><br />
				<div class="row">
					<div class="col-sm-4">
						<label class="control-label">'.$l['eu_send_rebuild_email'].'</label>
						<span class="help-block">'.$l['eu_send_rebuild_email_exp'].'</span>
					</div>
					<div class="col-sm-6">
						<input type="checkbox" class="ios" id="eu_send_rebuild_email" name="eu_send_rebuild_email" '.POSTchecked('eu_send_rebuild_email', 1).' value="1" />
					</div>
					<div class="col-sm-2"></div>
				</div>
				<br /><br />
				<div class="row">
					<center><input type="submit" class="btn" name="reos" id="reos" value="'.$l['reinstall'].'"/></center>	
				</div>	
			</form>
			</div>
		</div>
	</div>
</div>
<div class="modal" id="manage_subnets">
	<div class="modal-dialog" style="max-width:700px; width:100%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<img width="20" src="'.$theme['images'].'managesubnets.png" />&nbsp;<span class="modal-title"><label class="control-label">'.$l['manage_ipv6_subnets'].'</label></span>
			</div>
			<div class="modal-body" id="manage_subnets_body"></div>
		</div>
	</div>
</div>
<div class="modal" id="managevdf">
	<div class="modal-dialog" style="max-width:900px; width:100%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<img width="20" src="'.$theme['images'].'haproxy.png" />&nbsp;<span class="modal-title"><label class="control-label">'.$l['managevdf'].'</label></span>
			</div>
			<div class="modal-body" id="managevdf_body">
				<div class="row" id="showaddvdfformbtn">
					<div class="col-sm-12 text-right">
						<span style="margin-right: 10px;" >
							<span onclick="$(\'#vdf_infobox\').toggle();">
								<i class="icon-info" style="font-size: 25px; color: #555; padding: 0px; margin: 0px 5px; cursor: pointer;" ></i>
							</span>
							<a href="'.$globals['docs'].'Manage_VPS_Domain_Forwarding" target="_blank" class="wiki_help" title="'.$l['wiki_help'].'">
								<i class="icon-help" style="font-size: 25px;" ></i>
							</a>
						</span>
						<button class="btn text-center" onclick="showvdfform();">'.$l['new'].'</button>
					</div>
				</div>
				<div id="vdf_infobox">
					<div class="heading">'.$l['vdf_info'].'</div>
					<div class="body">
						<div>
							<b>'.$l['vdf_info_reservedports'].':</b> '.$server_config['haproxy_reservedports'].'<br/>
							<b>'.$l['vdf_info_reservedports_http'].':</b> '.$server_config['haproxy_reservedports_http'].'<br/>
							<b>'.$l['vdf_info_allowedports'].':</b> '.$server_config['haproxy_allowedports'].'<br/>
						</div><br/>
					</div>
				</div>
				<div class="row" id="addvdf_form_div" style="display: none;">
					<div class="row title">
						<div class="col-xs-12">'.$l['vdf_add_title'].'</div>
					</div>
					<form name="addvdf" id="addvdf" method="post" action="" onsubmit="return false;">
						<div class="row">
							<div class="col-sm-2 form-group">
								<div class="col-sm-12">
									<label class="control-label" for="protocol">'.$l['vdf_proto'].'</label>
								</div>
								<div class="col-sm-12">
									<select class="form-control chosen" id="protocol" name="protocol" onchange="processaddvdfform(this);" ></select>
								</div>
							</div>
							<div class="col-sm-4 form-group">
								<div class="col-sm-12">
									<label class="control-label" for="src_hostname" >'.$l['vdf_src_hname'].'</label>
								</div>
								<div class="col-sm-12">
									<select class="form-control chosen" id="src_hostname" name="src_hostname" ></select>
								</div>
							</div>
						
							<div class="col-sm-2 form-group">
								<div class="col-sm-12">
									<label class="control-label" for="src_port">'.$l['vdf_src_port'].'</label>
								</div>
								<div class="col-sm-12">
									<input type="text" class="form-control" id="src_port" name="src_port" />
								</div>
							</div>
							<div class="col-sm-2 form-group">
								<div class="col-sm-12">
									<label class="control-label" for="dest_ip">'.$l['vdf_dest_ip'].'</label>
								</div>
								<div class="col-sm-12">
									<select class="form-control chosen" id="dest_ip" name="dest_ip" ></select>
								</div>
							</div>
							<div class="col-sm-2 form-group">
								<div class="col-sm-12">
									<label class="control-label" for="dest_port">'.$l['vdf_dest_port'].'</label>
								</div>
								<div class="col-sm-12">
									<input type="text" class="form-control" id="dest_port" name="dest_port" />
								</div>
							</div>
						</div>
						<div class="row text-center">
							<button id="submitaddvdf" onclick="processaddvdfform(this);" class="btn text-center" name="submitaddvdf">'.$l['add'].'</button>
							<button class="btn text-center" onclick=\'$("#addvdf_form_div").css("display", "none");$("#showaddvdfformbtn").css("display","block");\' >'.$l['close'].'</button>
							<input type="hidden" value="addvdf" name="action" />
						</div>
					</form><hr />
				</div>
				<div class="row">
					<div class="scrollbar-virt">
						<div id="vdf_tbl_div"></div>
					</div>
					<div class="row bottom-menu">
						<div class="col-sm-6"></div>
						<div class="col-sm-6">
							<label>'.$l['with_selected'].'</label>
							<select name="haproxy_multiselect_action" id="haproxy_multiselect_action" class="form-control">
								<option value="0">---</option>
								<option value="1">'.$l['delete'].'</option>
							</select>&nbsp;<input type="submit" id="haproxy_task_submit" class="go_btn" name="haproxy_task_submit" value="'.$l['go'].'" onclick="vdf_confirm(); return false;" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="edivs-form-container">
<form  accept-charset="'.$globals['charset'].'" action="" method="post" id="editvs" name="editvs" class="form-horizontal">
<div class="row">
	<div class="col-sm-6 col-xs-12">
	
		<!---------------------
		// VPS INFO Window
		---------------------->
		
		<div class="roundheader" onclick="toggle_option(\'vps_info\');" style="cursor:pointer;height:30px;"><label id="vps_info_toggle_indicator" style="width:10px;">-</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp'.$l['vps_info'].'</div>
		<div id="vps_info" class="bgaddv">
			<div class="row">
				<div class="col-sm-5">
					<label class="control-label">'.$l['vs_server'].'</label>
					<span class="help-block"></span>
				</div>
				<div class="col-sm-7">
					<span class="val">'.$servers[$vps['serid']]['server_name'].'</span> ('.$l['vs_ser_id'].': <i>'.$vps['serid'].'</i>)
					<input type="hidden" name="serid" value="'.$vps['serid'].'" />
				</div>
			</div>
			<div class="row">
				<div class="col-sm-5">
					<label class="control-label">'.$l['id'].'</label>
					<span class="help-block"></span>
				</div>
				<div class="col-sm-7">
					<span class="val">'.$vps['vpsid'].'</span>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-5">
					<label class="control-label">'.$l['vid'].'</label>
					<span class="help-block"></span>
				</div>
				<div class="col-sm-7">
					<span class="val">'.$vps['vps_name'].'</span>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-5">
					<label class="control-label">'.$l['create_date'].'</label>
					<span class="help-block"></span>
				</div>
				<div class="col-sm-7">
					<span class="help-block">'.(empty($vps['time']) ? 'N/A' : datify($vps['time'])).'</span>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-5">
					<label class="control-label">'.$l['last_edited'].'</label>
					<span class="help-block"></span>
				</div>
				<div class="col-sm-7">
					<span class="help-block">'.(empty($vps['edittime']) ? 'N/A' : datify($vps['edittime'])).'</span>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-5">
					<label class="control-label">'.$l['vsos'].'</label>
					<span class="help-block"></span>
				</div>
				<div class="col-sm-7">
					<span class="val">'.$vps['os_name'].'</span>
				</div>
			</div>
			<div class="row" id="primary_ip">
				<div class="col-sm-5">
					<label class="control-label">'.$l['primary_ip'].'</label>
					<span class="help-block"></span>
				</div>
				<div class="col-sm-7">
					<span class="help-block h5">'.$primary_ip.'</span>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-5">
					<label class="control-label">'.$l['disk_space'].'</label>
					<span class="help-block"></span>
				</div>
				<div class="col-sm-7">
					<span class="val">'.$vps['space'].'&nbsp;GB</span>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-5">
					<label class="control-label">'.$l['ram'].'</label>
					<span class="help-block"></span>
				</div>
				<div class="col-sm-7">
					<span class="val">'.$vps['ram'].'&nbsp;MB</span>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-5">
					<label class="control-label">'.$l['num_ipv4'].'</label>
					<span class="help-block"></span>
				</div>
				<div class="col-sm-7">
					<span class="val">'.count($vps['ips']).'</span>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-5">
					<label class="control-label">'.$l['num_ipv6'].'</label>
					<span class="help-block"></span>
				</div>
				<div class="col-sm-7">
					<span class="val">'.count($vps['ips6']).'</span>
				</div>
			</div>
		</div><br />
		
		<!---------------------
		// VPS Manage Window
		---------------------->
		
		<div class="roundheader" onclick="toggle_option(\'manage\');" style="cursor:pointer;height:30px;"><label id="manage_toggle_indicator" style="width:10px;">-</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp'.$l['manage'].'
		</div>
		<div id="manage" class="bgaddv">
			<div class="row">
				<div class="col-sm-3 col-xs-4 pan-button">
					<center><a href="javascript: void(0);" target="" onclick="edit_action(\'start\')"><img id="start" src="'.$theme['images'].'start.png" width="48"/><br />'.$l['title_start'].'</a></center>
				</div>
				<div class="col-sm-3 col-xs-4 pan-button">
					<center><a href="javascript: void(0);" target="" onclick="edit_action(\'stop\')"><img id="stop" src="'.$theme['images'].'stop.png" width="48"/><br />'.$l['title_stop'].'</a></center>
				</div>
				<div class="col-sm-3 col-xs-4 pan-button">
					<center><a href="javascript: void(0);" target="" onclick="edit_action(\'restart\')"><img id="restart" src="'.$theme['images'].'restart.png" width="48" /><br />'.$l['title_restart'].'</a></center>
				</div>
				<div class="col-sm-3 col-xs-4 pan-button">
					<center><a href="javascript: void(0);" target="" onclick="edit_action(\'poweroff\')"><img id="poweroff" src="'.$theme['images'].'poweroff.png" width="48" /><br />'.$l['title_poweroff'].'</a></center>
				</div>	
				<div class="col-sm-3 col-xs-4 pan-button">
					<center><a href="javascript: void(0);" target="" onclick="edit_action(\'suspend\')"><img id="suspend" src="'.$theme['images'].'suspend.png" width="48" /><br />'.$l['title_suspend'].'</a></center>
				</div>
				<div class="col-sm-3 col-xs-4 pan-button">
					<center><a href="javascript: void(0);" target="" onclick="edit_action(\'unsuspend\')"><img id="unsuspend" src="'.$theme['images'].'unsuspend.png" width="48" /><br />'.$l['title_unsuspend'].'</a></center>
				</div>
				<div class="col-sm-3 col-xs-4 pan-button">
					<center><a href="javascript: void(0);" data-toggle="modal" data-target="#rebuildmodal"><img id="rescue" src="'.$theme['images'].'os.png" width="48" /><br />'.$l['rebuildvs'].'</a></center>
				</div>';
				
				if($kernel->features('rescue_support', $vps['virt'])){
					echo '<div class="col-sm-3 col-xs-4 pan-button">
						<center><a href="javascript: void(0);" data-toggle="modal" data-target="#rescuemodal"><img id="rescue" src="'.$theme['images'].'rescue.png" width="48" /><br />'.$l['rescue_mode'].'</a></center>
					</div>';
				}
				
				if(!empty($vps['vnc'])){
					echo '<div class="col-sm-3 col-xs-4 pan-button">
						<center><a href="javascript:void(0)" id="java" class="vncButton" data-toggle="modal" data-target="#vncModal"><img src="'.$theme['images'].'vnc.png" width="48" title="'.$l['vpmenu_vnc'].'"/><br />'.$l['title_vnc'].'</a></center>
					</div>';
				}
				
				echo '
				<div class="col-sm-3 col-xs-4 pan-button">
					<center><a href="javascript: void(0);" target="" onclick="edit_action(\'suspend_net\')"><img id="suspend_net" src="'.$theme['images'].'suspend_net.png" width="48" /><br />'.$l['title_suspend_net'].'</a></center>
				</div>
				<div class="col-sm-3 col-xs-4 pan-button">
					<center><a href="javascript: void(0);" target="" onclick="edit_action(\'unsuspend_net\')"><img id="unsuspend_net" src="'.$theme['images'].'unsuspend_net.png" width="48" /><br />'.$l['title_unsuspend_net'].'</a></center>
				</div>
				<div class="col-sm-3 col-xs-4 pan-button">
					<center><a href="javascript: void(0);" target="" onclick="bwresert();"><img id="bwresert_img" src="'.$theme['images'].'bandwidth.png" width="48" /><br />'.$l['title_bwreset'].'</a></center>
				</div>
				<div class="col-sm-3 col-xs-4 pan-button">
					<center><a href="javascript:void(0);" target="" onclick="navigateto(\'resource_utilization\');"><img src="'.$theme['images'].'monitoring.png" width="48" /><br />'.$l['resource_utilization'].'</a></center>
				</div>
				<div class="col-sm-3 col-xs-4 pan-button">
					<center><a href="javascript:void(0);" target="" onclick="navigateto(\'edit_vps\', \'ipv4_row\');"><img src="'.$theme['images'].'admin/ipv4.png" width="48" /><br />'.$l['ipaddresses'].'</a></center>
				</div>
				<div class="col-sm-3 col-xs-4 pan-button">
					<center><a href="javascript:void(0);" target="" onclick="navigateto(\'edit_vps\', \'hostname_row\');"><img src="'.$theme['images'].'hostname.png" width="48" /><br />'.$l['hostname'].'</a></center>
				</div>
				<div class="col-sm-3 col-xs-4 pan-button">
					<center><a href="javascript:void(0);" target="" onclick="navigateto(\'edit_vps\', \'rootpass_row\');"><img src="'.$theme['images'].'password.png" width="48" /><br />'.$l['change_password'].'</a></center>
				</div>
				<div class="col-sm-3 col-xs-4 pan-button">
					<center><a href="javascript:void(0);" target="" onclick="navigateto(\'edit_user\', \'change_user_row\');"><img src="'.$theme['images'].'admin/user.png" width="48" /><br />'.$l['change_user'].'</a></center>
				</div>';
				// IPv6
				if(!empty($ips6) || !empty($vps['ips6'])){
					echo '<div class="col-sm-3 col-xs-4 pan-button">
						<center><a href="javascript:void(0);" target="" onclick="navigateto(\'edit_vps\', \'ipv6_row\');"><img src="'.$theme['images'].'admin/ipv6.png" width="48" /><br />'.$l['ipaddresses_ip6'].'</a></center>
					</div>';
				}
				
				// IPv6 Subnet
				if(!empty($ips6_subnet) || !empty($vps['ips6_subnet'])){
					echo '<div class="col-sm-3 col-xs-4 pan-button">
						<center><a href="javascript:void(0);" target="" onclick="navigateto(\'edit_vps\', \'ips6_subnet_row\');"><img src="'.$theme['images'].'admin/ipv6subnet.png" width="48" /><br />'.$l['ipaddresses_ip6subnet'].'</a></center>
					</div>';
				}				
				echo '<div class="col-sm-3 col-xs-4 pan-button">				<center><a href="javascript:void(0);" target="" onclick="navigateto(\'vps_info\', \'primary_ip\');"><img src="'.$theme['images'].'admin/primary_ip.png" width="48"/><br />'.$l['primary_ip'].'</a></center></div>';

				// Internal IPs
				if(!empty($ips_int) || !empty($vps['ips_int'])){
					echo '<div class="col-sm-3 col-xs-4 pan-button">
						<center><a href="javascript:void(0);" target="" onclick="navigateto(\'advanced\', \'internal_ip_row\', \'network_tab\');"><img src="'.$theme['images'].'admin/internal.png" width="48" /><br />'.$l['ips_int'].'</a></center>
					</div>';
				}
				
				// Disk tuning
				if($vps['virt'] == 'kvm' || $vps['virt'] == 'proxk'){
					echo '<div class="col-sm-3 col-xs-4 pan-button">
						<center><a href="javascript:void(0);" target="" onclick="navigateto(\'advanced\', \'disk_tab\', \'disk_tab\');"><img src="'.$theme['images'].'disk.png" width="48" /><br />'.$l['disk_tuning'].'</a></center>
					</div>';

					// Video
					if($kernel->features('video', $vps['_virt'])){
						echo '<div class="col-sm-3 col-xs-4 pan-button">
							<center><a href="javascript:void(0);" target="" onclick="navigateto(\'advanced\', \'video_tab\', \'video_tab\');"><img src="'.$theme['images'].'admin/video.png" width="48" /><br />'.$l['video'].'</a></center>
						</div>';
					}
					
				}
				
				if($kernel->features('mac_support', $vps['_virt'])){
					echo '<div class="col-sm-3 col-xs-4 pan-button">
						<center><a href="javascript:void(0);" target="" onclick="navigateto(\'advanced\', \'mac_row\', \'network_tab\');"><img src="'.$theme['images'].'admin/mac.png" width="48" /><br />'.$l['macaddr'].'</a></center>
					</div>';
				}
				
				$nic_support = $kernel->features('nic', $vps['_virt']);
				if(!empty($nic_support)){
					echo '<div class="col-sm-3 col-xs-4 pan-button">
						<center><a href="javascript:void(0);" target="" onclick="navigateto(\'advanced\', \'nic_row\', \'network_tab\');"><img src="'.$theme['images'].'admin/nic.png" width="48" /><br />'.$l['nic'].'</a></center>
					</div>';
				}
				
				echo '<div class="col-sm-3 col-xs-4 pan-button">
						<center><a href="javascript:void(0);" target="" onclick="navigateto(\'notes\', \'notes\');"><img src="'.$theme['images'].'logs.png"  /><br />'.$l['notes'].'</a></center>
					</div><br>';
				
				if($kernel->features('iso_support', $vps['_virt'])){
					
					echo '<div class="col-sm-3 col-xs-4 pan-button">
							<center><a href="javascript:void(0);" target="" onclick="navigateto(\'advanced\', \'boot_order_row\', \'general_tab\');"><img src="'.$theme['images'].'os.png" /><br />'.$l['boot_order'].'</a></center>
						</div>';
				}
				
				if($kernel->features('cpu_pinning', $vps['virt'])){
					
					echo '<div class="col-sm-3 col-xs-4 pan-button">
							<center><a href="javascript:void(0);" target="" onclick="navigateto(\'advanced\', \'cpu_tab\', \'cpu_tab\');"><img src="'.$theme['images'].'cpu.png" width="48" /><br />'.$l['cpu_tune'].'</a></center>
						</div>';
				}
				
				if($kernel->features('iso_support', $vps['_virt']) && !empty($isos)){
					echo '<div class="col-sm-3 col-xs-4 pan-button">
							<center><a href="javascript:void(0);" target="" onclick="navigateto(\'advanced\', \'iso_row\', \'media_tab\');"><img src="'.$theme['images'].'admin/iso.png" width="48" /><br />'.$l['iso'].'</a></center>
						</div>';
				}
				
				if(!empty($vps['ips6_subnet'])) {
					echo '<div class="col-sm-3 col-xs-4 pan-button">
							<center><a href="javascript:void(0);" target="" data-toggle="modal" data-target="#manage_subnets"><img src="'.$theme['images'].'managesubnets.png" width="48" /><br />'.$l['manage_ipv6_subnets'].'</a></center>
						</div>';
				}
				
				if(!empty($globals['haproxy_enable'])) {
					echo '<div class="col-sm-3 col-xs-4 pan-button">
						<center><a href="javascript:void(0);" target="" data-toggle="modal" data-target="#managevdf"><img src="'.$theme['images'].'haproxy.png" width="48" /><br />'.$l['managevdf'].'</a></center>
					</div>';
				}
				
				// Webuzo block
				echo '<div class="col-sm-3 col-xs-4 pan-button">
						<center><a href="javascript: void(0);" data-toggle="modal" data-target="#webuzomodal" onclick="'.(!empty($vps['locked']) ? '' : 'load_scripts();').'"><img id="webuzo" src="'.$theme['images'].'webuzo_50.gif" width="45" /><br />'.$l['webuzo_mode'].'</a></center>
					</div>';
			echo '</div>
			</div><br />
		
		<!------------------------
		// Advanced Options Window
		------------------------->
		
		<div class="roundheader" onclick="toggle_option(\'advanced\');" style="cursor:pointer;height:30px;"><label id="advanced_toggle_indicator" style="width:10px;">-</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp'.$l['advanced'].'</div>
		<div id="advanced" class="bgaddv">
			<ul class="nav nav-tabs">
				<li class="active"><a data-toggle="tab" href="#general_tab">'.$l['general'].'</a></li>
				'.($kernel->features('cpu_pinning', $vps['virt']) || $kernel->features('cpu_mode', $vps['virt']) || $kernel->features('cpu_topology', $vps['virt'])? '<li><a data-toggle="tab" href="#cpu_tab">'.$l['cpu'].'</a></li>' : '').'
				<li><a data-toggle="tab" href="#media_tab">'.$l['media'].'</a></li>
				<li><a data-toggle="tab" href="#network_tab">'.$l['network'].'</a></li>
				'.($vps['virt'] == 'kvm' || $vps['virt'] == 'proxk' ? '<li><a data-toggle="tab" href="#disk_tab">'.$l['disk'].'</a></li>' : '').'
				'.($kernel->features('video', $vps['virt']) ? '<li><a data-toggle="tab" href="#video_tab"> Video </a></li>' : '').'
			</ul><br /><br />
			
			<div class="tab-content">
			
				<!---- // General //---------->
				
				<div id="general_tab" class="tab-pane in fade active">';
				
					if((($vps['virt'] == 'xen' || $vps['virt'] == 'xcp') && !empty($vps['hvm'])) || $vps['virt'] == 'kvm'){
						if($vps['virt'] != 'proxk'){
						
						echo '<div class="row">
							<div class="col-sm-6">
								<label class="control-label">ACPI</label>
							</div>
							<div class="col-sm-6">
								<input type="checkbox" class="ios" name="acpi" '.POSTchecked('acpi', $vps['acpi']).' value="1"/>
							</div>
						</div><br/>
						<div class="row">
							<div class="col-sm-6">
								<label class="control-label">APIC</label>
							</div>
							<div class="col-sm-6">
								<input type="checkbox" class="ios" name="apic" '.POSTchecked('apic', $vps['apic']).' value="1"/>
							</div>
						</div><br/>
						<div class="row">
							<div class="col-sm-6">
								<label class="control-label">PAE</label>
							</div>
							<div class="col-sm-6">
								<input type="checkbox" class="ios" name="pae" '.POSTchecked('pae', $vps['pae']).' value="1"/>
							</div>
						</div><br/>';
						}
					}
					
					if($kernel->features('iso_support', $vps['_virt'])){
						
						echo '<div class="row" id="boot_order_row">
							<div class="col-sm-6">
								<label class="control-label">'.$l['boot_order'].'</label>
							</div>
							<div class="col-sm-6">
								<select class="form-control chosen" name="boot">
									<option value="cda" '.(POSTval('boot') == 'cda' || (empty($_POST) && $vps['boot'] == 'cda') ? 'selected="selected"' : '').'>1) Hard Disk 2) CD Drive</option>
									<option value="dca" '.(POSTval('boot') == 'dca' || (empty($_POST) && $vps['boot'] == 'dca') ? 'selected="selected"' : '').'>1) CD Drive 2) Hard Disk</option>
								</select>
							</div>
						</div><br />';
					}
					
					// Only available for XEN-HVM
					if(($vps['virt'] == 'xen' && !empty($vps['hvm'])) && distro_check(0, 1, 1, 0, 1)){
						
						// PV-on-HVM drivers
						
						echo '<div class="row">
							<div class="col-xs-10 col-sm-6">
								<label class="control-label">'.$l['pv_on_hvm'].'</label><br />
								<apsn class="help-block">'.$l['exp_pv_on_hvm'].'</span>
							</div>
							<div class="col-xs-2 col-sm-6">
								<input type="checkbox" class="ios" name="pv_on_hvm" id="pv_on_hvm" '.POSTchecked('pv_on_hvm',$vps['pv_on_hvm']).' onchange="ispvonhvm();" value="1"/>
							</div>
					   </div><br/>';
					}
					
					// TUN/TAP 
					if($vps['virt'] == 'openvz' || $vps['virt'] == 'vzo' || $vps['virt'] == 'proxo'){
						
						$openvz_features = $vps['openvz_features'];
				
						echo '<div class="row hide_for_plan">
								<div class="col-xs-10 col-sm-6">
									<label class="control-label">'.$l['tuntap'].'</label><br />
									<span class="help-block">'.$l['exp_tuntap'].'</span>
								</div>
								<div class="col-xs-2 col-sm-6">
									<input type="checkbox" class="ios" name="tuntap" id="tuntap" '.POSTchecked('tuntap', $vps['tuntap']).' value="1"/>
								</div>
							</div>
							<div class="row hide_for_plan">
								<div class="col-xs-10 col-sm-6">
									<label class="control-label">'.$l['ppp'].'</label><br />
									<span class="help-block">'.$l['exp_ppp'].'</span>
								</div>
								<div class="col-xs-2 col-sm-6">
									<input type="checkbox" class="ios" name="ppp" id="ppp" '.POSTchecked('ppp', $vps['ppp']).' value="1"/>
								</div>
							</div><br/>
							<div class="row">
								<div class="col-xs-10 col-sm-6">
									<label class="control-label">'.$l['fuse'].'</label><br />
									<span class="help-block">'.$l['fuse_exp'].'</span>
								</div>
								<div class="col-xs-2 col-sm-6">
									<input type="checkbox" value="1" name="fuse" id="fuse" '.POSTchecked('fuse', $openvz_features['fuse']).' />
								</div>
							</div>
							<div class="row">
								<div class="col-xs-10 col-sm-6">
									<label class="control-label">'.$l['ipip'].'</label><br />
									<span class="help-block">'.$l['ipip_exp'].'</span>
								</div>
								<div class="col-xs-2 col-sm-6">
									<input type="checkbox" value="1" name="ipip" id="ipip" '.POSTchecked('ipip', $openvz_features['ipip']).' />
								</div>
							</div>
							<div class="row">
								<div class="col-xs-10 col-sm-6">
									<label class="control-label">'.$l['ipgre'].'</label><br />
									<span class="help-block">'.$l['ipgre_exp'].'</span>
								</div>
								<div class="col-xs-2 col-sm-6">
									<input type="checkbox" value="1" name="ipgre" id="ipgre" '.POSTchecked('ipgre', $openvz_features['ipgre']).' />
								</div>
							</div>
							<div class="row">
								<div class="col-xs-10 col-sm-6">
									<label class="control-label">'.$l['nfs'].'</label><br />
									<span class="help-block">'.$l['nfs_exp'].'</span>
								</div>
								<div class="col-xs-2 col-sm-6">
									<input type="checkbox" value="1" name="nfs" id="nfs" '.POSTchecked('nfs', $openvz_features['nfs']).' />
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<label class="control-label">'.$l['quotaugidlimit'].'</label><br />
									<span class="help-block">'.$l['quotaugidlimit_exp'].'</span>
								</div>
								<div class="col-sm-6">
									<input type="text" class="form-control" name="quotaugidlimit" id="quotaugidlimit" size="30" value="'.POSTval('quotaugidlimit', $openvz_features['quotaugidlimit']).'" />
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<label class="control-label">'.$l['iolimit'].'</label><br />
									<span class="help-block">'.$l['iolimit_exp'].'</span>
								</div>
								<div class="col-sm-6">
									<input type="text" class="form-control" name="iolimit" id="iolimit" size="30" value="'.POSTval('iolimit', $openvz_features['iolimit']).'" />
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<label class="control-label">'.$l['iopslimit'].'</label><br />
									<span class="help-block">'.$l['iopslimit_exp'].'</span>
								</div>
								<div class="col-sm-6">
									<input type="text" class="form-control" name="iopslimit" id="iopslimit" size="30" value="'.POSTval('iopslimit', $openvz_features['iopslimit']).'" />
								</div>
							</div>';	
					}
					
					if($kernel->features('ebtables_support', $vps['_virt'])){
					
						echo '<div class="row">
							<div class="col-sm-6">
								<label class="control-label">'.$l['disable_ebtables'].'</label>
								<span class="help-block"></span>
							</div>
							<div class="col-sm-6">
								<input type="checkbox" class="ios" name="disable_ebtables" id="disable_ebtables" '.POSTchecked('disable_ebtables', $vps['disable_ebtables']).' value="1"/>
							</div>
						</div><br />';
					
					}
					
					if($kernel->features('xenserver_tools', $vps['_virt'])){
					
						echo '<div class="row">
							<div class="col-sm-6">
								<label class="control-label">'.$l['install_xentools'].'</label>
								<span class="help-block"></span>
							</div>
							<div class="col-sm-6">
								<input type="checkbox" class="ios" name="install_xentools" id="install_xentools" '.POSTchecked('install_xentools', $vps['install_xentools']).' '.(!empty($vps['install_xentools']) ? 'disabled="disabled"' : '').' value="1"/>
							</div>
						</div><br />';
					
					}
					
					if($kernel->features('win_support', $vps['_virt'])){
						
						echo '<div class="row hide_for_plan">
							<div class="col-xs-10 col-sm-6">
								<label class="control-label">'.$l['rdp'].'</label><br />
								<span class="help-block">'.$l['exp_rdp'].'</span>
							</div>
							<div class="col-xs-2 col-sm-6">
								<input type="checkbox" class="ios" name="rdp" id="rdp" '.POSTchecked('rdp',$vps['rdp']).' value="1"/>
							</div>	
					   </div><br/>';
					
					}
					
					if(!empty($vps['vnc']) && $kernel->features('vnc_key_map', $vps['_virt'])){
						
						echo '<div class="row" id="vnckeymap">
							<div class="col-sm-6">
								<label class="control-label">'.$l['vnc_keymap'].'</span>
							</div>
							<div class="col-sm-6">
								<select class="form-control chosen" name="vnc_keymap" id="vnc_keymap" >
									<option value="en-us" '.ex_POSTselect('vnc_keymap', 'en-us', $vps['vnc_keymap']).'>en-us</option>
								<option value="de-ch" '.ex_POSTselect('vnc_keymap', 'de-ch', $vps['vnc_keymap']).'>de-ch</option>
								<option value="ar" '.ex_POSTselect('vnc_keymap', 'ar', $vps['vnc_keymap']).'>ar</option>
								<option value="da" '.ex_POSTselect('vnc_keymap', 'da', $vps['vnc_keymap']).'>da</option>
								<option value="et" '.ex_POSTselect('vnc_keymap', 'et', $vps['vnc_keymap']).'>et</option>
								<option value="fo" '.ex_POSTselect('vnc_keymap', 'fo', $vps['vnc_keymap']).'>fo</option>
								<option value="fr-be" '.ex_POSTselect('vnc_keymap', 'fr-be', $vps['vnc_keymap']).'>fr-be</option>
								<option value="fr-ch" '.ex_POSTselect('vnc_keymap', 'fr-ch', $vps['vnc_keymap']).'>fr-ch</option>
								<option value="hu" '.ex_POSTselect('vnc_keymap', 'hu', $vps['vnc_keymap']).'>hu</option>
								<option value="it" '.ex_POSTselect('vnc_keymap', 'it', $vps['vnc_keymap']).'>it</option>
								<option value="lt" '.ex_POSTselect('vnc_keymap', 'lt', $vps['vnc_keymap']).'>lt</option>
								<option value="mk" '.ex_POSTselect('vnc_keymap', 'mk', $vps['vnc_keymap']).'>mk</option>
								<option value="nl" '.ex_POSTselect('vnc_keymap', 'nl', $vps['vnc_keymap']).'>nl</option>
								<option value="no" '.ex_POSTselect('vnc_keymap', 'no', $vps['vnc_keymap']).'>no</option>
								<option value="pt" '.ex_POSTselect('vnc_keymap', 'pt', $vps['vnc_keymap']).'>pt</option>
								<option value="ru" '.ex_POSTselect('vnc_keymap', 'ru', $vps['vnc_keymap']).'>ru</option>
								<option value="sv" '.ex_POSTselect('vnc_keymap', 'sv', $vps['vnc_keymap']).'>sv</option>
								<option value="tr" '.ex_POSTselect('vnc_keymap', 'tr', $vps['vnc_keymap']).'>tr</option>
								<option value="de" '.ex_POSTselect('vnc_keymap', 'de', $vps['vnc_keymap']).'>de</option>
								<option value="en-gb" '.ex_POSTselect('vnc_keymap', 'en-gb', $vps['vnc_keymap']).'>en-gb</option>
								<option value="es" '.ex_POSTselect('vnc_keymap', 'es', $vps['vnc_keymap']).'>es</option>
								<option value="fi" '.ex_POSTselect('vnc_keymap', 'fi', $vps['vnc_keymap']).'>fi</option>
								<option value="fr" '.ex_POSTselect('vnc_keymap', 'fr', $vps['vnc_keymap']).'>fr</option>
								<option value="fr-ca" '.ex_POSTselect('vnc_keymap', 'fr-ca', $vps['vnc_keymap']).'>fr-ca</option>
								<option value="hr" '.ex_POSTselect('vnc_keymap', 'hr', $vps['vnc_keymap']).'>hr</option>
								<option value="is" '.ex_POSTselect('vnc_keymap', 'is', $vps['vnc_keymap']).'>is</option>
								<option value="ja" '.ex_POSTselect('vnc_keymap', 'ja', $vps['vnc_keymap']).'>ja</option>
								<option value="lv" '.ex_POSTselect('vnc_keymap', 'lv', $vps['vnc_keymap']).'>lv</option>
								<option value="nl-be" '.ex_POSTselect('vnc_keymap', 'nl-be', $vps['vnc_keymap']).'>nl-be</option>
								<option value="pl" '.ex_POSTselect('vnc_keymap', 'pl', $vps['vnc_keymap']).'>pl</option>
								<option value="pt-br" '.ex_POSTselect('vnc_keymap', 'pt-br', $vps['vnc_keymap']).'>pt-br</option>
								<option value="sl" '.ex_POSTselect('vnc_keymap', 'sl', $vps['vnc_keymap']).'>sl</option>
								<option value="th" '.ex_POSTselect('vnc_keymap', 'th', $vps['vnc_keymap']).'>th</option>
								</select>
							</div>
						</div><br />';
					}
					
				echo '<div class="row">
						<div class="col-sm-6">
							<label class="control-label">'.$l['admin_managed'].'</label>&nbsp;<img class="wiki_help" title="'.$l['exp_admin_managed'].'" src="'.$theme['images'].'admin/information.gif" /><br />
							<span class="help-block"></span>
						</div>
						<div class="col-sm-6">
							<input type="checkbox" class="ios" name="admin_managed" '.POSTchecked('admin_managed', $vps['admin_managed']).' value="1"/>
						</div>
					</div>';
					
					if($kernel->features('nw_config', $vps['virt'])){
					
						echo '
					<div class="row">
						<div class="col-sm-6">
							<label class="control-label">'.$l['disable_nw_config'].'</label>
							<span class="help-block">'.$l['exp_disable_nw_config'].'</span>
						</div>
						<div class="col-sm-6">
							<input type="checkbox" class="ios" name="disable_nw_config" id="disable_nw_config" '.POSTchecked('disable_nw_config', $vps['disable_nw_config']).' value="1"/>
						</div>
					</div>';
					
					}
					
				echo '</div>
				
				<!---- // CPU //---------->
				<div id="cpu_tab" class="tab-pane">';
				
					if($kernel->features('cpu_pinning', $vps['virt'])){
					
						echo '
					<div class="row">
						<div class="col-sm-6">
							<label class="control-label">'.$l['cpupin'].'</label><br />
							<span class="help-block">'.$l['cpupin_exp'].'</span>
						</div>
						<div class="col-sm-6">
							<label><input type="checkbox" class="ios" name="allcores" id="allcores" '.(!empty($_POST) ? (is_array($_POST['cpupin']) ? '' : 'checked="checked"') : ($vps['cpupin'] == -1 ? 'checked="checked"' : '')).' onchange="pincheck()" value="-1" />&nbsp;Default</label>';
						echo '</div>
					</div><br />
					<div class="row" id="pincores">
						<div class="col-sm-6">
							<label class="control-label">'.$l['cpupin_select'].'</label>
							<span class="help-block">&nbsp;</span>
						</div>
							
						<div class="col-sm-6">';
						for($i=0; $i < $resources['cpucores']; $i++){
							echo '<label><input type="checkbox" id="pin'.$i.'" class="ios cpupin" name="cpupin['.$i.']" value="'.$i.'" '.(!empty($_POST) ? (in_array($i, $_POST['cpupin']) || in_array((string)$i, $_POST['cpupin']) ? 'checked="checked"' : '') : (in_array($i, $vps['_cpupin']) || in_array((string)$i, $vps['_cpupin']) ? 'checked="checked"' : '')).' />&nbsp;vCPU '.($i+1).'&nbsp;</label>';
						}
							
						echo '</div>
					</div><br />';
					}
				
					if($kernel->features('numa',  $vps['_virt'])){
						echo '
						<div class="row">
							<div class="col-sm-6">
								<label class="control-label">'.$l['usenuma'].'</label>
								<span class="help-block">&nbsp;</span>
							</div>
							<div class="col-sm-6">
								<input type="checkbox" class="ios" name="numa" value="1" id="numa" '.POSTchecked('numa', $vps['numa']).' />
							</div>
						</div>';
					}
					
					
					if($kernel->features('cpu_mode',  $vps['_virt'])){	
						echo '<div class="row">
							<div class="col-sm-6">
								<label class="control-label">'.$l['cpu_mode'].'</label>
								<span class="help-block">&nbsp;</span>
							</div>
							<div class="col-sm-6">
								<select class="form-control chosen" name="cpu_mode" id="cpu_mode">';
						
								foreach($cpu_modes as $ck => $cv){
									echo '<option value="'.$ck.'" '.ex_POSTselect('cpu_mode', $ck, $vps['cpu_mode']).'>'.$cv.'</option>';
								}
								
								echo '
								</select>
							</div>
						</div><br />';
					}
					
					if($kernel->features('cpu_topology', $vps['_virt'])){
						echo '
						<div class="row">
							<div class="col-sm-6">
								<label class="control-label">'.$l['cpu_topology'].'</label>
							</div>
							<div class="col-sm-6">
								<input type="checkbox" class="ios" id="enable_cpu_topology" name="enable_cpu_topology" '.(!empty($vps['topology_sockets']) ? 'checked="checked"' : '' ).' onclick="cputopology();" value="1"/>
							</div>
						</div><br />
						<div id="cpu_topology_option">
							<div class="row">
								<div class="col-sm-6">
									<label class="control-label">Sockets</label>
								</div>
								<div class="col-sm-6">
									<input type="text" name="topology_sockets" id="topology_sockets" class="form-control numbersonly" size="6" value="'.POSTval('topology_sockets', $vps['topology_sockets']).'" >
								</div>
							</div><br />
							<div class="row">
								<div class="col-sm-6">
									<label class="control-label">Cores</label>
								</div>
								<div class="col-sm-6">
									<input type="text" class="form-control numbersonly" name="topology_cores" id="topology_cores" size="6" value="'.POSTval('topology_cores', $vps['topology_cores']).'" >
								</div>
							</div><br />';
							
						if($kernel->features('cpu_threads', $vps['virt'])){
							echo '
							<div class="row">
								<div class="col-sm-6">
									<label class="control-label">Threads</label>
								</div>
								<div class="col-sm-6">
									<input type="text" class="form-control numbersonly" name="topology_threads" id="topology_threads" size="6" value="'.POSTval('topology_threads', $vps['topology_threads']).'" >
								</div>
							</div><br />';
						}
						
						echo '
						</div>';
					}
					
				echo '</div>
				
				<!---- // Media //---------->
				<div id="media_tab" class="tab-pane">';
					
					// Is webuzo enabled ?
					if(!empty($iscripts) && $ostemplates[$vps['osid']]['distro'] == 'webuzo'){

						echo '<div class="row">
							<div class="col-sm-12">
								<label class="control-label">'.$l['apps'].'</label>&nbsp;<img class="wiki_help" title="'.$l['apps_exp'].'" src="'.$theme['images'].'admin/information.gif" /><br /><br />
							</div>
							<div class="col-sm-10">
							
							<select class="form-control" name="sid" id="sid">
							<option value="0" '.(POSTval('sid') == 0 ? 'selected="selected"' : '').'>'.$l['none'].'</option>';
							
							foreach($iscripts as $k => $v){		
								echo '<option value="'.$k.'" '.(POSTval('sid', $vps['webuzo']) == $k ? 'selected="selected"' : '').'>'.$scripts[$k]['name'].'</option>';
							}		
							 echo'</select>
							</div>
						</div><br />';
					}
				
					echo '<div class="row">
						<div class="col-sm-5">
							<label class="control-label">'.$l['mg'].'</label><br />
							<span class="help-block">'.$l['mg_exp'].'</span>
						</div>
						<div class="col-sm-6">
							<select class="form-control" name="mgs[]" id="mg" multiple="multiple" style="width:250px">';
								
								foreach($mgs as $mk => $mv){
									if($vps['_virt'] != $mv['mg_type']) continue;
									echo '<option value="'.$mk.'" type="'.$mv['mg_type'].'" '.(in_array($mk, (empty($_POST['editvps']) ? $vps['mg'] : @$_POST['mgs'])) ? 'selected="selected"' : '').'>'.$mv['mg_name'].'</option>';
								}
								
							echo '</select>
							<span class="help-block"></span>
						</div>
						<div class="col-sm-1"></div>
					</div>
					<div class="row">
						<div class="col-sm-5">
							<label class="control-label">'.$l['osreinstall'].'</label><br />
							<span class="help-block">'.$l['exp_osreinstall'].'</span>
						</div>
						<div class="col-sm-6">
							<input type="text" class="form-control" name="osreinstall_limit" id="osreinstall_limit"  value="'.POSTval('osreinstall_limit', $vps['osreinstall_limit']).'" />
						</div>
						<div class="col-sm-1"></div>
					</div>';
					
					if(!empty($isos) && $kernel->features('iso_support', $vps['_virt'])){
						echo '<div class="row" id ="iso_row">
							<div class="col-sm-12">
								<label class="control-label">'.$l['vsiso'].'</label>&nbsp;<img class="wiki_help" title="'.$l['vsiso_exp'].'" src="'.$theme['images'].'admin/information.gif" /><br /><br />
							</div>
							<div class="col-sm-10">';
							
							echo '<select class="form-control chosen" name="iso" id="iso">
							<option value="0" '.(POSTval('iso') == 0 ? 'selected="selected"' : '').'>'.$l['none'].'</option>';
							
							$options_eu_iso = $options_iso = '';
							foreach($isos as $k => $v){
								if(!empty($v['isuseriso'])){
									$options_eu_iso .= '<option value="'.$k.'" '.(POSTval('iso') == $k ? 'selected="selected"' : ($vps['iso'] == $k ? 'selected="selected"' : '')). '>'.$v['name'].'</option>';
								}else{
									$options_iso .= '<option value="'.$k.'" '.(POSTval('iso') == $k ? 'selected="selected"' : ($vps['iso'] == $k ? 'selected="selected"' : '')).'>'.$v['name'].'</option>';
								}
							}
							if(!empty($options_iso)){
								echo '<optgroup label="'.$l['admin_iso'].'">'.$options_iso.'</optgroup>';
							}
							
							if(!empty($options_eu_iso)){
								echo '<optgroup label="'.$l['eu_iso'].'">'.$options_eu_iso.'</optgroup>';
							}
							 echo'</select>
							</div>
						</div><br />';
					}
							
					if(!empty($isos) && $kernel->features('sec_iso_support', $vps['_virt'])){
						echo '<div class="row">
							<div class="col-sm-12">
								<label class="control-label">'.$l['sec_vsiso'].'</label>&nbsp;<img class="wiki_help" title="'.$l['sec_vsiso_exp'].'" src="'.$theme['images'].'admin/information.gif" /><br /><br />
							</div>
							<div class="col-sm-10">
							
							<select class="form-control chosen" name="sec_iso" id="sec_iso">
							<option value="0" '.(POSTval('sec_iso') == 0 ? 'selected="selected"' : '').'>'.$l['none'].'</option>';
							$options_eu_iso = $options_iso = '';
							foreach($isos as $k => $v){
								if(!empty($v['isuseriso'])){
									$options_eu_iso .= '<option value="'.$k.'" '.(POSTval('sec_iso') == $k ? 'selected="selected"' : ($vps['sec_iso'] == $k ? 'selected="selected"' : '')).'>'.$v['name'].'</option>';
								}else{
									$options_iso .= '<option value="'.$k.'" '.(POSTval('sec_iso') == $k ? 'selected="selected"' : ($vps['sec_iso'] == $k ? 'selected="selected"' : '')).'>'.$v['name'].'</option>';
								}
							}

							if(!empty($options_iso)){
								echo '<optgroup label="'.$l['admin_iso'].'">'.$options_iso.'</optgroup>';
							}
							if(!empty($options_eu_iso)){
								echo '<optgroup label="'.$l['eu_iso'].'">'.$options_eu_iso.'</optgroup>';
							}
							echo'</select>
							</div>
						</div><br />';
					}
					
				echo '</div>
				
				<!---- // Networking //---------->
				
				<div id="network_tab" class="tab-pane">
					
					<div class="row">
						<div class="col-sm-6">
							<label class="control-label">'.$l['band'].'</label>&nbsp;<img class="wiki_help" title="'.$l['exp_band'].'" src="'.$theme['images'].'admin/information.gif" /><br /><br />
						</div>
						<div class="col-sm-5">
						<input type="text" class="form-control" name="bandwidth" id="band" size="30" value="'.POSTval('bandwidth', $vps['bandwidth']).'" onchange="handle_capping();" />
						</div>
						<div class="col-sm-1" style="padding-top:10px">'.$l['band_gb'].'</div>
					</div><br /><br />

					<script type="text/javascript">
					function netspeed(r){
						$_("network_speed").value = r;
						handle_capping();
					}
					function upspeed(r){
						$_("upload_speed").value = r;
						handle_capping();
					}
					</script>

					<div class="row">
						<div class="col-sm-12">
							<label class="control-label">'.$l['network_speed'].'</label>&nbsp;<img class="wiki_help" title="'.$l['network_speed_exp'].'" src="'.$theme['images'].'admin/information.gif" /><br /><br />
						</div>
						<div class="col-sm-4">
							<input type="text" class="form-control" name="network_speed" id="network_speed" size="8" value="'.POSTval('network_speed', $vps['network_speed']).'" onchange="handle_capping();" />
						 </div>
						 <div class="col-sm-2" style="padding-top:8px;">'.$l['net_kb'].'</div>
						 <div class="col-sm-6">
							<select class="form-control speedmbits chosen" name="network_speed2" id="network_speed2" onchange="netspeed(this.value)"></select>

						 </div>
					</div><br /><br />
					
					<div class="row">
						<div class="col-sm-12">
							<label class="control-label">'.$l['upload_speed'].'</label>&nbsp;<img class="wiki_help" title="'.$l['upload_speed_exp'].'" src="'.$theme['images'].'admin/information.gif" /><br /><br />
						</div>
						<div class="col-sm-4">
							<input type="text" class="form-control" name="upload_speed" id="upload_speed" size="8" value="'.POSTval('upload_speed', $vps['upload_speed']).'" onchange="handle_capping();" />
						</div>
						<div class="col-sm-2" style="padding-top:8px">'.$l['net_kb'].'</div>
						<div class="col-sm-6">
							<select class="form-control speedmbits chosen" name="upload_speed2" id="upload_speed2" onchange="upspeed(this.value)"></select>
						</div>
					</div><br /><br />
					
					<div class="row">
						<div class="col-xs-10 col-sm-6">
							<label class="control-label">'.$l['band_suspend'].'</label><br />
							<span class="help-block">'.$l['exp_band_suspend'].'</span>
						</div><br />
						<div class="col-xs-2 col-sm-6">
							<input type="checkbox" class="ios" name="band_suspend" id="band_suspend" '.POSTchecked('band_suspend',$vps['band_suspend']).' onchange="handle_capping();" value="1"/>
						</div>	
					</div><br />
					<div id="speed_cap_limit">
						<div class="row">
							<div class="col-xs-10 col-sm-6">
								<label class="control-label" for="speed_cap_down">'.$l['speed_cap_down'].'</label>
								<span class="help-block">'.$l['exp_speed_cap_down'].'</span>
							</div>
							<div class="col-xs-2 col-sm-6">
								<input type="number" class="form-control" name="speed_cap_down" id="speed_cap_down" value="'.POSTval('speed_cap_down', $vps['speed_cap']['down']).'" onmouseout="blur();"/>&nbsp;'.$l['net_kb'].'
								<select class="form-control speedmbits chosen" name="speed_cap_down2" id="speed_cap_down2" onchange="$(\'#speed_cap_down\').val(this.value)"></select>
							</div>
						</div><br>
						<div class="row">
							<div class="col-xs-10 col-sm-6">
								<label class="control-label" for="speed_cap_up">'.$l['speed_cap_up'].'</label>
								<span class="help-block">'.$l['exp_speed_cap_up'].'</span>
							</div>
							<div class="col-xs-2 col-sm-6">
								<input type="number" class="form-control" name="speed_cap_up" id="speed_cap_up" value="'.POSTval('speed_cap_up', $vps['speed_cap']['up']).'" onmouseout="blur();"/>&nbsp;'.$l['net_kb'].'
								<select class="form-control speedmbits chosen" name="speed_cap_up2" id="speed_cap_up2" onchange="$(\'#speed_cap_up\').val(this.value)"></select>
							</div>
						</div><br>
					</div>';
				   
					// Internal IP
					if(!empty($ips_int) || !empty($vps['ips_int'])){
					
						echo '<div class="row" id="internal_ip_row">
						<div class="col-sm-6">
							<label class="control-label">'.$l['ips_int'].'</label><br />
							<span class="help-block">'.$l['ips_int_exp'].'</span>
						</div>
						<div class="col-sm-6">
						<select id="ips_int" class="form-control" name="ips_int[]" size="5" multiple="multiple">';
					
						$_ips_int = array();

						// This is to put the Posted IPs first
						if(!empty($_POST['ips_int'])){
							foreach($_POST['ips_int'] as $k => $v){
								$_ips_int[$v] = $v;
							}
						}
						
						// Then the VPS IPs
						foreach($vps['ips_int'] as $k => $v){
							$_ips_int[$v] = $v;
						}
						
						// Then the extra IPs
						foreach($ips_int as $k => $v){
							$_ips_int[$v['ip']] = $v['ip'];
						}
						
						$_ips_int = array_unique($_ips_int);
						
						foreach($_ips_int as $k => $v){	 
							echo '<option value="'.$v.'" '.(isset($_POST['editvps']) ? (@in_array($v, $_POST['ips_int']) ? 'selected="selected"' : '') : (@in_array($v, $vps['ips_int']) ? 'selected="selected"' : '')).'>'.$v.'</option>';
						}
								
						echo '</select>
						</div>
						</div><br/>';
						
					}
					
					if(!empty($vps['mac'])){
						echo '<div class="row" id="mac_row">
							<div class="col-sm-6">
								<label class="control-label">'.$l['mac'].'</span>&nbsp;<img class="wiki_help" title="'.$l['exp_mac'].'" src="'.$theme['images'].'admin/information.gif" />
							</div>
							<div class="col-sm-6">
								<input type="text" class="form-control" name="mac" id="mac" value="'.POSTval('mac', $vps['mac']).'">
							</div>
						</div><br /><br />';
					}
				
					if(!empty($supported_nics)){
						
						echo '<div class="row" id="nic_row">
							<div class="col-sm-6">
								<label class="control-label">'.$l['changenic'].'</label><br /><br />
							</div>
							<div class="col-sm-6">
								<select class="form-control chosen" name="nic_type" id="nic_type" >';
							
								foreach($supported_nics as $k => $v){
					
									echo '<option value="'.$v.'" '.ex_POSTselect('nic_type', $v, $vps['nic_type']).'>'.(!empty($l['nic_'.$v]) ? $l['nic_'.$v] : $v).'</option>';
								}
								
								echo '</select>
							</div>
						</div><br /><br />';
					}
				
					if($vps['virt'] == 'xen' && !empty($vps['hvm'])){
						echo '<div class="row" id="tr_viftype">
							<div class="col-sm-6">
								<label>'.$l['change_vif_type'].'&nbsp<img class="wiki_help" title="'.$l['exp_change_vif_type'].'" src="'.$theme['images'].'admin/information.gif" /></label>
							</div>
							<div class="col-sm-6">
								<select name="vif_type" class="form-control" id="vif_type">
									<option value="netfront" '.ex_POSTselect('vif_type', 'netfront', $vps['vif_type']).'>'.$l['viftype_netfront'].'</option>
									<option value="ioemu" '.ex_POSTselect('vif_type', 'ioemu', $vps['vif_type']).'>'.$l['viftype_ioemu'].'</option>
								</select>
							</div>
						</div><br /><br />';
					}
					
					echo '<div class="row">
						<div class="col-sm-6">
							<label class="control-label">'.$l['dns'].'</label><br />
							<span class="help-block">'.$l['exp_dns'].'</span>
						</div>
						<div class="col-sm-6">
							<div class="row">
								<div class="col-sm-12" id="dnstable">';
								 
									$_dns = @(empty($_POST['dns']) ? $vps['dns'] : $_POST['dns']);

									if(is_array($_dns) && !empty($_dns)){
										foreach($_dns as $d => $ds){
											if(empty($ds)){
												unset($_dns[$d]);
											}
										}
									}
										
									if(empty($_dns)){
										$_dns = array(NULL);
									}
									 
									foreach($_dns as $dn){
										echo '<div class="row">
										<div class="col-sm-12">
											<input type="text" class="form-control" name="dns[]" value="'.$dn.'" />
										</div>
										</div>';
									}
										
								echo '
									<br />
								</div>	
								</div>
								<input type="button" onclick="adddnsrow(\'dnstable\')" class="go_btn" value="'.$l['add_dns'].'"/>

						</div>
					</div><br />
				</div>
				
				<!---- // DISK //---------->
				<div id="disk_tab" class="tab-pane">';
				
				if($kernel->features('virtio', $vps['_virt'])){
					
					echo '
					<div class="row">
						<div class="col-xs-10 col-sm-6">
							<label class="control-label">'.$l['usevirtio'].'</label><br />
							<span class="help-block">'.$l['exp_usevirtio'].'</span>
						</div>
						<div class="col-xs-2 col-sm-6">
							<input type="checkbox" class="ios" name="virtio" value="1" id="virtio" onchange="return virtio_warning()" '.POSTchecked('virtio', $vps['virtio']).' />
						</div>
					</div><br/>';
					
				}	

				if($kernel->features('kvm_cache', $vps['virt'])){
					
					echo '
					<div class="row">
						<div class="col-sm-6">
							<label class="control-label">'.$l['kvm_cache'].'</label><br />
							<span class="help-block">'.$l['exp_kvm_cache'].'</span>
						</div>
						<div class="col-sm-6">
							<select class="form-control chosen" name="kvm_cache" id="kvm_cache">
								<option value="0" '.ex_POSTselect('kvm_cache', 'none', $vps['kvm_cache']).'>None</option>
								<option value="writeback" '.ex_POSTselect('kvm_cache', 'writeback', $vps['kvm_cache']).'>Writeback</option>
								<option value="writethrough" '.ex_POSTselect('kvm_cache', 'writethrough', $vps['kvm_cache']).'>Writethrough</option>
								<option value="directsync" '.ex_POSTselect('kvm_cache', 'directsync', $vps['kvm_cache']).'>Direct Sync</option>
								<option value="default" '.ex_POSTselect('kvm_cache', 'default', $vps['kvm_cache']).'>Default</option>
							</select>
						</div>
					</div><br/>';
				}
					
				if($kernel->features('io_mode', $vps['_virt'])){
					
					echo '
					<div class="row">
						<div class="col-sm-6">
							<label class="control-label">'.$l['io_mode'].'</label>
							<span class="help-block"></span>
						</div>
						<div class="col-sm-6">
							<select class="form-control chosen" name="io_mode" id="io_mode">
								<option value="0" '.ex_POSTselect('io_mode', 'default', $vps['io_mode']).'>Default</option>
								<option value="native" '.ex_POSTselect('io_mode', 'native', $vps['io_mode']).'>Native</option>
								<option value="threads" '.ex_POSTselect('io_mode', 'threads', $vps['io_mode']).'>Threads</option>
							</select>
						</div>
					</div><br />';
				}
					
					if(!empty($os_check)){
						echo '<div class="row">
							<div class="col-sm-6">
								<label class="control-label">'.$l['total_iops_sec'].'</label>
							</div>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="total_iops_sec" value="'.POSTval('total_iops_sec', $vps['total_iops_sec']).'" />
							</div>
							<div class="col-sm-2"></div>
						</div><br/>
						<div class="row">
							<div class="col-sm-6">
								<label class="control-label">'.$l['read_bytes_sec'].'</label>
							</div>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="read_bytes_sec" value="'.POSTval('read_bytes_sec', $vps['read_bytes_sec']).'" />
							</div>
							<div class="col-sm-2" style="padding-top: 8px;">MB/s</div>
						</div><br/>
						<div class="row">
							<div class="col-sm-6">
								<label class="control-label">'.$l['write_bytes_sec'].'</label>
							</div>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="write_bytes_sec" value="'.POSTval('write_bytes_sec', $vps['write_bytes_sec']).'" />
							</div>
							<div class="col-sm-2" style="padding-top: 8px;">MB/s</div>
						</div><br/>';
					}
					
					
				echo '</div>
				
				<!---- // Video //---------->
				<div id="video_tab" class="tab-pane">
					<div class="row">
						<div class="col-xs-10 col-sm-6">
							<label class="control-label">'.$l['kvm_vga'].'</label>
							<span class="help-block"></span>
						</div>
						<div class="col-xs-2 col-sm-6">
							<input type="checkbox" class="ios" name="kvm_vga" id="kvm_vga" onchange="enable_accel();" '.POSTchecked('kvm_vga', $vps['kvm_vga']).' value="1"/>
						</div>
					</div>
					<div class="row" id="enable_acceleration" style="display:none;">
						<div class="col-xs-10 col-sm-6">
							<label class="control-label">'.$l['acceleration'].'</label>
							<span class="help-block">'.$l['exp_acceleration'].'</span>
						</div>
						<div class="col-xs-2 col-sm-6">
							<input type="checkbox" class="ios" name="acceleration" id="acceleration" '.POSTchecked('acceleration', $vps['acceleration']).' value="1"/>
						</div>
					</div>
				</div>
			</div><br />
		</div><br />
		
		<!-----------------------------
		//  Resource Utilization Window
		------------------------------>
		
		<div class="roundheader" onclick="toggle_option(\'resource_utilization\');" style="cursor:pointer;height:30px;"><label id="resource_utilization_toggle_indicator" style="width:10px;">-</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp'.$l['resource_utilization'].'</div>
		<div id="resource_utilization" class="bgaddv">
			<label class="control-label"> CPU :</label><br /><br />
			<div class="progress">
				<div id="cpu_bar" class="progress-bar" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:0%">
					0%
				</div>
			</div>
			<span class="help-block">'.$l['cores'].' : '.$vps['cores'].'&nbsp; | &nbsp;'.$l['percent'].' : '.$vps['cpu_percent'].'</span>
			<label class="control-label">'.$l['band'].' :</label><br /><br />
			<div class="progress">
				<div id="bandwidth_bar" class="progress-bar" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:0%">
					0%
				</div>
			</div>
			<span class="help-block">'.$l['band_used'].' : '.$vps['used_bandwidth'].'&nbsp; | &nbsp;'.$l['band_limit'].' : '.$vps['bandwidth'].'</span>
			<label class="control-label">'.$l['disk'].' :</label><br /><br />
			<div class="progress">
				<div id="disk_bar" class="progress-bar" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:0%">
					0%
				</div>
			</div>';
			
			// File system for non OpenVZ 
			if(!$kernel->features('openvz', $vps['_virt'])){
				echo '<span class="help-block">'.$l['file_system'].' : '.$vps['cached_disk']['disk']['Filesystem'].'</span>';
			}
			
			echo '<span class="help-block">'.$l['utilized'].' : '.$vps['stats']['used_disk'].'&nbsp;GB &nbsp; | &nbsp; '.$l['avaiable'].' : '.$vps['stats']['avaiable_disk'].'&nbsp;GB</span>';
			
			if($kernel->features('vps_ram_info', $vps['_virt'])){
				
				echo '<label class="control-label">'.$l['ram'].' :</label><br /><br />
				<div class="progress">
					<div id="ram_bar" class="progress-bar" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:0%">
						0%
					</div>
				</div>
				<span class="help-block">'.$l['allocated_ram'].' : '.$vps['ram'].' MB &nbsp; | &nbsp; '.$l['used_ram'].' : '.$vps['stats']['used_ram'].' MB</span>';
			}
			
		echo '</div><br />
		
		<!-----------------------------
		//  VPS Notes Window
		------------------------------>
		
		<div class="roundheader" onclick="toggle_option(\'notes\');" style="cursor:pointer;height:30px;"><label id="notes_toggle_indicator" style="width:10px;">-</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp'.$l['notes'].'</div>
		<div id="notes" class="bgaddv">
			<div class="row">
				<div class="col-sm-12">
					<textarea  name="notes" id="notes" rows="10" style="width:100%">'.POSTval('notes', $vps['notes']).'</textarea>
				</div>
			</div>
		</div>
		
	</div>
	<div class="col-sm-6 col-xs-12">
		
		<!--------------------------
		//  Edit User Window
		--------------------------->
		
		<div class="roundheader" onclick="toggle_option(\'edit_user\');" style="cursor:pointer;height:30px;"><label id="edit_user_toggle_indicator" style="width:10px;">-</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp'.$l['edit_user'].'</div>
		<div id="edit_user" class="bgaddv">
			<div class="row" id="change_user_row">
				<div class="col-sm-6">
					<label class="control-label">'.$l['vs_user'].'&nbsp;&nbsp;<a href="https://'.$globals ['HTTP_HOST'].':4083/'.$SESS['token_key'].'/index.php?suid='.$vps['uid'].'" target="_blank" title="'.$l['svs_admin_login'].'"><img title="'.$l['svs_admin_login'].'" src="'.$theme['images'].'admin/svs_login.gif"/></a></label>
					<span class="help-block">'.$l['vs_user_exp'].'</span>
				</div>
				<div class="col-sm-6">
					<select class="form-control chosen" name="uid" id="uid" onchange="adduser()">
					<option value="0" '.(isset($_POST['editvps']) && $_POST['uid'] == 0 ? 'selected="selected"' : '').'>'.$l['add_user'].'</option>';	
						foreach($users as $k => $v){
							echo '<option value="'.$k.'" '.ex_POSTselect('uid', $k, $vps['uid']).'>'.$v['email'].'</option>';
						}
				
					echo'</select>
				</div>
			</div>
			
			<div class="row">
				<div class="col-sm-12">
					<fieldset class="user_details_f" style="display:none">
						<legend class="user_details_f">'.$l['user_details'].'</legend>
						<div class="row user_details" style="display:none">
							<div class="col-sm-5">
								<label class="control-label">'.$l['user_email'].'</label>
							</div>
							<div class="col-sm-6">
								<input type="text" class="form-control" name="user_email" size="30" value="'.(isset($_POST['editvps']) && !empty($_POST['user_email']) ? $_POST['user_email'] : '').'" />
								<span class="help-block"></span>
							</div>
							<div class="col-sm-1"></div>
							<span class="help-block">&nbsp;</span>
						</div>
						<div class="row user_details" style="display:none">
							<div class="col-sm-5">
								<label class="control-label">'.$l['user_pass'].'</label>
							</div>
							<div class="col-sm-6">
								<input type="text" class="form-control" name="user_pass" size="30" value="'.(isset($_POST['editvps']) && !empty($_POST['user_pass']) ? $_POST['user_pass'] : '').'" id="rand_user_pass"/>
								<span class="help-block"></span>
							</div>
							<div class="col-sm-1" style="padding-top: 8px;">
								<a href="javascript: void(0);" onclick="$_(\'rand_user_pass\').value=rand_pass(12);check_pass_strength(\'rand_user_pass\');return false;" title="'.$l['randpass'].'"><img src="'.$theme['images'].'randpass.gif" /></a>
							</div>
						</div>
						
						<div class="row user_details" style="display:none">
							<div class="col-sm-5">
								<label class="control-label">'.$l['fname'].'</label>
								<span class="help-block">&nbsp;</span>
							</div>
							<div class="col-sm-6">
								<input type="text" class="form-control" name="fname" size="30" value="'.(isset($_POST['editvps']) && !empty($_POST['fname']) ? $_POST['fname'] : '').'" />
							</div>
							<div class="col-sm-1"></div>
						</div>
						
						<div class="row user_details" style="display:none">
							<div class="col-sm-5">
								<label class="control-label">'.$l['lname'].'</label>
								<span class="help-block">&nbsp;</span>
							</div>
							<div class="col-sm-6">
								<input type="text" class="form-control" name="lname" size="30" value="'.(isset($_POST['editvps']) && !empty($_POST['lname']) ? $_POST['lname'] : '').'" />
							</div>
							<div class="col-sm-1"></div>
						</div>
					</fieldset>
				</div>
			</div>
			<div class="row user_details" style="display:none">
				<div class="col-sm-6">
					<label class="control-label">'.$l['dnsplid'].'</label>
					<span class="help-block">'.$l['dnsplid_exp'].'</span>
				</div>
				<div class="col-sm-6">		
					<select class="form-control" name="dnsplid">';
						 foreach($dnsplans as $dk => $dv){		 
							echo '<option value="'.$dk.'" '.(isset($_POST['editvps']) && $_POST['dnsplid'] == $dk ? 'selected="selected"' : '').'>'.$dv['plan_name'].'</option>';
						}
				
					echo '</select>
				</div>
			</div><br />		
		</div><br />
		
		<!--------------------------
		//  Edit VPS Window
		--------------------------->
		
		<div class="roundheader" onclick="toggle_option(\'edit_vps\');" style="cursor:pointer;height:30px;"><label id="edit_vps_toggle_indicator" style="width:10px;">-</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp'.$l['edit_vps'].'</div>
		<div id="edit_vps" class="bgaddv">
			<div class="row"> 
				<div class="col-sm-12">
					<label class="control-label">'.$l['vs_plan'].'</label><br />
				</div>
				<div class="col-sm-10">
					<br /><select class="form-control chosen" id="plid" name="plid" onchange="loadplan(this.value)">';
				 foreach($plans as $k => $v){
					echo '<option value="'.$k.'" '.(POSTval('plid') == $k ? 'selected="selected"' : ($vps['plid'] == $k ? 'selected="selected"' : '')).'>'.$v['plan_name'].'</option>';
				}
				
			echo '</select>
				</div>
			</div><br /><br />';
			
			if(!empty($backup_plans)){
					
				$bpid = !empty($_POST) ? POSTval('bpid') : $vps['bpid'];
			
				echo '<div class="row"> 
				<div class="col-sm-12">
					<label class="control-label">'.$l['backup_plan'].'</label><br />
				</div>
				<div class="col-sm-10">
					<br /><select class="form-control chosen" name="bpid">
					<option value="0" '.($bpid == 0 ? 'selected="selected"' : '').'>'.$l['none'].'</option>
					<option value="-1" '.($bpid == -1 ? 'selected="selected"' : '').'>'.$l['same_as_vps_plan'].'</option>';
					
					foreach($backup_plans as $k => $v){
						echo '<option value="'.$k.'" '.($bpid == $k ? 'selected="selected"' : '').'>'.$v['plan_name'].'</option>';
					}
				
				echo '</select>
				</div>
			</div><br /><br />';
			
			}
			
			 echo '<div class="row" id="hostname_row">
				<div class="col-sm-12">
					<label class="control-label">'.$l['hostname'].'&nbsp;&nbsp;<a href="https://'.$globals ['HTTP_HOST'].':4083/'.$SESS['token_key'].'/index.php?#!svs='.$vps['vpsid'].'" target="_blank" title="'.$l['svs_adm_login'].'"><img src="'.$theme['images'].'admin/svs_login.gif"" title="'.$l['svs_admin_login'].'"/></a></label><br /><br />
				</div>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="hostname" id="hostname" size="30" value="'.POSTval('hostname', $vps['hostname']).'" />
				</div>
			</div><br /><br />

			<div class="row" id="rootpass_row">
				<div class="col-sm-12">
					<label class="control-label">'.$l['rootpass'].'</label><br /><br />
				</div>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="rootpass" id="rootpass" size="30" onkeyup="check_pass_strength(\'rootpass\');" value="'.POSTval('rootpass', '').'" /><div id="rootpass_pass-strength-result" class="">'.$l['strength_indicator'].'</div>
				</div>
				<div class="col-sm-2">
					<a href="javascript: void(0);" onclick="$_(\'rootpass\').value=randstr(12, 1, '.(!empty($globals['pass_strength']) ? $globals['pass_strength'] : 0).');check_pass_strength(\'rootpass\');return false;" title="'.$l['randpass'].'"><img src="'.$theme['images'].'randpass.gif" /></a>
				</div>
			</div><br /><br />

			<div class="row" id="ipv4_row">
				<div class="col-sm-12">
					<label class="control-label">'.$l['ip'].'</label><br /><br />
				</div>
				<div class="col-sm-6">
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td valign="top">
							<table cellpadding="3" cellspacing="0" class="" id="iptable" style="width:100%;">';
					 
							$_ips = @(empty($_POST['ips']) ? $vps['ips'] : $_POST['ips']);
							
							if(is_array($_ips) && !empty($_ips)){
								foreach($_ips as $k => $ip){
									if(empty($ip)){
										unset($_ips[$k]);
									}
								}
							}
							
							if(empty($_ips)){
								$_ips = array(NULL);
							}
						 
							foreach($_ips as $ip){
								echo '<tr>
									<td>
										<input type="text" class="form-control ips" style="width:80%;float:left;" name="ips[]" value="'.$ip.'" onblur="checkippool();" size="15" />
										<a class="delip" title="'.$l['rem_from_ips'].'"><img style="padding-top:10px;" src="'.$theme['images'].'admin/delete.png" width="12" /></a>
										<br/><span class="help-block">&nbsp;</span>
									</td>
								</tr>';
							}
							
						echo '</table>
							
							<input type="button" class="go_btn" onclick="javascript:addrow(\'iptable\');" value="'.$l['add_ip'].'"/>
						</td>
					</tr>
					</table>
					<span class="help-block">&nbsp;</span>
				</div>
				<div class="col-sm-6">
					<div id="iplist_parent">
						<select class="form-control" id="iplist" size="5" multiple="multiple">';
						foreach($ips as $k => $v){	 
							echo '<option value="'.$k.'">'.$v['ip'].'</option>';
						}		
						echo'</select><br /><br />
						<a href="javascript:addtoips(\'iptable\');" class="go_btn">'.$l['add_to_ips'].'</a>
						<br />
					</div>
				</div>
				<span class="help-block">&nbsp;</span>
			</div><br /><br />';

			if(!empty($ips6_subnet) || !empty($vps['ips6_subnet'])){

			echo '<div class="row" id="ips6_subnet_row">
				<div class="col-sm-12">
					<label class="control-label">'.$l['ips6_subnet'].'</label><br /><br />
				</div>
				<div class="col-sm-10">';	
				// Then the VPS IPs
				foreach($vps['ips6_subnet'] as $k => $v){
					$tmp = explode('/', $v);
					$_ipv6_subnet[$k] = array('ip' => $tmp[0], 'ipr_netmask' => $tmp[1]);
				}
				
				// Then the extra IPs
				foreach($ips6_subnet as $k => $v){
					$_ipv6_subnet[$k] = array('ip' => $v['ip'], 'ipr_netmask' => $v['ipr_netmask']);
				}
				
				// This is to put the Posted IPs first
				if(!empty($_POST['ipv6_subnet'])){

					$posted_subnet = array();
					
					foreach($_POST['ipv6_subnet'] as $k => $v){
						//$_ipv6_subnet[$v] = $v;
						
						foreach($_ipv6_subnet as $_k => $_v){
							if($v == $_v['ip']){
								$posted_subnet[$_k] = $_k;
								$__ipv6_subnet[$_k] = $_ipv6_subnet[$_k];
							}
						}
									
					}
					
				}
				
				// Then the remaining IPs
				foreach($_ipv6_subnet as $k => $v){
					$__ipv6_subnet[$k] = $v;
				}
				
				echo '<select id="ipv6_subnet" class="form-control" name="ipv6_subnet[]" size="5" multiple="multiple" style="font-size:10px">';
				foreach($__ipv6_subnet as $k => $v){
					echo '<option value="'.$v['ip'].'" '.(isset($_POST['editvps']) ? (!empty($posted_subnet[$k]) ? 'selected="selected"' : '') : (!empty($vps['ips6_subnet'][$k]) ? 'selected="selected"' : '')).'>'.$v['ip'].' / '.$v['ipr_netmask'].'</option>';
				}		
				echo'</select>
				<span class="help-block">&nbsp;</span>
				</div>
			</div><br /><br />';

			}

			// IPv6
			if(!empty($ips6) || !empty($vps['ips6'])){

			echo '<div class="row" id="ipv6_row">
				<div class="col-sm-12">
					<label class="control-label">'.$l['ips6'].'</label><br /><br />
				</div>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="ipv6count" id="ipv6count" size="10" onblur="ipchoose(this.name, this.value)" /><br />
				</div>
				<div class="col-sm-10">
					<select id="ipv6" class="form-control" name="ipv6[]" size="5" multiple="multiple" onchange="countipv6(\'ipv6count\');" style="font-size:10px">';
					
					$_ipv6 = array();
					
					// This is to put the Posted IPs first
					if(!empty($_POST['ipv6'])){
						foreach($_POST['ipv6'] as $k => $v){
							$_ipv6[$v] = $v;
						}
					}
					
					// Then the VPS IPs
					foreach($vps['ips6'] as $k => $v){
						$_ipv6[$v] = $v;
					}
					
					// Then the extra IPs
					foreach($ips6 as $k => $v){
						$_ipv6[$v['ip']] = $v['ip'];
					}
					
					$_ipv6 = array_unique($_ipv6);
					
					foreach($_ipv6 as $k => $v){	 
						echo '<option value="'.$v.'" '.(isset($_POST['editvps']) ? (@in_array($v, $_POST['ipv6']) ? 'selected="selected"' : '') : (@in_array($v, $vps['ips6']) ? 'selected="selected"' : '')).'>'.$v.'</option>';
					}
						
					echo '</select>
					<span class="help-block">&nbsp;</span>
				</div>
			</div><br /><br />';

			}

			echo '

<style>
.uuid{
font-size: 11px;
}
</style>';	
			if($bus_driver){
			echo '<div class="row">
					<div class="col-sm-12">
						<label class="control-label">'.$l['hdd'].'</label><br /><br />
					</div>
					<div class="col-sm-12">
						<table name="storages" id="storages" cellpadding="3" cellspacing="" class="" style="border-collapse: collapse;">';
							
							// Find the disk information 
							if(empty($disks)){
								
								if(!empty($vps['disks'])){
									$disks = $vps['disks'];
								}else{
									$disks[0]['size'] = $vps['space'];
									$disks[0]['st_uuid'] = $storages[$stid]['st_uuid'];
								}
							}
							
							if($vps['virt'] == 'xen' && empty($vps['hvm'])){
								unset($disks[1]);
							}
							
							foreach($disks as $k => $v){
								echo '<tr>
									<td width="20%">
										<select class="form-control bus_driver" id = "bus_driver" name="bus_driver[]" disabled="disabled" style="width: 90%;">';
											
										echo '<option value="'.$v['bus_driver'].'">'.strtoupper($v['bus_driver']).'</option>';
												
									echo '</select>
									</td>
									<td width="15%">
										<select class="form-control bus_driver_num" id = "bus_driver_num" name="bus_driver_num[]" disabled="disabled" style="width: 80%;">';
											
										echo '<option value="'.$v['bus_driver_num'].'">'.$v['bus_driver_num'].'</option>';
												
									echo '</select>
									</td>
									<td width="15%">
										<input type="text" id="hdd_'.$k.'" class="form-control size" name="tmp_space[]" size="15" value="'.$v['size'].'" style="width: 90%;"/>
									</td>
									<td width="15%">
										'.$l['space_gb'].' '.($v['disk_uuid'] ? '<img class="wiki_help" title="Disk UUID : '.$v['disk_uuid'].'" src="'.$theme['images'].'admin/information.gif" /> ': '').'
									</td>
									<td width="35%">
										<select class="form-control storages_list" name="storages[]" id="storages'.$k.'" disabled="disabled" style="width: 85%;float: left;">';
										
											foreach($storages as $sk => $sv){
												echo '<option value="'.$sv['st_uuid'].'" '.($sv['st_uuid'] == $v['st_uuid'] ? 'selected="selected"' : '').'>'.$sv['name'].'&nbsp;('.$sv['disk_space'].'GB '.(empty($sv['oversell']) ? $l['free'] : $l['oversell_free']).')</option>';
											}
										echo '</select>&nbsp;'.virthelp('No_Space').'&nbsp;&nbsp;'.($k != 0 ? '<a class="delstorage" title="'.$l['rem_storage'].'"><img  src="'.$theme['images'].'admin/delete.png" width="12" /></a> ' : '').
										'<input class="disk_uuids" type="hidden" name="disk_uuids[]" value="'.$v['disk_uuid'].'">
									</td>
								</tr>';
							}
							
						echo '</table><input type="hidden" id="hdd" name="space" />
						<br />
						<a id="add_storage" href="javascript:add_storage_bus(\'storages\');" class="go_btn">'.$l['add_ip'].'</a>
					</div>
				</div><br /><br />';
			}else{
			echo '<div class="row">
					<div class="col-sm-12">
						<label class="control-label">'.$l['hdd'].'</label><br /><br />
					</div>
					<div class="col-sm-12">
						<table name="storages" id="storages" cellpadding="3" cellspacing="" class="" style="border-collapse: collapse;">';
							
							// Find the disk information 
							if(empty($disks)){
								
								if(!empty($vps['disks'])){
									$disks = $vps['disks'];
								}else{
									$disks[0]['size'] = $vps['space'];
									$disks[0]['st_uuid'] = $storages[$stid]['st_uuid'];
								}
							}
							
							if($vps['virt'] == 'xen' && empty($vps['hvm'])){
								unset($disks[1]);
							}
							
							foreach($disks as $k => $v){
								echo '<tr>
									<td width="20%">
										<input type="text" id="hdd_'.$k.'" class="form-control size" name="tmp_space[]" size="15" value="'.$v['size'].'" />
									</td>
									<td width="30%">
										&nbsp;'.$l['space_gb'].' '.($v['disk_uuid'] ? '(<a class="uuid" href="javascript:void(0)" title="Disk UUID : '.$v['disk_uuid'].'">'.$v['disk_uuid'].'</a>)': '').'
									</td>
									<td>
										<select class="form-control storages_list" name="storages[]" id="storages'.$k.'" disabled="disabled" style="width: 85%;float: left;">';
										
											foreach($storages as $sk => $sv){
												echo '<option value="'.$sv['st_uuid'].'" '.($sv['st_uuid'] == $v['st_uuid'] ? 'selected="selected"' : '').'>'.$sv['name'].'&nbsp;('.$sv['disk_space'].'GB '.(empty($sv['oversell']) ? $l['free'] : $l['oversell_free']).')</option>';
											}
										echo '</select>&nbsp;'.virthelp('No_Space').'&nbsp;&nbsp;'.($k != 0 ? '<a class="delstorage" title="'.$l['rem_storage'].'"><img  src="'.$theme['images'].'admin/delete.png" width="12" /></a> ' : '').
										'<input class="disk_uuids" type="hidden" name="disk_uuids[]" value="'.$v['disk_uuid'].'">
									</td>
								</tr>';
							}
							
						echo '</table><input type="hidden" id="hdd" name="space" />
						<br />
						<a id="add_storage" href="javascript:add_storage(\'storages\');" class="go_btn">'.$l['add_ip'].'</a>
					</div>
				</div><br /><br />';	
			}

			echo '
			<br />
			<div class="row">
				<div class="col-sm-12">
					<label class="control-label">'.$l['gram'].'</label>&nbsp;<img class="wiki_help" title="'.$l['exp_gram'].'" src="'.$theme['images'].'admin/information.gif" /><br /><br />
				</div>
				<div class="col-sm-6">
					<input type="text" class="form-control" name="ram" id="gram" size="30" value="'.POSTval('ram', $vps['ram']).'" />
				</div>
				<div class="col-sm-6" style="padding-top:8px" >
					'.$l['ram_mb'].' (<label class="control-label">'.(empty($resources['overcommit']) ? $resources['ram'] : ($resources['overcommit'] - $servers[$globals['server']]['alloc_ram'])).' MB</span> '.(empty($resources['overcommit']) ? $l['free'] : $l['overcomit_free']).')
				</div>
			</div><br /><br />';

			if($vps['virt'] == 'openvz' || $vps['virt'] == 'proxo'){

				echo '<div class="row" id="burst">
					<div class="col-sm-12">
						<label class="control-label">'.$l['bram'].'</label>&nbsp;<img class="wiki_help" title="'.$l['exp_bram'].'" src="'.$theme['images'].'admin/information.gif" /><br /><br />
					</div>
					<div class="col-sm-6">
						<input type="text" class="form-control" name="burst" id="bram" size="30" value="'.POSTval('burst', $vps['burst']).'" />
					</div>
					<div class="col-sm-6" style="padding-top:8px">
						'.$l['ram_mb'].'
					</div>
				</div><br /><br />';

			}
			
			if($vps['virt'] != 'openvz' && $vps['virt'] != 'proxo'){

				echo '<div class="row" id="swappedram">
					<div class="col-sm-12">
						<label class="control-label">'.$l['swap'].'</span><br /><br />
					</div>
					<div class="col-sm-6">
						<input type="text" class="form-control" name="swapram" id="swap" size="30" value="'.POSTval('swapram', $vps['swap']).'" />
					</div>
					<div class="col-sm-6" style="padding-top:8px">
						'.$l['ram_mb'].'
					</div>
				</div><br /><br />';

			}

			echo '
			<div class="row">
				<div class="col-sm-12">
					<label class="control-label">'.$l['cpunit'].'</label><br /><a href="'.$globals['docs'].'Creating_A_VPS#CPU_Parameters" target="_blank">'.$l['need_info'].'</a><br /><br />
				</div>
				<div class="col-sm-6">
					<input type="text" class="form-control" name="cpu" id="cpunit" size="30" value="'.POSTval('cpu', $vps['cpu']).'" />
				</div>
				<div class="col-sm-6" style="padding-top:8px">
					'.$l['units'].'
				</div>
			</div><br /><br />

			<div class="row">
				<div class="col-sm-12">
					<label class="control-label">'.$l['cores'].'</label><br />
					<a href="'.$globals['docs'].'Creating_A_VPS#CPU_Parameters" target="_blank">'.$l['need_info'].'</a><br /><br />
				</div>
				<div class="col-sm-6">
					<input type="text" class="form-control" name="cores" id="cores" size="30" value="'.POSTval('cores', $vps['cores']).'" />
				</div>
				<div class="col-sm-6"></div>
			</div><br /><br />';
			
			if($vps['virt'] != 'lxc'){
				echo '
			<div class="row" id="cpupercent">
				<div class="col-sm-12">
					<label class="control-label">'.$l['percent'].'</label><br /><a href="'.$globals['docs'].'Creating_A_VPS#CPU_Parameters" target="_blank">'.$l['need_info'].'</a><br /><br />
				</div>
				<div class="col-sm-6">
					<input type="text" class="form-control" name="cpu_percent" id="percent" size="30" value="'.POSTval('cpu_percent', intval($vps['cpu_percent'])).'" />
				</div>
				<div style="padding-top:8px" class="col-sm-6">%</div>
			</div><br /><br />';
			}

			if($kernel->features('io_priority', $vps['_virt'])){		
					
				echo '<div class="row">
					<div class="col-sm-12">
						<label class="control-label">'.$l['io'].'</label>&nbsp;<img class="wiki_help" title="'.$l['io0-7'].'" src="'.$theme['images'].'admin/information.gif" /><br /><br />
					</div>
					<div class="col-sm-6">
						<select id="prior" name="priority" class="form-control">
							<option value="0">0</option>
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
							<option value="6">6</option>
							<option value="7">7</option>
						</select>
					</div>
				</div><br /><br />';
			}

			if($vps['virt'] == 'openvz'){

				if(!distro_check(1)){
					echo '<div class="row">
						<div class="col-sm-6">
							<label class="control-label">'.$l['ploop'].'</label>
							<span class="help-block">'.$l['ploop_exp'].'</span><br />
						</div>
						<div class="col-sm-6">
							<input type="checkbox" class="ios" name="ploop" '.POSTchecked('ploop', $vps['ploop']).' '.(!empty($vps['ploop']) ? 'disabled=disabled' : '').' value="1"/>
						</div>
					</div><br />';
				}
				echo '<div class="row" id="ubc">
					<div class="col-sm-6">
						<label class="control-label">'.$l['ubcsettings'].'</label>
						<span class="help-block">'.$l['exp_ubc'].'</span><br />
					</div>
					<div class="col-sm-6">
						<input type="checkbox" class="ios" name="ubcsettings" '.POSTchecked('ubcsettings').' value="1"/>
						<a href="'.$globals['index'].'act=ubc&vpsid='.$vps['vpsid'].'">'.$l['ubcsettings'].'</a>
					</div>
				</div><br />';

			}

			if($vps['virt'] == 'xen' && !empty($vps['hvm'])){
					
				echo '<div class="row">
					<div class="col-sm-12">
						<label class="control-label">'.$l['shadow'].'</label>&nbsp;<img class="wiki_help" title="'.$l['shadow_exp'].'" src="'.$theme['images'].'admin/information.gif" /><br /><br />
					</div>
					<div class="col-sm-6">
						<input type="text" class="form-control" name="shadow" id="shadow" size="30" value="'.POSTval('shadow', $vps['shadow']).'" />
					</div>
					<div style="padding-top:8px" class="col-sm-6">MB</div>
					
					
				</div><br /><br />';

			}

			if($kernel->features('vnc_support', $vps['_virt'])){

				$vncpasslen = $kernel->features('vncpasslen', $virt);
				echo '
				<br/>
				<div class="row">
					<div class="col-sm-6">
						<label class="control-label">VNC</label>
					</div>
					<div class="col-sm-6">
						<input type="checkbox" class="ios" name="vnc" id="vnc" '.POSTchecked('vnc', $vps['vnc']).' onchange="checkvnc();" value="1"/>
					</div>
				</div><br /><br />
				<div class="row" id="vncpassrow">
					<div class="col-sm-12">
						<label class="control-label">'.$l['vncpass'].'</label>&nbsp;<img class="wiki_help" title="'.$l['vncpass_exp'].'" src="'.$theme['images'].'admin/information.gif" /><br /><br />
					</div>
					<div class="col-sm-6">
						<input type="text" class="form-control" name="vncpass" id="vncpass" size="30" value="'.POSTval('vncpass', '').'" />
					</div>
					<div class="col-sm-6">
						<a href="javascript: void(0);" onclick="$_(\'vncpass\').value=randstr('.$vncpasslen.');return false;" title="'.$l['vncpass'].'"><img src="'.$theme['images'].'vncpass.gif" /></a>
					</div>
				</div><br /><br />';
			}
			
			if($kernel->features('ha', $vps['_virt']) && array_key_exists($globals['server'], $ha_enabled)){
				echo '<div class="row hide_for_plan">
						<div class="col-sm-6">
							<label class="control-label">'.$l['ha'].'</label>
							<span class="help-block">'.$l['ha_exp'].'</span>
						</div>
						<div class="col-sm-6">
							<input type="checkbox" class="ios" id="ha" name="ha" '.POSTchecked('ha', $vps['ha']).' value="1" disable="disable"/>
						</div>
				</div><br /><br />';
			}
			
		echo '<br /><br />
		</div>
	</div>
</div><br />
<center><input id="editvps" type="submit" name="editvps" value="'.$l['submit'].'" class="btn"/></center>

<!-- <div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; <a href="'.$globals['ind'].'act=oldeditvs&vpsid='.$vps['vpsid'].'" >'.$l['old_editvs_note'].'</a></div> -->

<br /><br />
</form>
</div>
</div>
</div>
';
}// End of Server Offline if
softfooter();
echo '
<div id="processing_symb"><img src="'.$theme['images'].'loader.gif" /></div>
';
}

?>