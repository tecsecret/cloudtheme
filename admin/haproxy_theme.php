<?php

//////////////////////////////////////////////////////////////
//===========================================================
// haproxy.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Abhijeet
// Date:       10th Feb 2017
// Time:       16:30 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function haproxy_theme(){

global $theme, $globals, $servergroups, $servers, $supported_protocols, $haproxydata, $vpses, $l, $error, $done, $server_haconfigs;

softheader($l['<haproxy>']);
echo '
<style>

</style>
';

echo '<link rel="stylesheet" type="text/css" href="'.$theme['url'].'/css2/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="'.$theme['url'].'/js2/jquery.dataTables.min.js"></script>';

echo '
<script>';

// Parse and include haproxy.js file
echo parse_lang(file_get_contents($theme['url'].'/js/haproxy.js'));

echo '
	
	var $server_haconfigs = '.json_encode($server_haconfigs).';
	var $haproxydata = '.json_encode($haproxydata).';
	var $servers = '.json_encode($servers).';
	var $vpses = '.json_encode($vpses).';
	var $supported_protocols = '.json_encode($supported_protocols).';
	var $l = '.json_encode($l).';
	
	var $edit = null; 
	
	var is_admin = 1;
	vdf_edit_ico = "fa icon-edit";
	vdf_save_ico = "fa icon-save";
	vdf_delete_ico = "fa icon-delete";
	vdf_revert_ico = "fa icon-undo";
	arr_haproxy_src_ips = '.json_encode($arr_haproxy_src_ips).';
	
	var vdf_url = window.location.origin+window.location.pathname+"?act=haproxy&api=json"+(window.location.search.includes("debug=died") ? "&debug=died" : "");
	
	$(document).ready(function(){
		update_record_alerts();
	});
	
	function ajaxprocess(self, postdata, success, fail, showprocess){
		
		var url_path = window.location.origin+window.location.pathname+"?act=";
		
		//Show processing
		if(showprocess != undefined){
			var progressbar = "<img style=\"width: 20px; height: 20px;\" src=\"'.$theme['images'].'progress.gif\" />";
			var oldhtml = $(self).html();
			var oldclass = $(self).attr("class");
			$(self).attr("class","");
			$(self).html(progressbar);
		}
		
		$.ajax({
			type: "post",
			url: url_path+"haproxy&api=json"+(window.location.search.includes("debug=died") ? "&debug=died" : ""),
			data: postdata,
			dataType : "json",
			success: function (data){
				//Remove processing
				if(showprocess != undefined){
					$(self).html(oldhtml);
					$(self).attr("class", oldclass);
				}
				eval(success)(self, data);
				
			},
			error: function(data){
				//Remove processing
				if(showprocess != undefined){
					$(self).html(oldhtml);
					$(self).attr("class", oldclass);
				}
				
				var reg_pat = /could\snot\smake\sthe\squery/i;
				var alert_str = (reg_pat.test(data.responseText) == true ? $l["db_query_error"] : $l["err_unknown"]);
				alert(alert_str);
				eval(fail)(self, data);
			}
		});
	}
	
	// Script to Manage HAProxy Servers
	function showhaservers_page(){
		$("#showhaserversbtn").attr("id", "showvdfbtn");
		$("#showvdfbtn").attr("onclick", "showvdf_page()");
		$("#showvdfbtn").html($l["list_records"]);
		$("#vdf_search_link").hide();
		$("#showvdfformbtn").hide();
		$("#showsearch").hide();
		
		$("#domain_forwarders_div").hide();
		$("#haservers_div").show();
		show_ha_servers_tbl();
	}
	
	function showvdf_page(){
		$("#showvdfbtn").attr("id", "showhaserversbtn");
		$("#showhaserversbtn").attr("onclick", "showhaservers_page()");
		$("#showhaserversbtn").html("Servers");
		$("#vdf_search_link").show();
		$("#showvdfformbtn").show();
		
		$("#domain_forwarders_div").show();
		$("#haservers_div").hide();
		$("#haservers_div").html("");
		
	}
	$(document).ready(function(){
		var server_btn = "&nbsp;<button id=\"showhaserversbtn\" class=\"btn\" onclick=\"showhaservers_page()\" >"+$l["servers"]+"</button>";
		$("#showvdfformbtn").parent().append(server_btn);
	});
	
	function show_ha_servers_tbl(){
		$str = "";
		$str += "<div class=\"row\"> \
			<div class=\"col-sm-12\">\
				<table class=\"table table-hover table-responsive\" id=\"ha_servers_tbl\">\
					<thead>\
						<tr>\
							<th style=\"width: 20px;\">'.$l['id'].'</th>\
							<th style=\"width: 350px;\">'.$l['serid'].'</th>\
							<th style=\"width: 50px;\">'.$l['enabled'].'</th>\
							<th style=\"width: 50px;\">'.$l['status'].'</th>\
							<th style=\"width: 150px;\">'.$l['actions'].'</th>\
						</tr>\
					</thead>\
					<tbody></tbody>\
					<tfoot></tfoot>\
				</table>\
			</div>\
		</div><br />"
		$("#haservers_div").html($str);
		
		ajaxprocess("", "server_page=1", populate_ha_servers_tbl, populate_ha_servers_tbl);
		
		$("#ha_servers_tbl").after("<div class=\"row wait_img\"><div class=\"col-sm-12 text-center\"><img src=\"'.$theme['images'].'bar.gif\" style=\"width: 200px;\"/></div></div>");
	}
	
	function populate_ha_servers_tbl(self ,ajaxdata){
		
		if(ajaxdata != undefined && ajaxdata.servers != undefined){
			$servers = (!!ajaxdata.servers ? ajaxdata.servers : {});
		}
		
		$("#haservers_div .wait_img").remove();
		var strdata = "";
		$.each($servers, function(i,t){
			var status = "--";
			if(t["haproxy_status"] == "1"){
				status = $l["error_state"];
			}
			if(t["haproxy_status"] == "0"){
				status = $l["running"];
			}
			if(t["haproxy_status"] == "3"){
				status = $l["stopped"];
			}
			
			strdata += "\
			<tr>\
				<td class=\"text-center\">"+t["serid"]+"</td> \
				<td class=\"text-center\">"+t["server_name"]+"</td> \
				<td class=\"text-center\">"+(t["haproxy_enable"] ? $l["yes"] : $l["no"])+"</td> \
				<td class=\"text-center\">"+status+"</td> \
				<td class=\"text-right ha_servers_actions\" id=\""+t["serid"]+"_server_actions\">\
					<span ><button class=\"btn\" data-toggle=\"tooltip\" data-placement=\"auto\" title=\""+$l["tooltip_update_server_src_ips"]+"\" onclick=\"show_update_server_src_ips_form(this);\" >"+$l["map_ips"]+"</button></span> \
					<span ><button class=\"btn\" style=\"background-color: #55A555;\" data-toggle=\"tooltip\" data-placement=\"auto\" title=\""+$l["tooltip_reload_haproxy_service"]+"\" onclick=\"reload_haproxy_service(this);\" >"+$l["service_reload"]+"</button></span> \
				</td> \
			</tr>\
			";
		});
		$("#ha_servers_tbl tbody").eq(0).html(strdata);
	}
	
	function reload_haproxy_service(self, ajaxdata){
		var serid = $(self).parents().eq(1).attr("id").split("_")[0];
		
		if(ajaxdata == undefined){
			ajaxprocess(self, "server_page=1&reload=1&serid="+encodeURIComponent(serid), reload_haproxy_service, reload_haproxy_service, 1);
			return;
		}else{
			if(ajaxdata.done != undefined && ajaxdata.done.success != undefined){
				alert($l["success_reload_ha_service"]);
				populate_ha_servers_tbl("", ajaxdata);
				
			}else if(ajaxdata.error != undefined){
				var str = "";
				$.each(ajaxdata.error, function(i,t){
					str += t+"\n";
				});
				alert($l["errors"]+":\n"+str);
			}else{
				alert($l["err_reload_ha_service"]);
			}
		}
	}
	function show_update_server_src_ips_form(self, ajaxdata){
		var serid = $(self).parents().eq(1).attr("id").split("_")[0];
		
		if(ajaxdata == undefined){
			ajaxprocess(self, "server_page=1&get_server_old_src_ips=1&serid="+encodeURIComponent(serid)+"", show_update_server_src_ips_form, show_update_server_src_ips_form, 1)
			return true;
		}else{
			if(ajaxdata.done != undefined){
				if(ajaxdata.server_old_src_ips == undefined || ajaxdata.server_old_src_ips.length == 0){
					alert($l["err_no_old_src_ips"]);
					return;
				}
				$str = "<div class=\"row\" id=\"update_server_src_ips_form_div\">";
				$str += "<div class=\"row head2\"><div class=\"cols-sm-12\" ><span>"+$servers[serid]["server_name"]+"</span></div></div><hr />";
				$str += "<div class=\"row head3\"><div class=\"col-sm-6\"><span>"+$l["head_old_src_ips"]+"</span></div><div class=\"col-sm-6\"><span>"+$l["head_new_src_ips"]+"</span></div></div><hr />";
				$str += "<form id=\"update_server_src_ips_form\" name=\"update_server_src_ips_form\" action=\"\" onsubmit=\"return false;\" >";
				$.each(ajaxdata.server_old_src_ips[serid], function(i, t){
					$str += "<div class=\"row form-group\"><div class=\"col-sm-6\"><label>"+t+"</label></div><div class=\"col-sm-6\"><select class=\"form-control\" name=\"src_ips["+t+"]\">";
						$str += "<option value=\"-1\" >"+$l["vdf_select_ip"]+"</option>";
						$.each($servers[serid]["arr_haproxy_src_ips"], function(k, v){
							$str += "<option value=\""+v+"\" >"+v+"</option>";
						});
					$str += "</select></div></div>";
				});
				$str += "<input type=\"hidden\" name=\"serid\" value=\""+serid+"\" >";
				$str += "<div class=\"row\"><div class=\"col-xs-12 text-center\"><span onclick=\"update_server_src_ips(this)\" ><button class=\"btn btn-sm\" >"+$l["update"]+"</button></span></div></div>";
				$str += "<div>";
				$("#haproxymodal1 .modal-body").eq(0).html($str);
				$("#haproxymodal1").modal({show:true});				
			}else if(ajaxdata.error != undefined){
				var str = "";
				$.each(ajaxdata.error, function(i,t){
					str += t+"\n";
				});
				alert($l["errors"]+":\n"+str);
			}else{
				alert($l["err_request"]);
				return false;
			}
		}
	}
	
	function update_server_src_ips(self, ajaxdata){
		
		if(ajaxdata == undefined){
			
			var update_confirm = confirm($l["confirm_update_src_ips"]);
			
			if(!update_confirm){
				return false;
			}
			
			var postdata = "";
			postdata += "server_page=1&update_server_src_ips=1&"+$("#update_server_src_ips_form").serialize();
			
			ajaxprocess(self, postdata, update_server_src_ips, update_server_src_ips, 1)
			
			return true;
			
		}else{
			
			if(ajaxdata.done != undefined && ajaxdata.done.success != undefined){
				$haproxydata = (!!ajaxdata.haproxydata ? ajaxdata.haproxydata : {});
				alert($l["vdf_success_update"]);
				$("#haproxymodal1").modal("hide");
				
			}else if(ajaxdata.error != undefined){
				
				var str = "";
				$.each(ajaxdata.error, function(i,t){
					str += t+"\n";
				});
				
				alert($l["errors"]+":\n"+str);
				
			}else{
				
				alert($l["err_request"]);
				
				return false;
			}
		}
	}
</script>

<!-- Update server\'s source IPs form modal -->
<div class="modal fade" id="haproxymodal1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header head_bar">
				<div class="row">
					<div class="col-xs-11">
						<span class="modal_title"><span>'.$l["heading_update_old_src_ips"].'</span></span>
					</div>
					<div class="col-xs-1 close text-center" data-dismiss="modal">&times;</div>
				</div>
			</div>
			<div class="modal-body"></div>
		</div>
	</div>
</div>
<div class="bg">
<center class="tit">
<i class="icon icon-firewall2 icon-head"></i>&nbsp; '.$l['title_haproxy'].'<span style="float:right"  ><span onclick="$(\'#vdf_infobox\').toggle();"><i class="icon-info" style="padding: 0px; margin: 0px 5px; font-size: 25px; cursor: pointer;" ></i></span><span id="vdf_search_link" onclick="showsearch();"><i class="icon-search" style="padding: 0px; margin: 0px 5px; font-size: 25px; cursor: pointer;" ></i></span><a href="'.$globals['docs'].'Manage_VPS_Domain_Forwarding" target="_blank" class="wiki_help" title="'.$l['wiki_help'].'"><i class="icon-help" ></i></a></span></center>';
echo '
<div id="vdf_infobox">
	<div class="heading">'.$l['vdf_info'].'</div>
	<div class="body">
		<div class="scrollbar-virt">
			<div style="height: 150px;">';
			foreach($server_haconfigs as $k => $v){
				echo '<div>
					<b>'.$l['vdf_info_server'].':</b> '.$servers[$k]["server_name"].'<br/>
					<b>'.$l['vdf_info_reservedports'].':</b> '.$v["haproxy_reservedports"].'<br/>
					<b>'.$l['vdf_info_reservedports_http'].':</b> '.$v["haproxy_reservedports_http"].'<br/>
					<b>'.$l['vdf_info_allowedports'].':</b> '.$v["haproxy_allowedports"].'<br/>
					</div><br/>
				';
			}
		echo '</div>
		</div>
	</div>
</div>';
//-------------- Search VDF Block Starts ---------------------
echo '<div id="showsearch" style="display:'.(optREQ('haproxysearch') || (!empty($haproxydata) && !empty($globals['showsearch'])) ? "" : "none").';">
<form accept-charset="'.$globals ['charset'].'" name="haproxysearchform" method="get" action="" id="haproxysearchform" class="form-horizontal">
<input type="hidden" name="act" value="haproxy">
<div class="form-group_head">
	<div class="row">
	    <div class="col-sm-3">
			<div class="col-xs-12"><label>'.$l['id'].'</label></div>
			<div class="col-xs-12"><input class="form-control" type="text" name="s_id" id="s_id" size="15" value="'.REQval('s_id','').'"/></div>
		</div>
		<div class="col-sm-3">
			<div class="col-xs-12"><label>'.$l['vdf_serid'].'</label></div>
			<div class="col-xs-12 server-select-lg">
				<select name="s_serid" id="s_serid" class="form-control virt-select" style="width:100%">
					<option value="-1" '.(REQval('s_serid') == - 1 ? 'selected="selected"' : '').'>'.$l['select_server'].'</option>';
					foreach($servers as $key => $id) {
						echo '<option value="'.$key.'" '.(isset($_REQUEST ['s_serid']) && REQval('s_serid') == $key ? 'selected="selected"' : '').'>'.$id ['server_name'].'</option>';
					}
		
		echo 	'</select>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="col-xs-12"><label>'.$l['vdf_vpsid'].'</label></div>
			<div class="col-xs-12"><input class="form-control" type="text" name="s_vpsid" id="s_vpsuuid" size="15" value="'.REQval('s_vpsid','').'"/></div>
		</div>
		<div class="col-sm-3">
			<div class="col-xs-12"><label>'.$l['vdf_proto'].'</label></div>
			<div class="col-xs-12">
				<select name="s_protocol" id="s_protocol" class="form-control">
					<option value="-1" '.(REQval('s_protocol') == - 1 ? 'selected="selected"' : '').'>'.$l['vdf_sel_proto'].'</option>';
					foreach($supported_protocols as $key => $id) {
						echo '<option value="'.$id.'" '.(isset($_REQUEST ['s_protocol']) && REQval('s_protocol') == $id ? 'selected="selected"' : '').'>'.$id.'</option>';
					}
		
		echo 	'</select>
			</div>
		</div>
  	</div>
  	<div class="row">
		<div class="col-sm-3">
			<div class="col-xs-12"><label>'.$l['vdf_src_hname'].'</label></div>
			<div class="col-xs-12"><input type="text" class="form-control" name="s_src_hostname" id="s_src_hostname" size="25" value="'.REQval('s_src_hostname','').'"/></div>
	    </div>
		<div class="col-sm-3">
			<div class="col-xs-12"><label>'.$l['vdf_src_port'].'</label></div>
			<div class="col-xs-12"><input type="text" class="form-control" name="s_src_port" id="s_src_port" size="25" value="'.REQval('s_src_port','').'"/></div>
		</div>
		<div class="col-sm-3">
			<div class="col-xs-12"><label>'.$l['vdf_dest_ip'].'</label></div>
			<div class="col-xs-12"><input type="text" class="form-control" name="s_dest_ip" id="s_dest_ip" size="25" value="'.REQval('s_dest_ip','').'"/></div>
		</div>
		<div class="col-sm-3">
			<div class="col-xs-12"><label>'.$l['vdf_dest_port'].'</label></div>
			<div class="col-xs-12"><input type="text" class="form-control" name="s_dest_port" id="s_dest_port" size="25" value="'.REQval('s_dest_port','').'"/></div>
		</div>
  	</div>
	<div class="row">
		<div class="col-sm-12" style="text-align: center;"><button type="submit" name="haproxysearch" class="go_btn" value="1"/>'.$l['search'].'</button></div>
	</div>
</div>
</form>
<br/>			
</div>';
//-------------- Search VDF Block Ends ---------------------
error_handle($error);
if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
}
echo '<script>
	$(document).ready(function(){
		$("#domain_forwarders_div").css({"width":($("#softcontent").innerWidth())*0.93+"px", "margin-left":"1%", "margin-right":"auto"});
	});
</script>';
echo '<div class="row"><div class="col-sm-12 text-right"><button id="showvdfformbtn" class="btn text-center" onclick="showvdfform();">'.$l['new'].'</button></div></div>';
echo '
	<div class="row" id="addvdf_form_div" style="display: none;">
		<div class="row title"><div class="col-xs-12">'.$l['vdf_add_title'].'</div></div>
		<form name="addvdf" id="addvdf" method="post" action="" onsubmit="return false;">
			<div class="row">	
				<div class="col-sm-3 form-group">
					<div class="col-sm-12"><label class="control-label" for="serid">'.$l['vdf_serid'].'</label></div>
					<div class="col-sm-12">
						<select class="form-control chosen" id="serid" name="serid" onchange="processaddvdfform(this)" ></select>
					</div>
				</div>
				<div class="col-sm-3 form-group">
					<div class="col-sm-12"><label class="control-label" for="vpsuuid">'.$l['vdf_vpsid'].'</label></div>
					<div class="col-sm-12">
						<select class="form-control chosen" id="vpsuuid" name="vpsuuid" onchange="processaddvdfform(this)" ></select>
					</div>
				</div>
				<div class="col-sm-3 form-group">
					<div class="col-sm-12"><label class="control-label" for="protocol">'.$l['vdf_proto'].'</label></div>
					<div class="col-sm-12">
						<select class="form-control chosen" id="protocol" name="protocol" onchange="processaddvdfform(this)" ></select>
					</div>
				</div>
			</div>
			<div class="row">	
				<div class="col-sm-3 form-group">
					<div class="col-sm-12"><label class="control-label" for="src_hostname">'.$l['vdf_src_hname'].'</label></div>
					<div class="col-sm-12">
						<input type="text" class="form-control" id="src_hostname" name="src_hostname" />
					</div>
				</div>
				<div class="col-sm-3 form-group">
					<div class="col-sm-12"><label class="control-label" for="src_port">'.$l['vdf_src_port'].'</label></div>
					<div class="col-sm-12">
						<input type="text" class="form-control" id="src_port" name="src_port" />
					</div>
				</div>
				<div class="col-sm-3 form-group">
					<div class="col-sm-12"><label class="control-label" for="dest_ip">'.$l['vdf_dest_ip'].'</label></div>
					<div class="col-sm-12">
						<select class="form-control chosen" id="dest_ip" name="dest_ip" ></select>
					</div>
				</div>
				<div class="col-sm-3 form-group">
					<div class="col-sm-12"><label class="control-label" for="dest_port">'.$l['vdf_dest_port'].'</label></div>
					<div class="col-sm-12">
						<input type="text" class="form-control" id="dest_port" name="dest_port" />
					</div>
				</div>
			</div>
			<div class="row text-center">
				<button id="submitaddvdf" onclick="processaddvdfform(this);" class="btn text-center" name="submitaddvdf" >'.$l['add'].'</button> 
				<button class="btn text-center" onclick=\'$("#addvdf_form_div").css("display", "none"); $("#showvdfformbtn").parents().eq(1).show();\'>'.$l['close'].'</button>
				<input type="hidden" value="addvdf" name="action" />
			</div>	
		</form>
	</div><br />
	<div class="row" id="domain_forwarders_div">
	';
	page_links($globals['num_res'], $globals['cur_page'], $globals['reslen']);
	echo '<br />
		<div class="scrollbar-virt">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12">
						<table class="table table-hover" id="vps_forwarders_tbl">
							<thead>
								<tr>
									<th>'.$l['id'].'</th>
									<th>'.$l['vdf_vpsid'].'</th>
									<th>'.$l['vdf_serid'].'</th>
									<th>'.$l['vdf_proto'].'</th>
									<th>'.$l['vdf_src_hname'].'</th>
									<th width="80">'.$l['vdf_src_port'].'</th>
									<th>'.$l['vdf_dest_ip'].'</th>
									<th width="30">'.$l['vdf_dest_port'].'</th>
									<th style="width: 120px;">'.$l['actions'].'</th>
									<th><input type="checkbox" class="select_all" name="select_all" id="select_all"/></th>
								</tr>
							</thead>
							<tbody>';
							foreach($haproxydata as $k => $v){
								echo '<tr style="text-align:center;">
								<td><span id="'.$k.'_id"><span class="coldat">'.$v['id'].'</span></span></td>
								<td><span id="'.$k.'_vpsuuid"><span class="coldat">'.$v['vpsid'].' ('.$v['hostname'].')'.'</span></span></td>
								<td><span id="'.$k.'_serid"><span class="coldat">'.$servers[$v['serid']]['server_name'].'</span></span></td>
								<td><span id="'.$k.'_protocol"><span class="coldat">'.$v['protocol'].'</span></span></td>
								<td><span id="'.$k.'_src_hostname"><span class="coldat">'.$v['src_hostname'].'</span></span></td>
								<td><span id="'.$k.'_src_port"><span class="coldat">'.$v['src_port'].'</span></span></td>
								<td><span id="'.$k.'_dest_ip"><span class="coldat">'.$v['dest_ip'].'</span></span></td>
								<td><span id="'.$k.'_dest_port"><span class="coldat">'.$v['dest_port'].'</span></span></td>
								<td><span class="vdf_actions" id="'.$v['id'].'_vdf_actions"><span class="vdf_edit" id="'.$v['id'].'_edit"><i class="fa icon-edit" onclick="edit_row(this, '.$v['id'].')" data-toggle="tooltip" data-placement="auto" title="'.$l['vdf_tooltip_edit'].'"></i></span><span class="vdf_delete"><i class="fa icon-delete" onclick="vdf_confirm('.$v['id'].');" data-toggle="tooltip" data-placement="auto" title="'.$l['vdf_tooltip_delete'].'" ></i></span></span></td>
								<td><span id="'.$v['id'].'_checkbox"><input type="checkbox" class="ios" name="vdf_list[]" value="'.$v['id'].'"/></span></td>
								</tr>';
							}
							echo '
							</tbody>
							<tfoot></tfoot>
						</table>
					</div>
				</div>
			</div><br />
			<!--<div  id="vdf_tbl_div"></div>-->
		</div>';
		echo page_links($globals['num_res'], $globals['cur_page'], $globals['reslen']);
		echo '
		<div class="row bottom-menu">
			<div class="col-sm-7"></div>
			<div class="col-sm-5">
				<label>'.$l['with_selected'].'</label>
				<select name="haproxy_multiselect_action chosen" id="haproxy_multiselect_action" class="form-control">
					<option value="0">---</option>
					<option value="1">'.$l['delete'].'</option>
				</select>&nbsp;
				<input type="submit" id ="haproxy_task_submit" class="go_btn" name="haproxy_task_submit" value="'.$l['go'].'" onclick="vdf_confirm(); return false;">
			</div>
		</div>
	</div>
	<div class="row" id="haservers_div">
	</div>
';
echo '</div>'; // End of BG html class
softfooter();
echo '
<div id="processing_symb"><img src="'.$theme['images'].'loader.gif" /></div>
';
}

?>