<?php

//////////////////////////////////////////////////////////////
//===========================================================
// dashboard_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function adminindex_theme(){

global $theme, $globals, $cluster, $user, $l, $stats, $servers, $users;

$acl_cluster_statistics = is_allowed("cluster_statistics");
$acl_server_statistics = is_allowed("server_statistics");

softheader($l['<title>']);

echo '<script src="'.$theme['url'].'/js/jquery.easypiechart.min.js"></script>';

echo '<style type="text/css">
	.resource_gr {
		border-radius: 4px;
		color: #FFFFFF;
	}
	.resource_header {
		background-color: rgba(255, 255, 255, 0.2);
		border-radius: 4px 4px 0 0;
		font-weight: 300;
		padding: 8px;
		font-size: 16px;
		text-align: center;
	}
	.resource_content {
		font-size: 16px;
		padding: 14px 16px 8px 16px;
		text-align: center;
	}
	.resource_container {
		display: inline-block;
		text-align: center;
		height: 80px;
		width: 80px;
		position: relative;
	}
	.resource_container .text {
		line-height: 80px;
		height: 100%;
	}
	.resource_container .chart-2 {
		position: absolute;
		top: 0;
		left: 0;
	}
	.resource_value {
		font-size: 12px;
		margin-top: 8px;
	}
	
	#search-input {
		transition: all 0.5s;
		float: right;
		width: 100%;
	}
	#search-input.collapsed {
		width: 0;
		padding: 0;
		border: 0;
	}
	
	#cluster_stats [class*=col] {
		margin-bottom: 8px;
	}
	
	.dash {
		border-radius: 3px;
		height: 80px;
		position: relative;
	}
	.dash-text {
		position: absolute;
		left: 12px;
		top: 12px;
	}
	.dash-title {
		position: absolute;
		left: 12px;
		bottom: 12px;
	}
	.dash .badge {
		background-color:rgba(0,0,0,0.5);
		font-size: 16px;
		font-weight:700;
	}
	.dash i {
		position: absolute;
		right: 12px;
		top: 12px;
		font-size: 48px;
		opacity: 0.4;
	}
	.dash.compact .badge {
		font-size: 18px;
	}
	.dash.compact i {
		font-size: 36px;
	}
	.dash.compact .dash-title {
		font-size: 11px;
	}
	
	.server_gr {
		text-align: center;
	}
	.server_gr .title {
		font-size: 16px;
		font-weight: bold;
		margin: 20px 0 16px 0;
		text-transform: uppercase;
	}
	.server_gr .content {
		display: inline-block;
		position: relative;
		height: 140px;
		width: 140px;
	}
	.server_gr .content .text {
		display: table-cell;
		vertical-align: middle;
		width: 140px;
		height: 140px;
	}
	.server_gr .content .text div {
		margin-left: auto;
		margin-right: auto;
	}
	.server_gr .content .value {
		font-size: 16px;
	}
	.server_gr .chart {
		position: absolute;
		top: 0;
		left: 0;
		display: inline-block;
	}
	.server_gr .bottom-text {
		margin: 12px 0;
	}
	
	table.tool {
		width: 150px;
		margin: 4px;
	}
	table.tool td {
		padding: 4px;
	}
	
	.panel {
		min-height: 120px;
	}
	.panel-content {
		max-height: 200px;
		overflow: hidden;
	}
	.panel.panel-red {
		border-color: #EF5350;
	}
	.panel.panel-red .panel-heading {
		background-color: #EF5350;
		border-color: #EF5350;
		color: #fff;
	}
	.panel.panel-red1 .table-dash thead tr {
		background-color: #EF5350CC;
		color: #fff;
	}
	.panel.panel-blue {
		border-color: #3083FF;
	}
	.panel.panel-blue .panel-heading {
		background-color: #3083FF;
		border-color: #3083FF;
		color: #fff;
	}
	.panel.panel-blue1 .table-dash thead tr {
		background-color: #3083FFCC;
		color: #fff;
	}
	.panel.panel-blue-grey {
		border-color: #607D8B;
	}
	.panel.panel-blue-grey .panel-heading {
		background-color: #607D8B;
		border-color: #607D8B;
		color: #fff;
	}
	.panel.panel-blue-grey1 .table-dash thead tr {
		background-color: #607D8BCC;
		color: #fff;
	}
	
	.table-dash {
		margin-bottom: 4px;
	}
	.table-dash tbody tr td {
		vertical-align: middle;
	}
	.table-dash tr td:first-child, .table-dash tr th:first-child {
		padding-left: 16px;
	}
	.table-dash tr td:last-child, .table-dash tr th:last-child {
		padding-right: 16px;
	}
	
	.btn-logs {
		margin: 0;
		padding: 4px 8px;
		font-size: 12px;
	}
</style>';

echo '<div class="bg" style="width:99%">
<center class="tit"><i class="icon icon-vs icon-head"></i>&nbsp; '.$l['<title>'].'</center>
<br />

<div class="modal fade" role="dialog" id="logs_modal">
	<div class="modal-dialog" style="width: 60%; height: 50%;">
		<div class="modal-content">
			<div class="modal-header text-center" id="logs_modal_head">
				<button class="close" data-dismiss="modal">&times;</button>
				<span class="fhead">'.$l['logs'].'</span>
			</div>
			<div class="modal-body" id="logs_modal_body" ></div>
		</div>
	</div>
</div>';

//ACL Validation
if(!$acl_cluster_statistics && !$acl_server_statistics){
	$error[] = $l['err_acl_no_allowed'];
	error_handle($error);
	softfooter();
	return false;
}

echo '<br />';

//ACL Validation
if($acl_cluster_statistics){
	echo '
	<div class="roundheader">'.$l['cluster_stats'].'</div>
	<br />
	<div id="cluster_stats" class="row">
		
		<div class="col-sm-12 col-md-6 col-lg-3">
			<div class="row">
				<div class="col-xs-6" style="padding-right:4px;">
					<a href="'.$globals['ind'].'act=vs">
						<div id="dash_vps" class="dash compact" style="background-color:#3083FF; border:1px solid #3083FF; color:#FFF;">
							<div class="dash-text">
								<span class="badge">'.$stats['num_vps'].'</span>&nbsp;&nbsp;'._strtoupper($l['vps']).'
							</div>
							<div class="dash-title">'.$stats['num_vps_suspended'].'/'.$stats['num_vps'].' '.$l['suspended'].'</div>
							<i class="icon icon-vs"></i>
						</div>
					</a>
				</div>
				<div class="col-xs-6" style="padding-left:4px;">
					<a href="'.$globals['ind'].'act=servers">
						<div class="dash compact" style="background-color:#EF5350; border:1px solid #EF5350; color:#FFF;">
							<div class="dash-text">
								<span class="badge">'.$stats['servers'].'</span><br />
							</div>
							<div class="dash-title">'._strtoupper($l['servers']).'</div>
							<i class="icon icon-servers"></i>
						</div>
					</a>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<a href="'.$globals['ind'].'act=users">
						<div id="dash_users" class="dash" style="background-color:#00bca4; border:1px solid #00bca4; color:#FFF;">
							<div class="dash-text">
								<span class="badge">'.$stats['num_total_users'].'</span>&nbsp;&nbsp;'._strtoupper($l['users']).'
							</div>
							<div class="dash-title">'.$stats['num_suspended_users'].' / '.$stats['num_total_users'].' '.$l['suspended'].'</div>
							<i class="icon icon-users"></i>
						</div>
					</a>
				</div>
			</div>
		</div>
		<div class="col-sm-12 col-md-6 col-lg-3">
			<div class="row">
				<div class="col-sm-12">
					<a href="'.$globals['ind'].'act=ippool">
						<div class="dash" style="background-color:#83d160; border:1px solid #83d160; color:#FFF;">
							<div class="dash-text">
								<span class="badge">'.$stats['free_ipv4'].'</span>&nbsp;&nbsp;'.$l['free_ipv4'].'
							</div>
							<div class="dash-title">'.$stats['ipv4'].' / '.$stats['total_ipv4'].' '.$l['used_ipv4'].'</div>
							<i class="icon icon-ippool"></i>
						</div>
					</a>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<a href="'.$globals['ind'].'act=ippool">
						<div class="dash" style="background-color:#83d160; border:1px solid #83d160; color:#FFF;">
							<div class="dash-text">
								<span class="badge">'.$stats['free_ipv6'].'</span>&nbsp;&nbsp;'.$l['free_ipv6'].'
							</div>
							<div class="dash-title">'.$stats['ipv6'].' / '.$stats['total_ipv6'].' '.$l['used_ipv6'].'</div>
							<i class="icon icon-ippool"></i>
						</div>
					</a>
				</div>
			</div>
		</div>
		<div class="col-sm-6 col-md-6 col-lg-3">
			<div class="resource_gr" style="background-color: #3F51B5;border: 1px solid #3F51B5;">
				<div class="resource_header">'._strtoupper($l['cluster_ram']).'</div>
				<div class="resource_content">
					<div class="resource_container">
						<div class="text">'.round($stats['ram_used'] / $stats['total_ram'] * 100).'%</div>
						<div class="chart-2" data-percent="'.round($stats['ram_used'] / $stats['total_ram'] * 100).'"></div>
					</div>
					<div class="resource_value">'.$stats['ram_used'].' / '.$stats['total_ram'].' MB Used</div>
				</div>
			</div>
		</div>
		<div class="col-sm-6 col-md-6 col-lg-3">
			<div class="resource_gr" style="background-color: #FB8C00;border: 1px solid #FB8C00;">
				<div class="resource_header">'._strtoupper($l['cluster_storage']).'</div>
				<div class="resource_content">
					<div class="resource_container">
						<div class="text">'.round($stats['space_used'] / $stats['total_space'] * 100).'%</div>
						<div class="chart-2" data-percent="'.round($stats['space_used'] / $stats['total_space'] * 100).'"></div>
					</div>
					<div class="resource_value">'.$stats['space_used'].' / '.$stats['total_space'].' GB Used</div>
				</div>
			</div>
		</div>
	</div>';
}

echo '
	<div class="row">
		<div class="col-sm-12">
			<div class="divroundshad">
				<br />
				
				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<div id="panel-servers" class="panel panel-red">
							<div class="panel-heading">
								<span>'.$l['server_status'].'</span>
								<i class="icon icon-servers" style="float:right; font-size:20px; opacity:0.5"></i>
							</div>
							<div class="panel-content scrollbar-virt">
								<table class="table table-striped table-responsive table-dash" style="background-color:#fff">
									<thead>
										<tr>
											<th style="text-align:left">#</th>
											<th style="text-align:left">'.$l['server'].'</th>
											<th style="text-align:center">'.$l['vps'].'</th>
											<th style="text-align:center">'.$l['version'].'</th>
											<th style="text-align:center">'.$l['licence_expires'].'</th>
											<th style="text-align:center">'.$l['status'].'</th>
										</tr>
									</thead>
									<tbody>'; 
									
									foreach($servers as $s) {
										if($s['status'] == 0) {
											$status_txt = $l['offline'];
										} else if($s['status'] == 1) {
											$status_txt = $l['online'];
										} else {
											$status_txt = $l['licence_expired'];
										}
										
										echo '<tr>
											<td style="vertical-align:middle; text-align:left; width:50px;"><img src="'.$theme['images'].'admin/'.$s['virt'].'_28.png" /></td>
											<td style="text-align:left">'.$s['server_name'].' ('.$s['ip'].')</td>
											<td style="text-align:center">'.(empty($s['numvps']) ? '0' : $s['numvps']).'</td>
											<td style="text-align:center">'.(empty($s['version']) ? '-' : $s['version'].' ('.$s['patch'].')').'</td>
											<td style="text-align:center">'.(empty($s['lic_expires']) ? '-' : $s['lic_expires']).'</td>
											<td style="vertical-align:middle; text-align:center; width:50px;"><img src="'.$theme['images'].($s['status'] == 1 ? 'online' : 'offline').'.png" alt="'.$status_txt.'" title="'.$status_txt.'" class="server-status" /></td>
										</tr>';
									}
									
									echo '</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6">
						<div id="panel-vps" class="panel panel-blue">
							<div class="panel-heading">
								<span>'.$l['vps_info'].'</span>
								<i class="icon icon-vs" style="float:right; font-size:20px; opacity:0.5"></i>
							</div>
							<div class="panel-content scrollbar-virt">
								<table class="table table-striped table-dash">
									<tbody>';
									
									if(!empty($stats['num_openvz'])) {
										echo '<tr>
											<td>'.$l['openvz_vm'].'</td>
											<td style="padding-right:16px; text-align:center; width:30px;"><span class="badge">'.$stats['num_openvz'].'</span></td>
										</tr>';
									}
									
									if(!empty($stats['num_kvm'])) {
										echo '<tr>
											<td>'.$l['kvm_vm'].'</td>
											<td style="text-align:center; width:30px;"><span class="badge">'.$stats['num_kvm'].'</span></td>
										</tr>';
									}
									
									if(!empty($stats['num_xen'])) {
										echo '<tr>
											<td>'.$l['xen_vm'].'</td>
											<td style="text-align:center; width:30px;"><span class="badge">'.$stats['num_xen'].'</span></td>
										</tr>';
									}
									
									if(!empty($stats['num_xenhvm'])) {
										echo '<tr>
											<td>'.$l['xenhvm_vm'].'</td>
											<td style="text-align:center; width:30px;"><span class="badge">'.$stats['num_xenhvm'].'</span></td>
										</tr>';
									}
									
									if(!empty($stats['num_xcp'])) {
										echo '<tr>
											<td>'.$l['xcp_vm'].'</td>
											<td style="text-align:center; width:30px;"><span class="badge">'.$stats['num_xcp'].'</span></td>
										</tr>';
									}
									
									if(!empty($stats['num_xcphvm'])) {
										echo '<tr>
											<td>'.$l['xcphvm_vm'].'</td>
											<td style="text-align:center; width:30px;"><span class="badge">'.$stats['num_xcphvm'].'</span></td>
										</tr>';
									}
									
									if(!empty($stats['num_lxc'])) {
										echo '<tr>
											<td>'.$l['lxc_vm'].'</td>
											<td style="text-align:center; width:30px;"><span class="badge">'.$stats['num_lxc'].'</span></td>
										</tr>';
									}
									
									if(!empty($stats['num_vzo'])) {
										echo '<tr>
											<td>'.$l['vzo_vm'].'</td>
											<td style="text-align:center; width:30px;"><span class="badge">'.$stats['num_vzo'].'</span></td>
										</tr>';
									}
									
									if(!empty($stats['num_vzk'])) {
										echo '<tr>
											<td>'.$l['vzk_vm'].'</td>
											<td style="text-align:center; width:30px;"><span class="badge">'.$stats['num_vzk'].'</span></td>
										</tr>';
									}
									
									echo '</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<div id="panel-tasks" class="panel panel-blue-grey">
							<div class="panel-heading">
								<span>'.$l['recent_tasks'].'</span>
								<i class="icon icon-tasks" style="float:right; font-size:20px; opacity:0.5"></i>
							</div>
							<div class="panel-content scrollbar-virt">
								<table class="table table-striped table-dash">
									<thead>
										<tr>
											<th>'.$l['user'].'</th>
											<th>'.$l['action'].'</th>
											<th style="width:80px;">'.$l['logs'].'</th>
										</tr>
									</thead>
									<tbody>';
									
									foreach($stats['tasks'] as $task) {
										echo '<tr>
											<td>'.(empty($task['uid']) ? 'root' : $task['email']).'</td>
											<td>'.$l[$task['action']].'</td>
											<td style="width:80px;"><button class="btn btn-logs" onclick="loadlogs('.$task['actid'].');">'.$l['show'].'</button></td>
										</tr>';
									}
									
									echo '</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6">
						<div id="panel-logins" class="panel panel-blue-grey">
							<div class="panel-heading">
								<span>'.$l['recent_logins'].'</span>
								<i class="icon icon-users" style="float:right; font-size:20px; opacity:0.5"></i>
							</div>
							<div class="panel-content scrollbar-virt">
								<table class="table table-striped table-dash">
									<tbody>';
									
									foreach($stats['logins'] as $login) {
										// If it's not today's date then include date in string
										$time_text = datify($login['time'], 1, 1);
										
										echo '<tr>
											<td>
												'.(empty($login['name']) ? '' : '<div>'.$login['name'].'</div>').'
												<div>'.(empty($login['name']) ? $login['username'] : '<span style="color: rgba(0,0,0,0.54)">'.$login['username'].'</span>').'</div>
											</td>
											<td style="text-align:right;">'.$time_text.'</td>
										</tr>';
									}
									echo '</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	

	<div class="row">
		<div class="col-sm-12">
			<div class="divroundshad">
				<div class="roundheader">'.$l['server_stats'].'</div>
				
				<br />
				
				<div class="row" style="margin-bottom:16px;">
					<div class="hidden-xs col-md-3"></div>
					<div class="col-xs-12 col-md-6">
						<select id="cserver" class="form-control" onchange="callserverstats(this.value)" disabled>';
									
						foreach($servers as $k => $v){
							echo '<option value="'.$k.'" '.($globals['server'] == $k ? 'selected="selected"' : '').'>'.$v['server_name'].'</option>';
						}
						echo '<option value="-1">'.$l['all_servers'].'</option></select>
					</div>
					<div class="hidden-xs col-md-3"></div>
				</div>';
				
			foreach($servers as $k => $v){
					
				echo '<div class="e_notice" id="server_error'.$k.'" style="display:none; margin-bottom:32px;"></div>
				
				<div id="row_server_loading'.$k.'" class="row" style="display:none;">
					<div class="col-xs-12" style="padding:96px 0; text-align:center;">
						<img src="'.$theme['images'].'progress_bar.gif" height="20" width="20" />
					</div>
				</div>
				
				<div id="row_server_graphs'.$k.'" class="row" style="display:none; margin-bottom:16px">
					<div class="roundheader">'.$l['server'].' : '.ucwords($v['server_name']).'</div>					
					<div class="hidden-xs col-lg-1"></div>
					<div id="server_cpu'.$k.'" class="server_gr col-xs-6 col-sm-6 col-md-4 col-lg-2">
						<div class="title">'.$l['cpu'].'</div>
						<div class="content">
							<div class="text">
								<div class="value">0</div>
								<div>'.$l['used'].'</div>
							</div>
							<div class="chart" data-percent="0"></div>
						</div>
						<div class="bottom-text">0</div>
					</div>
					<div id="server_ram'.$k.'" class="server_gr col-xs-6 col-sm-6 col-md-4 col-lg-2">
						<div class="title">'.$l['ram'].'</div>
						<div class="content">
							<div class="text">
								<div class="value">0</div>
								<div>'.$l['used'].'</div>
							</div>
							<div class="chart" data-percent="0"></div>
						</div>
						<div class="bottom-text">0</div>
					</div>
					<div id="server_disk'.$k.'" class="server_gr col-xs-6 col-sm-6 col-md-4 col-lg-2">
						<div class="title">'.$l['disk'].'</div>
						<div class="content">
							<div class="text">
								<div class="value">0</div>
								<div>'.$l['used'].'</div>
							</div>
							<div class="chart" data-percent="0"></div>
						</div>
						<div class="bottom-text">0</div>
					</div>
					<div id="server_storage'.$k.'" class="server_gr col-xs-6 col-sm-6 col-md-6 col-lg-2">
						<div class="title">'.$l['storage'].'</div>
						<div class="content">
							<div class="text">
								<div class="value">0</div>
								<div>'.$l['used'].'</div>
							</div>
							<div class="chart" data-percent="0"></div>
						</div>
						<div class="bottom-text">0</div>
					</div>
					<div id="server_band'.$k.'" class="server_gr col-xs-12 col-sm-12 col-md-6 col-lg-2">
						<div class="title">'.$l['bandwidth'].'</div>
						<div class="content">
							<div class="text">
								<div class="value">0</div>
								<div>'.$l['used'].'</div>
							</div>
							<div class="chart" data-percent="0"></div>
						</div>
						<div class="bottom-text">0</div>
					</div>
					<div class="hidden-xs col-lg-1"></div>
				</div>
				
				<div id="row_server_table'.$k.'" class="row" style="display:none;">
					<div class="col-xs-12 col-md-12">
						<div>
							<table class="table table-striped" width="100%" cellspacing="1" cellpadding="6" border="0" align="center">
								<tbody>
								<tr>
									<td rowspan="5" style="background-color:#fff; text-align:center;" valign="middle">
										<div>'.$l['poweredby'].'</div><br />
										<div id="server_poweredby'.$k.'"></div>
									</td>
									<td>'.$l['uptime'].'</td>
									<td id="server_uptime'.$k.'"></td>
								</tr>
								<tr>
									<td>'.$l['os'].'</td>
									<td class="val" id="server_os'.$k.'"></td>
								</tr>
								<tr>
									<td>'.$l['cpumodel'].'</td>
									<td id="cpumodel'.$k.'"></td>
								</tr>
								<tr>
									<td>'.$l['kernel'].'</td>
									<td id="server_kernel'.$k.'"></td>
								</tr>
								<tr>
									<td>'.$l['numvps'].'</td>
									<td id="server_vps'.$k.'"></td>
								</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>';
			}
			echo '</div>
		</div>
	</div>
';

echo '
<script language="javascript" type="text/javascript" src="https://a.softaculous.com/virtualizor/latestinfo.php"></script>
<script type="text/javascript">
	var servers = new Array('.implode(', ', array_keys($servers)).');	
	function callserverstats(serid){
		if(serid < 0){
			if(empty(servers)){
				getserverstats(0);
			}else{
				$("#cserver").prop("disabled", true);
				servers.sort(function(a, b){return b-a});
				for(x in servers){	
					//alert(servers[x]);
					getserverstats(servers[x]);				
				}
			}
		}else{
			$("#cserver").prop("disabled", true);
			getserverstats(serid);			
		}
		
	}
	
	function hideserverstats(){
		if(!empty(servers) && $("#cserver").find("option:selected").val() >= 0){
			for(x in servers){
				$("#row_server_graphs"+servers[x]).hide();
				$("#row_server_table"+servers[x]).hide();
				$("#server_error"+servers[x]).hide();
			}
		}
	}
	
	function getserverstats(serid){
		//$_("refresh_status").src = "'.$theme['images'].'admin/vpsloading.gif";
		
		serid = serid || 0;		
		
		$("#server_error").hide();
		$("#row_server_loading"+serid).show();
		hideserverstats();
		
		$.getJSON("'.$globals['index'].'act=manageserver&changeserid="+serid+"&api=json", function(data) {
			
			$("#cserver").prop("disabled", false);
			$("#row_server_loading"+serid).hide();
			
			// Is it offline ?
			if(data["offline"]){
				$("#server_error"+serid).html("<img src=\"'.$theme['images'].'notice.gif\" /> &nbsp;" + data["offline"]);
				$("#server_error"+serid).show();
				return false;
			}
			
			$("#row_server_graphs"+serid).show();
			$("#row_server_table"+serid).show();
			
			// Its online
			// CPU
			var cpu_per = data["usage"]["cpu"]["percent"].toFixed(2);
			$("#server_cpu"+serid+" .chart").data("easyPieChart").update(cpu_per);
			$("#server_cpu"+serid+" .value").text(cpu_per + "%");
			$("#server_cpu"+serid+" .bottom-text").text(cpu_per + " %");
			
			// RAM
			var ram_per = data["usage"]["ram"]["percent"].toFixed(2);
			var ram_used = data["usage"]["ram"]["used"];
			var ram_limit = data["usage"]["ram"]["limit"];
			$("#server_ram"+serid+" .chart").data("easyPieChart").update(ram_per);
			$("#server_ram"+serid+" .value").text(ram_per + "%");
			$("#server_ram"+serid+" .bottom-text").text(ram_used + " / " + ram_limit + " MB");
			
			// DISK
			var disk_per = data["usage"]["disk"]["/"]["percent"].toFixed(2);
			var disk_used = data["usage"]["disk"]["/"]["used_gb"];
			var disk_limit = data["usage"]["disk"]["/"]["limit_gb"];
			$("#server_disk"+serid+" .chart").data("easyPieChart").update(disk_per);
			$("#server_disk"+serid+" .value").text(disk_per + "%");
			$("#server_disk"+serid+" .bottom-text").text(disk_used + " / " + disk_limit + " GB");
			
			// VPS Storage DISK
			var vps_used = (data["info"]["resources"]["total_space"] - data["info"]["resources"]["space"]).toFixed(2);
			var vps_total = data["info"]["resources"]["total_space"];
			var vps_per = (vps_used / vps_total * 100).toFixed(2);
			$("#server_storage"+serid+" .chart").data("easyPieChart").update(vps_per);
			$("#server_storage"+serid+" .value").text(vps_per + "%");
			$("#server_storage"+serid+" .bottom-text").text(vps_used + " / " + vps_total + " GB");
			
			// Bandwidth
			var band_per = data["usage"]["bandwidth"]["percent"].toFixed(2);
			var band_used = data["usage"]["bandwidth"]["used_gb"];
			var band_limit = data["usage"]["bandwidth"]["limit_gb"];
			$("#server_band"+serid+" .chart").data("easyPieChart").update(band_per);
			$("#server_band"+serid+" .value").text(band_per + "%");
			$("#server_band"+serid+" .bottom-text").text(band_used + " / " + band_limit + " GB");
			
			if((data["info"]["check_kernel"]).length > 10){
				$_("server_error"+serid).innerHTML = \'<img src="'.$theme['images'].'notice.gif" /> &nbsp; \'+data["info"]["check_kernel"];
				$("#server_error"+serid).show();
			}else{
				$("#server_error"+serid).hide();
			}
			
			$_("server_uptime"+serid).innerHTML = data["info"]["uptime"];
			$_("server_os"+serid).innerHTML = data["info"]["os"]["name"];
			$_("server_kernel"+serid).innerHTML = data["info"]["kernel_name"];
			$_("server_poweredby"+serid).innerHTML = \'<img src="'.$theme['images'].'\'+data["usage"]["cpu"]["manu"]+\'.gif" />\';
			$_("server_vps"+serid).innerHTML = data["info"]["numvps"] ? data["info"]["numvps"] : 0;
			$_("cpumodel"+serid).innerHTML = data["usage"]["cpu"]["cpumodel"];
		});
	}
';

if($acl_server_statistics){
echo '$(document).ready(function(){
		getserverstats('.$globals['server'].');
	});';
}

echo '
$(document).ready(function(){
	$_("softnews").style.width = $_("softnewsholder").offsetWidth;
	//The news
	if(typeof(soft_news) == "undefined"){
		$_("softnews").innerHTML = "'.$l['conect_to_soft'].'";
	}else{
		var newsstr = "";
		for(x in soft_news){
			newsstr = newsstr+\'<div class="softnewshead">\'+soft_news[x][0]+\'</div>\'+\'<div class="softnewsblock">\'+soft_news[x][1]+\'</div><br />\';
		}
		$_("softnews").innerHTML = newsstr;
	}
	
	$(".close_pinguzo_banner").click(function(){
		 $.ajax({
			url: "'.$globals['index'].'disable_pinguzo_ban=1", 
			success: function(result){
				$("#close_pinguzo_banner_div").hide();
			}
		});
	});
});
</script>

<br/>
<div class="roundheader">'.$l['news'].'</div>
<table width="100%" cellpadding="1" cellspacing="1">			
	<tr>
	<td width="100%" height="200" valign="top" id="softnewsholder">
	<div class="softnews" id="softnews"></div>
	</td>
	</tr>
</table>
<hr>';

if(!file_exists($globals['var'].'/disable_pinguzo_banner.txt')){
	echo '
	<div class="clearfix"></div>
	<div class="row alert alert-info" id="close_pinguzo_banner_div">
		<a style="float:right;display:inline-block;padding:2px 5px;cursor:pointer;font-size:20px;" title="Close this" class="close_pinguzo_banner"><b>[x]</b></a>
		<div class="col-md-7">
			<h2 style="margin-top:5px;">Pinguzo - Server and Website Monitoring</h2>
			<span style="font-size:15px; line-height:150%">We have been developing Pinguzo, which is a Server and Website Monitoring SaaS. As you know, downtime can happen on your Servers and Websites. Pinguzo can send notifications instantly, so that you can take corrective steps. 
			<br>You can use your <b>Softaculous Account to Sign In</b>. Since its a SaaS, you will not need to manage any storage or processes related to monitoring.
			<center style="margin-top:15px;"><a href="https://cp.pinguzo.com" class="btn btn-lg btn-success" target="new">Let\'s Try Pinguzo</a></center>
			<br>If you have any feedback / questions, please do let us know - <a href="mailto:support%40pinguzo.com">support@pinguzo.com</a>.</span>
		</div>
		<div class="col-md-5">
			<img src="https://pinguzo.com/images/servers.jpg" class="img-responsive">
		</div>
	</div>';
}
echo '
</div>
';

if(!empty($stats['num_vps'])) {
	$table_vps = '<table class=\"tool\">'.
	(!empty($stats['num_openvz']) ? '<tr><td align=\"left\">OpenVZ</td><td align=\"right\">'.$stats['num_openvz'].'</td></tr>' : '').
	(!empty($stats['num_kvm']) ? '<tr><td align=\"left\">KVM</td><td align=\"right\">'.$stats['num_kvm'].'</td></tr>' : '').
	(!empty($stats['num_xen']) ? '<tr><td align=\"left\">Xen</td><td align=\"right\">'.$stats['num_xen'].'</td></tr>' : '').
	(!empty($stats['num_xenhvm']) ? '<tr><td align=\"left\">Xen HVM</td><td align=\"right\">'.$stats['num_xenhvm'].'</td></tr>' : '').
	(!empty($stats['num_xcp']) ? '<tr><td align=\"left\">Xen Server</td><td align=\"right\">'.$stats['num_xcp'].'</td></tr>' : '').
	(!empty($stats['num_xcphvm']) ? '<tr><td align=\"left\">Xen Server HVM</td align=\"right\"><td>'.$stats['num_xcphvm'].'</td></tr>' : '').
	(!empty($stats['num_lxc']) ? '<tr><td align=\"left\">LXC</td><td align=\"right\">'.$stats['num_lxc'].'</td></tr>' : '').
	(!empty($stats['num_vzo']) ? '<tr><td align=\"left\">Virtuozzo OpenVZ</td><td align=\"right\">'.$stats['num_vzo'].'</td></tr>' : '').
	(!empty($stats['num_vzk']) ? '<tr><td align=\"left\">Virtuozzo KVM</td><td align=\"right\">'.$stats['num_vzk'].'</td></tr>' : '').
	'</table>';
} else {
	$table_vps = '';
}
if(!empty($stats['num_total_users'])) {
	$table_users = '<table class=\"tool\">'.
	(!empty($stats['num_admins']) ? '<tr><td align=\"left\">'.$l['num_admins'].'</td><td align=\"right\">'.$stats['num_admins'].'</td></tr>' : '').
	(!empty($stats['num_clouds']) ? '<tr><td align=\"left\">'.$l['num_clouds'].'</td><td align=\"right\">'.$stats['num_clouds'].'</td></tr>' : '').
	(!empty($stats['num_users']) ? '<tr><td align=\"left\">'.$l['num_users'].'</td><td align=\"right\">'.$stats['num_users'].'</td></tr>' : '').
	'</table>';
} else {
	$table_users = '';
}

echo '<script type="text/javascript">

	function loadlogs(actid){
		
		actid = actid || 0;
		
		$.ajax({
			url: "'.$globals['index'].'&act=tasks&api=json",
			data: "show_logs="+actid,
			dataType : "json",
			method : "post",
			success:function(data){
				
				logs_tab_head = "";
				logs_tab_data = "";
				var j = 0;
				active = "";
				
				common_logs = data["logs_data"]["common_logs"] || "";
				
				if(typeof(data["logs_data"]["logs"]) === "object"){
					
					$.each(data["logs_data"]["logs"], function(i, item){
						
						if(j == 0){
							active = "active";
						}else{
							active = "";
						}
						
						logs_tab_head += "<li class="+active+"><a data-toggle=\"tab\" href=\"#"+i+"_tab\">"+i+"</a></li>";
						logs_tab_data += "<div id=\""+i+"_tab\" class=\"tab-pane in fade "+active+"\"><pre style=\"max-height: 450px;\" >"+item+"</pre></div>";
						j++;
					});
					
					common_logs_html = "";
					if(common_logs != ""){
						common_logs_html = "<div class=\"notice\"><img src=\"'.$theme['images'].'notice.gif\" /> &nbsp; '.$l['common_logs'].'</div>";
					}
					
					logs_html = common_logs_html+"<ul class=\"nav nav-tabs\">"+logs_tab_head+"</ul><div class=\"tab-content\">"+logs_tab_data+"</div>";
					
				}else{
					logs_html = "<pre style=\"max-height: 500px;\" >"+data["logs_data"]["logs"]+"</pre>";				
				}		
				
				$("#logs_modal").modal("show");
				$("#logs_modal_body").html(logs_html);
			}
		});
		
	}

	function bar_color(v) {
		if(v <= 50) {
			return "#00C853";
		} else if(v <= 80) {
			return "#FF9100";
		} else {
			return "#D50000";
		}
	}

    $(".chart").easyPieChart({
		barColor: bar_color,
		trackColor: "#f2f2f2",
		scaleColor: "#cccccc",
		animate: { duration: 1000, enabled: true },
		lineWidth: 10,
		size: 140,
		lineCap: "cap"
	});

    $(".chart-2").easyPieChart({
		barColor: "#fff",
		trackColor: "rgba(255,255,255,0.3)",
		scaleColor: "#fff",
		animate: { duration: 1000, enabled: true },
		lineWidth: 4,
		size: 80,
		lineCap: "cap"
	});
	
	$(document).ready(function() {
		$("#dash_vps").tooltip({
			html: true,
			placement: "right",
			title: "'.$table_vps.'"
		});
		
		$("#dash_users").tooltip({
			html: true,
			placement: "right",
			title: "'.$table_users.'"
		});
		
		$(".server-status").tooltip({
			html: false,
			placement: "left"
		});
	});
	
	
	$(window).on("load resize", function(){
		$("#panel-servers, #panel-vps, #panel-tasks, #panel-logins").height("auto");
		
		var screenWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
		
		// Only set height if not mobile screen
		if (screenWidth >= 768) {
			var max_row1 = Math.max($("#panel-servers").height(), $("#panel-vps").height());
			var max_row2 = Math.max($("#panel-tasks").height(), $("#panel-logins").height());
			
			$("#panel-servers").height(max_row1 + "px");
			$("#panel-vps").height(max_row1 + "px");
			
			$("#panel-tasks").height(max_row2 + "px");
			$("#panel-logins").height(max_row2 + "px");
		}
	});
	
</script>';

softfooter();

}

?>
