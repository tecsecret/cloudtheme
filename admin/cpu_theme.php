<?php

//////////////////////////////////////////////////////////////
//===========================================================
// cpu_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function cpu_theme(){

global $theme, $globals, $cluster, $user, $l, $cpu , $cluster;

if(optGET('ajax')){	

	echo 'var cpu = [
		{ label: "Used",  data: '.$cpu['percent'].'},
		{ label: "Free",  data: '.$cpu['percent_free'].'}
	];';
		
	return true;

}

softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-cpu icon-head"></i>&nbsp; '.$l['header'].'<span style="float:right;" ><a href="'.$globals['docs'].'Server_CPU_Usage" target="_blank" class="wiki_help" title="'.$l['wiki_help'].'"><i class="icon-help" ></i></a></span></center><br />';

// Is it offline ?
$hypervisor_status = $cluster->statewise($globals['server']);
if($hypervisor_status == 0 || $hypervisor_status == 2){

	echo '<div class="e_notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['server_status_'.$hypervisor_status].'</div>';
	
}else{

echo '<script language="javascript" type="text/javascript"><!-- // --><![CDATA[

// Draw a Resource Graph
function resource_graph(id, data){

    $.plot($("#"+id), data, 
	{
		series: {
			pie: { 
				innerRadius: 0.7,
				radius: 1,
				show: true,
				label: {
					show: true,
					radius: 0,
					formatter: function(label, series){
						if(label != "Used") return "";
						return \'<div style="font-size:18px;text-align:center;padding:2px;color:black;">\'+Math.round(series.percent)+\'%</div><div style="font-size:10px;">\'+label+\'</div>\';	
					}
				}
			}
		},
		legend: {
			show: false
		}
	});
}


													   
function getusage(){
	if(AJAX("'.$globals['index'].'act=cpu&ajax=true", "drawpie(re)")){
		return false;
	}else{
		return true;	
	}
};

function startusage(){
	ajaxtimer = setInterval("getusage()", 5000);
};	

function drawpie(re){	
	
	var cpu = [
		{ label: "Used",  data: '.$cpu['percent'].'},
		{ label: "Free",  data: '.$cpu['percent_free'].'}
	];
	
	if(re.length > 0){
		try{
			eval(re);
		}catch(e){ }
	}
	
	resource_graph("holder", cpu);
	$_("cppercent").innerHTML = cpu[0]["data"]+" %";
	
};

addonload("drawpie(\'void(0);\');startusage();");

// ]]></script>


<div class="row">
	<div class="col-sm-6" style="border-right:1px solid #CCCCCC">
		<div class="roundheader">'.$l['cpuinfo'].'</div>
		<table align="center" class="table table-hover" cellpadding="3" cellspacing="4" border="0" width="100%">
			<tr><td align="left" class="blue_td" width="30%">'.$l['cpuinfo'].'</td><td class="fhead val">'.$cpu['limit'].'</td></tr>
			<tr><td align="left" class="blue_td">'.$l['cpuutilised'].'</td><td class="val" id="cppercent"></td></tr>
			<tr><td align="left" class="blue_td">'.$l['poweredby'].'</td><td><img src="'.$theme['images'].$cpu['manu'].'.gif" /></td></tr>
		</table>
	</div>
	<div class="col-sm-6" align="center" width="50%">
		<div class="roundheader">'.$l['graphheader'].'</div>
		<div id="holder" style="width:170px; height:170px; margin:5px;">
	</div>
</div>
<div class="clearfix"></div>
<br/>
<div class="notice">
'.$l['trademarks'].'
</div>';

}
echo '</div>';
softfooter();

}

?>