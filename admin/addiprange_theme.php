<?php

//////////////////////////////////////////////////////////////
//===========================================================
// addiprange_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function addiprange_theme(){	

global $theme, $globals, $cluster, $user, $l, $ippools, $servers, $error, $done, $ips, $allowed_netmasks, $allowed_input_netmasks;

softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-ippool icon-head"></i>&nbsp; '.$l['addip'].'</center>';

error_handle($error);

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
}

echo '<script language="javascript" type="text/javascript">

function addrow6(id){
	var t = $_(id);

	var row = document.createElement("div");
	row.className = "ipv6-range";
	var col1 = document.createElement("div");
	var col2 = document.createElement("div");
	var col3 = document.createElement("div");
	var col4 = document.createElement("div");
	var col5 = document.createElement("div");
	var col6 = document.createElement("div");
	var col7 = document.createElement("div");
	var col8 = document.createElement("div");
	
	col1.innerHTML = \'<input type="text" name="ips6[\'+id+\'][1]" value="" size="4" maxlength="4" class="form-control ips6box ips6_1" /><label> : </label>\';
	col2.innerHTML = \'<input type="text" name="ips6[\'+id+\'][2]" value="" size="4" maxlength="4" class="form-control ips6box ips6_2" /><label> : </label>\';
	col3.innerHTML = \'<input type="text" name="ips6[\'+id+\'][3]" value="" size="4" maxlength="4" class="form-control ips6box ips6_3" /><label> : </label>\';
	col4.innerHTML = \'<input type="text" name="ips6[\'+id+\'][4]" value="" size="4" maxlength="4" class="form-control ips6box ips6_4" /><label> : </label>\';
	col5.innerHTML = \'<input type="text" name="ips6[\'+id+\'][5]" value="" size="4" maxlength="4" class="form-control ips6box ips6_5" /><label> : </label>\';
	col6.innerHTML = \'<input type="text" name="ips6[\'+id+\'][6]" value="" size="4" maxlength="4" class="form-control ips6box ips6_6" /><label> : </label>\';
	col7.innerHTML = \'<input type="text" name="ips6[\'+id+\'][7]" value="" size="4" maxlength="4" class="form-control ips6box ips6_7" /><label> : </label>\';
	col8.innerHTML = \'<input type="text" name="ips6[\'+id+\'][8]" value="" size="4" maxlength="4" class="form-control ips6box ips6_8" />\';

	row.appendChild(col1);
	row.appendChild(col2);
	row.appendChild(col3);
	row.appendChild(col4);
	row.appendChild(col5);
	row.appendChild(col6);
	row.appendChild(col7);
	row.appendChild(col8);
	
	t.appendChild(row);
	disable_fields();
};

function disable_range_fields(){
	
	var val = parseInt($("#input_netmask").val());
	rem = (val % 16);
	
	required = Math.floor(val/16);
	
	if(rem > 0){
		rem = (rem/4);
		required += 1;
	}
	
	for(x = 1; x <= 8; x++){
		
		$(".ipv6_"+x).each(function(index){
			
			var todo = false;
			
			if(x > (required)){
				todo = true;
			}
			
			if(rem == 0){
				rem = 4;
			}
			
			if(x == required && rem > 0){
			
				$(this).attr("maxlength", rem);
			}
			
			$(this).prop("disabled", todo);
		});
	}
}

function disable_fields(){
	var val = parseInt($("#netmask").val());
	val = (val/16);
	
	for(x = 1; x <= 8; x++){
		
		$(".ips6_"+x).each(function(index){
			var todo = false;
			if(x >= (val+1)){
				todo = true;
			}
			$(this).prop("disabled", todo);
		});
	}
	
	disable_range_fields();
}

addonload("disable_fields();");

</script>

<div id="form-container">
<form accept-charset="'.$globals['charset'].'" name="addippool" method="post" action="" class="form-horizontal">

<div class="row">
	<div class="col-sm-4"><label class="fhead">'.$l['netmask'].'</label>
	<span class="help-block">'.$l['output_netmask'].'</span></div>
	<div class="col-sm-4" style="margin-left:-14px">';
		$default_netmask = (int) empty($_REQUEST['netmask']) ? 112 : $_REQUEST['netmask'];
		echo '<label style="float:left; padding:8px 8px 0px 0px;">/</label><select id="netmask" class="form-control" name="netmask" style="width:90%;" onchange="disable_fields()">';
			foreach($allowed_netmasks as $k => $v){
				echo '<option value="'.$k.'" '.($default_netmask == $k ? 'selected="selected"' : '').'>'.$k.'</option>';
			}			
		echo '</select>
		<span class="help-block"></span>
	</div>
	<div class="col-sm-4"></div>
</div>

<div class="row">
	<div class="col-sm-4"><label class="control-label">'.$l['input_netmask'].'</label></div>
	<div class="col-sm-4" style="margin-left:-14px">';
		$input_netmask = (int) empty($_REQUEST['input_netmask']) ? 112 : $_REQUEST['input_netmask'];
		echo '<label style="float:left; padding:8px 8px 0px 0px;">/</label><div id="in_netmask"><select id="input_netmask" class="form-control" name="input_netmask" style="width:90%;" onchange="disable_fields()">';
			foreach($allowed_input_netmasks as $k => $v){
				echo '<option value="'.$k.'" '.($input_netmask == $k ? 'selected="selected"' : '').'>'.$k.'</option>';
			}			
		echo '</select></div>
		<span class="help-block"></span>
	</div>
	<div class="col-sm-4"></div>
</div>

<div class="row" id="ips6">
	<fieldset class="user_details_f">
		
		<legend class="user_details_f">'.$l['gen_ipv6_ip'].'</legend>
		<div class="col-sm-4">
			<label class="control-label">'.$l['enterip'].'</label>
			<span class="help-block">'.$l['enterip_exp_6'].'</span>
		</div>
		<div class="col-sm-8">
			<div id="iptable6">';
			 
				$ips = @$_POST['ips6'];
				
				if(is_array($ips) && !empty($ips)){
					foreach($ips as $k => $ip){
						$ip_ = implode('', $ip);
						if(empty($ip_)){
							unset($ips[$k]);
						}
					}
				}
				
				if(empty($ips)){
					$ips = array(NULL);
				}
			 
				foreach($ips as $k => $ip){
					echo '
				<div class="ipv6-range">
					<div><input type="text" name="ips6['.$k.'][1]" value="'.$ip[1].'" size="4" maxlength="4" class="form-control ips6box ips6_1" /><label class="colon"> : </label></div>
					<div><input type="text" name="ips6['.$k.'][2]" value="'.$ip[2].'" size="4" maxlength="4" class="form-control ips6box ips6_2" /><label class="colon"> : </label></div>
					<div><input type="text" name="ips6['.$k.'][3]" value="'.$ip[3].'" size="4" maxlength="4" class="form-control ips6box ips6_3" /><label class="colon"> : </label></div>
					<div><input type="text" name="ips6['.$k.'][4]" value="'.@$ip[4].'" size="4" maxlength="4" class="form-control ips6box ips6_4" /><label class="colon"> : </label></div>
					<div><input type="text" name="ips6['.$k.'][5]" value="'.@$ip[5].'" size="4" maxlength="4" class="form-control ips6box ips6_5" /><label class="colon"> : </label></div>
					<div><input type="text" name="ips6['.$k.'][6]" value="'.@$ip[6].'" size="4" maxlength="4" class="form-control ips6box ips6_6" /><label class="colon"> : </label></div>
					<div><input type="text" name="ips6['.$k.'][7]" value="'.@$ip[7].'" size="4" maxlength="4" class="form-control ips6box ips6_7" /><label class="colon"> : </label></div>
					<div><input type="text" name="ips6['.$k.'][8]" value="'.@$ip[8].'" size="4" maxlength="4" class="form-control ips6box ips6_8" /> </div>
				</div>';
				}
					
			echo '
			</div>
			<input type="button" value="'.$l['add_more_ips'].'" onclick="addrow6(\'iptable6\')" class="go_btn" />
		</div>
	</fieldset>
</div>

<div class="row" id="gen_ipv6"><div class="col-sm-12 tit" align="center"><span class="help-block"></span>'.$l['gen_ipv6'].'</div></div>

<div class="row" id="ipv6_range">
	<div class="col-sm-4">
		<label class="control-label">'.$l['subnet'].'</label>
	</div>
	<div class="col-sm-8">
		<div class="ipv6-range">
			<div><input type="text" name="ipv6_1" value="'.POSTval('ipv6_1', '').'" size="4" maxlength="4" class="form-control ips6box ipv6_1"><label> : </label></div>
			<div><input type="text" name="ipv6_2" value="'.POSTval('ipv6_2', '').'" size="4" maxlength="4" class="form-control ips6box ipv6_2"><label> : </label></div>
			<div><input type="text" name="ipv6_3" value="'.POSTval('ipv6_3', '').'" size="4" maxlength="4" class="form-control ips6box ipv6_3"><label> : </label></div>
			<div><input type="text" name="ipv6_4" value="'.POSTval('ipv6_4', '').'" size="4" maxlength="4" class="form-control ips6box ipv6_4"><label> : </label></div>
			<div><input type="text" name="ipv6_5" value="'.POSTval('ipv6_5', '').'" size="4" maxlength="4" class="form-control ips6box ipv6_5"><label> : </label></div>
			<div><input type="text" name="ipv6_6" value="'.POSTval('ipv6_6', '').'" size="4" maxlength="4" class="form-control ips6box ipv6_6"><label> : </label></div>
			<div><input type="text" name="ipv6_7" value="'.POSTval('ipv6_7', '').'" size="4" maxlength="4" class="form-control ips6box ipv6_7"><label> : </label></div>
			<div><input type="text" name="ipv6_8" value="'.POSTval('ipv6_8', '').'" size="4" maxlength="4" class="form-control ips6box ipv6_8"> </div>
		</div>
	</div>
</div>

<span class="help-block"></span>
<div class="row" id="ipv6_num">
	<div class="col-sm-4">
		<label class="control-label">'.$l['ipv6_num'].'</label>
		<span class="help-block">'.$l['ipv6_num_exp'].'</span>
	</div>
	<div class="col-sm-4"><input type="text" class="form-control" name="ipv6_num" value="'.POSTval('ipv6_num', 50).'" size="10"></div>
	<div class="col-sm-4"></div>
</div>

<div class="row">
	<div class="col-sm-4"><label class="control-label">'.$l['sel_ippool'].'</label></div>
	<div class="col-sm-4">
		<select class="form-control" name="ippid" id="ippid">
			<option value="0">'.$l['ipp_none'].'</option>';	
		
			// Select the default IP Pool if there in the GET
			if(!POSTval('ippid') && !POSTval('submitip')){
				$_POST['ippid'] = optGET('ippid');
			}
			
			foreach($ippools as $k => $v){		 
				echo '<option value="'.$k.'" '.((POSTval('ippid') == $k) ? 'selected="selected"' : '').' ipv="'.(!empty($v['ipv6']) ? 6 : 4).'">'.$v['ippool_name'].'</option>';
			}
		
	 	echo'</select>
	 	<span class="help-block"></span>
	</div>
	<div class="col-sm-4"></div>
</div>

</div>


<center><input type="submit" name="submitip" value="'.$l['submitip'].'" class="btn" /></center>
	
</form>
</div>
';

softfooter();

}

?>