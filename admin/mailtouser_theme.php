<?php

//////////////////////////////////////////////////////////////
//===========================================================
// vscpu_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function mailtouser_theme(){

global $theme, $globals, $cluster, $users, $l, $vpsusage, $available, $vpses, $error, $done, $servergroups, $servers, $users_email;

softheader($l['<title>']);

echo '
<div class="bg" style="width:90%">
<center class="tit"><i class="icon icon-users icon-head"></i>&nbsp; '.$l['add_user'].'</center>';

error_handle($error);

if(!empty($done)){
	echo '<div class="notice" style="width:80%; margin:10px auto;"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
}

echo '
<script type="text/javascript" src="'.$theme['url'].'/js/tinymce/tinymce.min.js"></script>
<style type="text/css">
	.mce-tinymce{
		margin: 0 auto !important;
		width: 82%;
		min-width: 370px;
	}
	.usr{
		font-size: 16px;
		color: #444444;
		text-decoration: none;
		height: 30px;
		font-family: OpenSans,Arial, Verdana, Helvetica, sans-serif;
	}
	.cat-wrapper{
		margin: 0px auto !important;
		width:82%;
	}
	.usr label,#plus2{
		cursor:pointer;
		font-weight: normal;
	}
	.usr_small{
		margin-left:4px;
	}
	.usr_small::after{
		content:")";
	}
	.usr_small::before{
		content:"(";
	}
	.to_label{
		color: black;
		height: 5px;
		font-size:14px;
		font-weight: bold;
		margin-top: 5px;
	}
	.subject_label{
		color: black;
		height: 5px;
		font-size:14px;
		font-weight: bold;
		margin-top: 5px;
	}
	.error {
		width: 82%;
		margin: 10px auto;
	}
	.togglediv{
		padding:8px 0px 0px 8px;
	}
	
	label{
		vertical-align: super;
	  	padding-left: 8px;
	}
		
	control-label{
		padding-top: 8px !important;
	}
	
	.adj_ht{
		height : 30px;
	}
	
	.adj_ht_cont{
		height : 30px;
		padding : 6px;
		font-size : 12px;
		font-weight : bold;		
	}
	
	.btn.adj_ht_cont{
		height : 22.5px;
		margin : 3px;
		padding : 2.7px;
		padding-left : 20px;
		padding-right : 20px;		
	}
	
	#toggle_indicator{
		float: left;
		font-size: 1em;
		margin-top: 0px;
	}
	
	.hover_user div{
		padding: 5px;
		margin: 0px;
	}
	
	.hover_user:hover div{
		background-color:#c3c3c3;
	}
	
</style>
<script>
	var users = '.json_encode($users).';
	var users_email = '.json_encode($users_email).';
	var sel_usrs = [];
	var sel_usrs_cnt = 0;
	
	function toggle_div(id){
		if ($("#"+id).is(":hidden")){
			$("#toggle_indicator").html("<span class=\"h4\"> - </span>");
			$("#"+id).slideDown("slow");
		}else{
			$("#toggle_indicator").html("<span class=\"h4\"> + </span>");
			$("#"+id).slideUp("slow");
		}
	}
	
	function get_users(){
		
		var userType = $("select[name=\'userType\']").val();
		var servers = $("select[name=\'serid\']").val();
		var groupId = "";
		var userList = "";
		var usrfilter = [];
		var usrfilter_temp = [];
		if(servers.match(/_group/) != null){
			groupId = servers.replace(/_group/, "");	
		}
		
		// Filter the ids according to given data
		for(var userId in users){
			if((groupId != null) && (users[userId]["sgid_list"] != undefined && users[userId]["sgid_list"].indexOf(groupId) != -1) && ((users[userId]["type"] == userType) || (userType == "-1"))){
				usrfilter_temp[users[userId]["uid"]] = 1;
			}else if((users[userId]["serid_list"] != undefined && users[userId]["serid_list"].indexOf(servers) != -1) && ((users[userId]["type"] == userType) || (userType == "-1"))){
				usrfilter_temp[users[userId]["uid"]] = 1;
			}else if(servers == "-1"){
				if(userType == "-1"){
					usrfilter_temp[users[userId]["uid"]] = 1;
				}else if(users[userId]["type"] == userType){
					usrfilter_temp[users[userId]["uid"]] = 1;
				}
			}else if(servers == "-2" && users[userId]["serid_list"] == undefined){
				if(userType == "-1"){
					usrfilter_temp[users[userId]["uid"]] = 1;
				}else if(users[userId]["type"] == userType){
					usrfilter_temp[users[userId]["uid"]] = 1;
				}
			}
		}
		
		// Sort Users in ascending order of email
		for(var x in usrfilter_temp){
			if(usrfilter_temp[x]){
				usrfilter.push(x);
			}
		}
		
		usrfilter.sort(function(a, b){
			if(users_email[a] && users_email[b]){
				return users_email[a].localeCompare(users_email[b]);
			}
			return -1;			
		});
		
		email_count = 0;
		
		for(var x in usrfilter){
			userList += \'<div class="col-sm-6 usr"><label><input type="checkbox" class="ios" name="users[]" value="\'+usrfilter[x]+\'" /> &nbsp; \'+users_email[usrfilter[x]]+\'</label></div>\';
			email_count++;			
		}
		
		$("#users_count").html(email_count);

		$("#user-list").html(userList);
		
		if($("#user-list").html() == ""){
			if($("#user-list").css("display") == "block"){
				toggle_div(\'user-list\');
				toggle_div(\'user-list-options\');
			}
			return;
		}else{
			if($("#user-list").css("display") == "none"){
				toggle_div(\'user-list\');
				toggle_div(\'user-list-options\');
			}
		}
	}
	
	function process_users_slist(action, uid){
		
		if(action == "add"){
			$("#select_all").prop("checked", false);
			chk = $("#user-list input:checked");
			for(var i = 0; i < chk.length; i++){
				var va = chk[i].value
				sel_usrs[va] = 1;
			}
		}else if(action == "remove"){
			if(uid){
				delete sel_usrs[uid];
			}else{
				sel_usrs = [];
				sel_usrs_cnt = 0;
			}
		}
		
		var sel_usr_str = "";
		var sel_usrs_cnt1 = 0;
		
		for(var k in sel_usrs){
			sel_usr_str += "<div class=\"col-sm-6 hover_user\" id="+k+"><div class=\"col-xs-10\" >"+users_email[k]+"</div><div class=\"col-xs-1\" style=\"cursor: pointer;\" onclick=\"process_users_slist(\'remove\', "+k+")\" >X</div></div>";
			sel_usrs_cnt1++;
		}
		
		if(sel_usrs_cnt1 != 0 && sel_usrs_cnt == sel_usrs_cnt1){
			alert("'.$l['user_already_added'].'");
		}else{
			sel_usrs_cnt = sel_usrs_cnt1;
		}
		
		$("#to_div").html(sel_usr_str);
		$("#usr_selected_count").html(sel_usrs_cnt1);
		$("#hidden_users").val(Object.keys(sel_usrs).toString());
		get_users();
	}
	
	function plus_onmouseover(id){
		$("#"+id).attr("src", "'.$theme['images'].'admin/plus_hover.gif");
	}

	function plus_onmouseout(id){
		$("#"+id).attr("src", "'.$theme['images'].'admin/plus.gif");
	}

	tinymce.init({
		selector:"textarea",
		plugins: [
			"advlist autolink lists link image charmap print preview hr anchor pagebreak",
			"searchreplace wordcount visualblocks visualchars code fullscreen",
			"insertdatetime nonbreaking save table contextmenu directionality",
			"emoticons template paste textcolor"
		],
		toolbar1: "insertfile undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent blockquote| link image",
		toolbar2: "preview media | forecolor backcolor emoticons | formatselect fontselect fontsizeselect "
	});
	
	function validate_form(){
		
		if($("#mail [name=users]").val().trim() == ""){
			alert("'.$l['empty_user'].'");
			return false;
		}
		
		if($("#mail [name=subject]").val().trim() == ""){
			alert("'.$l['no_subject'].'");
			return false;
		}
		
		return true;
	}
	
	$(document).ready(function(){
		get_users();
	});
	
</script>
	<div class="adj_ht" style="width:82%; margin: 0px auto; padding: 0px; min-width: 370px; background-color: #f0f0f0; border: 1px solid #e0e0e0;">
		<div class="col-xs-2 adj_ht_cont text-center" style="background-color: #b0b0b0;" ><label class="control-label" for="serid">'.$l['server'].'</label></div>
		<div class="col-xs-10" style="padding: 0px;" ><select class="form-control adj_ht_cont" style="width: 100%;" name="serid" id="serid" onchange="get_users();">
			<option value="-1">'.$l['all'].'</option>
			<option value="-2">'.$l['no_server'].'</option>';
			foreach ($servergroups as $k => $v){
				echo '<option class="fhead" value="'.$k.'_group" >[Group]&nbsp;'.$v['sg_name'].'</option>';
				
				foreach ($servers as $m => $n){
					if($n['sgid'] == $k){
						echo '<option value="'.$n['serid'].'" >&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;'.$n['server_name'].'</option>';
					}
				}
			}
		echo '</select>
		</div>
	</div><br />
	<div class="adj_ht" style="width:82%; margin: 0px auto; padding: 0px; min-width: 370px; background-color: #f0f0f0; border: 1px solid #e0e0e0;">
		<div class="col-xs-2 adj_ht_cont text-center" style="background-color: #b0b0b0;" ><label class="control-label" for="userType" >'.$l['user_type'].'</label></div>
		<div class="col-xs-10" style="padding: 0px;"><select class="form-control adj_ht_cont" name="userType" id="userType" onchange="get_users();">
			<option value="-1">'.$l['all'].'</option>
			<option value="0">'.$l['user'].'</option>
			<option value="1">'.$l['admin'].'</option>
			<option value="2">'.$l['cloud'].'</option>
			</select>
		</div>
	</div><br />
	<div class="row adj_ht" style="width:82%; margin: 0px auto; min-width: 370px; background-color: #f0f0f0; color: #000; border: 1px solid #e0e0e0;">
		<div id="toggle_indicator" class="col-xs-4 text-left adj_ht_cont" style="cursor: pointer; font-weight: bold; margin: 0px;" onclick="toggle_div(\'user-list\');toggle_div(\'user-list-options\');">+</div>
		<div class="col-xs-4 text-center adj_ht_cont" >'.$l['user_list'].'&nbsp;&nbsp;(<span id="users_count"></span>)</div>
	</div>
	<div id="user-list" style="width:82%; margin: 0px auto; min-width: 370px; border: 1px solid #f0f0f0; padding: 10px; display:none; max-height:250px; overflow-x: hidden; background-color: #FFF;" class="togglediv"></div>
	<div class="row" id="user-list-options" style="width:82%; margin: 0px auto; min-width: 370px; background-color: #f0f0f0; color: #000; border: 1px solid #e0e0e0; display: none;">
		<div class="col-xs-6 text-center adj_ht_cont" style="clear: all; float: left;">
			<input type="checkbox" name="select_all" id="select_all" class="select_all" />
			<label style="display:inline; cursor: pointer;" for="select_all" >'.$l['selectall'].'</label>
		</div>
		<div class="col-xs-6 text-center adj_ht" >
			<button class="btn adj_ht_cont" onclick="process_users_slist(\'add\')" >'.$l['add'].'</button>
		</div>
	</div>
	<br />
<form id="mail"  accept-charset="'.$globals['charset'].'" action="" method="post" name="mail" class="form-horizontal" onsubmit="return validate_form();">
	<input type="hidden" name="users" id="hidden_users"/>
	<div style="width:82%; margin:0px auto;min-width: 370px;">
		<div class="row" style="border: 1px solid #e0e0e0; margin: 0px; padding: 0px; background-color: #f0f0f0;">
			<div class="col-xs-2 text-center adj_ht_cont" style="background-color: #b0b0b0; margin: 0px;">
				<label class="to_label" style="margin: 0px; padding: 0px;">'.$l['to'].'</label>
			</div>
			
			<div class="col-xs-10" style="background-color: #f0f0f0; margin:0px; padding: 0px;"></div>
			
			<div class="col-xs-12" style="border-radius: 0px; border: 1px solid #e0e0e0; overflow: auto; overflow-x: hidden; height: 100px; padding: 0px; margin: 0px; background-color: white;" name="to">
				<div id="to_div" class="row" style="padding: 1%;" ></div>
			</div>
			<div class="row adj_ht">
				<div class="col-xs-6 text-center adj_ht_cont" >'.$l['usr_selected'].': <span id="usr_selected_count">0</span></div>
				<div class="col-xs-6 text-center" >
					<button class="btn adj_ht_cont" onclick="process_users_slist(\'remove\'); return false;" >'.$l['remove_all'].'</button>
				</div>
			</div>
		</div><br />
		
		<div class="row adj_ht" style="border: 1px solid #e0e0e0; margin: 0px; background-color: #f0f0f0;">
			<div class="col-xs-2 adj_ht_cont" style="text-align: center; margin: 0px; background-color: #b0b0b0;">
				<label for="subject">'.$l['subject'].'</label>
			</div>
			<div class="col-xs-10" style="margin: 0px; padding: 0px;">
				<input type="text" id="subject" class="form-control adj_ht" name="subject" maxlength="180" style="margin: 0px; border-radius: 0px;" />
			</div>
		</div>
	</div><br />
	
	<textarea rows="20" name="mailbox"></textarea>
	
</br>
</br>
<center><input type="submit" name="submit" value="'.$l['submit'].'" class="btn"/></center>
</form>
</div>
';
softfooter();

}

?>
