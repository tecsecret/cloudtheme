<?php

//////////////////////////////////////////////////////////////
//===========================================================
// addips_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function addips_theme(){	

global $theme, $globals, $cluster, $user, $l, $ippools, $servers, $error, $done, $ips;

softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-ippool icon-head"></i>&nbsp; '.$l['addip'].'<span style="float:right;" ><a href="'.$globals['docs'].'Add_IP" target="_blank" class="wiki_help" title="'.$l['wiki_help'].'"><i class="icon-help" ></i></a></span></center>';

error_handle($error);

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
}

echo '<script language="javascript" type="text/javascript">
function addrow(id){
	var t = $_(id);

	var row = document.createElement("div");
	row.className = "row";
	var col1 = document.createElement("div");
	col1.className = "col-sm-6 ip-mac";
	var col2 = document.createElement("div");
	col2.className = "col-sm-6 ip-mac";

	col1.innerHTML = \'<label>'.$l['ip'].' :&nbsp;&nbsp;</label><input type="text" class="form-control" name="ips[]" size="20" />\';
	col2.innerHTML = \'<label>'.$l['mac_addr'].' :&nbsp;&nbsp;</label><input type="text" class="form-control" name="macs[]" size="20" />\';

	row.appendChild(col1);
	row.appendChild(col2);
	t.appendChild(row);

};

function addrow6(id){
	var t = $_(id);
	var lastrow = $(".ipv6-range").length - 1;
	var row = document.createElement("div");
	row.className = "ipv6-range";
	var col1 = document.createElement("div");
	var col2 = document.createElement("div");
	var col3 = document.createElement("div");
	var col4 = document.createElement("div");
	var col5 = document.createElement("div");
	var col6 = document.createElement("div");
	var col7 = document.createElement("div");
	var col8 = document.createElement("div");
	
	col1.innerHTML = \'<input type="text" class="form-control ips6box" name="ips6[\'+lastrow+\'][1]" value="" size="4" maxlength="4" /><label> : </label>\';
	col2.innerHTML = \'<input type="text" class="form-control ips6box" name="ips6[\'+lastrow+\'][2]" value="" size="4" maxlength="4" /><label> : </label>\';
	col3.innerHTML = \'<input type="text" class="form-control ips6box" name="ips6[\'+lastrow+\'][3]" value="" size="4" maxlength="4" /><label> : </label>\';
	col4.innerHTML = \'<input type="text" class="form-control ips6box" name="ips6[\'+lastrow+\'][4]" value="" size="4" maxlength="4" /><label> : </label>\';
	col5.innerHTML = \'<input type="text" class="form-control ips6box" name="ips6[\'+lastrow+\'][5]" value="" size="4" maxlength="4" /><label> : </label>\';
	col6.innerHTML = \'<input type="text" class="form-control ips6box" name="ips6[\'+lastrow+\'][6]" value="" size="4" maxlength="4" /><label> : </label>\';
	col7.innerHTML = \'<input type="text" class="form-control ips6box" name="ips6[\'+lastrow+\'][7]" value="" size="4" maxlength="4" /><label> : </label>\';
	col8.innerHTML = \'<input type="text" class="form-control ips6box" name="ips6[\'+lastrow+\'][8]" value="" size="4" maxlength="4" />\';

	row.appendChild(col1);
	row.appendChild(col2);
	row.appendChild(col3);
	row.appendChild(col4);
	row.appendChild(col5);
	row.appendChild(col6);
	row.appendChild(col7);
	row.appendChild(col8);
	
	t.appendChild(row);
};

function showserver(){
	if($_("ippid").value == 0){
		$_("ip_serid").style.display = "";
	}else{		
		$_("ip_serid").style.display = "none";
	}
};

function changetype(iptype){
	$_("ipv4").className = "col-sm-4";
	$_("ipv6").className = "col-sm-4";
	
	if (iptype == 4){
		$_("ips").style.display="";
		$_("ips6").style.display="none";
		$_("ip_range").style.display="";
		$_("gen_ipv6").style.display="none";
		$_("firstip").style.display="";
		$_("lastip").style.display="";
		$_("ipv6_range").style.display="none";
		$_("ipv6_num").style.display="none";
	}else{
		$_("ips").style.display="none";
		$_("ips6").style.display="";
		$_("ip_range").style.display="none";
		$_("gen_ipv6").style.display="";
		$_("firstip").style.display="none";
		$_("lastip").style.display="none";
		$_("ipv6_range").style.display="";
		$_("ipv6_num").style.display="";
	}		
	$_("ipv"+iptype).className = "active col-sm-4";
	$_("iptype").value = iptype;
	
	for(var i=0; i<$_("ippid").options.length; i++){

		// The NONE Value
		if($_("ippid").options[i].value == 0){
			continue;
		}	
		
		if(getAttributeByName($_("ippid").options[i], "ipv") == 4 && iptype == 4){
			$_("ippid").options[i].disabled = false;
		}else if(getAttributeByName($_("ippid").options[i], "ipv") == 6 && iptype == 6){
			$_("ippid").options[i].disabled = false;
		}else{
			$_("ippid").options[i].disabled = true;
		}
	}
	
	$(".virt-select").select2();
	
};

addonload("showserver();changetype('.POSTval('iptype', 4).');");
</script>


<div id="form-container">
<form accept-charset="'.$globals['charset'].'" name="addippool" method="post" action="" class="form-horizontal">

<input type="hidden" id="iptype" name="iptype" value="'.POSTval('iptype', 4).'">
<ul class="nav nav-tabs">
	<li class="col-xs-4"><label class="control-label">'.$l['iptype'].'</label></li>
	<li class="col-xs-4" id="ipv4" style="text-align:center"><a href="javascript:changetype(4);"><img src="'.$theme['images'].'admin/ipv4.gif" /></a></li>
	<li class="col-xs-4" id="ipv6" style="text-align:center"><a href="javascript:changetype(6);"><img src="'.$theme['images'].'admin/ipv6.gif" /></a></li>
</ul>
<span class="help-block"></span>
<div class="row" id="ips">
	<div class="col-sm-4">
		<label class="control-label">'.$l['enterip'].'</label>
		<span class="help-block">'.$l['enterip_exp'].'</span>
		<span class="help-block">'.$l['entermac_exp'].'</span>
	</div>
	<div class="col-sm-8">
		<div id="iptable" style="margin-left:-25px">';
		 
		 	//$ips = @$_POST['ips'];
			
			if(is_array($ips) && !empty($ips)){
				foreach($ips as $k => $ip){
					if(empty($ip)){
						unset($ips[$k]);
					}
				}
			}
			
			if(empty($ips)){
				$ips = array(NULL);
			}
		 
		 	foreach($ips as $ip){
				echo '<div class="row">
					<div class="col-sm-6 ip-mac">
						<label>'.$l['ip'].' :&nbsp;&nbsp;</label><input type="text" class="form-control" name="ips[]" value="'.$ip['ip'].'" size="30" />
					</div>
					<div class="col-sm-6 ip-mac">
						<label>'.$l['mac_addr'].' :&nbsp;&nbsp;</label><input type="text" class="form-control" name="macs[]" value="'.$ip['mac_addr'].'" size="30" />
					</div>
				</div>';
			}
		 		
		echo '</div>
		<input type="button" value="'.$l['add_more_ips'].'" onclick="addrow(\'iptable\')" class="go_btn" />
	</div>
</div>
<div class="row" id="ips6">
	<div class="col-sm-4">
		<label class="control-label">'.$l['enterip'].'</label>
		<span class="help-block">'.$l['enterip_exp_6'].'</span>
	</div>
	<div class="col-sm-8">
		<div id="iptable6">';
			$ips = @$_POST['ips6'];
			
			if(is_array($ips) && !empty($ips)){
				foreach($ips as $k => $ip){
					$ip_ = implode('', $ip);
					if(empty($ip_)){
						unset($ips[$k]);
					}
				}
			}
			
			if(empty($ips)){
				$ips = array(NULL);
			}
		 
		 	foreach($ips as $k => $ip){
				echo '<div class="ipv6-range">
						<div><input type="text" class="form-control" name="ips6['.$k.'][1]" value="'.$ip[1].'" size="4" maxlength="4" class="ips6box" /><label> : </label></div>
						<div><input type="text" class="form-control" name="ips6['.$k.'][2]" value="'.$ip[2].'" size="4" maxlength="4" class="ips6box" /><label> : </label></div>
						<div><input type="text" class="form-control" name="ips6['.$k.'][3]" value="'.$ip[3].'" size="4" maxlength="4" class="ips6box" /><label> : </label></div>
						<div><input type="text" class="form-control" name="ips6['.$k.'][4]" value="'.$ip[4].'" size="4" maxlength="4" class="ips6box" /><label> : </label></div>
						<div><input type="text" class="form-control" name="ips6['.$k.'][5]" value="'.$ip[5].'" size="4" maxlength="4" class="ips6box" /><label> : </label></div>
						<div><input type="text" class="form-control" name="ips6['.$k.'][6]" value="'.$ip[6].'" size="4" maxlength="4" class="ips6box" /><label> : </label></div>
						<div><input type="text" class="form-control" name="ips6['.$k.'][7]" value="'.$ip[7].'" size="4" maxlength="4" class="ips6box" /><label> : </label></div>
						<div><input type="text" class="form-control" name="ips6['.$k.'][8]" value="'.$ip[8].'" size="4" maxlength="4" class="ips6box" /> </div>
				</div>';
			}

			echo '
		</div>
		<input type="button" value="'.$l['add_more_ips'].'" onclick="addrow6(\'iptable6\')" class="go_btn"/>
	</div>
</div>

<br/>
<div class="row" id="ip_range"><div class="col-sm-12 tit" align="center"  style="font-size:25px;">'.$l['ip_range'].'</div></div>

<div class="row" id="gen_ipv6"><span class="help-block"></span><div class="col-sm-12 tit" align="center" style="font-size:25px;">'.$l['gen_ipv6'].'</div></div>

<span class="help-block"></span>
<div class="row" id="firstip">
	<div class="col-sm-4"><label class="control-label">'.$l['first_IP'].'</label></div>
	<div class="col-sm-4"><input type="text" class="form-control" name="firstip" value="'.POSTval('firstip', '').'" size="30" /></div>
	<div class="col-sm-4"></div>
</div>

<span class="help-block"></span>
<div class="row" id="lastip">
	<div class="col-sm-4"><label class="control-label">'.$l['last_IP'].'</label></div>
	<div class="col-sm-4"><input type="text" class="form-control" name="lastip" value="'.POSTval('lastip', '').'" size="30" /></div>
	<div class="col-sm-4"></div>
</div>

<div class="row" id="ipv6_range">
	<div class="col-sm-4"><label class="control-label">'.$l['ipv6_range'].'</label></div>
	<div class="col-sm-8">
		<div class="ipv6-range">
			<div> <input type="text" class="form-control" name="ipv6_1" value="'.POSTval('ipv6_1', '').'" size="4" maxlength="4"><label> : </label></div>
			<div> <input type="text" class="form-control" name="ipv6_2" value="'.POSTval('ipv6_2', '').'" size="4" maxlength="4"><label> : </label></div>
			<div> <input type="text" class="form-control" name="ipv6_3" value="'.POSTval('ipv6_3', '').'" size="4" maxlength="4"><label> : </label></div>
			<div> <input type="text" class="form-control" name="ipv6_4" value="'.POSTval('ipv6_4', '').'" size="4" maxlength="4"><label> : </label></div>
			<div> <input type="text" class="form-control" name="ipv6_5" value="'.POSTval('ipv6_5', '').'" size="4" maxlength="4"><label> : </label></div>
			<div> <input type="text" class="form-control" name="ipv6_6" value="'.POSTval('ipv6_6', '').'" size="4" maxlength="4"></div>
			<div style="width:15%"><label> : auto : auto </label></div>
		</div>
	</div>
</div>

<span class="help-block"></span>
<div class="row" id="ipv6_num">
	<div class="col-sm-4">
		<label class="control-label">'.$l['ipv6_num'].'</label>
		<span class="help-block">'.$l['ipv6_num_exp'].'</span>
	</div>
	<div class="col-sm-4">
		<input type="text" class="form-control" name="ipv6_num" value="'.POSTval('ipv6_num', 50).'" size="10">
	</div>
	<div class="col-sm-4"></div>
</div>

<div class="row">
	<div class="col-sm-4"><label class="control-label">'.$l['sel_ippool'].'</label></div>
	<div class="col-sm-4 server-select-lg">
		<select class="form-control virt-select" name="ippid" id="ippid" onchange="showserver()" style="width:100%">
			<option value="0">'.$l['ipp_none'].'</option>';	
		
			// Select the default IP Pool if there in the GET
			if(!POSTval('ippid') && !POSTval('submitip')){
				$_POST['ippid'] = optGET('ippid');
			}
			
			foreach($ippools as $k => $v){		 
				echo '<option value="'.$k.'" '.((POSTval('ippid') == $k) ? 'selected="selected"' : '').' ipv="'.(!empty($v['ipv6']) ? 6 : 4).'">'.$v['ippool_name'].'</option>';
			}
	
 		echo'</select>
	</div>
	<div class="col-sm-4"></div>
</div>

<span class="help-block"></span>
<div class="row" id="ip_serid">
	<div class="col-sm-4"><label class="control-label">'.$l['sel_server'].'</label></div>
	<div class="col-sm-4 server-select-lg">
		<select class="form-control virt-select" name="ip_serid" style="width:100%">';	
			foreach($servers as $k => $v){		 
				echo '<option value="'.$k.'" '.(POSTval('ip_serid') == $k ? 'selected="selected"' : '').'>'.$v['server_name'].'</option>';
			}
			
		 echo'</select>
		 <span class="help-block"></span>
	</div>
	<div class="col-sm-4"></div>
</div>
</div>
<br /><br />

<center><input type="submit" name="submitip" value="'.$l['submitip'].'" class="btn"/></center>
	
</form>

</div>
';

softfooter();

}

?>