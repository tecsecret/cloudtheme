<?php

//////////////////////////////////////////////////////////////
//===========================================================
// backup_plans_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 2.8.1
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Husain	
// Date:       10th Nov 2015
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function backup_plans_theme(){

global $theme, $globals, $kernel, $user, $l, $backup_plans, $error, $done;

softheader($l['<title>']);

echo '
<div class="bg" style="width: 99%">
<center class="tit">
<i class="icon icon-plans icon-head"></i>&nbsp; '.$l['backup_plans_tit'].'<span style="float:right"><a href="javascript:showsearch();"><img src="'.$theme['images'].'admin/search.gif" /></a></span></center>';

error_handle($error);

echo '<script language="javascript" type="text/javascript"><!-- // --><![CDATA[

function go_click() {
	
	var backup_list = new Array();
	
	$(".ios:checked").each(function() {
		backup_list.push($(this).val());
	});
	
	if(backup_list.length < 1){
		alert("'.$l['nothing_selected'].'");
		return false;
	}
	
	var action = $("#backup_task_select").val();
	
	if(action == 1) {
		change_backup_plan(backup_list, "enable");
	} else if(action == 2) {
		change_backup_plan(backup_list, "disable");
	} else if(action == 3) {
		Delbackup_plans(backup_list);
	} else {
		alert("'.$l['no_action'].'");
		return false;
	}
	
}

function change_backup_plan(backup_list, action) {
	
	var finalData = new Object();
	
	finalData["bpids"] = backup_list.join(",");
	finalData["action"] = action;
	
	$("#progress_bar").show();
	
	$.ajax({
		type: "POST",
		url: "'.$globals['index'].'act=backup_plans&api=json",
		data : finalData,
		dataType : "json",
		success: function(data){
			$("#progress_bar").hide();
			if("done" in data){
				alert("'.$l['action_completed'].'");
				location.reload(true);
			}
		},
		error: function(data) {
			$("#progress_bar").hide();
			return false;
		}
	});
	
}

function Delbackup_plans(backup_list){
	var backup_conf = confirm("'.$l['del_conf'].'");
	if(backup_conf == false){
		return false;
	}
	
	var finalData = new Object();
	finalData["delete"] = backup_list.join(",");

	//alert(finalData);
	//return false;
	
	$("#progress_bar").show();
	
	$.ajax({
		type: "POST",
		url: "'.$globals['index'].'act=backup_plans&api=json",
		data : finalData,
		dataType : "json",
		success: function(data){
			$("#progress_bar").hide();
			if("done" in data){
				alert("'.$l['action_completed'].'");
				location.reload(true);
			}
		},
		error: function(data) {
			$("#progress_bar").hide();
			//alert(data.description);
			return false;
		}
	});
	
	return false;
};

// ]]></script>

<div id="showsearch" style="display:'.(optREQ('search') || (!empty($backup_plans) && !empty($globals['showsearch'])) ? "" : "none").';">
<form accept-charset="'.$globals['charset'].'" name="backup_plans" method="GET" action="" class="form-horizontal">
<input type="hidden" name="act" value="backup_plans">
		
<div class="form-group_head">
  <div class="row">
    <div class="col-sm-2"></div>
    <div class="col-sm-2"><label>'.$l['bp_name'].'</label></div>
    <div class="col-sm-4"><input type="text" class="form-control" name="planname" id="planname" size="30" value="'.POSTval('planname','').'"/></div>
    <div class="col-sm-2" style="text-align: center;"><button type="submit" name="search" class="go_btn" value="Search"/>'.$l['submit'].'</button></div>
    <div class="col-sm-2"></div>
  </div>
</div>
</form>
<br />
<br />
</div>';

if($done){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['saved'].'</div><br />';
}
if(empty($backup_plans)){

	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.(optREQ('search') ? $l['no_res'] : $l['no_plans']).'</div>';
}else{

page_links($globals['num_res'], $globals['cur_page'], $globals['reslen']);
echo '<br /><br />
<form accept-charset="'.$globals['charset'].'" name="multi_dns" id="multi_dns" method="post" action="" class="form-horizontal">
<table class="table table-hover tablesorter">
<tr>
	<th align="center">'.$l['bpid'].'</th>
	<th align="center">'.$l['bp_name'].'</th>
	<th align="center">'.$l['backup_server'].'</th>
	<th align="center">'.$l['frequency'].'</th>
	<th align="center">'.$l['dir'].'</th>
	<th align="center">'.$l['disabled'].'</th>
	<th colspan="2" width="10">'.$l['manage'].'</th>
	<th><input type="checkbox" class="select_all" name="select_all" id="select_all"></th>
</tr>';
$i = 0;
foreach($backup_plans as $k => $v){
	echo '<tr>
		<td align="left">'.$v['bpid'].'</td>
		<td align="left">'.$v['plan_name'].'</td>
		<td>'.$v['backup_server'].'</td>
		<td align="center">'.$v['frequency'].'</td>
		<td align="center">'.$v['dir'].'</td>
		<td align="center">'.(empty($v['disabled']) ? $l['no'] : $l['yes']).'</td>
		<td align="center" width="10"><a href="?act=editbackup_plan&bpid='.$v['bpid'].'" title="Edit"><img src="'.$theme['images'].'admin/edit.png" /></a></td>	
		<td align="center" width="10"><a href="javascript:void(0);" onclick="return Delbackup_plans(['.$k.']);" title="Delete"><img src="'.$theme['images'].'admin/delete.png" /></td>
		<td width="20" valign="middle" align="center">
			<input type="checkbox" class="ios" name="backup_list[]" value="'.$k.'"/>
		</td>
	</tr>';
$i++;	
}
echo '</table>

<div class="row bottom-menu">
		
	<div class="col-sm-7"></div>
	<div class="col-sm-5"><label>'.$l['with_selected'].'</label>
		<select class="form-control" name="backup_task_select" id="backup_task_select">
			<option value="0">---</option>
			<option value="1">'.$l['ms_enable'].'</option>
			<option value="2">'.$l['ms_disable'].'</option>
			<option value="3">'.$l['ms_delete'].'</option>
		</select>&nbsp;
		<input type="submit" id ="backup_submit" class="go_btn" name="backup_submit" value="'.$l['go'].'" onclick="go_click(); return false;">
	</div>
</div>

</form>

<div id="progress_bar" style="height:125px; display:none">
	<br />
	<center>
		<font id="progress_txt" size="4" color="#222222">'.$l['action_msg'].'</font>
		<br>
		<br>
	</center>
	<table id="table_progress" width="500" height="28" cellspacing="0" cellpadding="0" border="0" align="center" style="border:1px solid #CCC; -moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px;background-color:#efefef;">
		<tbody>
			<tr>
				<td id="progress_color" width="100%" style="background-image: url(themes/default/images/bar.gif); -moz-border-radius: 4px; -webkit-border-radius: 4px; border-radius: 4px;"></td>
				<td id="progress_nocolor"> </td>
			</tr>
		</tbody>
	</table>
	<br>
	<center>
		'.$l['notify_msg'].'
	</center>
</div>

<br />
<center><a href="'.$globals['ind'].'act=addbackup_plan" class="link_btn">'.$l['add_backup_plan'].'</a></center>';

}

page_links($globals['num_res'], $globals['cur_page'], $globals['reslen']);

echo '</div>';
softfooter();

}
?>