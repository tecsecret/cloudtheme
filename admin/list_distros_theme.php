<?php

//////////////////////////////////////////////////////////////
//===========================================================
// list_distros_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function list_distros_theme(){

global $theme, $globals, $kernel, $user, $l, $oslist, $error, $installed, $done, $mgs, $distros;

softheader($l['<title>']);

echo '
<div class="bg" style="width: 99%">
<center class="tit"><i class="icon icon-ostemplates icon-head"></i> &nbsp; '.$l['tit_list_distros'].'</center>';

error_handle($error);

echo '<script language="javascript" type="text/javascript"><!-- // --><![CDATA[

function showsearch(){
	if($_("showsearch").style.display == ""){
	$_("showsearch").style.display="none";
	}else{
		$_("showsearch").style.display="";
	}
};

function conf_del(){
	return confirm("'.$l['conf_del'].'");
};

// ]]></script>';

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
}

echo '<table class="table table-hover tablesorter">

<tr>
	<th align="center" width="65">'.$l['head_logo'].'</th>
	<th align="center" width="65">'.$l['head_distro'].'</th>
	<th align="center" >'.$l['head_name'].'</th>
	<th align="center">'.$l['head_desc'].'</th>
	<th align="center" colspan="2">&nbsp;</th>
</tr>';
//r_print($distros);

$i = 1;

foreach($distros as $k => $v){

	echo'<tr>
	<td align="center"><img src='.distro_logo($k).' /></td>
	<td align="center">'.$v['distro'].'</td>
	<td align="left">'.$v['name'].'</td>
	<td align="left">'.$v['desc'].'</td>
	<td align="center" width="20"><a href="'.$globals['ind'].'act=add_distro&edit='.$v['distro'].'"  title="'.$l['edit_distro'].'"><img src="'.$theme['images'].'admin/edit.png" /></a></td>
	<td align="center" width="20">'.(empty($v['permanent']) ? '<a href="'.$globals['ind'].'act=list_distros&delete='.$v['distro'].'" onclick="return conf_del();"  title="'.$l['del_distro'].'"><img src="'.$theme['images'].'admin/delete.png" /></a>' : '&nbsp;').'</td>
	</tr>';
	
	$i++;
		
}

echo '</table><br /><br />
<center><a href="'.$globals['ind'].'act=add_distro" title="'.$l['add_distro'].'" class="link_btn">'.$l['add_distro'].'</a></center></div>';
		
softfooter();

}

?>