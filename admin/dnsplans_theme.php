<?php

//////////////////////////////////////////////////////////////
//===========================================================
// plans_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function dnsplans_theme(){

global $theme, $globals, $kernel, $user, $l, $dnsplans, $error, $done;

softheader($l['<title>']);

echo '
<div class="bg" style="width: 99%">
<center class="tit">
<i class="icon icon-plans icon-head"></i>&nbsp; '.$l['dnsplans_tit'].'<span style="float:right"><a href="javascript:showsearch();"><img src="'.$theme['images'].'admin/search.gif" /></a></span></center>';

error_handle($error);

echo '<script language="javascript" type="text/javascript"><!-- // --><![CDATA[


function Deldns(dnsplid){

	dnsplid = dnsplid || 0;
	
	// List of ids to delete
	var dns_list = new Array();
	
	if(dnsplid < 1){
		
		if($("#dns_task_select").val() != 1){
			alert("'.$l['no_action'].'");
			return false;
		}
		
		$(".ios:checked").each(function() {
			dns_list.push($(this).val());
		});
		
	}else{
		
		dns_list.push(dnsplid);
		
	}
	
	if(dns_list.length < 1){
		alert("'.$l['nothing_selected'].'");
		return false;
	}
	
	var dns_conf = confirm("'.$l['del_conf'].'");
	if(dns_conf == false){
		return false;
	}
	
	var finalData = new Object();
	finalData["delete"] = dns_list.join(",");

	//alert(finalData);
	//return false;
	
	$("#progress_bar").show();
	
	$.ajax({
		type: "POST",
		url: "'.$globals['index'].'act=dnsplans&api=json",
		data : finalData,
		dataType : "json",
		success: function(data){
			$("#progress_bar").hide();
			if("done" in data){
				alert("'.$l['action_completed'].'");
				location.reload(true);
			}
		},
		error: function(data) {
			$("#progress_bar").hide();
			//alert(data.description);
			return false;
		}
	});
	
	return false;
};

// ]]></script>

<div id="showsearch" style="display:'.(optREQ('search') || (!empty($dnsplans) && !empty($globals['showsearch'])) ? "" : "none").';">
<form accept-charset="'.$globals['charset'].'" name="dnsplans" method="GET" action="" class="form-horizontal">
<input type="hidden" name="act" value="dnsplans">
		
<div class="form-group_head">
  <div class="row">
    <div class="col-sm-2"></div>
    <div class="col-sm-2"><label>'.$l['sbyplan'].'</label></div>
    <div class="col-sm-4"><input type="text" class="form-control" name="planname" id="planname" size="30" value="'.POSTval('planname','').'"/></div>
    <div class="col-sm-2" style="text-align: center;"><button type="submit" name="search" class="go_btn" value="Search"/>'.$l['submit'].'</button></div>
    <div class="col-sm-2"></div>
  </div>
</div>
</form>
<br />
<br />
</div>';

if($done){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['saved'].'</div><br />';
}
if(empty($dnsplans)){

	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.(optREQ('search') ? $l['no_res'] : $l['no_plans']).'</div>';
}else{

page_links($globals['num_res'], $globals['cur_page'], $globals['reslen']);
echo '<br /><br />
<form accept-charset="'.$globals['charset'].'" name="multi_dns" id="multi_dns" method="post" action="" class="form-horizontal">
<table class="table table-hover tablesorter">
<tr>
	<th align="center">'.$l['dnspl_id'].'</th>
	<th align="center">'.$l['dnspl_name'].'</th>
	<th align="center">'.$l['dnspl_server'].'</th>
	<th align="center">'.$l['dnspl_maxdomains'].'</th>
	<th align="center">'.$l['dnspl_maxrecords'].'</th>
	<th align="center">'.$l['dnspl_ttl'].'</th>
	<th colspan="2" width="10">'.$l['manage'].'</th>
	<th><input type="checkbox" class="select_all" name="select_all" id="select_all"></th>
</tr>';
$i = 0;
foreach($dnsplans as $k => $v){
	echo '<tr>
		<td align="left">'.$v['dnsplid'].'</td>
		<td align="left">'.$v['plan_name'].'</td>
		<td>'.$v['dns_server_name'].'</td>
		<td align="center">'.$v['max_domains'].'</td>
		<td align="center">'.$v['max_domain_records'].'</td>
		<td align="center">'.$v['def_ttl'].'</td>
		<td align="center" width="10"><a href="?act=editdnsplan&dnsplid='.$v['dnsplid'].'" title="Edit"><img src="'.$theme['images'].'admin/edit.png" /></a></td>	
		<td align="center" width="10"><a href="javascript:void(0);" onclick="return Deldns('.$k.');" title="Delete"><img src="'.$theme['images'].'admin/delete.png" /></td>
		<td width="20" valign="middle" align="center">
			<input type="checkbox" class="ios" name="dnsplan_list[]" value="'.$k.'"/>
		</td>
	</tr>';
$i++;	
}
echo '</table>

<div class="row bottom-menu">
		
	<div class="col-sm-7"></div>
	<div class="col-sm-5"><label>'.$l['with_selected'].'</label>
		<select class="form-control" name="dns_task_select" id="dns_task_select">
			<option value="0">---</option>
			<option value="1">'.$l['ms_delete'].'</option>
		</select>&nbsp;
		<input type="submit" id ="dnsplan_submit" class="go_btn" name="dnsplan_submit" value="'.$l['go'].'" onclick="Deldns(); return false;">
	</div>
</div>

</form>

<div id="progress_bar" style="height:125px; display:none">
	<br />
	<center>
		<font id="progress_txt" size="4" color="#222222">'.$l['action_msg'].'</font>
		<br>
		<br>
	</center>
	<table id="table_progress" width="500" height="28" cellspacing="0" cellpadding="0" border="0" align="center" style="border:1px solid #CCC; -moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px;background-color:#efefef;">
		<tbody>
			<tr>
				<td id="progress_color" width="100%" style="background-image: url(themes/default/images/bar.gif); -moz-border-radius: 4px; -webkit-border-radius: 4px; border-radius: 4px;"></td>
				<td id="progress_nocolor"> </td>
			</tr>
		</tbody>
	</table>
	<br>
	<center>
		'.$l['notify_msg'].'
	</center>
</div>

<br />
<center><a href="'.$globals['ind'].'act=adddnsplan" class="link_btn">'.$l['add_dns_plan'].'</a></center>';

}

page_links($globals['num_res'], $globals['cur_page'], $globals['reslen']);

echo '</div>';
softfooter();

}
?>