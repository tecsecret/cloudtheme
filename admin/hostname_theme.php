<?php

//////////////////////////////////////////////////////////////
//===========================================================
// hostname_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function hostname_theme(){

global $theme, $globals, $cluster, $user, $l, $host, $current, $error, $done, $onboot; 

softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-config icon-head"></i>&nbsp; &nbsp;'.$l['<title>'].'</center><br />';

error_handle($error);

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['host_final'].'</div>';
}


echo '<div id="form-container">
<form accept-charset="'.$globals['charset'].'" action="" method="post" name="changehost" class="form-horizontal">
<div class="col-sm-12">
<div class="row">
	<div class="col-sm-5">
		<label class="control-label">Current Hostname :</label>
	</div>
	<div class="col-sm-7">
		<label class="val">'.$current.'</label>
		<span class="help-block"></span>
	</div>
</div>
<div class="row">
	<div class="col-sm-5">
		<label class="control-label">'.$l['new_host'].' :</label>
	</div>
	<div class="col-sm-7">
		<input type="text" class="form-control" name="newhost" id="newhost" size="30" value="'.POSTval('newhost').'" />
		<span class="help-block"></span>
	</div>
</div>

</div>
<br />
<br />
<center>
<input type="submit" value="'.$l['submit_button'].'" class="btn" name="changehost" />
</center>

<br />
<br />
</div>
</form>


</div>
';

softfooter();

}

?>
