<?php

//////////////////////////////////////////////////////////////
//===========================================================
// editiprange_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function editiprange_theme(){
	

global $theme, $globals, $cluster, $servers, $user, $l, $error, $ippools, $ip, $iprid, $done, $allowed_netmasks;

softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-ippool icon-head"></i> '.$l['editip'].'</center>';

error_handle($error);

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
}

echo '<form accept-charset="'.$globals['charset'].'" name="editiprange" method="post" action="" class="form-horizontal">

<div class="row">
	<div class="col-sm-5">
		<label class="control-label">'.$l['enterip'].'</label>
		<span class="help-block">'.$l['enterip_exp'].'</span>
	</div>
	<div class="col-sm-7">
		<input type="text" class="form-control" name="ip"  size="50" '.(empty($ip['vpsid']) ? ' value="'.POSTval('ip' , $ip['ip']).'"' : 'value="'.$ip['ip'].'" disabled="disabled" ').'/>
	</div>
</div>

<div class="row">
	<div class="col-sm-5">
		<label class="control-label">'.$l['netmask'].'</label>
		<span class="help-block">'.$l['netmask_exp'].'</span>
	</div>
	<div class="col-sm-7">';
		$default_netmask = (int) empty($_REQUEST['netmask']) ? $ip['ipr_netmask'] : $_REQUEST['netmask'];
		echo '<label style="float: left;padding: 2px 9px 0px 0px;font-size:20px;">/</label><select id="netmask" class="form-control" name="netmask" style="width:95%;">';
			foreach($allowed_netmasks as $k => $v){
				echo '<option value="'.$k.'" '.($default_netmask == $k ? 'selected="selected"' : '').'>'.$k.'</option>';
			}			
		echo '</select>
	</div>
</div>
<div class="row">
	<div class="col-sm-5">
		<label class="control-label">'.$l['lockedip_lbl'].'</label>
		<span class="help-block">'.$l['lockedip_exp'].'</span>
	</div>
	<div class="col-sm-7">
		<input type="checkbox" name="locked" '.POSTchecked('locked', $ip['locked']).' />
	</div>
</div>
<div class="row">
	<div class="col-sm-5">
		<label class="control-label">'.$l['ippool'].'</label>
	</div>
	<div class="col-sm-7">
		'.(empty($ippools[$ip['ippid']]['ippool_name']) ? '<i>'.$l['none'].'</i>' : $ippools[$ip['ippid']]['ippool_name']).'
	</div>
</div>
<br />
<center>
	<input type="submit" name="editip" class="btn" value="'.$l['editbtn'].'"/>
</center>
</div>
';

softfooter();

}

?>