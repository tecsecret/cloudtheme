<?php

//////////////////////////////////////////////////////////////
//===========================================================
// maintenance_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function maintenance_theme(){

global $theme, $globals, $cluster, $user, $l, $info, $error, $done, $servers;

softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-config icon-head"></i> &nbsp;'.$l['heading'].'</center>';

error_handle($error, '100%');

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['change_final'].'</div>';
}

// Is it offline ?
$hypervisor_status = $cluster->statewise($globals['server']);
if($hypervisor_status == 0 || $hypervisor_status == 2){

	echo '<div class="e_notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['server_status_'.$hypervisor_status].'</div>';
	
}else{

echo '<br /><center><font size="3"> '.$l['maintenance_server'].': <em><b>' . $servers[$globals['server']]['server_name'] .'</b> ID: ' . $globals['server'] . '</em></font></center><br ><br />';


echo '<div id="form-container">
<form accept-charset="'.$globals['charset'].'" name="maintenance" method="post" action="" class="form-horizontal">

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['soft_off'].'</label>
		<span class="help-block">'.$l['soft_off_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="checkbox" class="ios-slide" name="off" '.POSTchecked('off', $info['off']).' />
	</div>
</div>
<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['off_sub'].'</label>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="off_subject" size="30" value="'.POSTval('off_subject', $info['off_subject']).'" />
		<span class="help-block"></span>
	</div>
</div>
<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['off_message'].'</label>
	</div>
	<div class="col-sm-6">
		<textarea class="form-control" name="off_message" cols="30" rows="6">'.POSTval('off_message', $info['off_message']).'</textarea>
	</div>
</div>


</div>
<br />
<br />
<p align="center"><input type="submit" name="maintenance" value="'.$l['edit_settings'].'" class="btn"/></p>

<br />
<br />

</form>
</div>
';



}

softfooter();

}

?>