<?php

//////////////////////////////////////////////////////////////
//===========================================================
// editvs_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function editvs_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $cluster, $servers, $oslist, $done, $virt;
global $plans, $ips, $ips6, $ips6_subnet, $ips_int, $ipools, $vps, $servers, $ostemplates, $users, $isos, $resources, $mgs, $servergroups, $scripts, $iscripts, $storages, $stid, $disk_space, $os_check, $storage_uuids, $disks, $supported_nics, $cpu_modes, $backup_plans, $bus_driver, $ha_enabled;

//Is Ajax Mode on?
if(optGET('ajax')){	

	$planid = optGET('plan');
	$uid  = (int) optGET('uid');
	if(!empty($uid)){
		useriso($uid);
		$options = '';		
		$iso = array('iso', 'sec_iso');
		$res = array();
		foreach($iso as $isov){
			$options_eu_iso = $options_iso = '';			
			$options = '<option value="0">'.$l['none'].'</option>';
			foreach($isos as $k => $v){
				if(!empty($v['isuseriso'])){
					$options_eu_iso .= '<option value="'.$k.'" '.($vps[$isov] == $k ? 'selected="selected"' : "").'>'.$v['name'].'</option>';
				}else{
					$options_iso .= '<option value="'.$k.'" '.($vps[$isov] == $k ? 'selected="selected"' : "").'>'.$v['name'].'</option>';
				}
			}

			if(!empty($options_iso)){
				$options .= '<optgroup label="'.$l['admin_iso'].'">'.$options_iso.'</optgroup>';
			}			
			if(!empty($options_eu_iso)){
				$options .= '<optgroup label="'.$l['eu_iso'].'">'.$options_eu_iso.'</optgroup>';
			}
			$res[$isov] = $options;
		}		
		if(!empty($res)){
			echo json_encode($res);
		}
		return true;
	}
	
	echo 'var $plan = new Object();';
	
	foreach($plans[$planid] as $k => $v){
		
		if($k == 'dns_nameserver'){
			$tmp_dns = unserialize($v);
			echo '$plan["dns_nameserver"] = '.json_encode($tmp_dns).';';
			continue;
		}
		
		if($k == 'mgs'){
			$tmp_mgs = cexplode(',', $v);
			echo '$plan["mgs"] = '.json_encode($tmp_mgs).';';
			continue;
		}
		
		if($k == 'speed_cap' && !empty($v)){
			$v = _unserialize($v);
			echo '$plan["speed_cap_down"] = '.$v['down'].';
				$plan["speed_cap_up"] = '.$v['up'].';
			';
			continue;
		}		
	
		if($k == 'ippoolid'){
			echo '$plan["plan_ips"] = '.json_encode($ips).';';
			echo '$plan["internal_ips"] = '.json_encode($ips_int).';';
			echo '$plan["ipsv6"] = '.json_encode($ips6).';';
			echo '$plan["ipsv6_subnet"] = '.json_encode($ips6_subnet).';';
			continue;
		}
		
		echo '$plan["'.$k.'"] = "'.str_replace('"', '\"', $v).'";';
	}
	
	return true;
}

softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-vs icon-head"></i> '.$l['heading'].'</center>';

//echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; <a href="'.$globals['ind'].'act=editvs&vpsid='.$vps['vpsid'].'" >'.$l['new_editvs_note'].'</a></div>';

error_handle($error);

// Rescume mode checking. VPS edit not allowed if Rescue Mode is ON
if(!empty($vps['rescue'])){
	softfooter();
	return;
}

if(!empty($done)){
		
	$msg= '';
	$msg .= (!empty($done['done']) ? $l['done'] : '');
	
	// if any changes done then reboot msg dispaly
	if(!empty($done['mac']) || !empty($done['dns'])){
		$msg .= '<BR />'.$l['reboot'];
	}
	
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$msg.'</div>';	
}

echo '<script language="javascript" type="text/javascript">

var ips = new Object();';

foreach($ips as $k => $v){
	echo 'ips['.$k.'] = "'.$v['ip'].'";';
}
foreach($vps['ips'] as $k => $v){
	echo 'ips['.$k.'] = "'.$v.'";';
}

echo 'function addrow(id, val){

	var t = $_(id);
	var lastRow = (t.rows.length);
	var x= t.insertRow(lastRow);
	var y = x.insertCell(0);
	y.innerHTML = \'<input type="text" style="width:90%;float:left" class="form-control" name="ips[]" value="\'+(val || "")+\'" onblur="checkippool()" size="20" /></td><td><a class="delip" title="'.$l['rem_from_ips'].'"><img  src="'.$theme['images'].'admin/delete.png" width="12"/></a><br/><span class="help-block">&nbsp;</span>\';
	ipdel();
};

function toggle_driver_num(driver_type){
	$(".ide, .sata, .virtio, .scsi").hide();
	$("." + driver_type).show();
};

$(document).ready(function(){
	
	$("#editvps").click(function(){
		
		var space = new Array();
		var size = new Array();
		var storage = new Array();
		var disk_uuid = new Array();
		var tmp_space = new Object();
		
		size = $(".size").map(function(){
			return this.value;
		}).get();
		
		storage = $(".storages_list").map(function(){
			return this.value;
		}).get();
		
		disk_uuid = $(".disk_uuids").map(function(){
			return this.value;
		}).get();
		
		if($(".bus_driver")){
			bus_driver = $(".bus_driver").map(function(){
				return this.value;
			}).get();
		
			bus_driver_num = $(".bus_driver_num").map(function(){
				return this.value;
			}).get();
		}
		
		 for(i = 0; i < $(".size").length; i++){
		 
			 if($(".bus_driver")){
			 	tmp_space[i] = {size: size[i], st_uuid: storage[i], disk_uuid : disk_uuid[i], bus_driver: bus_driver[i], bus_driver_num: bus_driver_num[i]};
			 }else{
			 	tmp_space[i] = {size: size[i], st_uuid: storage[i], disk_uuid : disk_uuid[i]};
			 }
			
			space.push(tmp_space[i]);
		}
			
		var json_data = JSON.stringify(space);
		$("#hdd").val(json_data);
	});
	
	$("#editvs .chosen").chosen({width: "100%"});
});

function add_storage_list(id, val){
	
	var y = id.insertCell(2);
	var z = id.insertCell(3);

	var str = \'<select class="form-control storages_list" name="storages[]" '.($virt == 'vzk' ? 'disabled="disabled"' : '').'>\';';
	
	foreach($storages as $sk => $sv){
		echo 'str += \'<option value="'.$sv['st_uuid'].'" '.($sv['st_uuid'] == $storages[$stid]['st_uuid'] ? 'selected="selected"' : '').' '.((($virt == 'vzk') && ($vps['disks'][0]['st_uuid'] == $sv['st_uuid'])) ? 'selected="selected"' : '').'>'.$sv['name'].'&nbsp;('.$sv['disk_space'].' GB '.(empty($sv['oversell']) ? $l['free'] : $l['oversell_free']).')</option>\';';
	}
	
	echo 'str += \'</select>\';
	y.innerHTML = str;
	y.style.paddingTop = "10px";
	z.style.paddingTop = "10px";
	z.innerHTML = \'<a class="delstorage" title="'.$l['rem_storage'].'"><img  src="'.$theme['images'].'admin/delete.png" width="12" /></a>\';
	storagedel();

	
};

function add_storage(id, val){

	var t = $_(id);
	var lastRow = (t.rows.length);
	var x = t.insertRow(lastRow);
	var y = x.insertCell(0);
	var z = x.insertCell(1);

	y.innerHTML = \'<input name="size" class="form-control size" type="text" size="15" value="" />\';
	z.innerHTML = \'&nbsp;'.$l['space_gb'].'\';

	y.style.paddingTop = "10px";
	z.style.paddingTop = "10px";
	add_storage_list(x);
	
};

function add_storage_bus(id, val){

	var t = $_(id);
	var lastRow = (t.rows.length);
	var x = t.insertRow(lastRow);
	var y = x.insertCell(0);
	var z = x.insertCell(1);
	var p = x.insertCell(2);
	var q = x.insertCell(3);
	var r = x.insertCell(4);
	var s = x.insertCell(5);
	var j = x.insertCell(6);
	var k = x.insertCell(7);
	
	var str1 = \'<select class="form-control bus_driver" name="bus_driver[]" onchange="toggle_driver_num(this.value)" >\';';
						
	foreach($bus_driver as $bdk => $bdv){
		echo 'str1 += \'<option value="'.$bdk.'">'.$bdv.'</option>\';';
	}
							
	echo 'str1 += \'</select>\';
	

	z.innerHTML = \'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp\';
	z.style.paddingTop = "10px";
	var str2 = \'<select class="form-control bus_driver_num" name="bus_driver_num[]">\';';
	
	
	echo 'str2 += \'<option class = "sata virtio scsi" value="0" >0</option>\';';
	echo 'str2 += \'<option class = "sata virtio scsi" value="1" >1</option>\';';
	echo 'str2 += \'<option class = "ide sata virtio scsi" value="2" >2</option>\';';
	echo 'str2 += \'<option class = "ide sata virtio scsi" value="3" >3</option>\';';
	echo 'str2 += \'<option class = "virtio scsi" value="4" >4</option>\';';
	echo 'str2 += \'<option class = "sata virtio scsi" value="5" >5</option>\';';
	echo 'str2 += \'<option class = "virtio scsi" value="6" >6</option>\';';
	echo 'str2 += \'<option class = "virtio scsi" value="7" >7</option>\';';
	echo 'str2 += \'<option class = "virtio scsi" value="8" >8</option>\';';
	echo 'str2 += \'<option class = "virtio scsi" value="9" >9</option>\';';
	echo 'str2 += \'<option class = "virtio scsi" value="10" >10</option>\';';
	echo 'str2 += \'<option class = "virtio scsi" value="11" >11</option>\';';
	echo 'str2 += \'<option class = "virtio scsi" value="12" >12</option>\';';
	echo 'str2 += \'<option class = "virtio scsi" value="13" >13</option>\';';
	echo 'str2 += \'<option class = "virtio" value="14" >14</option>\';';
	echo 'str2 += \'<option class = "virtio" value="15" >15</option>\';';


	echo 'str2 += \'</select>\';
	
	var str3 = \'<span class="help-block">&nbsp;</span>\';
	
	r.innerHTML = \'<input name="size" class="form-control size" type="text" size="15" value="" />\';
	s.innerHTML = \'&nbsp;'.$l['space_gb'].'\';
	
	r.style.paddingTop = "10px";
	r.style.width = "100px";
	s.style.paddingTop = "10px";
					
	var str4 = \'<select class="form-control storages_list" name="storages[]" style="width: 90%;float: left;">\';';
	
	foreach($storages as $sk => $sv){
		echo 'str4 += \'<option value="'.$sv['st_uuid'].'" '.($sv['st_uuid'] == $storages[$stid]['st_uuid'] ? 'selected="selected"' : '').'>'.$sv['name'].'&nbsp;('.$sv['disk_space'].' GB '.(empty($sv['oversell']) ? $l['free'] : $l['oversell_free']).')</option>\';';
	}
	
	echo 'str4 += \'</select>\';
	
	y.innerHTML = str1;
	y.style.paddingTop = "10px";
	p.innerHTML = str2;
	p.style.paddingTop = "10px";
	q.innerHTML = str3;
	q.style.paddingTop = "10px";
	j.innerHTML = str4;
	j.style.paddingTop = "10px";
	k.style.paddingTop = "10px";
	k.innerHTML = \'<a class="delstorage" title="'.$l['rem_storage'].'"><img  src="'.$theme['images'].'admin/delete.png" width="12"/></a>\';
	toggle_driver_num("sata");
	storagedel();
};

function ipdel(){
	$(".delip").each(function(){
		$(this).unbind("click");
		$(this).click(function(){
			var parent = $(this).parent();
			parent.remove();
			checkippool();
		});
	});
}

function enable_accel(){
	
	if(is_checked("kvm_vga")){
		$("#enable_acceleration").show();
	}else{
		$("#enable_acceleration").hide();
	}
	
	change_checkbox("kvm_vga");
}

function change_checkbox(id){
	
	if(is_checked(id)){
		$("#hidden_"+id).val(1);
		$("#hidden_"+id).attr("disabled", false);
	}else{
		$("#hidden_"+id).val(0);
		$("#hidden_"+id).attr("disabled", true);
	}
}

function storagedel(){
	$(".delstorage").each(function(){
		$(this).unbind("click");
		$(this).click(function(){
			var parent = $(this).parent();
			
			if($(this).prev().prev().attr("disabled") == "disabled"){
				var y = confirm("'.$l['del_storage_warn'].'");
				if(y == false){
					return;
				}
			}
			var granparent = parent.parent();
			parent.remove();
			granparent.remove();
		});
	});
}


$(document).ready(function(){
	ipdel();
	storagedel();
});

function adddnsrow(id, val){
	var t = $_(id);
	var ele = document.createElement("div");
    ele.setAttribute("class","row");
    ele.innerHTML=\'<div class="col-sm-6"><input type="text" class="form-control" name="dns[]"  value="\'+(val || "")+\'"  size="20" /></div>\';
    t.appendChild(ele);

    var sp = document.createElement("span");
    sp.setAttribute("class","help-block");
    t.appendChild(sp);
	
};

function checkippool(){
	try{
	// Clear the Select
	while($_("iplist").options.length != 0){
		$_("iplist").remove(0);
	}
	
	vpsips = curips();
	
	for(x in ips){
		is_cur = 0;	
		for(xx in vpsips){
			if(vpsips[xx] == ips[x]){
				is_cur = 1;
			}
		}
		if(is_cur == 0){
			createOption(x, ips[x]);
		}
	}
	}catch(e){	}
	
	if($("#iplist option").length === 0){
		$("#iplist_parent").hide();
	}else{
		$("#iplist_parent").show();
	}
};

function addtoips(){
	try{
	for(i=0; i < $_("iplist").options.length; i++) {
		if ($_("iplist").options[i].selected) {
			addrow("iptable", ips[$_("iplist").options[i].value]);
		}
	}
	
	checkippool();
	
	// Make the Table again
	while($_("iptable").rows.length != 0){
		$_("iptable").deleteRow(0);
	}
	
	for(x in vpsips){
		addrow("iptable", vpsips[x]);
	}
	}catch(e){	}
};

function createOption(val, txt){
	var opt = document.createElement("option");
	opt.text = txt;
	opt.value = val;
	
	if(document.all && !window.opera){
		$_("iplist").add(opt);
	}else{
		$_("iplist").add(opt, null);
	}
};

function curips(){
	var vpsips = new Object();
	var cur_ips = document.getElementsByName("ips[]");
	for(i=0; i < cur_ips.length; i++){
		if(cur_ips.item(i).value != ""){
			vpsips[i] = cur_ips.item(i).value;
		}
	}
	return vpsips;
};

function planips(num){
	if(num > 0){
		try{
			for(i=0; i < num; i++) {
				$_("iplist").options[i].selected = true;
			}
		}catch(e){	}
		addtoips();
	}
};

function countipv6(){
	try{
		var num = 0;
		for(i=0; i < $_("ipv6").options.length; i++) {
			if($_("ipv6").options[i].selected){
				$_("ipv6").options[i].selected = true;
				num = num + 1;
			}
		}
		$_("ipv6count").value = num;
	}catch(e){	}
};

function loadplan(planid){
	
	if(planid == 0){
		return false;
	}
	
	if(AJAX("'.$globals['index'].'act=editvs&vpsid='.$vps['vpsid'].'&ajax=true&plan="+planid+"&plid="+planid, "plandetails(re)")){
		return false;
	}else{
		return true;	
	}
};

function loadiso(uid){
	
	if(uid == 0){
		return false;
	}
	
	if(AJAX("'.$globals['index'].'act=editvs&vpsid='.$vps['vpsid'].'&ajax=true&uid="+uid, "isodetails(re)")){
		return false;
	}else{
		return true;
	}	
};

function isodetails(re){
	var re = $.parseJSON(re);	
	$.each(re, function(k,v){		
		if((k == "iso") && ($("#iso").length > 0)){
			$("#iso").empty().append(v).trigger("chosen:updated");
		}
		if((k == "sec_iso") && ($("#sec_iso").length > 0)){
			$("#sec_iso").empty().append(v).trigger("chosen:updated");
		}
	});
};

function plandetails(re){
	eval(re);
	
	try{
		if($plan["virtio"] == 1){
			if(!$_("virtio").checked){
				virtio_warning();
			}
		}else{
			if($_("virtio").checked){
				virtio_warning();
			}
		}
	}catch(e){
		// Do not do anything !
	}
	
	var topology_enabled = 0;
	
	$.each($plan, function( index, value ){
		
		if(index == "io") index = "priority";
		if(index == "swap") index = "swapram";
		
		if(index == "mgs"){
			$("#mg option:selected").removeAttr("selected");
			for(var i=0; i<$_("mg").options.length; i++){
				$_("mg").options[i].style.display = "";
				$($_("mg").options[i]).prop("selected", true);
				if($_("mg").options[i].value != value[i]){
					$_("mg").options[i].style.display = "none";
				}
			}
		}else if(index == "dns_nameserver" && (typeof value == "object")){
			$("#dnstable").html("");
			$.each(value, function( ind, val ){
				adddnsrow(\'dnstable\', val);
			});			
		}else if(index == "plan_ips" && (typeof value == "object")){
			$("#iplist").html("");
			ips = [];
			$.each(value, function( ind, val ){
				let row = "<option value="+ind+">"+val.ip+"</option>";
				$("#iplist").append(row);
				ips[ind] = val.ip;
			});			
		}else if(index == "internal_ips" && (typeof value == "object")){
			$("#ips_int").html("");
			ips_int = [];
			$.each(value, function( ind, val ){
				let row = "<option value="+val.ip+">"+val.ip+"</option>";
				$("#ips_int").append(row);
			});
		}else if(index == "ipsv6" && (typeof value == "object")){
			$("#ipv6").html("");
			ipv6 = [];
			$.each(value, function( ind, val ){
				let row = "<option value="+val.ip+">"+val.ip+"</option>";
				$("#ipv6").append(row);
			});
		}else if(index == "ipsv6_subnet" && (typeof value == "object")){
			$("#ipv6_subnet").html("");
			ipv6_subnet = [];
			$.each(value, function( ind, val ){
				let row = "<option value="+val.ip+">"+val.ip+"</option>";
				$("#ipv6_subnet").append(row);
			});
		}else if(index == "bpid"){
			// Do nothing since bpid should remain as -1
		}else{
			
			if($("#editvs").find("[name=show_"+ index +"]").prop("type") == "checkbox"){
				index = "show_"+index;
			}
			
			$("#editvs").field( index, value);
			
			if((index == "topology_sockets" || index == "topology_cores" || index == "cpu_threads") && (!empty(value)) && (empty(topology_enabled))){
				$("#enable_cpu_topology").prop("checked", true);
				topology_enabled = 1;
			}
		}
	});
	
	$_("hdd_0").value = $plan["space"];
	
	$("#ipv6count").val(parseInt($plan["ips6"]));
	ipchoose("ipv6", $plan["ips6"]);
	ipchoose("ips_int", $plan["ips_int"]);
	ipchoose("ipv6_subnet", $plan["ips6_subnet"]);

	if(document.getElementsByName("ips[]").length < $plan["ips"]){
		var diff = $plan["ips"] - document.getElementsByName("ips[]").length;
		planips(diff);
	}
	
	checkvnc();
	change_cpu_topology();
	handle_capping();
};

function changepriority(priority){
	if(!$_("prior")){
		return false;
	}
	$_("prior").value = priority;
};

function pincheck(){
	if(!$_("allcores")){
		return false;
	}
	if(is_checked("allcores")){';
		for($i=0; $i < $resources['cpucores']; $i++){
			echo '$_("pin'.$i.'").checked = 0;';
		}
		echo '$("#pincores").hide();
	}else{
		$("#pincores").show();
	}
};

function addvbutton(){
	if($_("macaddress").style.display == "none"){
		$_("macaddress").style.display = ""; 
		$_("addv_opt_label").innerHTML = "'.$l['addvhide'].'";  	
		$_("addv_opt_button").value = "'.$l['addvhide'].'";     
    } else {
		$_("macaddress").style.display = "none";
		$_("addv_opt_label").innerHTML = "'.$l["addvoption"].'";  
		$_("addv_opt_button").value = "'.$l["addvoption"].'";  	    
	}	
}

function toggle_advoptions(){
	if ($("#advoptions").is(":hidden")){
		$("#advoptions").slideDown("slow");
		$("#toggle_indicator").text(" - ");
	}
	else{
		$("#advoptions").slideUp("slow");
		$("#toggle_indicator").text(" + ");
	}
}

function plus_onmouseover(){
	$("#plus").attr("src", "'.$theme['images'].'admin/plus_hover.gif");
}

function plus_onmouseout(){
	$("#plus").attr("src", "'.$theme['images'].'admin/plus.gif");
}

function checkvnc(){
	
	if(!$("#vnc")){
		return false;
	}
	
	if(is_checked("vnc")){
		$("#vncpassrow").show();
	}else{
		$("#vncpassrow").hide();
	}
	change_checkbox("vnc");
};

function ispvonhvm(){
	
	var pv_on_hvm = false;
	if(!$("#pv_on_hvm")){
		pv_on_hvm = false;
	}else{
		if(is_checked("pv_on_hvm")){
			pv_on_hvm =  true;
		}
	}
	
	if($("#tr_viftype")){
		if(pv_on_hvm){
			$("#tr_viftype").hide();
		}else{
			$("#tr_viftype").show();
		}
	}
	change_checkbox("pv_on_hvm");
	return true;
};

var lang = Array();
lang["bad"] = "'.$l['bad'].'";
lang["good"] = "'.$l['good'].'";
lang["strong"] = "'.$l['strong'].'";
lang["short"] = "'.$l['short'].'";
lang["strength_indicator"] = "'.$l['strength_indicator'].'";

function virtio_warning(){
	
	var isenabled = $_("virtio").checked;
	
	var r = confirm("'.$l['virtio_warning'].'");
	
	if(!r){
		
		// preserve the state if user click on cancel
		// we do this invertly as isenabled get the state as after change bcoz event is called on onchange.
		if(isenabled){
			$_("virtio").checked = 0;
		}else{
			$_("virtio").checked = 1;
		}
	}
	
	change_checkbox("virtio");
};

function custom_valert(output_msg, title_msg)
{
	if (!title_msg)
		title_msg = "Success!";

	if (!output_msg)
		output_msg = "No Message to Display.";

	$("<div></div>").html(output_msg).dialog({
		title: title_msg,
		resizable: true,
		buttons: {
            "Close": function() 
            {
                $(this).dialog( "close" );
            }
        },
		modal: true,
		width: 350,
		maxHeight: 250
	});
}


function edit_action(action){
	var id = '.$vps['vpsid'].'; 
	
	$("#"+action).attr("src","'.$theme['images'].'loading_50.gif");
	
	if(action == "start" || action == "stop" || action == "restart" || action == "poweroff" || action == "ssh" || action == "suspend" || action == "unsuspend" || action == "suspend_net" || action == "unsuspend_net"){
	
		$.ajax({type: "POST",
			url: "'.$globals['index'].'act=vs&action="+action+"&vpsid="+id+"&api=json&random="+Math.random(),
			success:function(response){
				response_data = JSON.parse(response);
				
				if(response_data.error_msg){
					custom_valert(response_data.error_msg, "'.$l['error'].'");
				}else if(response_data.done_msg){
					custom_valert(response_data.done_msg);
				}else{
					custom_valert("'.$l['alert_failure'].'", "'.$l['error'].'");
				}
				$("#"+action).attr("src","'.$theme['images'].'"+action+".png");
			}
		});
	}
};

function launchvnc(type){
	if(type === 1){
		var thisURL = window.location.href;
		thisURL = thisURL.toString();
		thisURL = thisURL.replace("http:", "https:");
		thisURL = thisURL.replace(":4084", ":4085");
		window.open("'.$globals['index'].'act=vnc&novnc='.$vps['vpsid'].'", "_blank", "height=400,width=720");
	}else if(type === 2){
		AJAX("'.$globals['index'].'act=vnc&ajax='.$vps['vpsid'].'", "vnchandle(re)");
	}
};

function vnchandle(resp){
	$("#vnc_launcher").html(resp);
}

$(document).ready(function(){
	
	// We will just disable the hidden checkboxes and check the parent element just for safe side tha both are for checkbox only
	$.each($("input[type=\'hidden\']"), function (index, value) {
		if($(this).prev().prop("type") == "checkbox" && $(this).prev().prop("name") == "show_"+$(this).prop("name") && !$(this).prev().is(":checked")){
			$(this).attr("disabled", true);
			//alert($(this).prev().prop("id") +" "+$(this).prev().prop("type") + " --"+ $(this).prop("id"))
		}
	});
	
	if($("#iplist option").length === 0){
		$("#iplist_parent").hide();
	}';
	
	// if($vps['virt'] == 'xen' && $vps['_virt'] != 'xenhvm'){
	// 	echo '$("#tr_viftype").hide();';
	// }
	
	if(!$kernel->features('multiple_disk_support', $vps['_virt'])){
		echo '$_("add_storage").style.display = "none";';
	}
	
echo '});

function change_cpu_topology(){
	
	if($("#enable_cpu_topology").prop("checked")){
		$("#cpu_topology").css("display", "");
	}else{
		$("#cpu_topology").css("display", "none");
		$("#topology_sockets").val(0);
		$("#topology_cores").val(0);
		$("#topology_threads").val(0);
	}
}

var lang_no_limit = "'.$l['no_limit'].'";

addonload("handle_capping(); fillspeedmbits(); planips(0); checkippool(); changepriority(\''.POSTval('priority', $vps['io']).'\'); checkvnc(); pincheck(); ispvonhvm();check_pass_strength(\'rootpass\');enable_accel();change_cpu_topology();");
</script>
<div id="vnc_launcher" style="position:absolute;width:0px;left:-100%"></div>

		<div class="row">
		<center>
		<div class="headericonstd">
			<a href="javascript: void(0);" target="" onclick="edit_action(\'start\')"><img id="start" src="'.$theme['images'].'start.png" /><br /><br />'.$l['title_start'].'</a>
		</div>
		<div class="headericonstd">
			<a href="javascript: void(0);" target="" onclick="edit_action(\'stop\')"><img id="stop" src="'.$theme['images'].'stop.png" /><br /><br />'.$l['title_stop'].'</a>
		</div>
		<div class="headericonstd">
			<a href="javascript: void(0);" target="" onclick="edit_action(\'restart\')"><img id="restart" src="'.$theme['images'].'restart.png" /><br /><br />'.$l['title_restart'].'</a>
		</div>
		<div class="headericonstd">
			<a href="javascript: void(0);" target="" onclick="edit_action(\'poweroff\')"><img id="poweroff" src="'.$theme['images'].'poweroff.png" /><br /><br />'.$l['title_poweroff'].'</a>
		</div>
		'.(($vps['virt'] != 'openvz' && $vps['virt'] != 'lxc')?(!empty($globals['novnc']) ? 
		'<div class="headericonstd">
			<a href="javascript:void(0)" id="novncURL" class="vncButton" onclick="launchvnc(1)"><img style="padding-left:4px" src="'.$theme['images'].'vnc.png" title="'.(!empty($l['vpmenu_novnc'])?$l['vpmenu_novnc']:'').'"/><br /><br />'.$l['title_vnc'].'</a>
		</div>' : (empty($globals['disable_java_vnc']) ?

		'<div class="headericonstd">
						  <a href="javascript:void(0)" id="java_vnc" class="vncButton" onclick="launchvnc(2)"/><img style="padding-left:4px" src="'.$theme['images'].'vnc.png" title="'.$l['vpmenu_javavnc'].'"/><br /><br />'.$l['title_vnc'].'</a></div>' : '')) : '').'
		
		<div class="headericonstd">
			<a href="javascript: void(0);" target="" onclick="edit_action(\'suspend\')"><img id="suspend" src="'.$theme['images'].'suspend.png" /><br /><br />'.$l['title_suspend'].'</a>
		</div>
		<div class="headericonstd">
			<a href="javascript: void(0);" target="" onclick="edit_action(\'unsuspend\')"><img id="unsuspend" src="'.$theme['images'].'unsuspend.png" /><br /><br />'.$l['title_unsuspend'].'</a>
		</div>
		</center>
	</div>
<br />
<br />

<div id="form-container">
<div class="divroundshad">
<div class="roundheader">'.$l['<title>'].'</div>
<form  accept-charset="'.$globals['charset'].'" action="" method="post" id="editvs" name="editvs" class="form-horizontal">

<div class="row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['vs_server'].'</label>
		<span class="help-block">'.$l['exp_server'].'</span>
	</div>
	<div class="col-sm-4">
		<span class="val">'.$servers[$vps['serid']]['server_name'].'</span> ('.$l['vs_ser_id'].': <i>'.$vps['serid'].'</i>) ('.$l['vpsname'].': '.$vps['vps_name'].')
		<input type="hidden" name="serid" value="'.$vps['serid'].'" />
	</div>
	<div class="col-sm-4"></div>
</div>

<div class="row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['vs_user'].'</label>
		<span class="help-block">'.$l['vs_user_exp'].'</span>
	</div>
	<div class="col-sm-4 server-select-lg">
		<select class="form-control virt-select" name="uid" id="uid" onchange="loadiso(this.value)" style="width:100%">';
	foreach($users as $k => $v){		 
		echo '<option value="'.$k.'" '.(POSTval('uid') == $k ? 'selected="selected"' : ($vps['uid'] == $k ? 'selected="selected"' : '')).'>'.$v['email'].'</option>';
	}
	
echo'</select>
	</div>
	<div class="col-sm-4"></div>

</div>

<div class="row"> 
	<div class="col-sm-4">
		<label class="control-label">'.$l['vs_plan'].'</label>
		<span class="help-block">'.$l['plan_exp'].'</span>
	</div>
	<div class="col-sm-4 server-select-lg">
		<select class="form-control virt-select" name="plid" onchange="loadplan(this.value)" style="width:100%">';
	 foreach($plans as $k => $v){		 
		echo '<option value="'.$k.'" '.(POSTval('plid') == $k ? 'selected="selected"' : ($vps['plid'] == $k ? 'selected="selected"' : '')).'>'.$v['plan_name'].'</option>';
	}
 echo'</select>
	</div>
	<div class="col-sm-4"></div>
</div>';
	
// Is webuzo enabled ?
if(!empty($iscripts) && $ostemplates[$vps['osid']]['distro'] == 'webuzo'){

echo '<div class="row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['apps'].'</label><br />
		<span class="help-block">'.$l['apps_exp'].'</span>
	</div>
	<div class="col-sm-4">
	
	<select class="form-control" name="sid" id="sid">
	<option value="0" '.(POSTval('sid') == 0 ? 'selected="selected"' : '').'>'.$l['none'].'</option>';
	
foreach($iscripts as $k => $v){		
	echo '<option value="'.$k.'" '.(POSTval('sid', $vps['webuzo']) == $k ? 'selected="selected"' : '').'>'.$scripts[$k]['name'].'</option>';
}		
     echo'</select>
	</div>
	<div class="col-sm-4"></div>
</div>';

}

echo '<div class="row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['vsos'].'</label><br />
		<span class="help-block">'.$l['vsos_exp'].'</span>
	</div>
	<div class="col-sm-4">
		'.$vps['os_name'].'
	</div>
	<div class="col-sm-4"></div>
</div>';

if(!empty($isos)){
	echo '<div class="row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['vsiso'].'</label><br />
		<span class="help-block">'.$l['vsiso_exp'].'</span>
	</div>
	<div class="col-sm-4">

	<select class="form-control chosen" name="iso" id="iso">
	<option value="0" '.(POSTval('iso') == 0 ? 'selected="selected"' : '').'>'.$l['none'].'</option>';
	
	$options_eu_iso = $options_iso = '';
	
	foreach($isos as $k => $v){
		if(!empty($v['isuseriso'])){
			$options_eu_iso .= '<option value="'.$k.'" '.(POSTval('iso') == $k ? 'selected="selected"' : ($vps['iso'] == $k ? 'selected="selected"' : '')). '>'.$v['name'].'</option>';
		}else{
			$options_iso .= '<option value="'.$k.'" '.(POSTval('iso') == $k ? 'selected="selected"' : ($vps['iso'] == $k ? 'selected="selected"' : '')).'>'.$v['name'].'</option>';
		}
	}

	if(!empty($options_iso)){
		echo '<optgroup label="'.$l['admin_iso'].'">'.$options_iso.'</optgroup>';
	}
	
	if(!empty($options_eu_iso)){
		echo '<optgroup label="'.$l['eu_iso'].'">'.$options_eu_iso.'</optgroup>';
	}
	
    echo'</select>
	</div>
	<div class="col-sm-4"></div>
</div>';

}

echo '<div class="row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['hostname'].'</label><br />
		<span class="help-block">'.$l['hostname_exp'].'</span>
	</div>
	<div class="col-sm-4">
		<input type="text" class="form-control" name="hostname" id="hostname" size="30" value="'.POSTval('hostname', $vps['hostname']).'" />
	</div>
	<div class="col-sm-4"></div>
</div>

<div class="row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['rootpass'].'</label><br />
		<span class="help-block">'.$l['rootpass_exp'].'</span>
	</div>
	<div class="col-sm-4">
		<input type="text" class="form-control" name="rootpass" id="rootpass" size="30" onkeyup="check_pass_strength(\'rootpass\');" value="'.POSTval('rootpass', '').'" autocomplete="off" /><div id="rootpass_pass-strength-result" class="">'.$l['strength_indicator'].'</div>
	</div>
	<div class="col-sm-4">
		<a href="javascript: void(0);" onclick="$_(\'rootpass\').value=rand_pass(12);check_pass_strength(\'rootpass\');return false;" title="'.$l['randpass'].'"><img src="'.$theme['images'].'randpass.gif" /></a>
	</div>
</div>

<div class="row hide_for_plan">
	<div class="col-sm-4">
		<label class="control-label">'.$l['ip'].'</label><br />
		<span class="help-block">'.$l['ips_exp'].'</span>
	</div>
	<div class="col-sm-4">
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr>
			<td valign="top">
				<table cellpadding="3" cellspacing="0" class="" id="iptable" style="width:100%;">';
		 
				$_ips = @(empty($_POST['ips']) ? $vps['ips'] : $_POST['ips']);
				
				if(is_array($_ips) && !empty($_ips)){
					foreach($_ips as $k => $ip){
						if(empty($ip)){
							unset($_ips[$k]);
						}
					}
				}
				
				if(empty($_ips)){
					$_ips = array(NULL);
				}
			 
				foreach($_ips as $ip){
					echo '<tr>
						<td>
							<input type="text" class="form-control" style="width:90%;float:left;" name="ips[]" value="'.$ip.'" onblur="checkippool();" size="20" />
							<a class="delip" title="'.$l['rem_from_ips'].'"><img src="'.$theme['images'].'admin/delete.png" width="12" /></a>
							<br/><span class="help-block">&nbsp;</span>
						</td>
					</tr>';
				}
		 		
			echo '</table>
				
				<input type="button" class="go_btn" onclick="javascript:addrow(\'iptable\');" value="'.$l['add_ip'].'"/>
			</td>
		</tr>
		</table>
		<span class="help-block">&nbsp;</span>
	</div>
	<div class="col-sm-4">
		<div id="iplist_parent">
			<select class="form-control" id="iplist" size="5" multiple="multiple">';
			foreach($ips as $k => $v){	 
				echo '<option value="'.$k.'" '.(POSTval('osid') == $k ? 'selected="selected"' : '').'>'.$v['ip'].'</option>';
			}		
			echo'</select><br /><br />
			<a href="javascript:addtoips(\'iptable\');" class="go_btn">'.$l['add_to_ips'].'</a>
			<br />
		</div>
	</div>
	<span class="help-block">&nbsp;</span>
</div>';

if(!empty($ips6_subnet) || !empty($vps['ips6_subnet'])){

echo '<div class="row hide_for_plan">
	<div class="col-sm-4">
		<label class="control-label">'.$l['ips6_subnet'].'</label>
		<span class="help-block">'.$l['ips6_subnet_exp'].'</span>
	</div>
	<div class="col-sm-4">';	
	// Then the VPS IPs
	foreach($vps['ips6_subnet'] as $k => $v){
		$tmp = explode('/', $v);
		$_ipv6_subnet[$k] = array('ip' => $tmp[0], 'ipr_netmask' => $tmp[1]);
	}
	
	// Then the extra IPs
	foreach($ips6_subnet as $k => $v){
		$_ipv6_subnet[$k] = array('ip' => $v['ip'], 'ipr_netmask' => $v['ipr_netmask']);
	}
	
	// This is to put the Posted IPs first
	if(!empty($_POST['ipv6_subnet'])){

		$posted_subnet = array();
		
		foreach($_POST['ipv6_subnet'] as $k => $v){
			//$_ipv6_subnet[$v] = $v;
			
			foreach($_ipv6_subnet as $_k => $_v){
				if($v == $_v['ip']){
					$posted_subnet[$_k] = $_k;
					$__ipv6_subnet[$_k] = $_ipv6_subnet[$_k];
				}
			}
						
		}
		
	}
	
	// Then the remaining IPs
	foreach($_ipv6_subnet as $k => $v){
		$__ipv6_subnet[$k] = $v;
	}
	
	echo '<select id="ipv6_subnet" class="form-control" name="ipv6_subnet[]" size="5" multiple="multiple" style="font-size:10px">';
	foreach($__ipv6_subnet as $k => $v){
		echo '<option value="'.$v['ip'].'" '.(isset($_POST['editvps']) ? (!empty($posted_subnet[$k]) ? 'selected="selected"' : '') : (!empty($vps['ips6_subnet'][$k]) ? 'selected="selected"' : '')).'>'.$v['ip'].' / '.$v['ipr_netmask'].'</option>';
	}		
	echo'</select>
	<span class="help-block">&nbsp;</span>
	</div>
	<div class="col-sm-4"></div>
</div>';

}

// IPv6
if(!empty($ips6) || !empty($vps['ips6'])){

echo '<div class="row hide_for_plan">
	<div class="col-sm-4">
		<label class="control-label">'.$l['ips6'].'</label><br />
		<span class="help-block">'.$l['ips6_exp'].'</span>
	</div>
	<div class="col-sm-4">
	<input type="text" class="form-control" name="ipv6count" id="ipv6count" value="'.((count($vps['ips6']) > 0) ? count($vps['ips6']) : 0).'" size="10" onblur="ipchoose(this.name, this.value)" /><br /><br />
	<select id="ipv6" class="form-control" name="ipv6[]" size="5" multiple="multiple" onchange="countipv6(\'ipv6count\');" style="font-size:10px">';
	
	$_ipv6 = array();
	
	// This is to put the Posted IPs first
	if(!empty($_POST['ipv6'])){
		foreach($_POST['ipv6'] as $k => $v){
			$_ipv6[$v] = $v;
		}
	}
	
	// Then the VPS IPs
	foreach($vps['ips6'] as $k => $v){
		$_ipv6[$v] = $v;
	}
	
	// Then the extra IPs
	foreach($ips6 as $k => $v){
		$_ipv6[$v['ip']] = $v['ip'];
	}
	
	$_ipv6 = array_unique($_ipv6);
	
	foreach($_ipv6 as $k => $v){	 
		echo '<option value="'.$v.'" '.(isset($_POST['editvps']) ? (@in_array($v, $_POST['ipv6']) ? 'selected="selected"' : '') : (@in_array($v, $vps['ips6']) ? 'selected="selected"' : '')).'>'.$v.'</option>';
	}
		
	echo '</select>
	<span class="help-block">&nbsp;</span>
	</div>
	<div class="col-sm-4"></div>
</div>';

}
if($bus_driver){
echo '<div class="row hide_for_plan">
	<div class="col-sm-4">
		<label class="control-label">'.$l['hdd'].'</label><br />
		<span class="help-block">'.$l['exp_hdd'].'</span>
	</div>
	<div class="col-sm-8">
		<table name="storages" id="storages" cellpadding="3" cellspacing="0" class="" style="100%;">';
			
			// Find the disk information 
			if(empty($disks)){
				
				if(!empty($vps['disks'])){
					$disks = $vps['disks'];
				}else{
					$disks[0]['size'] = $vps['space'];
					$disks[0]['st_uuid'] = $storages[$stid]['st_uuid'];
				}
			}
			
			foreach($disks as $k => $v){
				echo '<tr>
					<td>
						<select class="form-control bus_driver" id = "bus_driver" name="bus_driver[]" disabled="disabled" >';
							
						echo '<option value="'.$v['bus_driver'].'">'.strtoupper($v['bus_driver']).'</option>';
								
					echo   '</select>
					</td>
					<td>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</td>
					<td>
						<select class="form-control bus_driver_num" id = "bus_driver_num" name="bus_driver_num[]" disabled="disabled">';
							
						echo '<option value="'.$v['bus_driver_num'].'">'.$v['bus_driver_num'].'</option>';
								
					echo   '</select>
					</td>
					<td>
						<span class="help-block">&nbsp;</span>
					</td>
					<td width="100">
						<input type="text" id="hdd_'.$k.'" class="form-control size" name="tmp_space[]" size="15" value="'.$v['size'].'" />
					</td>
					<td>
						&nbsp;'.$l['space_gb'].' '.($v['disk_uuid'] ? '(<a href="javascript:void(0)" title="Disk UUID">'.$v['disk_uuid'].'</a>)': '').'
					</td>
					<td>
						<select class="form-control storages_list" name="storages[]" id="storages'.$k.'" disabled="disabled"
						style="width: 90%;float: left;">';
						
							foreach($storages as $sk => $sv){
								echo '<option value="'.$sv['st_uuid'].'" '.($sv['st_uuid'] == $v['st_uuid'] ? 'selected="selected"' : '').'>'.$sv['name'].'&nbsp;('.$sv['disk_space'].'GB '.(empty($sv['oversell']) ? $l['free'] : $l['oversell_free']).')</option>';
							}
						echo '</select>&nbsp;
					</td>
					<td>';
						echo virthelp('No_Space').'&nbsp;&nbsp;'.($k != 0 ? '<a class="delstorage" title="'.$l['rem_storage'].'"><img  src="'.$theme['images'].'admin/delete.png" width="12" /></a> ' : '').'<input class="disk_uuids" type="hidden" name="disk_uuids[]" value="'.$v['disk_uuid'].'">
					</td>
				</tr>';
			}
			
		echo '</table><input type="hidden" id="hdd" name="space" />
		<br />
		<a id="add_storage" href="javascript:add_storage_bus(\'storages\');" class="go_btn">'.$l['add_ip'].'</a>
	</div>
</div>';
	
}else{
echo '<div class="row hide_for_plan">
	<div class="col-sm-4">
		<label class="control-label">'.$l['hdd'].'</label><br />
		<span class="help-block">'.$l['exp_hdd'].'</span>
	</div>
	<div class="col-sm-8">
		<table name="storages" id="storages" cellpadding="3" cellspacing="0" class="" style="100%;">';
			
			// Find the disk information 
			if(empty($disks)){
				
				if(!empty($vps['disks'])){
					$disks = $vps['disks'];
				}else{
					$disks[0]['size'] = $vps['space'];
					$disks[0]['st_uuid'] = $storages[$stid]['st_uuid'];
				}
			}
			
			if($vps['virt'] == 'xen' && empty($vps['hvm'])){
				unset($disks[1]);
			}
			
			foreach($disks as $k => $v){
				echo '<tr>
					<td width="25%">
						<input type="text" id="hdd_'.$k.'" class="form-control size" name="tmp_space[]" size="15" value="'.$v['size'].'" />
					</td>
					<td>
						&nbsp;'.$l['space_gb'].' '.($v['disk_uuid'] ? '(<a href="javascript:void(0)" title="Disk UUID">'.$v['disk_uuid'].'</a>)': '').'
					</td>
					<td>
						<select class="form-control storages_list" name="storages[]" id="storages'.$k.'" disabled="disabled" style="width: 90%;float: left;">';
						
							foreach($storages as $sk => $sv){
								echo '<option value="'.$sv['st_uuid'].'" '.($sv['st_uuid'] == $v['st_uuid'] ? 'selected="selected"' : '').'>'.$sv['name'].'&nbsp;('.$sv['disk_space'].'GB '.(empty($sv['oversell']) ? $l['free'] : $l['oversell_free']).')</option>';
							}
						echo '</select>&nbsp;'.virthelp('No_Space').'&nbsp;&nbsp;'.($k != 0 ? '<a class="delstorage" title="'.$l['rem_storage'].'"><img  src="'.$theme['images'].'admin/delete.png" width="12" /></a> ' : '').
						'<input class="disk_uuids" type="hidden" name="disk_uuids[]" value="'.$v['disk_uuid'].'">
					</td>
					
				</tr>';
			}
			
		echo '</table><input type="hidden" id="hdd" name="space" />
		<br />
		<a id="add_storage" href="javascript:add_storage(\'storages\');" class="go_btn">'.$l['add_ip'].'</a>
	</div>
</div>';
	
}


echo '
<br>
<div class="row hide_for_plan">
	<div class="col-sm-4">
		<label class="control-label">'.$l['gram'].'</label><br />
		<span class="help-block">'.$l['exp_gram'].'</span>
	</div>
	<div class="col-sm-4">
		<input type="text" class="form-control" name="ram" id="gram" size="30" value="'.POSTval('ram', $vps['ram']).'" />
	</div>
	<div class="col-sm-4">
		'.$l['ram_mb'].' (<label class="control-label">'.(empty($resources['overcommit']) ? $resources['ram'] : ($resources['overcommit'] - $servers[$globals['server']]['alloc_ram'])).' MB</span> '.(empty($resources['overcommit']) ? $l['free'] : $l['overcomit_free']).')
	</div>
</div>';

if($vps['virt'] == 'openvz' || $vps['virt'] == 'proxo'){

echo '<div class="row hide_for_plan" id="burst">
	<div class="col-sm-4">
		<label class="control-label">'.$l['bram'].'</label><br />
		<span class="help-block">'.$l['exp_bram'].'</span>
	</div>
	<div class="col-sm-4">
		<input type="text" class="form-control" name="burst" id="bram" size="30" value="'.POSTval('burst', $vps['burst']).'" />
	</div>
	<div class="col-sm-4">
		 '.$l['ram_mb'].'
	</div>
</div>';

}

if($virt != 'openvz' && $vps['virt'] != 'proxo'){

echo '<div class="row hide_for_plan" id="swappedram">
	<div class="col-sm-4">
		<label class="control-label">'.$l['swap'].'</span><br />
		<span class="help-block">'.$l['exp_swap'].'</span>
	</div>
	<div class="col-sm-4">
		<input type="text" class="form-control" name="swapram" id="swap" size="30" value="'.POSTval('swapram', $vps['swap']).'" />
	</div>
	<div class="col-sm-4">
		'.$l['ram_mb'].'
	</div>
</div>';

}

echo '
<br/>
<div class="row hide_for_plan">
	<div class="col-sm-4">
		<label class="control-label">'.$l['band'].'</label><br />
		<span class="help-block">'.$l['exp_band'].'</span>
	</div>
	<div class="col-sm-4">
	<input type="text" class="form-control" name="bandwidth" id="band" size="30" value="'.POSTval('bandwidth', $vps['bandwidth']).'" onchange="handle_capping();" />
	</div>
	<div class="col-sm-4">'.$l['band_gb'].'</div>
</div>

<script type="text/javascript">
function netspeed(r){
	$_("network_speed").value = r;
	handle_capping();
}
function upspeed(r){
	$_("upload_speed").value = r;
	handle_capping();
}
</script>

<div class="row hide_for_plan">
	<div class="col-sm-4">
		<label class="control-label">'.$l['network_speed'].'</label><br />
		<span class="help-block">'.$l['network_speed_exp'].'</span>
	</div>
	<div class="col-sm-4">
		<input type="text" class="form-control" name="network_speed" id="network_speed" size="8" value="'.POSTval('network_speed', $vps['network_speed']).'" onchange="handle_capping();" />
	 </div>
	 <div class="col-sm-1" style="padding-top:8px;">'.$l['net_kb'].'</div>
	 <div class="col-sm-3 servre-select-lg">
	 	<select class="form-control speedmbits virt-select" name="network_speed2" id="network_speed2" onchange="netspeed(this.value)" style="width:100%"></select>

	 </div>
</div>

<div class="row hide_for_plan">
	<div class="col-sm-4">
		<label class="control-label">'.$l['cpunit'].'</label><br />
		<span class="help-block">'.$l['cpalloc'].' <br />
		<a href="'.$globals['docs'].'Creating_A_VPS#CPU_Parameters" target="_blank">'.$l['need_info'].'</a></span>
	</div>
	<div class="col-sm-4">
		<input type="text" class="form-control" name="cpu" id="cpunit" size="30" value="'.POSTval('cpu', $vps['cpu']).'" />
	</div>
	<div class="col-sm-4"> '.$l['units'].'</div>
</div>

<div class="row hide_for_plan">
	<div class="col-sm-4">
		<label class="control-label">'.$l['cores'].'</label><br />
		<span class="help-block"><a href="'.$globals['docs'].'Creating_A_VPS#CPU_Parameters" target="_blank">'.$l['need_info'].'</a></span>
	</div>
	<div class="col-sm-4">
		<input type="text" class="form-control" name="cores" id="cores" size="30" value="'.POSTval('cores', $vps['cores']).'" />
	</div>
	<div class="col-sm-4"></div>
</div>';
if($virt != 'lxc'){
	echo '
	<br/>
	<div class="row hide_for_plan" id="cpupercent">
		<div class="col-sm-4">
			<label class="control-label">'.$l['percent'].'</label><br />
			<span class="help-block">'.$l['cpuperutil'].'<br />
			<a href="'.$globals['docs'].'Creating_A_VPS#CPU_Parameters" target="_blank">'.$l['need_info'].'</a></span>
		</div>
		<div class="col-sm-4">
			<input type="text" class="form-control" name="cpu_percent" id="percent" size="30" value="'.POSTval('cpu_percent', intval($vps['cpu_percent'])).'" />
		</div>
		<div class="col-sm-4">%</div>
	</div>';
}

if($kernel->features('io_priority', $virt)){
echo '<div class="row hide_for_plan">
	<div class="col-sm-4">
		<label class="control-label">'.$l['io'].'</label><br />
		<span class="help-block">'.$l['io0-7'].'</span>
	</div>
	<div class="col-sm-4">
		<select id="prior" name="priority" class="form-control">
			<option value="0">0</option>
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
			<option value="4">4</option>
			<option value="5">5</option>
			<option value="6">6</option>
			<option value="7">7</option>
		</select>
	</div>
</div>';
}

if($vps['virt'] == 'openvz'){

if(!distro_check(1)){
	echo '<div class="row hide_for_plan">
		<div class="col-sm-4">
			<label class="control-label">'.$l['ploop'].'</label><br />
			<span class="help-block">'.$l['ploop_exp'].'</span>
		</div>
		<div class="col-sm-4">
			<input type="checkbox" class="ios" id="ploop" name="show_ploop" '.POSTchecked('ploop', $vps['ploop']).' '.(!empty($vps['ploop']) ? 'disabled=disabled' : '').' onchange="change_checkbox(\'ploop\');"/>
			<input type="hidden" name="ploop" id="hidden_ploop" value="'.POSTval('ploop', $vps['ploop']).'">
		</div>
		<div class="col-sm-4"></div>
	</div>';
}
echo '<div class="row hide_for_plan" id="ubc">
	<div class="col-sm-4">
		<label class="control-label">'.$l['ubcsettings'].'</label><br />
		<span class="help-block">'.$l['exp_ubc'].'</span>
	</div>
	<div class="col-sm-4">
		<input type="checkbox" class="ios" name="show_ubcsettings" '.POSTchecked('ubcsettings').' onchange="change_checkbox(\'ubcsettings\');"/>
		<input type="hidden" name="ubcsettings" id="hidden_ubcsettings" '.POSTchecked('ubcsettings').'>
		<a href="'.$globals['index'].'act=ubc&vpsid='.$vps['vpsid'].'">'.$l['ubcsettings'].'</a>
		
	</div>
	<div class="col-sm-4"></div>
</div>';

}

if($vps['virt'] == 'xen' && !empty($vps['hvm'])){
	
echo '<div class="row hide_for_plan">
	<div class="col-sm-4">
		<label class="control-label">'.$l['shadow'].'</label><br />
		<span class="help-block">'.$l['shadow_exp'].'</span>
	</div>
	<div class="col-sm-4">
		<input type="text" class="form-control" name="shadow" id="shadow" size="30" value="'.POSTval('shadow', $vps['shadow']).'" />
	</div>
	<div class="col-sm-4">MB</div>
</div>';

}

if($kernel->features('vnc_support', $virt)){
$vncpasslen = $kernel->features('vncpasslen', $virt);
	echo '
	<div class="row hide_for_plan">
		<div class="col-sm-4">
			<label class="control-label">VNC</label>
			<span class="help-block">&nbsp;</span>
		</div>
		<div class="col-sm-4">
			<input type="checkbox" class="ios" name="show_vnc" id="vnc" '.POSTchecked('vnc', $vps['vnc']).' onchange="checkvnc();"/>
			<input type="hidden" name="vnc" id="hidden_vnc" value="'.POSTval('vnc', $vps['vnc']).'">
		</div>
		<div class="col-sm-4"></div>
	</div>
	<div class="row hide_for_plan" id="vncpassrow">
		<div class="col-sm-4">
			<label class="control-label">'.$l['vncpass'].'</label><br />
			<span class="help-block">'.$l['vncpass_exp'].'</span>
		</div>
		<div class="col-sm-4">
			<input type="text" class="form-control" name="vncpass" id="vncpass" size="30" value="'.POSTval('vncpass', '').'" autocomplete="off"/>
		</div>
		<div class="col-sm-4">
			<a href="javascript: void(0);" onclick="$_(\'vncpass\').value=randstr('.$vncpasslen.');return false;" title="'.$l['vncpass'].'"><img src="'.$theme['images'].'vncpass.gif" /></a>
		</div>
	</div><br/>';

}

if($kernel->features('ha', $vps['virt']) && array_key_exists($globals['server'], $ha_enabled)){
	echo '<div class="row hide_for_plan">
			<div class="col-sm-4">
				<label class="control-label">'.$l['ha'].'</label>
				<span class="help-block">'.$l['ha_exp'].'</span>
			</div>
			<div class="col-sm-4">
				<input type="checkbox" class="ios" id="ha" name="ha" '.POSTchecked('ha', $vps['ha']).' onchange="change_checkbox(\'ha\');" disable="disable"/>
				<input type="hidden" name="ha" id="hidden_ha" value="'.POSTval('ha', $vps['ha']).'" disable="disable">
			</div>
			<div class="col-sm-4"></div>
	</div>';
}

if((($vps['virt'] == 'xen' || $vps['virt'] == 'xcp') && !empty($vps['hvm'])) || $vps['virt'] == 'kvm' || $vps['virt'] == 'proxk' || $vps['virt'] == 'vzk'){
	if($vps['virt'] != 'proxk'){
		echo '
<div class="row hide_for_plan">
	<div class="col-sm-4">
		<label class="control-label">ACPI</label>
		<span class="help-block">&nbsp;</span>
	</div>
	<div class="col-sm-4">
		<input type="checkbox" class="ios" id="acpi" name="show_acpi" '.POSTchecked('acpi', $vps['acpi']).' onchange="change_checkbox(\'acpi\');"/>
		<input type="hidden" name="acpi" id="hidden_acpi" value="'.POSTval('acpi', $vps['acpi']).'">
	</div>
	<div class="col-sm-4"></div>
</div>
<div class="row hide_for_plan">
	<div class="col-sm-4">
		<label class="control-label">APIC</label>
		<span class="help-block">&nbsp;</span>
	</div>
	<div class="col-sm-4">
		<input type="checkbox" class="ios" id="apic" name="show_apic" '.POSTchecked('apic', $vps['apic']).' onchange="change_checkbox(\'apic\');"/>
		<input type="hidden" name="apic" id="hidden_apic" value="'.POSTval('apic', $vps['apic']).'">
	</div>
	<div class="col-sm-4"></div>
</div>
<div class="row hide_for_plan">
	<div class="col-sm-4">
		<label class="control-label">PAE</label>
		<span class="help-block">&nbsp;</span>
	</div>
	<div class="col-sm-4">
		<input type="checkbox" class="ios" id="pae" name="show_pae" '.POSTchecked('pae', $vps['pae']).' onchange="change_checkbox(\'pae\');"/>
		<input type="hidden" name="pae" id="hidden_pae" value="'.POSTval('pae', $vps['pae']).'">
	</div>
	<div class="col-sm-4"></div>
</div>';
	}
	if($kernel->features('iso_support', $vps['_virt'])){
echo '
<div class="row hide_for_plan">
	<div class="col-sm-4">
		<label class="control-label">'.$l['boot_order'].'</label>
		<span class="help-block">&nbsp;</span>
	</div>
	<div class="col-sm-4">
		<select class="form-control" name="boot">
			<option value="cda" '.(POSTval('boot') == 'cda' || (empty($_POST) && $vps['boot'] == 'cda') ? 'selected="selected"' : '').'>1) Hard Disk 2) CD Drive</option>
			<option value="dca" '.(POSTval('boot') == 'dca' || (empty($_POST) && $vps['boot'] == 'dca') ? 'selected="selected"' : '').'>1) CD Drive 2) Hard Disk</option>
		</select>
	</div>
	<div class="col-sm-4"></div>
</div>';

	}

}

echo '<br />	

<div class="roundheader toggle_hover hide_for_plan" onclick="toggle_advoptions();" style="cursor:pointer;"><label id="toggle_indicator" style="width:10px;">+</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp'.$l['addvoption'].'</div>
<div id="advoptions"  style="display:none; width:100%" class="hide_for_plan">
			<div class="col-sm-12 bgaddv">';
			
				// Internal IP
				if(!empty($ips_int) || !empty($vps['ips_int'])){
				
					echo '<div class="row hide_for_plan">
					<div class="col-sm-6">
						<label class="control-label">'.$l['ips_int'].'</label><br />
						<span class="help-block">'.$l['ips_int_exp'].'</span>
					</div>
					<div class="col-sm-6">
					<select id="ips_int" class="form-control" name="ips_int[]" size="5" multiple="multiple">';
				
					$_ips_int = array();

					// This is to put the Posted IPs first
					if(!empty($_POST['ips_int'])){
						foreach($_POST['ips_int'] as $k => $v){
							$_ips_int[$v] = $v;
						}
					}
					
					// Then the VPS IPs
					foreach($vps['ips_int'] as $k => $v){
						$_ips_int[$v] = $v;
					}
					
					// Then the extra IPs
					foreach($ips_int as $k => $v){
						$_ips_int[$v['ip']] = $v['ip'];
					}
					
					$_ips_int = array_unique($_ips_int);
					
					foreach($_ips_int as $k => $v){	 
						echo '<option value="'.$v.'" '.(isset($_POST['editvps']) ? (@in_array($v, $_POST['ips_int']) ? 'selected="selected"' : '') : (@in_array($v, $vps['ips_int']) ? 'selected="selected"' : '')).'>'.$v.'</option>';
					}
							
					echo '</select>
					</div>
					</div><br/>';
					
				}

				// CPU Affinity. (CPU PIN)
				if($kernel->features('cpu_pinning', $vps['_virt'])){
					echo '<div class="row hide_for_plan">
							<div class="col-xs-10 col-sm-6">
								<label class="control-label">'.$l['cpupin'].'</label><br />
								<span class="help-block">'.$l['cpupin_exp'].'</span>
							</div>
							<div class="col-xs-2 col-sm-6">
								<label><input type="checkbox" class="ios" name="allcores" id="allcores" '.(!empty($_POST) ? (is_array($_POST['cpupin']) ? '' : 'checked="checked"') : ($vps['cpupin'] == -1 ? 'checked="checked"' : '')).' onchange="pincheck()" value="-1" />&nbsp;Default</label>';
							echo '</div>
					</div><br/>
					<div class="row hide_for_plan" id="pincores">
						<div class="col-xs-10 col-sm-6">
							<label class="control-label">'.$l['cpupin_select'].'</label>
							<span class="help-block">&nbsp;</span>
						</div>
						
						<div class="col-xs-2 col-sm-6">';
						for($i=0; $i < $resources['cpucores']; $i++){
							echo '<label><input type="checkbox" id="pin'.$i.'" class="ios" name="cpupin['.$i.']" value="'.$i.'" '.(!empty($_POST) ? (in_array($i, $_POST['cpupin']) || in_array((string)$i, $_POST['cpupin']) ? 'checked="checked"' : '') : (in_array($i, $vps['_cpupin']) || in_array((string)$i, $vps['_cpupin']) ? 'checked="checked"' : '')).' />&nbsp;vCPU '.($i+1).'&nbsp;</label>';
						}
						
						echo '</div>
					</div><br/>';

				}

				echo ' 
					<div class="row hide_for_plan">
						<div class="col-sm-6">
							<label class="control-label">'.$l['upload_speed'].'</label>
							<span class="help-block">'.$l['upload_speed_exp'].'</span>
						</div>
						<div class="col-sm-2">
							<input type="text" class="form-control" name="upload_speed" id="upload_speed" size="8" value="'.POSTval('upload_speed', $vps['upload_speed']).'" onchange="handle_capping();" />
						</div>
						<div class="col-sm-1" style="padding-top:8px">'.$l['net_kb'].'</div>
						<div class="col-sm-3">
							<select class="form-control speedmbits" name="upload_speed2" id="upload_speed2" onchange="upspeed(this.value)"></select>
						</div>
					</div><br/>
				<div class="row hide_for_plan">
					<div class="col-xs-10 col-sm-6">
						<label class="control-label">'.$l['band_suspend'].'</label><br />
						<span class="help-block">'.$l['exp_band_suspend'].'</span>
					</div>
					<div class="col-xs-2 col-sm-6">
						<input type="checkbox" class="ios" name="show_band_suspend" id="band_suspend" '.POSTchecked('band_suspend',$vps['band_suspend']).' onchange="change_checkbox(\'band_suspend\'); handle_capping();"/>
						<input type="hidden" name="band_suspend" id="hidden_band_suspend" value="'.POSTval('band_suspend', $vps['band_suspend']).'">
					</div>	
			   </div><br/>
				<div id="speed_cap_limit">
					<div class="row">
						<div class="col-xs-10 col-sm-6">
							<label class="control-label" for="speed_cap_down">'.$l['speed_cap_down'].'</label>
							<span class="help-block">'.$l['exp_speed_cap_down'].'</span>
						</div>
						<div class="col-sm-2 col-xs-8">
							<input type="number" class="form-control" name="speed_cap_down" id="speed_cap_down" value="'.POSTval('speed_cap_down', $vps['speed_cap']['down']).'" onmouseout="blur();"/>
						</div>
						<div class="col-sm-1" style="padding-top:8px">'.$l['net_kb'].'</div>
						<div class="col-sm-3 col-xs-8 sm_space">
							<select class="form-control speedmbits" name="speed_cap_down2" id="speed_cap_down2" onchange="$(\'#speed_cap_down\').val(this.value)"></select>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-10 col-sm-6">
							<label class="control-label" for="speed_cap_up">'.$l['speed_cap_up'].'</label>
							<span class="help-block">'.$l['exp_speed_cap_up'].'</span>
						</div>
						<div class="col-sm-2 col-xs-8">
							<input type="number" class="form-control" name="speed_cap_up" id="speed_cap_up" value="'.POSTval('speed_cap_up', $vps['speed_cap']['up']).'" onmouseout="blur();"/>
						</div>
						<div class="col-sm-1" style="padding-top:8px">'.$l['net_kb'].'</div>
						<div class="col-sm-3 col-xs-8 sm_space">
							<select class="form-control speedmbits" name="speed_cap_up2" id="speed_cap_up2" onchange="$(\'#speed_cap_up\').val(this.value)"></select>
						</div>
					</div>
				</div>';
				
				
				if($kernel->features('numa', $vps['virt'])){
					echo '
				<div class="row">
					<div class="col-sm-6">
						<label class="control-label">'.$l['usenuma'].'</label>
						<span class="help-block">&nbsp;</span>
					</div>
					<div class="col-sm-6">
						<input type="checkbox" class="ios" name="numa" value="1" id="numa" '.POSTchecked('numa', $vps['numa']).' />
					</div>
				</div>';
				}
							
				if($kernel->features('virtio', $vps['virt'])){
					
					// If user has disabled virtio and has not stopped and started the VM, we need to handle it here.
					$virtio_status = $vps['virtio'];
					if(empty($vps['virtio']) || $vps['virtio'] == 3){
						$virtio_status = 0;
					}
					
					echo '
				<div class="row hide_for_plan">
					<div class="col-xs-10 col-sm-6">
						<label class="control-label">'.$l['usevirtio'].'</label><br />
						<span class="help-block">'.$l['exp_usevirtio'].'</span>
					</div>
					<div class="col-xs-2 col-sm-6">
						<input type="checkbox" class="ios" name="show_virtio" value="1" id="virtio" onchange="return virtio_warning()" '.POSTchecked('virtio', $virtio_status).' />
						<input type="hidden" name="virtio" id="hidden_virtio" value="'.POSTval('virtio', $vps['virtio']).'">
					</div>
				</div>';
				
				}
				
				if($kernel->features('xenserver_tools', $vps['virt'])){
					echo '
					<div class="row">
						<div class="col-sm-6">
							<label class="control-label">'.$l['install_xentools'].'</label>
							<span class="help-block">&nbsp;</span>
						</div>
						<div class="col-sm-6">
							<input type="checkbox" '.(!empty($vps['install_xentools']) ? 'disabled="disabled"' : '').' class="ios" name="install_xentools" id="install_xentools" '.POSTchecked('install_xentools').' value="1" />
						</div>
					</div><br />';
				}
					
				if($kernel->features('cpu_mode', $vps['virt'])){
					
					echo '
				<div class="row">
					<div class="col-sm-6">
						<label class="control-label">'.$l['cpu_mode'].'</label>
						<span class="help-block">&nbsp;</span>
					</div>
					<div class="col-sm-6">
						<select class="form-control" name="cpu_mode" id="cpu_mode">';
						
						foreach($cpu_modes as $ck => $cv){
							echo '<option value="'.$ck.'" '.ex_POSTselect('cpu_mode', $ck, $vps['cpu_mode']).'>'.$cv.'</option>';
						}
						
						echo '
						</select>
					</div>
				</div>';
				
				}
						
				if($kernel->features('cpu_topology', $vps['virt'])){
					
					echo '
				<div class="row" id="enable_cpu_topology_row">
						<div class="col-sm-6">
							<label class="control-label">'.$l['enable_cpu_topology'].'</label>
							<span class="help-block">'.$l['enable_cpu_topology_exp'].'</span>
						</div>
						<div class="col-sm-6">
							<input type="checkbox" class="ios" name="enable_cpu_topology" id="enable_cpu_topology" '.POSTchecked('enable_cpu_topology', $vps['topology_sockets']).' onchange="change_cpu_topology();" />
						</div>
					</div>
					<div id="cpu_topology" style="display:none;">
						<div class="row">
							<div class="col-sm-6">
								<label class="control-label">Sockets</label>
								<span class="help-block">&nbsp;</span>
							</div>
							<div class="col-sm-6">
								<input type="text" class="form-control numbersonly" name="topology_sockets" id="topology_sockets" value="'.POSTval('topology_sockets', $vps['topology_sockets']).'" />
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6">
								<label class="control-label">Cores</label>
								<span class="help-block">&nbsp;</span>
							</div>
							<div class="col-sm-6">
								<input type="text" class="form-control numbersonly" name="topology_cores" id="topology_cores"  value="'.POSTval('topology_cores', $vps['topology_cores']).'" />
							</div>
						</div>';
						
						if($kernel->features('cpu_threads', $vps['virt'])){	
					
							echo '
						<div class="row">
							<div class="col-sm-6">
								<label class="control-label">Threads</label>
								<span class="help-block">&nbsp;</span>
							</div>
							<div class="col-sm-6">
								<input type="text" class="form-control numbersonly" name="topology_threads" id="topology_threads" value="'.POSTval('topology_threads', $vps['topology_threads']).'" />
							</div>
						</div>';
						
						}
						
					echo '
				</div>';
						
				}
					
					
				if(!empty($isos) && $kernel->features('sec_iso_support', $vps['_virt'])){
					echo '
				<div class="row hide_for_plan">
					<div class="col-sm-6">
						<label class="control-label">'.$l['sec_vsiso'].'</label><br />
						<span class="help-block">'.$l['sec_vsiso_exp'].'</span>
					</div>
					<div class="col-sm-6">
					<select class="form-control chosen" name="sec_iso" id="sec_iso">
					<option value="0" '.(POSTval('sec_iso') == 0 ? 'selected="selected"' : '').'>'.$l['none'].'</option>';
					$options_eu_iso = $options_iso = $options = '';
					foreach($isos as $k => $v){
						if(!empty($v['isuseriso'])){
							$options_eu_iso .= '<option value="'.$k.'" '.(POSTval('sec_iso') == $k || (empty($_POST) && $vps['sec_iso'] == $k) ? 'selected="selected"' : '').'>'.$v['name'].'</option>';
						}else{
							$options_iso .= '<option value="'.$k.'" '.(POSTval('sec_iso') == $k || (empty($_POST) && $vps['sec_iso'] == $k) ? 'selected="selected"' : '').'>'.$v['name'].'</option>';
						}
					}

					if(!empty($options_iso)){
						echo '<optgroup label="'.$l['admin_iso'].'">'.$options_iso.'</optgroup>';
					}
					
					if(!empty($options_eu_iso)){
						echo '<optgroup label="'.$l['eu_iso'].'">'.$options_eu_iso.'</optgroup>';
					}
					
					 echo'</select>
					</div>
				</div><br/>';

				}
				
				if($kernel->features('kvm_cache', $vps['virt'])){				
					echo '
				<div class="row hide_for_plan">
					<div class="col-sm-6">
						<label class="control-label">'.$l['kvm_cache'].'</label><br />
						<span class="help-block">'.$l['exp_kvm_cache'].'</span>
					</div>
					<div class="col-sm-6">
						<select class="form-control" name="kvm_cache" id="kvm_cache">
							<option value="0" '.ex_POSTselect('kvm_cache', 'none', $vps['kvm_cache']).'>None</option>
							<option value="writeback" '.ex_POSTselect('kvm_cache', 'writeback', $vps['kvm_cache']).'>Writeback</option>
							<option value="writethrough" '.ex_POSTselect('kvm_cache', 'writethrough', $vps['kvm_cache']).'>Writethrough</option>
							<option value="directsync" '.ex_POSTselect('kvm_cache', 'directsync', $vps['kvm_cache']).'>Direct Sync</option>
							<option value="default" '.ex_POSTselect('kvm_cache', 'default', $vps['kvm_cache']).'>Default</option>
						</select>
					</div>
				</div><br/>';
				}
					
				if($kernel->features('io_mode', $vps['virt'])){
					echo '
				<div class="row hide_for_plan">
						<div class="col-sm-6">
							<label class="control-label">'.$l['io_mode'].'</label>
							<span class="help-block">&nbsp;</span>
						</div>
						<div class="col-sm-6">
							<select class="form-control" name="io_mode" id="io_mode">
								<option value="0" '.ex_POSTselect('io_mode', 'default', $vps['io_mode']).'>Default</option>
								<option value="native" '.ex_POSTselect('io_mode', 'native', $vps['io_mode']).'>Native</option>
								<option value="threads" '.ex_POSTselect('io_mode', 'threads', $vps['io_mode']).'>Threads</option>
							</select>
						</div>
					</div>
					<div class="row hide_for_plan">
						<div class="col-xs-10 col-sm-6">
							<label class="control-label">'.$l['kvm_vga'].'</label>
							<span class="help-block"></span>
						</div>
						<div class="col-xs-2 col-sm-6">
							<input type="checkbox" class="ios" name="show_kvm_vga" id="kvm_vga" onchange="enable_accel();" '.POSTchecked('kvm_vga', $vps['kvm_vga']).' />
							<input type="hidden" name="kvm_vga" id="hidden_kvm_vga" value="'.POSTval('kvm_vga', $vps['kvm_vga']).'">
						</div>
					</div>
					<div class="row hide_for_plan" id="enable_acceleration" style="display:none;">
						<div class="col-xs-10 col-sm-6">
							<label class="control-label">'.$l['acceleration'].'</label>
							<span class="help-block">'.$l['exp_acceleration'].'</span>
						</div>
						<div class="col-xs-2 col-sm-6">
							<input type="checkbox" class="ios" name="show_acceleration" id="acceleration" '.POSTchecked('acceleration', $vps['acceleration']).' onchange="change_checkbox(\'acceleration\');"/>
							<input type="hidden" name="acceleration" id="hidden_acceleration" value="'.POSTval('acceleration', $vps['acceleration']).'">
						</div>
					</div>
					<br/>';
				}
					
				if(!empty($os_check)){
					echo '<div class="row hide_for_plan">
						<div class="col-sm-6">
							<label class="control-label">'.$l['total_iops_sec'].'</label>
						</div>
						<div class="col-sm-6">
							<input type="text" class="form-control" name="total_iops_sec" value="'.POSTval('total_iops_sec', $vps['total_iops_sec']).'" />
						</div>
					</div><br/>
					<div class="row hide_for_plan">
						<div class="col-sm-6">
							<label class="control-label">'.$l['read_bytes_sec'].'</label>
						</div>
						<div class="col-sm-6">
							<input type="text" class="form-control" name="read_bytes_sec" value="'.POSTval('read_bytes_sec', $vps['read_bytes_sec']).'" />&nbsp; MB/s
						</div>
					</div><br/>
					<div class="row hide_for_plan">
						<div class="col-sm-6">
							<label class="control-label">'.$l['write_bytes_sec'].'</label>
						</div>
						<div class="col-sm-6">
							<input type="text" class="form-control" name="write_bytes_sec" value="'.POSTval('write_bytes_sec', $vps['write_bytes_sec']).'" />&nbsp; MB/s
						</div>
					</div><br/>';
				}
				
				
				if($kernel->features('ebtables_support', $vps['virt'])){
					
					echo '<div class="row">
						<div class="col-sm-6">
							<label class="control-label">'.$l['disable_ebtables'].'</label>
							<span class="help-block">&nbsp;</span>
						</div>
						<div class="col-sm-6">
							<input type="checkbox" class="ios" name="show_disable_ebtables" id="disable_ebtables" '.POSTchecked('disable_ebtables', $vps['disable_ebtables']).' onchange="change_checkbox(\'disable_ebtables\');"/>
							<input type="hidden" name="disable_ebtables" id="hidden_disable_ebtables" value="'.POSTval('disable_ebtables', $vps['disable_ebtables']).'">
						</div>
					</div><br />';
					
				}
	
				if($kernel->features('win_support', $vps['_virt'])){
					
					echo '<div class="row hide_for_plan">
						<div class="col-xs-10 col-sm-6">
							<label class="control-label">'.$l['rdp'].'</label><br />
							<span class="help-block">'.$l['exp_rdp'].'</span>
						</div>
						<div class="col-xs-2 col-sm-6">
							<input type="checkbox" class="ios" name="show_rdp" id="rdp" '.POSTchecked('rdp', $vps['rdp']).' onchange="change_checkbox(\'rdp\');"/>
							<input type="hidden" name="rdp" id="hidden_rdp" value="'.POSTval('rdp', $vps['rdp']).'">
						</div>	
				   </div><br/>';
				
				}
				
				if($kernel->features('vnc_key_map', $vps['_virt'])){
					echo '<div class="row hide_for_plan">
						<div class="col-sm-6">
							<label class="control-label">'.$l['vnc_keymap'].'</span>
						</div>
						<div class="col-sm-6 server-select-lg">
							<select class="form-control virt-select" name="vnc_keymap" id="vnc_keymap" style="width:100%">
								<option value="en-us" '.ex_POSTselect('vnc_keymap', 'en-us', $vps['vnc_keymap']).'>en-us</option>
								<option value="de-ch" '.ex_POSTselect('vnc_keymap', 'de-ch', $vps['vnc_keymap']).'>de-ch</option>
								<option value="ar" '.ex_POSTselect('vnc_keymap', 'ar', $vps['vnc_keymap']).'>ar</option>
								<option value="da" '.ex_POSTselect('vnc_keymap', 'da', $vps['vnc_keymap']).'>da</option>
								<option value="et" '.ex_POSTselect('vnc_keymap', 'et', $vps['vnc_keymap']).'>et</option>
								<option value="fo" '.ex_POSTselect('vnc_keymap', 'fo', $vps['vnc_keymap']).'>fo</option>
								<option value="fr-be" '.ex_POSTselect('vnc_keymap', 'fr-be', $vps['vnc_keymap']).'>fr-be</option>
								<option value="fr-ch" '.ex_POSTselect('vnc_keymap', 'fr-ch', $vps['vnc_keymap']).'>fr-ch</option>
								<option value="hu" '.ex_POSTselect('vnc_keymap', 'hu', $vps['vnc_keymap']).'>hu</option>
								<option value="it" '.ex_POSTselect('vnc_keymap', 'it', $vps['vnc_keymap']).'>it</option>
								<option value="lt" '.ex_POSTselect('vnc_keymap', 'lt', $vps['vnc_keymap']).'>lt</option>
								<option value="mk" '.ex_POSTselect('vnc_keymap', 'mk', $vps['vnc_keymap']).'>mk</option>
								<option value="nl" '.ex_POSTselect('vnc_keymap', 'nl', $vps['vnc_keymap']).'>nl</option>
								<option value="no" '.ex_POSTselect('vnc_keymap', 'no', $vps['vnc_keymap']).'>no</option>
								<option value="pt" '.ex_POSTselect('vnc_keymap', 'pt', $vps['vnc_keymap']).'>pt</option>
								<option value="ru" '.ex_POSTselect('vnc_keymap', 'ru', $vps['vnc_keymap']).'>ru</option>
								<option value="sv" '.ex_POSTselect('vnc_keymap', 'sv', $vps['vnc_keymap']).'>sv</option>
								<option value="tr" '.ex_POSTselect('vnc_keymap', 'tr', $vps['vnc_keymap']).'>tr</option>
								<option value="de" '.ex_POSTselect('vnc_keymap', 'de', $vps['vnc_keymap']).'>de</option>
								<option value="en-gb" '.ex_POSTselect('vnc_keymap', 'en-gb', $vps['vnc_keymap']).'>en-gb</option>
								<option value="es" '.ex_POSTselect('vnc_keymap', 'es', $vps['vnc_keymap']).'>es</option>
								<option value="fi" '.ex_POSTselect('vnc_keymap', 'fi', $vps['vnc_keymap']).'>fi</option>
								<option value="fr" '.ex_POSTselect('vnc_keymap', 'fr', $vps['vnc_keymap']).'>fr</option>
								<option value="fr-ca" '.ex_POSTselect('vnc_keymap', 'fr-ca', $vps['vnc_keymap']).'>fr-ca</option>
								<option value="hr" '.ex_POSTselect('vnc_keymap', 'hr', $vps['vnc_keymap']).'>hr</option>
								<option value="is" '.ex_POSTselect('vnc_keymap', 'is', $vps['vnc_keymap']).'>is</option>
								<option value="ja" '.ex_POSTselect('vnc_keymap', 'ja', $vps['vnc_keymap']).'>ja</option>
								<option value="lv" '.ex_POSTselect('vnc_keymap', 'lv', $vps['vnc_keymap']).'>lv</option>
								<option value="nl-be" '.ex_POSTselect('vnc_keymap', 'nl-be', $vps['vnc_keymap']).'>nl-be</option>
								<option value="pl" '.ex_POSTselect('vnc_keymap', 'pl', $vps['vnc_keymap']).'>pl</option>
								<option value="pt-br" '.ex_POSTselect('vnc_keymap', 'pt-br', $vps['vnc_keymap']).'>pt-br</option>
								<option value="sl" '.ex_POSTselect('vnc_keymap', 'sl', $vps['vnc_keymap']).'>sl</option>
								<option value="th" '.ex_POSTselect('vnc_keymap', 'th', $vps['vnc_keymap']).'>th</option>
							</select>
						</div>
					</div>
					<br/>';
				}
				
				if(!empty($supported_nics)){
				echo '
					<div class="row hide_for_plan">
						<div class="col-sm-6">
							<label class="control-label">'.$l['changenic'].'</label><br />
							<span class="help-block">'.$l['exp_changenic'].'</span>
						</div>
						<div class="col-sm-6">
							<select class="form-control" name="nic_type" id="nic_type" >';
						
								foreach($supported_nics as $k => $v){
					
									echo '<option value="'.$v.'" '.ex_POSTselect('nic_type', $v, $vps['nic_type']).'>'.(!empty($l['nic_'.$v]) ? $l['nic_'.$v] : $v).'</option>';
								}
							
							echo '</select>
						</div>
					</div><br/>';
				}
				
				// Only available for XEN-HVM
				if(($vps['virt'] == 'xen' && !empty($vps['hvm'])) && distro_check(0, 1, 1, 0, 1)){
					
					// PV-on-HVM drivers
					
					echo '<div class="row hide_for_plan">
						<div class="col-xs-10 col-sm-6">
							<label class="control-label">'.$l['pv_on_hvm'].'</label><br />
							<span class="help-block">'.$l['exp_pv_on_hvm'].'</span>
						</div>
						<div class="col-xs-2 col-sm-6">
							<input type="checkbox" class="ios" name="show_pv_on_hvm" id="pv_on_hvm" '.POSTchecked('pv_on_hvm',$vps['pv_on_hvm']).' onchange="ispvonhvm();" />
							<input type="hidden" name="pv_on_hvm" id="hidden_pv_on_hvm" value="'.POSTval('pv_on_hvm', $vps['pv_on_hvm']).'">
						</div>	
				   </div><br/>';
				}
				
				if($vps['virt'] == 'xen' && !empty($vps['hvm'])){
					echo '<div class="row hide_for_plan" id="tr_viftype">
						<div class="col-sm-6">
							<label class="control-label">'.$l['change_vif_type'].'</label><br />
							<span class="help-block">'.$l['exp_change_vif_type'].'</span>
						</div>
						<div class="col-sm-6">
							'.$l['viftype_netfront'].'<input type="radio" name="vif_type" id="vif_type"  value="netfront" '.(POSTval('vif_type', $vps['vif_type']) == 'netfront' ? 'checked="checked"' : '').'/>
							'.$l['viftype_ioemu'].'<input type="radio" name="vif_type" id="vif_type"  value="ioemu" '.(POSTval('vif_type', $vps['vif_type']) == 'ioemu' ? 'checked="checked"' : '').'/>
						</div>
					</div><br/>';
				}
				
				// TUN/TAP 
				if($vps['virt'] == 'openvz' || $vps['virt'] == 'vzo' || $vps['virt'] == 'proxo'){
					
				$openvz_features = $vps['openvz_features'];
				
				echo '<div class="row hide_for_plan">
						<div class="col-xs-10 col-sm-6">
							<label class="control-label">'.$l['tuntap'].'</label><br />
							<span class="help-block">'.$l['exp_tuntap'].'</span>
						</div>
						<div class="col-xs-2 col-sm-6">
							<input type="checkbox" class="ios" name="show_tuntap" id="tuntap" '.POSTchecked('tuntap', $vps['tuntap']).' onchange="change_checkbox(\'tuntap\');"/>
							<input type="hidden" name="tuntap" id="hidden_tuntap" value="'.POSTval('tuntap', $vps['tuntap']).'">
						</div>
					</div>
					<div class="row hide_for_plan">
						<div class="col-xs-10 col-sm-6">
							<label class="control-label">'.$l['ppp'].'</label><br />
							<span class="help-block">'.$l['exp_ppp'].'</span>
						</div>
						<div class="col-xs-2 col-sm-6">
							<input type="checkbox" class="ios" name="show_ppp" id="ppp" '.POSTchecked('ppp', $vps['ppp']).' onchange="change_checkbox(\'ppp\');"/>
							<input type="hidden" name="ppp" id="hidden_ppp" value="'.POSTval('ppp', $vps['ppp']).'">
						</div>
					</div><br/>
					<div class="row">
						<div class="col-xs-10 col-sm-6">
							<label class="control-label">'.$l['fuse'].'</label><br />
							<span class="help-block">'.$l['fuse_exp'].'</span>
						</div>
						<div class="col-xs-2 col-sm-6">
							<input type="checkbox" value="1" name="show_fuse" id="fuse" '.POSTchecked('fuse', $openvz_features['fuse']).' onchange="change_checkbox(\'fuse\');"/>
							<input type="hidden" name="fuse" id="hidden_fuse" value="'.POSTval('fuse', $openvz_features['fuse']).'">
						</div>
					</div>
					<div class="row">
						<div class="col-xs-10 col-sm-6">
							<label class="control-label">'.$l['ipip'].'</label><br />
							<span class="help-block">'.$l['ipip_exp'].'</span>
						</div>
						<div class="col-xs-2 col-sm-6">
							<input type="checkbox" value="1" name="show_ipip" id="ipip" '.POSTchecked('ipip', $openvz_features['ipip']).' onchange="change_checkbox(\'ipip\');"/>
							<input type="hidden" name="ipip" id="hidden_ipip" value="'.POSTval('ipip', $openvz_features['ipip']).'">
						</div>
					</div>
					<div class="row">
						<div class="col-xs-10 col-sm-6">
							<label class="control-label">'.$l['ipgre'].'</label><br />
							<span class="help-block">'.$l['ipgre_exp'].'</span>
						</div>
						<div class="col-xs-2 col-sm-6">
							<input type="checkbox" value="1" name="show_ipgre" id="ipgre" '.POSTchecked('ipgre', $openvz_features['ipgre']).' onchange="change_checkbox(\'ipgre\');"/>
							<input type="hidden" name="ipgre" id="hidden_ipgre" value="'.POSTval('ipgre', $openvz_features['ipgre']).'">
						</div>
					</div>
					<div class="row">
						<div class="col-xs-10 col-sm-6">
							<label class="control-label">'.$l['nfs'].'</label><br />
							<span class="help-block">'.$l['nfs_exp'].'</span>
						</div>
						<div class="col-xs-2 col-sm-6">
							<input type="checkbox" value="1" name="show_nfs" id="nfs" '.POSTchecked('nfs', $openvz_features['nfs']).'  onchange="change_checkbox(\'nfs\');"/>
							<input type="hidden" name="nfs" id="hidden_nfs" value="'.POSTval('nfs', $openvz_features['nfs']).'">
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<label class="control-label">'.$l['quotaugidlimit'].'</label><br />
							<span class="help-block">'.$l['quotaugidlimit_exp'].'</span>
						</div>
						<div class="col-sm-6">
							<input type="text" class="form-control" name="quotaugidlimit" id="quotaugidlimit" size="30" value="'.POSTval('quotaugidlimit', $openvz_features['quotaugidlimit']).'" />
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<label class="control-label">'.$l['iolimit'].'</label><br />
							<span class="help-block">'.$l['iolimit_exp'].'</span>
						</div>
						<div class="col-sm-6">
							<input type="text" class="form-control" name="iolimit" id="iolimit" size="30" value="'.POSTval('iolimit', $openvz_features['iolimit']).'" />
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<label class="control-label">'.$l['iopslimit'].'</label><br />
							<span class="help-block">'.$l['iopslimit_exp'].'</span>
						</div>
						<div class="col-sm-6">
							<input type="text" class="form-control" name="iopslimit" id="iopslimit" size="30" value="'.POSTval('iopslimit', $openvz_features['iopslimit']).'" />
						</div>
					</div>';
				}
				
				// All kernel. DNS Nameserver 
				echo '<div class="row hide_for_plan">
						<div class="col-sm-6">
							<label class="control-label">'.$l['dns'].'</label><br />
							<span class="help-block">'.$l['exp_dns'].'</span>
						</div>
						<div class="col-sm-6">
							<div class="row hide_for_plan">
								<div class="col-sm-12" id="dnstable">';
								 
									$_dns = @(empty($_POST['dns']) ? $vps['dns'] : $_POST['dns']);

									if(is_array($_dns) && !empty($_dns)){
										foreach($_dns as $d => $ds){
											if(empty($ds)){
												unset($_dns[$d]);
											}
										}
									}
										
									if(empty($_dns)){
										$_dns = array(NULL);
									}
									 
									foreach($_dns as $dn){
										echo '<div class="row hide_for_plan">
										<div class="col-sm-6">
											<input type="text" class="form-control" name="dns[]" value="'.$dn.'" />
										</div>
										</div>';
									}
										
								echo '
									<br />
								</div>	
								</div>
								<input type="button" onclick="adddnsrow(\'dnstable\')" class="go_btn" value="'.$l['add_dns'].'"/>

						</div>
					</div><br/>';
					
				//os reinstall
				echo '<div class="row hide_for_plan">
					<div class="col-sm-6">
						<label class="control-label">'.$l['osreinstall'].'</label><br />
						<span class="help-block">'.$l['exp_osreinstall'].'</span>
					</div>
					<div class="col-sm-6">
						<input type="text" class="form-control" name="osreinstall_limit" id="osreinstall_limit"  value="'.POSTval('osreinstall_limit', $vps['osreinstall_limit']).'" />
					</div>
				</div>
				<div class="row hide_for_plan">
					<div class="col-sm-6">
						<label class="control-label">'.$l['admin_managed'].'</label><br />
						<span class="help-block">'.$l['exp_admin_managed'].'</span>
					</div>
					<div class="col-sm-6">
						<input type="checkbox" class="ios" name="show_admin_managed" id="admin_managed" '.POSTchecked('admin_managed', $vps['admin_managed']).' onchange="change_checkbox(\'admin_managed\');"/>
						<input type="hidden" name="admin_managed" id="hidden_admin_managed" value="'.POSTval('admin_managed', $vps['admin_managed']).'">
					</div>
				</div>';
				
				if($kernel->features('nw_config', $virt)){
				
					echo '<div class="row">
						<div class="col-sm-6">
							<label class="control-label">'.$l['disable_nw_config'].'</label>
							<span class="help-block">'.$l['exp_disable_nw_config'].'</span>
						</div>
						<div class="col-sm-6">
							<input type="checkbox" class="ios" name="show_disable_nw_config" id="disable_nw_config" '.POSTchecked('disable_nw_config', $vps['disable_nw_config']).' onchange="change_checkbox(\'disable_nw_config\');"/>
							<input type="hidden" name="disable_nw_config" id="hidden_disable_nw_config" value="'.POSTval('disable_nw_config', $vps['disable_nw_config']).'">
						</div>
					</div>';
				}
			
				if(!empty($backup_plans)){
				
					$bpid = !empty($_POST) ? POSTval('bpid') : $vps['bpid'];
					
					echo '
				<div class="row"> 
					<div class="col-sm-6">
						<label class="control-label">'.$l['backup_plan'].'</label>
						<span class="help-block">'.$l['backup_plan_exp'].'</span>
					</div>
					<div class="col-sm-6 server-select-lg">
						<select class="form-control virt-select" name="bpid" style="width:100%">
							<option value="0" '.($bpid == 0 ? 'selected="selected"' : '').'>'.$l['none'].'</option>
							<option value="-1" '.($bpid == -1 ? 'selected="selected"' : '').'>'.$l['same_as_vps_plan'].'</option>';
							
							foreach($backup_plans as $k => $v){
								echo '<option value="'.$k.'" '.($bpid == $k ? 'selected="selected"' : '').'>'.$v['plan_name'].'</option>';
							}
					
					echo'
						</select>
					</div>					
				</div>';
				
				}
			
		echo '</div>
	</div>
<br />
<div class="row hide_for_plan">
	<div class="col-sm-6">
		<label class="control-label">'.$l['mg'].'</label><br />
		<span class="help-block">'.$l['mg_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<select class="form-control" name="mgs[]" id="mg" multiple="multiple" style="width:250px">';
			
			foreach($mgs as $mk => $mv){
				if($vps['_virt'] != $mv['mg_type']) continue;
				echo '<option value="'.$mk.'" type="'.$mv['mg_type'].'" '.(in_array($mk, (empty($_POST['editvps']) ? $vps['mg'] : @$_POST['mgs'])) ? 'selected="selected"' : '').'>'.$mv['mg_name'].'</option>';
			}
			
		echo '</select>
	</div>
</div>
<div class="row hide_for_plan">
	<div class="col-sm-6">
		<label class="control-label">'.$l['create_date'].'</span>
		<span class="help-block">&nbsp;</span>
	</div>
	<div class="col-sm-6">
		<span class="help-block">'.(empty($vps['time']) ? 'N/A' : datify($vps['time'])).'</i>
	</div>
</div>
<div class="row hide_for_plan">
	<div class="col-sm-6">
		<label class="control-label">'.$l['last_edited'].'</span>
		<span class="help-block">&nbsp;</span>
	</div>
	<div class="col-sm-6">
		<span class="help-block">'.(empty($vps['edittime']) ? 'N/A' : datify($vps['edittime'])).'</i>
	</div>
</div>
<br /><br />

<center><input type="submit" id="editvps" name="editvps" value="'.$l['submit'].'" class="btn"/></center>

<br /><br />
</form>
</div>
</div>
';
softfooter();

}

?>