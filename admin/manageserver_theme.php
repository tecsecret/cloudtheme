<?php

//////////////////////////////////////////////////////////////
//===========================================================
// dashboard_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function manageserver_theme(){

global $theme, $error, $globals, $cluster, $user, $l, $usage, $info;

softheader($l['<title>']);

echo '
<div class="bg" style="width: 99%">
<center class="tit"><i class="icon icon-servers icon-head"></i>&nbsp; '.$l['<title>'].'</center><br />';

error_handle($error); 

// Is it offline ?
$hypervisor_status = $cluster->statewise($globals['server']);
if($hypervisor_status == 0 || $hypervisor_status == 2){

	echo '<div class="e_notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['server_status_'.$hypervisor_status].'</div>';
	
}else{

// Is it loaded into the correct kernel
if(strlen($info['check_kernel']) > 1){
	echo '<div class="e_notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$info['check_kernel'].'</div>';
}

if(!empty($GLOBALS['rebooted'])){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['server_rebooting'].'</div>';
}

echo '<script language="javascript" type="text/javascript"><!-- // --><![CDATA[';

if(is_allowed('server_statistics')){

echo '

	function startusage(){
		ajaxtimer = setInterval("getserverstats('.$globals['server'].')", 5000);
	};

	// Draw a Server Resource Graph
	function server_graph(id, data){

		$.plot($("#"+id), data, 
		{
			series: {
				pie: { 
					innerRadius: 0.7,
					radius: 1,
					show: true,
					label: {
						show: true,
						radius: 0,
						formatter: function(label, series){
							if(label != "Used") return "";
							return \'<div style="font-size:13px;"><b>\'+Math.round(series.percent)+\'%</b></div><div style="font-size:9px;">\'+label+\'</div>\';	
						}
					}
				}
			},
			legend: {
				show: false
			}
		});
	}

	function getserverstats(serid){
		
		serid = serid || 0;
		
		$.getJSON("'.$globals['index'].'act=manageserver&changeserid="+serid+"&api=json", function(data) {
			
			var server_cpu = [
				{ label: "Used",  data: data["usage"]["cpu"]["percent"]},
				{ label: "Free",  data: data["usage"]["cpu"]["percent_free"]}
			];
			
			var server_ram = [
				{ label: "Used",  data: data["usage"]["ram"]["percent"]},
				{ label: "Free",  data: data["usage"]["ram"]["percent_free"]}
			];
			
			var server_disk = [
				{ label: "Used",  data: data["usage"]["disk"]["/"]["percent"]},
				{ label: "Free",  data: data["usage"]["disk"]["/"]["percent_free"]}
			];
			
			var server_bandwidth = [
				{ label: "Used",  data: data["usage"]["bandwidth"]["used_gb"]},
				{ label: "Free",  data: data["usage"]["bandwidth"]["free_gb"]}
			];
			
			// CPU
			server_graph("server_cpu", server_cpu);
			
			// RAM
			server_graph("server_ram", server_ram);
			
			// DISK
			server_graph("server_disk", server_disk);
			
			// Bandwidth
			server_graph("server_bandwidth", server_bandwidth);
			
			// Fill in the Text
			$_("server_cpu_text").innerHTML = data["usage"]["cpu"]["percent"]+"% / 100%";
			$_("server_ram_text").innerHTML = data["usage"]["ram"]["used"]+" MB / "+data["usage"]["ram"]["limit"]+" MB";
			$_("server_disk_text").innerHTML = data["usage"]["disk"]["/"]["used_gb"]+" GB / "+data["usage"]["disk"]["/"]["limit_gb"]+" GB";
			$_("server_bandwidth_text").innerHTML = data["usage"]["bandwidth"]["used_gb"]+" GB";
			
			// Update Server Load Average
			$("#uptime_data").html(data["info"]["uptime"]);
			
		});
	}';
}

echo '$(document).ready(function(){
	getserverstats('.$globals['server'].');
	startusage();
});

function conf_reboot(){
	return confirm("'.$l['conf_reboot'].'");
};

// ]]></script>';

if(is_allowed('server_statistics')){

	echo '<div class="divroundshad">
	<div class="roundheader"><center>'.$l['basic'].'</center></div>
	<br />
	<div class="row">
		<div class="col-sm-6" style="border-right:1px solid #CCCCCC">
			<table align="center" class="table table-hover" cellpadding="3" cellspacing="4" border="0" width="100%">
				<tr>
					<td align="left" class="blue_td" width="30%">'.$l['hostname'].'</td>
					<td class="fhead val">'.$info['hostname'].'</td>
				</tr>
				<tr>
					<td align="left" class="blue_td">'.$l['ipaddr'].'</td>
					<td class="val">'.$info['ips'][0].'</td>
				</tr>
				<tr>
					<td align="left" class="blue_td">'.$l['numvps'].'</td>
					<td class="val">'.$info['details']['numvps'].'</td>
				</tr>
				<tr>
					<td align="left" class="blue_td">'.$l['alloc_cpu'].'</td>
					<td class="val">'.$info['details']['alloc_cpu'].'</td>
				</tr>
				<tr>
					<td align="left" class="blue_td">'.$l['alloc_space'].'</td>
					<td class="val">'.$info['details']['alloc_space'].' GB</td>
				</tr>
				<tr>
					<td align="left" class="blue_td">'.$l['poweredby'].'</td>
					<td class="val"><img src="'.$theme['images'].$usage['cpu']['manu'].'.gif" /></td>
				</tr>
				<tr>
					<td align="left" class="blue_td">'.$l['cpumodel'].'</td>					
					<td class="val">'.$usage['cpu']['cpumodel'].'</td>
				</tr>
			</table>
		</div>
		<div class="col-sm-6">
			<table align="center" class="table table-hover" cellpadding="3" cellspacing="4" border="0" width="100%">
				<tr>
					<td align="left" class="blue_td" width="30%">'.$l['osinstalled'].'</td>
					<td class="fhead val"><img src="'.$theme['images'].'/'.$info['os']['distro'].'.gif" />&nbsp;&nbsp;'.$info['os']['name'].'</td>
				</tr>
				<tr>
					<td align="left" class="blue_td">'.$l['servstatus'].'</td>
					<td class="val">'.($info['status'] == 1 ? '<img src="'.$theme['images'].'runbig.gif" />&nbsp;&nbsp;'.$l['online'].'' : '<img src="'.$theme['images'].'stopbig.gif" />&nbsp;&nbsp;'.$l['offline'].'').'</td>
				</tr>
				<tr>
					<td align="left" class="blue_td">'.$l['uptime'].'</td>
					<td class="val" id="uptime_data">'.$info['uptime'].'</td>
				</tr>
				<tr>
					<td align="left" class="blue_td">'.$l['kernel'].'</td>
					<td class="val">'.$info['kernel_name'].'</td>
				</tr>
				<tr>
					<td align="left" class="blue_td">'.$l['alloc_ram'].'</td>
					<td class="val">'.$info['details']['alloc_ram'].' MB</td>
				</tr>
				<tr>
					<td align="left" class="blue_td">'.$l['alloc_bandwidth'].'</td>
					<td class="val">'.$info['details']['alloc_bandwidth'].' GB</td>
				</tr>
				<tr><td align="left" class="blue_td">'.$l['alloc_space'].'</td><td class="val">'.$info['details']['alloc_space'].' GB</td></tr>
			</table>
		</div>
	</div>
</div>

<br />
<br />';
}

if(is_allowed('server_statistics')){

echo '<div class="divroundshad">
	<div class="roundheader"><center>'.$l['server_resources'].'</center></div>

	<div class="row" style="text-align:center;">
		<div class="col-sm-3">
			<div class="h4" style="padding:5px;">'.$l['cpuinfo'].'</div>
			<div id="server_cpu" class="mgraph" style="margin:auto;"></div>
			<br /><div id="server_cpu_text">&nbsp;</div>
		</div>
		<div class="col-sm-3">
			<div class="h4" style="padding:5px;">'.$l['raminfo'].'</div>
			<div id="server_ram" class="mgraph" style="margin:auto;"></div>
			<br /><div id="server_ram_text">&nbsp;</div>
		</div>
		<div class="col-sm-3">
			<div class="h4" style="padding:5px;">'.$l['diskinfo'].'</div>
			<div id="server_disk" class="mgraph" style="margin:auto;"></div>
			<br /><div id="server_disk_text">&nbsp;</div>
		</div>
		<div class="col-sm-3">
			<div class="h4" style="padding:5px;">'.$l['bandwidthinfo'].'</div>
			<div id="server_bandwidth" class="mgraph" style="margin:auto;"></div>
			<br /><div id="server_bandwidth_text">&nbsp;</div>
		</div>
	</div>
</div>
<br />
<br />';
}

echo '
<div class="divroundshad">
	<div class="roundheader"><center>'.$l['server_options'].'</center></div>
	<div class="row">
		<div class="col-sm-1"></div>
		<div class="col-sm-10">
			<div class="serv_op">
				<a href="'.$globals['index'].'act=config" class="micons">
				<img src="'.$theme['images'].'admin/settingsdp.gif" /><br /><div style="padding-top:5px;">'.$l['settings'].'</div>
				</a>
			</div>
			<div class="serv_op">
				<a href="'.$globals['index'].'act=vscpu" class="micons">
				<img src="'.$theme['images'].'cpu.png" /><br /><div style="padding-top:5px;">'.$l['cpu_usage'].'</div>
				</a>
			</div>
			<div class="serv_op">
				<a href="'.$globals['index'].'act=vsram" class="micons">
				<img src="'.$theme['images'].'ram.png" /><br /><div style="padding-top:5px;">'.$l['ram_usage'].'</div>
				</a>
			</div>
			<div class="serv_op">
				<a href="'.$globals['index'].'act=vsbandwidth" class="micons">
				<img src="'.$theme['images'].'bandwidth.png" /><br /><div style="padding-top:5px;">'.$l['bandwidth_usage'].'</div>
				</a>
			</div>
			<div class="serv_op">
				<a href="'.$globals['index'].'act=manageserver&reboot=1" class="micons" onclick="return conf_reboot();">
				<img src="'.$theme['images'].'restart.png" /><br /><div style="padding-top:5px;">'.$l['reboot'].'</div>
				</a>
			</div>
			<div class="serv_op">
				<a href="'.$globals['index'].'act=backup_plans" class="micons">
				<img src="'.$theme['images'].'admin/backups.png" /><br /><div style="padding-top:5px;">'.$l['backups'].'</div>
				</a>
			</div>
			<div class="serv_op">
				<a href="'.$globals['index'].'act=vpsrestore" class="micons">
				<img src="'.$theme['images'].'admin/restore.png" /><br /><div style="padding-top:5px;">'.$l['list_backups'].'</div>
				</a>
			</div>

		</div>
		<div class="col-sm-1"></div>
	</div>
	
</div>

<p class="notice">'.$l['trademarks'].'</p>'; 

}

echo '</div>';

softfooter();

}

?>