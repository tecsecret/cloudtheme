<?php

//////////////////////////////////////////////////////////////
//===========================================================
// addserver_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function migrate_theme(){

global $theme, $globals, $servers, $user, $l, $list, $cluster, $error, $done, $servergroups, $migrate_status, $cpu, $progress, $migrate_done, $migrate_run, $mprog, $vpsid, $alliplist, $vpsips, $actid, $multi_mig_started, $selected_act, $disk_storages, $ip_resources;
	
//Is Ajax Mode on?
if(optREQ('do')){

	$do = optREQ('do');
	
	// Is there any error
	if(!empty($error)){
		$json['error'] = $error;
	}
	
	if($do == 'getvpslist'){	
		$json['list'] = $list;	
		echo array2json($json);
	}

	if($do == 'get_resources'){
		$json['disk_storages'] = $disk_storages;
		$json['vps_ip_resources'] = $ip_resources['vps'];
		$json['server_ip_resources'] = $ip_resources['server'];
		$json['vpsinfo'] = getvps($vpsid);
		echo array2json($json);
	}
	
	return true;
}


softheader($l['<'.$selected_act[0].'title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-vs icon-head"></i>&nbsp; '.$l['<'.$selected_act[0].'title>'].'<span style="float:right;" ><a href="'.$globals['docs'].'GUI_Migration" target="_blank" class="wiki_help" title="'.$l['wiki_help'].'"><i class="icon-help" ></i></a></span></center>';

if(!empty($saved)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['added'].'</div><br />';
}

if(!empty($error['vdf_errors'])){
	$error[] = $l['err_vdf_suggest'];
}
error_handle($error);

if(!empty($multi_mig_started)){
	echo '<div class="notice" id="running"><img src="'.$theme['images'].'notice.gif" /> '.$l['note_mig_started'].'</div>';
}

echo '<script language="javascript" type="text/javascript"><!-- // --><![CDATA[
var actid = '.(isset($actid) ? $actid : "-1").';
var progress = 0;
var progress_update = "";
var update_status_txt = "";
var timer = null;
var server_ip_resources = "";

function getvpslist(){
	
	var from_server = $("#from_server").val();
	
	// If there are no VPS we check for that and return from here only. As there is nothing to check !
	if(from_server == "select_server"){
		return;
	}
	
	if(from_server == -1){
		$("#ip_tr").show();
		$("#pass_tr").show();
		
		var from_ip = $("#from_ip").val();
		var from_pass = $("#from_pass").val();
		
		if(from_ip.length < 1 || from_pass.length < 1){
			return false;
		}
		
	}else{
		$("#ip_tr").hide();
		$("#pass_tr").hide();
	}
	
	// Show the image
	$("#prog_img").show();
	
	$.ajax({
		type: "POST",
		dataType: "json",
		url: "index.php?act='.$selected_act.'&do=getvpslist",
		data: $("#migrate_form").serialize(),
		success: function(data, textStatus, jqXHR){	
	
			// Hide the image
			$("#prog_img").hide();			
			
			// Are there any errors ?
			if(typeof(data["error"]) != "undefined"){
				var errors = "";
				for(x in data["error"]){
					errors = errors + data["error"][x];
				}
				alert(errors);
			}
			
			// Build the VPS list
			var vpsopts = "";
			for(x in data["list"]){
				vpsopts = vpsopts + \'<option value="\'+x+\'">\'+x+\' - \'+data["list"][x]["hostname"]+\'</option>\';
			}
			
			$("#vpsid").html(vpsopts);
			//$("#vpsid").select2();		
		}
	});

}

function toggle_resources(){
	
	var selected_vps = [];
	
	$_("storage_row").style.display = "none";
	$("#ip_resource").hide();
	
	if($("#to_server").val().localeCompare("select_server") == 0){
		return;
	}
	
	$("#vpsid :selected").each(function(i, selected){
		selected_vps[i] = $(selected).text(); 
	});
	
	// If the count is more than 1, then do not display storage selection fields
	if(selected_vps.length != 1){
		$_("storage_row").style.display = "none";
		$("#ip_resource").hide();
		return;
	}
	
	$_("storage_row").style.display = "block";
	
	if(!$("#preserve_ip").is(":checked")){
		$("#ip_resource").show();
	}
}


function get_resources(){
	
	var selected_vps = [];
	var vps_id = $("#vpsid").val();
	
	// If there are no VPS we check for that and return from here only. As there is nothing to do !
	if(vps_id == 0){
		return;
	}
	
	$("#vpsid :selected").each(function(i, selected){
		selected_vps[i] = $(selected).text(); 
	});
	
	if(selected_vps.length != 1){
		return;
	}
	
	$("#storage").html("");
	
	toggle_resources();
	
	';
	if($selected_act != 'migrate'){
		echo '
		if($("#to_server").val() == 0){
			$("#ignore_if_vdfconflict").prop("checked", true);
			$("#ignore_if_vdfconflict").prop("disabled", true);
		}else{
			$("#ignore_if_vdfconflict").prop("checked", false);
			$("#ignore_if_vdfconflict").prop("disabled", false);
		}';
	}
	
	echo '
	
	if($("#to_server").val().localeCompare("select_server") == 0){
		return;
	}
	
	$("#storage").html("<img src=\"'.$theme['images'].'loading_35.gif\"/>");
	
	$.ajax({
		type: "POST",
		dataType: "json",
		url: "index.php?act='.$selected_act.'&do=get_resources",
		data: $("#migrate_form").serialize(),
		success: function(data, textStatus, jqXHR){
	
			// Are there any errors ?
			if(typeof(data["error"]) != "undefined"){
				var errors = "";
				for(x in data["error"]){
					errors = errors + data["error"][x];
				}
				//alert(errors);
			}
			
			var ip_resources_table = "<table border=\'0\' class=\"table\" style=\"width:80% !important\"><tr><th></th><th>VPS IPs</th><th>'.$l['num_free_ips'].'"+$("#to_server option:selected").text()+"</th></tr>";
			
			var num_ips = {ips:"'.$l['ips'].'", ipv6:"'.$l['ipv6'].'", ipv6_subnet:"'.$l['ipv6_subnet'].'", ips_int : "'.$l['ips_int'].'"}; 
			server_ip_resources =  data["server_ip_resources"];
			for(x in data["vps_ip_resources"]){
				
				var bgcolor = "style=\"color:green;\"";
				if(data["vps_ip_resources"][x] > data["server_ip_resources"][x]){
					var bgcolor = "style=\"color:red;\"";
				}
				
				ip_resources_table += "<tr id=\'"+x+"_tr\' "+bgcolor+"><td>"+num_ips[x]+"</td><td>"+data["vps_ip_resources"][x]+"</td><td >"+data["server_ip_resources"][x]+"</td></tr>";
				var num = data["vps_ip_resources"][x];
				$("#num_"+x).val(num);
				$("#num_"+x+"_row").show();
			}
			
			ip_resources_table += "</table>";
			$("#ip_resource").show();
			$("#ip_resources_table").html(ip_resources_table);
			
			var storageopts = "";
			
			if(data["disk_storages"] == undefined ){
				var msg = "'.$l["no_storage_select"].'";
				$("#storage").html(msg);
				return;
			}
			
			if(data["disk_storages"]["no_storage_select"]){
				$("#storage").html(data["disk_storages"]["no_storage_select"]);
				return;
			}
			
			// Build the Storage List
			for(x in data["disk_storages"]){
				var num = parseInt(x);
				var size = data["vpsinfo"]["disks"][x]["size"];
				
				size = parseFloat(size).toFixed(2);
				num++;
				
				var format = data["vpsinfo"]["disks"][x]["format"];
				
				if(format == ""){
					if(data["vpsinfo"]["disks"][x]["type"] == "block"){
						format = "raw";
					}else if(data["vpsinfo"]["disks"][x]["type"] == "file"){
						format = "qcow2";
					}
				}
				
				storageopts += \'<div class="row"><div class="col-sm-3"> <label class="control-label" style="margin-top:8px"> DISK&nbsp;\' + num + \'</label> (&nbsp;\'+size+\'&nbsp;GB&nbsp;)<br />\' + format.toUpperCase() +\'</div>\';
				
				if(typeof data["disk_storages"][x] == "object"){
					storageopts +=  \'<div class="col-sm-9"><select class="form-control" name="storage[\'+x+\']">\';
					
						for(y in data["disk_storages"][x]){
							
							storageopts += \'<option value="\'+y+\'" \'+(data["disk_storages"][x][y]["primary_storage"] == 1 ? "selected=selected" : "")+\'>\'+y+\' - \'+data["disk_storages"][x][y]["name"]+\' (\'+data["disk_storages"][x][y]["free"]+\'GB Free)</option>\';
						}
					storageopts += \'</select></div></div><br />\'; 
				}else if(typeof data["disk_storages"][x] == "string"){
					storageopts += \'<div class="col-sm-9"><span style="color:red;">\'+data["disk_storages"][x]+\'</span></div></div>\';
				}
			}
			
			if(storageopts == ""){
				storageopts = errors;
			}
			
			$("#storage").html(storageopts);
		}
	});

}';

echo '
// Clean IDs
function ids_clean(str){
	str = str.replace(/\./gi, "_"); 
	str = str.replace(/\:/gi, "_"); 
	return str;
}

// Draw a Resource Graph
function resource_graph(id, data){

    $.plot($("#"+id), data, 
	{
		series: {
			pie: { 
				innerRadius: 0.7,
				radius: 1,
				show: true,
				label: {
					show: true,
					radius: 0,
					formatter: function(label, series){
						if(label != "Completed") return "";
						return \'<div style="font-size:18px;text-align:center;padding:2px;color:black;">\'+Math.round(series.percent)+\'%</div><div style="font-size:10px;">\'+label+\'</div>\';	
					}
				}
			}
		},
		legend: {
			show: false
		}
	});
}


function donefunc(data){

	if(data == 1){
		$("#complete").show();
		$("#running").hide();
		$("#progress-cont").hide();
	}else{
		progress_onload();
		$("#complete").hide();
		$("#running").show();
		$("#progress-cont").show();
	}
};

function startusage(){
	toggle_resources();
	timer = setTimeout("get_progress(\'migrate\')", 1000);
}

function make_mig_table(id, data){
		
	try{
		data = JSON.parse(data);
	}catch(e){
		return;
	}
	var j = 1;
	var html = "<center><table o><tr>";
	$.each(data, function(i, item){
		
		html += "<td>&nbsp;&nbsp;<b>"+i+"&nbsp;&nbsp;&nbsp;</b></td><td>"+item+"</td>";
		if(j % 2 == 0){
			html += "</tr><tr>";
		}
		j++;
	});
	
	html += "</tr></table></center>";
	$(id).html(html);
};

function changeipres(name){
	
	var value = parseInt($("#"+name).val());
	
	var name = name.substr(4);
	
	if(server_ip_resources[name] < value){
		$("#"+name+"_tr").css("color", "red");
	}else{
		$("#"+name+"_tr").css("color", "green");
	}
}

addonload("getvpslist(); '.(!empty($migrate_status) || !empty($migrate_done) ? "startusage();donefunc(2);$('#progressbar').progressbar();" : "").' '.(isset($_POST['migrate_but']) ? "get_resources();" : "").'");

// ]]></script>

<div id="form-container">
<form accept-charset="'.$globals['charset'].'" name="migrate_form" method="post" action="" id="migrate_form" class="form-horizontal">

<div class="row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['from_server'].'</label>
		<span class="help-block">'.$l['from_server_exp'].'</span>
	</div>
	<div class="col-sm-4 server-select-lg">
		<select class="form-control virt-select" name="from_server" id="from_server" onchange="getvpslist();" style="width:100%">
		<option value="select_server" selected="selected">'.$l['select_server'].'</option>';
		
		foreach($servers as $k => $v){		 
			echo '<option value="'.$k.'" '.(POSTval('from_server') == $k && isset($_POST['from_server']) ? 'selected="selected"' : '').'>'.$v['server_name'].'</option>';
		}
		
		// Uncomment it if we plan to release migrate from non-slaved server
		//<option value="-1" '.(POSTval('from_server') == -1 ? 'selected="selected"' : '').'>'.$l['enter_keys'].'</option>
		
	echo '</select>
	</div>
	<div class="col-sm-4"></div>
</div>

<div class="row" id="ip_tr" style="display:none;">
	<div class="col-sm-4">
		<label class="control-label">'.$l['from_ip'].'</label>
		<span class="help-block">'.$l['from_ip_exp'].'</span>
	</div>
	<div class="col-sm-4">
		<input type="text" class="form-control" name="from_ip" id="from_ip" size="30" value="'.POSTval('from_ip', '').'" />
	</div>	
	<div class="col-sm-4"></div>
</div>

<div class="row" id="pass_tr" style="display:none;">
	<div class="col-sm-4">
		<label class="control-label">'.$l['from_pass'].'</label>
		<span class="help-block">'.$l['from_pass_exp'].'</span>
	</div>
	<div class="col-sm-4">
		<input type="text" class="form-control" name="from_pass" id="from_pass" size="30" value="'.POSTval('from_pass', '').'" /> &nbsp; <input type="button" class="submit" onmouseover="sub_but(this)" onclick="getvpslist();" value="'.$l['getvpslist'].'" />
	</div>
	<div class="col-sm-4"></div>
</div>

<div class="row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['select_vps'].'</label>
		<span class="help-block">'.$l['select_vps_exp'].'</span>
	</div>
	<div class="col-sm-4">
		<div class="">
			<select name="vpsid[]" id="vpsid" style="width:100%" multiple="multiple" size="10" onchange="toggle_resources();get_resources();">
				<option value="0">'.$l['select_vps'].'</option>
			</select>
			<div class="help-block"></div>
		</div>
		<img src="'.$theme['images'].'loading_35.gif" id="prog_img" style="display:none;"/>
	</div>	
	<div class="col-sm-4"></div>
</div>

<div class="row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['to_server'].'</label>
		<span class="help-block">'.$l['to_server_exp'].'</span>
	</div>
	<div class="col-sm-4 server-select-lg">
		<select class="form-control virt-select" name="to_server" id="to_server" onchange="get_resources();" style="width:100%">
		<option value="select_server" selected="selected">'.$l['select_server'].'</option>';
		
		foreach($servers as $k => $v){		 
			echo '<option value="'.$k.'" '.(POSTval('to_server') == $k && isset($_POST['to_server']) ? 'selected="selected"' : '').'>'.$v['server_name'].'</option>';
		}
		
	echo '</select>
	</div>
	<div class="col-sm-4"></div>
</div>
<div class="row" id="storage_row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['select_storage'].'</label>
		<span class="help-block">'.$l['select_storage_exp'].'</span>
	</div>
	<div class="col-sm-4" id="storage"></div>
</div>';

if($selected_act == 'migrate'){

	echo '<br />
	<div class="row">
		<div class="col-sm-4" id="preserve_ip_row">
			<label class="control-label" for="preserve_ip" >'.$l['preserve_ip'].'</label>
			<span class="help-block">'.$l['preserve_ip_exp'].'</span>
		</div>
		<div class="col-sm-4">
			<input type="checkbox" name="preserve_ip" id="preserve_ip" value="1" '.POSTchecked('preserve_ip').' onchange="toggle_resources();" />
		</div>
		<div class="col-sm-4"></div>
	</div>
	<div class="row hide_for_multiple" id="ip_resource" style="display:none">
	
		<fieldset class="user_details_f">
			<legend class="user_details_f">'.$l['ip_resources'].'</legend>
			<div class="col-sm-6">
				<div class="row hide_for_multiple" id="num_ips_row" style="display:none">
					<div class="col-sm-6">
						<label class="control-label" for="num_ips" >'.$l['num_ips'].'</label>
						<span class="help-block">'.$l['num_ips_exp'].'</span>
					</div>
					<div class="col-sm-6">
						<input type="text" class="form-control ip_res" name="num_ips" id="num_ips" value="'.POSTval('num_ips').'" onkeyup="changeipres(this.name);" />
					</div>
				</div>
				<div class="row hide_for_multiple" id="num_ipv6_row" style="display:none">
					<div class="col-sm-6">
						<label class="control-label" for="num_ipv6" >'.$l['num_ipv6'].'</label>
						<span class="help-block">'.$l['num_ipv6_exp'].'</span>
					</div>
					<div class="col-sm-6">
						<input type="text" class="form-control ip_res" name="num_ipv6" id="num_ipv6" value="'.POSTval('num_ipv6').'" onkeyup="changeipres(this.name);" />
					</div>
				</div>
				<div class="row hide_for_multiple" id="num_ipv6_subnet_row" style="display:none">
					<div class="col-sm-6">
						<label class="control-label" for="num_ipv6" >'.$l['num_ipv6_subnet'].'</label>
						<span class="help-block">'.$l['num_ipv6_subnet_exp'].'</span>
					</div>
					<div class="col-sm-6">
						<input type="text" class="form-control ip_res" name="num_ipv6_subnet" id="num_ipv6_subnet" value="'.POSTval('num_ipv6_subnet').'" onkeyup="changeipres(this.name);" />
					</div>
				</div>
				<div class="row hide_for_multiple" id="num_ips_int_row" style="display:none">
					<div class="col-sm-6">
						<label class="control-label" for="num_ips_int" >'.$l['num_ips_int'].'</label>
						<span class="help-block">'.$l['num_ips_int_exp'].'</span>
					</div>
					<div class="col-sm-6">
						<input type="text" class="form-control ip_res" name="num_ips_int" id="num_ips_int" value="'.POSTval('num_ips_int').'" onkeyup="changeipres(this.name);" />
					</div>
				</div>
			</div>
			<div class="col-sm-6" id="ip_resources_table">
			</div>
		</fieldset>
	</div>';
}

if($selected_act == 'migrate'){
	
	echo '<div class="row">
		<div class="col-sm-4">
			<label class="control-label">'.$l['m_del_orig'].'</label>
			<span class="help-block">'.$l['m_del_orig_exp'].'<br><b>NOTE</b> : '.$l['mnote4'].'</span>
		</div>
		<div class="col-sm-4">
			<input type="checkbox" name="del_orig" '.POSTchecked('del_orig').' />
		</div>
		<div class="col-sm-4"></div>
	</div>';	
}

echo '<div class="row">
		<div class="col-sm-4">
			<label class="control-label" for="ignore_if_vdfconflict" >'.$l['m_ignore_if_vdfconflict'].'</label>
			<span class="help-block">'.$l['m_ignore_if_vdfconflict_exp'].'</span>
		</div>
		<div class="col-sm-4">
			<input type="checkbox" id="ignore_if_vdfconflict" name="ignore_if_vdfconflict" '.POSTchecked('ignore_if_vdfconflict').' />
		</div>
		<div class="col-sm-4"></div>
	</div>
	<div class="row">
	<div class="col-sm-4 ">
		<label class="control-label">'.$l['speed_limit'].'</label>
		<span class="help-block">'.$l['speed_limit_exp'].'</span>
	</div>
	<div class="col-sm-4">
		<input type="text" class="form-control" name="speed_limit" id="speed_limit" value="'.POSTval('speed_limit').'"/>
	</div>
	<div class="col-sm-4"></div>
</div><br />

<div class="row">
	<div class="col-sm-4 ">
		<label class="control-label">'.$l['disable_gzip'].'</label>
		<span class="help-block">'.$l['disable_gzip_exp'].'</span>
	</div>
	<div class="col-sm-4">
		<input type="checkbox" name="disable_gzip" '.POSTchecked('disable_gzip').' />
	</div>
	<div class="col-sm-4"></div>
</div>
</div>


<br /><br />
<input type="hidden" name="migrate" value="1" />
<center><input type="submit" class="btn" name="migrate_but" id="migrate_but" value="'.$l[$selected_act[0].'sub_but'].'" /></center>

<br /><br />
</form>
';

if(!empty($mprog)){
	echo '
	<div id="progress_onload"></div>
	<br />
	<div class="notice" id="complete"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l[$selected_act[0].'_complete'].'</div>
	<br />
	
	<div class="notice" id="running"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['existing'].' '.$l['mnote3'].'</div>
	<br />';
}

echo '
<div class="notebox">
<font class="bboxtxt"><b>NOTE</b> : <br />
<ul>
<li><b>'.$l['mnote4'].'</b></li>
<li>'.$l['mnote1'].'</li>
<li>'.$l['mnote2'].'</li>
<li>'.$l['mnote3'].'</li>
</ul>
</font>
</div>
</div>';

softfooter();

}

?>