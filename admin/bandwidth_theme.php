<?php

//////////////////////////////////////////////////////////////
//===========================================================
// bandwidth_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function bandwidth_theme(){

global $theme, $globals, $cluster, $user, $l, $bandwidth, $month, $band;

softheader($l['<title>']);

echo '
<div class="bg" style="width:99%">
<center class="tit"><i class="icon icon-bandwidth icon-head"></i>&nbsp; &nbsp;'.$l['heading'].'<span style="float:right;" ><a href="'.$globals['docs'].'Server_Bandwidth_Usage" target="_blank" class="wiki_help" title="'.$l['wiki_help'].'"><i class="icon-help" ></i></a></span></center><br />';

// Is it offline ?
$hypervisor_status = $cluster->statewise($globals['server']);
if($hypervisor_status == 0 || $hypervisor_status == 2){

	echo '<div class="e_notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['server_status_'.$hypervisor_status].'</div>';
	
}else{

echo '
<div class="row">
	<div class="col-sm-6" style="border-right:1px solid #CCCCCC">
		<div class="roundheader">'.$l['bandwidthinfo'].'</div>
		<table align="center" class="table table-hover" cellpadding="3" cellspacing="4" border="0" width="100%">
			<tr><td align="left" class="blue_td" width="30%">'.$l['Total_Bandwidth'].'</td><td class="fhead val">'.$bandwidth['limit_gb'].' GB</td></tr>
			<tr><td align="left" class="blue_td">'.$l['Bandwidth_utilised'].'</td><td class="fhead val">'.$bandwidth['used_gb'].' GB</td></tr>
			<tr><td align="left" class="blue_td">'.$l['in_used'].'</td><td>'.$bandwidth['in']['used_gb'].' GB</td></tr>
			<tr><td align="left" class="blue_td">'.$l['out_used'].'</td><td>'.$bandwidth['out']['used_gb'].' GB</td></tr>
			<tr><td align="left" class="blue_td">'.$l['percent_utilised'].'</td><td>'.$bandwidth['percent'].' %</td></tr>
		</table>
	</div>
	<div class="col-sm-6" align="center" width="50%">
		<div class="roundheader">'.$l['graphheader'].'</div>
		<br />
		<div id="bandwidth_pie" style="width:200px; height:200px;"></div>
	</div>
</div>
<div class="clearfix"></div>
<div class="row">
	<div class="col-sm-12" align="center" id="graph_td">';
			echo '<center class="tit">'.$month['mth_txt'].' '.$month['yr'].'</center>';
	        echo'<div style="width: 98%; height: 300px; padding: 0px; position: relative;overflow: hidden;" id="bandwidth_graph"></div>
	</div>
</div>
<br />
<br />

<div class="row">
	<div class="col-sm-12" align="center">
		<a href="'.$globals['ind'].'act=bandwidth&show='.$month['prev'].'" class="link_btn">Prev Month</a>
	
	'.($month['next'] > date('Ym') ? '' : '<a href="'.$globals['ind'].'act=bandwidth&show='.$month['next'].'" class="link_btn">Next Month</a>').'
	</div>
	
</div>

<br />

<script type="text/javascript" charset="utf-8"><!-- // --><![CDATA[	

// Draw a Resource Graph
function resource_graph(id, data){

    $.plot($("#"+id), data, 
	{
		series: {
			pie: { 
				innerRadius: 0.7,
				radius: 1,
				show: true,
				label: {
					show: true,
					radius: 0,
					formatter: function(label, series){
						if(label != "Used") return "";
						return \'<div style="font-size:18px;text-align:center;padding:2px;color:black;">\'+Math.round(series.percent)+\'%</div><div style="font-size:10px;">\'+label+\'</div>\';	
					}
				}
			}
		},
		legend: {
			show: false
		}
	});
}
	
var bandwidth_resource = [
	{ label: "Used",  data: '.(empty($bandwidth['used_gb']) ? 0.01 : $bandwidth['used_gb']).'},
	{ label: "Free",  data: '.(empty($bandwidth['free_gb']) ? 1 : $bandwidth['free_gb']).'}
];

resource_graph("bandwidth_pie", bandwidth_resource);

$(function () {
	
	function makedata(data){
	
		var fdata = [];
		i = 0;
		for (x in data){
			fdata.push([i, (data[x])]);
			i++;
		}
	
		return fdata;
		
	}

	var d1 = makedata([0, '.implode(', ', $bandwidth['in']['usage']).']);
	var d2 = makedata([0, '.implode(', ', $bandwidth['out']['usage']).']);
	
	var bandwidth_graph = [
		{ label: "In",  data: d1},
		{ label: "Out",  data: d2}
	];
	
	$.plot($("#bandwidth_graph"), bandwidth_graph, {
		series: {
			stack: true,
            points: { show: true },
			lines: { show: true, fill: true, steps: false }
		},
		legend: {
			show: true
		},
        grid: { hoverable: true}
	});
	
	function showTooltip(x, y, contents) {
        $(\'<div id="tooltip">\' + contents + \'</div>\').css( {
            position: "absolute",
            display: "none",
            top: y ,
            left: x + 20,
            border: "1px solid #CCCCCC",
            padding: "2px",
            "background-color": "#EFEFEF",
            opacity: 0.80
        }).appendTo("body").fadeIn(200);
    }

    var previousPoint = null;
    $("#bandwidth_graph").bind("plothover", function (event, pos, item) {
        $("#x").text(pos.x.toFixed(2));
        $("#y").text(pos.y.toFixed(2));

        if (item) {
			
			if (previousPoint != item.dataIndex) {
				previousPoint = item.dataIndex;
				
				$("#tooltip").remove();
				var x = item.datapoint[0].toFixed(2),
					y = item.datapoint[1].toFixed(2),
					inb = item.datapoint[2].toFixed(2);
			
				if(inb == 0){
					return;
				}
				
				showTooltip(item.pageX, item.pageY,
							"Total : " + parseInt(y) + " MB<br> In : " + parseInt(inb) + " MB<br> Out : " + parseInt(y - inb) + " MB <br>Day : " + parseInt(x));
			}
		} else {
			$("#tooltip").remove();
			previousPoint = null;
		}
    });
	
});

// ]]></script>';

}

echo '</div>';
softfooter();

}

?>