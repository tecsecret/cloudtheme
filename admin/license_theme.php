<?php

//////////////////////////////////////////////////////////////
//===========================================================
// license_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function license_theme(){

global $theme, $globals, $user, $l, $cluster, $info;

softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-config icon-head"></i>&nbsp; '.$l['lic_info'].'<span style="float:right;" ><a href="'.$globals['docs'].'Refresh_License" target="_blank" class="wiki_help" title="'.$l['wiki_help'].'"><i class="icon-help" ></i></a></span></center><br />';

// Is it offline ?
if(empty($info)){

	echo '<div class="e_notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['server_status_'.$hypervisor_status].'</div>';
	
}else{


echo '

<div class="row">
	<div class="col-sm-12">
		<div class="roundheader">'.$l['lic_info'].'</div>
		<table align="center" class="table no_border table-hover" cellpadding="3" cellspacing="4" border="0" width="100%">
			<tr>
				<td align="left" class="blue_td fhead" width="30%">'.$l['soft_license'].'</td>
				<td class="val">'.$info['license'].' &nbsp; <a href="'.$globals['ind'].'act=license&refreshlicense=1" class="btn">'.$l['refreshlicense'].'</a></td>
			</tr>'.
				(!empty($info['primary_ip']) ? '
				<tr>
					<td align="left" class="blue_td fhead" width="30%">'.$l['license_ip'].'</td>
					<td class="val">'.$info['primary_ip'].'</td>
				</tr>' : '').'
			<tr>
				<td align="left" class="blue_td fhead" width="30%">'.$l['server_addr'].'</td>
				<td class="val">'.$info['SERVER_ADDR'].'</td>
			</tr>
			<tr>
				<td align="left" class="blue_td fhead" width="30%">'.$l['soft_license_type'].'</td>
				<td class="val"><label class="val">'.$info['lictype_txt'].'</label> &nbsp;(<a href="https://www.virtualizor.com/pricing" target="_blank">'.$l['pricing'].'</a>) &nbsp;(<a href="https://www.virtualizor.com/clients" target="_blank">'.$l['client_center'].'</a>)</td>
			</tr>
			<tr>
				<td align="left" class="blue_td fhead" width="30%">'.$l['licactive'].'</td>
				<td class="val">'.$info['active_txt'].'</td>
			</tr>
			<tr>
				<td align="left" class="blue_td fhead" width="30%">'.$l['licexpires'].'</td>
				<td class="val">'.$info['licexpires_txt'].'</td>
			</tr>
			<tr>
				<td align="left" class="blue_td fhead" width="33%">'.$l['licnumvs'].'</td>
				<td class="val">'.(empty($info['licnumvs']) ? $l['vs_unlimited'] : $info['licnumvs']).'</td>
			</tr>
		</table>

		<div class="roundheader toggle_hover" onclick="toggle_enterlic();" style="cursor:pointer;"><label id="toggle_indicator" style="width:10px;">+</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp'.$l['enter_lic_key'].'</div>
		<div id="add_license_key" style="width:100%;">
			<div class="cols-sm-12">
				<form name="lic_form" action="" method="POST" onsubmit="submit_licenseKey();return false;">
					<div class="form-group">
						<div align="left" class="col-sm-4 blue_td" width="20%" style="padding:12px;">
							<label for="input_license_key" class="fhead">'.$l['lic_key'].'</label><br />
							<span class="help-block">'.$l['lic_key_exp'].'</span>							
						</div>
						<div class="col-sm-7">
							<input type="text" class="form-control" id="input_license_key" name="input_license_key" style="margin: 10px 0px;">
						</div>	
						<div class="col-sm-1 text-center">
							<input type="submit" class="btn" style="margin:5px 0px;" value="'.$l['submit'].'" />
						</div>	
					</div>
				</form>
			</div>
		</div>
		<div class="col-sm-12 text-center" id="process_img" style="display:none;">
			<img src="'.$theme['images'].'/loading_35.gif">
		</div>
	</div>
</div>
<br />
<br />';

}
echo '</div>';
echo '
<script>

	function toggle_enterlic(){

		if ($("#add_license_key").is(":hidden")){
			$("#add_license_key").slideDown("slow");
			$("#toggle_indicator").text(" - ");
		}else{
			$("#add_license_key").slideUp("slow");
			$("#toggle_indicator").text(" + ");
		}
	}
	
	function submit_licenseKey(){
		
		var lickey = $.trim($("#input_license_key").val());
		
		if(lickey.length != 29){
			alert("'.$l['inv_key_length'].'");
			return;
		}
		
		if(!(/^VIRT(D|E)/.exec(lickey))){
			alert("'.$l['inv_lic'].'");
			return;
		}
		
		$("#process_img").css("display","");
		
		$.ajax({
			url: "'.$globals['ind'].'act=license&api=json",
			data: "lickey="+lickey+"&licform=1",
			dataType: "json",
			type: "post",
			success: function(data){
				
				$("#process_img").css("display","none");
				if("error" in data){
					alert(data["error"]);
					return false;
				}
				if("done" in data){
					alert(data["done"]);
					location.href = location.href;
				}
			},
			error: function(data){
				$("#process_img").css("display","none");
				alert("'.$l['lic_error'].'");
			}
		})
	}
	
</script>
';
softfooter();

}

?>