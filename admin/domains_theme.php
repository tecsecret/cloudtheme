<?php

//////////////////////////////////////////////////////////////
//===========================================================
// domains_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function domains_theme(){

global $theme, $globals, $kernel, $user, $l, $cluster, $error, $servers, $done, $domains, $pdnsid;

softheader($l['<title>']);

echo '
<div class="bg" style="width:99%">
<center class="tit">
<i class="icon icon-pdns icon-head"></i>&nbsp; '.$l['dns_zones'].'<span style="float:right"><a href="javascript:showsearch();"><img src="'.$theme['images'].'admin/search.gif" /></a></span></center>';

error_handle($error);

echo '<script language="javascript" type="text/javascript"><!-- // --><![CDATA[

function Deldomains(id){

	id = id || 0;
	
	// List of ids to delete
	var domain_list = new Array();
	
	if(id < 1){
		
		if($("#domains_task_select").val() != 1){
			alert("'.$l['no_action'].'");
			return false;
		}
		
		$(".ios:checked").each(function() {
			domain_list.push($(this).val());
		});
		
	}else{
		
		domain_list.push(id);
		
	}
	
	if(domain_list.length < 1){
		alert("'.$l['nothing_selected'].'");
		return false;
	}
	
	var domain_conf = confirm("'.$l['del_conf'].'");
	if(domain_conf == false){
		return false;
	}
	
	var finalData = new Object();
	finalData["del"] = domain_list.join(",");
	
	//alert(finalData);
	//return false;
	
	$("#progress_bar").show();
	
	$.ajax({
		type: "POST",
		url: "'.$globals['index'].'act=domains&pdnsid='.$pdnsid.'&api=json",
		data : finalData,
		dataType : "json",
		success: function(data){
			$("#progress_bar").hide();
			if("done" in data){
				alert("'.$l['action_completed'].'");
			}
			if("error" in data){
				alert(data["error"]);
			}
			location.reload(true);
		},
		error: function(data) {
			$("#progress_bar").hide();
			//alert(data.description);
			return false;
		}
	});
	
	return false;
};


// ]]></script>

<div id="showsearch" style="display:'.(optREQ('search') || (!empty($domains) && !empty($globals['showsearch'])) ? "" : "none").';">
<form accept-charset="'.$globals['charset'].'" name="pdns" method="GET" action="" class="form-horizontal">
<input type="hidden" name="act" value="domains">
<input type="hidden" name="pdnsid" value="'.$pdnsid.'">
<div class="form-group_head">
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-2"><label>'.$l['name'].'</label> </div>
		<div class="col-sm-3"><input type="text" class="form-control" name="domain_name" id="domain_name" size="30" value="'.POSTval('domain_name', '').'" /></div>
		<div class="col-sm-3"><input type="submit" name="search" value="'.$l['submit'].'" class="go_btn"/></div>
		<div class="col-sm-1"></div>
	</div>
</div>

</form>
<br />
<br />
</div>';

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
}

if(empty($domains)){

	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.(optREQ('search') ? $l['no_res'] : $l['no_zones']).'</div>';
	
}
else{
	
page_links($globals['num_res'], $globals['cur_page'], $globals['reslen']);
echo '<br /><br />
<form accept-charset="'.$globals['charset'].'" name="multi_domain" id="multi_domain" method="post" action="" class="form-horizontal">
	<table align="center" cellpadding="10" cellspacing="1" border="0" width="95%" class="table table-hover tablesorter">
		<tr>
			<th align="center">'.$l['id'].'</td>
			<th align="center">'.$l['name'].'</td>
			<th align="center">'.$l['type'].'</td>
			<th align="center">'.$l['master'].'</td>
			<th align="center">'.$l['lastcheck'].'</td>
			<th align="center">'.$l['serial'].'</td>
			<th align="center">'.$l['account'].'</td>
			<th align="center" colspan="2" width="10">'.$l['manage'].'</td>
			<th><input type="checkbox" class="select_all" name="select_all" id="select_all"></th>
		<tr>';
	
$i = 1;
	
foreach($domains as $k => $v){
		
	echo '<tr>
		<td align="left">'.$v['id'].'</td>
		<td>'.$v['name'].'</td>
		<td>'.$v['type'].'</td>
		<td>'.$v['master'].'</td>
		<td>'.$v['lastcheck'].'</td>	
		<td>'.$v['serial'].'</td>	
		<td>'.$v['account'].'</td>	
		<td width="10" align="center"><a id="view_records" href="'.$globals['index'].'act=dnsrecords&pdnsid='.$pdnsid.'&domain_id='.$k.'" title="'.$l['view_records'].'"><img src="'.$theme['images'].'admin/plans.gif" /></a></td>
		<td width="10" align="center"><a id="delete" href="javascript:void(0);" onclick="return Deldomains('.$k.');" title="'.$l['del_pdns'].'"><img src="'.$theme['images'].'admin/delete.png" /></a></td>
		<td width="20" valign="middle" align="center">
			<input type="checkbox" class="ios" name="domains_list[]" value="'.$k.'"/>
		</td>
		</tr>';
		
		$i++;
 }


echo '</table>

<div class="row">
	<div class="col-sm-7"></div>
	<div class="col-sm-5"><label style="float:left;padding: 8px;padding-left: 37px;">'.$l['with_selected'].'</label>
		<select class="form-control" name="domains_task_select" id="domains_task_select" style="width:35%;float:left;">
			<option value="0">---</option>
			<option value="1">'.$l['ms_delete'].'</option>
		</select>&nbsp;
		<input type="submit" id ="domains_submit" class="go_btn" name="domains_submit" value="Go" onclick="Deldomains(); return false;">
	</div>
</div>
</form>


<div id="progress_bar" style="height:125px; display:none">
	<br />
	<center>
		<font id="progress_txt" size="4" color="#222222">'.$l['action_msg'].'</font>
		<br>
		<br>
	</center>
	<table id="table_progress" width="500" height="28" cellspacing="0" cellpadding="0" border="0" align="center" style="border:1px solid #CCC; -moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px;background-color:#efefef;">
		<tbody>
			<tr>
				<td id="progress_color" width="100%" style="background-image: url(themes/default/images/bar.gif); -moz-border-radius: 4px; -webkit-border-radius: 4px; border-radius: 4px;"></td>
				<td id="progress_nocolor"> </td>
			</tr>
		</tbody>
	</table>
	<br>
	<center>
		'.$l['notify_msg'].'
	</center>
</div>

<br />
<center><a onclick="delete()" href="'.$globals['ind'].'act=rdns&pdnsid='.$pdnsid.'" class="link_btn">'.$l['add_rdns'].'</a>
<a href="'.$globals['ind'].'act=dnsrecords&pdnsid='.$pdnsid.'" class="link_btn">'.$l['dnsrecords'].'</a></center>';

}

page_links($globals['num_res'], $globals['cur_page'], $globals['reslen']);
echo '</div>';
softfooter();

}

?>