<?php

//////////////////////////////////////////////////////////////
//===========================================================
// admin_acl_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 2.2.5
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       0th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function add_admin_acl_theme(){

global $theme, $globals, $cluster, $servers, $user, $l, $done, $error;

softheader($l['<title>']);

echo '<style>label{font-weight: normal;padding-top: 5px;}</style>
<div class="bg">
<center class="tit"><i class="icon icon-users icon-head"></i>&nbsp; '.$l['header'].'<span style="float:right;" ><a href="'.$globals['docs'].'Administrator_ACL" target="_blank" class="wiki_help" title="'.$l['wiki_help'].'"><i class="icon-help" ></i></a></span></center><br />';

error_handle($error);

if(!is_allowed('add_admin_acl')){
	softfooter();
	return;
}

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'&nbsp;<a href="'.$globals['ind'].'act=admin_acl">'.$l['back_admin_acl'].'</a></div>';
}else{

echo '
<script language="javascript" type="text/javascript"><!-- // --><![CDATA[

function toggle_div(id){
	if ($("#"+id).is(":hidden")){
		$("#"+id).slideDown("slow");
	}else{
		$("#"+id).slideUp("slow");
	}
}

function plus_onmouseover(id){
	$("#"+id).attr("src", "'.$theme['images'].'admin/plus_hover.gif");
}

function plus_onmouseout(id){
$("#"+id).attr("src", "'.$theme['images'].'admin/plus.gif");
}

// ]]></script>
<br/><br/>
<form accept-charset="'.$globals['charset'].'" name="add_admin_acl" method="post" action="" class="form-horizontal">
<center>
	<div class="row">
		<div class="col-sm-2"></div>
		<div class="col-sm-2">
			<label class="control-label" style="margin-top:7px">'.$l['name'].'</label></div>
		<div class="col-sm-6 pull-left">
			<input type="text" class="form-control" name="name" id="name" size="30" value="'.POSTval('name', '').'" style="width:60%"/></div>
		<div class="col-sm-2"></div>
	</div>
</center>

<br/><br/>
<center><div><label><input type="checkbox" class="select_all" id="select_all" /> &nbsp; '.$l['checkall'].'</label></div></center>
<br/><br/>

<div class="cat-wrapper">
	<div class="roundheader" style="cursor:pointer;valign:middle;" onclick="toggle_div(\'admindash\');">'.$l['cat_admin_dash'].'</div>
	<div id="admindash" style="width:100%;" class="togglediv">
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_cluster_statistics" '.POSTchecked('act_cluster_statistics').' /> &nbsp; '.$l['cluster_statistics'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_server_statistics" '.POSTchecked('act_server_statistics').' /> &nbsp; '.$l['server_statistics'].'</label></div>
			<div class="col-sm-3"></div>
			<div class="col-sm-3"></div>
		</div>
	</div>
</div>
<br/>

<div class="cat-wrapper">
	<div class="roundheader" style="cursor:pointer;" onclick="toggle_div(\'vs\');">'.$l['cat_vs'].'</div>
	<div id="vs" style="width:100%;" class="togglediv">
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" '.POSTchecked('act_vs').' name="act_vs" /> &nbsp; '.$l['listvs'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" '.POSTchecked('act_vsresources').' name="act_vsresources" /> &nbsp; '.$l['vsresources'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" '.POSTchecked('act_addvs').' name="act_addvs" /> &nbsp; '.$l['addvs'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" '.POSTchecked('act_rebuildvs').' name="act_rebuildvs" /> &nbsp; '.$l['rebuildvs'].'</label></div>
		</div>
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" '.POSTchecked('act_editvs').' name="act_editvs"/> &nbsp; '.$l['editvs'].'</label></div>		
			<div class="col-sm-3"><label><input type="checkbox" class="ios" '.POSTchecked('act_deletevs').' name="act_deletevs" /> &nbsp; '.$l['deletevs'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_suspendvs" '.POSTchecked('act_suspendvs').' /> &nbsp; '.$l['suspendvs'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_unsuspendvs" '.POSTchecked('act_unsuspendvs').' /> &nbsp; '.$l['unsuspendvs'].'</label></div>
		</div>
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_vnc" '.POSTchecked('act_vnc').' /> &nbsp; '.$l['vsvnc'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" '.POSTchecked('act_migrate').' name="act_migrate" /> &nbsp; '.$l['migrate'].'</label></div>
		</div>
	</div>
</div>
<br/>

<div class="cat-wrapper">
	<div class="roundheader" style="cursor:pointer;" onclick="toggle_div(\'ippool\');">'.$l['cat_ippools'].'</div>
	<div id="ippool" style="width:100%;" class="togglediv">
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_ippool" '.POSTchecked('act_ippool').'  /> &nbsp; '.$l['ippools'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_addippool" '.POSTchecked('act_addippool').'  /> &nbsp; '.$l['addippool'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_editippool" '.POSTchecked('act_editippool').'  /> &nbsp; '.$l['editippool'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_deleteippool" '.POSTchecked('act_deleteippool').'  /> &nbsp; '.$l['deleteippool'].'</label></div>		
		</div>
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_ips" '.POSTchecked('act_ips').'  /> &nbsp; '.$l['ips'].'</label></div>		
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_addips" '.POSTchecked('act_addips').'  /> &nbsp; '.$l['addips'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_editips" '.POSTchecked('act_editips').' /> &nbsp; '.$l['editips'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_deleteips" '.POSTchecked('act_deleteips').' /> &nbsp; '.$l['deleteips'].'</label></div>
		</div>
		<br/>
	</div>
</div>
<br/>

<div class="cat-wrapper">
	<div class="roundheader" style="cursor:pointer;" onclick="toggle_div(\'servers\');">'.$l['cat_servers'].'</div>
	<div id=servers style="width:100%;" class="togglediv">
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_servers" '.POSTchecked('act_servers').' /> &nbsp; '.$l['servers'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_addserver" '.POSTchecked('act_addserver').' /> &nbsp; '.$l['addserver'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_editserver" '.POSTchecked('act_editserver').' /> &nbsp; '.$l['editserver'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_deleteserver" '.POSTchecked('act_deleteserver').' /> &nbsp; '.$l['deleteserver'].'</label></div>
		</div>
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_sg" '.POSTchecked('act_sg').' /> &nbsp; '.$l['sg'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_addsg" '.POSTchecked('act_addsg').' /> &nbsp; '.$l['addsg'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_editsg" '.POSTchecked('act_editsg').' /> &nbsp; '.$l['editsg'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_deletesg" '.POSTchecked('act_deletesg').' /> &nbsp; '.$l['deletesg'].'</label></div>
		</div>
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_manageserver" '.POSTchecked('act_manageserver').' /> &nbsp; '.$l['manageserver'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_rebootserver" '.POSTchecked('act_rebootserver').' /> &nbsp; '.$l['reboot'].'</label></div>
		</div>
	</div>
</div>
<br/>

<div class="cat-wrapper">
	<div class="roundheader" style="cursor:pointer;" onclick="toggle_div(\'storage\');">'.$l['cat_storage'].'</div>
	<div id="storage" style="width:100%;" class="togglediv">
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_storage" '.POSTchecked('act_storage').' /> &nbsp; '.$l['storage'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_addstorage" '.POSTchecked('act_addstorage').' /> &nbsp; '.$l['addstorage'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_editstorage" '.POSTchecked('act_editstorage').' /> &nbsp; '.$l['editstorage'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_deletestorage" '.POSTchecked('act_deletestorage').' /> &nbsp; '.$l['deletestorage'].'</label></div>
		</div>
	</div>
</div>
<br/>

<div class="cat-wrapper">
	<div class="roundheader" style="cursor:pointer;" onclick="toggle_div(\'backups\');">'.$l['cat_backups'].'</div>
	<div id="backups" style="width:100%;" class="togglediv">
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_databackup" '.POSTchecked('act_databackup').' /> &nbsp; '.$l['backups'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_performdatabackup" '.POSTchecked('act_performdatabackup').' /> &nbsp; '.$l['perf_db_backups'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_dldatabackup" '.POSTchecked('act_dldatabackup').' /> &nbsp; '.$l['dl_db_bakcups'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_deletedatabackup" '.POSTchecked('act_deletedatabackup').' /> &nbsp; '.$l['del_db_backups'].'</label></div>
		</div>
		<div class="row">		
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_vpsbackupsettings" '.POSTchecked('act_vpsbackupsettings').' /> &nbsp; '.$l['edit_vps_backups'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_vpsbackups" '.POSTchecked('act_vpsbackups').' /> &nbsp; '.$l['vps_backups'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_restorevpsbackup" '.POSTchecked('act_restorevpsbackup').' /> &nbsp; '.$l['restore_vs_backups'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_deletevpsbackup" '.POSTchecked('act_deletevpsbackup').' /> &nbsp; '.$l['delete_vs_backups'].'</label></div>		
		</div>
		<div class="row">		
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_backupservers" '.POSTchecked('act_backupservers').' /> &nbsp; '.$l['list_backupservers'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_addbackupserver" '.POSTchecked('act_addbackupserver').' /> &nbsp; '.$l['add_backupserver'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_editbackupservsers" '.POSTchecked('act_editbackupservsers').' /> &nbsp; '.$l['edit_backupserver'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_deletebackupserver" '.POSTchecked('act_deletebackupserver').' /> &nbsp; '.$l['delete_backupserver'].'</label></div>
		</div>
		<div class="row">		
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_backup_plans" '.POSTchecked('act_backup_plans').' /> &nbsp; '.$l['list_backup_plans'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_addbackup_plan" '.POSTchecked('act_addbackup_plan').' /> &nbsp; '.$l['addbackup_plan'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_editbackup_plan" '.POSTchecked('act_editbackup_plan').' /> &nbsp; '.$l['editbackup_plan'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_deletebackup_plan" '.POSTchecked('act_deletebackup_plan').' /> &nbsp; '.$l['deletebackup_plan'].'</label></div>
		</div>
	</div>
</div>
<br/>

<div class="cat-wrapper">
	<div class="roundheader" style="cursor:pointer;" onclick="toggle_div(\'plans\');">'.$l['cat_plans'].'</div>
	<div id="plans" style="width:100%;" class="togglediv">
		<div class="row">		
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_plans" '.POSTchecked('act_plans').' /> &nbsp; '.$l['plans'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_addplan" '.POSTchecked('act_addplan').' /> &nbsp; '.$l['addplan'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_editplan" '.POSTchecked('act_editplan').' /> &nbsp; '.$l['editplan'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_deleteplan" '.POSTchecked('act_deleteplan').' /> &nbsp; '.$l['deleteplan'].'</label></div>		
		</div>
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_dnsplans" '.POSTchecked('act_dnsplans').' /> &nbsp; '.$l['dnsplans'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_adddnsplan" '.POSTchecked('act_adddnsplan').' /> &nbsp; '.$l['adddnsplan'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_editdnsplan" '.POSTchecked('act_editdnsplan').' /> &nbsp; '.$l['editdnsplan'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_deletednsplan" '.POSTchecked('act_deletednsplan').' /> &nbsp; '.$l['deletednsplan'].'</label></div>
		</div>
	</div>
</div>
<br/>

<div class="cat-wrapper">
	<div class="roundheader" style="cursor:pointer;" onclick="toggle_div(\'users\');">'.$l['cat_users'].'</div>
	<div id="users" style="width:100%;"  class="togglediv">
		<div class="row">		
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_users" '.POSTchecked('act_users').' /> &nbsp; '.$l['users'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_adduser" '.POSTchecked('act_adduser').' /> &nbsp; '.$l['adduser'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_edituser" '.POSTchecked('act_edituser').' /> &nbsp; '.$l['edituser'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_deleteuser" '.POSTchecked('act_deleteuser').' /> &nbsp; '.$l['deleteuser'].'</label></div>
		</div>
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_suspend_user" '.POSTchecked('act_suspend_user').' /> &nbsp; '.$l['suspend_user'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_unsuspend_user" '.POSTchecked('act_unsuspend_user').' /> &nbsp; '.$l['unsuspend_user'].'</label></div>
		</div>
	</div>
</div>
<br/>

<div class="cat-wrapper">
	<div class="roundheader" style="cursor:pointer;" onclick="toggle_div(\'media\');">'.$l['cat_media'].'</div>
	<div id="media" style="width:100%;" class="togglediv">
		<div class="row">		
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_ostemplates" '.POSTchecked('act_ostemplates').' /> &nbsp; '.$l['templates'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_os" '.POSTchecked('act_os').' /> &nbsp; '.$l['templatebrowser'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_addtemplate" '.POSTchecked('act_addtemplate').' /> &nbsp; '.$l['addtemplates'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_edittemplate" '.POSTchecked('act_edittemplate').' /> &nbsp; '.$l['edittemplates'].'</label></div>		
		</div>
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_deletetemplate" '.POSTchecked('act_deletetemplate').' /> &nbsp; '.$l['deltetemplates'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_createtemplate" '.POSTchecked('act_createtemplate').' /> &nbsp; '.$l['createtemplate'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_iso" '.POSTchecked('act_iso').' /> &nbsp; '.$l['isos'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_addiso" '.POSTchecked('act_addiso').' /> &nbsp; '.$l['addiso'].'</label></div>	
		</div>
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_editiso" '.POSTchecked('act_editiso').' /> &nbsp; '.$l['editisos'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_deleteiso" '.POSTchecked('act_deleteiso').' /> &nbsp; '.$l['deleteiso'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_mg" '.POSTchecked('act_mg').' /> &nbsp; '.$l['mgs'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_addmg" '.POSTchecked('act_addmg').' /> &nbsp; '.$l['addmgs'].'</label></div>		
		</div>
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_editmg" '.POSTchecked('act_editmg').' /> &nbsp; '.$l['editmg'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_deletemg" '.POSTchecked('act_deletemg').' /> &nbsp; '.$l['deletemgs'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_list_distros" '.POSTchecked('act_list_distros').' /> &nbsp; '.$l['list_distros'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_add_distro" '.POSTchecked('act_add_distro').' /> &nbsp; '.$l['add_distro'].'</label></div>
		</div>
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_euiso" '.POSTchecked('act_euiso').' /> &nbsp; '.$l['euiso'].'</label></div>
		</div>
	</div>
</div>
<br/>

<div class="cat-wrapper">
	<div class="roundheader" style="cursor:pointer;" onclick="toggle_div(\'recipe_div\');">'.$l['cat_recipe'].'</div>
	<div id="recipe_div" style="width:100%;" class="togglediv">
		<div class="row">		
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_recipes" '.POSTchecked('act_recipes').' /> &nbsp; '.$l['recipes'].'</label></div>		
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_addrecipe" '.POSTchecked('act_addrecipe').' /> &nbsp; '.$l['addrecipe'].'</label></div>		
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_editrecipe" '.POSTchecked('act_editrecipe').' /> &nbsp; '.$l['editrecipe'].'</label></div>
		</div>
	</div>
</div>
<br/>

<div class="cat-wrapper">
	<div class="roundheader" style="cursor:pointer;" onclick="toggle_div(\'config\');">'.$l['cat_config'].'</div>
	<div id="config" style="width:100%;"  class="togglediv">
		<div class="row">		
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_config" '.POSTchecked('act_config').' /> &nbsp; '.$l['edit_settings'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_emailsettings" '.POSTchecked('act_emailsettings').' /> &nbsp; '.$l['edit_emailsettings'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_adminacl" '.POSTchecked('act_adminacl').'  /> &nbsp; '.$l['admin_acl'].'</label></div>		
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_add_admin_acl" '.POSTchecked('act_add_admin_acl').'  /> &nbsp; '.$l['add_admin_acl'].'</label></div>		
		</div>
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_edit_admin_acl" '.POSTchecked('act_edit_admin_acl').'  /> &nbsp; '.$l['edit_admin_acl'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_delete_admin_acl" '.POSTchecked('act_delete_admin_acl').'  /> &nbsp; '.$l['delete_admin_acl'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_serverinfo" '.POSTchecked('act_serverinfo').' /> &nbsp; '.$l['serverinfo'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_licenseinfo" '.POSTchecked('act_licenseinfo').' /> &nbsp; '.$l['licenseinfo'].'</label></div>
		</div>
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_hostname" '.POSTchecked('act_hostname').' /> &nbsp; '.$l['hostname'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_changehostname" '.POSTchecked('act_changehostname').' /> &nbsp; '.$l['edithostname'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_maintenance" '.POSTchecked('act_maintenance').' /> &nbsp; '.$l['maintenance'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_kernconfig" '.POSTchecked('act_kernconfig').' /> &nbsp; '.$l['kern_config'].'</label></div>
		</div>
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_updates" '.POSTchecked('act_updates').' /> &nbsp; '.$l['updates'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_emailtemps" '.POSTchecked('act_emailtemps').' /> &nbsp; '.$l['email_temps'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_editemailtemps" '.POSTchecked('act_editemailtemps').' /> &nbsp; '.$l['edit_email_temps'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_ssl" '.POSTchecked('act_ssl').' /> &nbsp; '.$l['ssl'].'</label></div>
		</div>
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_editssl" '.POSTchecked('act_editssl').' /> &nbsp; '.$l['editssl'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_createssl" '.POSTchecked('act_createssl').' /> &nbsp; '.$l['createssl'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_firewall" '.POSTchecked('act_firewall').' /> &nbsp; '.$l['firewall'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_importvs" '.POSTchecked('act_importvs').' /> &nbsp; '.$l['importvs'].'</label></div>
		</div>
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_phpmyadmin" '.POSTchecked('act_phpmyadmin').' /> &nbsp; '.$l['pma'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_ssh" '.POSTchecked('act_ssh').' /> &nbsp; '.$l['ssh'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_defaultvsconf" '.POSTchecked('act_defaultvsconf').' /> &nbsp; '.$l['dif_vps_config'].'</label></div>
		</div>
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_twofactauth" '.POSTchecked('act_twofactauth').' /> &nbsp; '.$l['twofactauth'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_orphaneddisk" '.POSTchecked('act_orphaneddisk').' /> &nbsp; '.$l['orphaneddisk'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_deleteorphaneddisk" '.POSTchecked('act_deleteorphaneddisk').' /> &nbsp; '.$l['deleteorphaneddisk'].'</label></div>
		</div>
	</div>
</div>
<br/>

<div class="cat-wrapper">
	<div class="roundheader" style="cursor:pointer;" onclick="toggle_div(\'pdns\');">'.$l['cat_pdns'].'</div>
	<div id="pdns" style="width:100%;" class="togglediv">
		<div class="row">		
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_pdns" '.POSTchecked('act_pdns').' /> &nbsp; '.$l['pdns'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_managepdns" '.POSTchecked('act_managepdns').' /> &nbsp; '.$l['manage_pdns'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_addpdns" '.POSTchecked('act_addpdns').' /> &nbsp; '.$l['add_pdns'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_editpdns" '.POSTchecked('act_editpdns').' /> &nbsp; '.$l['edit_pdns'].'</label></div>		
		</div>
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_deletepdns" '.POSTchecked('act_deletepdns').' /> &nbsp; '.$l['delete_pdns'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_rdns" '.POSTchecked('act_rdns').' /> &nbsp; '.$l['rdns'].'</label></div>
		</div>
	</div>
</div>
<br/>

<div class="cat-wrapper">
	<div class="roundheader" style="cursor:pointer;" onclick="toggle_div(\'procs\');">'.$l['cat_procs'].'</div>
	<div id="procs" style="width:100%;" class="togglediv">
		<div class="row">		
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_procs" '.POSTchecked('act_procs').' /> &nbsp; '.$l['manage_procs'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_services" '.POSTchecked('act_services').' /> &nbsp; '.$l['manage_services'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_webserver" '.POSTchecked('act_webserver').' /> &nbsp; '.$l['restart_web'].'</label></div>	
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_network" '.POSTchecked('act_network').' /> &nbsp; '.$l['restart_net'].'</label></div>
		</div>
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_sendmail" '.POSTchecked('act_sendmail').' /> &nbsp; '.$l['restart_mail'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_mysqld" '.POSTchecked('act_mysqld').'  /> &nbsp; '.$l['restart_mysql'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_iptables" '.POSTchecked('act_iptables').'  /> &nbsp; '.$l['restart_iptables'].'</label></div>
		</div>
	</div>
</div>
<br/>

<div class="cat-wrapper">
	<div class="roundheader" style="cursor:pointer;" onclick="toggle_div(\'logs\');">'.$l['cat_logs'].'</div>
	<div id="logs" style="width:100%;" class="togglediv"> 
		<div class="row">		
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_logs" '.POSTchecked('act_logs').'  /> &nbsp; '.$l['logs'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_deletelogs" '.POSTchecked('act_deletelogs').' /> &nbsp; '.$l['deletelogs'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_userlogs" '.POSTchecked('act_userlogs').'  /> &nbsp; '.$l['userlog'].'</label></div>	
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_deleteuserlogs" '.POSTchecked('act_deleteuserlogs').' /> &nbsp; '.$l['deleteuserlogs'].'</label></div>			
		</div>
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_loginlogs" '.POSTchecked('act_loginlogs').' /> &nbsp; '.$l['loginlogs'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_deleteloginlogs" '.POSTchecked('act_deleteloginlogs').' /> &nbsp; '.$l['delteloginlogs'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_iplogs" '.POSTchecked('act_iplogs').'  /> &nbsp; '.$l['iplogs'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_deliplogs" '.POSTchecked('act_deliplogs').'  /> &nbsp; '.$l['deliplogs'].'</label></div>
		</div>
	</div>
</div>
<br/>

<div class="cat-wrapper">
	<div class="roundheader" style="cursor:pointer;" onclick="toggle_div(\'logs\');">'.$l['cat_haproxy'].'</div>
	<div id="logs" style="width:100%;" class="togglediv"> 
		<div class="row">		
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_haproxy" '.POSTchecked('act_haproxy').'  />&nbsp;'.$l['haproxy'].'</label></div>
		</div>
	</div>
</div>

<br /><br /><br />

<center><input type="submit" name="add_admin_acl" value="'.$l['save'].'" class="btn"></center>
</form>
<br />

</div>';
}

softfooter();
}
?>