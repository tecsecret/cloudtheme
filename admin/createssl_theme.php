<?php

//////////////////////////////////////////////////////////////
//===========================================================
// createssl_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function createssl_theme(){

global $theme, $globals, $kernel, $user, $l, $info, $error, $done;

softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-ssl icon-head"></i>&nbsp; '.$l['<title>'].'</center>';

error_handle($error);

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
}


echo '<div id="form-container">
<form accept-charset="'.$globals['charset'].'" name="createssl" method="post" action="" class="form-horizontal">

<div class="row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['country'].'</label><br />
		<span class="help-block">'.$l['country_desc'].'</span></div>
	<div class="col-sm-8">
		<input type="text" class="form-control" name="country" id="country" size="30" value="'.POSTval('country', '').'" />
	</div>
</div>
<div class="row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['state'].'</label><br />
		<span class="help-block">'.$l['state_desc'].'</span></div>
	<div class="col-sm-8">
		<input type="text" class="form-control" name="state" id="state" size="30" value="'.POSTval('state', '').'" />
	</div>
</div>

<div class="row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['locality'].'</label><br />
		<span class="help-block">'.$l['locality_desc'].'</span></div>
	<div class="col-sm-8">
		<input type="text" class="form-control" name="locality" id="locality" size="30" value="'.POSTval('locality', '').'" />
	</div>
</div>
<div class="row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['organisation'].'</label><br />
		<span class="help-block">'.$l['organisation_desc'].'</span></div>
	<div class="col-sm-8">
		<input type="text" class="form-control" name="organisation" id="organisation" size="30" value="'.POSTval('organisation', '').'" />
	</div>
</div>
<div class="row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['org_unit'].'</label><br />
		<span class="help-block">'.$l['org_unit_desc'].'</span></div>
	<div class="col-sm-8">
		<input type="text" class="form-control" name="orgunit" id="orgunit" size="30" value="'.POSTval('orgunit', '').'" />
	</div>
</div>
<div class="row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['comname'].'</label><br />
		<span class="help-block">'.$l['comname_desc'].'</span></div>
	<div class="col-sm-8">
		<input type="text" class="form-control" name="comname" id="comname" size="30" value="'.POSTval('comname', '').'" />
	</div>
</div>
<div class="row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['email'].'</label><br />
		<span class="help-block">'.$l['email_desc'].'</span></div>
	<div class="col-sm-8">
		<input type="text" class="form-control" name="email" id="email" size="30" value="'.POSTval('email', '').'" />
	</div>
</div>
<div class="row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['key_size'].'</label></div>
	<div class="col-sm-8">
		<select class="form-control" name="keysize" id="keysize">
			<option value="1024">1024</option>
			<option value="2048">2048</option>
		</select>
	</div>
</div>
</div>
<br /><br />
<center><input type="submit" name="create" value="'.$l['submit'].'" class="btn"></center>

<br /><br />
</form>
<div class="notebox">
<font class="bboxtxt"><b>'.$l['note'].'</b> : <br />'.$l['ssl_note'].'</font>
</div>
</div>
';
softfooter();

}

?>