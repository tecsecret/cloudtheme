<?php

//////////////////////////////////////////////////////////////
//===========================================================
// addserver_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function editstorage_theme(){

global $theme, $globals, $kernel, $user, $l, $cluster, $error, $servers, $servergroups, $valid_storages, $stid, $storage, $storage_servers, $done;

// Check the stid belongs to which servers
foreach($storage_servers as $p => $q){
	if($q['serid'] == -1){
		$ipgp[$q['stid']][] = $l['all_servers'];
	}elseif($q['serid'] == -2){
		$ipgp[$q['stid']][] = '[Group] '.$servergroups[$q['sgid']]['sg_name'];
	}else{
		foreach($servers as $x => $y){
			if($q['serid'] == $y['serid'] || $q['sgid'] == $y['sgid']){
				$ipgp[$q['stid']][] = $y['server_name'];
			}
		}
	}
}

softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-storage icon-head"></i>&nbsp; '.$l['edit_storage'].'</center>';

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['edited'].'</div><br />';
}

error_handle($error);

echo '<div id="form-container">

<form accept-charset="'.$globals['charset'].'" name="editstorage" method="post" action="" class="form-horizontal">

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['st_name'].'</label>
		<span class="help-block">'.$l['st_name_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="name" id="st_name" size="30" value="'.POSTval('name', $storage['name']).'" />
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['st_server'].'</label>
		<span class="help-block">'.$l['st_exp_server'].'</span>
	</div>
	<div class="col-sm-6">
		';
	
		$POSTserid = @$_POST['serid'];
		$POSTserid = empty($POSTserid) ? array() : $POSTserid;
		
		echo '<select class="form-control" name="serid[]" id="serid" size="8" multiple="multiple" disabled="disabled">
			<option value="-1" '.(in_array(-1, $POSTserid) || (in_array('All Servers', $ipgp[$stid], true)) ? 'selected="selected"' : '').'>'.$l['all_servers'].'</option>';
			
			foreach ($servergroups as $k => $v){
				echo '<option class="fhead" value="'.$k.'_group" '.(in_array($k.'_group', $POSTserid) || (in_array('[Group] '.$v['sg_name'], $ipgp[$stid], true)) ? 'selected="selected"' : '').'>[Group]&nbsp;'.$v['sg_name'].'</option>';
				
				foreach ($servers as $m => $n){
					if($n['sgid'] == $k){
						echo '<option value="'.$n['serid'].'" '.(in_array((string)$n['serid'], $POSTserid) || (in_array((string)$n['server_name'], $ipgp[$stid], true)) ? 'selected="selected"' : '').'>&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;'.$n['server_name'].'</option>';
					}
				}
			}
		echo '</select>
		<span class="help-block"></span>
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['st_path'].'</label>
		<span class="help-block">'.$l['st_path_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<span class="val">'.$storage['path'].'</span>
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['st_type'].'</label>
		<span class="help-block">'.$l['st_type_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<span class="val">'.storage_type_text($storage['type']).'</span>
	</div>
</div>';

// If format is there then only we have to show this row.
if(!empty($storage['format'])){
echo '<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['st_format'].'</label>
		<span class="help-block">'.$l['st_format_exp'].'</span>
	</div>
	<div class="col-sm-6">
		'.strtoupper($storage['format']).'
	</div>
</div>';
}

echo '<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['st_oversell'].'</label>
		<span class="help-block">'.$l['st_oversell_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="oversell" size="30" value="'.POSTval('oversell', $storage['oversell']).'" style="width:15%;"/>
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['st_alert_threshold'].'</label>
		<span class="help-block">'.$l['st_alert_threshold_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="alert_threshold" size="10" value="'.POSTval('alert_threshold', $storage['alert_threshold']).'" style="float:left; width:15%" />
		<label style="padding:8px 0px 0px 8px;">%</label>
	</div>
</div>

<div class="row">
	<div class="col-sm-6 col-xs-10">
		<label class="control-label">'.$l['st_primary_storage'].'</label>
		<span class="help-block">'.$l['st_primary_storage_exp'].'</span>
	</div>
	<div class="col-sm-6 col-xs-2">
		<input type="checkbox" class="ios" id="primary_storage" name="primary_storage" '.POSTchecked('primary_storage', $storage['primary_storage']).' />
	</div>
</div>

</div>

<br /><br />
<center><input type="submit" name="editstorage" class="btn" value="'.$l['sub_but'].'" /></center>

</form>
</div>
';

softfooter();

}

?>