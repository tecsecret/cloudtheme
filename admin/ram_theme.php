<?php

//////////////////////////////////////////////////////////////
//===========================================================
// ram_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function ram_theme(){

global $theme, $globals, $cluster, $servers, $user, $l, $ram;

if(optGET('ajax')){	

	echo 'var ram = [
		{ label: "Used",  data: '.$ram['used'].'},
		{ label: "Free",  data: '.$ram['free'].'}
	];
	
	var swapram = [
		{ label: "Used",  data: '.$ram['swap_used'].'},
		{ label: "Free",  data: '.$ram['swap_free'].'}
	];';
		
			return true;
}

softheader($l['<title>']);

echo '
<div class="bg" style="width:99%">
<center class="tit"><i class="icon icon-ram icon-head"></i>&nbsp; '.$l['header'].'<span style="float:right;" ><a href="'.$globals['docs'].'Server_RAM_Usage" target="_blank" class="wiki_help" title="'.$l['wiki_help'].'"><i class="icon-help" ></i></a></span></center><br />';

// Is it offline ?
$hypervisor_status = $cluster->statewise($globals['server']);
if($hypervisor_status == 0 || $hypervisor_status == 2){

	echo '<div class="e_notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['server_status_'.$hypervisor_status].'</div>';
	
}else{

echo '

<script language="javascript" type="text/javascript"><!-- // --><![CDATA[

// Draw a Resource Graph
function resource_graph(id, data){

    $.plot($("#"+id), data, 
	{
		series: {
			pie: { 
				innerRadius: 0.7,
				radius: 1,
				show: true,
				label: {
					show: true,
					radius: 0,
					formatter: function(label, series){
						if(label != "Used") return "";
						return \'<div style="font-size:18px;text-align:center;padding:2px;color:black;">\'+Math.round(series.percent)+\'%</div><div style="font-size:10px;">\'+label+\'</div>\';	
					}
				}
			}
		},
		legend: {
			show: false
		}
	});
}
																			   
function getusage(){
	if(AJAX("'.$globals['index'].'act=ram&ajax=true", "drawpie(re)")){
		return false;
	}else{
		return true;	
	}
};

function startusage(){
	ajaxtimer = setInterval("getusage()", 5000);
};	

function drawpie(re){
	
	var ram = [
		{ label: "Used",  data: '.$ram['used'].'},
		{ label: "Free",  data: '.$ram['free'].'}
	];
	
	var swapram = [
		{ label: "Used",  data: '.$ram['swap_used'].'},
		{ label: "Free",  data: '.$ram['swap_free'].'}
	];
	
	if(re.length > 0){
		try{
			eval(re);
		}catch(e){ }
	}
	
	var total = (ram[0]["data"] + ram[1]["data"]);
	total = (total ? total : 1);
	$_("ramused").innerHTML = ram[0]["data"]+" MB";
	$_("raminpercent").innerHTML = (ram[0]["data"]/total*100).toFixed(2)+" %";
	$_("swap_used").innerHTML = swapram[0]["data"]+" MB";
	$_("swap_free").innerHTML = swapram[1]["data"]+" MB";
	
	resource_graph("ram_holder", ram);
	resource_graph("swap_holder", swapram);

};

addonload("drawpie(\'void(0);\');startusage();");
// ]]></script>';

echo '
<div class="row">
	<div class="col-sm-6" style="border-right:1px solid #CCCCCC">
		<div class="roundheader">'.$l['raminfo'].'</div>
		<table align="center" class="table table-hover" cellpadding="3" cellspacing="4" border="0" width="100%">
			<tr><td align="left" class="blue_td" width="30%">'.$l['totalram'].'</td><td class="fhead val">'.$ram['limit'].' MB</td></tr>
			<tr><td align="left" class="blue_td">'.$l['utilised'].'</td><td class="val" id="ramused">'.$ram['used'].' MB</td></tr>
			<tr><td align="left" class="blue_td">'.$l['percentram'].'</td><td class="val" id="raminpercent">'.$ram['percent'].' %</td></tr>
		</table>
	</div>
	<div class="col-sm-6" align="center" width="50%">
		<div class="roundheader">'.$l['graphheader'].'</div>
		<div id="ram_holder" style="width:170px; height:170px; margin:5px;"></div>
	</div>
</div>

<center class="tit"><i class="icon icon-ram icon-head"></i>&nbsp; '.$l['swap_header'].'</center>
<div class="row">
	<div class="col-sm-6" style="border-right:1px solid #CCCCCC">
		<div class="roundheader">'.$l['swapinfo'].'</div>
		<table align="center" class="table table-hover" cellpadding="3" cellspacing="4" border="0" width="100%">
			<tr><td align="left" class="blue_td" width="30%">'.$l['swap'].'</td><td class="fhead val">'.$ram['swap'].' MB</td></tr>
			<tr><td align="left" class="blue_td">'.$l['utilised'].'</td><td class="val" id="swap_used">'.$ram['swap_used'].' MB</td></tr>
			<tr><td align="left" class="blue_td">'.$l['swap_free'].'</td><td class="val" id="swap_free">'.$ram['swap_free'].' MB</td></tr>
		</table>
	</div>
	<div class="col-sm-6" align="center" width="50%">
		<div class="roundheader">'.$l['swap_graph'].'</div>
		<div id="swap_holder" style="width:170px; height:170px; margin:5px;"></div>
	</div>
</div>
<br />';
}

echo '</div>';
softfooter();

}

?>