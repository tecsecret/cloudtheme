<?php

//////////////////////////////////////////////////////////////
//===========================================================
// iplogs_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function iplogs_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $current_status, $iplogs;

softheader($l['<title>']);

echo '
<div class="bg" style="width: 99%">
<center class="tit"><i class="icon icon-logs icon-head"></i> &nbsp; '.$l['heading'].'<span style="float:right"><a href="javascript:showsearch();"><img src="'.$theme['images'].'admin/search.gif" /></a></span></center>

<div id="showsearch" style="display:'.(optREQ('search') || (!empty($iplogs) && !empty($globals['showsearch'])) ? "" : "none").';">
<form accept-charset="'.$globals['charset'].'" name="iplogs" method="get" action="" class="form-horizontal">
<input type="hidden" name="act" value="iplogs">
		
<div class="form-group_head">
  <div class="row">
    <div class="col-sm-2"></div>
    <div class="col-sm-1"><label>'.$l['ip'].'</label></div>
    <div class="col-sm-2"><input type="text" class="form-control" name="ip" id="ip" size="30" value="'.REQval('ip','').'"/></div>
    <div class="col-sm-1"><label>'.$l['vpsid'].'</label></div>
    <div class="col-sm-2"><input type="text" class="form-control" name="vpsid" id="vpsid" size="30" value="'.REQval('vpsid','').'"/></div>
    <div class="col-sm-2" style="text-align: center;"><button type="submit" name="search" class="go_btn" value="Search"/>'.$l['submit'].'</button></div>
    <div class="col-sm-2"></div>
  </div>
</div>
</form>
<br />
<br />
</div>';

if(count($current_status) == 1){
	
	$current = current($current_status);
	
	$str = (empty($current['vpsid']) ? $l['ip_free'] : $l['ip_used']);
	
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.lang_vars_name($str, array('vpsid' => $current['vpsid'], 
																												'ip' => $current['ip'])
																											).'</div>';
}

if(empty($iplogs)){

	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.(optREQ('search') ? $l['no_res'] : $l['no_iplog']).'</div>';
	
}else{

	page_links($globals['num_res'], $globals['cur_page'], $globals['reslen']);
	
	echo '<br /><br />
	<table class="table table-hover tablesorter">
	<tr>
		<th align="center">'.$l['iplid'].'</th>
		<th align="center">'.$l['ipid'].'</th>
		<th align="center">'.$l['ip'].'</th>
		<th align="center">'.$l['vpsid'].'</th>
		<th align="center">'.$l['uid'].'</th>
		<th align="center">'.$l['email'].'</th>
		<th align="center">'.$l['cloud_uid'].'</th>
		<th align="center">'.$l['cloud_email'].'</th>
		<th align="center">'.$l['time'].'</th>
		<th align="center">'.$l['date'].'</th>
	</tr>';
	$i = 0;
	foreach($iplogs as $k => $v){
		
	echo '<tr>
			<td align="left">'.$v['iplid'].'</td>
			<td align="left">'.$v['ipid'].'</td>
			<td>'.$v['ip'].'</td>
			<td>'.$v['vpsid'].'</td>
			<td>'.$v['uid'].'</td>
			<td>'.$v['email'].'</td>
			<td>'.$v['cloud_uid'].'</td>
			<td>'.$v['cloud_email'].'</td>
			<td>'.datify($v['time'], 0, 1, "h:i:s A").'</td>
			<td>'.datify($v['date'], 0, 1, "Y-m-d").'</td>
		<tr>';
		$i++;
	}
	
	echo '</table><br /><br /><br />

	<form accept-charset="'.$globals['charset'].'" name="iplogs" method="post" action="" class="form-horizontal">
	<center><input type="submit" name="delete" value="'.$l['delete'].'" class="btn"></center>
	</form>
	<br /><br />';
}
page_links($globals['num_res'], $globals['cur_page'], $globals['reslen']);
echo '</div>';
softfooter();

}

?>