<?php

//////////////////////////////////////////////////////////////
//===========================================================
// servers_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function servers_theme(){

global $theme, $globals, $kernel, $user, $l, $cluster, $servers, $servs, $error, $done, $force_delete, $servergroups, $distros, $remove_ha, $delete;

softheader($l['<title>']);

echo '
<div class="bg" style="width: 99%">
<center class="tit">
<i class="icon icon-servers icon-head"></i>&nbsp; '.$l['page_head'].'<span style="float:right"><a href="javascript:showsearch();"><img src="'.$theme['images'].'admin/search.gif" /></a></span></center>';

error_handle($error);


if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
}

if(!empty($force_delete) && empty($done)){

	echo '<div class="notice"><a href="'.$globals['ind'].'act=servers&force='.$force_delete.'&remove_ha='.(!empty($remove_ha) ? '1' : 0).'"><span style="float:center">'.$l['force_unslave'].'</span></a><br> <span style="float:center">'.$l['ha_force_unslave_warn'].'</span></div>';
	return;
}

if(!empty($remove_ha) && empty($done)){
	$link = $globals['ind'].'act=servers&delete='.$delete.'&remove_ha=';
	echo '<div class="notice"><span style="float:center">'.$l['ha_remove_warning'].'</span><br><br><center><input type="button" value="'.$l['yes'].'" class="link_btn" onclick="window.location =\''.$link.'1\';">&nbsp;&nbsp;<input type="button" value="'.$l['no'].'" class="link_btn" onclick="window.location =\''.$link.'0\';"></center></div>';
	return;
}

echo '<script language="javascript" type="text/javascript"><!-- // --><![CDATA[

function mouse_over(el){
	el.className = "server_over";
	el.onmouseout = function(){el.className="";}
};

function conf_del(){
	return confirm("'.$l['conf_del'].'");
};

function lockServer(serid){

	serid = serid || 0;
	
	// List of server ids to lock
	var server_list = new Array();
	
		
	if($("#server_task_select").val() == 0){
		alert("'.$l['no_action'].'");
		return false;
	}
	
	$(".ios:checked").each(function() {
		server_list.push($(this).val());
	});
	
	if(server_list.length < 1){
		alert("'.$l['nothing_selected'].'");
		return false;
	}
	
	var action = $("#server_task_select").val();
	var l = Array();
	l["lock_lang"] = "'.$l['lock_conf'].'";
	l["unlock_lang"] = "'.$l['unlock_conf'].'";
	
	var server_conf = confirm(l[action+"_lang"]);
	if(server_conf == false){
		return false;
	}
	
	var finalData = new Object();
	
	finalData[action] = server_list.join(",");

	//alert(JSON.stringify(finalData));
	//return false;
	
	$("#progress_bar").show();
	
	$.ajax({
		type: "POST",
		url: "'.$globals['index'].'act=servers&api=json",
		data : finalData,
		dataType : "json",
		success: function(data){
			$("#progress_bar").hide();
			if("done" in data){
				alert("'.$l['action_completed'].'");
				location.reload(true);
			}
		},
		error: function(data) {
			$("#progress_bar").hide();
			//alert(data.description);
			return false;
		}
	});
	
	return false;
};

// ]]></script>

<div id="showsearch" style="display:'.(optREQ('search') || (!empty($servs) && !empty($globals['showsearch'])) ? "" : "none").';">
<form accept-charset="'.$globals['charset'].'" name="servers" method="GET" action="" class="form-horizontal">
<input type="hidden" name="act" value="servers">
		
<div class="form-group_head">
	<div class="row">
		<div class="col-sm-1"><label>'.$l['sbyservername'].'</label></div>
		<div class="col-sm-2"><input type="text" class="form-control" name="servername" id="servername" size="30" value="'.REQval('servername', '').'" /></div>
		<div class="col-sm-1"><label>'.$l['sbyserverip'].'</label></div>
		<div class="col-sm-2"><input type="text" class="form-control" name="serverip" id="serverip" size="30" value="'.REQval('serverip','').'"/></div>
		<div class="col-sm-1"><label>'.$l['sbyservertype'].'</label></div>
			<div class="col-sm-2">
				<select name="ptype"  style="width:99%" class="form-control">
					<option value="">All</option>
					<option value="openvz" '.(optREQ('ptype') == 'openvz' ? 'selected=selected' : '').'>Openvz</option>
					<option value="xen" '.(optREQ('ptype') == 'xen' ? 'selected=selected' : '').'>Xen</option>
					<option value="kvm" '.(optREQ('ptype') == 'kvm' ? 'selected=selected' : '').'>KVM</option>
					<option value="xcp" '.(optREQ('ptype') == 'xcp' ? 'selected=selected' : '').'>XCP</option>
					<option value="vzo" '.(optREQ('ptype') == 'vzo' ? 'selected=selected' : '').'>Virtuozzo Openvz</option>
					<option value="vzk" '.(optREQ('ptype') == 'vzk' ? 'selected=selected' : '').'>Virtuozzo KVM</option>
					<option value="proxk" '.(optREQ('ptype') == 'proxk' ? 'selected=selected' : '').'>Proxmox KVM</option>
					<option value="proxl" '.(optREQ('ptype') == 'proxl' ? 'selected=selected' : '').'>Proxmox LXC</option>
					<option value="proxo" '.(optREQ('ptype') == 'proxo' ? 'selected=selected' : '').'>Proxmox OpenVZ</option>
					<!--<option value="vmware" '.(optREQ('ptype') == 'vmware' ? 'selected=selected' : '').'>VMWARE ESXi</option>-->
				</select>
			</div>
		<div class="col-sm-3" style="text-align: center;"><button type="submit" name="search" class="go_btn" value="Search"/>'.$l['submit'].'</button></div>
	</div>
</div>
</form>
<br />
<br />
</div>';

if(!empty($saved)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div><br />';
}

if(empty($servs)){

	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['no_res'].'</div>';
	
}else{
	page_links($globals['num_res'], $globals['cur_page'], $globals['reslen']);
echo '<br /><br />
<form accept-charset="'.$globals['charset'].'" name="multi_server" id="multi_server" method="post" action="" class="form-horizontal">
<table class="table table-hover tablesorter">
	<tr>
		<th height="30">'.$l['server_id'].'</th>
		<th>'.$l['server_type'].'</th>
		<th>'.$l['os'].'</th>
		<th>'.$l['server_name'].'</th>
		<th>'.$l['server_ip'].'</th>
		<th>'.$l['server_ram'].'</th>
		<th>'.$l['server_vps'].'</th>
		<th>'.$l['expires_on'].'</th>
		<th>'.$l['version'].'</th>
		<th>'.$l['server_group'].'</th>
		<th colspan="3">'.$l['manage'].'</th>
		<th><input type="checkbox" class="select_all" name="select_all" id="select_all"></th>		
	</tr>';		
			
	$i = 1;

	foreach($servs as $k => $v){
		
		if(preg_match('/CentOS/is', $v['os'])){
			$servers_ditro = 'centos';
		}elseif(preg_match('/ubuntu/is', $v['os'])){
			$servers_ditro = 'ubuntu';
		}elseif(preg_match('/vmware/is', $v['os'])){
			$servers_ditro = 'vmware';
		}else{
			$servers_ditro = 'others';
		}
		
		echo '<tr>
		<td width="30" align="left">'.$v['serid'].'</td>
		<td align="center"><img src="'.$theme['images'].'admin/'.$v['virt'].'_server.gif" width="40" /></td>
		<td align="center"><img src="'.distro_logo($servers_ditro).'" title="'.$v['os'].'"/></td>
		<td>'.$v['server_name'];
		if(!empty($servs[$k]['locked'])){
			echo ' &nbsp;<img style="vertical-align:top" src="'.$theme['images'].'admin/lock.png" title="'.$l['server_locked'].'"/>';
		}
		echo '</td> 
		<td>'.$v['ip'].'</td>
		<td>'.(!empty($v['alloc_ram']) ? $v['alloc_ram'] : '0').' MB / '.$v['total_ram'].' MB</td>
		<td>'.(!empty($v['numvps']) ? $v['numvps'] : '0').'</td>
		<td align="center">'.$v['lic_expires'].'</td>
		<td align="center">'.$v['version'].' ('.$v['patch'].')</td>
		<td align="center">'.$servergroups[$v['sgid']]['sg_name'].'</td>
		<td width="30" style="padding:7px 7px 0px 0px" align="center" class="manage-ico"><a href="'.$globals['ind'].'act=manageserver&changeserid='.$k.'"><img title="'.$l['manage'].'" src="'.$theme['images'].'admin/manage.png" width="20" /></a></td>
		<td width="30" style="padding:7px 7px 0px 0px" align="center" class="manage-ico"><a href="'.$globals['ind'].'act=editserver&serid='.$k.'"><img title="'.$l['edit'].'" src="'.$theme['images'].'admin/edit.png" /></a></td>
		<td width="30" style="padding:7px 7px 0px 0px" align="center">';
		
		if(!empty($k)){
			echo '<a href="'.$globals['ind'].'act=servers&delete='.$k.'" onclick="return conf_del();"><img title="'.$l['delete'].'" src="'.$theme['images'].'admin/delete.png" /></a>';			
		}else{
			echo '&nbsp;';
		}
		echo '</td>
			<td width="20" valign="middle" align="center">
				<input type="checkbox" class="ios" name="servers_list[]" value="'.$v['serid'].'"/>
			</td>
		</tr>';
		$i++;
	}
	
	echo '</table>
<div class="row bottom-menu">
	<div class="col-sm-7"></div>
	<div class="col-sm-5"><label>'.$l['with_selected'].'</label>
		<select name="server_task_select" id="server_task_select" class="form-control">
			<option value="0">---</option>
			<option value="lock">'.$l['ms_lock'].'</option>
			<option value="unlock">'.$l['ms_unlock'].'</option>
		</select>&nbsp;
		<input type="submit" id ="server_submit" class="go_btn" name="server_submit" value="Go" onclick="lockServer(); return false;">
	</div>
</div>
</form>

<div id="progress_bar" style="height:125px; display:none">
	<br />
	<center>
		<font id="progress_txt" size="4" color="#222222">'.$l['action_msg'].'</font>
		<br>
		<br>
	</center>
	<table id="table_progress" width="500" height="28" cellspacing="0" cellpadding="0" border="0" align="center" style="border:1px solid #CCC; -moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px;background-color:#efefef;">
		<tbody>
			<tr>
				<td id="progress_color" width="100%" style="background-image: url(themes/default/images/bar.gif); -moz-border-radius: 4px; -webkit-border-radius: 4px; border-radius: 4px;"></td>
				<td id="progress_nocolor"> </td>
			</tr>
		</tbody>
	</table>
	<br>
	<center>
		'.$l['notify_msg'].'
	</center>
</div>';

}

page_links($globals['num_res'], $globals['cur_page'], $globals['reslen']);

echo '
<br /><br />
<center><input type="button" value="'.$l['add_server'].'" class="link_btn" onclick="window.location =\''.$globals['ind'].'act=addserver\';"></center></div>
</div>';

softfooter();

}

?>