<?php

//////////////////////////////////////////////////////////////
//===========================================================
// addippool_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function addippool_theme(){

global $theme, $globals, $kernel, $user, $l, $cluster, $error, $servers, $done, $ips, $virt, $servergroups;

softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-ippool icon-head"></i> '.$l['addippool'].'<span style="float:right;" ><a href="'.$globals['docs'].'Add_IP_Pool" target="_blank" class="wiki_help" title="'.$l['wiki_help'].'"><i class="icon-help" ></i></a></span></center>';

error_handle($error);

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';

	if(!empty($done['ipv6_subnet'])){
		echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.lang_vars_name($l['done_ipv6_subnet'], array('ippid' => $done['ipv6_subnet'])).'</div>';
	}
	
}else{

echo '<script language="javascript" type="text/javascript">

function addrow(id){
	var t = $_(id);

	var row = document.createElement("div");
	row.className = "row";
	var col1 = document.createElement("div");
	col1.className = "col-sm-6 ip-mac";
	var col2 = document.createElement("div");
	col2.className = "col-sm-6 ip-mac";

	col1.innerHTML = \'<label>'.$l['ip'].' :&nbsp;&nbsp;</label><input type="text" class="form-control" name="ips[]"/>\';
	col2.innerHTML = \'<label>'.$l['mac_addr'].' :&nbsp;&nbsp;</label><input type="text" class="form-control" name="macs[]"/>\';

	row.appendChild(col1);
	row.appendChild(col2);
	t.appendChild(row);
};

function ipv6_subnet_gen(){
	if(is_checked("ipv6_subnet")){
		$("#ipv6_range").hide();
		$("#ipv6_num").hide();
	}else{
		$("#ipv6_range").show();
		$("#ipv6_num").show();
	}
}
	
function changetype(iptype){
	$_("ipv4").className = "col-sm-4";
	$_("ipv6").className = "col-sm-4";
	
	if (iptype == 4){
		$_("netmask_6").style.display="none";
		$_("6_slash").style.display="none";
		$_("4_ns1").style.display="";
		$_("4_ns2").style.display="";
		$_("ns_1").value="";
		$_("ns_2").value="";
		$_("gateway").value="";
		$_("6_ns1").style.display="none";
		$_("6_ns2").style.display="none";
		$_("firstip").style.display="";
		$_("lastip").style.display="";
		$_("ips").style.display="";
		$_("or").style.display="";
		$_("ipv6_subnet_row").style.display="none";
		$_("ipv6_range").style.display="none";
		$_("ipv6_num").style.display="none";
		$_("ipv4_ns").style.display="";
		$_("ipv6_ns").style.display="none";
		$_("internal_tr").style.display="";
		if($("#internal").attr("checked")){
			$_("internal_bridge_tr").style.display="";
		}
		$_("vlan_tr").style.display="";
		if($("#vlan").attr("checked")){
			$_("vlan_bridge_tr").style.display="";
		}
		$_("netmask").style.width="100%";
		$("#4_space").addClass("help-block");
	}else{		
		$_("netmask_6").style.display="";
		$_("6_slash").style.display="";
		$_("4_ns1").style.display="none";
		$_("4_ns2").style.display="none";
		$_("6_ns1").style.display="";
		$_("6_ns2").style.display="";
		$_("ns_1").value="";
		$_("ns_2").value="";
		$_("gateway").value="";
		$_("firstip").style.display="none";
		$_("lastip").style.display="none";
		$_("ips").style.display="none";
		$_("or").style.display="none";
		$_("ipv6_subnet_row").style.display="";
		$_("ipv6_range").style.display="";
		$_("ipv6_num").style.display="";
		$_("ipv4_ns").style.display="none";
		$_("ipv6_ns").style.display="";
		$_("internal_tr").style.display="none";
		$_("internal_bridge_tr").style.display="none";
		$_("vlan_tr").style.display="";
		if($("#vlan").attr("checked")){
			$_("vlan_bridge_tr").style.display="";
		}
		$("#nat_tr").hide();
		$_("netmask").style.width="90%";
		ipv6_subnet_gen();
		$("#4_space").removeClass("help-block");
	}		
	$_("ipv"+iptype).className = "active col-sm-4";
	$_("iptype").value = iptype;
	
};

function changeDNS(ips){
	ip = ips.split(\',\');
	$("input[type=text][name=ns1]").val(ip[0]);
	$("input[type=text][name=ns2]").val(ip[1]);
}

function handletr(){
	
	var serid = $("#serid").val();
	if(serid == "-1"){
		$("#nat_tr").hide();
		$("#routing_tr").hide();
	}else{
		if($_("iptype").value == 4){
			$("#nat_tr").show();
		}
		$("#routing_tr").show();
	}
};

function changeipsettings(id){
	
		var temp = new Array("routing", "internal", "nat", "vlan");
				 
		if(is_checked(id)){
			$("#"+id).removeAttr("disabled");
			$("#"+id).show();
			
			if(id == "internal"){
				$("#internal_bridge_tr").show();
			}
			
			if(id == "vlan"){
				$("#vlan_bridge_tr").show();
			}
			
			for(x in temp){
				if(temp[x] != id){
					$("#"+temp[x]).attr("disabled", "disabled");
					if(temp[x] == "internal"){
						$("#internal_bridge_tr").hide();
					}
					if(temp[x] == "vlan"){
						$("#vlan_bridge_tr").hide();
					}
				}
			}
			
		}else{
			
			for(x in temp){
				$("#"+temp[x]).removeAttr("disabled");
				if(temp[x] == "internal"){
					$("#internal_bridge_tr").hide();
				}
				if(temp[x] == "vlan"){
					$("#vlan_bridge_tr").hide();
				}
			}
			
		}
		
};

addonload("changetype('.POSTval('iptype', 4).');handletr();");
</script>

<div id="form-container">
<form accept-charset="'.$globals['charset'].'" name="addippool" method="post" action="" class="form-horizontal">

<input type="hidden" id="iptype" name="iptype" value="'.POSTval('iptype', 4).'">

<ul class="nav nav-tabs">
	<li class="col-xs-4"><label class="control-label">'.$l['iptype'].'</label></li>
	<li class="col-xs-4" id="ipv4" style="text-align:center"><a href="javascript:changetype(4);"><img src="'.$theme['images'].'admin/ipv4.gif" /></a></li>
	<li class="col-xs-4" id="ipv6" style="text-align:center"><a href="javascript:changetype(6);"><img src="'.$theme['images'].'admin/ipv6.gif" /></a></li>
</ul>

<span class="help-block"></span>
<div class="row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['pool_server'].'</label>
		<span class="help-block">'.$l['exp_server'].'</span>
	</div>
	<div class="col-sm-4">';
	
		$POSTserid = @$_POST['serid'];
		$POSTserid = empty($POSTserid) ? array() : $POSTserid;
		
		echo '<select class="form-control" name="serid[]" id="serid" size="8" onchange="handletr();" multiple="multiple">
			<option value="-1" '.(in_array(-1, $POSTserid) ? 'selected="selected"' : '').'>'.$l['all_servers'].'</option>';
			
			foreach ($servergroups as $k => $v){
				echo '<option class="fhead" value="'.$k.'_group" '.(in_array($k.'_group', $POSTserid) ? 'selected="selected"' : '').'>[Group]&nbsp;'.$v['sg_name'].'</option>';
				
				foreach ($servers as $m => $n){
					if($n['sgid'] == $k){
						echo '<option value="'.$n['serid'].'" '.(in_array((string)$n['serid'], $POSTserid) ? 'selected="selected"' : '').'>&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;'.$n['server_name'].'</option>';
					}
				}
			}
	echo '</select>
		</div>
	<div class="col-sm-4"></div>
</div>
<span class="help-block"></span>
<div class="row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['ippool_name'].'</label>
		<span class="help-block">'.$l['nameofip'].'</span>
	</div>
	<div class="col-sm-4"><input type="text" class="form-control" name="ippool_name" id="ippool_name" size="30" value="'.POSTval('ippool_name', '').'" /></div>
	<div class="col-sm-4"></div>
</div>

<div class="row">
	<div class="col-sm-4"><label class="control-label">'.$l['gateway'].'</label></div>
	<div class="col-sm-4"><input type="text" class="form-control" id="gateway" name="gateway" value="'.POSTval('gateway', '').'" size="30"></div>
	<div class="col-sm-4"></div>
</div>
<span class="help-block"></span>
<div class="row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['netmask'].'</label>
		<span id="netmask_6"><label class="control-label">'.$l['routing_prefix'].'</label><span class="help-block">'.$l['netmask_6'].'</span></span>
	</div>
	<div class="col-sm-4"><label id="6_slash" style="float:left; font-size: 25px; padding: 0 22px 0 0;">/</label><input id="netmask" type="text" class="form-control" name="netmask" value="'.POSTval('netmask', '').'" size="30">
	</div>
</div>
<span id="4_space"></span>
<div class="row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['nameserver'].' 1</label><br />
		<span class="help-block" id="4_ns1">'.$l['unsure_1'].'</span>
		<span class="help-block" id="6_ns1">'.$l['unsure_1_6'].'</span>
	</div>
	<div class="col-sm-4">
		<input type="text" class="form-control" id = "ns_1" name="ns1" value="'.POSTval('ns1', '').'" size="30">
	</div>
	<div class="col-sm-4">
		<span id="ipv4_ns"><a href="javascript:changeDNS(\'8.8.8.8,8.8.4.4\');">[Google]</a>&nbsp;<a href="javascript:changeDNS(\'208.67.222.222,208.67.220.220\');">[Open DNS]</a>&nbsp;<a href="javascript:changeDNS(\'8.26.56.26,8.20.247.20\');">[Comodo]</a>&nbsp;<a href="javascript:changeDNS(\'208.76.50.50,208.76.51.51\');">[Smart Viper]</a></span>
		<span id="ipv6_ns"><a href="javascript:changeDNS(\'2001:4860:4860::8888,2001:4860:4860::8844\');">[Google]</a></span>
	</div>
</div>

<div class="row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['nameserver'].' 2</label><br />
		<span class="help-block" id="4_ns2">'.$l['unsure_2'].'</span>
		<span class="help-block" id="6_ns2">'.$l['unsure_2_6'].'</span>
	</div>
	<div class="col-sm-4">
		<input type="text" class="form-control" id = "ns_2" name="ns2" value="'.POSTval('ns2', '').'" size="30">
	</div>
	<div class="col-sm-4"></div>
</div>

<div class="row" id="firstip">
	<div class="col-sm-4">
		<label class="control-label">'.$l['first_IP'].'</label><br/>
		<span class="help-block">'.$l['optional_firstip'].'</span> 
	</div>
	<div class="col-sm-4"><input type="text" class="form-control" name="firstip" value="'.POSTval('firstip', '').'" size="30"></div>
	<div class="col-sm-4"></div>
</div>

<div class="row" id="lastip">
	<div class="col-sm-4">
		<label class="control-label">'.$l['last_IP'].'</label><br/>
		<span class="help-block">'.$l['optional_lastip'].'</span> 
	</div>
	<div class="col-sm-4"><input type="text" class="form-control" name="lastip" value="'.POSTval('lastip', '').'" size="30"></div>
	<div class="col-sm-4"></div>
</div>

<div class="row" id="internal_tr">
	<div class="col-sm-4">
		<label class="control-label">'.$l['internal'].'</label><br/>
		<span class="help-block">'.$l['internal_exp'].'</span>
	</div>
	<div class="col-sm-4">
		<input type="checkbox" id="internal" class="ios" name="internal" '.POSTchecked('internal', '').' onchange="changeipsettings(this.id);" size="30" value="1">
	</div>
	<div class="col-sm-4"></div>
</div>

<div class="row" id="internal_bridge_tr" style="display:none;">
	<div class="col-sm-4">
		<label class="fhead">'.$l['internal_bridge'].'</label><br/>
		<span class="help-block">'.$l['internal_bridge_exp'].'</span>
	</div>
	<div class="col-sm-4">
		<input type="text" class="form-control" name="internal_bridge" value="'.POSTval('internal_bridge', '').'" size="30">
	</div>
	<div class="col-sm-4"></div>
</div>

<div class="row" id="vlan_tr">
	<div class="col-sm-4">
		<label class="control-label">'.$l['vlan'].'</label><br/>
		<span class="help-block">'.$l['vlan_exp'].'</span>
	</div>
	<div class="col-sm-4">
		<input type="checkbox" id="vlan" name="vlan" '.POSTchecked('vlan', '').' onchange="changeipsettings(this.id);" size="30" value="1">
	</div>
	<div class="col-sm-4"></div>
</div>

<div class="row" id="vlan_bridge_tr" style="display:none;">
	<div class="col-sm-4">
		<label class="fhead">'.$l['vlan_bridge'].'</label><br/>
		<span class="help-block">'.$l['vlan_bridge_exp'].'</span>
	</div>
	<div class="col-sm-4">
		<input type="text" class="form-control" name="vlan_bridge" value="'.POSTval('vlan_bridge', '').'" size="30">
	</div>
	<div class="col-sm-4"></div>
</div>

<div class="row" id="nat_tr" style="display:none;">
	<div class="col-sm-4">
		<label class="control-label">'.$l['subnet'].'</label><br/>
		<span class="help-block">'.$l['exp_subnet'].'</span>
	</div>
	<div class="col-sm-4">
		<input type="checkbox" id="nat" class="ios" name="nat" '.POSTchecked('nat', '').' onchange="changeipsettings(this.id);" size="30">
	</div>
	<div class="col-sm-4"></div>
</div>

<div class="row" id="routing_tr" style="display:none;">
	<div class="col-sm-4">
		<label class="control-label">'.$l['add_route'].'</label><br/>
		<span class="help-block">'.$l['exp_add_route'].'</span>
	</div>
	<div class="col-sm-4">
		<input type="checkbox" id="routing" class="ios" name="routing" '.POSTchecked('routing', '').' onchange="changeipsettings(this.id);" size="30">
	</div>
	<div class="col-sm-4"></div>
</div>

<div class="row" id="or"><div class="col-sm-12" align="center" style="font-size:25px;">'.$l['or'].'</div></div>

<div class="row" id="ips">
	<div class="col-sm-4">
		<label class="control-label">Enter IP</label><br />
		<span class="help-block">Enter IP and its associated MAC address</span><br />
		<span class="help-block">'.$l['entermac_exp'].'</span>
	</div>
	<div class="col-sm-8">
		<div id="iptable" style="margin-left:-16px">';
			
			if(is_array($ips) && !empty($ips)){
				foreach($ips as $k => $ip){
					if(empty($ip['ip'])){
						unset($ip['ip']);
					}
					if(empty($ip['mac_addr'])){
						unset($ip['mac_addr']);
					}
				}
			}
			
			if(empty($ips)){
				$ips = array(NULL);
			}
		 
		 	foreach($ips as $ip){
				echo '
				<div class="row">
					<div class="col-sm-6 ip-mac">
						<label>'.$l['ip'].' :&nbsp;&nbsp;</label><input type="text" class="form-control" name="ips[]" value="'.$ip['ip'].'"/>
					</div>
					<div class="col-sm-6 ip-mac">
						<label>'.$l['mac_addr'].' :&nbsp;&nbsp;</label><input type="text" class="form-control" name="macs[]" value="'.$ip['mac_addr'].'"/>
					</div>
				</div>';
			}
  echo '</div><br />
  <input type="button" onclick="addrow(\'iptable\');" class="go_btn" value="'.$l['add_more_ips'].'"/>
	</div>
	<div class="col-sm-0"></div>
</div>

<div class="row" id="ipv6_subnet_row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['ipv6_subnet'].'</label>
		<span class="help-block">'.$l['ipv6_subnet_exp'].'</span>
	</div>
	<div class="col-sm-4">
		<input type="checkbox" class="ios" name="ipv6_subnet" id="ipv6_subnet" '.POSTchecked('ipv6_subnet').' onchange="ipv6_subnet_gen()">
	</div>	
	<div class="col-sm-4"></div>
</div>

<div class="row" id="ipv6_range">
	<div class="col-sm-4">
		<label class="control-label">'.$l['ipv6_range'].'</label>
	</div>
	<div class="col-sm-8">
		<div class="ipv6-range">
			<div> <input type="text" class="form-control" name="ipv6_1" value="'.POSTval('ipv6_1', '').'" size="4" maxlength="4"><label> : </label></div>
			<div> <input type="text" class="form-control" name="ipv6_2" value="'.POSTval('ipv6_2', '').'" size="4" maxlength="4"><label> : </label></div>
			<div> <input type="text" class="form-control" name="ipv6_3" value="'.POSTval('ipv6_3', '').'" size="4" maxlength="4"><label> : </label></div>
			<div> <input type="text" class="form-control" name="ipv6_4" value="'.POSTval('ipv6_4', '').'" size="4" maxlength="4"><label> : </label></div>
			<div> <input type="text" class="form-control" name="ipv6_5" value="'.POSTval('ipv6_5', '').'" size="4" maxlength="4"><label> : </label></div>
			<div> <input type="text" class="form-control" name="ipv6_6" value="'.POSTval('ipv6_6', '').'" size="4" maxlength="4"></div>
			<div style="width:80px"><label> : auto : auto </label></div>
		</div>
	</div>
	
</div>
<span class="help-block"></span>
<div class="row" id="ipv6_num">
	<div class="col-sm-4">
		<label class="control-label">'.$l['ipv6_num'].'</label><br />
		<span class="help-block">'.$l['ipv6_num_exp'].'</span>
	</div>
	<div class="col-sm-4">
		<input type="text" class="form-control" name="ipv6_num" value="'.POSTval('ipv6_num', 50).'" size="10">
	</div>	
	<div class="col-sm-4"></div>
</div>
<div class="row" id="mtu">
	<div class="col-sm-4">
		<label class="control-label">'.$l['mtu'].'</label><br />
		<span class="help-block">'.$l['exp_mtu'].'</span>
	</div>
	<div class="col-sm-4">
		<input type="text" class="form-control" name="mtu" value="'.POSTval('mtu').'" size="10">
	</div>	
	<div class="col-sm-4" style="padding-top:8px;">Bytes</div>
</div>

</div>

<center><input type="submit" value="'.$l['sub_but'].'" class="btn" name="addippool"></center>

</form>
</div>';

}

echo '</div>';
softfooter();

}

?>