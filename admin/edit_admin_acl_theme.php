<?php

//////////////////////////////////////////////////////////////
//===========================================================
// admin_acl_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 2.2.5
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function edit_admin_acl_theme(){

global $theme, $globals, $cluster, $servers, $user, $l, $done, $error, $acls;

softheader($l['<title>']);

echo '<style>label{font-weight: normal;padding-top: 5px;}</style>
<div class="bg" style="width:99%">
<center class="tit"><i class="icon icon-users icon-head"></i>&nbsp;  '.$l['header'].'</center><br />';

error_handle($error);

if(!is_allowed('edit_admin_acl')){
	softfooter();
	return;
}

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp;  '.$l['done'].'</div>';
}

echo '<script language="javascript" type="text/javascript"><!-- // --><![CDATA[

$(document).ready(function(){

	
	$(document).find(":checkbox").each(function(index, element){		
		if(element.checked){
			 $(this).parent().parent().parent().parent().parent().parent().show();	
		}
	});
});

function toggle_div(id){
	if ($("#"+id).is(":hidden")){
		$("#"+id).slideDown("slow");
	}else{
		$("#"+id).slideUp("slow");
	}
}

function plus_onmouseover(id){
	$("#"+id).attr("src", "'.$theme['images'].'admin/plus_hover.gif");
}

function plus_onmouseout(id){
$("#"+id).attr("src", "'.$theme['images'].'admin/plus.gif");
}

// ]]></script>
<style>
.togglediv{
	padding:8px 0px 0px 8px;
}

label{
	vertical-align: super;
  	padding-left: 8px;
}
</style>
<br/><br/>
<form accept-charset="'.$globals['charset'].'" name="edit_admin_acl" method="post" action="" class="form-horizontal">
<center>
<div style="width:40%;">
	<label class="control-label" style="float:left; margin-top:7px">Name</label>
	<input type="text" class="form-control" name="name" id="name" size="30" value="'.POSTval('name', $acls['acl_name']).'" style="width:50%"/>
</div>
</center>
<br/><br/>
<center><label><input type="checkbox" class="select_all" id="select_all" /> '.$l['checkall'].'</label></center>
<br/><br/>

<div class="cat-wrapper">
	<div class="roundheader" style="cursor:pointer;" onclick="toggle_div(\'admindash\');"> '.$l['cat_admin_dash'].'</div>
	
	<div id="admindash" style="width:100%;" class="togglediv">
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_cluster_statistics" '.POSTchecked('act_cluster_statistics', $acls['act_cluster_statistics']).' />  '.$l['cluster_statistics'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_server_statistics" '.POSTchecked('act_server_statistics', $acls['act_server_statistics']).' />  '.$l['server_statistics'].'</label></div>
			<div class="col-sm-3"></div>
			<div class="col-sm-3"></div>
		</div>
	</div>
</div>
<br/>

<div class="cat-wrapper">
	<div class="roundheader" style="cursor:pointer;" onclick="toggle_div(\'vs\');"> '.$l['cat_vs'].'</div>
	<div id="vs" style="width:100%;" class="togglediv">
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" '.POSTchecked('act_vs', $acls['act_vs']).' name="act_vs" /> '.$l['listvs'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" '.POSTchecked('act_vsresources', $acls['act_vsresources']).' name="act_vsresources" /> '.$l['vsresources'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" '.POSTchecked('act_addvs', $acls['act_addvs']).' name="act_addvs" /> '.$l['addvs'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" '.POSTchecked('act_rebuildvs', $acls['act_rebuildvs']).' name="act_rebuildvs" /> '.$l['rebuildvs'].'</label></div>
		</div>
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" '.POSTchecked('act_editvs', $acls['act_editvs']).' name="act_editvs"/> '.$l['editvs'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" '.POSTchecked('act_deletevs', $acls['act_deletevs']).' name="act_deletevs" /> '.$l['deletevs'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_suspendvs" '.POSTchecked('act_suspendvs', $acls['act_suspendvs']).' /> '.$l['suspendvs'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_unsuspendvs" '.POSTchecked('act_unsuspendvs', $acls['act_unsuspendvs']).' /> '.$l['unsuspendvs'].'</label></div>
		</div>
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_vnc" '.POSTchecked('act_vnc', $acls['act_vnc']).' /> '.$l['vsvnc'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_migrate" '.POSTchecked('act_migrate', $acls['act_migrate']).' /> '.$l['migrate'].'</label></div>
		</div>
	</div>
</div>
<br/>

<div class="cat-wrapper">
	<div class="roundheader" style="cursor:pointer;" onclick="toggle_div(\'ippool\');"> '.$l['cat_ippools'].'</div>
	<div id=ippool style="width:100%;" class="togglediv">
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_ippool" '.POSTchecked('act_ippool', $acls['act_ippool']).'  /> '.$l['ippools'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_addippool" '.POSTchecked('act_addippool', $acls['act_addippool']).'  /> '.$l['addippool'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_editippool" '.POSTchecked('act_editippool', $acls['act_editippool']).'  /> '.$l['editippool'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_deleteippool" '.POSTchecked('act_deleteippool', $acls['act_deleteippool']).'  /> '.$l['deleteippool'].'</label></div>			
		</div>
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_ips" '.POSTchecked('act_ips', $acls['act_ips']).'  /> '.$l['ips'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_addips" '.POSTchecked('act_addips', $acls['act_addips']).'  /> '.$l['addips'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_editips" '.POSTchecked('act_editips', $acls['act_editips']).' /> '.$l['editips'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_deleteips" '.POSTchecked('act_deleteips', $acls['act_deleteips']).' /> '.$l['deleteips'].'</label></div>
		</div>
	</div>
</div>
<br/>

<div class="cat-wrapper">
	<div class="roundheader" style="cursor:pointer;" onclick="toggle_div(\'servers\');"> '.$l['cat_servers'].'</div>
	<div id=servers style="width:100%;" class="togglediv">
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_servers" '.POSTchecked('act_servers', $acls['act_servers']).' /> '.$l['servers'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_addserver" '.POSTchecked('act_addserver', $acls['act_addserver']).' /> '.$l['addserver'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_editserver" '.POSTchecked('act_editserver', $acls['act_editserver']).' /> '.$l['editserver'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_deleteserver" '.POSTchecked('act_deleteserver', $acls['act_deleteserver']).' /> '.$l['deleteserver'].'</label></div>
		</div>
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_sg" '.POSTchecked('act_sg', $acls['act_sg']).' /> '.$l['sg'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_addsg" '.POSTchecked('act_addsg', $acls['act_addsg']).' /> '.$l['addsg'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_editsg" '.POSTchecked('act_editsg', $acls['act_editsg']).' /> '.$l['editsg'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_deletesg" '.POSTchecked('act_deletesg', $acls['act_deletesg']).' /> '.$l['deletesg'].'</label></div>
		</div>
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_manageserver" '.POSTchecked('act_manageserver', $acls['act_manageserver']).' /> &nbsp; '.$l['manageserver'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_rebootserver" '.POSTchecked('act_rebootserver', $acls['act_rebootserver']).' /> &nbsp; '.$l['reboot'].'</label></div>
		</div>
	</div>
</div>
<br/>

<div class="cat-wrapper">
	<div class="roundheader" style="cursor:pointer;" onclick="toggle_div(\'storage\');">'.$l['cat_storage'].'</div>
	<div id="storage" style="width:100%;" class="togglediv">
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_storage" '.POSTchecked('act_storage', $acls['act_storage']).' /> &nbsp; '.$l['storage'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_addstorage" '.POSTchecked('act_addstorage', $acls['act_addstorage']).' /> &nbsp; '.$l['addstorage'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_editstorage" '.POSTchecked('act_editstorage', $acls['act_editstorage']).' /> &nbsp; '.$l['editstorage'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_deletestorage" '.POSTchecked('act_deletestorage', $acls['act_deletestorage']).' /> &nbsp; '.$l['deletestorage'].'</label></div>
		</div>
	</div>
</div>
<br/>

<div class="cat-wrapper">
	<div class="roundheader" style="cursor:pointer;" onclick="toggle_div(\'backups\');"> '.$l['cat_backups'].'</div>
	<div id=backups style="width:100%;" class="togglediv">
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_databackup" '.POSTchecked('act_databackup', $acls['act_databackup']).' /> '.$l['backups'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_performdatabackup" '.POSTchecked('act_performdatabackup', $acls['act_performdatabackup']).' /> '.$l['perf_db_backups'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_dldatabackup" '.POSTchecked('act_dldatabackup', $acls['act_dldatabackup']).' /> '.$l['dl_db_bakcups'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_deletedatabackup" '.POSTchecked('act_deletedatabackup', $acls['act_deletedatabackup']).' /> '.$l['del_db_backups'].'</label></div>
		</div>
		<div class="row">		
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_vpsbackupsettings" '.POSTchecked('act_vpsbackupsettings', $acls['act_vpsbackupsettings']).' /> '.$l['edit_vps_backups'].'</label></div>		
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_vpsbackups" '.POSTchecked('act_vpsbackups', $acls['act_vpsbackups']).' /> '.$l['vps_backups'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_restorevpsbackup" '.POSTchecked('act_restorevpsbackup', $acls['act_restorevpsbackup']).' /> '.$l['restore_vs_backups'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_deletevpsbackup" '.POSTchecked('act_deletevpsbackup', $acls['act_deletevpsbackup']).' /> '.$l['delete_vs_backups'].'</label></div>
		</div>
		<div class="row">		
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_backupservers" '.POSTchecked('act_backupservers', $acls['act_backupservers']).' /> '.$l['list_backupservers'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_addbackupserver" '.POSTchecked('act_addbackupserver', $acls['act_addbackupserver']).' /> '.$l['add_backupserver'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_editbackupservsers" '.POSTchecked('act_editbackupservsers', $acls['act_editbackupservsers']).' /> '.$l['edit_backupserver'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_deletebackupserver" '.POSTchecked('act_deletebackupserver', $acls['act_deletebackupserver']).' /> '.$l['delete_backupserver'].'</label></div>
		</div>
		<div class="row">		
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_backup_plans" '.POSTchecked('act_backup_plans', $acls['act_backup_plans']).' /> '.$l['list_backup_plans'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_addbackup_plan" '.POSTchecked('act_addbackup_plan', $acls['act_addbackup_plan']).' /> '.$l['addbackup_plan'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_editbackup_plan" '.POSTchecked('act_editbackup_plan', $acls['act_editbackup_plan']).' /> '.$l['editbackup_plan'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_deletebackup_plan" '.POSTchecked('act_deletebackup_plan', $acls['act_deletebackup_plan']).' /> '.$l['deletebackup_plan'].'</label></div>
		</div>
	</div>
</div>
<br/>

<div class="cat-wrapper">
	<div class="roundheader" style="cursor:pointer;" onclick="toggle_div(\'plans\');"> '.$l['cat_plans'].'</div>
	<div id=plans style="width:100%;" class="togglediv">
		<div class="row">		
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_plans" '.POSTchecked('act_plans', $acls['act_plans']).' /> '.$l['plans'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_addplan" '.POSTchecked('act_addplan', $acls['act_addplan']).' /> '.$l['addplan'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_editplan" '.POSTchecked('act_editplan', $acls['act_editplan']).' /> '.$l['editplan'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_deleteplan" '.POSTchecked('act_deleteplan', $acls['act_deleteplan']).' /> '.$l['deleteplan'].'</label></div>
		</div>
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_dnsplans" '.POSTchecked('act_dnsplans', $acls['act_dnsplans']).' /> '.$l['dnsplans'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_adddnsplan" '.POSTchecked('act_adddnsplan', $acls['act_adddnsplan']).' /> '.$l['adddnsplan'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_editdnsplan" '.POSTchecked('act_editdnsplan', $acls['act_editdnsplan']).' /> '.$l['editdnsplan'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_deletednsplan" '.POSTchecked('act_deletednsplan', $acls['act_deletednsplan']).' /> '.$l['deletednsplan'].'</label></div>
		</div>
	</div>
</div>
<br/>

<div class="cat-wrapper">
	<div class="roundheader" style="cursor:pointer;" onclick="toggle_div(\'users\');"> '.$l['cat_users'].'</div>
	<div id=users style="width:100%;" class="togglediv">
		<div class="row">		
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_users" '.POSTchecked('act_users', $acls['act_users']).' /> '.$l['users'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_adduser" '.POSTchecked('act_adduser', $acls['act_adduser']).' /> '.$l['adduser'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_edituser" '.POSTchecked('act_edituser', $acls['act_edituser']).' /> '.$l['edituser'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_deleteuser" '.POSTchecked('act_deleteuser', $acls['act_deleteuser']).' /> '.$l['deleteuser'].'</label></div>
		</div>
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_suspend_user" '.POSTchecked('act_suspend_user', $acls['act_suspend_user']).' /> &nbsp; '.$l['suspend_user'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_unsuspend_user" '.POSTchecked('act_unsuspend_user', $acls['act_unsuspend_user']).' /> &nbsp; '.$l['unsuspend_user'].'</label></div>
		</div>
	</div>
</div>
<br/>

<div class="cat-wrapper">
	<div class="roundheader" style="cursor:pointer;" onclick="toggle_div(\'media\');"> '.$l['cat_media'].'</div>
	<div id=media style="width:100%;"  class="togglediv">
		<div class="row">		
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_ostemplates" '.POSTchecked('act_ostemplates', $acls['act_ostemplates']).' /> '.$l['templates'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_os" '.POSTchecked('act_os', $acls['act_os']).' /> '.$l['templatebrowser'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_addtemplate" '.POSTchecked('act_addtemplate', $acls['act_addtemplate']).' /> '.$l['addtemplates'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_edittemplate" '.POSTchecked('act_edittemplate', $acls['act_edittemplate']).' /> '.$l['edittemplates'].'</div>
		</div>
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_deletetemplate" '.POSTchecked('act_deletetemplate', $acls['act_deletetemplate']).' /> '.$l['deltetemplates'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_createtemplate" '.POSTchecked('act_createtemplate', $acls['act_createtemplate']).' /> '.$l['createtemplate'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_iso" '.POSTchecked('act_iso', $acls['act_iso']).' /> '.$l['isos'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_addiso" '.POSTchecked('act_addiso', $acls['act_addiso']).' /> '.$l['addiso'].'</label></div>
		</div>
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_editiso" '.POSTchecked('act_editiso', $acls['act_editiso']).' /> '.$l['editisos'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_deleteiso" '.POSTchecked('act_deleteiso', $acls['act_deleteiso']).' /> '.$l['deleteiso'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_mg" '.POSTchecked('act_mg', $acls['act_mg']).' /> '.$l['mgs'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_addmg" '.POSTchecked('act_addmg', $acls['act_addmg']).' /> '.$l['addmgs'].'</label></div>
		</div>
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_editmg" '.POSTchecked('act_editmg', $acls['act_editmg']).' /> '.$l['editmg'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_deletemg" '.POSTchecked('act_deletemg', $acls['act_deletemg']).' /> '.$l['deletemgs'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_list_distros" '.POSTchecked('act_list_distros', $acls['act_list_distros']).' /> '.$l['list_distros'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_add_distro" '.POSTchecked('act_add_distro', $acls['act_add_distro']).' /> '.$l['add_distro'].'</label></div>
		</div>
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_euiso" '.POSTchecked('act_euiso', $acls['act_euiso']).' />&nbsp;'.$l['euiso'].'</label></div>
		</div>
	</div>
</div>
<br/>

<div class="cat-wrapper">
	<div class="roundheader" style="cursor:pointer;" onclick="toggle_div(\'recipe_div\');"> '.$l['cat_recipe'].'</div>
	<div id="recipe_div" style="width:100%;" class="togglediv">
		<div class="row">		
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_recipes" '.POSTchecked('act_recipes', $acls['act_recipes']).' /> '.$l['recipes'].'</label></div>		
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_addrecipe" '.POSTchecked('act_addrecipe', $acls['act_addrecipe']).' /> '.$l['addrecipe'].'</label></div>		
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_editrecipe" '.POSTchecked('act_editrecipe', $acls['act_editrecipe']).' /> '.$l['editrecipe'].'</label></div>
		</div>
	</div>
</div>
<br/>

<div class="cat-wrapper">
	<div class="roundheader" style="cursor:pointer;" onclick="toggle_div(\'config\');"> '.$l['cat_config'].'</div>
	<div id=config style="width:100%;" class="togglediv">
		<div class="row">		
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_config" '.POSTchecked('act_config', $acls['act_config']).' /> '.$l['edit_settings'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_emailsettings" '.POSTchecked('act_emailsettings', $acls['act_emailsettings']).' /> '.$l['edit_emailsettings'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_adminacl" '.POSTchecked('act_adminacl', $acls['act_adminacl']).'  /> '.$l['admin_acl'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_add_admin_acl" '.POSTchecked('act_add_admin_acl', $acls['act_add_admin_acl']).'  /> '.$l['add_admin_acl'].'</label></div>		
		</div>
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_edit_admin_acl" '.POSTchecked('act_edit_admin_acl', $acls['act_edit_admin_acl']).'  /> '.$l['edit_admin_acl'].'</label></div>		
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_delete_admin_acl" '.POSTchecked('act_delete_admin_acl', $acls['act_delete_admin_acl']).'  /> '.$l['delete_admin_acl'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_serverinfo" '.POSTchecked('act_serverinfo', $acls['act_serverinfo']).' /> '.$l['serverinfo'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_licenseinfo" '.POSTchecked('act_licenseinfo', $acls['act_licenseinfo']).' /> '.$l['licenseinfo'].'</label></div>				
		</div>
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_hostname" '.POSTchecked('act_hostname', $acls['act_hostname']).' /> '.$l['hostname'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_changehostname" '.POSTchecked('act_changehostname', $acls['act_changehostname']).' /> '.$l['edithostname'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_maintenance" '.POSTchecked('act_maintenance', $acls['act_maintenance']).' /> '.$l['maintenance'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_kernconfig" '.POSTchecked('act_kernconfig', $acls['act_kernconfig']).' /> '.$l['kern_config'].'</label></div>		
		</div>
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_updates" '.POSTchecked('act_updates', $acls['act_updates']).' /> '.$l['updates'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_emailtemps" '.POSTchecked('act_emailtemps', $acls['act_emailtemps']).' /> '.$l['email_temps'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_editemailtemps" '.POSTchecked('act_editemailtemps', $acls['act_editemailtemps']).' /> '.$l['edit_email_temps'].'</label></div>		
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_ssl" '.POSTchecked('act_ssl', $acls['act_ssl']).' /> '.$l['ssl'].'</label></div>
		</div>
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_editssl" '.POSTchecked('act_editssl', $acls['act_editssl']).' /> '.$l['editssl'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_createssl" '.POSTchecked('act_createssl', $acls['act_createssl']).' /> '.$l['createssl'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_firewall" '.POSTchecked('act_firewall', $acls['act_firewall']).' /> '.$l['firewall'].'</label></div>	
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_importvs" '.POSTchecked('act_importvs', $acls['act_importvs']).' /> '.$l['importvs'].'</label></div>		
		</div>
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_phpmyadmin" '.POSTchecked('act_phpmyadmin', $acls['act_phpmyadmin']).' /> '.$l['pma'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_filemanager" '.POSTchecked('act_filemanager', $acls['act_filemanager']).' /> '.$l['filemanager'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_ssh" '.POSTchecked('act_ssh', $acls['act_ssh']).' /> '.$l['ssh'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_defaultvsconf" '.POSTchecked('act_defaultvsconf', $acls['act_defaultvsconf']).'/> '.$l['dif_vps_config'].'</label></div>
		</div>
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_twofactauth" '.POSTchecked('act_twofactauth', $acls['act_twofactauth']).' /> &nbsp; '.$l['twofactauth'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_orphaneddisk" '.POSTchecked('act_orphaneddisk', $acls['act_orphaneddisk']).' /> '.$l['orphaneddisk'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_deleteorphaneddisk" '.POSTchecked('act_deleteorphaneddisk', $acls['act_deleteorphaneddisk']).' /> '.$l['deleteorphaneddisk'].'</label></div>
		</div>
		<br/>
	</div>
</div>
<br/>

<div class="cat-wrapper">
	<div class="roundheader" style="cursor:pointer;" onclick="toggle_div(\'pdns\');"> '.$l['cat_pdns'].'</div>
	<div id=pdns style="width:100%;"  class="togglediv">
		<div class="row">		
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_pdns" '.POSTchecked('act_pdns', $acls['act_pdns']).' /> '.$l['pdns'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_managepdns" '.POSTchecked('act_managepdns', $acls['act_managepdns']).' /> '.$l['manage_pdns'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_addpdns" '.POSTchecked('act_addpdns', $acls['act_addpdns']).' /> '.$l['add_pdns'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_editpdns" '.POSTchecked('act_editpdns', $acls['act_editpdns']).' /> '.$l['edit_pdns'].'</label></div>
		</div>
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_deletepdns" '.POSTchecked('act_deletepdns', $acls['act_deletepdns']).' /> '.$l['delete_pdns'].'</label></div>		
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_rdns" '.POSTchecked('act_rdns', $acls['act_rdns']).' /> '.$l['rdns'].'</label></div>
		</div>
		<br/>
	</div>
</div>
<br/>

<div class="cat-wrapper">
	<div class="roundheader" style="cursor:pointer;" onclick="toggle_div(\'procs\');"> '.$l['cat_procs'].'</div>
	<div id=procs style="width:100%;" class="togglediv">
		<div class="row">		
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_procs" '.POSTchecked('act_procs', $acls['act_procs']).' /> '.$l['manage_procs'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_services" '.POSTchecked('act_services', $acls['act_services']).' /> '.$l['manage_services'].'</label></div>	
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_webserver" '.POSTchecked('act_webserver', $acls['act_webserver']).' /> '.$l['restart_web'].'</label></div>	
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_network" '.POSTchecked('act_network', $acls['act_network']).' /> '.$l['restart_net'].'</label></div>		
		</div>
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_sendmail" '.POSTchecked('act_sendmail', $acls['act_sendmail']).' /> '.$l['restart_mail'].'</label></div>		
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_mysqld" '.POSTchecked('act_mysqld', $acls['act_mysqld']).'  /> '.$l['restart_mysql'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_iptables" '.POSTchecked('act_iptables', $acls['act_iptables']).'  /> '.$l['restart_iptables'].'</label></div>
		</div>
		<br/>
	</div>
</div>
<br/>

<div class="cat-wrapper">
	<div class="roundheader" style="cursor:pointer;" onclick="toggle_div(\'logs\');"> '.$l['cat_logs'].'</div>
	<div id=logs style="width:100%;" class="togglediv"> 
		<div class="row">		
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_logs" '.POSTchecked('act_logs', $acls['act_logs']).'  /> '.$l['logs'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_deletelogs" '.POSTchecked('act_deletelogs', $acls['act_deletelogs']).' /> '.$l['deletelogs'].'</label></div>	
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_userlogs" '.POSTchecked('act_userlogs', $acls['act_userlogs']).'  /> '.$l['userlog'].'</label></div>	
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_deleteuserlogs" '.POSTchecked('act_deleteuserlogs', $acls['act_deleteuserlogs']).' /> '.$l['deleteuserlogs'].'</label></div>			
		</div>
		<div class="row">
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_loginlogs" '.POSTchecked('act_loginlogs', $acls['act_loginlogs']).' /> '.$l['loginlogs'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_deleteloginlogs" '.POSTchecked('act_deleteloginlogs', $acls['act_deleteloginlogs']).' /> '.$l['delteloginlogs'].'</label></div>	
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_iplogs" '.POSTchecked('act_iplogs', $acls['act_iplogs']).'  /> &nbsp; '.$l['iplogs'].'</label></div>
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_deliplogs" '.POSTchecked('act_deliplogs', $acls['act_deliplogs']).'  /> &nbsp; '.$l['deliplogs'].'</label></div>
		</div>
		<br/>
	</div>
</div>
<br />

<div class="cat-wrapper">
	<div class="roundheader" style="cursor:pointer;" onclick="toggle_div(\'logs\');">'.$l['cat_haproxy'].'</div>
	<div id="logs" style="width:100%;" class="togglediv"> 
		<div class="row">		
			<div class="col-sm-3"><label><input type="checkbox" class="ios" name="act_haproxy" '.POSTchecked('act_haproxy', $acls['act_haproxy']).'  />&nbsp;'.$l['haproxy'].'</label></div>
		</div>
	</div>
</div>
<br />

<center><input type="submit" name="edit_admin_acl" value=" '.$l['save'].'"  class="btn"></center>
</form>
<br />

</div>
';

softfooter();

}

?>