<?php

//////////////////////////////////////////////////////////////
//===========================================================
// editrecipe_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function editrecipe_theme(){

global $theme, $globals, $kernel, $recipe, $l, $info, $error, $done, $recipe;

softheader($l['<title>']);


echo '
<div class="bg">
<center class="tit"><i class="icon icon-recipes icon-head"></i>&nbsp; '.$l['<title>'].'</center>';

error_handle($error);

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
}

echo '
<div id="form-container">
	<form accept-charset="'.$globals['charset'].'" name="editrecipe" method="post" action=""class="form-horizontal">
	<div class="row">
		<div class="col-sm-5">
			<label class="control-label">'.$l['recipe_name'].'</label><br />
			<span class="help-block">'.$l['recipe_name_exp'].'</span>
		</div>
		<div class="col-sm-6">
			<input type="text" class="form-control" name="name" id="base" size="40" value="'.POSTval('name', $recipe['name']).'" />
		</div>
	</div>
	<div class="row">
		<div class="col-sm-5">
			<label class="control-label">'.$l['recipe_logo'].'</label><br />
			<span class="help-block">'.$l['url_exp'].'</span>
		</div>
		<div class="col-sm-6">
			<input type="text" class="form-control" name="recipe_logo" id="recipe_logo" size="40" value="'.POSTval('recipe_logo', $recipe['logo']).'"/>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-5">
				<label class="control-label">'.$l['code'].'</label><br />
				<span class="help-block">'.$l['code_exp'].'</span>
		</div>
		<div class="col-sm-6">
			<textarea class="form-control" style="color: #333333; border: 1px solid #CCCCCC; font-family:monospace" cols="70" name="code" rows="15" >'.POSTval('code', $recipe['code']).'</textarea>
		</div>
	</div><br />
	<div class="row">
		<div class="col-sm-5">
			<label class="control-label">'.$l['recipe_desc'].'</label><br />
			<span class="help-block">'.$l['exp_desc'].'</span>
		</div>
		<div class="col-sm-6">
			<textarea class="form-control" style="color: #333333; border: 1px solid #CCCCCC; font-family:monospace" cols="70" name="desc" rows="15" >'.POSTval('desc', $recipe['desc']).'</textarea>
		</div>
	</div><br />
	<center><input type="submit" name="editrecipe" value="'.$l['submit'].'" class="btn"></center>
	</form>
</div>
</div>';

softfooter();

}

?>