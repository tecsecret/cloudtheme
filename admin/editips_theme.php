<?php

//////////////////////////////////////////////////////////////
//===========================================================
// editips_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function editips_theme(){
	

global $theme, $globals, $cluster, $servers, $user, $l, $error, $ippools, $ip, $ipid, $done;

softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-ippool icon-head"></i> '.$l['editip'].'</center>';

error_handle($error);

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
}

echo '
<form accept-charset="'.$globals['charset'].'" name="editips" method="post" action="" class="form-horizontal">

<div class="row">
	<div class="col-sm-5">
		<label class="control-label">'.$l['enterip'].'</label>
		<span class="help-block">'.$l['enterip_exp'].'</span>
	</div>
	<div class="col-sm-7">
		<input type="text" class="form-control" name="ip"  size="30" '.(empty($ip['vpsid']) ? ' value="'.POSTval('ip' , $ip['ip']).'"' : 'value="'.$ip['ip'].'" disabled="disabled" ').'/>
	</div>
</div>

<div class="row">
	<div class="col-sm-5">
		<label class="control-label">'.$l['entermac'].'</label>
		<span class="help-block">'.$l['entermac_exp'].'</span>
	</div>
	<div class="col-sm-7">
		<input type="text" class="form-control" name="mac_addr" value="'.POSTval('mac_addr', $ip['mac_addr']).'" size="30" />
	</div>
</div>

<div class="row">
	<div class="col-sm-5">
		<label class="control-label">'.$l['lockedip_lbl'].'</label>
		<span class="help-block">'.$l['lockedip_exp'].'</span>
	</div>
	<div class="col-sm-7">
		<input type="checkbox" name="locked" '.POSTchecked('locked', $ip['locked']).' />
	</div>
</div>

<div class="row">
	<div class="col-sm-5">
		<label class="control-label">'.$l['note'].'</label>
		<span class="help-block">'.$l['note_exp'].'</span>
	</div>
	<div class="col-sm-7">
		<textarea class="form-control" name="note" rows="4">'.POSTval('note', $ip['note']).'</textarea>
	</div>
</div>

<br>

<div class="row">
	<div class="col-sm-5">
		<label class="control-label">'.$l['ippool'].'</label>
		<span class="help-block"></span>
	</div>
	<div class="col-sm-7">
		'.(empty($ippools[$ip['ippid']]['ippool_name']) ? '<i>'.$l['none'].'</i>' : $ippools[$ip['ippid']]['ippool_name']).'
	</div>
</div>

<div id="ip_serid" class="row">
	<div class="col-sm-5">
		<label class="control-label">'.$l['sel_server'].'</label>
		<span class="help-block"></span>
	</div>
	<div class="col-sm-7">';
	if(!empty($ip['ip_serid'])){
		foreach($ip['ip_serid'] as $key => $value){
			echo ($value == -1 ? $l['all_servers'] : $servers[$value]['server_name']."<br>");
		}
	}
	echo '</div>
</div>

<div class="row">
	<div class="col-sm-12">
	<span class="help-block"></span>
		<center><input type="submit" name="editip" class="btn" value="'.$l['editbtn'].'"/></center>
	</div>
</div>

</form>
</div>
';

softfooter();

}

?>