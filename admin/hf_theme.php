<?php

//////////////////////////////////////////////////////////////
//===========================================================
// hf_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function js_url(){	
	$js['givejs'] = func_get_args();
	return $GLOBALS['theme']['url'].'/js/givejs.php?'.http_build_query($js).'&'.$GLOBALS['globals']['version'];
}

function css_url(){	
	$r['givecss'] = func_get_args();
	return $GLOBALS['theme']['url'].'/css/givecss.php?'.http_build_query($r).'&'.$GLOBALS['globals']['version'];
}

function softheader($title = '', $leftbody = true){

global $theme, $globals, $kernel, $user, $l, $error, $servers;

	if(optGET('jsnohf')){
		return true;
	}
	
	$title = ((empty($title)) ? $globals['sn'] : $title);
	
	// Show the all option instead
	$all = (in_array($GLOBALS['act'], array('addiso', 'addplan', 'addserver', 'addtemplate', 'adduser', 'mail', 'createtemplate', 'downloadiso', 'editemailtemp', 'editippool', 'editips', 'editplan', 'editserver', 'edittemplate', 'edituser', 'editvs', 'emailtemp', 'ippool', 'ips', 'iso', 'loginlogs', 'iplogs', 'logs', 'os', 'ostemplates', 'plans', 'servers', 'ubc', 'userlogs', 'users', 'vnc', 'webuzo', 'vs', 'backupservers', 'addbackupserver', 'editbackupserver', 'databackup', 'migrate', 'feedback', 'tasks', 'addippool', 'config', 'updates', 'recipes', 'addrecipe', 'editrecipe', 'clone', 'vps_stats', 'server_stats', 'config_slave', 'addvs', 'haproxy', 'backup_plans', 'addbackup_plan', 'editbackup_plan', 'euiso', 'webuzo', 'ha', 'twofactauth', 'storage', '')) ? 1 : 0);
	
	//Lets echo the top headers
	echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset='.$globals['charset'].'" />
	<meta name="keywords" content="softaculous, software" />
	<title>'.$title.'</title>
	<link rel="stylesheet" type="text/css" href="'.css_url('bootstrap.min.css', 'jquery-ui.css', 'select2.css', 'dataTables-jui.css', 'common.css', 'font.css', 'admin.css', 'jquery.scrollbar.css', 'chosen.min.css').'" />
	
	<link rel="shortcut icon" href="'.$theme['images'].'favicon.ico" />
	<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="'.$theme['url'].'/js/excanvas.min.js"></script><![endif]-->
	<!--[if IE 7]>
	<link rel="stylesheet" type="text/css" href="'.css_url('ie7.css').'">
	<![endif]-->
	<script language="javascript" src="'.js_url('jquery.js', 'bootstrap.min.js', 'tiptip.js', 'domdrag.js', 'menu.js', 'jquery-ui.js', 'slider.js', 'select2.js',   'jquery.tablesorter.min.js', 'universal.js', 'jquery.flot.min.js', 'jquery.flot.stack.js', 'jquery.flot.pie.min.js', 'jquery.flot.selection.min.js', 'jquery.flot.axislabels.js', 'jquery.flot.resize.min.js', 'jquery.sparkline.min.js', 'jquery.scrollbar.min.js', 'chosen.jquery.min.js').'" type="text/javascript"></script>
	<script language="javascript" id="lazyload" type="text/javascript"></script>
<script language="javascript" type="text/javascript"><!-- // --><![CDATA[

// For refreshing public notes every 10 secs
var interval = null;
var notes_search = null;
var notes_count = [0, 0];
var notes_currpage = [1, 1];

var pbar_notice = "'.$l['pbar_notice'].'";
var pbar_tip = "'.$l['pbar_tip'].'";

cur_server = new Object();
cur_server.id = '.$globals['server'].';
cur_server.virt = "'.$servers[$globals['server']]['virt'].'";
function changeserver(id, virt, el){
	puller("serv_list");
	if(id == cur_server.id){
		return false;
	}
	cur_server.id = id;
	cur_server.virt = virt;
	setcookie("'.$globals['cookie_name'].'_server", id, 365);
	var cells = el.cells;
	$_("cur_serv_virt").src="'.$theme['images'].'admin/"+virt+"_28.gif";
	$_("cur_serv_name").innerHTML = cells[1].innerHTML;
	$_("cur_serv_ip").innerHTML = cells[2].innerHTML;
	try{ on_server_change(); }catch(e){	window.location.href = window.location.href; }
	return true;
};

// New changeserver function
function changeserver2(id) {
	setcookie("'.$globals['cookie_name'].'_server", id, 365);
	window.location = window.location;
}

// Returns the template for server select drop down
function formatState (state) {
	if (!state.id) { return state.text; }
	var icon = $(state.element).data("icon");
	var $state = $(
		"<span><img src=\"" + icon + "\" class=\"img-flag\" style=\"margin-right:16px\" /> " + state.text + "</span>"
	);
	return $state;
};

$(document).ready(function() {
	$(".server-select").select2({
		templateResult: formatState,
		templateSelection: formatState
	});
	
	$(".server-select").on("change", function() {
		changeserver2(this.value);
	});	

	$(".virt-select").select2();
});

// A function to show detailed errors
function deterr(){
	$(".deterr-toggle").toggle();
	$("#deterr-container").toggle("slow");
}

$( document ).ready(function() {
	initiate_status();
	if($(".wiki_help").length > 0){
		$(".wiki_help").tipTip({delay:100, defaultPosition:"left"});
	}
});
		
function initiate_status(){
		
	var main = $("#softmain tr");
	
	main.each(function(){
		
		var cookie_id = $(this).closest("tr").attr("id");
		var if_isset = getcookie(cookie_id);
		var tmp_cookieid = String(cookie_id);
		//alert(tmp_cookieid +" - "+ if_isset);
		if(if_isset == 2 && tmp_cookieid != "undefined"){
			
			//alert(if_isset+"--"+ cookie_id)
			var id = $($("#"+cookie_id).next("tr").find("div"));
			//alert(id.attr("id"))
			id.show();	
			var tmp_img = tmp_cookieid.split("_");
			$("#icat"+tmp_img[1]).attr("src", "'.$theme['images'].'expanded.png");
		}
	});
}

function init(){
	$(".soft_cathead_slide").click(function(){
		
		var cat_head = $(this).attr("id");
		var tmp_img = cat_head.split("_");
		//alert(cat_head);
		
		var id = $($(this).next("tr").find("div"));
		//alert(id.attr("id"))
		if(id.css("display") == "none"){
			id.slideDown("slow");
			$("#icat"+tmp_img[1]).attr("src", "'.$theme['images'].'expanded.png");
			setcookie(cat_head, 2, 365);
		}else{
			id.slideUp("slow");
			removecookie(cat_head);
			setcookie(cat_head, "", -365);
			$("#icat"+tmp_img[1]).attr("src", "'.$theme['images'].'collapsed.png");
		}
	});
}

var note_template = \'<div data-id="" data-type="" class="note"><div class="note_title"></div><pre class="note_content"></pre><div class="note_bottom"><div class="note_actions"><div class="note_notify checkbox"><label><input type="checkbox" checked /> Notify admins via email</label></div><a class="note_save" href="#" title="'.$l['note_save'].'" onclick="save_note(this); return false;"><i class="icon icon-save"></i></a><a class="note_cancel" href="#" title="'.$l['note_cancel'].'" onclick="cancel_edit_note(this); return false;"><i class="icon icon-undo"></i></a><a class="note_edit" href="#" title="'.$l['edit'].'" onclick="edit_note(this); return false;"><i class="icon icon-edit"></i></a><a class="note_done" href="#" title="'.$l['note_done'].'" onclick="perform_action(this, \\\'done\\\'); return false;"><i class="icon icon-done"></i></a><a class="note_delete" href="#" title="'.$l['delete'].'" onclick="perform_action(this, \\\'delete\\\'); return false;"><i class="icon icon-delete"></i></a></div><div class="note_info"></div></div></div>\';

var note_form_template = \'<div class="note_form"><input class="note_title form-control" type="text" maxlength="128" placeholder="'.$l['note_title'].'..." /><textarea class="note_content form-control" placeholder="'.$l['note_content'].'..." rows="1" onkeyup="adjust_textarea_height(this)"></textarea></div>\';

function add_note() {
	var type = $(".admin_notes ul.nav-tabs li.active").data("type");
	var el = type == 0 ? "#notes_private" : "#notes_public";
	
	// Create a copy of note template
	var $temp = $(note_template).clone();
	
	// Insert data into template
	$temp.data("type", type).addClass("note_editing note_unsaved");
	$temp.find(".note_info").html("'.$l['note_unsaved'].'");
	$temp.find(".note_edit, .note_done, .note_delete").hide();
	$temp.find("div.note_title").before(note_form_template);
	$temp.find("div.note_title, pre.note_content").hide();
	
	if(type == 0) {
		// If private note remove notify checkbox
		$temp.find(".note_notify").remove();
	} else {
		// If public note remove edit button
		$temp.find(".note_edit").remove();
	}
	
	// Add to start of notes
	$temp.prependTo(el + " .notes_content");
	
	update_note_placeholders();
}

function edit_note(el) {
	var note = $(el).closest(".note");
	
	$(note).addClass("note_editing");
	$(note).find(".note_edit, .note_done, .note_delete").hide();
	$(note).find(".note_save, .note_cancel").show();
	
	var title = $(note).find("div.note_title").text();
	var content = $(note).find("pre.note_content").text();
	
	// Insert note form and hide title, content
	$(note).find("div.note_title").before(note_form_template);
	$(note).find("div.note_title, pre.note_content").hide();
	
	// Set note form initial values
	$(note).find("input.note_title").val(title);
	$(note).find("textarea.note_content").val(content);
	
	adjust_textarea_height($(note).find("textarea.note_content"));
}

function cancel_edit_note(el) {
	var note = $(el).closest(".note");
	
	if($(note).hasClass("note_unsaved")) {
		$(note).remove();
		update_note_placeholders();
		return;
	}
	
	$(note).removeClass("note_editing");
	$(note).find(".note_edit, .note_done, .note_delete").show();
	$(note).find(".note_save, .note_cancel").hide();
	
	$(note).find("div.note_form").remove();
	$(note).find("div.note_title, pre.note_content").show();
}

function save_note(el) {
	var note = $(el).closest(".note");
	var noteid = $(note).data("id");
	var title = $(note).find("input.note_title").val();
	var content = $(note).find("textarea.note_content").val();
	var type = $(note).data("type");
	var notify = $(note).find(".note_notify input").is(":checked");
	
	if(!title) {
		alert("'.$l['note_no_title'].'");
		return false;
	}
	
	if(!content) {
		alert("'.$l['note_no_content'].'");
		return false;
	}
	
	var notedata = "edit="+noteid;
	var type_txt = type == 0 ? "private" : "public";
	
	// If noteid not present then add note
	if(!noteid) {
		notedata = "add=1&type="+type;
	}

	notedata += "&title="+encodeURIComponent(title)+"&content="+encodeURIComponent(content);
	
	if(notify) {
		notedata += "&notify=1";
	}
	
	// Stop refreshing temporarily
	clearInterval(interval);
	
	$.ajax({
		url: "'.$globals['index'].'&act=adminnotes&"+type_txt+"=1&page="+notes_currpage[type]+"&api=json",
		data: notedata,
		dataType : "json",
		method : "post",
		success:function(data){
			if("error" in data) {
				alert(data["error"]);
				return;
			}
			
			update_count(type, data["count_"+type_txt]);
			
			if(!noteid) {
				noteid = data["done"];
			}
			
			var notes = data["notes_"+type_txt];
			
			var found = false;
			for(var i=0; i<notes.length; i++) {
				if(notes[i]["noteid"] == noteid) {
					// Update the note
					$(note).data("id", noteid);
					$(note).removeClass("note_editing note_unsaved");
					$(note).find(".note_notify").remove();
					$(note).find(".note_edit, .note_done, .note_delete").show();
					$(note).find(".note_save, .note_cancel").hide();
					
					$(note).find("div.note_form").remove();
					$(note).find("div.note_title").html(notes[i]["title"]).show();
					$(note).find("pre.note_content").html(notes[i]["content"]).show();
					
					if(type == 0) {
						$(note).find(".note_info").html(notes[i]["time"]).show();
					} else {
						$(note).find(".note_info").html(notes[i]["user"] + " (" + notes[i]["time"] + ")").show();
					}
					
					found = true;
					break;
				}
			}
			
			if(!found) {
				// Note does not exist on current page anymore.
				$(note).remove();
			}
		},
		complete: function() {
			// Start refreshing again
			interval = setInterval(function() {
				refresh_notes(1);
			}, 10000);
		}
	});
}

function perform_action(el, action){
	
	if(action == "delete"){
		var conf = confirm("'.$l['note_confirm_delete'].'");
		if(!conf){
			return false;
		}
	}
	
	var note = $(el).closest(".note");
	var noteid = $(note).data("id");
	var type = $(note).data("type");
	var type_txt = type == 0 ? "private" : "public";
	
	clearInterval(interval);
	
	$.ajax({
		url: "'.$globals['index'].'&act=adminnotes&api=json&"+type_txt+"=1",
		data: action+"="+noteid,
		dataType : "json",
		method : "post",
		success:function(data){
			if("error" in data) {
				alert(data["error"]);
				return;
			}
			
			$(note).remove();
			
			
			update_count(type, data["count_"+type_txt]);
			
			update_note_placeholders();
		},
		complete: function() {
			// Refresh public notes every 10 secs
			interval = setInterval(function() {
				refresh_notes(1);
			}, 10000);
		}
	});
};

function refresh_notes(type) {
	var query = "";
	
	if(type == 0) {
		query = "&private=1";
	} else if(type == 1) {
		query = "&public=1";
	}
	
	query += "&page="+notes_currpage[type];
	
	if(notes_search) {
		query += "&search="+encodeURIComponent(notes_search);
	}
	
	$.ajax({
		url: "'.$globals['index'].'&act=adminnotes&api=json"+query,
		dataType : "json",
		method : "get",
		timeout: 10000,
		success: function(data){
			if("notes_private" in data) {
				var notes = data["notes_private"];
				
				$("#notes_private .note:not(.note_editing)").remove();
				
				for(var i=0; i<notes.length; i++) {
					var $temp = $(note_template).clone();
					
					$temp.data("id", notes[i]["noteid"]).data("type", 0);
					$temp.find(".note_title").html(notes[i]["title"]);
					$temp.find(".note_content").html(notes[i]["content"]);
					$temp.find(".note_info").html(notes[i]["time"]);
					$temp.find(".note_save, .note_cancel").hide();
					$temp.find(".note_notify").remove();
					
					$temp.appendTo("#notes_private .notes_content");
				}
				
				update_count(0, data["count_private"]);
			}
			
			if("notes_public" in data) {
				var notes = data["notes_public"];
				
				$("#notes_public .note:not(.note_editing)").remove();
				
				for(var i=0; i<notes.length; i++) {
					var $temp = $(note_template).clone();
					
					$temp.data("id", notes[i]["noteid"]).data("type", 1);
					$temp.find(".note_title").html(notes[i]["title"]);
					$temp.find(".note_content").html(notes[i]["content"]);
					$temp.find(".note_info").html(notes[i]["user"] + " (" + notes[i]["time"] + ")");
					$temp.find(".note_notify, .note_save, .note_cancel, .note_edit").remove();
					
					if(notes[i]["uid"] != '.$user['uid'].') {
						$temp.find(".note_done, .note_delete").remove();
					}
					
					$temp.appendTo("#notes_public .notes_content");
				}
				
				update_count(1, data["count_public"]);
			}
			
			update_note_placeholders();
		}
	});
}

function refresh_search() {
	notes_search = $("#notes_search_input").val();
	notes_currpage = [1, 1];
	refresh_notes();
}

// Updates the notes count and pagination links
function update_count(type, count) {
	
	notes_count[type] = count;
	var pages = Math.ceil(count / 20);
	pages = pages == 0 ? 1 : pages;
	
	// If current page does not exist anymore
	if(notes_currpage[type] > pages) {
		notes_currpage[type] = pages;
	}
	
	var type_txt = type == 0 ? "private" : "public";
	
	// Update the count
	$("#count_"+type_txt).text(count);
	
	// Calculate pages
	var pg_start = (notes_currpage[type] - 2) < 1 ? 1 : (notes_currpage[type] - 2);
	var pg_end = (pg_start + 4) > pages ? pages : (pg_start + 4);
	
	var $ul = $("#notes_"+type_txt).find("ul");
	
	// Remove all page links
	$ul.find("li").remove();
	
	// Add prev link
	$ul.append("<li class=\"" + (notes_currpage[type] == 1 ? "disabled" : "") + "\"><a href=\"#\" onclick=\"navigate_page(" + type + ", \'prev\'); return false;\"><span aria-hidden=\"true\">&laquo;</span></a></li>");
	
	// Add page links
	for(var i=pg_start; i<=pg_end; i++) {
		$ul.append("<li class=\"" + (i == notes_currpage[type] ? "active" : "") + "\"><a href=\"#\" onclick=\"navigate_page(" + type + ", " + i + "); return false;\"><span aria-hidden=\"true\">" + i + "</span></a></li>");
	}
	
	// Add next link
	$ul.append("<li class=\"" + (notes_currpage[type] == pages ? "disabled" : "") + "\"><a href=\"#\" onclick=\"navigate_page(" + type + ", \'next\'); return false;\"><span aria-hidden=\"true\">&raquo;</span></a></li>");
}

function update_note_placeholders() {
	
	// private notes
	if(notes_count[0] == 0 && $("#notes_private .note").length == 0) {
		$("#notes_private .note_placeholder").text("'.$l['note_no_notes'].'").show();
	} else {
		$("#notes_private .note_placeholder").hide();
	}
	
	// public notes
	if(notes_count[1] == 0 && $("#notes_public .note").length == 0) {
		$("#notes_public .note_placeholder").text("'.$l['note_no_notes'].'").show();
	} else {
		$("#notes_public .note_placeholder").hide();
	}
}

function navigate_page(type, page) {
	var type_txt = type == 0 ? "private" : "public";
	var pages = Math.ceil(notes_count[type] / 20);
	
	if(page == "prev" && notes_currpage[type] > 1) {
		notes_currpage[type]--;
	} else if(page == "next" && notes_currpage[type] < pages) {
		notes_currpage[type]++;
	} else if(page >= 1 && page <= pages) {
		notes_currpage[type] = page;
	} else {
		// Invalid page return
		return;
	}
	
	// Remove notes & show loading
	$("#notes_"+type_txt).find(".note:not(.note_unsaved)").remove();
	$("#notes_"+type_txt).find(".note_placeholder").text("'.$l['loading'].'").show();
	
	// Refresh the page.
	clearInterval(interval);
	
	refresh_notes(type);
	
	// Refresh public notes every 10 secs
	interval = setInterval(function() {
		refresh_notes(1);
	}, 10000);
}

function adjust_textarea_height(el) {
	$(el).each(function() {
		$(this).height("1px");
		$(this).height($(this).prop("scrollHeight")+"px");
	});
}

function close_notes() {
	$("#notes_link").popover("hide");
}

$(document).ready(function() {
	$("#notes_link").popover({
		content: \'<ul class="nav nav-tabs" role="tablist"><li role="presentation" class="active" data-type="0"><a href="#notes_private" role="tab" data-toggle="tab">'.$l['note_private'].'&nbsp;<span id="count_private" class="badge">0</span></a></li><li role="presentation" data-type="1"><a href="#notes_public" role="tab" data-toggle="tab">'.$l['note_public'].'&nbsp;<span id="count_public" class="badge">0</span></a></li></ul><div class="top_right"><form class="form-inline notes_search" onsubmit="refresh_search(); return false;"><div class="input-group"><input id="notes_search_input" type="text" class="form-control" placeholder="'.$l['note_search'].'..."><span class="input-group-btn"><button class="btn btn-default" type="submit">'.$l['note_go'].'</button></span></div></form><a href="#" onclick="add_note(); return false;" title="'.$l['note_add_note'].'"><i class="icon icon-add"></i></a><a href="#" onclick="close_notes(); return false;" title="'.$l['note_close'].'"><i class="icon icon-delete"></i></a></div><div class="tab-content scrollbar-virt"><div role="tabpanel" class="tab-pane fade in active" id="notes_private"><nav aria-label="..."><ul class="pagination"><li class="disabled"><a href="#"><span aria-hidden="true">&laquo;</span></a></li><li class="active"><a href="#">1</a></li><li class="disabled"><a href="#"><span aria-hidden="true">&raquo;</span></a></li></ul></nav><div class="notes_content"></div><div class="note_placeholder">'.$l['note_loading'].'...</div></div><div role="tabpanel" class="tab-pane fade" id="notes_public"><nav aria-label="..."><ul class="pagination"><li class="disabled"><a href="#"><span aria-hidden="true">&laquo;</span></a></li><li class="active"><a href="#">1</a></li><li class="disabled"><a href="#"><span aria-hidden="true">&raquo;</span></a></li></ul></nav><div class="notes_content"></div><div class="note_placeholder">'.$l['note_loading'].'...</div></div></div>\',
		placement: "bottom",
		html: true,
		trigger: "click",
		template: \'<div class="popover admin_notes" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>\',
		container: "body",
		viewport: "body"
	});
	
	$("#notes_link").on("show.bs.popover", function () {
		// Once refresh both notes
		refresh_notes();
		
		clearInterval(interval);
		
		// Refresh public notes every 10 secs
		interval = setInterval(function() {
			refresh_notes(1);
		}, 10000);
	});
	
	$("#notes_link").on("shown.bs.popover", function () {
		$(".tab-content").scrollbar();
	});
	
	$("#notes_link").on("hide.bs.popover", function () {
		clearInterval(interval);
		notes_search = null;
		notes_count = [0, 0];
		notes_currpage = [1, 1];
	});
	
	$(".scrollbar-virt").scrollbar();
});

// ]]></script>

	</head>
	<body onload="bodyonload();">';
	
	echo '<div class="row" id="virt_header" style="background: black;background-repeat: repeat-x;background-size: contain;min-width:374px;margin-right:0px" >
			<div class="col-xs-6 col-sm-3 col-md-2 col-lg-2">
				<div style="float:left; width:220px;" class="logo"><a href="'.$globals['ind'].'"><img src="'.(empty($globals['logo_url']) ? $theme['images'].'loginlogo.gif' : $globals['logo_url']).'" alt="" /></a></div>
			</div>
			<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3" style="padding-top: 5px;">
				<div style="float:left;" title="'.$l['alt_tasks'].'" class="menu-icon"><a href="'.$globals['ind'].'act=tasks"><i class="fa icon-tasks"></i></a></div>
				<div style="float:left;" title="'.$l['alt_feedback'].'" class="menu-icon"><a href="'.$globals['ind'].'act=feedback"><i class="icon-feedback"></i></a></div>
				<div style="float:left;" class="menu-icon"><a href="'.$globals['ind'].'"><i class="icon-house"></i></a></div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4" style="padding-top: 5px;">';
			if(!$all){
				echo '<table style="table-layout:fixed;" border="0" cellpadding="0" cellspacing="0" width="350px" height="30" align="center" class="cur_server" onclick="dropmenu(this, \'serv_list\');" onmouseover="clearhider()" onmouseout="pullmenu(\'serv_list\')">
					<tr>
						<td title="'.$servers[$globals['server']]['virt'].'" width="48" align="center" valign="middle" class="cs_right">
							<img src="'.$theme['images'].'admin/'.$servers[$globals['server']]['virt'].'_28.gif" id="cur_serv_virt" />
						</td>
						<td title="'.$servers[$globals['server']]['server_name'].'" style="padding:0px 5px 0px 5px;white-space:nowrap;text-overflow:ellipsis;overflow:hidden;text-align:center;" width="135px" id="cur_serv_name">
							'.$servers[$globals['server']]['server_name'].'
						</td>
						<td title="'.$servers[$globals['server']]['ip'].'" style="padding:0px 2px 0px 2px;" id="cur_serv_ip" class="cs_right" valign="middle" align="center">
							<span class="serv_exp">'.$l['sl_ip'].': <i>'.$servers[$globals['server']]['ip'].'</i></span>
						</td>
						<td width="20" valign="middle" align="center">
							<img src="'.$theme['images'].'admin/down_arrow.png" />
						</td>
					</tr>
				</table>';
			}
			echo '</div>
			<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3" style="text-align:right;">
				<div id="notes_container" class="menu-icon" style="display:inline-block; padding-top:6px"><a id="notes_link" href="#" onclick="return false;"><i class="icon-notes"></i></a></div>&nbsp;&nbsp;&nbsp;
				<span style="color:white;">'.$l['welcome'].',</span>&nbsp;&nbsp;&nbsp;
				<span style="color:white;"><b>'.@$user['name'].'</b></span>&nbsp;&nbsp;&nbsp;
				<a style="text-decoration:none; color:white;" href="'.$globals['ind'].'act=logout"><b>['.$l['logout'].']</b></a>
				<a href="'.$globals['docs'].'" class="menu-icon" target="_blank"><i class="icon-help"></i></a>
			</div>
		  </div>';// end of id="virt_header"
	
	//The Menus of softwares
	if(!empty($leftbody)){
	
		$theme['leftbody'] = $leftbody;
		
		$leftmenu = array();
		
		$leftmenu['vs'][] = 'vs';
		$leftmenu['vs'][] = 'addvs';				
		$leftmenu['vs'][] = 'rebuild';
		$leftmenu['vs'][] = 'migrate';
//		$leftmenu['vs'][] = 'import';
		$leftmenu['vs'][] = 'clone';
		$leftmenu['vs'][] = 'vnc';
		$leftmenu['vs'][] = 'haproxy';
		$leftmenu['vs'][] = 'ha';
		
		$leftmenu['ippool'][] = 'ippool';
		$leftmenu['ippool'][] = 'addippool';
		$leftmenu['ippool'][] = 'ips';
		$leftmenu['ippool'][] = 'addips';
		$leftmenu['ippool'][] = 'ipranges';
		$leftmenu['ippool'][] = 'addiprange';
		//$leftmenu['ippool'][] = 'managesubnets';
		
		$leftmenu['servers'][] = 'servers';
		$leftmenu['servers'][] = 'addserver';
		$leftmenu['servers'][] = 'servergroups';
		$leftmenu['servers'][] = 'addsg';
		
		$leftmenu['servers'][] = 'backupservers';
		$leftmenu['servers'][] = 'addbackupserver';
		
		$leftmenu['storage'][] = 'storage';
		$leftmenu['storage'][] = 'addstorage';
		$leftmenu['storage'][] = 'orphaneddisks';
		
		$leftmenu['plans'][] = 'plans';
		$leftmenu['plans'][] = 'addplan';
		$leftmenu['plans'][] = 'user_plans';
		$leftmenu['plans'][] = 'adduser_plans';
		$leftmenu['plans'][] = 'dnsplans';
		$leftmenu['plans'][] = 'adddnsplan';
		$leftmenu['plans'][] = 'backup_plans';
		$leftmenu['plans'][] = 'addbackup_plan';
		
		$leftmenu['users'][] = 'users';
		$leftmenu['users'][] = 'adduser';
		$leftmenu['users'][] = 'admin_acl';
		$leftmenu['users'][] = 'mail';
		//$leftmenu['users']['addcloud'] = 'adduser&create=cloud';
		
		if(!empty($globals['enable_storage'])){
			$leftmenu['storage'][] = 'storage';
			$leftmenu['storage'][] = 'addstorage';		
		}
		
		$leftmenu['ostemplates'][] = 'ostemplates';
		$leftmenu['ostemplates'][] = 'os';
		$leftmenu['ostemplates'][] = 'addtemplate';
		$leftmenu['ostemplates'][] = 'createtemplate';
		$leftmenu['ostemplates'][] = 'iso';
		$leftmenu['ostemplates'][] = 'addiso';
		$leftmenu['ostemplates'][] = 'mediagroups';
		$leftmenu['ostemplates'][] = 'addmg';
		$leftmenu['ostemplates'][] = 'list_distros';
		$leftmenu['ostemplates'][] = 'add_distro';
		$leftmenu['ostemplates'][] = 'euiso';
		
		$leftmenu['recipes'][] = 'recipes';
		$leftmenu['recipes'][] = 'addrecipe';
		
		$leftmenu['tasks'] = array();
		
		$leftmenu['config'][] = 'config';
		$leftmenu['config'][] = 'config_slave';
		$leftmenu['config'][] = 'webuzo';
		$leftmenu['config'][] = 'emailconfig';
		$leftmenu['config'][] = 'serverinfo';
		$leftmenu['config'][] = 'multivirt';
		$leftmenu['config'][] = 'license';
		$leftmenu['config'][] = 'hostname';
		$leftmenu['config'][] = 'maintenance';
		$leftmenu['config'][] = 'kernelconf';
		if(server_virt($globals['server'], 'openvz') == 'openvz'){
			$leftmenu['config'][] = 'defaultvsconf';
		}
		$leftmenu['config'][] = 'updates';
		$leftmenu['config'][] = 'emailtemp';
		$leftmenu['config'][] = 'twofactauth';
		$leftmenu['config']['pma'] = 'pma" target="_blank"';
		
		$leftmenu['billing'][] = 'billing';
		$leftmenu['billing'][] = 'resource_pricing';
		$leftmenu['billing'][] = 'invoices';
		$leftmenu['billing'][] = 'transactions';
		$leftmenu['billing'][] = 'addinvoice';
		$leftmenu['billing'][] = 'addtransaction';
		
		$leftmenu['databackup'][] = 'databackup';
		$leftmenu['databackup'][] = 'vpsrestore';
		$leftmenu['databackup'][] = 'backup_plans';
		
		$leftmenu['pdns'] = array();
		$leftmenu['pdns'][] = 'pdns';
		$leftmenu['pdns'][] = 'addpdns';
		$leftmenu['pdns'][] = 'rdns';
		
		$leftmenu['import'][] = 'import';
		$leftmenu['import']['solusvm'] = 'import&sa=solusvm';
		$leftmenu['import']['hypervm'] = 'import&sa=hypervm';
		$leftmenu['import']['feathur'] = 'import&sa=feathur';
		$leftmenu['import']['xenserver'] = 'import&sa=xcp';
		$leftmenu['import']['proxmox'] = 'import&sa=proxmox';
		$leftmenu['import']['openvz7'] = 'import&sa=openvz7';
		
		//$leftmenu['migrate'] = array();
		
		$leftmenu['ssl'][] = 'ssl';
		$leftmenu['ssl'][] = 'createssl';
		$leftmenu['ssl'][] = 'letsencrypt';
		// Adding server statistics
		$leftmenu['server_stats'] = array();
		
		// Adding VPS statistics
		$leftmenu['vps_stats'] = array();
		
		$leftmenu['cpu'] = array();
		//$leftmenu['cpu'][] = 'vscpu';
		
		$leftmenu['serverloads'] = array();
		
		$leftmenu['ram'] = array();
		//$leftmenu['ram'][] = 'vsram';
		
		$leftmenu['disk'] = array();
		
		$leftmenu['bandwidth'] = array();
		//$leftmenu['bandwidth'][] = 'vsbandwidth';
				
		$leftmenu['firewall'] = array();
		
		$leftmenu['performance'] = array();
		$leftmenu['processes'] = array();
		
		$leftmenu['services'][] = 'services';
		$leftmenu['services']['res_httpd'] = 'restartservices&service=webserver';
		$leftmenu['services']['res_network'] = 'restartservices&service=network';
		$leftmenu['services']['res_sendmail'] = 'restartservices&service=sendmail';
		$leftmenu['services']['res_mysqld'] = 'restartservices&service=mysqld';
		$leftmenu['services']['res_iptables'] = 'restartservices&service=iptables';
		
		//$leftmenu['filemanager'] = array();
		$leftmenu['ssh'] = array();
			
		$leftmenu['logs'][] = 'logs';
		$leftmenu['logs'][] = 'userlogs';
		$leftmenu['logs'][] = 'loginlogs';
		$leftmenu['logs'][] = 'iplogs';
		$leftmenu['logs'][] = 'adminnotes';
		
		// Get the Counts
		$res = makequery("SELECT 
								(SELECT COUNT(vps.vpsid) FROM `vps`) AS vpsnum,
								(SELECT COUNT(ipid) FROM `ips`) AS ipsnum,
								(SELECT COUNT(stid) FROM `storage`) AS numstorage, 
								(SELECT COUNT(plid) FROM `plans`) AS numplans, 
								(SELECT COUNT(uid) FROM `users`) AS numusers, 
								(SELECT COUNT(id) FROM `pdns`) AS numpdns, 
								(SELECT COUNT(osid) FROM `os`) AS numos, 
								(SELECT COUNT(rid) FROM `recipes`) AS numrecipes, 
								(SELECT COUNT(actid) FROM `tasks` WHERE 
												ended = 0 AND 
												updated >= ".(time() - $globals['notupdated_task'])." AND 
												started >= ".(time() - $globals['notupdated_task']).") AS numtasks");
		
		// Are there any vlues returned ?
		if(vsql_num_rows($res) > 0){		
			$row = vsql_fetch_assoc($res);
			$total['vs'] = $row['vpsnum'];
			$total['ippool'] = $row['ipsnum'];
			$total['servers'] = count($servers);
			$total['storage'] = $row['numstorage'];
			$total['plans'] = $row['numplans'];
			$total['users'] = $row['numusers'];
			$total['pdns'] = $row['numpdns'];
			$total['ostemplates'] = $row['numos'];
			$total['recipes'] = $row['numrecipes'];
			$total['tasks'] = $row['numtasks'];
		}
		 
		echo '<table border="0" cellpadding="0" cellspacing="0" width="100%" >
	<tr>
		<td align="left" valign="top" width="220" class="fill_bg" id="left-menu">
		<table border="0" cellpadding="0" cellspacing="0" width="100%" >
			<tr>
				<td align="right">
					<form accept-charset="'.$globals['charset'].'" name="search" method="post" action="" onsubmit="return false;" id="searchform" class="form-horizontal">
						<input type="text" class="form-control" name="inputString" id="inputString" onfocus="this.value=\'\';" value="'.$l['search'].'" onblur="initiate_status();" onKeyUp="search_scripts(this.value);" autocomplete="off" placeholder="'.$l['search'].'" style="width:165px; height:30px;padding: 0 0 0 10px;" />
						<div id="suggestions" class="sai_suggestions" style="position:absolute;left:0px; top:0px; display:none;"></div>
					</form>
				</td>
				<td>
					<img src="'.$theme['images'].'search.png"/>
				</td>
			</tr>
		</table>
		<div id="left_panel">
		<script>
		var cat_lang = new Array();
		var sub_cat_lang = new Array();
		
		var services_act = new Array();
		services_act["webserver"] = "res_httpd";
		services_act["network"] = "res_network";
		services_act["sendmail"] = "res_sendmail";
		services_act["mysqld"] = "res_mysqld";
		services_act["iptables"] = "res_iptables";
		
		var total_cnt = '.json_encode($total, JSON_FORCE_OBJECT).';';
		
		echo 'var leftmenu = '.json_encode($leftmenu, JSON_FORCE_OBJECT).';
';
		
		foreach($leftmenu as $menu => $submenu){
			
			echo 'cat_lang[\'lm_'.$menu.'\'] = "'.$l['lm_'.$menu].'";
';
			if(!empty($submenu)){
				foreach($submenu as $k => $v){
				
					if(is_integer($k)) $k = $v;
					
					echo 'sub_cat_lang[\'lms_'.$k.'\'] = "'.$l['lms_'.$k].'";
';
				}
			}
		}
		
		echo '</script></div>
		</td>
		<td><a id="pull"><img src="'.$theme['images'].'menu.png'.'" style="padding: 10px;width: 40px;"/></a></td>
		<td valign="top" id="right_body" style="padding-left:20px;">
		<br />';
		
	}
	
	echo '<script>
//To fix a bug in flot library
var matched, browser;
jQuery.uaMatch = function( ua ) {
    ua = ua.toLowerCase();

    var match = /(chrome)[ \/]([\w.]+)/.exec( ua ) ||
        /(webkit)[ \/]([\w.]+)/.exec( ua ) ||
        /(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
        /(msie) ([\w.]+)/.exec( ua ) ||
        ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
        [];

    return {
        browser: match[ 1 ] || "",
        version: match[ 2 ] || "0"
    };
};

matched = jQuery.uaMatch( navigator.userAgent );
browser = {};

if ( matched.browser ) {
    browser[ matched.browser ] = true;
    browser.version = matched.version;
}

// Chrome is Webkit, but Webkit is also Safari.
if ( browser.chrome ) {
    browser.webkit = true;
} else if ( browser.webkit ) {
    browser.safari = true;
}

jQuery.browser = browser;

	function show_left_panel(){
		
		var str_html = "";
		$.each(leftmenu, function (menu, submenu) {
			var adjust_count = "";
			var cat_link = "javascript:void(0);";
			var drop_img = \'&nbsp;&nbsp;<img src="'.$theme['images'].'collapsed.png" alt="" valign="top" id="icat\'+ menu +\'" />\';
			
			if(typeof submenu[0] == "undefined"){
				var cat_link = \''.$globals['ind'].'act=\'+ menu;
				var drop_img = "";
				var adjust_count = "style=\'margin-right:23px;\'";
			}
			
			var cat_key = "lm_"+menu;
			var disp_total = "";
			
			// We will search for the sa first (If there)
			var sa = getParameterByName("sa");
			var current_act = sa;
			
			// We will check for the service (If there)
			if(sa == ""){
				var service_sa = getParameterByName("service");
				var current_act = services_act[service_sa];
			}
			
			// If we have not found the current act yet than it will be from act only
			if(current_act == "" || typeof(current_act) == "undefined"){
				var act = getParameterByName("act");
				var current_act = act;
			}
			
			if(total_cnt[menu] > 0){
				var disp_total = \'<div class="count" \'+adjust_count+\'>\'+ total_cnt[menu] +\'</div>\';
			}
			
			str_html += \'<table border="0" cellpadding="0" cellspacing="0" id="softmain" width="100%"><tr id="head_\'+ menu +\'" class="soft_cathead_slide tr_width"><td class="soft_cathead" valign="middle" width="100%"><a href="\'+ cat_link +\'"><i class="icon icon-\'+ menu +\'"></i>&nbsp; \'+ cat_lang[cat_key] +\'<span class="no-effect">\'+ disp_total + drop_img +\'</span></a></td></tr>\';
			
			var sub_len = 0;
			for(x in submenu){
				sub_len++;
			}
			
			if(sub_len > 0){
				
				str_html += \'<tr><td><div id="leftcontent" style="display:none;"><ul class="softlinks">\';
				
				$.each(submenu, function (submenu_id, submenu_name) {
					//alert(submenu_id+" - "+submenu_name);
					var sub_cat_key = "lms_"+($.isNumeric(submenu_id) ? submenu_name : submenu_id);
					str_html += \'<a href="'.$globals['ind'].'act=\'+ submenu[submenu_id] +\'" \'+ (current_act == ($.isNumeric(submenu_id) ? submenu_name : submenu_id) ? "class=\"current\"" : "") + \'><li>\'+ sub_cat_lang[sub_cat_key] +\'</li></a>\';
				});
				str_html += \'</ul></div></td></tr>\';
			}
			str_html += \'</table>\';
		});
		
		$("#left_panel").html(str_html);
		init();
	}
	
	show_left_panel();
	
	function search_scripts(val){
		
		var q = val.toLowerCase();
		var qlen = val.length;
		
		if(qlen == 0){
			show_left_panel();
			initiate_status();
			return;
		}
		
		var str_html = "";
		
		$.each(leftmenu, function (menu, submenu) {
			
			var cat_link = "javascript:void(0);";
			var drop_img = \'&nbsp;&nbsp;<img src="'.$theme['images'].'collapsed.png" alt="" valign="top" id="icat\'+ menu +\'" />\';
			
			if(typeof submenu[0] == "undefined"){
				var cat_link = \''.$globals['ind'].'act=\'+ menu;
				var drop_img = "";
			}
			
			var cat_key = "lm_"+menu;
			var disp_total = "";
			
			var current_act = "'.$GLOBALS['act'].'";
			
			if(total_cnt[menu] > 0){
				var disp_total = \'<div class="count">\'+ total_cnt[menu] +\'</div>\';
			}
			
			str_html += \'<table border="0" cellpadding="0" cellspacing="0" id="softmain" width="100%"><tr id="head_\'+ menu +\'" class="soft_cathead_slide tr_width"><td class="soft_cathead" valign="middle" width="100%"><a href="\'+ cat_link +\'"><i class="icon icon-\'+ menu +\'"></i>&nbsp; \'+ cat_lang[cat_key] +\'<span class="no-effect">\'+ disp_total + drop_img +\'</span></a></td></tr>\';
			
			if(typeof submenu[0] != "undefined"){
				
				str_html += \'<tr><td><div id="leftcontent"><ul class="softlinks">\';
				
				$.each(submenu, function (submenu_id, submenu_name) {
					
					var thisRegex = new RegExp(q, "i");
					
					var sub_cat_key = "lms_"+($.isNumeric(submenu_id) ? submenu_name : submenu_id);
					
					if(thisRegex.test(sub_cat_lang[sub_cat_key])){
										
						str_html += \'<a href="'.$globals['ind'].'act=\'+ submenu[submenu_id] +\'" \'+ (current_act == submenu_name ? "class=\"current\"" : "") + \'><li>\'+ sub_cat_lang[sub_cat_key] +\'</li></a>\';
					}
				});
				str_html += \'</ul></div></td></tr>\';
				
			// For Only category to be searched (like tasks, firewall etc..)
			}else{
				str_html += \'<tr><td><div id="leftcontent"><ul class="softlinks">\';
				var thisRegex = new RegExp(q, "i");
				var tmp_cat_key = "lm_"+menu;
				if(thisRegex.test(cat_lang[tmp_cat_key])){
					str_html += \'<a href="'.$globals['ind'].'act=\'+ menu +\'" \'+ (current_act == menu ? "class=\"current\"" : "") + \'></a>\';
				}
				str_html += \'</ul></div></td></tr>\';
			}
			str_html += \'</table>\';
		});
		
		$("#left_panel").html(str_html);
		remove_unnecessary_tables();
		init();
	}
	
	function remove_unnecessary_tables(){
		$.each(leftmenu, function (menu, submenu) {
			var id = $($("#head_"+menu).next("tr").find("div").children());
			if(id.children().length == 0){
				$("#head_"+menu).closest("table").remove();
				$("#head_"+menu).remove();
			}
		});
	}
	</script>';
	
	//Everything else will go here
	echo '<div id="softcontent" style="padding:0px; margin:0px; position:relative;">
	<div class="modal fade" role="dialog" id="logs_modal">
		<div class="modal-dialog" style="width: 60%; height: 50%;">
			<div class="modal-content">
				<div class="modal-header text-center" id="logs_modal_head">
					<button class="close" data-dismiss="modal">&times;</button>
					<span class="fhead">'.$l['logs'].'</span>
				</div>
				<div class="modal-body" id="logs_modal_body" ></div>
			</div>
		</div>
	</div>';
	
}


function softfooter(){

global $theme, $globals, $kernel, $user, $l, $error, $end_time, $start_time, $servers;

if(optGET('jsnohf')){
	return true;
}

echo '</div>';

if(!empty($theme['leftbody'])){
	
	echo '</td>
	</tr>
	</table>';

}

echo '<br />';

echo '<br /><div align="center">'.$l['times_are'].(empty($globals['pgtimezone']) ? '' : ' '.($globals['pgtimezone'] > 0 ? '+' : '').$globals['pgtimezone']).'. '.$l['time_is'].' '.datify(time(), false).'.</div>';

$pageinfo = array();

if(!empty($globals['showntimetaken'])){

	$pageinfo[] = $l['page_time'].':'.substr($end_time-$start_time,0,5);

}

echo '<br />
<table width="98%" align="center" class="table" style="border:2px solid #efefef;background-color:#efefef;">
<tr>
<td align="left">'.copyright().'</td>'.(empty($pageinfo) ? '' : '<td align="right">'.implode('&nbsp;&nbsp;|&nbsp;&nbsp;', $pageinfo).'</td>').'
</tr>
</table>';

if(!empty($theme['copyright'])){

	echo unhtmlentities($theme['copyright']).'';

}

echo '<script language="javascript" type="text/javascript"><!-- // --><![CDATA[
function bodyonload(){
	if(aefonload != \'\'){		
		eval(aefonload);
	}
	'.(empty($onload) ? '' : 'eval(\''.implode(';', $onload).'\');').'
};
// ]]></script>';

echo '
<div style="width:350px;'.(count($servers) > 5 ? 'height:250px;': '').'" class="serv_list" id="serv_list" onmouseover="clearhider()" onmouseout="pullmenu(\'serv_list\')">
<table style="table-layout:fixed;" cellpadding="4" cellspacing="0" border="0" width="100%" height="100%">';
	
	foreach($servers as $k => $v){
		echo '<tr height="32px" class="serv_server" onclick="changeserver('.$k.', \''.$v['virt'].'\', this);">
			<td style="width:48px;" class="serv_right" align="center"><img src="'.$theme['images'].'admin/'.$v['virt'].'_28.gif" /></td>
			<td title="'.$v['server_name'].'" style="padding:0px 5px 0px 5px;white-space:nowrap;text-overflow:ellipsis;overflow:hidden;text-align:center;" width="135px">
				'.$v['server_name'].'
			</td>
			<td >
				'.$l['sl_ip'].': <i>'.$v['ip'].'</i>
			</td>
		</tr>';
	}
	
echo '</table>
</div>';

if($globals['lictype'] == '-2'){
echo '
<script type="text/javascript">
	$(function(){
		$(window).resize(function(e){
			placeDevBanner();
		});
		$.event.add(window, "scroll", function() {
			placeDevBanner();
		});
		placeDevBanner();
		$("#virt_dev_banner").css("position","absolute");
		$("#virt_dev_banner").css("display","inline");
		$("body").css("margin","0 0 "+$("#virt_dev_banner").height()+"px 0");
	});
	function placeDevBanner() {
		var docheight = $("body").height();
		var newheight = $(document).scrollTop() + parseInt($(window).height()) - parseInt($("#virt_dev_banner").height());
		if (newheight>docheight) newheight = docheight;
		$("#virt_dev_banner").css("top",newheight);
		$("body").css("margin","0 0 "+$(virt_dev_banner).height()+"px 0");
	}
</script>
<div id="virt_dev_banner" style="display:block;margin:0;padding:0;width:100%;background-color:#ffffd2;">
	<div style="padding:10px 35px;font-size:16px;text-align:center;color:#555;">'.$l['dev_lic_notice'].'</div>
</div>';
}

echo '
</body>
</html>';
}


function error_handle($error, $table_width = '100%', $center = false, $ret = false){

global $l, $theme;
	
	$str = '';
	
	// For showing logs
	$actid = (int) optREQ('actid');
	
	//on error call the form
	if(!empty($error)){
		
		$str = '
			<div id="error_box">
				<div style="width:'.$table_width.'" class="error alert alert-danger" '.(($center) ? 'align="center"' : '' ).'>
					'.$l['following_errors_occured'].' :<br/>
				<ul>';
		
		foreach($error as $ek => $ev){

			if(strcmp($ek, 'derr') == 0){
				$debug_arr = $error['derr'];
				continue;
			}
			
			$str .= '<li>'.$ev.'</li>';
		
		}
		
		if(!empty($actid)){
			$str .= '<li><a href="javascript:void(0);" onclick="return loadlogs('.$actid.');" class="btn" style="padding:3px;">'.$l['show_logs'].'</a></li>';
		}
		
		$str .= '</ul>
		</div>
		
			<br />';
			
			//Append the additional debug error information
			if(!empty($debug_arr)){
				$str.= '<br />
				
				<div class="deterr-toggle">
					<center><a class="deterr" style="text-decoration:none;" href="#" onclick="deterr();"><img src="'.$theme['images'].'admin/collapsed.gif"/>'.$l['show_debug_info'].'</a></center>
				</div>
				
				<div class="deterr-toggle" style="display:none;">
					<center><a class="deterr" style="text-decoration:none;" href="#" onclick="deterr();"><img src="'.$theme['images'].'admin/expanded.gif"/>'.$l['hide_debug_info'].'</a></center>
				</div>
				
				<br/><br/>
				<div id="deterr-container" style="display:none;background-color:rgb(230, 230, 230);">
					<b>OUTPUT</b>:<pre>'.implode('', $debug_arr['output']).'</pre><br />
					<b>RETURN</b>: '.$debug_arr['return'].'
				</div>
				<br /><br />';
			}
			
		$str .= '</div>';
		
		if(empty($ret)){
			echo $str;
		}else{
			return $str;
		}
		
	}

}


//This will just echo that everything went fine
function success_message($message, $table_width = '100%', $center = false){

global $l;

	//on error call the form
	if(!empty($message)){
		
		echo '<table width="'.$table_width.'" cellpadding="2" cellspacing="1" class="error" '.(($center) ? 'align="center"' : '' ).'>
			<tr>
			<td>
			'.$l['following_message'].' :
			<ul type="square">';
		
		foreach($message as $mk => $mv){
		
			echo '<li>'.$mv.'</li>';
		
		}
		
		
		echo '</ul>
			</td>
			</tr>
			</table>'.(($center) ? '</center>' : '' ).'
			<br />';
		
		
	}

}


function majorerror($title, $text, $heading = ''){

global $theme, $globals, $user, $l;

softheader(((empty($title)) ? $l['fatal_error'] : $title), false);

?><br /><br />
<div class="row">
<div class="col-sm-2 col-md-2"></div>
<div class="col-sm-8 col-md-8" align="center">
<table cellpadding="2" cellspacing="1" class="table tablesorter" align="center" style="border:2px solid #EFEFEF">
	<tr>
	<th align="left">
	<span class="pull-left"><?php echo ((empty($heading)) ? $l['following_fatal_error'].':' : $heading);?></span>
	</th>
	</tr>
	
	<tr>
	<td colspan="2" align="center">
	<img src="<?php echo $theme['images'];?>error.gif" alt="" />
	</td>
	</tr>
	
	<tr>
	<td class="fhead" align="left"><?php echo $text;?><br />
	</td>
	</tr>

</table>
</div>
</div>
<center><a href="javascript: window.history.go(-1)" class="link_btn"><?php echo $l['go_back'];?></a></center>
<?php

softfooter();

//We must return
return true;

}

function message($title, $heading = '', $icon, $text){

global $theme, $globals, $user, $l;

softheader(((empty($title)) ? $l['soft_message'] : $title), false);

?><br /><br />
<div class="row">
<div class="col-sm-2 col-md-2"></div>
<div class="col-sm-8 col-md-8" align="center">
<table width="70%" cellpadding="2" cellspacing="1" class="table tablesorter" align="center" style="border:2px solid #EFEFEF" >
	
	<tr>
	<th align="left">
		<span class="pull-left"><?php echo ((empty($heading)) ? $l['following_soft_message'].':' : $heading);?></span>
	</th>
	</tr>
	
	<tr>
	<td class="bg" colspan="2" align="center">
	<img src="<?php echo $theme['images'].(empty($icon)?'info.gif':$icon);?>" alt="" />
	</td>
	</tr>
	
	<tr>
	<td class="" align="left"><?php echo $text;?><br />
	</td>
	</tr>

</table>
</div>
</div>

<?php

softfooter();

//We must return
return true;

}

function page_links($num_res, $page, $max = 50){
	
global $l, $js, $globals, $theme;
	
	$pages = ceil($num_res/$max); // Number of Pages	
	$pg = ceil(($page/$max) + 1); //Current Page

	$_pages = array();
	
	if($pages > 1){
		
		// Show th Back Links if required
		if($pg != 1){			
			$_pages['&lt;&lt;'] = 1;			
			$_pages['&lt;'] = ($pg - 1);		
		}
		
		for($i = ($pg - 4); $i < $pg; $i++){			
			if($i >= 1){			
				$_pages[$i] = $i;			
			}		
		}
		
		$_pages[$pg] = $pg;
				
		for($i = ($pg + 1); $i <= ($pg + 4); $i++){		
			if($i <= $pages){			
				$_pages[$i] = $i;			
			}		
		}
		
		
		if($pg != $pages){			
			$_pages['&gt;'] = ($pg + 1);			
			$_pages['&gt;&gt;'] = $pages;		
		}	
	}
	
	// We show the jumper if its more than 50
	if($num_res > 50){

$name = 'pgjmp_'.generateRandStr(4);

if(empty($js['menu'])){
	echo '<script language="javascript" src="'.$theme['url'].'/js/menu.js" type="text/javascript"></script>

<script language="javascript" type="text/javascript"><!-- // --><![CDATA[
function go_to(max, pg, urlto){
	try{
	var urlto = (urlto || window.location).toString();
	var pg = pg || 0;
	var final = urlto.replace(/(&?)page\=(\d{1,4})|(&?)reslen\=(\d{1,500})/gi,"")+"&page="+pg+"&reslen="+max;
	window.location = final;
	}catch(e){ }
};
// ]]></script>';

	$js['menu'] = true;
}

$links = '<script language="javascript" type="text/javascript"><!-- // --><![CDATA[
createmenu("'.$name.'", [
[\'<input type="text" name="page" id="in'.$name.'" size="10" /><input type="button" value="'.$l['page_go'].'" onclick="go_to('.$max.', $_(\\\'in'.$name.'\\\').value)" class="go_btn go_btn_small"/>\']
]);

// ]]></script>

<table class="cbgbor" cellspacing="1" cellpadding="0" border="0">
<tr>
<td><a href="#" onmouseover="dropmenu(this, \''.$name.'\')" onmouseout="pullmenu(\''.$name.'\')" title="'.$l['page_jump_title'].'" > &nbsp; '.$l['page_page'].' '.$pg.' '.$l['page_of'].' '.$pages.' &nbsp; </a></td>';

foreach($_pages as $k => $lv){
	$links .= '<td class="'.($k == $pg ? 'activepage' : 'pagelinks' ).'"><a href="javascript:go_to('.$max.', '.$lv.')">'.$k.'</a></td>';
}

$links .= '<td colspan="0">
<select name = "reslen" class="perpage form-control" onchange="return go_to(this.value)">
<option value="-1">--</option>
<option value="50" '.($max == 50 ? 'selected="selected"' : '').'>50</option>
<option value="100" '.($max == 100 ? 'selected="selected"' : '').'>100</option>
<option value="250" '.($max == 250 ? 'selected="selected"' : '').'>250</option>
<option value="500" '.($max == 500 ? 'selected="selected"' : '').'>500</option>
<option value="1000000" '.($max == 1000000 ? 'selected="selected"' : '').'>'.$l['all'].'</option>
</select></td><td> &nbsp; '.$l['entries_per_page'].' &nbsp; </td></tr>
</table>';
	
	echo $links;
	
	}
	
}

// Echos a (?) URL
function virthelp($link){
	
	global $globals;
	
	return '<a href="'.$globals['docs'].$link.'" target="_blank">(?)</a>';	
}

// Displays the servers selection dropdown
function server_select(){
	
	global $servers, $theme, $globals, $l;
	
	echo '<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-2 h5" style="text-align:right;">
			<label class="control-label">'.$l['select_server'].' : </label>
		</div>
		<div class="col-sm-4 server-select-lg">
			<select id="server-select" class="server-select" style="width:100%">';
					
				foreach($servers as $k => $v) {
					echo '<option value="'.$k.'" data-icon="'.$theme['images'].'admin/'.$v['virt'].'_28.png" '.($k == $globals['server'] ? 'selected' : '').'><span><img src="'.$theme['images'].'admin/'.$v['virt'].'_28.png" />'.$v['server_name'].' ('.$v['ip'].')</span></option>';
				}	
				
			echo '</select>
		</div>
		<div class="col-sm-3"></div>
	</div><br />';
	
}

?>