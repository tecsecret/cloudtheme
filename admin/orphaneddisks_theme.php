<?php

//////////////////////////////////////////////////////////////
//===========================================================
// serverinfo_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function orphaneddisks_theme(){

global $theme, $globals, $cluster, $user, $l , $error, $orphaned_disks, $storages;

softheader($l['<title>']);

echo '<div class="bg">
<center class="tit">
<i class="icon icon-disk icon-head"></i> &nbsp;'.$l['heading'].'
<span style="float:right"><a href="javascript:showsearch();"><img src="'.$theme['images'].'admin/search.gif" /></a></span>
</center>
<br />';

echo '<script language="javascript" type="text/javascript"><!-- // --><![CDATA[

function del_disk(disk){
	
	var disk_list = new Array();
	
	if(!disk) {
		if($("#disk_task_select").val() != "delete"){
			alert("'.$l['no_action'].'");
			return false;
		}
		
		$(".ios:checked").each(function() {
			disk_list.push($(this).val());
		});
	} else {
		disk_list.push(disk);
	}
	
	if(disk_list.length < 1){
		alert("'.$l['nothing_selected'].'");
		return false;
	}

	var finalData = new Object();
	finalData["delete"] = disk_list.join(",");
	
	var disk_conf = confirm("'.$l['del_conf'].'");
	if(disk_conf == false){
		return false;
	}
	
	$("#progress_bar").show();
	
	$.ajax({
		type: "POST",
		url: "'.$globals['index'].'act=orphaneddisks&api=json",
		data : finalData,
		dataType : "json",
		success: function(data){
			$("#progress_bar").hide();
			if("error" in data){
				alert(data["error"]);
			}
			location.reload(true);
		},
		error: function(data) {
			$("#progress_bar").hide();
			return false;
		}
	});
	
	return false;
};

// ]]></script>';

$st_types = array_column($storages, 'type');
$st_types = array_unique($st_types);

$_st_types = REQval('st_type');

echo '<div id="showsearch" style="display:'.(optREQ('search') || (!empty($vs) && !empty($globals['showsearch'])) ? "" : "none").';">
<form accept-charset="'.$globals ['charset'].'" name="orphan" method="get" action="" id="orphanform" class="form-horizontal">
<input type="hidden" name="act" value="orphaneddisks">
<div class="form-group_head">
	<div class="row">
	    <div class="col-sm-1"></div>
		
	    <div class="col-sm-2"><label>'.$l['st_id'].'</label></div>
		<div class="col-sm-3"><input type="text" class="form-control" name="st_id" id="st_id" size="30" value="'.REQval('st_id', '').'" /></div>
		
		<div class="col-sm-2"><label>'.$l['st_type'].'</label></div>
		<div class="col-sm-3">
	    	<select name="st_type" id="st_type" class="form-control">
				<option value="0" '.(empty($_st_types) ? 'selected="selected"' : '').'>---</option>';
				
				foreach($st_types as $st_type) {
					echo '<option value="'.$st_type.'" '.(REQval('st_type') == $st_type ? 'selected="selected"' : '').'>'.storage_type_text($st_type).'</option>';
				}
				
			echo '</select>
	    </div>
		
		<div class="col-sm-1"></div>
  	</div>
  	<div class="row">
	    <div class="col-sm-1"></div>
	    
	    <div class="col-sm-2"><label>'.$l['disk_path'].'</label></div>
		<div class="col-sm-8"><input type="text" class="form-control" name="disk_path" id="disk_path" size="30" value="'.REQval('disk_path', '').'" /></div>
		
	    <div class="col-sm-1"></div>
  	</div>
	<div class="row">
		<div class="col-sm-12" style="text-align: center;"><button type="submit" name="search" class="go_btn" value="Search"/>'.$l['submit'].'</button></div>
	</div>
</div>
</form>
<br/><br/><br/>
</div>';

echo '<div class="row"><div class="col-sm-12">';
echo '<table width="100%" class="table table-hover tablesorter"><tbody>';
echo '<tr>
<th><span>' . $l['disk_path'] . '</span></th>
<th><span>' . $l['disk_size'] . '</span></th>
<th><span>' . $l['st_id'] . '</span></th>
<th><span>' . $l['st_type'] . '</span></th>
<th width="75" align="center"><span>' . $l['action'] . '</span></th>
<th style="padding:8px 3px !important"><input id="select_all" class="select_all" name="select_all" type="checkbox"></th></tr>';
foreach($orphaned_disks as $o) {
	echo '<tr>';
	echo '<td>' . $o['path'] . '</td>';
	echo '<td align="center">' . round($o['size'] / 1024 / 1024 / 1024, 2) . ' GB</td>';
	echo '<td align="center">' . $o['st_id'] . '</td>';
	echo '<td align="center">' . storage_type_text($storages[$o['st_id']]['type']) . '</td>';
	echo '<td align="center"><a href="#" onclick="del_disk(\''.$o['path'].'\'); return false;"><img src="'.$theme['images'].'admin/delete.png" /></a>&nbsp;</td>';
	echo '<td style="padding:8px 3px !important" align="center"><input class="ios" name="disk_list[]" value="'.$o['path'].'" type="checkbox"></td>';
	echo '</tr>';
}
echo '</tbody></table>';
echo '</div></div>';

echo '<div class="row bottom-menu">
	<div class="col-sm-6"></div>
	<div class="col-sm-6">
		<label>'.$l['with_selected'].'</label>
		<select name="disk_task_select" id="disk_task_select" class="form-control">
			<option value="0">---</option>
			<option value="delete">'.$l['ms_delete'].'</option>
		</select>&nbsp;
		<input type="button" value="'.$l['go'].'" onclick="del_disk()" class="go_btn"/>
	</div>
</div>';

echo '<div id="progress_bar" style="height:125px; display:none">
	<br />
	<center>
		<font id="progress_txt" size="4" color="#222222">'.$l['action_msg'].'</font>
		<br>
		<br>
	</center>
	<table id="table_progress" width="500" height="28" cellspacing="0" cellpadding="0" border="0" align="center" style="border:1px solid #CCC; -moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px;background-color:#efefef;">
		<tbody>
			<tr>
				<td id="progress_color" width="100%" style="background-image: url(themes/default/images/bar.gif); -moz-border-radius: 4px; -webkit-border-radius: 4px; border-radius: 4px;"></td>
				<td id="progress_nocolor"> </td>
			</tr>
		</tbody>
	</table>
	<br>
	<center>
		'.$l['notify_msg'].'
	</center>
</div>';

softfooter();
	
}

?>