<?php

//////////////////////////////////////////////////////////////
//===========================================================
// emailtemp_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function emailtemp_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $emailtemps, $done, $notice;

softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-config icon-head"></i>&nbsp; '.$l['emailtemp'].'</center>';

error_handle($error);

if(!empty($notice)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['action_not_applicable'].'</div>';
}else{

echo '<table class="table table-hover tablesorter" align="center">
<tr>
	<th align="center">'.$l['emailtemp'].'</th>
	<th align="center">&nbsp;</th>
</tr>';
$i=0;
foreach($emailtemps as $k=>$v){

echo '<tr>		
	<td>'.$l['temp_'.$k].'</td>
	<td><center><a href="'.$globals['ind'].'act=editemailtemp&temp='.$k.'"><img src="'.$theme['images'].'admin/edit.png" /></a></center></td>
</tr>';
$i++;
}

echo '</table>';

}

echo '</div>';
softfooter();

}

?>