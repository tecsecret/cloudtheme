<?php

//////////////////////////////////////////////////////////////
//===========================================================
// emailconfig_theme.php
//===========================================================
// SOFTACULOUS 
// Version : 1.1
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       10th Jan 2009
// Time:       21:00 hrs
// Site:       http://www.softaculous.com/ (SOFTACULOUS)
// ----------------------------------------------------------
// Please Read the Terms of use at http://www.softaculous.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Inc.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function emailconfig_theme(){

global $theme, $globals, $cluster, $l, $langs, $skins, $error, $saved , $done, $info;

softheader($l['<title>']);

echo '
<script type="text/javascript">

	$(document).ready(function(){	
		change_email_opt();
	});

	function change_email_opt(){
		var val = $("#mail_type option:selected").attr("value");
		$(".smtpmail").css("display", "none");
		if(val == 0){
			$(".smtpmail").show();
		}
	}

</script>
<div class="bg">
<center class="tit"><i class="icon icon-config icon-head"></i>&nbsp;&nbsp;'.$l['heading'].'<span style="float:right;" ><a href="'.$globals['docs'].'Configure_Email_Settings" target="_blank" class="wiki_help" title="'.$l['wiki_help'].'"><i class="icon-help" ></i></a></span></center>';

error_handle($error, '100%');

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['change_final'].'</div>';
}

// Is it offline ?
$hypervisor_status = $cluster->statewise($globals['server']);
if($hypervisor_status == 0 || $hypervisor_status == 2){

	echo '<div class="e_notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['server_status_'.$hypervisor_status].'</div>';
	
}else{


echo '<div id="form-container">
<form accept-charset="'.$globals['charset'].'" name="editemailconfigsettings" method="post" action="" class="form-horizontal">

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['mailmethod'].'</label><br />
		<span class="help-block">'.$l['mailmethod_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<select class="form-control" name="mail" onchange="change_email_opt();" id="mail_type">
			<option value="1" '.(POSTval('mail') == '1' ? 'selected="selected"' : ($info['mail'] == 1 ? 'selected="selected"' : '')).'>PHP Mail</option>
			<option value="0" '.(POSTval('mail') == '0' ? 'selected="selected"' : ($info['mail'] == 0 ? 'selected="selected"' : '')).'>SMTP</option>
		</select>
	</div>
</div>

<div class="row smtpmail">
	<div class="col-sm-6">
		<label class="control-label">'.$l['smtp_server'].'</label>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="mail_server" size="30" value="'.POSTval('mail_server', $info['mail_server']).'" />
		<span class="help-block"></span>
	</div>
</div>

<div class="row smtpmail">
	<div class="col-sm-6">
		<label class="control-label">'.$l['smtp_port'].'</label>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="mail_port" size="30" value="'.POSTval('mail_port', $info['mail_port']).'" />
		<span class="help-block"></span>
	</div>
</div>

<div class="row smtpmail">
	<div class="col-sm-6">
		<label class="control-label">'.$l['smtp_user'].'</label>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="mail_user" size="30" value="'.POSTval('mail_user', $info['mail_user']).'" />
		<span class="help-block"></span>
	</div>
</div>

<div class="row smtpmail">
	<div class="col-sm-6">
		<label class="control-label">'.$l['smtp_pass'].'</label>
	</div>
	<div class="col-sm-6">
		<input type="password" class="form-control" name="mail_pass" size="30" value="'.POSTval('mail_pass', $info['mail_pass']).'" />
		<span class="help-block"></span>
	</div>
</div>

<div class="row smtpmail">
	<div class="col-sm-6">
		<label class="control-label">'.$l['smtp_secure'].'</label><br />
		<span class="help-block">'.$l['smtp_secure_exp'].'</span>
	</div>
	<div class="col-sm-6" id="radio_secure">
		<div class="col-sm-4"><input type="radio" id="secure_none" value="0" name="mail_smtp_secure" '.POSTradio('mail_smtp_secure', 0, $info['mail_smtp_secure']).' /><label for="secure_none" style="padding-left: 10px;">None</label></div>
		<div class="col-sm-4"><input type="radio" id="secure_ssl" value="1" name="mail_smtp_secure" '.POSTradio('mail_smtp_secure', 1, $info['mail_smtp_secure']).' /><label for="secure_ssl" style="padding-left: 10px;">SSL</label></div>
		<div class="col-sm-4"><input type="radio" id="secure_starttls" value="2" name="mail_smtp_secure" '.POSTradio('mail_smtp_secure', 2, $info['mail_smtp_secure']).' /><label for="secure_starttls" style="padding-left: 10px;">STARTTLS</label></div>		
	</div>
</div>

<div class="row smtpmail">
	<div class="col-sm-6">
		<label class="control-label">'.$l['mail_connect_timeout'].'</label><br />
		<span class="help-block">'.$l['mail_connect_timeout_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="mail_connect_timeout" size="30" value="'.POSTval('mail_connect_timeout', $info['mail_connect_timeout']).'" />
		<span class="help-block"></span>
	</div>
</div>

<div class="row smtpmail">
	<div class="col-sm-6">
		<label class="control-label">'.$l['mail_debug'].'</label><br />
		<span class="help-block">'.$l['mail_debug_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="checkbox" class="ios" value="1" name="mail_debug" '.POSTchecked('mail_debug', $info['mail_debug']).' />
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['disable_email'].'</label><br />
		<span class="help-block">'.$l['disable_email_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="checkbox" class="ios" value="1" name="disable_email" '.POSTchecked('disable_email', $info['disable_email']).' />
	</div>
</div>

</div>
<br /><br />
<p align="center"><input type="submit" name="editemailconfigsettings" value="'.$l['edit_settings'].'" class="btn"/></p>
</form>';

}

echo '</div>';

softfooter();

}

?>