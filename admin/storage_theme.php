<?php

//////////////////////////////////////////////////////////////
//===========================================================
// ftpservers_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Julien
// Date:       19th July 2012
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function storage_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $storage, $storage_servers, $servers, $servergroups, $done;

softheader($l['<title>']);

if(!empty($request_test)){
	echo $done_test;
	return true;
}

echo '
<div class="bg" style="width: 99%">
<center class="tit">
<i class="icon icon-storage icon-head"></i>&nbsp; '.$l['plans_tit'].'<span style="float:right"><a href="javascript:showsearch();"><img src="'.$theme['images'].'admin/search.gif" /></a></span></center>';

error_handle($error);

// Check which stid belongs to which servers
foreach($storage_servers as $p => $q){
	if($q['serid'] == -1){
		$ipgp[$q['stid']][] = $l['all_servers'];
	}elseif($q['serid'] == -2){
		$ipgp[$q['stid']][] = '[Group] '.$servergroups[$q['sgid']]['sg_name'];
	}else{
		foreach($servers as $x => $y){
			if($q['serid'] == $y['serid'] || $q['sgid'] == $y['sgid']){
				$ipgp[$q['stid']][] = $y['server_name'];
			}
		}
	}
}

echo '<script language="javascript" type="text/javascript"><!-- // --><![CDATA[
function Delstorage(stid){

	stid = stid || 0;
	
	// List of ids to delete
	var storage_list = new Array();
	
	if(stid < 1){
		
		if($("#storage_task_select").val() != 1){
			alert("'.$l['no_action'].'");
			return false;
		}
		
		$(".ios:checked").each(function() {
			storage_list.push($(this).val());
		});
		
	}else{
		
		storage_list.push(stid);
		
	}
	
	if(storage_list.length < 1){
		alert("'.$l['nothing_selected'].'");
		return false;
	}
	
	var storage_conf = confirm("'.$l['del_conf'].'");
	if(storage_conf == false){
		return false;
	}
	
	var finalData = new Object();
	finalData["delete"] = storage_list.join(",");
	
	//alert(finalData);
	//return false;
	
	$("#progress_bar").show();
	
	$.ajax({
		type: "POST",
		url: "'.$globals['index'].'act=storage&api=json",
		data : finalData,
		dataType : "json",
		success: function(data){
			$("#progress_bar").hide();
			if("done" in data){
				alert("'.$l['action_completed'].'");
			}
			if("error" in data){
				alert(data["error"]);
			}
			
			if("fatal_error_text" in data){
				alert(data["fatal_error_text"]);
			}
			location.reload(true);
		},
		error: function(data) {
			$("#progress_bar").hide();
			//alert(data.description);
			return false;
		}
	});
	
	return false;
};

// ]]></script>

<div id="showsearch" style="display:'.(optREQ('search') || (!empty($storage) && !empty($globals['showsearch'])) ? "" : "none").';">
<form accept-charset="'.$globals['charset'].'" name="storage" method="GET" action="" class="form-horizontal">
<input type="hidden" name="act" value="storage">
		
<div class="form-group_head">
  <div class="row">
  	<div class="col-sm-2"></div>
    <div class="col-sm-1"><label>'.$l['sbyname'].'</label></div>
    <div class="col-sm-2"><input type="text" class="form-control" name="name" id="name" size="30" value="'.POSTval('name','').'"/></div>
    <div class="col-sm-1"><label>'.$l['sbypath'].'</label></div>
    <div class="col-sm-2"><input type="text" class="form-control" name="path" id="path" size="30" value="'.POSTval('path', '').'" /></div>
    <div class="col-sm-2" style="text-align: center;"><button type="submit" name="search" class="go_btn" value="Search"/>'.$l['submit'].'</button></div>
  	<div class="col-sm-2"></div>
  </div>
</div>
</form>
<br />
<br />
</div>';

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['saved'].'</div>';
}

if(empty($storage)){

	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.(optREQ('search') ? $l['no_res'] : $l['no_backupservers']).'</div>';
	
}else{

page_links($globals['num_res'], $globals['cur_page'], $globals['reslen']);
echo '<br /><br />
<form accept-charset="'.$globals['charset'].'" name="multi_storage" id="multi_storage" method="post" action="" class="form-horizontal">
<table class="table table-hover tablesorter">
<tr>
	<th align="center" width="2%">'.$l['id'].'</th>
	<th align="center" width="25%">'.$l['name'].'</th>
	<th align="center">'.$l['st_servers'].'</th>
	<th align="center">'.$l['st_uuid'].'</th>
	<th align="center">'.$l['st_type'].'</th>
	<th align="center">'.$l['st_path'].'</th>
	<th align="center">'.$l['st_primary'].'</th>
	<th align="center">'.$l['st_size'].'</th>
	<th align="center">'.$l['st_free'].'</th>
	<th align="center">'.$l['st_oversell'].'</th>
	<th align="center">'.$l['st_alert_threshold'].'</th>
	<th colspan="2">'.$l['manage'].'</th>
	<th><input type="checkbox" class="select_all" name="select_all" id="select_all"></th>
</tr>';
$i = 0;
	foreach($storage as $k => $v){
echo '<tr>
	<td align="left">'.$v['stid'].'</td>
	<td>'.$v['name'].'</td>
	<td>'.@implode(", ", $ipgp[$v['stid']]).'</td>
	<td>'.$v['st_uuid'].'</td>
	<td>'.storage_type_text($v['type']).'</td>
	<td>'.$v['path'].'</td>
	<td align="center">'.(empty($v['primary_storage']) ? $l['no'] : $l['yes']).'</td>
	<td align="center">'.$v['size'].' GB</td>
	<td align="center">'.$v['free'].' GB</td>
	<td align="center">'.$v['oversell'].' GB</td>
	<td align="center">'.$v['alert_threshold'].'%</td>
	<td align="center"><a href="?act=editstorage&stid='.$v['stid'].'" title="Edit"><img src="'.$theme['images'].'admin/edit.png" /></a></td>
	<td align="center"><a href="javascript:void(0);" onclick="return Delstorage('.$k.');" title="Delete"><img src="'.$theme['images'].'admin/delete.png" /></td>
	<td width="20" valign="top" align="center">
		<input type="checkbox" class="ios" name="storage_list[]" value="'.$k.'"/>
	</td>
	</tr>';
	$i++;
	}
echo '</table>


<div class="row bottom-menu">
		
	<div class="col-sm-7"></div>
	<div class="col-sm-5"><label>'.$l['with_selected'].'</label>
		<select name="storage_task_select" id="storage_task_select" class="form-control">
			<option value="0">---</option>
			<option value="1">'.$l['ms_delete'].'</option>
		</select>&nbsp;
		<input type="submit" id ="storage_submit" class="go_btn" name="storage_submit" value="'.$l['go'].'" onclick="Delstorage(); return false;">
	</div>
</div>
</form>

<div id="progress_bar" style="height:125px; display:none">
	<br />
	<center>
		<font id="progress_txt" size="4" color="#222222">'.$l['action_msg'].'</font>
		<br>
		<br>
	</center>
	<table id="table_progress" width="500" height="28" cellspacing="0" cellpadding="0" border="0" align="center" style="border:1px solid #CCC; -moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px;background-color:#efefef;">
		<tbody>
			<tr>
				<td id="progress_color" width="100%" style="background-image: url(themes/default/images/bar.gif); -moz-border-radius: 4px; -webkit-border-radius: 4px; border-radius: 4px;"></td>
				<td id="progress_nocolor"> </td>
			</tr>
		</tbody>
	</table>
	<br>
	<center>
		'.$l['notify_msg'].'
	</center>
</div>

<br />
<center><input type="button" onclick="window.location.href=\''.$globals['ind'].'act=addstorage\'" class="link_btn" value="'.$l['add_storage'].'"/></center></div>';

}

page_links($globals['num_res'], $globals['cur_page'], $globals['reslen']);

echo '</div>';
softfooter();

}
?>
