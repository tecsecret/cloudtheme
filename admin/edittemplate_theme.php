<?php

//////////////////////////////////////////////////////////////
//===========================================================
// edittemplate_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function edittemplate_theme(){

global $theme, $globals, $kernel, $user, $l, $oslist, $error, $theos, $done, $mgs, $distros;

softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-ostemplates icon-head"></i>'.$l['tit_ostmp'].'</center>';

error_handle($error);

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
}

echo '<script language="javascript" type="text/javascript">

function validurl(url){
	var tmp = url.replace(/^.*[\/\\\\]/g, \'\');
	choosetemplate($_("template").value);
};

booted = 0;

function changeplan(plan){	
	
	if('.$GLOBALS['precreated'].' && booted) return;
	
	$_("openvz").className = "";
	$_("xen").className = "";
	$_("xenhvm").className = "";
	$_("kvm").className = "";
	$_("xcp").className = "";
	$_("xcphvm").className = "";
	$_("lxc").className = "";
	$_("vzo").className = "";
	$_("vzk").className = "";
	$_("proxo").className = "";
	$_("proxl").className = "";
	$_("proxk").className = "";
	$_(plan).className = "server_over";
	$("#"+plan).attr("selected","selected");
	$_("cplan").value = plan;
	
	$_("openvzos").style.display = "none";
	$_("xenos").style.display = "none";
	$_("xenhvmos").style.display = "none";
	$_("xcpos").style.display = "none";
	$_("xcphvmos").style.display = "none";
	$_("kvmos").style.display = "none";
	$_("lxcos").style.display = "none";
	$_("vzoos").style.display = "none";
	$_("vzkos").style.display = "none";
	$_("proxoos").style.display = "none";
	$_("proxlos").style.display = "none";
	$_("proxkos").style.display = "none";
	$_(plan+"os").style.display = "";
	
	$_("pygrub").style.display = "none";
	$_("extrac").style.display = "none";
	$_("drive").style.display = "none";
	$_("fstype").style.display = "none";
	$_("hvm_pass").style.display = "none";
	
	$_("noresizefs").style.display = "none";
	$_("perf_ops").style.display = "none";
	$_("template_admin_name").style.display = "none";
	
	if(plan == "xen"){
		$_("pygrub").style.display = "";
		$_("extrac").style.display = "";
		$_("drive").style.display = "";
		$_("fstype").style.display = "";
	}else if(plan == "xenhvm" || plan == "kvm" || plan == "vzk" || plan == "proxk"){
		$_("extrac").style.display = "";
		$_("noresizefs").style.display = "";
		$_("perf_ops").style.display = "";
		$_("template_admin_name").style.display = "";
	}else if(plan == "xcp" || plan == "xcphvm"){
		$_("noresizefs").style.display = "";
		$_("perf_ops").style.display = "";
		$_("template_admin_name").style.display = "";
	}
	
	password_visible(plan,"");
	
	$("#mgs option").each(function(){
		if($(this).attr("type") != plan){
			$(this).attr("disabled","disabled");
		}else{
			$(this).attr("disabled", false);
		}
	});
	
};

function choosetemplate(template, dontdo){
	
	if('.$GLOBALS['precreated'].' && booted) return;
	
	$_("hvm_pass").style.display = "none";
		
	if (template != ""){
		$_("template").value = template;
	}
	
	password_visible("",template);
	
	if(dontdo == 1){
		return;
	}
	
	var tmp = $_("fileurl").value.replace(/^.*[\/\\\\]/g, \'\');
	if(tmp.match("^"+template)){
		$_("base").value = tmp;
	}else{
		$_("base").value = template+"-"+tmp;
	}
};

function password_visible(plan,template){
	
	if(plan == ""){
		plan = $("select#virt_dd").attr("value");
	}
	if(template == ""){
		template = $("#template option:selected").attr("id");
	}
	if((template == "temp_others" || template == "others" || template == "windows" || template == "temp_windows") && (plan == "xcphvm" || plan == "xenhvm" || plan == "kvm" || plan == "vzk")){
		$_("hvm_pass").style.display = "";
	}
	
}

addonload("choosetemplate(\''.POSTval('template', $theos['distro']).'\', 1);");
addonload("changeplan(\''.POSTval('cplan', $theos['type']).'\');");
addonload("booted = 1;");

</script>';
	
	echo '
<form accept-charset="'.$globals['charset'].'" id="edittemplate" name="edittemplate" method="post" action="" class="form-horizontal">

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['type'].'</label>
	</div>
	<div class="col-sm-6">
		<input type="hidden" id="cplan" name="cplan" value="">
		<select disabled="disabled" class="form-control" name="virt_dd" id="virt_dd" onchange="changeplan($(\'select#virt_dd\').attr(\'value\'));">
			<option value="openvz" id="openvz">Openvz</option>
			<option value="xen" id="xen">XEN</option>
			<option value="xenhvm" id="xenhvm">XEN-HVM</option>
			<option value="kvm" id="kvm">KVM</option>
			<option value="xcp" id="xcp">XCP</option>
			<option value="xcphvm" id="xcphvm">XCP-HVM</option>
			<option value="lxc" id="lxc">LXC</option>
			<option value="vzo" id="vzo">Virtuozzo Openvz</option>
			<option value="vzk" id="vzk">Virtuozzo KVM</option>
			<option value="proxo" id="proxo">Proxmox OpenVZ</option>
			<option value="proxl" id="proxl">Proxmox LXC</option>
			<option value="proxk" id="proxk">Proxmox QEMU / KVM</option>
		</select>
		<span class="help-block"></span>
	</div>
</div>
		
<div class="row">
	<div class="col-sm-6"><label class="control-label">OS Template</label></div>
	<div class="col-sm-6">
	<select id="template" class="form-control" name="template" onchange="choosetemplate(this.value);" >';
	foreach($distros as $k => $v){
		echo '<option id="temp_'.$k.'" value="'.$k.'">'.$v['name'].'</option>';
	}
	echo '<option id="temp_others" value="others">Others</option>
	</select>
	<span class="help-block"></span>
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['url'].'</label><br />
		<span class="help-block">'.$l['url_exp'].'</span>
	</div>
	<div class="col-sm-6"><input type="text" class="form-control" name="url" id="fileurl" size="40" onblur=validurl(this.value) value="'.($GLOBALS['precreated'] ? $theos['url'] : POSTval('url', $theos['url'])).'" '.($GLOBALS['precreated'] ? 'disabled="disabled"' : '').' /></div>
</div>

<div class="row">
	<div class="col-sm-6"><label class="control-label">'.$l['bname'].'</label><span class="help-block">&nbsp;</span></div>
	<div class="col-sm-6">
		<div class="row">
			<div style="float: left; padding: 6px 0px 0px 15px; font-size: 15px;">
				<span id="openvzos">'.$globals['openvzos'].'</span>
				<span id="xenos">'.$globals['xenos'].'</span>
				<span id="xenhvmos">'.$globals['xenos'].'</span>
				<span id="xcpos">'.$globals['xcpos'].'</span>
				<span id="xcphvmos">'.$globals['xcpos'].'</span>
				<span id="kvmos">'.$globals['kvmos'].'</span>
				<span id="lxcos">'.$globals['lxcos'].'</span>
				<span id="vzoos">'.$globals['vzoos'].'</span>
				<span id="vzkos">'.$globals['vzkos'].'</span>
				<span id="proxoos">'.$globals['proxoos'].'</span>
				<span id="proxlos">'.$globals['proxlos'].'</span>
				<span id="proxkos">'.$globals['proxkos'].'</span>
				/
			</div>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="filename" id="base" size="20" value="'.($GLOBALS['precreated'] ? $theos['filename'] : POSTval('filename', $theos['filename'])).'" '.($GLOBALS['precreated'] ? 'disabled="disabled"' : '').'/>
			</div>
		</div>
		<span class="help-block"></span>
	</div>
</div>
<div class="row" id="template_admin_name">
	<div class="col-sm-6">
		<label class="control-label">'.$l['template_admin_name'].'</label><br />
		<span class="help-block">'.$l['template_admin_name_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="template_admin_name"  size="40" value="'.($GLOBALS['precreated'] ? $theos['template_admin_name'] : POSTval('template_admin_name', $theos['template_admin_name'])).'" '.($GLOBALS['precreated'] ? 'disabled="disabled"' : '').'/>
	</div>
</div>
<div class="row" id="hvm_pass">
	<div class="col-sm-6"><label class="control-label">'.$l['hvm_pass'].'</label><span class="help-block">&nbsp;</span></div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="hvm_pass" value="'.POSTval('hvm_pass', @$theos['hvm_pass']).'"/>
		<span class="help-block"></span>
	</div>
</div>
<div class="row" id="pygrub">
	<div class="col-sm-6">
		<label class="control-label">'.$l['pygrub'].'</label>
		<span class="help-block">&nbsp;</span>
	</div>
	<div class="col-sm-6"><input type="checkbox" class="ios" name="pygrub" '.($GLOBALS['precreated'] ? 'disabled="disabled" '.(@$theos['pygrub'] ? 'checked="checked"' : '') : POSTchecked('pygrub', @$theos['pygrub'])).' /></div>
</div>
<div class="row" id="fstype">
	<div class="col-sm-6">
		<label class="control-label">'.$l['fstype'].'</label><br />
		<span class="help-block">'.$l['fstype_exp'].'</span>
	</div>
	<div class="col-sm-6"><input type="checkbox" class="ios" name="fstype" '.($GLOBALS['precreated'] ? 'disabled="disabled" '.($theos['fstype'] == 'ext4' ? 'checked="checked"' : '') : POSTchecked('fstype', $theos['fstype'] == 'ext4')).'/></div>
</div>
<div class="row" id="drive">	
	<div class="col-sm-6">
		<label class="control-label">'.$l['drive'].'</label><br />
		<span class="help-block">'.$l['drive_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="drive" id="driveid" value="'.($GLOBALS['precreated'] ? $theos['drive'] : POSTval('drive', @$theos['drive'])).'"  '.($GLOBALS['precreated'] ? 'disabled="disabled"' : '').' />
	</div>
</div>

<div class="row" id="noresizefs">
	<div class="col-sm-6"><label class="control-label">'.$l['noresizefs'].'</label><span class="help-block">&nbsp;</span></div>
	<div class="col-sm-6">
		<input type="checkbox" class="ios" name="noresizefs" '.($GLOBALS['precreated'] ? 'disabled="disabled" '.(@$theos['noresizefs'] ? 'checked="checked"' : '') : POSTchecked('noresizefs', @$theos['noresizefs'])).' />
		<span class="help-block"></span>
	</div>
</div>

<div class="row" id="perf_ops">
	<div class="col-sm-6">
		<label class="control-label">'.$l['perf_ops'].'</label><br />
		<span class="help-block">'.$l['perf_ops_exp'].'</span>
	</div>
	<div class="col-sm-6"><input type="checkbox" class="ios" name="perf_ops" '.($GLOBALS['precreated'] ? 'disabled="disabled" '.(@$theos['perf_ops'] ? 'checked="checked"' : '') : POSTchecked('perf_ops', @$theos['perf_ops'])).' /></div>
</div>

<div class="row" id="extrac">
	<div class="col-sm-6" valign="top"><label class="control-label">'.$l['extra'].'</label><span class="help-block">&nbsp;</span></div>
	<div class="col-sm-6">
		<textarea class="form-control" name="extra" rows="3" cols="30" '.($GLOBALS['precreated'] ? 'disabled="disabled"' : '').'>'.($GLOBALS['precreated'] ? @$theos['extra'] : POSTval('extra', @$theos['extra'])).'</textarea>
		<span class="help-block"></span>
	</div>
</div>

<div class="row">
	<div class="col-sm-6" valign="top"><label class="control-label">'.$l['media_groups'].'</label></div>
	<div class="col-sm-6" valign="top">
		<select class="form-control" name="mgs[]" multiple="multiple" id="mgs">';
			
			foreach($mgs as $mk => $mv){
				echo '<option value="'.$mk.'" type="'.$mv['mg_type'].'" '.(in_array($mk, (empty($_POST['edittemplate']) ? $theos['mg'] : $_POST['mgs'])) ? 'selected="selected"' : '').'>'.$mv['mg_name'].'</option>';
			}
			
		echo '</select>
		<span class="help-block"></span>
	</div>
</div>
<br />
<br />

<center><input type="submit" name="edittemplate" value="'.$l['submit'].'" class="btn"></center>

</form>
</div>
';

softfooter();

}

?>