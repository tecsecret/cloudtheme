<?php

//////////////////////////////////////////////////////////////
//===========================================================
// backup_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function backup_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $done, $vps_name, $vpslist;

softheader($l['<title>']);

echo '
<div class="bg" style="width:99%">
<center class="tit"><i class="icon icon-databackup"></i> &nbsp;'.$l['page_head'].'</center>';

error_handle($error);

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.(optREQ('sbackup') ? $l['done'] : $l['done_restore']).'</div>';
}

echo '<script language="javascript" type="text/javascript"><!-- // --><![CDATA[
function conf_back(){
	return confirm("'.$l['conf'].'");
};


// ]]></script>


<form accept-charset="'.$globals['charset'].'" action="" method="post" name="backup" class="form-horizontal">
<table align="center" cellpadding="5" cellspacing="1" border="0" width="100%" class="table table-hover tablesorter">
<tr>
	<th align="center">'.$l['vpsid'].'</td>
	<th align="center">'.$l['vpsname'].'</td>
	<th align="center"><input type="checkbox" class="select_all" id="select_all"/> </td>
</tr>';

foreach($vpslist as $k => $v){
		
			echo'<tr>
			<td align="center">'.$v['vpsid'].'</td>
			<td align="center">'.$v['vps_name'].'</td>
			<td align="left"><center><input type="checkbox" class="ios" name="selectvps[]" value="'.$v['vpsid'].'" /></center></td>
			</tr>';
}
echo '</table><br /><br />
<center><input type="submit" name="sbackup" value="'.$l['sbackup'].'" class="btn">
<input type="submit" name="backuprestore" value="'.$l['backuprestore'].'" class="btn"></center>
</form>
</div>';


softfooter();

}

?>