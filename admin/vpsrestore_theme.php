<?php

//////////////////////////////////////////////////////////////
//===========================================================
// vpsretore_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 2.1.9
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Suyash
// Date:       25th July 2017
// Time:       15:42 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function vpsrestore_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $cluster, $backup_servers, $vpses, $storages;
	
softheader($l['<title>']);


echo '
<div class="bg">
<center class="tit"><i class="icon icon-databackup icon-head"></i>&nbsp; '.$l['<title>'].'</center>';

error_handle($error);

// Is it offline ?
$hypervisor_status = $cluster->statewise($globals['server']);
if($hypervisor_status == 0 || $hypervisor_status == 2){

	echo '<div class="e_notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['server_status_'.$hypervisor_status].'</div>';

}else{

	echo '<script language="javascript" type="text/javascript">

	$(document).ready(function() {
		
		$("#vps").change(function() {
			$("#row_dir, #row_other_dir, #row_date, #no_backups, #backup_list").hide();
			
			$("#bid").val(-1);
			
			if($(this).val() == 0) {
				return;
			}
			
			$("#op").val("get_vps");
			$("#restore_form").trigger("submit");
		});
		
		$("#bid").change(function() {
			$("#row_dir, #row_other_dir, #row_date, #no_backups, #backup_list").hide();
			
			$("#vps").val(0);
			
			if($(this).val() == -1) {
				return;
			}
			
			$("#op").val("get_dirs");
			$("#restore_form").trigger("submit");
		});
		
		$("#dir").change(function() {
			$("#row_other_dir, #row_date, #no_backups, #backup_list").hide();
			
			$("#vps").val(0);
			
			if($(this).val() == 0) {
				return;
			}
			
			if($(this).val() == "other") {
				$("#row_other_dir").show();
				return;
			}
			
			$("#op").val("get_backups");
			$("#restore_form").trigger("submit");
		});
		
		$("#scan").click(function() {
			$("#row_date, #no_backups, #backup_list").hide();
			
			$("#vps").val(0);
			
			if(!$("#other_dir:visible").val()) {
				alert("'.$l['err_invalid_dir'].'");
				return false;
			}
			
			$("#op").val("get_backups");
			$("#restore_form").trigger("submit");
		});
		
		$("#other_dir").keyup(function (e) {
			// Enter key pressed
			if (e.which == 13) {
				$("#scan").trigger("click");
				return false;
			}
		});
		
		$("#date").change(function() {
			$("#no_backups, #backup_list").hide();
			
			$("#op").val("get_backups");
			$("#restore_form").trigger("submit");
		});
		
		$("#restore_form").submit(function(event) {
			$("#progress_bar").show();
			
			// Set the directory
			var dir = $("#other_dir:visible").val() || $("#dir:visible").val();
			$("#dir_input").val(dir);
			
			$.ajax({
				type: "POST",
				url: "'.$globals['index'].'act=vpsrestore&api=json",
				data: $(this).serialize(),
				dataType : "json",
				success: function(data){
					
					if("error" in data) {
						var err = "";
						$.each(data["error"], function(key, value) {
							err += value + "\n\n";
						});
						alert(err);
						return false;
					}
					
					var op = $("#op").val();
					
					if(op == "get_dirs" || op == "get_vps") {
						$("#dir").empty();
						
						$("<option />")
								.val(0)
								.text("'.$l['select'].'")
								.appendTo("#dir");
						
						$.each(data["directories"], function(key, dir) {
							$("<option />")
								.val(dir)
								.text(dir)
								.appendTo("#dir");
						});
						
						$("<option />")
								.val("other")
								.text("'.$l['other'].'")
								.appendTo("#dir");
								
						if($("#dir option").length == 2) {
							$("#dir").val("other").trigger("change");
						}
						
						$("#row_dir").show();
					}
					
					if(op == "get_backups" || op == "get_vps") {
						var selected = $("#date").val();
						
						$("#date").empty();
						
						var backups_list = [];
						
						if(!empty(data["backup_list"])) {
							$.each(data["backup_list"], function(key, value) {
								var date_str = key.substr(0, 4) + "/" + key.substr(4, 2) + "/" + key.substr(6, 2);
								
								$("<option />")
									.val(key)
									.text(date_str)
									.appendTo("#date");
								
								if(!empty(value)) {
									selected = key;
									backups_list = value;
								}
							});
						}
						
						// Sort by date desc
						var sorted = $("#date option").sort(function(a, b) { return b.value - a.value });
						$("#date").html(sorted);
						
						if(!empty(selected)) {
							$("#date").val(selected);
						}
						
						if($("#date option").length > 0) {
							$("#row_date").show();
						}
						
						$("#table_backup_list tbody").empty();
						
						if(!empty(backups_list)) {
							populate_table(backups_list);
						}
						
						refresh_note();
					}
					
					if(op == "get_vps") {
						$("#bid").val(data["vps_backup_server"]);
						$("#dir").val(data["vps_backup_dir"]);
					}
					
				},
				complete: function(data) {
					$("#progress_bar").hide();
				}
			});
			
			return false;
		});
		
		refresh_tasks();
		
		// Refresh tasks every 10 secs
		setInterval(refresh_tasks, 10000);
		
	});
		
	function populate_table(backups) {
		$.each(backups, function(key, value) {
			
			var file = value["abs_path"].split("/").pop();
			
			var matches = file.match(/^(\d+)/i);
			var vpsid = matches[1];
			// var vpsid = file.substr(0, file.indexOf("."));
			
			var size = Math.round(value["size"] / 1024 / 1024) + " M";
			
			$temp = $(\'<tr><td align="center" class="td_name"></td> \
				<td align="center" class="td_size"></td> \
				<td align="center"><a href="#" onclick="return restore(this, 0)" title="'.$l['restore'].'"><img src="themes/default/images/admin/import.gif" width="24"></a></td> \
				<td align="center"><a href="#" onclick="return restore_details(this)" title="'.$l['restore'].'"><img src="themes/default/images/admin/import.gif" width="24"></a></td> \
				<td align="center"><a href="#" onclick="return Delbackup(this)" title="'.$l['delete'].'"><img src="themes/default/images/admin/delete.png"></a></td> \
				<td width="20" valign="middle" align="center"><input class="ios" name="vpsbackup_list[]" value="2" type="checkbox"></td> \
			</tr>\');
			
			$temp.attr("data-file", file);
			$temp.attr("data-vpsid", vpsid);
			$temp.find(".td_name").text(file);
			$temp.find(".td_size").text(size);
			
			$temp.appendTo("#table_backup_list tbody");
			
		});
	}
	
	function restore_new() {
		
		if(empty($("#hostname").val())) {
			alert("'.$l['no_hostname'].'");
			return false;
		}
		
		if(empty($("#uid").val())) {
			alert("'.$l['no_uid'].'");
			return false;
		}
		
		$("#progress_bar2").show();
		
		$.ajax({
			type: "POST",
			url: "'.$globals['index'].'act=vpsrestore&api=json",
			data: $("#restore_new_form").serialize(),
			dataType : "json",
			success: function(data){
				if("restore_done" in data) {
					alert("'.$l['done_restore'].'");
					refresh_tasks();
				}
				
				if("error" in data) {
					var err = "";
					$.each(data["error"], function(key, value) {
						err += value + "\n\n";
					});
					alert(err);
					return false;
				}
				
				$("#logs_modal").modal("hide");
			},
			complete: function(data) {
				$("#progress_bar2").hide();
			}
		});
		
		return false;
		
	}

	function restore_details(el){
		$("#progress_bar").show();
		
		var vpsid = $(el).closest("tr").data("vpsid");
		var bid = $("#bid").val();
		var dir = $("#other_dir:visible").val() || $("#dir:visible").val();
		var date = $("#date").val();
		
		$.ajax({
			type: "POST",
			url: "'.$globals['index'].'act=vpsrestore&api=json",
			data: {
				restore_details: vpsid,
				bid: bid,
				dir: dir,
				date: date,
			},
			dataType : "json",
			success: function(data){
				if("error" in data) {
					var err = "";
					$.each(data["error"], function(key, value) {
						err += value + "\n\n";
					});
					alert(err);
					return false;
				}
				
				var restore_details = data["restore_details"];
				
				// Show the restore dialog
				var $modal_form = $(\'<form id="restore_new_form" action="" method="post" onsubmit="return restore_new()"> \
									<input type="hidden" id="restore" name="restore" value="1" /> \
									<input type="hidden" id="vpsid" name="vpsid" /> \
									<input type="hidden" id="bid2" name="bid" /> \
									<input type="hidden" id="dir2" name="dir" /> \
									<input type="hidden" id="date2" name="date" /> \
									<input type="hidden" id="newvps" name="newvps" value="1" /> \
									<div class="row"> \
										<div class="col-sm-6"> \
											<label class="control-label">'.$l['hostname'].'</label><br> \
											<span class="help-block">'.$l['hostname_exp'].'</span> \
										</div> \
										<div class="col-sm-6"> \
											<input type="text" class="form-control" name="hostname" id="hostname" size="30" value=""> \
										</div> \
									</div> \
									<div class="row"> \
										<div class="col-sm-6"> \
											<label class="control-label">'.$l['select_user'].'</label><br> \
											<span class="help-block">'.$l['select_user_exp'].'</span> \
										</div> \
										<div class="col-sm-6"> \
											<select class="form-control" id="uid" name="uid"> \
												<option value="0">'.$l['select'].'</option> \
											</select> \
										</div> \
									</div> \
									<div class="row"> \
										<div class="col-sm-6"> \
											<label class="control-label">'.$l['select_storage'].'</label><br> \
											<span class="help-block">'.$l['select_storage_exp'].'</span> \
										</div> \
										<div class="col-sm-6" id="disks"></div> \
									</div> \
									<div class="row"> \
										<div class="col-sm-12 text-center"> \
											<input class="btn" value="'.$l['restore'].'" type="submit"> \
										</div> \
									</div>\
								</form> \
								<div id="progress_bar2" style="height:125px; display:none"><br /><center class="col-sm-12"><font id="progress_txt" size="4" color="#222222">'.$l['action_msg'].'</font><br><br><table id="table_progress" width="450" height="28" cellspacing="0" cellpadding="0" border="0" align="center" style="border:1px solid #CCC; -moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px;background-color:#efefef;"><tbody><tr><td id="progress_color" width="100%" style="background-image: url(themes/default/images/bar.gif); -moz-border-radius: 4px; -webkit-border-radius: 4px; border-radius: 4px;"></td><td id="progress_nocolor"> </td></tr></tbody></table><br/>'.$l['notify_msg'].'</center></div>\');
				
				// Set form date
				$modal_form.find("#vpsid").val(vpsid);
				$modal_form.find("#bid2").val(bid);
				$modal_form.find("#dir2").val(dir);
				$modal_form.find("#date2").val(date);
				$modal_form.find("#hostname").val(restore_details["hostname"]);
				
				var storages_row = \'<div class="row"> \
					<div class="col-sm-3"> \
						<label class="control-label disk-number"></label><br> \
						<span class="help-block disk-size"></span> \
					</div> \
					<div class="col-sm-9"> \
						<select class="form-control st_uuid"> \ ';
							foreach($storages as $storage) {
								echo '<option value="'.$storage['st_uuid'].'">'.$storage['name'].'&nbsp;('.$storage['free'].' GB '.(empty($sv['oversell']) ? $l['free'] : $l['oversell_free']).')</option>';
							}
						echo '</select> \
					</div> \
				</div>\';
				
				$.each(restore_details["disks"], function(did, disk) {
					$row = $(storages_row).clone();
					
					$row.find(".disk-number").text("DISK "+(did+1));
					$row.find(".disk-size").text(disk["size"] + " GB");
					$row.find(".st_uuid").val(disk["st_uuid"]).attr("name", "st_uuid_" + did);
					
					$modal_form.find("#disks").append($row);
				});
				
				var $select = $modal_form.find("#uid");
				
				$.each(restore_details["users"], function(uid, email) {
					var $option = $("<option />").val(uid)
						.text(email);
					
					if(uid == restore_details["uid"]) {
						$option.attr("selected", true);
					}
					
					$option.appendTo($select);
				});
				
				// Show dialog for user select
				$("#logs_modal").modal("show");
				$("#logs_modal .fhead").text("'.$l['restore_new'].'");
				$("#logs_modal_body").html($modal_form);
			},
			complete: function(data) {
				$("#progress_bar").hide();
			}
		});
		
		return false;
	}
	
	function restore(el) {
		var vpsid = $(el).closest("tr").data("vpsid");
		var bid = $("#bid").val();
		var dir = $("#other_dir:visible").val() || $("#dir:visible").val();
		var date = $("#date").val();
		
		var backup_conf = confirm("'.$l['conf_res'].'");
		if(backup_conf == false){
			return false;
		}
		
		$("#progress_bar").show();
		
		$.ajax({
			type: "POST",
			url: "'.$globals['index'].'act=vpsrestore&api=json",
			data: {
				restore: 1,
				vpsid: vpsid,
				bid: bid,
				dir: dir,
				date: date
			},
			dataType : "json",
			success: function(data){
				if("restore_done" in data) {
					alert("'.$l['done_restore'].'");
					refresh_tasks();
				}
				
				if("error" in data) {
					var err = "";
					$.each(data["error"], function(key, value) {
						err += value + "\n\n";
					});
					alert(err);
					return false;
				}
			},
			complete: function(data) {
				$("#progress_bar").hide();
			}
		});
		
		return false;
	}
	
	function Delbackup(el){
		
		// List of ids to delete
		var backup_list = new Array();
		
		if(!el){
			
			if($("#backup_task_select").val() != 1){
				alert("'.$l['no_action'].'");
				return false;
			}
			
			$(".ios:checked").each(function() {
				backup_list.push($(this).closest("tr").data("file"));
			});
			
		}else{
			
			backup_list.push($(el).closest("tr").data("file"));
			
		}
		
		if(backup_list.length < 1){
			alert("'.$l['nothing_selected'].'");
			return false;
		}
		
		var backup_conf = confirm("'.$l['del_conf'].'");
		if(backup_conf == false){
			return false;
		}
		
		$("#progress_bar").show();
		
		$.ajax({
			type: "POST",
			url: "'.$globals['index'].'act=vpsrestore&api=json",
			data : {
				delete: backup_list.join(","),
				bid: $("#bid").val(),
				dir: $("#other_dir:visible").val() || $("#dir:visible").val(),
				date: $("#date").val(),
			},
			dataType : "json",
			success: function(data){
				if("delete_done" in data){
					
					// Remove all deleted rows
					$.each(backup_list, function(key, value) {
						$("tr[data-file=\'" + value + "\']").remove();
					});
					
					refresh_note();
					
					alert("'.$l['done_delete'].'");
				}
				
				if("error" in data) {
					var err = "";
					$.each(data["error"], function(key, value) {
						err += value + "\n\n";
					});
					alert(err);
					return false;
				}
			},
			complete: function() {
				$("#progress_bar").hide();
			}
		});
		
		return false;
	}
	
	function refresh_note() {
		// Show note if no backups found
		if($("#table_backup_list tbody tr").length > 0) {
			$("#backup_list").show();
			$(".notice").hide();
		} else {
			$("#backup_list").hide();
			
			$(".notice span").text("'.$l['no_backup_found'].'");
			$(".notice").show();
		}
	}
	
	function refresh_tasks() {
		var cols = new Object();
		cols["actid"] = {"l" : "'.$l['actid'].'", "centered" : true};
		cols["vpsid"] = {"l" : "'.$l['vpsid'].'", "centered" : true};
		cols["user"] = {"l" : "'.$l['user'].'", "centered" : true};
		cols["server"] = {"l" : "'.$l['server'].' '.'('.$l['id'].')", "centered" : true};
		cols["started"] = {"l" : "'.$l['started'].'", width: 100, "centered" : true};
		cols["updated"] = {"l" : "'.$l['updated'].'", width: 100, "centered" : true};
		cols["ended"] = {"l" : "'.$l['ended'].'", width: 100, "centered" : true};
		cols["action_txt"] = {"l" : "'.$l['action'].'", width: 100};
		cols["status_txt"] = {"l" : "'.$l['status'].'", width: 200};
		cols["progress"] = {"l" : "'.$l['progress'].'"};
		cols["logs"] = {"l" : "'.$l['logs'].'", "centered" : true};
		
		$.ajax({
			type: "GET",
			url: "'.$globals['index'].'act=tasks&action=restorevps_plan&api=json&page=0&reslen=20",
			dataType : "json",
			success: function(data){
				
				if(empty(data["tasks"])) {
					// Show no tasks note if table empty
					$("#table_tasks_container").html("<div class=\"notice\"><img src=\"'.$theme['images'].'notice.gif\" /> &nbsp; '.$l['no_tasks'].'</div>");
					return;
				}
				
				var rows = Array();
				
				$.each(data["tasks"], function(k, task) {
					
					rows[k] = new Object();
					rows[k]["actid"] = task["actid"];
					rows[k]["vpsid"] = task["vpsid"];
					rows[k]["user"] = empty(task["uid"]) ? "root" : task["email"];
					rows[k]["server"] = task["server_name"] + "(" + task["serid"] + ")";
					rows[k]["started"] = task["started"];
					rows[k]["updated"] = task["updated"];
					rows[k]["ended"] = task["ended"];
					rows[k]["action_txt"] = task["action_txt"];
					rows[k]["status_txt"] = task["status_txt"];
					rows[k]["progress"] = "<div style=\"width:150px;\" id=\"progress-cont"+task["actid"]+"\"><center><div style=\"font-size:15px;text-align:center;\" id=\"pbar"+task["actid"]+"\">"+task["progress"]+"%</center><div style=\"border-radius:4px;background-color: #ddd;border:1px solid #ccc;\"><div style=\"width:0px;height:18px;background-image: url(themes/default/images/bar.gif); -moz-border-radius: 4px; -webkit-border-radius: 4px; border-radius: 4px;\" id=\"progressbar"+task["actid"]+"\"></div></div></div>";
					rows[k]["logs"] = "<button class=\"btn btn-logs\" onclick=\"loadlogs("+task["actid"]+");\" style=\"margin: 0; padding: 4px 8px; font-size: 12px;\">'.$l['show'].'</button>";
					
				});
				
				rows.sort(function(a, b){
					a = parseInt(a["actid"]);
					b = parseInt(b["actid"]);
					if(a < b) return 1;
					if(a > b) return -1;
					return 0;
				});
				
				table({"id" : "table_tasks_container", "tid" : "table_tasks", "width" : "100%"}, cols, rows);
				
				$.each(data["tasks"], function(index, task) {
					
					if(task["progress"] == 100){
						$("#status"+index).text("'.$l['completed'].'");
						$("#pbar"+index).text("100% '.$l['completed'].' !");
						$("#progressbar"+index).hide();
						$("#progressbar"+index).parent().css("border", "0px");
					}else if(task["status"] == -1){
						$("#status"+index).html(task["status_txt"]);
						$("#pbar"+index).text("'.$l['task_notcomplete'].'");
						$("#progressbar"+index).hide();
						$("#progressbar"+index).parent().css("border", "0px");
					}else {
						
						if(isNaN(task["progress"])){
							$("#pbar"+index).text(task["progress"]);
							$("#progressbar"+index).hide();
						}else{
							make_progress(index, Number(task["progress"]));
							$("#progressbar"+index).show();
						}
					}
					
				});
				
			}
		});
		
	}

	function make_progress(index, progress){
		var steps = 20;
		var start = $("#pbar"+index).text();
		start = (start.slice(0, -1));
		if(isNaN(start)){
			start = 0;
		}
		var step = (progress - start)/steps;
		if(step < 0){ step = 0;}
		var increment = function(){
			var val = $("#pbar"+index).text();
			val = Number(val.slice(0, - 1));
			prog = parseInt(val + Math.round(step));
			//alert("progr"+prog);
			if(prog > progress){ prog = progress;}
			if(prog < parseInt("100")){
				$("#pbar"+index).text(prog + "%");
				$("#progressbar"+index).width(prog + "%");
			}else{
				$("#progressbar"+index).width(100 + "%");
			}
			if (prog < progress){setTimeout(increment, 500);}
		};
		increment();
	}

	</script>
	
	<style type="text/css">

	.panel {
		min-height: 120px;
	}
	.panel-content {
		max-height: 400px;
		overflow: hidden;
	}
	.panel.panel-blue-grey {
		border-color: #607D8B;
	}
	.panel.panel-blue-grey .panel-heading {
		background-color: #607D8B;
		border-color: #607D8B;
		color: #fff;
	}
	.table-dash {
		margin-bottom: 4px;
	}
	.table-dash tbody tr td {
		vertical-align: middle;
	}
	.table-dash tr td:first-child, .table-dash tr th:first-child {
		padding-left: 16px;
	}
	.table-dash tr td:last-child, .table-dash tr th:last-child {
		padding-right: 16px;
	}
	.btn-logs {
		margin: 0;
		padding: 4px 8px;
		font-size: 12px;
	}

	</style>';

	echo '<br />

	<form id="restore_form" name="restore_form">
	
		<input type="hidden" id="op" name="op" value="" />
		<input type="hidden" id="dir_input" name="dir" value="" />
	
		<div class="row" id="row_server">
			<div class="col-sm-3 col-sm-offset-1 text-right">
				<label class="control-label" style="margin-top:8px;">'.$l['select_vps'].'</label>
			</div>
			<div class="col-sm-5">		
				<select class="form-control" name="vpsid" id="vps">
					<option value="0" selected="selected">'.$l['select'].'</option>';
						
					foreach($vpses as $vpsid => $vps) {
						echo '<option value="'.$vpsid.'">[VID: '.$vps['vps_name'].'] [ID: '.$vpsid.'] ['.$vps['hostname'].']</option>';
					}
					
				echo '</select>
			</div>
		</div>
		
		<br />
	
		<div class="row">
			<div class="col-sm-12 text-center">
				<label class="control-label" style="font-size:15px; margin-top:8px;">'.$l['or'].'</label>
			</div>
		</div>
		
		<br />
	
		<div class="row" id="row_server">
			<div class="col-sm-3 col-sm-offset-1 text-right">
				<label class="control-label" style="margin-top:8px;">'.$l['select_server'].'</label>
			</div>
			<div class="col-sm-5">		
				<select class="form-control" name="bid" id="bid">
					<option value="-1" selected="selected">'.$l['select'].'</option>
					<option value="0">'.$l['local'].'</option>';
						
					foreach($backup_servers as $bid => $bs) {
						echo '<option value="'.$bid.'">'.$bs['name'].' ('.$bs['hostname'].')</option>';
					}
					
				echo '</select>
			</div>
		</div>

		<br />

		<div class="row" id="row_dir" style="display:none">
			<div class="col-sm-3 col-sm-offset-1 text-right">
				<label class="control-label" style="margin-top:8px;">'.$l['select_dir'].'</label>
			</div>
			<div class="col-sm-5">		
				<select class="form-control" id="dir">
					<option value="0" selected="selected">'.$l['select'].'</option>
				</select>
			</div>
		</div>

		<br />

		<div class="row" id="row_other_dir" style="display:none">
			<div class="col-sm-5 col-sm-offset-4">
				<div class="input-group">
					<input id="other_dir" type="text" class="form-control">
					<span class="input-group-btn">
						<button id="scan" class="btn btn-default" type="button" style="padding:6px 10px">'.$l['scan'].'</button>
					</span>
				</div>
			</div>
		</div>

		<br />

		<div class="row" id="row_date" style="display:none">
			<div class="col-sm-3 col-sm-offset-1 text-right">
				<label class="control-label" style="margin-top:8px;">'.$l['select_date'].'</label>
			</div>
			<div class="col-sm-5">		
				<select class="form-control" name="date" id="date"></select>
			</div>
		</div>
		
	</form>

	<br /><br />
	
	<div id="backup_list" style="display:none">
		<table id="table_backup_list" class="table table-hover tablesorter" width="90%" cellspacing="1" cellpadding="8" border="0" align="center">
			<thead>
				<tr>
					<th align="center">'.$l['name'].'</th>
					<th width="100" align="center">'.$l['size'].'</th>
					<th width="10" align="center">'.$l['restore'].'</th>
					<th width="10" align="center">'.$l['restore_new'].'</th>
					<th width="10" align="center">'.$l['delete'].'</th>
					<th width="20"><input class="select_all" name="select_all" id="select_all" type="checkbox"></th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
		
		<div class="row bottom-menu">			
			<div class="col-sm-7"></div>
			<div class="col-sm-5">
				<label>'.$l['with_selected'].'</label>
				<select name="backup_task_select" class="form-control" id="backup_task_select">
					<option value="0">---</option>
					<option value="1">'.$l['ms_delete'].'</option>
				</select>&nbsp;
				<button type="submit" class="go_btn" id="backup_submit" name="backup_submit" onclick="Delbackup(); return false;">Go</button>
			</div>
		</div>
	</div>

	<div id="no_backups" class="notice" style="display:none"><img src="'.$theme['images'].'notice.gif" />&nbsp;<span></span></div>

	<div id="progress_bar" style="height:125px; display:none">
		<br />
		<center class="col-sm-12">
			<font id="progress_txt" size="4" color="#222222">'.$l['action_msg'].'</font>
			<br>
			<br>
			<table id="table_progress" width="450" height="28" cellspacing="0" cellpadding="0" border="0" align="center" style="border:1px solid #CCC; -moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px;background-color:#efefef;">
				<tbody>
					<tr>
						<td id="progress_color" width="100%" style="background-image: url(themes/default/images/bar.gif); -moz-border-radius: 4px; -webkit-border-radius: 4px; border-radius: 4px;"></td>
						<td id="progress_nocolor"> </td>
					</tr>
				</tbody>
			</table>
			<br/>'.$l['notify_msg'].'
		</center>
	</div>
	
	<br /><br /><br />

	<div class="row">
		<div class="col-sm-12">
			<div id="panel-tasks" class="panel panel-blue-grey">
				<div class="panel-heading">
					<span>'.$l['list_tasks'].'</span>
					<i class="icon icon-tasks" style="float:right; font-size:20px; opacity:0.5"></i>
				</div>
				<div class="panel-content scrollbar-virt">
					<div id="table_tasks_container"></div>
				</div>
			</div>
		</div>
	</div>
	</div>
	';

}// End of Server Offline if	

echo '</div>';
softfooter();

}

?>