<?php

//////////////////////////////////////////////////////////////
//===========================================================
// resource_pricing_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function resource_pricing_theme(){

global $theme, $servers, $globals, $kernel, $cluster, $user, $l, $error, $done, $pricing, $servergroups, $resource_types, $rate_types;

softheader($l['<title>']);

echo '
<script language="javascript" type="text/javascript">	
var servergroups = '.json_encode($servergroups).';

var ratetypes = '.json_encode($rate_types).';

var lang = {};
lang.h_rate = \''.$l['h_rate'].'\';
lang.m_rate = \''.$l['m_rate'].'\';
lang.y_rate = \''.$l['y_rate'].'\';
lang.bandwidth_h_rate = \''.$l['bandwidth_h_rate'].'\';

</script>';
?>

<script language="javascript" type="text/javascript">

var tmpsgs = {};

// Fill and display modal with available servergroups
function selectregmod(prefix){
	
	if(!(prefix in tmpsgs)){
		tmpsgs[prefix] = {};
	}
	
	var dv = "";
	dv += '<div class="row" id="sglist" style="max-height: 500px; overflow: auto; padding: 5px;">';
	for(var x in servergroups){
		if(x in tmpsgs[prefix]){
			continue;
		}
		dv += '<div class="col-sm-6 selregrowclass">';
			dv += '<div class="row" sgid="'+servergroups[x].sgid+'" onclick="moreregionpricing(\''+prefix+'\', this); $(this).removeAttr(\'onclick\'); $(this).css(\'color\', \'#aeaeae\');">';
				dv += '<div class="col-sm-1">'+servergroups[x].sgid+'</div>';
				dv += '<div class="col-sm-10">'+servergroups[x].sg_name+'</div>';
			dv += '</div>';
		dv += '</div>';
	}
	dv += '</div>';
	
	$("#selectregionforpricing .modal-body").html(dv);
	$("#selectregionforpricing").modal({keyboard: true});
	$("#selectregionforpricing").modal({show:true});
}

// Add selected region (servergroup) pricing row
function moreregionpricing(prefix, selreg, reload){
	
	// Return back if we do not have a valid servergroup to add or if we are not reloading old values back.
	if(reload != 1 && (selreg == "" || $(selreg).attr("sgid") == null)){
		return false;
	}
	
	// Extract sgid
	var rsgid = (reload == 1 ? selreg : $(selreg).attr("sgid"));
	
	if($('#'+prefix+'_h_rate_'+rsgid).length){
		return true;
	}
	
	// Construct div for this sgid:
	var prow = "";
	prow += '<div class="row" style="margin1-bottom: 15px;">';
	prow += '<div class="col-sm-3" style="padding-bottom: 10px;">';
	prow += '<label>'+servergroups[rsgid].sg_name+'</label>';
	prow += '</div>';
	prow += '<div class="col-sm-8">';
	for(var i in ratetypes){
		if(prefix == 'bandwidth' && ratetypes[i] != 'h_rate'){
			continue;
		}
		var txt = prefix == 'bandwidth' ? lang[prefix+'_'+ratetypes[i]] : lang[ratetypes[i]];
		prow += '<div class="col-sm-4" style="padding-bottom: 10px;">';
		prow += '<input type="text" class="form-control" placeholder="'+txt+'" name="'+prefix+'_'+ratetypes[i]+'_'+rsgid+'" id="'+prefix+'_'+ratetypes[i]+'_'+rsgid+'" value="" />';
		prow += '</div>';
	}
	prow += '</div>';
	prow += '<div class="col-sm-1 text-right">';
	prow += '<button type="button" class="btn btn-danger" sgid="" style="background-color: #d9534f; border-color: #d43f3a; padding: 6px 12px;" onclick="removepricing(\''+prefix+'\', this, \''+rsgid+'\')">-</button>';
	prow += '</div>';
	prow += '</div>';
	
	// Append this new div to pricerowcontainer div
	$("#"+prefix+"_pricerowscontainer").append(prow);
	
	if(!(prefix in tmpsgs)){
		tmpsgs[prefix] = {};
	}
	
	// Add this to the group
	tmpsgs[prefix][rsgid] = rsgid;
	
}

// Remove selected region pricing row
function removepricing(prefix, ele, sgid){
	
	// Remove selected pricing region
	$(ele).parent().parent().remove();
	
	// Remove this
	delete tmpsgs[prefix][sgid];
	
}

</script>

<?php

echo '

<style>
#selectregionforpricing .close{
	float: right;
	background-color: red;
	color: white;
	weight: bold;
	cursor: pointer;
	padding: 2px 10px;
	
}
#sglist{
}
.selregrowclass div{
	background-color: #eaeaea;			
	margin: 1px;
	padding: 5px;
	cursor: pointer;
}
.selregrowclass div:active{
	background-color: #a7e2ff;
}
</style>
<form accept-charset="'.$globals['charset'].'" action="" method="post" name="config" class="form-horizontal">

<div class="bg" style="width:99%">
<center class="tit"><i class="icon icon-config icon-head"></i> &nbsp;'.$l['heading'].'</center>';

error_handle($error);

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
}

echo '
<div class="row text-center">
	<div class="col-sm-3"><label>'.$l['regions'].'</label></div>
	<div class="col-sm-8">
		<div class="col-sm-4">
			<label>'.$l['h_rate'].'</label>
		</div>
		<div class="col-sm-4">
			<label>'.$l['m_rate'].'</label>
		</div>
		<div class="col-sm-4">
			<label>'.$l['y_rate'].'</label>
		</div>
	</div>
</div>

<form accept-charset="'.$globals['charset'].'" action="" method="post" name="config" class="form-horizontal">';

foreach($resource_types as $v){

echo '
<div class="roundheader">&nbsp; &nbsp; &nbsp;'.$l[$v.'_pricing'].'</div>
<div><br />
	<div class="row" id="'.$v.'_pricerowscontainer" style="padding-left: 20px; padding-right: 20px;">
		<div class="row" id="pricerow[-1]">
			<div class="col-sm-3" style="padding-bottom: 10px;">
				<label>'.$l['all_reg'].'</label>
			</div>
			<div class="col-sm-8">';
			
			foreach($rate_types as $rv){
				
				if($v == 'bandwidth' && $rv != 'h_rate'){
					continue;
				}
				
				$prefix = ($v == 'bandwidth' ? 'bandwidth_' : '');
				
				echo '
				<div class="col-sm-4" style="padding-bottom: 10px;">
					<input type="text" class="form-control" placeholder="'.$l[$prefix.$rv].'" name="'.$v.'_'.$rv.'_-1" id="'.$v.'_'.$rv.'_-1" value="'.POSTval($v.'_'.$rv.'_-1', '').'" />
				</div>';
			}

echo '			
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12 text-right" >
			<button type="button" class="btn" data-toggle1="modal" data-target1="#selectregionforpricing" onclick="selectregmod(\''.$v.'\');" >'.$l['add_reg'].'</button>
		</div>
	</div>
</div>
<br /><br />

';

}

if(!empty($pricing) && empty($_POST)){
	foreach($pricing as $res => $val){
		foreach($val as $rtype => $sval){
			foreach($sval as $sgid => $v){
				$rates[$res.'_'.$rtype.'_'.$sgid] = $v;
			}
		}
	}
}	

// Are the rates posted ? Then we must add them
if(!empty($_POST)){
	$rates = $_POST;
}

//r_print($rates);

if(!empty($rates)){
	
	echo '<script language="javascript" type="text/javascript">';
	
	foreach($rates as $kk => $vv){
		
		if(!preg_match('/rate/is', $kk)){
			continue;
		}
		
		// If its a regular region
		if(substr($kk, -2) != '-1'){
			$tmp = explode('_', $kk);
			echo '
			moreregionpricing("'.$tmp[0].'", "'.$tmp[3].'", 1);';
		}
		
		echo '
		$("#'.$kk.'").val("'.$rates[$kk].'");';
		
	}
	
	echo '</script>';
	
}

echo '
<div class="modal fade" id="selectregionforpricing" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<div class="modal-title" style="float:left;"><b>'.$l['sel_reg'].'</b></div>
				<div class="close" data-dismiss="modal" >X</div>
			</div>
			<div class="modal-body"></div>
		</div>
	</div>
</div>
	
<center>
<input type="submit" name="editsettings" value="'.$l['saveconfig'].'" class="btn"/>
</center>
<br /><br />
</form>';

echo '</div>';

softfooter();

}



