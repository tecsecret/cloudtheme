<?php

//////////////////////////////////////////////////////////////
//===========================================================
// Rebuild_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.7
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       May 2011
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function rebuild_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $cluster, $servers, $oslist, $done, $vpses;
global $servers, $isos, $virt, $hvm, $newvs, $vps, $vnc;
global $flag_rebuildvps, $actid, $done_data;

if($flag_rebuildvps){
	echo $actid;
	$flag_rebuildvps = 0;
	
	return false;
}

if(optGET('done')){
		
		$done = $done_data['newvs']['vpsid'];
		$newvs = $done_data['newvs'];
		$vpsname = $done_data['vps']['vps_name'];
		$vnc = $done_data['vnc'];
		$ips = $done_data['vps']['ips'];
		$ipsv6 = $done_data['vps']['ips6'];
		$ipsv6_subnet = $done_data['vps']['ips6_subnet'];
						  
	   echo '<div class="bg"><div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
	
	echo '<div style="background-color: #FAFBD9;">
	<div class="row">
		<div class="col-sm-12" style="padding:5px">
			<div class="col-sm-2"></div>
			<div class="col-sm-4">
				<span class="fhead">'.$l['newvs_vpsid'].'</span>
			</div>
			<div class="col-sm-4">'.$done.'</div>
			<div class="col-sm-2"></div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12" style="padding:5px">
			<div class="col-sm-2"></div>
			<div class="col-sm-4">
				<span class="fhead">'.$l['newvs_vps_name'].'</span>
			</div>
			<div class="col-sm-4">'.$vpsname.'</div>
			<div class="col-sm-2"></div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12" style="padding:5px">
			<div class="col-sm-2"></div>
			<div class="col-sm-4">
				<span class="fhead">'.$l['newvs_rootpass'].'</span>
			</div>
			<div class="col-sm-4" id="newvs_rootpass">'.$newvs['pass'].'</div>
			<div class="col-sm-2"></div>
		</div>
	</div>';

	if(!empty($vnc)){

		echo '<div class="row">
			<div class="col-sm-12" style="padding:5px">
				<div class="col-sm-2"></div>
				<div class="col-sm-4">
					<span class="fhead">'.$l['vnc_details'].'</span>
				</div>
				<div class="col-sm-4">'.$vnc['ip'].':'.$vnc['port'].'</div>
				<div class="col-sm-2"></div>
			</div>
		</div>';
	
		if(!empty($vnc['password'])){
			echo '
			<div class="row">
				<div class="col-sm-12" style="padding:5px">
					<div class="col-sm-2"></div>
					<div class="col-sm-4">
						<span class="fhead">'.$l['vnc_pass'].'</span>
					</div>
					<div class="col-sm-4">'.$vnc['password'].'</div>
					<div class="col-sm-2"></div>
				</div>
			</div>';
		}
	}

	echo '
	<div class="row">
		<div class="col-sm-12" style="padding:5px">
			<div class="col-sm-2"></div>
			<div class="col-sm-4">
				<span class="fhead">'.$l['newvs_ips'].'</span>
			</div>
			<div class="col-sm-4">'.implode(', ', $ips).'</div>
			<div class="col-sm-2"></div>
		</div>
	</div>';

	if(!empty($vps['ipv6'])){

		echo '<div class="row">
			<div class="col-sm-12" style="padding:5px">
				<div class="col-sm-2"></div>
				<div class="col-sm-4">
					<span class="fhead">'.$l['newvs_ips6'].'</span>
				</div>
				<div class="col-sm-4">'.implode(', ', $ips6).'</div>
				<div class="col-sm-2"></div>
			</div>
		</div>';
	}

	if(!empty($ipsv6_subnet)){
		echo '
		<div class="row">
			<div class="col-sm-12" style="padding:5px">
				<div class="col-sm-2"></div>
				<div class="col-sm-4">
					<span class="fhead">'.$l['newvs_ips6_subnet'].'</span>
				</div>
				<div class="col-sm-4">'.implode(', ', $ipsv6_subnet).'</div>
				<div class="col-sm-2"></div>
			</div>
		</div>';
	}

	echo '</div></div>';
		return true;
}
	
softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-vs icon-head"></i>&nbsp; '.$l['add_vs'].'</center>';

error_handle($error);

// Is it offline ?
$hypervisor_status = $cluster->statewise($globals['server']);
if($hypervisor_status == 0 || $hypervisor_status == 2){

	echo '<div class="e_notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['server_status_'.$hypervisor_status].'</div>';

}else{

	if(!empty($done)){
		
		echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
	
		echo '<div style="background-color: #FAFBD9;">
		<div class="row">
			<div class="col-sm-12" style="padding:5px">
				<div class="col-sm-2"></div>
				<div class="col-sm-4">
					<span class="fhead">'.$l['newvs_vpsid'].'</span>
				</div>
				<div class="col-sm-4">'.$done.'</div>
				<div class="col-sm-2"></div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12" style="padding:5px">
				<div class="col-sm-2"></div>
				<div class="col-sm-4">
					<span class="fhead">'.$l['newvs_vps_name'].'</span>
				</div>
				<div class="col-sm-4">'.$vps['vps_name'].'</div>
				<div class="col-sm-2"></div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12" style="padding:5px">
				<div class="col-sm-2"></div>
				<div class="col-sm-4">
					<span class="fhead">'.$l['newvs_rootpass'].'</span>
				</div>
				<div class="col-sm-4">'.$vps['pass'].'</div>
				<div class="col-sm-2"></div>
			</div>
		</div>
		<br />
		<br />';
	
		if(!empty($vnc)){
	
			echo '<div class="row">
				<div class="col-sm-12" style="padding:5px">
					<div class="col-sm-2"></div>
					<div class="col-sm-4">
						<span class="fhead">'.$l['vnc_details'].'</span>
					</div>
					<div class="col-sm-4">'.$vnc['ip'].':'.$vnc['port'].'</div>
					<div class="col-sm-2"></div>
				</div>
			</div>';
		
			if(!empty($vnc['password'])){
				echo '
				<div class="row">
					<div class="col-sm-12" style="padding:5px">
						<div class="col-sm-2"></div>
						<div class="col-sm-4">
							<span class="fhead">'.$l['vnc_pass'].'</span>
						</div>
						<div class="col-sm-4">'.$vnc['password'].'</div>
						<div class="col-sm-2"></div>
					</div>
				</div>';
			}
		}
	
		echo '
		<div class="row">
			<div class="col-sm-12" style="padding:5px">
				<div class="col-sm-2"></div>
				<div class="col-sm-4">
					<span class="fhead">'.$l['newvs_ips'].'</span>
				</div>
				<div class="col-sm-4">'.implode(', ', $vps['ips']).'</div>
				<div class="col-sm-2"></div>
			</div>
		</div>';
	
		if(!empty($vps['ipv6'])){
	
			echo '<div class="row">
				<div class="col-sm-12" style="padding:5px">
					<div class="col-sm-2"></div>
					<div class="col-sm-4">
						<span class="fhead">'.$l['newvs_ips6'].'</span>
					</div>
					<div class="col-sm-4">'.implode(', ', $vps['ipv6']).'</div>
					<div class="col-sm-2"></div>
				</div>
			</div>';
	
		}
	
		echo '</div>';	
		
	}else{
		
			echo '<script language="javascript" type="text/javascript">
			
var actid = null;
var progress = 0;
var progress_update = "";
var timer = null;
var newvs_rootpass="";

function copy_password(){
	
	var conf = prompt("'.$l['copy_pass'].'", $("#newpass").val());
	if (conf == null) {
		return false;
	}
};

$(document).ready(function(){
	progress_onload();
	
	$("#vpsid").select2();
	
	$("#vpsid").on("change", function(){
		showtemplates();
	});
	
	$("#rebuildform").submit(function(){
	
		if(!confirm("'.$l['rebuild_data_lost_warn'].'")){
			return false;		
		}
		
		progress = 0;
		var params = $("#rebuildform").serialize();
		newvs_rootpass = $("#newpass").val();
		
		$.ajax({type: "POST",
		url: "'.$globals['index'].'jsnohf=1&act=rebuild",
		data:params+"&reos=1",
		success:function(response){
					if(!isNaN(response)){
						actid = response;
						timer = setTimeout("get_progress(\'rebuild\')", 500);
					}else{
						$("#softcontent").html(response);
					}
				}
		});
		
		$("#error_box").remove();
		$("#form-container").hide();
		$("#progress-cont").show();
		$("#pbar").html("'.$l['checking_data'].'" + " ( 0% ) ");
		window.scrollTo(0, 0);
		$("#progressbar").progressbar({value: 0});
		return false;
		});
});

function setpwd(size){
	var pwd = rand_pass(size);
	$("#newpass").val(pwd);
	$("#conf").val(pwd);
}

function ishvm(){
	
	/*var current_virt = "'.$virt.'";	
	//alert(current_virt)		
	if(current_virt != "xen" && current_virt != "xcp"){
		return false;
	}*/
	
	var vpsid = $_("vpsid").value;
	if(!vpsid){
		return false;
	}
	
	//var hvm = getAttributeByName($_("vps"+vpsid), "hvm");
	var hvm = $("#vps" + vpsid).attr("hvm");
	//alert(vpsid + " -- " + hvm)
	
	for(var i=0; i<$_("osid").options.length; i++){
		if($_("osid").options[i].value < 1) continue;
		
		if(getAttributeByName($_("osid").options[i], "hvm") == 1 && hvm == true){
			$_("osid").options[i].disabled = false;
		}else if(getAttributeByName($_("osid").options[i], "hvm") != 1 && hvm == false){
			$_("osid").options[i].style.display = "";
		}else{
			$_("osid").options[i].style.display = "none";
		}
	}
	return false;
};

var lang = Array();
lang["bad"] = "'.$l['bad'].'";
lang["good"] = "'.$l['good'].'";
lang["strong"] = "'.$l['strong'].'";
lang["short"] = "'.$l['short'].'";
lang["strength_indicator"] = "'.$l['strength_indicator'].'";

// Match the passwords
function pass_match(){
	var newpass = $("#newpass").val();
	var conf = $("#conf").val();
	if(newpass != conf){
		$("#message").text("'.$l['pass_match'].'");
		$("#message").css("color", "red");
	}else{
		$("#message").text("");
	}
}

// Show the templates
function showtemplates(){
	
	var vpsid= $("#vpsid").val();
	var Virt = $("#vps"+vpsid).attr("virt");
	
	$("select#osid option").each(function() {
		if(($(this).attr("virt") != "undefined") && $(this).attr("virt") == Virt){
			$(this).css("display", "");
		}else{
			$(this).css("display", "none");
		}
		
	});
}
addonload("check_pass_strength();");					
</script>
		 
	<form id="rebuildform" accept-charset="'.$globals['charset'].'" action="" method="post" name="rebuild" class="form-horizontal">
	 <div id="form-container">
		<div class="row">
		 	<div class="col-sm-4">
				<label class="control-label">'.$l['vs_server'].'</label>
				<span class="help-block">'.$l['exp_server'].'</span>
			</div>
			<div class="col-sm-4">
				<span class="val">'.$servers[$globals['server']]['server_name'].'</span> 
				('.$l['vs_ser_id'].': <i>'.$globals['server'].'</i>) 
				<input type="hidden" name="serid" value="'.$globals['server'].'" />
			</div>
			<div class="col-sm-4"></div>				
		</div>
		<div class="row">
			<div class="col-sm-4">
				<label class="control-label">'.$l['vps'].'</label>
				<span class="help-block">'.$l['vs_user_exp'].'</span>
			</div>
			<div class="col-sm-4" id="td_vpsid">
				<div class="">
					<select name="vpsid" id="vpsid" style="width:100%">
						<option value="0" '.(POSTval('vpsid')== 0 ? 'selected="selected"' :($vps['vps_name'] == 0 ? 'selected="selected"' : '')).'>'.$l['vps_id'].'</option>';	
							
					foreach($vpses as $k => $v){
						
						echo '<option value="'.$k.'" '.(POSTval('vpsid') == $k ? 'selected="selected"' : ($vps['vpsid'] == $k ? 'selected="selected"' : '')).' hvm="'.$v['hvm'].'" id="vps'.$k.'" virt="'.$v['virt'].(!empty($v['hvm']) ? 'hvm' : '').'">'.$v['vps_name'].'  -  '.$v['hostname'].'</option>';								
					
					}
					
				echo '</select>
				</div>
			</div>
			<div class="col-sm-4"></div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<label class="control-label">'.$l['vsos'].'</label>
				<span class="help-block">'.$l['vsos_exp'].'</span>
			</div>
			<div class="col-sm-4 server-select-lg">
				<select class="form-control virt-select" id="osid" name="osid" style="width:100%">
					<option value="0" '.(POSTval('osid')== 0 ?  'selected="selected"' :($vps['osid']== 0 ? 'selected="selected"' : '')).'>'.$l['select_os'].'</option>';
					
					foreach($oslist as $_virt => $vvvv){
					
						// Does this server support this $_virt ?
						if(server_virt($globals['server'], $_virt) != $_virt){
							continue;
						}
					
						foreach($oslist[$_virt] as $kk => $vv){		
						
							foreach($vv as $k => $v){
						
								echo '<option value="'.$k.'" '.(POSTval('osid') == $k ? 'selected="selected"' : ($vps['osid'] == $k ? 'selected="selected"' : '')).' '.(!empty($v['hvm']) ? 'hvm="1"' : '').' virt="'.$_virt.(!empty($v['hvm']) ? 'hvm' : '').'">'.(!empty($v['hvm']) ? 'HVM - ' : '').''.$v['name'].'</option>';
							
							}
							
						}
					}
			echo'</select>
			</div>
			<div class="col-sm-4"></div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<label class="control-label">'.$l['new_pass'].'</label>
			</div>
			<div class="col-sm-4">
				<input type="password" class="form-control" name="newpass" onkeyup="check_pass_strength(this.id);" id="newpass" size="30" value="" />
				<div id="newpass_pass-strength-result" class="">'.$l['strength_indicator'].'</div>
				<span class="help-block"></span>
			</div>
			<div class="col-sm-4"><a href="javascript:void(0);" onclick="setpwd(12);check_pass_strength(\'newpass\');pass_match();copy_password();return false;" title="'.$l['randpass'].'"><img src="'.$theme['images'].'randpass.gif" /></a></div>
		</div>
		<div class="row">
			<div class="col-sm-4"><label class="control-label">'.$l['retype_pass'].'</label></div>
			<div class="col-sm-4"><input type="password" class="form-control" onblur="pass_match();" name="conf" id="conf" size="30" value="" /><div id="message"></div></div>
			<div class="col-sm-4"></div>
		</div><br />';
		
		if($kernel->features('format_primary')){
			echo '
			<div class="row">
				<div class="col-sm-4">
					<label class="control-label">'.$l['format_primary'].'</label>
					<span class="help-block">'.$l['format_primary_exp'].'</span>
				</div>
				<div class="col-sm-4">
					<input type="checkbox" class="ios" id="format_primary" name="format_primary" '.POSTchecked('format_primary').'/>
				</div>
				<div class="col-sm-4"></div>
			</div>';
		}
		
		echo '
		<div class="row">
			<div class="col-sm-4">
				<label class="control-label">'.$l['eu_send_rebuild_email'].'</label>
				<span class="help-block">'.$l['eu_send_rebuild_email_exp'].'</span>
			</div>
			<div class="col-sm-4">
				<input type="checkbox" class="ios" id="eu_send_rebuild_email" name="eu_send_rebuild_email" '.POSTchecked('eu_send_rebuild_email', 1).' value="1" />
			</div>
			<div class="col-sm-4"></div>
		</div>
		<br /><br />
		
		<center><input type="submit" class="btn" name="reos"  id="reos" value="'.$l['reinstall'].'"/></center>
		<br /><br />';
		echo '<div class="notebox" align="left" style="font-size:12px; line-height:150%"><img src="'.$theme['images'].'/notice.gif" align=left/> &nbsp; '.$l['format_primary_warn'].'</div>
		</div>
		</form>';
	}

}// End of Server Offline if	


echo '<div id="progress_onload"></div></div>';

softfooter();

}

?>