<?php

//////////////////////////////////////////////////////////////
//===========================================================
// pdns_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function pdns_theme(){

global $theme, $globals, $kernel, $user, $l, $cluster, $error, $servers, $done, $pdns, $done_test, $request_test;

softheader($l['<title>']);

if(!empty($request_test)){
	echo $done_test;
	return true;
}

echo '
<div class="bg" style="width: 99%">
<center class="tit">
<i class="icon icon-pdns icon-head"></i>&nbsp; '.$l['pdns'].'<span style="float:right"><a href="javascript:showsearch();"><img src="'.$theme['images'].'admin/search.gif" /></a></span></center>';

error_handle($error);

echo '<script language="javascript" type="text/javascript"><!-- // --><![CDATA[

function go_click() {

	var selected = $("#pdns_task_select").val();
	
	if(selected == 0){
		alert("'.$l['no_action'].'");
		return false;
	}
	
	if(selected == 1) {
		test_pdns();
	} else if(selected == 2) {
		Delpdns();
	}
}

function test_pdns(pdnsid) {
	
	pdnsid = pdnsid || 0;
	
	// List of ids to delete
	var pdns_list = new Array();
	
	if(pdnsid < 1) {
		$(".ios:checked").each(function() {
			pdns_list.push($(this).closest("tr").attr("id"));
		});
		
		if(pdns_list.length < 1){
			alert("'.$l['nothing_selected'].'");
			return false;
		}
	}else{
		pdns_list.push(pdnsid);
	}
	
	pdns_list.forEach(function(id) {
		$("#pdns_list tr#" + id).find("img.test-icon").attr({"src":"'.$theme['images'].'progress_bar.gif", "width": 15});
	});
	
	pdns_list.reverse();
	
	pdns_test(pdns_list);
}

function pdns_test(pdns_list) {
	var id = pdns_list.pop();
	
	if(!id) {
		return false;
	}
	
	$.post("'.$globals['index'].'act=pdns&jsnohf=1&test="+id, function(data){
		if(data == "1"){
			$("#pdns_list tr#" + id).find("img.test-icon").attr({"src" : "'.$theme['images'].'/online.png", "title" : "'.$l['success_connect'].'"});
		}
		else{
			$("#pdns_list tr#" + id).find("img.test-icon").attr({"src" : "'.$theme['images'].'/offline.png", "title" : "'.$l['unable_connect'].'"});
		}
		
		pdns_test(pdns_list);
	});
}

function Delpdns(pdnsid){

	pdnsid = pdnsid || 0;
	
	// List of ids to delete
	var pdns_list = new Array();
	
	if(pdnsid < 1){
		
		if($("#pdns_task_select").val() == 0){
			alert("'.$l['no_action'].'");
			return false;
		}
		
		$(".ios:checked").each(function() {
			pdns_list.push($(this).val());
		});
		
	}else{
		
		pdns_list.push(pdnsid);
		
	}
	
	if(pdns_list.length < 1){
		alert("'.$l['nothing_selected'].'");
		return false;
	}
	
	var pdns_conf = confirm("'.$l['del_conf'].'");
	if(pdns_conf == false){
		return false;
	}
	
	var finalData = new Object();
	finalData["delete"] = pdns_list.join(",");

	//alert(finalData);
	//return false;
	
	$("#progress_bar").show();
	
	$.ajax({
		type: "POST",
		url: "'.$globals['index'].'act=pdns&api=json",
		data : finalData,
		dataType : "json",
		success: function(data){
			$("#progress_bar").hide();
			if("done" in data){
				alert("'.$l['action_completed'].'");
			}
			if("error" in data){
				alert(data["error"]);
			}
			location.reload(true);
		},
		error: function(data) {
			$("#progress_bar").hide();
			//alert(data.description);
			return false;
		}
	});
	
	return false;
};

// ]]></script>

<div id="showsearch" style="display:'.(optREQ('search') || (!empty($pdns) && !empty($globals['showsearch'])) ? "" : "none").';">
<form accept-charset="'.$globals['charset'].'" name="pdns" method="GET" action="" class="form-horizontal">
<input type="hidden" name="act" value="pdns">
		
<div class="form-group_head">
  <div class="row">
    <div class="col-sm-1"></div>
    <div class="col-sm-2"><label>'.$l['pdns_name'].'</label></div>
    <div class="col-sm-2"><input type="text" class="form-control" name="pdns_name" id="pdns_name" size="30" value="'.POSTval('pdns_name','').'"/></div>
    <div class="col-sm-2"><label>'.$l['pdns_ipaddress'].'</label></div>
    <div class="col-sm-2"><input type="text" class="form-control" name="pdns_ipaddress" id="pdns_ipaddress" size="30" value="'.POSTval('pdns_ipaddress','').'"/></div>
    <div class="col-sm-2" style="text-align: center;"><button type="submit" name="search" class="go_btn" value="Search"/>'.$l['submit'].'</button></div>
    <div class="col-sm-1"></div>
  </div>
</div>
		
</form>
<br />
<br />
</div>';

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
}

if(empty($pdns)){

	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.(optREQ('search') ? $l['no_res'] : $l['no_pdns']).'</div>';
	
}
else{

page_links($globals['num_res'], $globals['cur_page'], $globals['reslen']);
echo '<br /><br />
<form accept-charset="'.$globals['charset'].'" name="multi_pdns" id="multi_pdns" method="post" action="" class="form-horizontal">
<table id="pdns_list" class="table table-hover tablesorter">
	<tr>
		<th align="center" width="5%">'.$l['id'].'</th>
		<th align="center">'.$l['name'].'</th>
		<th align="center">'.$l['descr'].'</th>
		<th align="center" width="10%">'.$l['ip'].'</th>
		<th align="center" colspan="2">'.$l['view'].'</th>
		<th align="center" width="10" style="width:10px;">'.$l['test_pdns'].'</th>
		<th colspan="2" width="10">'.$l['manage'].'</td>
		<th><input type="checkbox" class="select_all" name="select_all" id="select_all"></th>
	<tr>';
	$i = 0;
	foreach($pdns as $k => $v){	
		
		echo '<tr id="'.$v['id'].'">
		<td>'.$v['id'].'</td>
		<td>'.$v['name'].'</td>
		<td>'.$v['description'].'</td>
		<td>'.$v['sql_ipaddress'].'</td>	
		<td align="center" width="10"><a href="'.$globals['index'].'act=domains&pdnsid='.$v['id'].'" title="'.$l['pdns_zones'].'"><img src="'.$theme['images'].'admin/manage.png" width="20" /></a></td>
		<td align="center" width="10"><a href="'.$globals['index'].'act=dnsrecords&pdnsid='.$v['id'].'" title="'.$l['pdns_records'].'"><img src="'.$theme['images'].'admin/plans.gif" /></a></td>
		<td class="test" align="center"><a href="#" onclick="test_pdns('.$v['id'].'); return false;" style="outline: 0px none;"><img class="test-icon" src="'.$theme['images'].'admin/connection.png" alt="'.$l['test_pdns'].'" width="20" /></a></td>
		<td align="center" width="10"><a href="'.$globals['index'].'act=editpdns&pdnsid='.$v['id'].'" title="'.$l['edit_pdns'].'"><img src="'.$theme['images'].'admin/edit.png" /></a></td>
		<td align="center" width="10"><a class="delete" href="javascript:void(0);" onclick="return Delpdns('.$k.');" title="'.$l['del_pdns'].'"><img src="'.$theme['images'].'admin/delete.png" /></a></td>
		<td width="20" valign="middle" align="center">
			<input type="checkbox" class="ios" name="pdns_list[]" value="'.$k.'"/>
		</td>
		</tr>';
	$i++;	
	}


echo '</table>

<div class="row bottom-menu">
		
	<div class="col-sm-7"></div>
	<div class="col-sm-5"><label>'.$l['with_selected'].'</label>
		<select name="pdns_task_select" id="pdns_task_select" class="form-control">
			<option value="0">---</option>
			<option value="1">'.$l['test_pdns'].'</option>
			<option value="2">'.$l['ms_delete'].'</option>
		</select>&nbsp;
		<input type="submit" id ="pdns_submit" class="go_btn" name="pdns_submit" value="Go" onclick="go_click(); return false;">
	</div>
</div>


<div id="progress_bar" style="height:125px; display:none">
	<br />
	<center>
		<font id="progress_txt" size="4" color="#222222">'.$l['action_msg'].'</font>
		<br>
		<br>
	</center>
	<table id="table_progress" width="500" height="28" cellspacing="0" cellpadding="0" border="0" align="center" style="border:1px solid #CCC; -moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px;background-color:#efefef;">
		<tbody>
			<tr>
				<td id="progress_color" width="100%" style="background-image: url(themes/default/images/bar.gif); -moz-border-radius: 4px; -webkit-border-radius: 4px; border-radius: 4px;"></td>
				<td id="progress_nocolor"> </td>
			</tr>
		</tbody>
	</table>
	<br>
	<center>
		'.$l['notify_msg'].'
	</center>
</div>

<br />
<center><a href="'.$globals['ind'].'act=addpdns" class="link_btn">'.$l['add_pdns'].'</a></center>';

}

echo '<br><br><br>

<center class="notice"><span>'.$l['powerdns_note'].'</span></center>';

page_links($globals['num_res'], $globals['cur_page'], $globals['reslen']);
echo '</div>';
softfooter();

}

?>