<?php

//////////////////////////////////////////////////////////////
//===========================================================
// serverinfo_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function serverinfo_theme(){

global $theme, $globals, $cluster, $user, $l , $error, $info , $langs , $skins;

softheader($l['<title>']);
echo '
<div class="bg">
<center class="tit"><i class="icon icon-config icon-head"></i> &nbsp;'.$l['heading'].'<label style="float:right;" ><a href="'.$globals['docs'].'Reset_API_Keys" target="_blank" class="wiki_help" title="'.$l['wiki_help'].'"><i class="icon-help" ></i></a></label></center>';

error_handle($error);

// Is it offline ?
$hypervisor_status = $cluster->statewise($globals['server']);
if($hypervisor_status == 0 || $hypervisor_status == 2){

	echo '<div class="e_notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['server_status_'.$hypervisor_status].'</div>';
	
}else{

echo '
<div class="row">
	<div class="col-sm-12">
		<div class="roundheader">'.$l['heading'].'</div>
		<table align="center" class="table no_border table-hover" cellpadding="3" cellspacing="4" border="0" width="100%">
			<tr>
				<td align="left" class="blue_td fhead" width="30%">'.$l['path'].'</td>
				<td class="val">'.$info['path'].'</td>
			</tr>
			<tr>
				<td align="left" class="blue_td fhead">'.$l['type'].'</td>
				<td class="val">'.($info['slave'] == 0 ? 'Master' : 'Slave').'</td>
			</tr>';
			
			if(!empty($info['slave'])){

				echo '
				<tr>
					<td align="left" class="blue_td fhead">'.$l['masterkey'].'</td>
					<td class="val">'.$info['masterkey'].'</td>
				</tr>
				<tr>
					<td align="left" class="blue_td fhead">'.$l['masterip'].'</td>
					<td class="val">'.$info['masterip'].'</td>
				</tr>';
			}
			
			echo '
			<tr>
				<td align="left" class="blue_td fhead">'.$l['key'].'</td>
				<td class="val">'.$info['key'].'</td>
			</tr>
			<tr>
				<td align="left" class="blue_td fhead">'.$l['pass'].'</td>
				<td class="val">'.$info['pass'].'</td>
			</tr>			
			<tr>
				<td align="left" class="blue_td fhead">'.$l['kernel'].'</td>
				<td class="val"><img src="'.$theme['images'].'admin/'.$info['kernel'].'_100.gif" width="70"/></td>
			</tr>			
			<tr>
				<td align="left" class="blue_td fhead">'.$l['num_vs'].'</td>
				<td class="val">'.$info['num_vs'].'</td>
			</tr>			
			<tr>
				<td align="left" class="blue_td fhead">'.$l['php_version'].'</td>
				<td class="val">'.phpversion().'</td>
			</tr>			
			<tr>
				<td align="left" class="blue_td fhead">'.$l['soft_version'].'</td>
				<td class="val">'.$info['version'].'</td>
			</tr>	
			<tr>
				<td align="left" class="blue_td fhead">'.$l['soft_patch'].'</td>
				<td class="val">'.$info['patch'].'</td>
			</tr>		
			<tr>
				<td align="left" class="blue_td fhead">'.$l['latest_soft_version'].'</td>
				<td class="val" id="newsoftversion"></td>
			</tr>
		</table>
	</div>
</div>

<br />
<br />

<center><a href="'.$globals['ind'].'act=serverinfo&genkey=1" class="link_btn">'.$l['reset_api_keys'].'</a></center>
<br />
<br />';
}
echo '</div>';
softfooter();
	
}

?>