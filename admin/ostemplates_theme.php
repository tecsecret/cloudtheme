<?php

//////////////////////////////////////////////////////////////
//===========================================================
// ostemplates_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function ostemplates_theme(){

global $theme, $globals, $kernel, $user, $l, $oslist, $error, $installed, $done, $mgs, $servers;

softheader($l['<title>']);

echo '
<div class="bg" style="width: 99%">
<center class="tit"><i class="icon icon-ostemplates icon-head"></i></i> &nbsp; '.$l['tit_ostmp'].'<span style="float:right;" ><a href="'.$globals['docs'].'OS_Templates" target="_blank" class="wiki_help" title="'.$l['wiki_help'].'"><i class="icon-help" ></i></a></span></center>';

error_handle($error);

echo '<script language="javascript" type="text/javascript"><!-- // --><![CDATA[

function go_click() {
	
	if($("#os_task_select").val() == 0){
		alert("'.$l['no_action'].'");
		return false;
	}
	
	var os_list = new Array();
	
	$(".ios:checked").each(function() {
		os_list.push($(this).val());
	});
	
	if(os_list.length < 1){
		alert("'.$l['nothing_selected'].'");
		return false;
	}
	
	if($("#os_task_select").val() == 1){
		Delos(os_list);
	} else if($("#os_task_select").val() == 2){
		show_server_select(os_list);
	}
	
}

function Delos(os_list){
	
	var os_conf = confirm("'.$l['del_conf'].'");
	if(os_conf == false){
		return false;
	}
	
	var finalData = new Object();
	finalData["delete"] = os_list.join(",");

	//alert(finalData);
	//return false;
	
	$("#progress_bar").show();
	
	$.ajax({
		type: "POST",
		url: "'.$globals['index'].'act=ostemplates&api=json",
		data : finalData,
		dataType : "json",
		success: function(data){
			$("#progress_bar").hide();
			if("done" in data){
				alert("'.$l['action_completed'].'");
			}
			if("error" in data){
				alert(data["error"]);
			}
			location.reload(true);
		},
		error: function(data) {
			$("#progress_bar").hide();
			//alert(data.description);
			return false;
		}
	});
	
	return false;
};

function serids_select_change() {
	var serids = $("#serids_select").val().join(",");
	$("#serids").val(serids);
}

function show_server_select(os_list) {';
	
	if(is_slave()) {
		echo 'alert("'.$l['slave_cant_exp'].'");
		return false;';
	} else {
		
		echo 'var $modal_form = $(\'<form id="sync_os_form" action="" method="post"> \
			<input type="hidden" id="sync_os" name="sync_os" /> \
			<input type="hidden" id="serids" name="serids" /> \
			<br> \
			<div class="row"> \
				<div class="col-sm-6"> \
					<label class="control-label">'.$l['select_servers'].'</label><br> \
					<span class="help-block">'.$l['select_servers_exp'].'</span> \
				</div> \
				<div class="col-sm-6"> \
					<select class="form-control" id="serids_select" onchange="serids_select_change();" multiple> \ ';
						
						foreach($servers as $serid => $server) {
							if(empty($serid)) continue;
							echo '<option value="'.$serid.'">'.$server['server_name'].' - '.$server['ip'].'</option> \ ';
						}
						
					echo '</select> \
				</div> \
			</div> \
			<br> \
			<div class="row"> \
				<div class="col-sm-6"> \
					<label class="control-label">'.$l['force_sync'].'</label><br> \
					<span class="help-block">'.$l['force_sync_exp'].'</span> \
				</div> \
				<div class="col-sm-6"> \
					<input type="checkbox" name="force_sync" /> \
				</div> \
			</div> \
			<br> \
			<div class="row"> \
				<div class="col-sm-12 text-center"> \
					<input class="btn" value="'.$l['sync_os'].'" type="submit"> \
				</div> \
			</div>\
		</form>\');
		
		// Show dialog for server select
		$modal_form.find("#sync_os").val(os_list.join(","));
		$("#logs_modal .fhead").text("'.$l['sync_os'].'");
		$("#logs_modal_body").html($modal_form);
		$("#logs_modal").modal("show");';
		
	}
	
echo '}

// ]]></script>

';

if($done == 'sync_os'){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done_sync_os'].'</div>';
} elseif(!empty($done)) {
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
}

if(empty($oslist)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['no_os'].'</div>';
}else{

echo '<form accept-charset="'.$globals['charset'].'" name="multi_os" id="multi_os" method="post" action="" class="form-horizontal">
<table class="table table-hover tablesorter">

<tr>
	<th>'.$l['osid'].'</th>
	<th align="center">'.$l['head_os'].'</th>
	<th align="center">'.$l['head_type'].'</th>
	<th align="center">'.$l['head_os_name'].'</th>
	<th align="center">'.$l['head_name'].'</th>
	<th align="center">'.$l['head_mg'].'</th>
	<th align="center">'.$l['head_size'].'</th>
	<th align="center">'.$l['head_ondisksize'].'</th>
	<th align="center">'.$l['head_usedby'].'</th>
	<th colspan="3">'.$l['manage'].'</th>
	<th><input type="checkbox" class="select_all" name="select_all" id="select_all"></th>
</tr>';
//r_print($oslist);

$i = 1;
foreach($oslist as $k => $v){

	foreach($v as $kv => $vv){
	
		foreach($vv as $kvv => $vs){
		
			echo'<tr>
			<td align="left">'.$kvv.'</td>
			<td align="center"><img src="'.distro_logo($kv).'" /></td>
			<td align="center"><img src="'.$theme['images'].'admin/'.$vs['type'].(!empty($vs['hvm']) ? 'hvm' : '').'_42.gif" /></td>
			<td align="left">'.$vs['name'].'</td>
			<td align="left">'.$vs['filename'].'</td>
			<td align="left">';
		
			$tmp = array();
			
			if(empty($vs['mg'])){
				echo '<em>'.$l['none'].'</em>';
			}else{
				foreach($vs['mg'] as $mk){
					$tmp[] = $mgs[$mk]['mg_name'];
				}
				echo implode(', ', $tmp);				
			}
						
			echo '</td>
			<td align="center">'.round($vs['size']/1024/1024).' MB</td>
			<td align="center">'.round(vfilesize($globals[$vs['type'].'os'].'/'.$vs['filename'])/1024/1024).' MB</td>
			<td align="center">'.(empty($installed[$kvv]) ? 0 : $installed[$kvv]).'</td>
			<td align="center" width="20"><a href="javascript:void(0);" title="'.$l['sync_os'].'" onclick="show_server_select(['.$kvv.']); return false"><img src="'.$theme['images'].'admin/restart.gif" /></a></td>
			<td align="center" width="20"><a href="'.$globals['ind'].'act=edittemplate&osid='.$kvv.'"  title="'.$l['edit_os'].'"><img src="'.$theme['images'].'admin/edit.png" /></a></td>
			<td align="center" width="20"><a href="javascript:void(0);" onclick="return Delos(['.$kvv.']);"  title="'.$l['del_os'].'"><img src="'.$theme['images'].'admin/delete.png" /></a></td>
			<td width="20" valign="middle" align="center">
				<input type="checkbox" class="ios" name="ostemplate_list[]" value="'.$kvv.'"/>
			</td>
			</tr>';
	
			$i++;
			
		}
		
	}
	
}
echo '</table>
<div class="row bottom-menu">
<div class="col-sm-7"></div>
<div class="col-sm-5"><label>'.$l['with_selected'].'</label>
<select class="form-control" name="os_task_select" id="os_task_select">
	<option value="0">---</option>
	<option value="1">'.$l['ms_delete'].'</option>
	<option value="2">'.$l['ms_sync_os'].'</option>
</select>&nbsp;
<input type="submit" id ="ostemplate_submit" class="go_btn" name="ostemplate_submit" value="Go" onclick="go_click(); return false;">
</div>
</div>
<br/>
<br/>
</form>

<div id="progress_bar" style="height:140px; display:none">
	<br />
	<center>
		<font id="progress_txt" size="4" color="#222222">'.$l['action_msg'].'</font>
		<br>
		<br>
	</center>
	<table id="table_progress" width="500" height="28" cellspacing="0" cellpadding="0" border="0" align="center" style="border:1px solid #CCC; -moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px;background-color:#efefef;">
		<tbody>
			<tr>
				<td id="progress_color" width="100%" style="background-image: url(themes/default/images/bar.gif); -moz-border-radius: 4px; -webkit-border-radius: 4px; border-radius: 4px;"></td>
				<td id="progress_nocolor"> </td>
			</tr>
		</tbody>
	</table>
	<br>
	<center>
		'.$l['notify_msg'].'
	</center>
</div>';

}

echo '<center><div class="col-sm-11"><a href="'.$globals['ind'].'act=addtemplate" title="'.$l['add_temp'].'" class="link_btn">'.$l['add_temp'].'</a> &nbsp;
<a href="'.$globals['ind'].'act=os" title="'.$l['temp_brwose'].'" class="link_btn">'.$l['temp_brwose'].'</a></div></center><br/>
</div>';
softfooter();

}

?>