<?php

//////////////////////////////////////////////////////////////
//===========================================================
// servergroups_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function servergroups_theme(){

global $theme, $globals, $servers, $user, $l, $cluster, $servers, $error, $deleted, $done, $servergroups;

softheader($l['<title>']);

echo '
<div class="bg" style="width: 99%">
<center class="tit">
<i class="icon icon-servers icon-head"></i>&nbsp; '.$l['page_head'].'<span style="float:right"><a href="javascript:showsearch();"><img src="'.$theme['images'].'admin/search.gif" /></a></span></center>';

error_handle($error);

if(!empty($deleted)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['deleted'].'</div>';
}

echo '<script language="javascript" type="text/javascript"><!-- // --><![CDATA[

function Delservergrp(sgid){

	sgid = sgid || 0;
	
	// List of ids to delete
	var servergrp_list = new Array();
	
	if(sgid < 1){
		
		if($("#servergrp_task_select").val() != 1){
			alert("'.$l['no_action'].'");
			return false;
		}
		
		$(".ios:checked").each(function() {
			servergrp_list.push($(this).val());
		});
		
	}else{
		
		servergrp_list.push(sgid);
		
	}
	
	if(servergrp_list.length < 1){
		alert("'.$l['nothing_selected'].'");
		return false;
	}
	
	var servergrp_conf = confirm("'.$l['del_conf'].'");
	if(servergrp_conf == false){
		return false;
	}
	
	var finalData = new Object();
	finalData["delete"] = servergrp_list.join(",");

	//alert(finalData);
	//return false;
	
	$("#progress_bar").show();
	
	$.ajax({
		type: "POST",
		url: "'.$globals['index'].'act=servergroups&api=json",
		data : finalData,
		dataType : "json",
		success: function(data){
			$("#progress_bar").hide();
			if("done" in data){
				alert("'.$l['action_completed'].'");
				location.reload(true);
			}
		},
		error: function(data) {
			$("#progress_bar").hide();
			//alert(data.description);
			return false;
		}
	});
	
	return false;
};

// ]]></script>

<div id="showsearch" style="display:'.(optREQ('search') || (!empty($servergroups) && !empty($globals['showsearch'])) ? "" : "none").';">
<form accept-charset="'.$globals['charset'].'" name="servergroup" method="GET" action="" class="form-horizontal">
<input type="hidden" name="act" value="servergroups">
		
<div class="form-group_head">
  <div class="row">
	<div class="col-sm-1"></div>
    <div class="col-sm-3"><label>'.$l['sg_name'].'</label></div>
    <div class="col-sm-4"><input type="text" class="form-control" name="sg_name" id="sg_name" size="30" value="'.POSTval('sg_name', '').'" /></div>
    <div class="col-sm-2" style="text-align: center;"><button type="submit" name="search" class="go_btn" value="Search"/>'.$l['submit'].'</button></div>
	<div class="col-sm-2"></div>
  </div>
</div>
</form>
<br />
<br />
</div>';

if(!empty($saved)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div><br />';
}

if(empty($servergroups)){

	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.(optREQ('search') ? $l['no_res'] : $l['no_servergroups']).'</div>';
	
}else{

page_links($globals['num_res'], $globals['cur_page'], $globals['reslen']);
echo '<br /><br />
<form accept-charset="'.$globals['charset'].'" name="multi_servergrp" id="multi_servergrp" method="post" action="" class="form-horizontal">
<table class="table table-hover tablesorter">
	<tr>
		<th align="center" width="30">'.$l['head_sgid'].'</th>
		<th align="center" width="100">'.$l['head_name'].'</th>
		<th align="center" width="300">'.$l['head_desc'].'</th>
		<th align="center" width="120">'.$l['head_select'].'</th>
		<th align="center" width="100">'.$l['head_servers'].'</th>
		<th align="center" width="30">'.$l['head_ha'].'</th>
		<th align="center" colspan="2">'.$l['manage'].'</th>
		<th><input type="checkbox" class="select_all" name="select_all" id="select_all"></th>
	</tr>';
	
$i = 1;

foreach($servergroups as $k => $v){

echo '<tr>
		<td align="left">'.$v['sgid'].'</td>
		<td>'.$v['sg_name'].'</td>
		<td>'.$v['sg_desc'].'</td>
		<td>'.(empty($v['sg_select']) ? $l['least_util'] : $l['first_avl']).'</td>
		<td>'.(!empty($v['servers']) ? implode(', ', $v['servers']) : '<em>'.$l['none'].'</em>').'</td>
		<td>'.(!empty($v['sg_ha']) ? '<img src='.$theme['images'].'/online.png alt="ha_on">' : '<img src='.$theme['images'].'/offline.png alt="ha_off"').'</td>
		<td align="center" style="padding:7px 0px 0px 0px" class="manage-ico">'.(empty($k) ? '' : '<a href="'.$globals['ind'].'act=editsg&sgid='.$v['sgid'].'"><img title="'.$l['edit'].'" src="'.$theme['images'].'admin/edit.png" /></a>').'</td>
		<td align="center" style="padding:7px 0px 0px 0px" class="manage-ico">'.(empty($k) ? '' : '<a href="javascript:void(0);" onclick="return Delservergrp('.$k.');"><img title="'.$l['delete'].'" src="'.$theme['images'].'admin/delete.png" /></a>').'</td>
		<td width="20" valign="middle" align="center">
			'.(empty($k) ? '' : '<input type="checkbox" class="ios" name="servergroup_list[]" value="'.$k.'"/>').'
		</td>
  	 </tr>';
	
	$i++;
}

echo '</table>
<div class="row bottom-menu">
	<div class="col-sm-7"></div>
	<div class="col-sm-5"><label>'.$l['with_selected'].'</label>
		<select name="servergrp_task_select" id="servergrp_task_select" class="form-control">
			<option value="0">---</option>
			<option value="1">'.$l['ms_delete'].'</option>
		</select>&nbsp;
		<input type="submit" id ="servergrp_submit" class="go_btn" name="servergrp_submit" value="Go" onclick="Delservergrp(); return false;">
	</div>
</div>
</form>

<div id="progress_bar" style="height:125px; display:none">
	<br />
	<center>
		<font id="progress_txt" size="4" color="#222222">'.$l['action_msg'].'</font>
		<br>
		<br>
	</center>
	<table id="table_progress" width="500" height="28" cellspacing="0" cellpadding="0" border="0" align="center" style="border:1px solid #CCC; -moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px;background-color:#efefef;">
		<tbody>
			<tr>
				<td id="progress_color" width="100%" style="background-image: url(themes/default/images/bar.gif); -moz-border-radius: 4px; -webkit-border-radius: 4px; border-radius: 4px;"></td>
				<td id="progress_nocolor"> </td>
			</tr>
		</tbody>
	</table>
	<br>
	<center>
		'.$l['notify_msg'].'
	</center>
</div>';
}

page_links($globals['num_res'], $globals['cur_page'], $globals['reslen']);

echo '<br /><br />
<center><input type="button" value="'.$l['add_sg'].'" class="link_btn" onclick="window.location =\''.$globals['ind'].'act=addsg\';">
</center>

</div></div>';

softfooter();

}

?>