<?php

//////////////////////////////////////////////////////////////
//===========================================================
// logs_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function feedback_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $mail_sent;

softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon-feedback icon-head"></i> &nbsp; '.$l['heading'].' <br /><br /><br />';


if(!empty($mail_sent)){

	echo '<div class="notice" id="complete"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['fb_sucess'].'</div>
	<br />
	<br />';
	
}else{

echo '<form accept-charset="'.$globals['charset'].'" name="send" method="post" action="" class="form-horizontal">
'.$l['m_type'].' : 
<select class="form-control" name="fbtype" id="fbtype" style="width:30%;">

<option value="'.$l['feedback'].'">'.$l['feedback'].'</option>
<option value="'.$l['bug_report'].'">'.$l['bug_report'].'</option>
<option value="'.$l['suggestion'].'">'.$l['suggestion'].'</option>
</select>
<br />
<br />
'.$l['subject'].' : <input type="text" class="form-control" name="fbsub" id="fbsub" style="width:50%;" /><br /><br />

<textarea rows="10" cols="70" class="form-control" name="fbtext" id="fbtext"></textarea></center>


<center><input type="submit" name="send" value="'.$l['send'].'" class="btn"></center>
</form>';
}

echo '</div>';
softfooter();

}

?>