<?php

//////////////////////////////////////////////////////////////
//===========================================================
// addserver_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function addstorage_theme(){

global $theme, $globals, $kernel, $user, $l, $cluster, $error, $servers, $servergroups, $done;

softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-storage icon-head"></i>&nbsp; '.$l['add_storage'].'<span style="float:right;" ><a href="'.$globals['docs'].'Add_New_Storage" target="_blank" class="wiki_help" title="'.$l['wiki_help'].'"><i class="icon-help" ></i></a></span></center>';

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['added'].'</div><br />';
}

error_handle($error);

echo '<script language="javascript" type="text/javascript"><!-- // --><![CDATA[

addonload("storageSet();");

function thin_block(){
	
	var thin_is = $_("type").value;
	
	if(thin_is =="thin block"){
		var r = confirm("'.$l['thin_warning'].'");
		if(r != true){
			return false;
		}
	}
};

function storageSet(storage){
	
	$(".block, .file, .openvz, .thin-block, .zfs-block, .zfs-thin-block, .zfs-block-compressed, .ceph-block").hide();
	
	if(typeof(storage) == "undefined" || storage === null){
		storage = "block";
	}
	
	// Replace spaces with - as class name conflicts 
	storage = storage.replace(/\s/g, "-")
	
	$("." + storage).show();
};

// ]]></script>

<div id="form-container">
<form accept-charset="'.$globals['charset'].'" name="addstorage" method="post" action="" class="form-horizontal">

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['st_name'].'</label>
		<span class="help-block">'.$l['st_name_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="name" id="st_name" size="30" value="'.POSTval('name', '').'" />
	</div>	
</div>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['st_server'].'</label>
		<span class="help-block">'.$l['st_exp_server'].'</span>
	</div>
	<div class="col-sm-6">';
	
		$POSTserid = @$_POST['serid'];
		$POSTserid = empty($POSTserid) ? array() : $POSTserid;
		
		echo '<select class="form-control" name="serid[]" id="serid" size="8" multiple="multiple">
			<option value="-1" '.(in_array(-1, $POSTserid) ? 'selected="selected"' : '').'>'.$l['all_servers'].'</option>';
			
			foreach ($servergroups as $k => $v){
				echo '<option class="fhead" value="'.$k.'_group" '.(in_array($k.'_group', $POSTserid) ? 'selected="selected"' : '').'>[Group]&nbsp;'.$v['sg_name'].'</option>';
				
				foreach ($servers as $m => $n){
					if($n['sgid'] == $k){
						echo '<option value="'.$n['serid'].'" '.(in_array((string)$n['serid'], $POSTserid) ? 'selected="selected"' : '').'>&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;'.$n['server_name'].'</option>';
					}
				}
			}
			
			echo '
		</select>
		<span class="help-block"></span>
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['st_type'].'</label>
		<span class="help-block">'.$l['st_type_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<select class="form-control" name="type" id="type" onchange="storageSet(this.value)">
			<option value="block" '.POSTselect('type', 'block', 1).'>'.storage_type_text('block').'</option>
			<option value="file" '.POSTselect('type', 'file').'>'.storage_type_text('file').'</option>
			<option value="openvz" '.POSTselect('type', 'openvz').'>'.storage_type_text('openvz').'</option>
			<option value="thin block" '.POSTselect('type', 'thin block').'>'.storage_type_text('thin block').'</option>
			<option value="zfs block" '.POSTselect('type', 'zfs block').'>'.storage_type_text('zfs block').'</option>
			<option value="zfs thin block" '.POSTselect('type', 'zfs thin block').'>'.storage_type_text('zfs thin block').'</option>
			<option value="zfs block compressed" '.POSTselect('type', 'zfs block compressed').'>'.storage_type_text('zfs block compressed').'</option>
			<option value="zfs thin block compressed" '.POSTselect('type', 'zfs thin block compressed').'>'.storage_type_text('zfs thin block compressed').'</option>
			<option value="ceph block" '.POSTselect('type', 'ceph block').'>'.storage_type_text('ceph block').'</option>
		</select>
	</div>	
</div>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label block file openvz thin-block zfs-block zfs-thin-block zfs-block-compressed zfs-thin-block-compressed ceph-block">'.$l['st_path'].'</label>
		<span class="help-block block">'.$l['st_lvm_path_exp'].'</span>
		<span class="help-block openvz" >'.$l['st_openvz_path_exp'].'</span>
		<span class="help-block file">'.$l['st_file_path_exp'].'</span>
		<span class="help-block thin-block">'.$l['st_thin_block_path_exp'].'</span>
		<span class="help-block zfs-block zfs-thin-block zfs-block-compressed zfs-thin-block-compressed">'.$l['st_zfs_path_exp'].'</span>
		<span class="help-block ceph-block">'.$l['ceph_pool_name_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="path" size="30" value="'.POSTval('path', '').'" />
	</div>	
</div>

<div class="row block file thin-block zfs-block zfs-thin-block zfs-block-compressed">
	<div class="col-sm-6">
		<label class="control-label">'.$l['st_format'].'</label>
		<span class="help-block">'.$l['st_format_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<select class="form-control" name="format">
			<option value="raw" '.POSTselect('format', 'raw', 1).'>RAW</option>
			<option value="qcow2" '.POSTselect('format', 'qcow2').'>QCOW2</option>
			<option value="vhd" '.POSTselect('format', 'vhd').'>VHD</option>';
			//<option value="vmdk" '.POSTselect('format', 'vmdk').'>VMDK</option>
			//<option value="vpc" '.POSTselect('format', 'vpc').'>VPC (VHD Format)</option>
		echo '</select>
	</div>	
</div>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['st_oversell'].'</label>
		<span class="help-block">'.$l['st_oversell_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="oversell" size="30" value="'.POSTval('oversell', '').'" />
	</div>	
</div>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['st_alert_threshold'].'</label>
		<span class="help-block">'.$l['st_alert_threshold_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="alert_threshold" size="10" value="'.POSTval('alert_threshold', '90').'"  style="float:left; width:15%" />
		<label style="padding:8px 0px 0px 8px;">%</label>
	</div>	
</div>

<div class="row">
	<div class="col-sm-6 col-xs-10">
		<label class="control-label">'.$l['st_primary_storage'].'</label>
		<span class="help-block">'.$l['st_primary_storage_exp'].'</span>
	</div>
	<div class="col-sm-6 col-xs-2">
		<input type="checkbox" id="primary_storage" class="ios" name="primary_storage" '.POSTchecked('primary_storage', '').' />
	</div>	
</div>

</div>

<div class="notebox" align="left" style="font-size:12px; line-height:150%"><img src="'.$theme['images'].'/notice.gif" align=left/> &nbsp; '.$l['zfs_warning'].'</div>


<br /><br />
<center><input type="submit" name="addstorage" class="btn" onclick="return thin_block();" value="'.$l['sub_but'].'" /></center>

</form>
</div>';

softfooter();

}

?>