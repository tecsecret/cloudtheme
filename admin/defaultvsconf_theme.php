<?php

//////////////////////////////////////////////////////////////
//===========================================================
// defaultvsconf_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function defaultvsconf_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $defaultvsconf, $cluster, $done;

softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-config icon-head"></i>&nbsp; '.$l['editconf'].'</center>';

error_handle($error);

// Is it offline ?
$hypervisor_status = $cluster->statewise($globals['server']);
if($hypervisor_status == 0 || $hypervisor_status == 2){

	echo '<div class="e_notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['server_status_'.$hypervisor_status].'</div>';
	
}else{

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
}

echo '<form accept-charset="'.$globals['charset'].'" name="vsconf" method="post" action="" class="form-horizontal">

<div class="row">
	<div class="col-sm-2"></div>
	<div class="col-sm-8">
		<textarea style="color: #333333; border: 1px solid #CCCCCC; padding: 5px;" cols="80" class="form-control" name="confinfo" rows="25">'.POSTval('confinfo', $defaultvsconf).'</textarea>
	</div>
	<div class="col-sm-2"></div>
</div>


<br />
<center><input type="submit" name="saveconf" value="'.$l['saveconf'].'" class="btn"></center>
</form>';

}

echo '</div>';
softfooter();

}

?>