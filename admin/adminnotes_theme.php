<?php

//////////////////////////////////////////////////////////////
//===========================================================
// addserver_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function adminnotes_theme(){

global $theme, $globals, $servers, $user, $l, $list, $cluster, $error, $done, $notes_archived, $count_archived;

softheader($l['<title>']);

echo '
<div class="bg" style="width:99%">
<center class="tit"><i class="icon icon-notes icon-head"></i>&nbsp; '.$l['<title>'].'<span style="float:right"><a href="javascript:showsearch();"><img src="'.$theme['images'].'admin/search.gif" /></a></span></center>';

error_handle($error);

echo '<script language="javascript" type="text/javascript"><!-- // --><![CDATA[

function go_click(select) {
	var selected = $("#note_task_select"+select).val();
	
	if(selected == 0){
		alert("'.$l['no_action'].'");
		return false;
	}
	
	if(selected == 1) {
		restore_note();
	} else if(selected == 2) {
		delete_note();
	}
}

function restore_note(noteid){
	
	// List of ids to delete
	var noteids = new Array();
	
	if(!noteid){
		
		$(".ios:checked").each(function() {
			noteids.push($(this).val());
		});
		
	}else{
		
		noteids.push(noteid);
		
	}
	
	if(noteids.length < 1){
		alert("'.$l['nothing_selected'].'");
		return false;
	}
	
	var res_conf = confirm("'.$l['res_conf'].'");
	if(res_conf == false){
		return false;
	}
	
	var finalData = new Object();
	finalData["restore"] = noteids.join(",");
	
	$("#progress_bar").show();
	
	$.ajax({
		type: "POST",
		url: "'.$globals['index'].'act=adminnotes&api=json",
		data : finalData,
		dataType : "json",
		success: function(data){
			$("#progress_bar").hide();
			
			if("done" in data){
				alert("'.$l['action_completed'].'");
			}
			
			if("error" in data){
				alert(data["error"]);
			}
			
			location.reload(true);
		},
		error: function(data) {
			$("#progress_bar").hide();
			return false;
		}
	});
	
	return false;
};

function delete_note(noteid){
	
	// List of ids to delete
	var noteids = new Array();
	
	if(!noteid){
		
		$(".ios:checked").each(function() {
			noteids.push($(this).val());
		});
		
	}else{
		
		noteids.push(noteid);
		
	}
	
	if(noteids.length < 1){
		alert("'.$l['nothing_selected'].'");
		return false;
	}
	
	var del_conf = confirm("'.$l['del_conf'].'");
	if(del_conf == false){
		return false;
	}
	
	var finalData = new Object();
	finalData["delete"] = noteids.join(",");
	
	$("#progress_bar").show();
	
	$.ajax({
		type: "POST",
		url: "'.$globals['index'].'act=adminnotes&api=json",
		data : finalData,
		dataType : "json",
		success: function(data){
			$("#progress_bar").hide();
			
			if("done" in data){
				alert("'.$l['action_completed'].'");
			}
			
			if("error" in data){
				alert(data["error"]);
			}
			
			location.reload(true);
		},
		error: function(data) {
			$("#progress_bar").hide();
			return false;
		}
	});
	
	return false;
};

// ]]></script>

<div id="showsearch" style="display:'.(optREQ('search') || (!empty($tasks) && !empty($globals['showsearch'])) ? "" : "none").';">
<form accept-charset="'.$globals['charset'].'"  class="form-horizontal" name="tasks" method="GET" action="">
<input type="hidden" name="act" value="adminnotes">

<div class="form-group_head">
	<div class="row">
		<div class="col-sm-1"><label>'.$l['title'].'</label></div>
		<div class="col-sm-5"><input type="text" class="form-control" name="title" id="title" size="15" value="'.REQval('title', '').'" /></div>
		<div class="col-sm-1"><label>'.$l['content'].'</label></div>
		<div class="col-sm-5"><input type="text" class="form-control" name="content" id="content" size="15" value="'.REQval('content', '').'" /></div>
	</div>
	<div class="row">
		<div class="col-sm-1"><label>'.$l['user'].'</label></div>
		<div class="col-sm-5"><input type="text" class="form-control" name="user" id="user" size="15" value="'.REQval('user', '').'" /></div>
		<div class="col-sm-1"><label>'.$l['type'].'</label></div>
		<div class="col-sm-5">
			<select class="form-control" name="type" id="type">
				<option value="0">---</option>
				<option value="1">'.$l['private_note'].'</option>
				<option value="2">'.$l['public_note'].'</option>
			</select>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12" style="text-align: center;"><input type="submit" name="search" value="'.$l['search'].'" class="go_btn"/></div>
	</div>
</div>


</form>
<br />
<br />
</div>';

page_links($count_archived, $globals['curr_page'], $globals['reslen_archived']);

echo '<form accept-charset="'.$globals['charset'].'" name="adminnotes" method="POST" action="" class="form-horizontal">';

if(empty($notes_archived)) {
	echo '<div class="notice" id="complete"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['no_notes'].'</div>';
} else {
	
	echo '<div class="row bottom-menu">
			<div class="col-sm-7"></div>
			<div class="col-sm-5"><label>'.$l['with_selected'].'</label>
				<select name="note_task_select" id="note_task_select2" class="form-control">
					<option value="0">---</option>
					<option value="1">'.$l['restore'].'</option>
					<option value="2">'.$l['delete'].'</option>
				</select>&nbsp;
				<input type="submit" id ="note_submit" class="go_btn" name="note_submit" value="Go" onclick="go_click(2); return false;">
			</div>
		</div><br>

		<table width="100%">';

	foreach($notes_archived as $note) {

		echo '<tr>
			<td>
				<div class="note" style="background-color:#FFFF8D; margin:0 0 16px 0;">
					<div class="note_title">'.$note['title'].'</div>
					<pre class="note_content">'.$note['content'].'</pre>
					<div class="note_bottom">
						<div class="note_actions">
							<a class="note_restore" href="#" title="'.$l['restore'].'" onclick="restore_note('.$note['noteid'].'); return false;"><i class="icon icon-undo" style="color:#888;"></i></a>
							<a class="note_delete" href="#" title="'.$l['delete'].'" onclick="delete_note('.$note['noteid'].'); return false;"><i class="icon icon-delete" style="color:#888;"></i></a>
						</div>
						<div class="note_info">'.$note['user'].' ('.$note['time'].') <b>'.(empty($note['type']) ? $l['private_note'] : $l['public_note']).'</b></div>
					</div>
				</div>
			</td>
			<td width="30" valign="top" align="center">
				<input type="checkbox" class="ios" name="noteids[]" value="'.$note['noteid'].'"/>
			</td>
		</tr>';

	}

	echo '</table>
		<div class="row bottom-menu">
			<div class="col-sm-7"></div>
			<div class="col-sm-5"><label>'.$l['with_selected'].'</label>
				<select name="note_task_select" id="note_task_select1" class="form-control">
					<option value="0">---</option>
					<option value="1">'.$l['restore'].'</option>
					<option value="2">'.$l['delete'].'</option>
				</select>&nbsp;
				<input type="submit" id ="note_submit" class="go_btn" name="note_submit" value="Go" onclick="go_click(1); return false;">
			</div>
		</div>';
	
}

echo '</form>

<div id="progress_bar" style="height:125px; display:none">
	<br />
	<center>
		<font id="progress_txt" size="4" color="#222222">'.$l['action_msg'].'</font>
		<br>
		<br>
	</center>
	<table id="table_progress" width="500" height="28" cellspacing="0" cellpadding="0" border="0" align="center" style="border:1px solid #CCC; -moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px;background-color:#efefef;">
		<tbody>
			<tr>
				<td id="progress_color" width="100%" style="background-image: url(themes/default/images/bar.gif); -moz-border-radius: 4px; -webkit-border-radius: 4px; border-radius: 4px;"></td>
				<td id="progress_nocolor"> </td>
			</tr>
		</tbody>
	</table>
	<br>
	<center>
		'.$l['notify_msg'].'
	</center>
</div>';

page_links($count_archived, $globals['curr_page'], $globals['reslen_archived']);

echo '</div>';

softfooter();
}

?>