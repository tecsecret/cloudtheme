<?php

//////////////////////////////////////////////////////////////
//===========================================================
// vpsbackups_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 2.1.9
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Julien
// Date:       26th July 2012
// Time:       15:42 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function vpsbackups_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $cluster, $servers, $restore_done, $delete_done, $backups_list, $curr_backup_list, $selected_date, $existing_vps;
	
softheader($l['<title>']);


echo '
<div class="bg">
<center class="tit"><i class="icon icon-databackup icon-head"></i>&nbsp; '.$l['vpsbackups'].'</center>';

error_handle($error);
// Is it offline ?
$hypervisor_status = $cluster->statewise($globals['server']);
if($hypervisor_status == 0 || $hypervisor_status == 2){

	echo '<div class="e_notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['server_status_'.$hypervisor_status].'</div>';

}else{

	if(!empty($restore_done)){
		echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['restore_bg'].'</div>';
	}

	if(!empty($delete_done)){
		echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['delete_bg'].'</div>';
	}

	echo '
	<script language="javascript" type="text/javascript">
	
		function conf_res(ele){
			var x = confirm("'.$l['conf_res'].'");
			if(x == true){
				return true;
			}
			return false;
		}
		
		function restore_new() {
			
			if(empty($("#hostname").val())) {
				alert("'.$l['no_hostname'].'");
				return false;
			}
			
			if(empty($("#uid").val())) {
				alert("'.$l['no_uid'].'");
				return false;
			}
			
			return true;
		}
	
		function restore_details(ele){
			$("#progress_bar").show();
			
			var vpsid = $(ele).data("vpsid");
			var date = $(ele).data("date");
			
			$.ajax({
				type: "POST",
				url: "'.$globals['index'].'act=vpsbackups&api=json",
				data: {
					restore_details: vpsid,
					date: date
				},
				dataType : "json",
				success: function(data){
					if("error" in data) {
						var err = "";
						$.each(data["error"], function(key, value) {
							err += value + "\n\n";
						});
						alert(err);
						return false;
					}
					
					var restore_details = data["restore_details"];
					
					// Show the restore dialog
					var $modal_form = $(\'<form action="" method="post" onsubmit="return restore_new()"> \
										<input type="hidden" id="restore" name="restore" /> \
										<input type="hidden" id="date" name="date" /> \
										<input type="hidden" id="newvps" name="newvps" value="1" /> \
										<div class="row"> \
											<div class="col-sm-6"> \
												<label class="control-label">'.$l['hostname'].'</label><br> \
												<span class="help-block">'.$l['hostname_exp'].'</span> \
											</div> \
											<div class="col-sm-6"> \
												<input type="text" class="form-control" name="hostname" id="hostname" size="30" value=""> \
											</div> \
										</div> \
										<div class="row"> \
											<div class="col-sm-6"> \
												<label class="control-label">'.$l['select_user'].'</label><br> \
												<span class="help-block">'.$l['select_user_exp'].'</span> \
											</div> \
											<div class="col-sm-6"> \
												<select class="form-control" id="uid" name="uid"> \
													<option value="0">'.$l['select'].'</option> \
												</select> \
											</div> \
										</div> \
										<div class="row"> \
											<div class="col-sm-12 text-center"> \
												<input class="btn" value="'.$l['restore'].'" type="submit"> \
											</div> \
										</div> \
									</form>\');
					
					// Set form date
					$modal_form.find("#restore").val(vpsid);
					$modal_form.find("#date").val(date);
					$modal_form.find("#hostname").val("Restored-" + restore_details["hostname"]);
					
					var $select = $modal_form.find("#uid");
					
					$.each(restore_details["users"], function(uid, email) {
						var $option = $("<option />").val(uid)
							.text(email);
						
						if(uid == restore_details["uid"]) {
							$option.attr("selected", true);
						}
						
						$option.appendTo($select);
					});
					
					// Show dialog for user select
					$("#logs_modal").modal("show");
					$("#logs_modal_body").html($modal_form);
				},
				complete: function(data) {
					$("#progress_bar").hide();
				}
			});
			
			return false;
		}
		
		function vps_alert() {
			alert("'.$l['vps_does_not_exist'].'");
			return false;
		}
		
		function Delbackup(backup){

			backup = backup || 0;
			
			// List of ids to delete
			var backup_list = new Array();
			
			if(backup < 1){
				
				if($("#backup_task_select").val() != 1){
					alert("'.$l['no_action'].'");
					return false;
				}
				
				$(".ios:checked").each(function() {
					backup_list.push($(this).val());
				});
				
			}else{
				
				backup_list.push(backup);
				
			}
			
			if(backup_list.length < 1){
				alert("'.$l['nothing_selected'].'");
				return false;
			}
			
			var backup_conf = confirm("'.$l['del_conf'].'");
			if(backup_conf == false){
				return false;
			}
			
			var finalData = new Object();
			finalData["delete"] = backup_list.join(",");';
			
			if(!empty($selected_date)){
				echo 'finalData["date"] = "'.$selected_date.'"';
			}
			
			echo '//alert(finalData["delete"]);
			//return false;
			
			$("#progress_bar").show();
			
			$.ajax({
				type: "POST",
				url: "'.$globals['index'].'act=vpsbackups&api=json",
				data : finalData,
				dataType : "json",
				success: function(data){
					$("#progress_bar").hide();
					if("delete_done" in data){
						alert("'.$l['action_completed'].'");
						location.reload(true);
					}
				},
				error: function(data) {
					$("#progress_bar").hide();
					//alert(data.description);
					return false;
				}
			});
			
			return false;
		};	
					
		$(document).ready(function(){
					
					$("#date_selector").change(function(data){
						window.location.href="index.php?act=vpsbackups&date="+data.target.value;
					});
		});
				
	</script>';
	
	if(!empty($backups_list)){

		echo '<br/><br /><br />
		<div class="row">
			<div class="col-sm-3"></div>
			<div class="col-sm-3"><b>'.$l['sel_data'].':</b></div>
			<div class="col-sm-3">
				<select id="date_selector" class="form-control" name="date_selector">';		
			foreach($backups_list as $k => $v){
				$clean_date = "";
				$tempdate = explode('/', $k);
				$str_date = $tempdate[count($tempdate)-1];
				$str = strval($str_date);
				for($i=0 ;$i < strlen($str); $i++){
					if($i == 4 || $i == 6){
						$clean_date .= '/';
					}
					$clean_date .= $str[$i];
				}
				echo '<option value='.$str_date.' '.ex_POSTselect('date_selector', $str_date, $selected_date).'>'.$clean_date.'</option>';
			}
		echo '</select>
			</div>
			<div class="col-sm-3"></div>
		</div>
		<br/><br /><br />
		<form accept-charset="'.$globals['charset'].'" name="multi_backup" id="multi_backup" method="post" action="" class="form-horizontal">
			<table cellpadding="8" cellspacing="1" border="0" width="90%" align="center" class="table table-hover tablesorter">
			
				<tr>
					<th align="center">'.$l['name'].'</th>
					<th width="100" align="center">'.$l['size'].'</th>
					<th width="10" align="center">'.$l['restore'].'</th>
					<th width="10" align="center">'.$l['restore_new_vps'].'</th>
					<th width="10" align="center">'.$l['delete'].'</th>
					<th width="20"><input type="checkbox" class="select_all" name="select_all" id="select_all"></th>
				</tr>';
		
		$i = 1;
		
		foreach($curr_backup_list as $k => $backup){
		
			if(!empty($backup)){
			
				foreach($backup as $index => $v){

					$temp = explode('/',$v['abs_path']);
					$filename = $temp[count($temp)-1];
					$temp = explode('.', $filename);
					$vpsid = $temp[0];

					echo '<tr>';
					echo '<td align="center">'.$filename.'</td>
						<td align="center">'.round(($v['size']/1024/1024)).' M</td>
						<td align="center"><a onclick="return '.(in_array($vpsid, $existing_vps) ? 'conf_res(this)' : 'vps_alert(this)').'" href="?act=vpsbackups&restore='.$vpsid.'&date='.$selected_date.'" title="'.$l['restore'].'"><img src="'.$theme['images'].'admin/import.gif" width="24" /></a></td>
						<td align="center"><a onclick="return restore_details(this)" href="#" data-vpsid="'.$vpsid.'" data-date="'.$selected_date.'" title="'.$l['restore_new_vps'].'"><img src="'.$theme['images'].'admin/import.gif" width="24" /></a></td>
						<td align="center">
							<a href="javascript:void(0);" title="'.$l['delete'].'" onclick="return Delbackup('.$vpsid.');"><img src="'.$theme['images'].'admin/delete.png" /></a>
						</td>
						<td width="20" valign="middle" align="center">
							<input type="checkbox" class="ios" name="vpsbackup_list[]" value="'.$vpsid.'"/>
						</td>
					</tr>';
				}
				$i++;
				
			}else{			
				echo '<tr><td colspan="5" align="center"><b>'.$l['no_backups_date'].'</b></td></tr>';
			}
		}
			
			echo '</table>
			<div class="row bottom-menu">			
			<div class="col-sm-7"></div>
			<div class="col-sm-5"><label>'.$l['with_selected'].'</label>
			<select name="backup_task_select" class="form-control" id="backup_task_select">
				<option value="0">---</option>
				<option value="1">'.$l['ms_delete'].'</option>
			</select>&nbsp;
			<button type="submit" class="go_btn" id="backup_submit" name="backup_submit" onclick="Delbackup(); return false;">Go</button></div>
			</div>
		</form>
		<div id="progress_bar" style="height:125px; display:none">
			<br />
			<center class="col-sm-11">
				<font id="progress_txt" size="4" color="#222222">'.$l['action_msg'].'</font>
				<br>
				<br>
				<table id="table_progress" width="450" height="28" cellspacing="0" cellpadding="0" border="0" align="center" style="border:1px solid #CCC; -moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px;background-color:#efefef;">
					<tbody>
						<tr>
							<td id="progress_color" width="100%" style="background-image: url(themes/default/images/bar.gif); -moz-border-radius: 4px; -webkit-border-radius: 4px; border-radius: 4px;"></td>
							<td id="progress_nocolor"> </td>
						</tr>
					</tbody>
				</table>
				<br/>'.$l['notify_msg'].'
			</center>
		</div><BR /><BR/>';
		
	}else{
		echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['no_backups'].'</div>';
	}

}// End of Server Offline if	

echo '</div>';
softfooter();

}

?>