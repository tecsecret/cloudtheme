<?php

//////////////////////////////////////////////////////////////
//===========================================================
// vnc_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function vnc_theme(){

global $theme, $globals, $cluster, $user, $l, $info, $vpses, $servers;

// is it no vnc mode ?
if(optGET('novnc') && !empty($globals['novnc'])){
	
	$vpsid = optREQ('novnc');
	
	// Invalid VPS
	if(empty($vpses[$vpsid])){
		echo '<div class="e_notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['invalid_vps'].'</div>';
		return false;
	}
	
	// Get the VPS VNC Details
	//$info = $cluster->vncDetails($vpsid);
	
	if(empty($info['port'])){
		echo '<div class="e_notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['vps_offline'].'</div>';
		return false;
	}
		
	// Ensure the service is running
	//$cluster->novnc_start($vpses[$vpsid]['serid']);
	
	$novnc_viewer = file_get_contents(dirname(dirname(__FILE__)).'/novnc/vnc_auto_virt.html');

	// The noVNC serid
	$novnc_serid = $vpses[$vpsid]['serid'];

	if(!empty($globals['novnc_master_only'])){
		$novnc_serid = 0;
	}
	
	$proto = 'http';
	$port = 4081;
	$websockify = 'websockify';
	if(!empty($_SERVER['HTTPS'])){
		$proto = 'https';
		$port = 4085;
		$websockify = 'novnc/';
	}
	
	if($vpses[$vpsid]['virt']== 'xcp'){
		$vnc_token = $vpsid.'-'.$vpses[$vpsid]['vnc_passwd'];
	}else{
		$vnc_token = $vpsid;
	}
	
	$novnc_viewer = lang_vars_name($novnc_viewer, array('HOST' => (!empty($globals['novnc_server_name']) ? server_hostname($novnc_serid) : server_vncip($novnc_serid)),
														'PORT' => $port,
														'PROTO' => $proto,
														'WEBSOCKET' => $websockify,
														'TOKEN' => $vnc_token,
														'PASSWORD' => $info['password']));
		
	echo $novnc_viewer;
	
	return true;
	
}

// Ajax handles
if(isset($_REQUEST['ajax'])){
	
	$vpsid = optREQ('ajax');
	
	echo '<br /><br />';
	
	// Invalid VPS
	if(empty($vpses[$vpsid])){
		echo '<div class="e_notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['invalid_vps'].'</div>';
		return false;
	}
	
	// Get the VPS VNC Details
	//$info = $cluster->vncDetails($vpsid);
	
	if(empty($info['port'])){
		echo '<div class="e_notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['vps_offline'].'</div>';
		return false;
	}
	
	echo '<div class="heading">'.$l['vnc_info'].'</div>
<br />
<table align="center" cellpadding="6" cellspacing="0" border="0">
	<tr>
		<td width="15%" class="fhead">&nbsp;&nbsp;&nbsp;'.$l['vnc_ip'].'</td>
		<td class="val" style="border-right:1px solid #CCCCCC" width="35%">'.$info['ip'].'</td>

		<td width="15%" align="right" class="fhead">&nbsp;&nbsp;&nbsp;'.$l['vnc_port'].'</td>
		<td class="val" width="35%" align="right">&nbsp;&nbsp;'.$info['port'].'</td>
	</tr>
</table>';
	
	$virt = $vpses[$vpsid]['virt'];
	
	if($virt == 'xcp'){
		
		echo '<APPLET ARCHIVE="'.$theme['url'].'/java/vnc/TightVncViewer.jar" CODE="com.tightvnc.vncviewer.VncViewer" WIDTH="1" HEIGHT="1">
		<PARAM NAME="SOCKETFACTORY" VALUE="com.tightvnc.vncviewer.SshTunneledSocketFactory">
		<PARAM NAME="SSHHOST" VALUE="'.$info['ip'].'">
		<PARAM NAME="HOST" VALUE="localhost">
		<PARAM NAME="PORT" VALUE="'.$info['port'].'">
		<PARAM NAME="Open New Window" VALUE="yes">
		</APPLET>';
		
	}else{

		echo '<APPLET ARCHIVE="https://s2.softaculous.com/a/virtualizor/files/VncViewer.jar" CODE="com.tigervnc.vncviewer.VncViewer" WIDTH="1" HEIGHT="1">
		<PARAM NAME="HOST" VALUE="'.$info['ip'].'">
		<PARAM NAME="PORT" VALUE="'.$info['port'].'">
		<PARAM NAME="PASSWORD" VALUE="'.$info['password'].'">
		<PARAM NAME="Open New Window" VALUE="yes">
		</APPLET>';
		
	}
	
	return true;
	
}

softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-vs icon-head"></i>&nbsp; '.$l['<title>'].'</center>';

if(empty($vpses)){
	echo '<div class="e_notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['no_vps'].'</div>';
	
}else{

echo '<script language="javascript" type="text/javascript">

function launchvnc(){
	$_("vnc_button").className = "link_btn";
	AJAX("'.$globals['index'].'act=vnc&ajax="+$_("vpsid").value, "vnchandle(re)");
};

function vnchandle(resp){
	$_("vnc_launcher").innerHTML = resp;
	$_("vnc_button").className = "link_btn";
}

function novncvps(){
	if('.(empty($globals['novnc']) ? 'false' :  'true').'){
		var thisURL = window.location.href;
		thisURL = thisURL.toString();
		thisURL = thisURL.replace("http:", "https:");
		thisURL = thisURL.replace(":4084", ":4085");
		$("#novncURL").attr("href", "'.$globals['index'].'act=vnc&novnc="+$_("vpsid").value);
	}
}

$(function() {
	$("#vpsid").select2();
});';

if($globals['novnc']){
	echo 'addonload("novncvps()");';
}

echo '</script>

<br />
<br />
	<div class="row" style="">
		<div class="col-sm-2"></div>
		<div class="col-sm-3 h5 fhead" style="vertical-align:middle;"><span class="pull-right">'.$l['choose_vps'].'</span></div>
		<div class="col-sm-4">
			<select name="vpsid" id="vpsid" style="width:100%;">';
			foreach($vpses as $k => $v){
				echo '<option value="'.$k.'">'.$v['vpsid'].' - '.$v['hostname'].'</option>';
			}
		echo '</select>
		</div>
		<div class="col-sm-4"></div>
	</div>
	<br />
	<br />
	<div class="row" style="text-align:center;">
		<div class="col-sm-2"></div>
		<div class="col-sm-8">
			'.(!empty($globals['novnc']) ? '<a href="" target="blank" id="novncURL" class="link_btn" onclick="novncvps()">'.$l['novnc_button'].'</a>' : '').(empty($globals['disable_java_vnc']) ? ' <input type="button" id="vnc_button" value="'.$l['vnc_button'].'" class="link_btn" onclick="launchvnc();" />' : '').'
		</div>
		<div class="col-sm-2"></div>
	</div>
<div id="vnc_launcher"></div>'; 

}

echo '</div>';
softfooter();

}

?>