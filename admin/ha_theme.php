<?php

//////////////////////////////////////////////////////////////
//===========================================================
// ha_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Ameya
// Date:       27th September 2018
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function ha_theme(){

global $theme, $globals, $kernel, $users, $ha_enabled, $l, $error, $done, $ha_group, $ha_cluster;

softheader($l['<title>']);

echo '
	<div class="bg" style="width: 99%">
		<center class="tit">
			<i class="icon icon-servers icon-head"></i>&nbsp; '.$l['ha_title'].'</center>';

error_handle($error);
echo '<script>
$(document).ready(function(){
	
	
	load_hacluster();
});

function load_hacluster(){
	
	$(".ha_stats").css("display", "none");
	sgid = $("#cserver").val();
	
	$("#sg_id_"+sgid).html("");
	$("#ha_stats_"+sgid).show();
	$("#row_server_loading"+sgid).show();
	
	var ha_html = "";
	$.getJSON("'.$globals['index'].'act=ha&get_ha_stats="+sgid+"&api=json", function(data){
		
		var node_status = data["ha_cluster"][sgid]["node_status"];
		var ha_resources = data["ha_cluster"][sgid]["ha_resources"];
		$("#row_server_loading"+sgid).hide();
			
		ha_html += "<table class=\"table table-striped\" width=\"100%\" cellspacing=\"1\" cellpadding=\"6\" border=\"0\" align=\"center\"> \
			<tr> \
				<th>'.$l['cluster_nodes'].'</th> \
				<th>'.$l['cluster_resource'].'</th> \
			</tr>";
		
		ha_html += "<tr> \
				<td> \
					<table class=\"table table-hover tablesorter\"> \
						<thead> \
							<tr> \
								<th>'.$l['name'].'</th> \
								<th>'.$l['status'].'</th> \
							</tr> \
						</thead> \
						<tbody>";
					$.each(node_status, function(n, ns){
						
						var img = "";
						var img_title = "";
						if(ns != 1){
							img = "offline.png";
							img_title = "'.$l['node_status_inactive'].'";
						}else{
							img = "online.png";
							img_title = "'.$l['node_status_active'].'";
						}
						
						ha_html += "<tr> \
								<td align=\"center\">"+n+"</td> \
								<td align=\"center\"><img src=\"'.$theme['images'].'"+img+"\"  title=\""+img_title+"\"></td> \
							</tr>";
					});
					
		ha_html += "</tbody>\
			</table>\
		</td>\
		<td> \
			<table class=\"table table-hover tablesorter\"> \
				<thead> \
					<tr> \
						<th>'.$l['vps_name'].'</th> \
						<th>'.$l['resource_name'].'</th> \
						<th>'.$l['servers'].'</th> \
						<th>'.$l['status'].'</th> \
					</tr> \
				</thead> \
				<tbody>";
				$.each(ha_resources, function(nr, nres){
					ha_html += "<tr> \
						<td align=\"center\">"+nres["vps_name"]+"</td> \
						<td align=\"center\">"+nr+"</td> \
						<td align=\"center\">"+nres["node"]+"</td> \
						<td align=\"center\">"+nres["status"]+"</td> \
					</tr>";
				});
		ha_html += "</tbody> \
			</table> \
		</td> \
			</tr> \
		</table> \
		<br />";
		
		$("#sg_id_"+sgid).html(ha_html);
		$("#sg_id_"+sgid).show();
	});
}

</script>';
if(empty($ha_enabled)){
	echo '<br /><br />
		<div class="alert alert-warning" role="alert">
		  <p >'.$l['no_cluster'].'</p>
		</div>
	';

}else{
	
	echo '
	<div class="row">
		<div class="col-sm-12">
			<div class="divroundshad">
				<div class="row">
					<div class="hidden-xs col-md-3">
						<label>'.$l['select_ha_group'].'</label>
					</div>
					<div class="col-xs-12 col-md-6">
						<select id="cserver" class="form-control" onchange="load_hacluster();">';
							foreach($ha_group as $k => $v){
								echo '<option value="'.$k.'">'.$v.'</option>';
							}
							echo '
						</select>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br><br>';
	
	// Blank divs to load hrml inside it
	foreach($ha_group as $k => $v){
		echo '<div id="ha_stats_'.$k.'" class="ha_stats" style="display:none;">
			<div id="row_server_loading'.$k.'" class="row" style="display:none;">
				<div class="col-xs-12" style="padding:96px 0; text-align:center;">
					<img src="'.$theme['images'].'progress_bar.gif" height="20" width="20" />
				</div>
			</div>
			<div id="sg_id_'.$k.'"></div>
		</div>';
	}
	
	echo '</div>';
}

softfooter();

}
?>