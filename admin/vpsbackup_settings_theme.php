<?php

//////////////////////////////////////////////////////////////
//===========================================================
// vpsbackup_settings_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 2.1.9
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Julien
// Date:       25th July 2012
// Time:       15:17 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function vpsbackup_settings_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $cluster, $servers, $done, $backupservers, $backup_settings, $vpses, $log, $log_len, $backup_now, $unsupported_arch;
	
softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-databackup icon-head"></i>&nbsp; '.$l['vpsbackups'].'<span style="float:right;" ><a href="'.$globals['docs'].'VPS_Backups" target="_blank" class="wiki_help" title="'.$l['wiki_help'].'"><i class="icon-help" ></i></a></span></center>';

error_handle($error);
// Is it offline ?
$hypervisor_status = $cluster->statewise($globals['server']);
if($hypervisor_status == 0 || $hypervisor_status == 2){

	echo '<div class="e_notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['server_status_'.$hypervisor_status].'</div>';

}else{

	if(!empty($done)){
		echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
	}
	
	if(!empty($backup_now)){
		echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['backup_now_done'].'</div>';
	}

	echo '
	<script language="javascript" type="text/javascript">
				
		$(document).ready(function(){';
		
			if($backup_settings['frequency'] == 'daily'){
				echo '$("#backupday_tr").hide();
					  $("#backupdate_tr").hide();
					  $("#tr_time").show();
					  $("#backuphourly_tr").hide();';
			}elseif($backup_settings['frequency'] == 'weekly'){
				echo '$("#backupday_tr").show();
					 $("#backupdate_tr").hide();
					 $("#tr_time").show();
					 $("#backuphourly_tr").hide();';
			}elseif($backup_settings['frequency'] == 'monthly'){
				echo '$("#backupday_tr").hide();
					  $("#backupdate_tr").show();
					  $("#tr_time").show();
					  $("#backuphourly_tr").hide();';
			}elseif($backup_settings['frequency'] == 'hourly'){
				echo '$("#backupday_tr").hide();
					  $("#backupdate_tr").hide();
					  $("#tr_time").hide();
					  $("#backuphourly_tr").show();';
			}

			if(!empty($backup_settings['bid'])){
				echo '$("#tr_backup_servers").show();
			          $("#tr_local_dir").show();';
				if($backupservers[$backup_settings['bid']]['type'] == 'FTP'){
					echo '$("#type").val("FTP");';
				}else{
					echo '$("#type").val("SSH");';
				}
			}else{
				echo '$("#tr_backup_servers").hide();
			              $("#tr_local_dir").show();
				      $("#type").val("LOCAL");';
			}
		
		echo '	$("input:radio").change(function(event){
				if(event.target.value == "weekly"){
					$("#backupday_tr").show();
					$("#backupdate_tr").hide();
					$("#tr_time").show();
					$("#backuphourly_tr").hide();
				}else if(event.target.value == "monthly"){
					$("#backupdate_tr").show();
					$("#backupday_tr").hide();
					$("#tr_time").show();
					$("#backuphourly_tr").hide();
				}else if(event.target.value == "daily"){
					$("#backupdate_tr").hide();
					$("#backupday_tr").hide();
					$("#tr_time").show();
					$("#backuphourly_tr").hide();
				}else if(event.target.value == "hourly"){
					$("#backupdate_tr").hide();
					$("#backupday_tr").hide();
					$("#tr_time").hide();
					$("#backuphourly_tr").show();
				}
			});

		$("#type").change(function(data){
			if(data.target.value == "LOCAL"){
				$("#tr_dir").show();
				$("#tr_backup_servers").hide();
			}else{
				$("#tr_dir").show();
				$("#tr_backup_servers").show();
			}

		});
			
			';
		if(empty($backup_settings['vpsids'])){
			 echo '$("#vpses").each(function(){
					$("#vpses option").attr("selected", "selected");
			  });';
		}
		
	echo '
		var textarea = document.getElementById("log-area");
		textarea.scrollTop = textarea.scrollHeight;
	});
	
	var log_len = '.$log_len.';	
	function backupnow() {
		$("#backupnow").prop("disabled", true);
		$("#backupnow").val("'.$l['please_wait'].'...");
		
		$.ajax({
			type : "POST",
			url : "'.$globals['index'].'act=vpsbackupsettings&api=json",
			data : "backupnow=true",
			dataType : "json",
			success: function(data){
				$("#backupnow").prop("disabled", false);
				$("#backupnow").val("'.$l['backupnow'].'");
			
				alert("'.$l['backup_now_done'].'");
				
				refresh_logs();
				
				$("body, html").animate({ 
					scrollTop: $("#backups-log").offset().top
				}, "fast");
			}
		});
	}
	
	function show_logs(logs) {
		if(!$.trim(logs)) logs = "'.$l['no_logs'].'";
		
		var textarea = $("#log-area");
		textarea.val(logs);
		textarea.scrollTop(textarea.prop("scrollHeight"));
	}
	
	function clear_logs() {
		$("#clearlog").prop("disabled", true);
		$("#clearlog").val("'.$l['please_wait'].'...");
		
		$.ajax({
			type : "POST",
			url : "'.$globals['index'].'act=vpsbackupsettings&api=json",
			data : "clearlog=true",
			dataType : "json",
			success: function(data){
				$("#clearlog").prop("disabled", false);
				$("#clearlog").val("'.$l['clear_logs'].'");
				
				show_logs(data["backupsettings"]["log"]);
			}
		});
	}
	
	function refresh_logs(recurring) {
		$.ajax({
			type : "POST",
			url : "'.$globals['index'].'act=vpsbackupsettings&api=json",
			data : "loglen=true&len="+log_len,
			dataType : "json",
			success: function(data){
				show_logs(data["backupsettings"]["log"]);
			},
			complete: function() {
				if(recurring) {
					setTimeout(function() {
						refresh_logs(true)
					}, 5000);
				}
			}
		});
	}
	
	function change_log_len() {
		log_len = $("#len").val();
		refresh_logs();
	}
	
	refresh_logs(true);
	
	</script>
	
	<div id="form-container">
	<form accept-charset="'.$globals['charset'].'" action="" method="post" name="vpsbackups" class="form-horizontal">
		<div class="row">
			<div class="col-sm-6">
				<label class="control-label">'.$l['sel_type'].'</label><br />
			</div>
			<div class="col-sm-6">
				<select id="type" class="form-control" name="type">';
				echo '<option value="LOCAL" '.($backupservers[$backup_settings['bid']]['type'] == 'LOCAL' ? 'selected=selected' : "").'>Local</option>';
				if(empty($unsupported_arch)){
					echo '<option value="FTP" '.($backupservers[$backup_settings['bid']]['type'] == 'FTP' ? 'selected=selected' : "").'>FTP</option>
					<option value="SSH" '.($backupservers[$backup_settings['bid']]['type'] == 'SSH' ? 'selected=selected' : "").'>SSH</option>';
				}
			echo '</select>
			</div>
		</div>
		<br/>
		<div class="row">
			<div class="col-sm-6">
				<label class="control-label">'.$l['backup_enabled'].'</label><br />
				<span class="help-block">'.$l['exp_backup_enabled'].'</span>
			</div>
			<div class="col-sm-6">
				<input type="checkbox" id="enabled" class="ios" name="enabled" '.POSTchecked('enabled', $backup_settings['enabled']).'/>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 head">
				<label class="control-label">'.$l['backup_newvps'].'</label><br />
				<span class="help-block">'.$l['exp_backup_newvps'].'</span>
			</div>
			<div class="col-sm-6">
				<input type="checkbox" id="newvps" class="ios" name="newvps" '.POSTchecked('newvps', $backup_settings['newvps']).'/>
			</div>
		</div>
		<div class="row" id="tr_backup_servers">
			<div class="col-sm-6">
				<label class="control-label">'.$l['sel_backupserver'].'</label><br />
				<span class="help-block">'.$l['exp_sel_backupserver'].'</span>
			</div>
			<div class="col-sm-6">
				<select class="form-control" name="id">';
				foreach($backupservers as $k => $v){
					echo '<option value="'.$v['bid'].'" '.ex_POSTselect('id', $v['bid'], $backup_settings['bid']).'>'.$v['name'].'</option>';
				}
			echo '</select>
			</div>
		</div>					
		<div class="row" id="tr_dir">
			<div class="col-sm-6">
				<label class="control-label">'.$l['local_dir'].'</label><br />
				<span class="help-block">'.$l['exp_local_dir'].'</span>
			</div>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="dir" value="'.$backup_settings['dir'].'" size="40"/>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<label class="control-label">'.$l['backup_freq'].'</label><br />
				<span class="help-block">'.$l['exp_backup_freq'].'</span>
			</div>
			<div class="col-sm-6">
				<input type="radio" id="freqh" name="freq" value="hourly" '.POSTradio('freq', 'hourly', $backup_settings['frequency']).'/>
				<label for="freqh">'.$l['freq_hourly'].'</label>
				<input type="radio" id="freqd" name="freq" value="daily" '.POSTradio('freq', 'daily', $backup_settings['frequency']).'/>
				<label for="freqd">'.$l['freq_daily'].'</label>
				<input type="radio" id="freqw" name="freq" value="weekly" '.POSTradio('freq', 'weekly', $backup_settings['frequency']).'/>
				<label for="freqw">'.$l['freq_weekly'].'</label>
				<input type="radio" id="freqm" name="freq" value="monthly" '.POSTradio('freq', 'monthly', $backup_settings['frequency']).'/>
				<label for="freqm">'.$l['freq_monthly'].'</label>
			</div>
		</div>
		<div class="row" id="backuphourly_tr" style="display:none">
			<div class="col-sm-6">
				<label class="control-label">'.$l['backup_hourly'].'</label><br />
				<span class="help-block">'.$l['exp_backup_hourly'].'</span>
			</div>
			<div class="col-sm-4">
				<select class="form-control" name="hourly_freq">';
					for($i=0;$i<24;$i++){
						echo '<option value="'.($i < 10 ? '0'.$i : $i).'" '.ex_POSTSelect('hourly_freq', $i, $backup_settings['hourly_freq']).' >'.($i < 10 ? '0'.$i : $i).'</option>';
					}
			echo'</select>'.$l['hrs'].'
			</div>
		</div>		
		<div class="row" id="backupday_tr" style="display:none;">
			<div class="col-sm-6">
				<label class="control-label">'.$l['backup_day'].'</label><br />
				<span class="help-block">'.$l['exp_backup_day'].'</span>
			</div>
			<div class="col-sm-4">
				<select class="form-control" name="day">
					<option value="1" '.ex_POSTSelect('day', "1", $backup_settings['run_day']).' >Monday</option>
					<option value="2" '.ex_POSTSelect('day', "2", $backup_settings['run_day']).' >Tuesday</option>
					<option value="3" '.ex_POSTSelect('day', "3", $backup_settings['run_day']).' >Wednesday</option>
					<option value="4" '.ex_POSTSelect('day', "4", $backup_settings['run_day']).' >Thursday</option>
					<option value="5" '.ex_POSTSelect('day', "5", $backup_settings['run_day']).' >Friday</option>
					<option value="6" '.ex_POSTSelect('day', "6", $backup_settings['run_day']).' >Saturday</option>
					<option value="7" '.ex_POSTSelect('day', "7", $backup_settings['run_day']).' >Sunday</option>
				</select>
			</div>
		</div>
		<div class="row" id="backupdate_tr" style="display:none;">
			<div class="col-sm-6">
				<label class="control-label">'.$l['backup_date'].'</label><br />
				<span class="help-block">'.$l['exp_backup_date'].'</span>
			</div>
			<div class="col-sm-4">
				<select name="date" class="form-control">';
				for($i=1;$i<32;$i++){
					echo '<option value="'.$i.'" '.ex_POSTSelect('date', $i, $backup_settings['run_date']).' >'.$i.'</option>';
				}
			echo '</select>
			</div>
		</div>
		<div class="row" id="tr_time">
			<div class="col-sm-6">
				<label class="control-label">'.$l['backup_time'].'</label><br />
				<span class="help-block">'.$l['exp_backup_time'].'</span>
			</div>
			<div class="col-sm-4">
				<select class="form-control" name="hrs" style="width:20%;float:left">';
				for($i=0;$i<24;$i++){
					echo '<option value="'.($i < 10 ? '0'.$i : $i).'" '.ex_POSTSelect('hrs', $i, $backup_settings['hrs']).' >'.($i < 10 ? '0'.$i : $i).'</option>';
				}
			echo '</select>
				<label class="side_lbl">'.$l['hrs'].'</label>
				<select class="form-control" name="min" style="width:20%;float:left">';
					for($i=0;$i<60;$i++){
						echo '<option value="'.($i < 10 ? '0'.$i : $i).'" '.ex_POSTSelect('min', $i, $backup_settings['min']).' >'.($i < 10 ? '0'.$i : $i).'</option>';
					}
			echo '</select><label class="side_lbl">'.$l['min'].'</label>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<label class="control-label">'.$l['backup_rotation'].'</label><br />
				<span class="help-block">'.$l['exp_backup_rotation'].'</span>
			</div>
			<div class="col-sm-4">
				<select class="form-control" name="rotation">';
				for($i=1;$i<11;$i++){
					echo '<option value="'.$i.'" '.ex_POSTSelect('rotation', $i, $backup_settings['rotation']).' >'.$i.'</option>';
				}
			echo '</select>
			</div>
		</div>				
		<div class="row">
			<div class="col-sm-6">
				<label class="control-label">'.$l['nice'].'</label><br />
				<span class="help-block">'.$l['nice_exp'].'</span>
			</div>
			<div class="col-sm-4">
				<select class="form-control" name="nice" id="nice">';
				for($i=-20; $i<20; $i++){
					echo '<option value="'.$i.'" '.ex_POSTselect('nice', $i, $backup_settings['nice']).'>'.$i.'</option>';
				}
			echo '</select>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<label class="control-label">'.$l['ionice_prio'].'</label><br />
				<span class="help-block">'.$l['ionice_prio_exp'].'</span>
			</div>
			<div class="col-sm-4">
				<select class="form-control" name="ionice_prio" id="ionice_prio">';
				for($i=0; $i<8; $i++){
					echo '<option value="'.$i.'" '.ex_POSTselect('ionice_prio', $i, $backup_settings['ionice_prio']).'>'.$i.'</option>';
				}
		echo '</select>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<label class="control-label">'.$l['ionice_class'].'</label><br />
				<span class="help-block">'.$l['ionice_class_exp'].'</span>
			</div>
			<div class="col-sm-4">
				<select class="form-control" name="ionice_class" id="ionice_class">
					<option value="1" '.ex_POSTselect('ionice_class', 1, $backup_settings['ionice_class']).' >Real Time</option>
					<option value="2" '.ex_POSTselect('ionice_class', 2, $backup_settings['ionice_class']).'>Best Effort</option>
					<option value="3" '.ex_POSTselect('ionice_class', 3, $backup_settings['ionice_class']).'>Idle</option>
				</select>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<label class="control-label">'.$l['backup_compression'].'</label><br />
				<span class="help-block">'.$l['exp_backup_compression'].'</span>
			</div>
			<div class="col-sm-4">
				<input type="checkbox" id="compression" class="ios" name="compression" '.POSTchecked('compression', $backup_settings['disable_compression']).'/>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<label class="control-label">'.$l['vpses'].'</label><br />
				<span class="help-block">'.$l['vpses_exp'].'</span>
			</div>
			<div class="col-sm-4">
				<select class="form-control" name="vpses[]" id="vpses" multiple="multiple" style="max-width:400px;height:200px;">';
				foreach($vpses as $vpsid => $vps){
						echo '<option value="'.$vpsid.'" '.(in_array($vpsid, @$backup_settings['vpsids']) ? 'selected="selected"' : '').'>'.$vps['vps_name'].' [VID:'.$vps['vpsid'].'] ['.$vps['hostname'].']</option>';
					}
			echo '</select>
			</div>
		</div>
		<br /><br />
		<center><input type="submit" name="vpsbackups"  id="vpsbackups" value="'.$l['submit'].'" class="btn"/></center>

		<br /><br />
	</form>
	
	<center><input type="submit" id="backupnow" name="backupnow" value="'.$l['backupnow'].'" onclick="backupnow();" class="btn"/></center>
	
	</div>
	<br />
	<br/><br/>
	<form accept-charset="'.$globals['charset'].'" action="" method="post" name="clearlog" class="form-horizontal">
		<br/>
		<div id="backups-log">
			<div class="row">
				<div class="col-sm-12" nowrap="nowrap">
				<center>
					<label class="control-label">'.$l['backup_logs'].'</label><br/>
					<textarea id="log-area" rows="20" readonly="readonly" style="width:85%;border: 3px solid #cccccc;padding: 5px;font-family: OpenSans,Courier; overflow:auto; resize:none;" WRAP=OFF>'
					.(!empty($log) ? $log : $l['no_logs']).
					'</textarea>
				</center>
				</div>
			</div>
		</div>
		<br/>
		<br/>
		<center>	
			<input type="submit" name="clearlog" id="clearlog" value="'.$l['clear_logs'].'" onclick="clear_logs(); return false;" class="btn"/>
		</center>
	</form>
    <br/>

	<form id="logs-form" accept-charset="'.$globals['charset'].'" action="" method="post" name="loglen" class="form-horizontal">
		<div class="row">
		<div class="col-sm-4"></div>
		<div class="col-sm-4">
			<label style="float:left;padding: 10px;">'.$l['show_log_lines_1'].'</label> <input size="2" type="text" class="form-control" id="len" name="len" value="'.$log_len.'" style="width:18%;float:left;"/>&nbsp;<label style="padding: 5px;">'.$l['show_log_lines_2'].'</label>&nbsp;&nbsp;
			<input type="submit" id="loglen" name="loglen" value="'.$l['log_len_go'].'" onclick="change_log_len(); return false;" class="btn go_btn"/>
		</div>
		<div class="col-sm-4"></div>
	</form>';

}// End of Server Offline if	

echo '</div>';
softfooter();

}

?>