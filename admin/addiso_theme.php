<?php

//////////////////////////////////////////////////////////////
//===========================================================
// addiso_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function addiso_theme(){	

global $theme, $globals, $kernel, $user, $l, $ipblocks, $error , $cluster, $mgs, $done;

softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-ostemplates icon-head"></i> &nbsp; '.$l['addiso'].'<span style="float:right;" ><a href="'.$globals['docs'].'Add_ISO" target="_blank" class="wiki_help" title="'.$l['wiki_help'].'"><i class="icon-help" ></i></a></span></center>';

error_handle($error);

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
}

echo '<script language="javascript" type="text/javascript">
function validurl(url){
	var tmp = url.replace(/^.*[\/\\\\]/g, \'\');
	$_("base").value = tmp;
};
</script>

<div class="col-sm-12">

<form accept-charset="'.$globals['charset'].'" name="addiso" method="post" action="" class="form-horizontal">

	<div class="row">
		<div class="col-sm-6">
			<label class="control-label">'.$l['isoname'].'</label>
			<span class="help-block">'.$l['isoname_exp'].'</span>
		</div>
		<div class="col-sm-6">
			<input type="text" class="form-control" name="url" id="fileurl" size="40" onblur=validurl(this.value) value="'.POSTval('url', '').'" />
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<label class="control-label">'.$l['isofile'].'</label>
		</div>
		<div class="col-sm-6">
			<input type="text" class="form-control" name="filename" id="base" size="30" value="'.POSTval('filename', '').'" />
			<span class="help-block"></span>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<label class="control-label">'.$l['media_groups'].'</label>
		</div>
		<div class="col-sm-6">
			<select class="form-control" name="mgs[]" multiple="multiple">';
				
				foreach($mgs as $mk => $mv){
					if(!$kernel->features('iso_support', $mv['mg_type'])) continue;
					echo '<option value="'.$mk.'">'.$mv['mg_name'].'</option>';
				}
				
			echo '</select>
			<span class="help-block"></span>
		</div>
	</div>
</div>
<br />
<br />
<center>
	<input type="submit" name="addiso" value="'.$l['submit'].'" class="btn">
</center>
	
</form>
</div>
</div>
';

softfooter();

}

?>