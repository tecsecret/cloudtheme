<?php

//////////////////////////////////////////////////////////////
//===========================================================
// twofactauth_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       4th September 2017
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function twofactauth_theme(){

global $theme, $globals, $kernel, $users, $l, $error, $done, $SESS, $twofactauth;

softheader($l['<title>']);

echo '
<div class="bg" style="width: 99%">
<center class="tit">
<i class="icon icon-users icon-head"></i>&nbsp; '.$l['twofactauth_title'].'</center>';

error_handle($error);

echo '<script language="javascript" type="text/javascript"><!-- // --><![CDATA[

$(document).ready(function(){ 	 	
	$("#twofactauth_passcode").val(""); 	 	
	//$("#otp_email_user").html(); 	 	
	var selected_type = "none"; 	 	
	if(!empty("'.$twofactauth['2fa_type'].'")){ 		
		selected_type = "'.$twofactauth['2fa_type'].'"; 	
	} 

	$("#2fa_type").val(selected_type); 	 	
	// Display the QR Code Always 	
	$("#qrcode").html("<img src=\"'.$twofactauth['qrcode'].'\" />"); 	 	
	// Display the Secret Code Always 	
	$("#secret_key").html("'.$twofactauth['secret_key'].'"); 	
	$("#secret_key_val").val("'.$twofactauth['secret_key'].'"); 	 	
	show_otp_divs(); 
});
	
function reset_otp_key(){
	
	var conf = confirm("'.$l['twofactauth_secret_key_conf'].'");
	
	if(!conf){
		return;
	}
	
	$.ajax({
		type: "GET",
		url: "'.$globals['index'].'act=twofactauth&api=json&reset_secret_key=1",
		dataType : "json",
		success: function(data){
			$("#secret_key").html(data.twofactauth.secret_key);
			$("#secret_key_val").val(data.twofactauth.secret_key);
			$("#qrcode").html("<img src=\""+data.twofactauth.qrcode+"\" />");
		},
		error: function(data) {
			//alert(data.description);
			return false;
		}
	});
	
	return false;
	
}


function show_otp_divs(){
	
	$(".otp_methods_div").css("display", "none");
	
	//alert($("#2fa_type").val())
	var show_div = $("#2fa_type").val()+"_otp_div";
	$("#"+show_div).show();
	if($("#2fa_type").val() != "none"){
		$("#otp_input_div").show();
	}
}

function send_passcode(){
	$("#progress_bar").show();
	
	$.ajax({
		type: "GET",
		url: "'.$globals['index'].'act=twofactauth&api=json&email_passcode=1",
		dataType : "json",
		success: function(data){
			$("#progress_bar").hide();
			
			//alert(data);
			if("done" in data){
				alert(data.done.msg);
			}
			
			if("error" in data){
				error(data.error);
			}
		},
		error: function(data) {
			$("#progress_bar").hide();
			//alert(data.description);
			return false;
		}
	});
	
	return false;
}

function twoFactAuthForm(){
	
	$("#progress_bar").show();
	
	$.ajax({
		type: "POST",
		url: "'.$globals['index'].'act=twofactauth&api=json",
		data :  $("#twofactauthform").serialize(),
		dataType : "json",
		success: function(data){
			$("#progress_bar").hide();
			//alert(JSON.stringify(data));

			if("done" in data){
				alert(data.done.msg);
			}
			
			if("error" in data){
				error(data.error);
			}
		},
		error: function(data) {
			$("#progress_bar").hide();
			//alert(data.description);
			return false;
		}
	});
	
	return false;
}

// ]]></script>';


if($done){
	echo '<center class="head"><img src="'.$theme['images'].'notice.gif" /> &nbsp; <span class="h3">'.$l['saved'].'</span></center>';
}

echo '<br /><br />

<div id="form-container">
<form accept-charset="'.$globals['charset'].'" name="twofactauth" class="form-horizontal" id="twofactauthform" onsubmit="return twoFactAuthForm();">
<div class="row">
	<div class="col-sm-6">
		<label class="val">'.$l['twofactauth_pref'].'</label>
		<span class="help-block">'.$l['twofactauth_pref_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<select class="form-control" name="2fa_type" id="2fa_type" onchange="show_otp_divs();">
			<option value="none">'.$l['none_twofactauth'].'</option>
			<option value="email">'.$l['email_twofactauth'].'</option>
			<option value="app">'.$l['app_twofactauth'].'</option>';
		echo '</select>
	</div>	
</div>

<div class="row otp_methods_div" id="email_otp_div" style="display:none;">
	<div class="col-sm-12">
		<div class="row">
			<div class="col-sm-6">
				<span class="val">'.$l['twofactauth_email_sent_to'].'&nbsp;&nbsp;&nbsp;<b id="otp_email_user">';if(empty($SESS["uid"])){ echo $globals["soft_email"];}else{ echo $SESS["og_name"];} echo '</b></span>
			</div>
			<div class="col-sm-6">
				<button type="button" class="btn" name="email_passcode_but" id="email_passcode_but" onclick="send_passcode();">'.$l['twofactauth_email_passcode'].'</button>
			</div>
		</div>
	</div>
</div><br />
<div class="row otp_methods_div" id="app_otp_div" style="display:none;">
	<div class="col-sm-12">
		<div class="row">
			<div class="col-sm-6">
				'.$l['twofactauth_desc'].'<br />
				<label class="control-label">'.$l['twofactauth_secret_key'].'<span id="secret_key"></span></label><br>
				<a href="#" onclick="reset_otp_key();return false;">'.$l['twofactauth_reset_key'].'</a>
			</div>
			<div class="col-sm-4">
				<div id="qrcode"></div><br />
			</div>
		</div>
	</div>
</div><br />
<div class="row otp_methods_div" id="otp_input_div" style="display:none;">
	<div class="col-sm-6 ">
		<span class="val">'.$l['twofactauth_code_input'].'</span>
		<span class="help-block">'.$l['twofactauth_code_input_exp'].'</span>
	</div>
	<div class="col-sm-4 ">
		<input type="text" name="twofactauth_passcode" maxlength="6" class="form-control" id="twofactauth_passcode" autocomplete="off"/>
	</div>
	<div class="col-sm-2 ">
	</div>
</div><br />

</div>

<br />
<br />
<center>
<input type="hidden" name="twofactauth_save" value="1" />
<input type="hidden" name="secret_key" id="secret_key_val" />
<button class="btn" type="submit"  name="twofactauth_submit" id="twofactauth_submit">'.$l['submit_button'].'</button>
</center>

</form>
<div id="progress_bar" style="height:125px; display:none">
	<br />
	<center class="col-sm-11">
		<font id="progress_txt" size="4" color="#222222">'.$l['action_msg'].'</font>
		<br>
		<br>
		<table id="table_progress" width="450" height="28" cellspacing="0" cellpadding="0" border="0" align="center" style="border:1px solid #CCC; -moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px;background-color:#efefef;">
			<tbody>
				<tr>
					<td id="progress_color" width="100%" style="background-image: url(themes/default/images/bar.gif); -moz-border-radius: 4px; -webkit-border-radius: 4px; border-radius: 4px;"></td>
					<td id="progress_nocolor"> </td>
				</tr>
			</tbody>
		</table>
		<br/>'.$l['notify_msg'].'
	</center>
</div>
';

echo '<br/><br/>
<br/><br/>

</div>';
softfooter();

}
?>