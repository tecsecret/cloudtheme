<?php

//////////////////////////////////////////////////////////////
//===========================================================
// editbackup_plan_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 2.8.1
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Husain
// Date:       16th Nov 2015
// Time:       15:20 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function editbackup_plan_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $backup_plan, $backupservers, $done, $servers, $vpses, $bpid, $servergroups;

softheader($l['<title>']);
echo '
<div class="bg">
<center class="tit"><i class="icon icon-plans icon-head"></i> '.$l['editbackup_plan'].'</center>';

error_handle($error);

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$done.'</div>';
}

echo '<script language="javascript" type="text/javascript">
	
	$(document).ready(function(){';
		
		if($backup_plan['frequency'] == 'daily'){
			echo '$("#backupday_tr").hide();
				  $("#backupdate_tr").hide();
				  $("#tr_time").show();
				  $("#backuphourly_tr").hide();';
		}elseif($backup_plan['frequency'] == 'weekly'){
			echo '$("#backupday_tr").show();
				 $("#backupdate_tr").hide();
				 $("#tr_time").show();
				 $("#backuphourly_tr").hide();';
		}elseif($backup_plan['frequency'] == 'monthly'){
			echo '$("#backupday_tr").hide();
				  $("#backupdate_tr").show();
				  $("#tr_time").show();
				  $("#backuphourly_tr").hide();';
		}elseif($backup_plan['frequency'] == 'hourly'){
			echo '$("#backupday_tr").hide();
				  $("#backupdate_tr").hide();
				  $("#tr_time").hide();
				  $("#backuphourly_tr").show();';
		}
		
		if(!empty($backup_plan['bid'])){
			echo '$("#tr_backup_servers").show();
				  $("#tr_local_dir").show();';
			if($backupservers[$backup_plan['bid']]['type'] == 'FTP'){
				echo '$("#type").val("FTP");';
			}else{
				echo '$("#type").val("SSH");';
			}
		}else{
			echo '$("#tr_backup_servers").hide();
				$("#tr_local_dir").show();
				$("#type").val("LOCAL");';
		}
		
		echo '$("input:radio").change(function(event){
			if(event.target.value == "weekly"){
				$("#backupday_tr").show();
				$("#backupdate_tr").hide();
				$("#tr_time").show();
				$("#backuphourly_tr").hide();
			}else if(event.target.value == "monthly"){
				$("#backupdate_tr").show();
				$("#backupday_tr").hide();
				$("#tr_time").show();
				$("#backuphourly_tr").hide();
			}else if(event.target.value == "daily"){
				$("#backupdate_tr").hide();
				$("#backupday_tr").hide();
				$("#tr_time").show();
				$("#backuphourly_tr").hide();
			}else if(event.target.value == "hourly"){
				$("#backupdate_tr").hide();
				$("#backupday_tr").hide();
				$("#tr_time").hide();
				$("#backuphourly_tr").show();
			}
		});

		$("#type").change(function(data){
			if(data.target.value == "LOCAL"){
				$("#tr_backup_servers").hide();
			}else{
				$("#tr_backup_servers").show();
				if(data.target.value == "FTP"){
					$(".FTP").show();
					$(".SSH").hide();
				}else{
					$(".SSH").show();
					$(".FTP").hide();
				}
			}

		});
		
		refresh_tasks();
		
		// Refresh tasks every 10 secs
		setInterval(refresh_tasks, 10000);

	});
	
	function refresh_tasks() {
		var cols = new Object();
		cols["actid"] = {"l" : "'.$l['actid'].'", "centered" : true};
		cols["vpsid"] = {"l" : "'.$l['vpsid'].'", "centered" : true};
		cols["user"] = {"l" : "'.$l['user'].'", "centered" : true};
		cols["server"] = {"l" : "'.$l['server'].' '.'('.$l['id'].')", "centered" : true};
		cols["started"] = {"l" : "'.$l['started'].'", width: 100, "centered" : true};
		cols["updated"] = {"l" : "'.$l['updated'].'", width: 100, "centered" : true};
		cols["ended"] = {"l" : "'.$l['ended'].'", width: 100, "centered" : true};
		cols["action_txt"] = {"l" : "'.$l['action'].'", width: 100};
		cols["status_txt"] = {"l" : "'.$l['status'].'", width: 200};
		cols["progress"] = {"l" : "'.$l['progress'].'"};
		cols["logs"] = {"l" : "'.$l['logs'].'", "centered" : true};';
		
		$vpsids = array_keys($vpses);
		
		echo '$.ajax({
			type: "GET",
			url: "'.$globals['index'].'act=tasks&vpsid='.implode(',', $vpsids).'&action=vpsbackups_plan,restorevps_plan&api=json&page=0&reslen=50",
			dataType : "json",
			success: function(data){
				
				if(empty(data["tasks"])) {
					// Show no tasks note if table empty
					$("#table_tasks_container").html("<div class=\"notice\"><img src=\"'.$theme['images'].'notice.gif\" /> &nbsp; '.$l['no_tasks'].'</div>");
					return;
				}
				
				var rows = Array();
				
				$.each(data["tasks"], function(k, task) {
					
					rows[k] = new Object();
					rows[k]["actid"] = task["actid"];
					rows[k]["vpsid"] = task["vpsid"];
					rows[k]["user"] = empty(task["uid"]) ? "root" : task["email"];
					rows[k]["server"] = task["server_name"] + "(" + task["serid"] + ")";
					rows[k]["started"] = task["started"];
					rows[k]["updated"] = task["updated"];
					rows[k]["ended"] = task["ended"];
					rows[k]["action_txt"] = task["action_txt"];
					rows[k]["status_txt"] = task["status_txt"];
					rows[k]["progress"] = "<div style=\"width:150px;\" id=\"progress-cont"+task["actid"]+"\"><center><div style=\"font-size:15px;text-align:center;\" id=\"pbar"+task["actid"]+"\">"+task["progress"]+"%</center><div style=\"border-radius:4px;background-color: #ddd;border:1px solid #ccc;\"><div style=\"width:0px;height:18px;background-image: url(themes/default/images/bar.gif); -moz-border-radius: 4px; -webkit-border-radius: 4px; border-radius: 4px;\" id=\"progressbar"+task["actid"]+"\"></div></div></div>";
					rows[k]["logs"] = "<button class=\"btn btn-logs\" onclick=\"loadlogs("+task["actid"]+");\" style=\"margin: 0; padding: 4px 8px; font-size: 12px;\">'.$l['show'].'</button>";
					
				});
				
				rows.sort(function(a, b){
					a = parseInt(a["actid"]);
					b = parseInt(b["actid"]);
					if(a < b) return 1;
					if(a > b) return -1;
					return 0;
				});
				
				table({"id" : "table_tasks_container", "tid" : "table_tasks", "width" : "100%"}, cols, rows);
				
				$.each(data["tasks"], function(index, task) {
					
					if(task["progress"] == 100){
						$("#status"+index).text("'.$l['completed'].'");
						$("#pbar"+index).text("100% '.$l['completed'].' !");
						$("#progressbar"+index).hide();
						$("#progressbar"+index).parent().css("border", "0px");
					}else if(task["status"] == -1){
						$("#status"+index).html(task["status_txt"]);
						$("#pbar"+index).text("'.$l['task_notcomplete'].'");
						$("#progressbar"+index).hide();
						$("#progressbar"+index).parent().css("border", "0px");
					}else {
						
						if(isNaN(task["progress"])){
							$("#pbar"+index).text(task["progress"]);
							$("#progressbar"+index).hide();
						}else{
							make_progress(index, Number(task["progress"]));
							$("#progressbar"+index).show();
						}
					}
					
				});
				
			}
		});
	}

	function make_progress(index, progress){
		var steps = 20;
		var start = $("#pbar"+index).text();
		start = (start.slice(0, -1));
		if(isNaN(start)){
			start = 0;
		}
		var step = (progress - start)/steps;
		if(step < 0){ step = 0;}
		var increment = function(){
			var val = $("#pbar"+index).text();
			val = Number(val.slice(0, - 1));
			prog = parseInt(val + Math.round(step));
			//alert("progr"+prog);
			if(prog > progress){ prog = progress;}
			if(prog < parseInt("100")){
				$("#pbar"+index).text(prog + "%");
				$("#progressbar"+index).width(prog + "%");
			}else{
				$("#progressbar"+index).width(100 + "%");
			}
			if (prog < progress){setTimeout(increment, 500);}
		};
		increment();
	};
	
	function toggle_advoptions(ele){
		
		var div_ele = $("#"+ele+"_advoptions");

		if (div_ele.is(":hidden")){
			$("#"+ele+"_advoptions_ind").text(" - ");
			div_ele.slideDown("slow");
		}
		else{
			$("#"+ele+"_advoptions_ind").text(" + ");
			div_ele.slideUp("slow");
		}
	}
	
</script>

<style type="text/css">

.panel {
	min-height: 120px;
}
.panel-content {
	max-height: 400px;
	overflow: hidden;
}
.panel.panel-blue-grey {
	border-color: #607D8B;
}
.panel.panel-blue-grey .panel-heading {
	background-color: #607D8B;
	border-color: #607D8B;
	color: #fff;
}
.table-dash {
	margin-bottom: 4px;
}
.table-dash tbody tr td {
	vertical-align: middle;
}
.table-dash tr td:first-child, .table-dash tr th:first-child {
	padding-left: 16px;
}
.table-dash tr td:last-child, .table-dash tr th:last-child {
	padding-right: 16px;
}
.btn-logs {
	margin: 0;
	padding: 4px 8px;
	font-size: 12px;
}
table.tablesorter th {
	background-color: #F5F5F5;
}

</style>

<form accept-charset="'.$globals['charset'].'" name="editbackup_plan" method="post" action="" class="form-horizontal">
	<div class="row">
			<div class="col-sm-6">
				<label class="control-label">'.$l['backup_disabled'].'</label><br />
				<span class="help-block">'.$l['exp_backup_disabled'].'</span>
			</div>
			<div class="col-sm-6">
				<input type="checkbox" id="disabled" class="ios" name="disabled" '.POSTchecked('disabled', $backup_plan['disabled']).'/>
			</div>
		</div>
	<div class="row">
		<div class="col-sm-6">
			<label class="control-label">'.$l['planname'].'</label>
			<span class="help-block">'.$l['nameofplan'].'</span>
		</div>
		<div class="col-sm-6">
			<input type="text" class="form-control" name="plan_name" id="plan_name" value="'.POSTval('plan_name', $backup_plan['plan_name']).'" />
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<label class="control-label">'.$l['sel_type'].'</label><br />
		</div>
		<div class="col-sm-6">
			<select id="type" class="form-control" name="type" id="type">
				<option value="LOCAL" '.($backup_plan['bid'] == 0 ? 'selected=selected' : "").'>Local</option>
				<option value="FTP" '.($backup_plan['bid'] != 0 && $backupservers[$backup_plan['bid']]['type'] == 'FTP' ? 'selected=selected' : "").'>FTP</option>
				<option value="SSH" '.($backup_plan['bid'] != 0 && $backupservers[$backup_plan['bid']]['type'] == 'SSH' ? 'selected=selected' : "").'>SSH</option>
			</select>
		</div>
	</div>
	<br/>
	<div class="row" id="tr_backup_servers" style="display:none">
		<div class="col-sm-6">
			<label class="control-label">'.$l['sel_backupserver'].'</label><br />
			<span class="help-block">'.$l['exp_sel_backupserver'].'</span>
		</div>
		<div class="col-sm-6">
			<select class="form-control" name="id">';
			foreach($backupservers as $k => $v){
				echo '<option class="'.$v['type'].'" value="'.$v['bid'].'" '.ex_POSTselect('id', $v['bid'], $backup_plan['bid']).'>'.$v['name'].'</option>';
			}
		echo '</select>
		</div>
	</div>					
	<div class="row" id="tr_dir">
		<div class="col-sm-6">
			<label class="control-label">'.$l['local_dir'].'</label><br />
			<span class="help-block">'.$l['exp_local_dir'].'</span>
		</div>
		<div class="col-sm-6">
			<input type="text" class="form-control" name="dir" id="dir" value="'.$backup_plan['dir'].'" size="40"/>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<label class="control-label">'.$l['backup_freq'].'</label><br />
			<span class="help-block">'.$l['exp_backup_freq'].'</span>
		</div>
		<div class="col-sm-6">
			<input type="radio" id="freqh" name="freq" value="hourly" '.POSTradio('freq', 'hourly', $backup_plan['frequency']).'/>
			<label for="freqh">'.$l['freq_hourly'].'</label>
			<input type="radio" id="freqd" name="freq" value="daily" '.POSTradio('freq', 'daily', $backup_plan['frequency']).'/>
			<label for="freqd">'.$l['freq_daily'].'</label>
			<input type="radio" id="freqw" name="freq" value="weekly" '.POSTradio('freq', 'weekly', $backup_plan['frequency']).'/>
			<label for="freqw">'.$l['freq_weekly'].'</label>
			<input type="radio" id="freqm" name="freq" value="monthly" '.POSTradio('freq', 'monthly', $backup_plan['frequency']).'/>
			<label for="freqm">'.$l['freq_monthly'].'</label>
		</div>
	</div>
	<div class="row" id="backuphourly_tr" style="display:none">
		<div class="col-sm-6">
			<label class="control-label">'.$l['backup_hourly'].'</label><br />
			<span class="help-block">'.$l['exp_backup_hourly'].'</span>
		</div>
		<div class="col-sm-6">
			<select class="form-control" name="hourly_freq" style="width:88%;float:left">';
				for($i=0;$i<24;$i++){
					echo '<option value="'.($i < 10 ? '0'.$i : $i).'" '.ex_POSTSelect('hourly_freq', $i, $backup_plan['hourly_freq']).' >'.($i < 10 ? '0'.$i : $i).'</option>';
				}
		echo'</select><label class="side_lbl">'.$l['hrs'].'</label>
		</div>
	</div>		
	<div class="row" id="backupday_tr" style="display:none;">
		<div class="col-sm-6">
			<label class="control-label">'.$l['backup_day'].'</label><br />
			<span class="help-block">'.$l['exp_backup_day'].'</span>
		</div>
		<div class="col-sm-6">
			<select class="form-control" name="day">
				<option value="1" '.ex_POSTselect('day', "1", $backup_plan['run_day']).' >Monday</option>
				<option value="2" '.ex_POSTselect('day', "2", $backup_plan['run_day']).' >Tuesday</option>
				<option value="3" '.ex_POSTselect('day', "3", $backup_plan['run_day']).' >Wednesday</option>
				<option value="4" '.ex_POSTselect('day', "4", $backup_plan['run_day']).' >Thursday</option>
				<option value="5" '.ex_POSTselect('day', "5", $backup_plan['run_day']).' >Friday</option>
				<option value="6" '.ex_POSTselect('day', "6", $backup_plan['run_day']).' >Saturday</option>
				<option value="7" '.ex_POSTselect('day', "7", $backup_plan['run_day']).' >Sunday</option>
			</select>
		</div>
	</div>
	<div class="row" id="backupdate_tr" style="display:none;">
		<div class="col-sm-6">
			<label class="control-label">'.$l['backup_date'].'</label><br />
			<span class="help-block">'.$l['exp_backup_date'].'</span>
		</div>
		<div class="col-sm-6">
			<select name="date" class="form-control">';
			for($i=1;$i<32;$i++){
				echo '<option value="'.$i.'" '.ex_POSTselect('date', $i, $backup_plan['run_date']).' >'.$i.'</option>';
			}
		echo '</select>
		</div>
	</div>
	<div class="row" id="tr_time">
		<div class="col-sm-6">
			<label class="control-label">'.$l['backup_time'].'</label><br />
			<span class="help-block">'.$l['exp_backup_time'].'</span>
		</div>
		<div class="col-sm-6">
			<select class="form-control" name="hrs" style="width:37%;float:left">';
			$parts = explode(':', $backup_plan['run_time']);
			$hrs = (int) $parts[0];
			$min = (int) $parts[1];
			
			for($i=0;$i<24;$i++){
				echo '<option value="'.($i < 10 ? '0'.$i : $i).'" '.ex_POSTselect('hrs', $i, $hrs).' >'.($i < 10 ? '0'.$i : $i).'</option>';
			}
		echo '</select>
			<label class="side_lbl">'.$l['hrs'].'</label>
			<select class="form-control" name="min" style="width:37%;float:left">';
				for($i=0;$i<60;$i++){
					echo '<option value="'.($i < 10 ? '0'.$i : $i).'" '.ex_POSTselect('min', $i, $min).' >'.($i < 10 ? '0'.$i : $i).'</option>';
				}
		echo '</select><label class="side_lbl">'.$l['min'].'</label>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<label class="control-label">'.$l['backup_rotation'].'</label><br />
			<span class="help-block">'.$l['exp_backup_rotation'].'</span>
		</div>
		<div class="col-sm-6">
			<select class="form-control" name="rotation">';
			for($i = 1; $i < 11; $i++){
				echo '<option value="'.$i.'" '.ex_POSTselect('rotation', $i, $backup_plan['rotation']).' >'.$i.'</option>';
			}
			
			echo '<option value="0" '.ex_POSTselect('rotation', 0, $backup_plan['rotation']).' >'.$l['unlimited'].'</option>';
			
		echo '</select>
		</div>
	</div>
	<div class="row" id="tr_backup_limit">
		<div class="col-sm-6">
			<label class="control-label">'.$l['backup_limit'].'</label><br />
			<span class="help-block">'.$l['exp_backup_limit'].'</span>
		</div>
		<div class="col-sm-6">
			<select class="form-control" name="backup_limit" id="backup_limit">';
			
			echo '<option value="0" '.ex_POSTselect('backup_limit', 0, $backup_plan['backup_limit']).' >'.$l['disabled'].'</option>';
			
			for($i = 1; $i < 11; $i++){
				echo '<option value="'.$i.'" '.ex_POSTselect('backup_limit', $i, $backup_plan['backup_limit']).' >'.$i.'</option>';
			}
			
			echo '<option value="-1" '.ex_POSTselect('backup_limit', -1, $backup_plan['backup_limit']).' >'.$l['unlimited'].'</option>';
			
		echo '</select>
		</div>
	</div>
	<div class="row" id="tr_restore_limit">
		<div class="col-sm-6">
			<label class="control-label">'.$l['restore_limit'].'</label><br />
			<span class="help-block">'.$l['exp_restore_limit'].'</span>
		</div>
		<div class="col-sm-6">
			<select class="form-control" name="restore_limit" id="restore_limit">';
			
			echo '<option value="0" '.ex_POSTselect('restore_limit', 0, $backup_plan['restore_limit']).' >'.$l['disabled'].'</option>';
			
			for($i = 1; $i < 11; $i++){
				echo '<option value="'.$i.'" '.ex_POSTselect('restore_limit', $i, $backup_plan['restore_limit']).' >'.$i.'</option>';
			}
			
			echo '<option value="-1" '.ex_POSTselect('restore_limit', -1, $backup_plan['restore_limit']).' >'.$l['unlimited'].'</option>';
			
		echo '</select>
		</div>
	</div>
	<!-- <div class="row">
		<div class="col-sm-6">
			<label class="control-label">'.$l['enable_enduser_backup_servers'].'</label><br />
			<span class="help-block">'.$l['exp_enable_enduser_backup_servers'].'</span>
		</div>
		<div class="col-sm-6">
			<input type="checkbox" id="enable_enduser_backup_servers" class="ios" name="enable_enduser_backup_servers" '.POSTchecked('enable_enduser_backup_servers', $backup_plan['enable_enduser_backup_servers']).'/>
		</div>
	</div> -->
	<div class="row">
		<div class="col-sm-6">
			<label class="control-label">'.$l['nice'].'</label><br />
			<span class="help-block">'.$l['nice_exp'].'</span>
		</div>
		<div class="col-sm-6">
			<select class="form-control" name="nice" id="nice">';
			for($i=-20; $i<20; $i++){
				echo '<option value="'.$i.'" '.ex_POSTselect('nice', $i, $backup_plan['nice']).'>'.$i.'</option>';
			}
		echo '</select>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<label class="control-label">'.$l['ionice_prio'].'</label><br />
			<span class="help-block">'.$l['ionice_prio_exp'].'</span>
		</div>
		<div class="col-sm-6">
			<select class="form-control" name="ionice_prio" id="ionice_prio">';
			for($i=0; $i<8; $i++){
				echo '<option value="'.$i.'" '.ex_POSTselect('ionice_prio', $i, $backup_plan['ionice_prio']).'>'.$i.'</option>';
			}
	echo '</select>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<label class="control-label">'.$l['ionice_class'].'</label><br />
			<span class="help-block">'.$l['ionice_class_exp'].'</span>
		</div>
		<div class="col-sm-6">
			<select class="form-control" name="ionice_class" id="ionice_class">
				<option value="1" '.ex_POSTselect('ionice_class', 1, $backup_plan['ionice_class']).' >Real Time</option>
				<option value="2" '.ex_POSTselect('ionice_class', 2, $backup_plan['ionice_class']).'>Best Effort</option>
				<option value="3" '.ex_POSTselect('ionice_class', 3, $backup_plan['ionice_class']).'>Idle</option>
			</select>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<label class="control-label">'.$l['io_limit'].'</label><br />
			<span class="help-block">'.$l['exp_io_limit'].'</span>
		</div>
		<div class="col-sm-6">
			<input type="text" class="form-control" name="io_limit" value="'.POSTval('io_limit', $backup_plan['io_limit']).'" />
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<label class="control-label">'.$l['backup_compression'].'</label><br />
			<span class="help-block">'.$l['exp_backup_compression'].'</span>
		</div>
		<div class="col-sm-6">
			<input type="checkbox" id="compression" class="ios" name="compression" '.POSTchecked('compression', $backup_plan['disable_compression']).'/>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<label class="control-label">'.$l['check_dir_permissions'].'</label><br />
			<span class="help-block">'.$l['exp_check_dir_permissions'].'</span>
		</div>
		<div class="col-sm-6">
			<input type="checkbox" id="check_dir_permissions" class="ios" name="check_dir_permissions" '.POSTchecked('check_dir_permissions', true).'/>
		</div>
	</div>
	
	<div class="roundheader hide_for_plan" onclick="toggle_advoptions(\'adv\');" style="cursor:pointer;"><label id="adv_advoptions_ind"  style="width:10px;">+</label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$l['addvoption'].'</div>
	<div id="adv_advoptions" style="display:none; width:100%" class="hide_for_plan">
		<div class="col-sm-12 bgaddv">
			<div class="row">
				<div class="col-sm-12">
					<label class="control-label">'.$l['backup_server_per_region'].'</label>
					<span class="help-block">'.$l['backup_server_per_region_exp'].'</span>
				</div>
				<div class="col-sm-12">
					<div class="scrollbar-virt" style="max-height: 50px; height: 50px;">
						<table class="table table-bordered">
							<tr class="active">
								<th>'.$l['server_group_header'].'</th>
								<th>'.$l['sel_backupserver'].'</th>
								<th>'.$l['local_dir'].'</th>
							</tr>';
							
							foreach($servergroups as $sgid => $servergroup) {
								
								echo '<tr>
									<th style="vertical-align:middle">[Group] '.$servergroup['sg_name'].'</th>
									<th><select class="virt-select" name="bs_sg'.$sgid.'" style="width:100%">
										<option value="0" '.ex_POSTselect('bs_sg'.$sgid, 0, $backup_plan['bs_servergroups']['bs_groups'][$sgid]).'>'.$l['default'].'</option>
										<option value="-1" '.ex_POSTselect('bs_sg'.$sgid, -1, $backup_plan['bs_servergroups']['bs_groups'][$sgid]).'>LOCAL</option>
										<optgroup label="SSH">';
									
											foreach($backupservers as $bid => $bs) {
												if($bs['type'] == 'SSH') {
													echo '<option value="'.$bid.'" '.ex_POSTselect('bs_sg'.$sgid, $bid, $backup_plan['bs_servergroups']['bs_groups'][$sgid]).'>'.$bs['name'].'</option>';
												}
											}
										
										echo '</optgroup>
										<optgroup label="FTP">';
											
											foreach($backupservers as $bid => $bs) {
												if($bs['type'] == 'FTP') {
													echo '<option value="'.$bid.'" '.ex_POSTselect('bs_sg'.$sgid, $bid, $backup_plan['bs_servergroups']['bs_groups'][$sgid]).'>'.$bs['name'].'</option>';
												}
											}
											
										echo '</optgroup>
									</select></th>
									<th><input type="text" class="form-control" name="dir_sg'.$sgid.'" value="'.POSTval('dir_sg'.$sgid, $backup_plan['bs_servergroups']['dir_groups'][$sgid]).'" placeholder="'.$l['default_dir'].'" size="40"/></th>
								</tr>';
								
								foreach($servers as $serid => $server) {
									
									if($server['sgid'] != $sgid) {
										continue;
									}
									
									echo '<tr>
										<td style="vertical-align:middle"> - '.$server['server_name'].' ('.$server['ip'].')</td>
										<th><select class="virt-select" name="bs_s'.$serid.'" style="width:100%">
											<option value="0" '.ex_POSTselect('bs_s'.$serid, 0, $backup_plan['bs_servergroups']['bs_servers'][$serid]).'>'.$l['default'].'</option>
											<option value="-1" '.ex_POSTselect('bs_s'.$serid, -1, $backup_plan['bs_servergroups']['bs_servers'][$serid]).'>LOCAL</option>
											<optgroup label="SSH">';
										
												foreach($backupservers as $bid => $bs) {
													if($bs['type'] == 'SSH') {
														echo '<option value="'.$bid.'" '.ex_POSTselect('bs_s'.$serid, $bid, $backup_plan['bs_servergroups']['bs_servers'][$serid]).'>'.$bs['name'].'</option>';
													}
												}
											
											echo '</optgroup>
											<optgroup label="FTP">';
												
												foreach($backupservers as $bid => $bs) {
													if($bs['type'] == 'FTP') {
														echo '<option value="'.$bid.'">'.$bs['name'].'</option>';
													}
												}
												
											echo '</optgroup>
										</select></th>
										<th><input type="text" class="form-control" name="dir_s'.$serid.'" value="'.POSTval('dir_s'.$serid, $backup_plan['bs_servergroups']['dir_servers'][$serid]).'"  placeholder="'.$l['default_dir'].'" size="40"/></th>
									</tr>';
									
								}
								
							}
							
							echo '</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<br /><br /><br />
	<center>
		<input type="submit" name="editbackup_plan"  id="editbackup_plan" value="'.$l['submit'].'" class="btn" />
		<br /><br />
		<input type="submit" name="backup_now"  id="backup_now" value="'.$l['start_immediate_backup'].'" class="btn" />
	</center>
</form>

<br /><br /><br />

<div class="roundheader" onclick="toggle_advoptions(\'vs\');" style="cursor:pointer;">
	<label id="vs_advoptions_ind" style="width:10px;">+</label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$l['list_vs'].'
</div>
<div id="vs_advoptions" style="display:none; width:100%">
	<div class="bgaddv" style="margin:0; padding:0;">
		<table class="table table-hover tablesorter" style="margin:0">
			<tr>
				<th width="100">'.$l['vpsid'].'</th>
				<th width="100">'.$l['vps_name'].'</th>
				<th>'.$l['server'].'</th>
				<th>'.$l['hostname'].'</th>
				<th>'.$l['space'].'</th>
			</tr>';
			
			if(empty($vpses)) {
				echo '<tr>
					<td colspan="5" align="center"><b>'.$l['no_vps_in_plan'].'</b></td>
				</tr>';
			}
			
			foreach($vpses as $vpsid => $vps) {
				
				echo '<tr>
						<td width="100">'.$vpsid.'</td>
						<td width="100">'.$vps['vps_name'].'</td>
						<td>'.$servers[$vps['serid']]['server_name'].'</td>
						<td>'.$vps['hostname'].'</td>
						<td align="center">'.$vps['space'].' GB</td>
					</tr>';
				
			}
			
		echo '</table>
	</div>
</div>
<br /><br />

<div class="row">
	<div class="col-sm-12">
		<div id="panel-tasks" class="panel panel-blue-grey">
			<div class="panel-heading">
				<span>'.$l['list_tasks'].'</span>
				<i class="icon icon-tasks" style="float:right; font-size:20px; opacity:0.5"></i>
			</div>
			<div class="panel-content scrollbar-virt">
				<div id="table_tasks_container"></div>
			</div>
		</div>
	</div>
</div>
</div>';

softfooter();

}

?>