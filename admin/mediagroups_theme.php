<?php

//////////////////////////////////////////////////////////////
//===========================================================
// vs_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function mediagroups_theme(){

global $theme, $globals, $cluster, $user, $oslist, $ostemplates, $distros, $l, $error, $mediagroups, $servers, $done, $deleted;

softheader($l['<title>']);

echo '<script language="javascript" type="text/javascript"><!-- // --><![CDATA[

function Delmediagrp(mgid){

	mgid = mgid || 0;
	
	// List of ids to delete
	var mediagrp = new Array();
	
	if(mgid < 1){
		
		if($("#mediagrp_task_select").val() != 1){
			alert("'.$l['no_action'].'");
			return false;
		}
		
		$(".ios:checked").each(function() {
			mediagrp.push($(this).val());
		});
		
	}else{
		
		mediagrp.push(mgid);
		
	}
	
	if(mediagrp.length < 1){
		alert("'.$l['nothing_selected'].'");
		return false;
	}
	
	var mediagrp_conf = confirm("'.$l['del_conf'].'");
	if(mediagrp_conf == false){
		return false;
	}
	
	var finalData = new Object();
	finalData["delete"] = mediagrp.join(",");
	
	//alert(finalData);
	//return false;
	
	$("#progress_bar").show();
	
	$.ajax({
		type: "POST",
		url: "'.$globals['index'].'act=mediagroups&api=json",
		data : finalData,
		dataType : "json",
		success: function(data){
			$("#progress_bar").hide();
			if("done" in data){
				alert("'.$l['action_completed'].'");
				location.reload(true);
			}
		},
		error: function(data) {
			$("#progress_bar").hide();
			//alert(data.description);
			return false;
		}
	});
	
	return false;
};

// ]]></script>

<div class="bg" style="width: 99%">
<center class="tit">
<i class="icon icon-ostemplates icon-head"></i>&nbsp; '.$l['page_head'].'<span style="float:right"><a href="javascript:showsearch();"><img src="'.$theme['images'].'admin/search.gif" /></a></span></center>';

error_handle($error);

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
}

if(!empty($deleted)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['deleted'].'</div>';
}

echo '<div id="showsearch" style="display:'.(optREQ('search') || (!empty($mediagroups) && !empty($globals['showsearch'])) ? "" : "none").';">
<form accept-charset="'.$globals['charset'].'" name="ippool" method="GET" action="" class="form-horizontal">
<input type="hidden" name="act" value="mediagroups">
		
<div class="form-group_head">
  <div class="row">
  	<div class="col-sm-1"></div>
    <div class="col-sm-1"><label>'.$l['mgid'].'</label></div>
    <div class="col-sm-2"><input type="text" class="form-control" name="mgid" id="mgid" size="25" value="'.POSTval('mgid','').'"/></div>
    <div class="col-sm-2"><label>'.$l['group_name'].'</label></div>
    <div class="col-sm-2"><input type="text" class="form-control" name="mg_name" id="mg_name" size="25" value="'.POSTval('mg_name','').'"/></div>
    <div class="col-sm-2 sm_space" style="text-align: right;"><button type="submit" name="search" class="go_btn" value="Search"/>'.$l['search_submit'].'</button></div>
  	<div class="col-sm-3"></div>
  </div>
</div>
</form>
<br />
<br />
</div>';

if(empty($mediagroups)){

	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.(optREQ('search') ? $l['no_res'] : $l['no_mediagroups']).'</div>';
	
}else{

page_links($globals['num_res'], $globals['cur_page'], $globals['reslen']);
echo '<br /><br />
<form accept-charset="'.$globals['charset'].'" name="multi_mediagrp" id="multi_mediagrp" method="post" action="" class="form-horizontal">

<table class="table table-hover tablesorter">
<tr>
	<th align="center">'.$l['mgid'].'</th>
	<th align="center">'.$l['mg_type'].'</th>
	<th align="center">'.$l['mg_name'].'</th>
	<th align="center">'.$l['mg_desc'].'</th>
	<th align="center">'.$l['mg_media'].'</th>
	<th align="center" colspan="2">'.$l['manage'].'</th>
	<th><input type="checkbox" class="select_all" name="select_all" id="select_all"></th>
</tr>';

$i = 1;

foreach($mediagroups as $k => $v){

echo '<tr>
	<td>'.$k.'</td>
	<td align="center"><img src="'.$theme['images'].'admin/'.$v['mg_type'].'_42.gif" /></td>
	<td>'.$v['mg_name'].'</td>
	<td>'.$v['mg_desc'].'</td>
	<td>'.(empty($v['mg_media']) ? '<em>'.$l['none'].'</em>' : implode(', ', $v['mg_media'])).'</td>
	<td align="center" width="24">
		<a href="'.$globals['ind'].'act=editmg&mgid='.$k.'" title="'.$l['edit'].'"><img src="'.$theme['images'].'admin/edit.png" /></a>
	</td>
	<td align="center" width="24">
		<a href="javascript:void(0);" onclick="return Delmediagrp('.$k.');" title="'.$l['delete'].'"><img src="'.$theme['images'].'admin/delete.png" /></a>
	</td>
	<td width="20" valign="middle" align="center">
		<input type="checkbox" class="ios" name="mediagrp_list[]" value="'.$k.'"/>
	</td>
</tr>';
	
	$i++;

}		
	
echo '</table>
<div class="row bottom-menu">
<div class="col-sm-7"></div>
<div class="col-sm-5"><label>'.$l['with_selected'].'</label>
<select class="form-control" name="mediagrp_task_select" id="mediagrp_task_select">
		<option value="0">---</option>
		<option value="1">'.$l['ms_delete'].'</option>
	</select>&nbsp;
<input type="submit" id ="mediagrp_submit" class="go_btn" name="mediagrp_submit" value="Go" onclick="Delmediagrp(); return false;">
</div>
</div>
</form>
<div id="progress_bar" style="height:125px; display:none">
	<br />
	<center>
		<font id="progress_txt" size="4" color="#222222">'.$l['action_msg'].'</font>
		<br>
		<br>
	</center>
	<table id="table_progress" width="500" height="28" cellspacing="0" cellpadding="0" border="0" align="center" style="border:1px solid #CCC; -moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px;background-color:#efefef;">
		<tbody>
			<tr>
				<td id="progress_color" width="100%" style="background-image: url(themes/default/images/bar.gif); -moz-border-radius: 4px; -webkit-border-radius: 4px; border-radius: 4px;"></td>
				<td id="progress_nocolor"> </td>
			</tr>
		</tbody>
	</table>
	<br>
	<center>
		'.$l['notify_msg'].'
	</center>
</div>
<br />
<center><a href="'.$globals['ind'].'act=addmg" class="link_btn">'.$l['add_mg'].'</a></center><br />';

}

page_links($globals['num_res'], $globals['cur_page'], $globals['reslen']);
echo '</div>';
softfooter();

}

?>