<?php

//////////////////////////////////////////////////////////////
//===========================================================
// editdnsplan_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 2.1.7
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Julien
// Date:       9th July 2012
// Time:       00:51 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function editdnsplan_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $done, $dns_servers, $dnsplan;

softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-pdns icon-head"></i>'.$l['edit_dns_plan'].'</center>';

error_handle($error);

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
}

echo '<script language="javascript" type="text/javascript">
	
</script>

<form accept-charset="'.$globals['charset'].'" name="editdnsplan" method="post" action="" class="form-horizontal">

<div class="row">
	<div class="col-sm-5">
		<label class="control-label">'.$l['planname'].'</label><br />
		<span class="help-block">'.$l['nameofplan'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="plan_name" id="plan_name" size="30" value="'.POSTval('plan_name', $dnsplan['plan_name']).'" />
	</div>
</div>
<div class="row">
	<div class="col-sm-5">
		<label class="control-label">'.$l['dnsserver'].'</label><br />
		<span class="help-block">'.$l['nameofdnsserver'].'</span>
	</div>
	<div class="col-sm-6">	
		<select class="form-control" name="dnsserverid">';
		foreach($dns_servers as $k => $v){
			echo '<option value="'.$v['id'].'" '.ex_POSTselect('dnsserverid', $v['id'], $dnsplan['pdnsid']).' >'.$v['name'].'</option>';
		}
echo   '</select>
	</div>
</div>
<div class="row">
	<div class="col-sm-5">
		<label class="control-label">'.$l['maxdomains'].'</label><br />
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="maxdomains" id="maxdomains" size="10" value="'.POSTval('maxdomains', $dnsplan['max_domains']).'" />
		<span class="help-block"></span>
	</div>	
</div>
<div class="row">
	<div class="col-sm-5">
		<label class="control-label">'.$l['maxdomainsrec'].'</label><br />	
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="maxdomainsrec" id="maxdomainsrec" size="10" value="'.POSTval('maxdomainsrec', $dnsplan['max_domain_records']).'" />
		<span class="help-block"></span>
	</div>	
</div>
<div class="row">
	<div class="col-sm-5">
		<label class="control-label">'.$l['ttl'].'</label><br />
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="ttl" id="ttl" size="10" value="'.POSTval('ttl', $dnsplan['def_ttl']).'" />
		<span class="help-block"></span>
	</div>	
</div>
<br />
<center><input type="submit" name="editdnsplan" value="'.$l['submit'].'" class="btn"></center>
</form>
</div>
</div>
';
softfooter();

}

?>