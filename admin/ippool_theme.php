<?php

//////////////////////////////////////////////////////////////
//===========================================================
// ippool_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function ippool_theme(){

global $theme, $globals, $servers, $user, $l, $cluster, $error, $ippools, $done, $ippool_servers, $servergroups, $ipgp;

softheader($l['<title>']);

echo '
<div class="bg" style="width: 99%">
<center class="tit">
<i class="icon icon-ippool icon-head"></i>&nbsp; '.$l['ip_pool'].'<span style="float:right"><a href="javascript:showsearch();"><img src="'.$theme['images'].'admin/search.gif" /></a></span></center>';

error_handle($error); 

echo '<script language="javascript" type="text/javascript"><!-- // --><![CDATA[

function Delippool(ippid){

	ippid = ippid || 0;
	
	// List of ids to delete
	var ippid_list = new Array();
	
	if(ippid < 1){
		
		if($("#ippool_task_select").val() != 1){
			alert("'.$l['no_action'].'");
			return false;
		}
		
		$(".ios:checked").each(function() {
			ippid_list.push($(this).val());
		});
		
	}else{
		
		ippid_list.push(ippid);
		
	}

	var finalData = new Object();
	finalData["delete"] = ippid_list.join(",");
	
	if(ippid_list.length < 1){
		alert("'.$l['nothing_selected'].'");
		return false;
	}
	
	var ippool_conf = confirm("'.$l['del_conf'].'");
	if(ippool_conf == false){
		return false;
	}
	
	//alert(finalData);
	//return false;
	
	$("#progress_bar").show();
	
	$.ajax({
		type: "POST",
		url: "'.$globals['index'].'act=ippool&api=json",
		data : finalData,
		dataType : "json",
		success: function(data){
			$("#progress_bar").hide();
			if("done" in data){
				alert("'.$l['action_completed'].'");
			}
			if("error" in data){
				alert(data["error"]);
			}
			location.reload(true);
		},
		error: function(data) {
			$("#progress_bar").hide();
			//alert(data.description);
			return false;
		}
	});
	
	return false;
};

// ]]></script>

<div id="showsearch" style="display:'.(optREQ('search') || (!empty($ippools) && !empty($globals['showsearch'])) ? "" : "none").';">
<form accept-charset="'.$globals['charset'].'" name="ippool" method="GET" action="" class="form-horizontal">
<input type="hidden" name="act" value="ippool">

<div class="form-group_head">
  <div class="row">
	<div class="col-md-4">
		<div class="col-md-4"><label>'.$l['poolname'].'</label></div>
		<div class="col-md-8"><input type="text" class="form-control" name="poolname" id="poolname" size="30" value="'.REQval('poolname','').'"/></div>
	</div>
	<div class="col-md-4">	
		<div class="col-md-4"><label>'.$l['poolgateway'].'</label></div>
		<div class="col-md-8"><input type="text" class="form-control" name="poolgateway" id="poolgateway" size="30" value="'.REQval('poolgateway','').'"/></div>
	</div>
	<div class="col-md-4">
		<div class="col-md-4"><label>'.$l['netmask'].'</label></div>
		<div class="col-md-8"><input type="text" class="form-control" name="netmask" id="netmask" size="30" value="'.REQval('netmask', '').'" /></div>
	</div>
  </div>
  <div class="row">
  	<div class="col-md-4">
		<div class="col-md-4"><label>'.$l['nameserver'].'</label></div>
		<div class="col-md-8"><input type="text" class="form-control" name="nameserver" id="nameserver" size="30" value="'.REQval('nameserver', '').'" /></div>
	</div>
	<div class="col-md-4">
		<div class="col-md-4"><label>'.$l['ipp_server'].'</label></div>
		<div class="col-sm-8 server-select-lg">
			<select name="servers_search" id="servers_search" class="form-control virt-select" style="width:100%">
				<option value="-1" '.(REQval('servers_search') == -1 ? 'selected="selected"' : '').'>'.$l['all_servers'].'</option>';
			
				foreach($servers as $key => $id){
					echo '<option value="'.$key.'" '.(isset($_REQUEST['servers_search']) && REQval('servers_search') == $key ? 'selected="selected"' : '').'>'.$id['server_name'].'</option>';
				}
		echo '</select>
    </div>
	</div>	
  </div>
  <div class="row">
    <div class="col-sm-12" style="text-align: center;"><button type="submit" name="search" class="go_btn" value="Search"/>'.$l['submit'].'</button></div>
  </div>  
</div>
</form>
<br />
<br />
</div>';

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
}

if(empty($ippools)){

	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.(optREQ('search') ? $l['no_res'] : $l['no_pools']).'</div>';
	
}else{
	
$i = 1;



page_links($globals['num_res'], $globals['cur_page'], $globals['reslen']);
echo '<br /><br />
<form accept-charset="'.$globals['charset'].'" name="multi_ippool" id="multi_ippool" method="post" action="" class="form-horizontal">
<table class="table table-striped table-hover tablesorter">
	<tr>
		<th align="center">'.$l['pool_id'].'</th>
		<th align="center">'.$l['pool_name'].'</th>
		<th align="center">'.$l['ipp_server'].'</th>
		<th align="center">'.$l['type'].'</th>
		<th align="center" width="32">'.$l['subnet-nat'].'</th>
		<th align="center" width="32">'.$l['vlan'].'</th>
		<th align="center" width="32">'.$l['internal'].'</th>
		<th align="center">'.$l['total_ip'].'</th>
		<th align="center" width="35"><span title="'.$l['locked_title'].'">'.$l['locked_head'].'</span></th>
		<th align="center">'.$l['free_ip'].'</th>
		<th align="center">'.$l['subnets'].'</th>
		<th align="center" colspan="3">'.$l['manage'].'</th>
		<th><input type="checkbox" class="select_all" name="select_all" id="select_all"></th>
	</tr>';

foreach($ippools as $k => $v){

echo '<tr>
		<td align="left">'.$k.'</a></td>
		<td>'.$v['ippool_name'].'</a></td>
		<td>'.@implode(", ", $ipgp[$v['ippid']]).'</td>
		<td align="center">'.($v['ipv6'] ? '<font color="green">IPv6</font>' : 'IPv4').'</td>
		<td align="center">'.(empty($v['nat']) ? 'No' : '<font color="green">Yes</font>').'</td>
		<td align="center">'.(empty($v['vlan']) ? 'No' : '<font color="green">Yes</font>').'</td>
		<td align="center">'.(empty($v['internal']) ? 'No' : '<font color="green">Yes</font>').'</td>
		<td align="center">'.(empty($v['totalip']) ? 0 : '<a href="'.$globals['ind'].'act=ips&ippid='.$k.'" style="text-decoration:none;" title="'.$l['view_ips'].'">'.$v['totalip'].' <img src="'.$theme['images'].'admin/link.gif" style="vertical-align:middle;" /></a>').'</td>
		<td align="center">'.(empty($v['total_locked_ips']) ? '0' : '<a href="'.$globals['ind'].'act=ips&ippid='.$k.'&lockedsearch=showlocked" style="text-decoration:none;" title="'.$l['view_ips'].'">'.$v['total_locked_ips'].' <img src="'.$theme['images'].'admin/link.gif" style="vertical-align:middle;" /></a>').'</td>
		<td align="center">'.(empty($v['freeip']) ? 0 : $v['freeip']).'</td>
		<td align="center">'.(empty($v['subnets']) ? 0 : $v['subnets']).' ('.(empty($v['free_subnets']) ? 0 : $v['free_subnets']).' '.$l['free_subnet'].') '.(empty($v['subnets']) ? '' : '<a href="'.$globals['ind'].'act=ipranges&ippid='.$k.'" style="text-decoration:none;" title="'.$l['view_subnets'].'"><img src="'.$theme['images'].'admin/link.gif" style="vertical-align:middle;" /></a>').'</td>
		<td align="center" width="24" ><a href="'.$globals['ind'].'act=addips&ippid='.$k.'" title="'.$l['add_ip'].'"><img src="'.$theme['images'].'admin/add.png" /></a></td>
		<td align="center" width="24"><a href="'.$globals['ind'].'act=editippool&ippid='.$k.'" title="'.$l['edit_pool'].'"><img src="'.$theme['images'].'admin/edit.png" /></a></td>
		<td align="center" width="24" ><a href="javascript:void(0);" onclick="return Delippool('.$k.');"  title="'.$l['del_pool'].'"><img src="'.$theme['images'].'admin/delete.png" /></a></td>
		<td width="20" valign="top" align="center">
			<input type="checkbox" class="ios" name="ippool_list[]" value="'.$k.'"/>
		</td>
  	 </tr>';
	
	$i++;
}

echo '</table>
<div class="row bottom-menu">
		
	<div class="col-sm-7"></div>
	<div class="col-sm-5">
		<label>'.$l['with_selected'].'</label>
		<select name="ippool_task_select" id="ippool_task_select" class="form-control">
			<option value="0">---</option>
			<option value="1">'.$l['ms_delete'].'</option>
		</select>&nbsp;
		<input type="submit" id ="ippool_submit" class="go_btn" name="ippool_submit" value="Go" onclick="Delippool(); return false;">
	</div>
</div>
</form>

<div id="progress_bar" style="height:125px; display:none">
	<br />
	<center>
		<font id="progress_txt" size="4" color="#222222">'.$l['action_msg'].'</font>
		<br>
		<br>
	</center>
	<table id="table_progress" width="500" height="28" cellspacing="0" cellpadding="0" border="0" align="center" style="border:1px solid #CCC; -moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px;background-color:#efefef;">
		<tbody>
			<tr>
				<td id="progress_color" width="100%" style="background-image: url(themes/default/images/bar.gif); -moz-border-radius: 4px; -webkit-border-radius: 4px; border-radius: 4px;"></td>
				<td id="progress_nocolor"> </td>
			</tr>
		</tbody>
	</table>
	<br>
	<center>
		'.$l['notify_msg'].'
	</center>
</div>';

}

page_links($globals['num_res'], $globals['cur_page'], $globals['reslen']);

echo '<br /><br />
<center><button onclick="window.location.href=\''.$globals['ind'].'act=addippool\'" class="link_btn">'.$l['add_ippool'].'</button></center></div>
</div>
';

softfooter();

}

?>