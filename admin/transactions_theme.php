<?php

//////////////////////////////////////////////////////////////
//===========================================================
// transactions_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function transactions_theme(){	

global $theme, $globals, $user, $l, $error, $done, $transactions;

softheader($l['<title>']);
	
echo '
<div class="bg" style="width: 99%">
<center class="tit">
<i class="icon icon-billing icon-head"></i>&nbsp; '.$l['_head'].' 
<span style="float:right"><a href="javascript:showsearch();"><img src="'.$theme['images'].'admin/search.gif" /></a></span>
</center>';
	
error_handle($error);

if(empty($globals['inhouse_billing'])){
	echo '<div class="alert alert-danger">'.$l['enable_billing'].'</div>';
}

echo '<script language="javascript" type="text/javascript"><!-- // --><![CDATA[
var from_moveips = 0;
function show_confirm(ipid){
	
	ipid = ipid || 0;
	
	ipsids = new Array();
	
	selopts = $("#selected_do_options option");
	selectvals = new Array();

	for(i=0; i<selopts.length; i++){
		val = selopts.eq(i).val();
		if(val != 0){
			selectvals.push(val);
		}
	}
	
	confirmmsg = ["'.$l["del_conf"].'"];
	donemsg = ["'.$l["done"].'"];
	selectIndx = selectvals.indexOf($_("selected_do_options").value);

	if(ipid < 1){

		if(selectIndx == -1){
			alert("'.$l['no_action'].'");
			return false;
		}
	
		$(".ios:checked").each(function() {
			ipsids.push($(this).val());
		});
	
	}else{
		ipsids.push(ipid);
		selectIndx = 0;
	}
	
	if(ipsids.length < 1){
		alert("'.$l['nothing_selected'].'");
		return false;
	}
	
	var finalips = new Object();
	
	var r = confirm(confirmmsg[selectIndx]);
	if(r != true){
		return false;
	}
	
	finalips["do"] = selectvals[selectIndx];
	finalips["ips"] = ipsids.join(",");
	
	// API backward compatibility
	if(selectvals[selectIndx] == "del"){
		finalips["delete"] = finalips["ips"];
	}
	
	$("#progress_bar").show();
	
	$.ajax({
		type: "POST",
		url: "'.$globals['index'].'act=transactions&api=json",
		data: finalips,
		dataType : "json",
		success:function(response){
			$("#progress_bar").hide();
			if("done" in response){
				alert(donemsg[selectIndx]);
				location.reload(true);
			}
			if("error" in response){
				alert(response["error"]);
				//location.reload(true);
			}
		},
		error: function(data) {
			$("#progress_bar").hide();
			alert("'.$l['error_occurred'].'");
			return false;
		}
	});
	
	return false;
};

// ]]></script>

<div id="showsearch" style="display:'.(optREQ('search') || (!empty($transactions) && !empty($globals['showsearch'])) ? "" : "none").';">
<form accept-charset="'.$globals['charset'].'" name="transactions" method="GET" action="" class="form-horizontal">
<input type="hidden" name="act" value="transactions">
		
<div class="form-group_head">
	<div class="row">
		<div class="col-sm-2"><label>'.$l['sby_trid'].'</label></div>
		<div class="col-sm-2"><input type="text" class="form-control" name="trid" id="sby_trid" size="30" value="'.REQval('trid', '').'"/></div>
		<div class="col-sm-2"><label>'.$l['sby_uid'].'</label></div>
		<div class="col-sm-2"><input type="text" class="form-control" name="uid" id="sby_uid" size="30" value="'.REQval('uid','').'"/></div>
		<div class="col-sm-2"><label>'.$l['sby_invoid'].'</label></div>
		<div class="col-sm-2"><input type="text" class="form-control" name="invoid" id="sby_invoid" size="30" value="'.REQval('invoid', '').'"/></div>
	</div>
	<div class="row">
		<div class="col-sm-2"><label>'.$l['sby_date'].'</label> (YYYY/MM/DD)</div>
		<div class="col-sm-2"><input type="text" class="form-control" name="date" id="sby_date" size="30" value="'.REQval('date','').'"/></div>
		<div class="col-sm-2"><label>'.$l['sby_gateway'].'</label></div>
		<div class="col-sm-2"><input type="text" class="form-control" name="gateway" id="sby_gateway" size="30" value="'.REQval('gateway','').'"/></div>
		<div class="col-sm-2"><label>'.$l['sby_token'].'</label></div>
		<div class="col-sm-2"><input type="text" class="form-control" name="token" id="sby_token" size="30" value="'.REQval('token','').'"/></div>
	</div>
	<div class="row">
		<div class="col-sm-2"><label>'.$l['sby_amt'].'</label></div>
		<div class="col-sm-2"><input type="text" class="form-control" name="amt" id="sby_amt" size="30" value="'.REQval('amt','').'"/></div>
		<div class="col-sm-2"><label>'.$l['sby_fees'].'</label></div>
		<div class="col-sm-2"><input type="text" class="form-control" name="fees" id="sby_fees" size="30" value="'.REQval('fees','').'"/></div>
		<div class="col-sm-2"><label>'.$l['sby_net'].'</label></div>
		<div class="col-sm-2"><input type="text" class="form-control" name="net" id="sby_net" size="30" value="'.REQval('net','').'"/></div>
	</div>
	<div class="row">
		<div class="col-sm-12" style="text-align: center;"><button type="submit" name="search" class="go_btn" value="Search" style="margin-left:30px;">'.$l['submit'].'</button></div>
	</div>
</div>
<br />
<br />
</form>
</div>';

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
}

if(empty($transactions)){

	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.(optREQ('search') ? $l['no_res'] : $l['no_trans']).'</div>';
	
}else{

page_links($globals['num_res'], $globals['cur_page'], $globals['reslen']);

echo '<br /><br />
<form name="transactions" id="transactions" method="post" action="" accept-charset="'.$globals['charset'].'" class="form-horizontal">
<table class="table table-hover tablesorter">
	<tr>
		<th align="center">'.$l['th_trid'].'</th>
		<th align="center">'.$l['th_email'].'</th>
		<th align="center">'.$l['th_date'].'</th>
		<th align="center">'.$l['th_gateway'].'</th>
		<th align="center">'.$l['th_token'].'</th>
		<th align="center">'.$l['th_amt'].'</th>
		<th align="center">'.$l['th_fees'].'</th>
		<th align="center">'.$l['th_net'].'</th>
		<th align="center" colspan="2">'.$l['manage'].'</th>
		<th><input type="checkbox" class="select_all" name="select_all" id="select_all"></th>
	</tr>';
	
foreach($transactions as $k => $v){
	
	echo '<tr>
		<td align="left">'.$k.'</td>
		<td>'.$v['email'].'</td>
		<td>'.datetime($v['date']).'</td>
		<td align="center">'.(empty($v['invoid']) ? $v['gateway'] : $l['sby_invoid'].' : '.$v['invoid']).'</td>
		<td>'.$v['token'].'</td>
		<td align="right">'.$v['amt'].'</td>
		<td align="right">'.$v['fees'].'</td>
		<td align="right">'.$v['net'].'</td>
		<td align="center" width="24" >
			<a href="'.$globals['ind'].'act=edittransaction&trid='.$k.'" title="'.$l['edit_trans'].'"><img src="'.$theme['images'].'admin/edit.png" /></a>
		</td>
		<td align="center" width="24" >
			<a href="javascript:void(0);" onclick="return show_confirm('.$k.');"  title="'.$l['del_trans'].'"><img src="'.$theme['images'].'admin/delete.png" /></a>
		</td>
		<td align="center" width="24" >
			<input type="checkbox" class="ios" name="trids[]" id="trid_'.$k.'" value="'.$k.'" />
		</td>
	</tr>';

	
	$i++;
}

echo '</table><br />';
		
}	

echo '
<div class="row bottom-menu">
		
	<div class="col-sm-7"></div>
	<div class="col-sm-5">
	
		<label>'.$l['with_selected'].'</label>
		<select name="selected_do_options" id="selected_do_options" class="form-control">
			<option value="0">---</option>
			<option value="del">'.$l['ms_delete'].'</option>
		</select>&nbsp;
		<input type="submit" class="go_btn" value="'.$l['go'].'" onclick="show_confirm(); return false;" />
	
	</div>
</div>
</form>';

page_links($globals['num_res'], $globals['cur_page'], $globals['reslen']);

echo '<center><input type="button" value="'.$l['create_trans'].'" class="link_btn" onclick="window.location =\''.$globals['ind'].'act=addtransaction\';"></center>
<div id="progress_bar" style="height:125px; display:none">
	<br />
	<center>
		<font id="progress_txt" size="4" color="#222222">'.$l['action_msg'].'</font>
		<br>
		<br>
	</center>
	<table id="table_progress" width="500" height="28" cellspacing="0" cellpadding="0" border="0" align="center" style="border:1px solid #CCC; -moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px;background-color:#efefef;">
		<tbody>
			<tr>
				<td id="progress_color" width="100%" style="background-image: url(themes/default/images/bar.gif); -moz-border-radius: 4px; -webkit-border-radius: 4px; border-radius: 4px;"></td>
				<td id="progress_nocolor"> </td>
			</tr>
		</tbody>
	</table>
	<br />
</div>

</div>
';

softfooter();

}

