<?php

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function getSortHeader($header_name, $sort_column, $sort_column_by, $column){

	global $theme, $globals;
		
		$str = '<table style="margin:auto;">
	<tr>
		<th rowspan="2" class="tablehead" style="background: none;border: none;color:black;padding-right:6px;">'.$header_name.'</th>
		<td width="10"><a href="'.$globals['index'].'act=vps_stats&sortcolumn='.$column.'&sortby=asc"><img src="'.$theme['images'].'admin/arrow_up.png"></a></td>
	</tr>
	<tr>
		<td><a href="'.$globals['index'].'act=vps_stats&sortcolumn='.$column.'&sortby=desc"><img src="'.$theme['images'].'admin/arrow_down.png"></a></td>
	</tr>
	</table>';
	
	return $str;
}

// Each Vps Stats
function vps_stats_theme(){

	global $theme, $globals, $ckernel, $user, $l, $vps_stats, $pie_data, $vps_data, $sort_column, $sort_column_by, $notice, $servers, $kernel;
	
	//Wrap up each vps data in td
	$table_data = '';
	
	softheader($l['<title>']);
	
	if(!empty($notice['no_vps'])){
		
		echo '<div class="bg">
				<center class="tit">
					<i class="icon icon-vs icon-head"></i>&nbsp; '.$l['page_head'].'
			</center>';
		
		server_select();
		
		echo '<div class="notice" id="note_box">
			<img src="'.$theme['images'].'notice.gif" />&nbsp; '.$l['no_vps'].'</div></div>';
		
			
	}else{
		
		//Initialize the pie chart values of vps bandwidth, cpu and ram
		$values_cpu = $values_ram = $values_bandwidth = $labels = array();
		
		// Total CPU and Bandwidth of all VPSs
		$total_cpu = 0;
		$total_band = 0;
		
		foreach($vps_data as $k => $v){
			
			// Values to plot the PIE
			
			$values_ram[$v['vpsid']] = '{"label" : "VPS ID : '.$v['vpsid'].' VPS Name : '.$v['vps_name'].'", data: '.$v['used_ram'].'}';
			
			$total_cpu += $v['used_cpu'];
			$values_cpu[$v['vpsid']] = '{"label" : "VPS ID : '.$v['vpsid'].' VPS Name : '.$v['vps_name'].'", data: '.$v['used_cpu'].'}';
			
			$total_band += $v['used_bandwidth'];
			$values_bandwidth[$v['vpsid']] = '{"label" : "VPS ID : '.$v['vpsid'].' VPS Name : '.$v['vps_name'].'", data: '.$v['used_bandwidth'].'}';
			
			//Check the status of the vps
			if($v['status'] == 1){
				$tmp = '<td align="center" id="spcpu_'.$k.'" ><div class="spcpu">'.(float)$v['used_cpu'].','.(100 - $v['used_cpu']).'</div><div class="small_txt">'.(float)$v['used_cpu'].'%</div></td>
								'.($kernel->features('vps_ram_info', $v['virt']) ? '<td align="center" id="spram_'.$k.'" ><div class="spram">'.(int)$v['used_ram'].','.((int)$v['ram'] - (int)$v['used_ram']).'</div><div class="small_txt">'.(int)$v['used_ram'].' / '.(int)$v['ram'].' MB</div></td>' : '<td align="center" id="spram_'.$k.'" >N/A</td>').'
								<td align="center" id="spdisk_'.$k.'"><div class="spdisk">'.($v['used_disk']).','.($v['disk'] - $v['used_disk']).'</div><div class="small_txt">'.round($v['used_disk'], 2).' / '.$v['disk'].' GB</div></td>
								'.(empty($v['inode']) ? '<td align="center" id="spinode_'.$k.'"> N/A </td>' : '<td align="center" id="spinode_'.$k.'"><div class="spinode">'.$v['used_inode'].','.($v['inode'] - $v['used_inode']).'</div><div class="small_txt">'.$v['used_inode'].' / '.$v['inode'].'</div></td>');
				$status_td = '<td align="center" id="status_'.$k.'"><img title="'.$l['vps_stat_on'].'" src="'.$theme['images'].'online.png" /></td>';				
			}elseif($v['status'] == 2){
				$status_td = '<td align="center" id="status_'.$k.'"><img title="'.$l['vps_stat_suspend'].'" src="'.$theme['images'].'suspended.png" /></td>';
				$tmp = '<td colspan="4" style="text-align: center" id="nodata_'.$k.'">'.$l['no_data'].'</td>';
			}else{
				$status_td = '<td align="center" id="status_'.$k.'"><img title="'.$l['vps_stat_off'].'" src="'.$theme['images'].'offline.png" /></td>';
				$tmp = '<td colspan="4" style="text-align: center" id="nodata_'.$k.'">'.$l['no_data'].'</td>';
			}
			
			//Check if speed cap is enabled
			if($v['speed_cap'] == '0' || $v['speed_cap'] == NULL){
				$speedCap = '<td align="center"><span><img src="'.$theme['images'].'offline.png" title="'.$l['vps_speed_cap_disabled'].'"></span></td>';
			}else{
				$speed_cap = explode(',',$v['speed_cap']);
				$speedCap = '<td align="center">'.$speed_cap[1].' KB <img src="'.$theme['images'].'admin/arrow_up.png" title="'.$l['vps_upload_speed'].'"><br>'.$speed_cap[0].' KB <img src="'.$theme['images'].'admin/arrow_down.png" title="'.$l['vps_download_speed'].'"></td>';
			}
				
			$table_data .= '<tr>
								'.$status_td.'
								<td id="vpsid_'.$k.'">'.$v['vpsid'].'</td>
								<td id="hostname_'.$k.'">'.$v['hostname'].' ('.$v['vps_name'].')</td>
								'.$tmp.'
								<td>
										<div class="spband" align="center" id="band_'.$k.'">
											'.$v['used_bandwidth'].','.(!empty($v['bandwidth']) ? $v['bandwidth'] - $v['used_bandwidth'] : 9999999).'
										</div>
										<div class="small_txt" align="center" id="band2_'.$k.'">
											'.$v['used_bandwidth'].'/'.(!empty($v['bandwidth']) ? $v['bandwidth'] - $v['used_bandwidth'] : $l['unlimited']).' GB
										</div>
								</td>
								<td align="center" id="netin_'.$k.'">'.byte_convert($v['net_in'], 2).'</td>
								<td align="center" id="netout_'.$k.'">'.byte_convert($v['net_out'], 2).'</td>
								<td align="center" id="io_read_'.$k.'">'.byte_convert($v['io_read'], 2).'</td>
								<td align="center" id="io_write_'.$k.'">'.byte_convert($v['io_write'], 2).'</td>
								'.$speedCap.'
								<td align="center"><span onclick="show_graph('.$v['vpsid'].')"><img style="cursor:hand;cursor:pointer;" src="'.$theme['images'].'admin/graph.png" title="'.$l['graph_display'].'"></span></td>
							</tr>';  
			
		}
		
		// Handle Server Ram 
		foreach($pie_data['server_ram'] as $k => $v){
			
			if($k == 0 && !empty($v['used_ram'])){
				$values_ram[$k] = '{"label" : "Server '.$l['free'].'&nbsp;'.$l['ram'].'", data: '.($v['ram'] - $v['used_ram']).'}';
				
			// DOM-0 RAM for XEN and XCP	
			}elseif($k == -1 && !empty($v['used_ram'])){
				$values_ram[$k] = '{"label" : "Dom0 '.$l['used'].'&nbsp;'.$l['ram'].'", data: '.$v['used_ram'].'}';
				$values_ram[-2] = '{"label" : "Dom0 '.$l['free'].'&nbsp;'.$l['ram'].'", data: '.($v['ram'] - $v['used_ram']).'}';
			}
		}
		
		// Handle Server CPU
		if(!empty($pie_data['server_cpu'])){
			
			// The actual CPU % free on main node retuned from server stats
			$values_cpu[0] = '{"label" : "Server '.$l['free'].'", data: '.$pie_data['server_cpu']['percent_free'].'}';
			
			// There is a possibility of total CPU % of all VPSs being greater than the actual CPU used % of the server
			if($total_cpu < $pie_data['server_cpu']['percent']){
				// CPU % utlized by the server itself apart from the VPSs
				$values_cpu[-1] = '{"label" : "Server '.$l['used'].'", data: '.($pie_data['server_cpu']['percent'] - $total_cpu).'}';
			}
			
		}
		
		// Handle Server Bandwidth
		if(!empty($pie_data['server_bandwidth'])){
			
			$values_bandwidth[0] = '{"label" : "Server '.$l['used'].'", data: '.$pie_data['server_bandwidth']['used_gb'].'}';
			
			$free = 0; // Server Free GB
			// Show Free bandwidth if there is any
			if($pie_data['server_bandwidth']['limit_gb'] > ($total_band + $pie_data['server_bandwidth']['used_gb'])){
				$free = $pie_data['server_bandwidth']['limit_gb'] - ($total_band + $pie_data['server_bandwidth']['used_gb']);
				$values_bandwidth[-1] = '{"label" : "Server '.$l['free'].'", data: '.$free.'}';
			}
		}
		
		echo '<style type="text/css">
		
		.spcpu, .spram, .spdisk, .spinode, .spband {
			margin-bottom: 4px;
		}
		
		.small_txt {
			font-size: 11px;
		}
		
		</style>
		
		<script language="javascript" type="text/javascript">
		
		//Assign a global variable to remember the vpsid when page loads
		vpsid = 0;
		
		var monthFullNames = ["'.$l['january'].'", "'.$l['february'].'", "'.$l['march'].'", "'.$l['april'].'", "'.$l['may'].'", "'.$l['june'].'", "'.$l['july'].'", "'.$l['august'].'", "'.$l['september'].'", "'.$l['october'].'", "'.$l['november'].'", "'.$l['december'].'" ];
		
		// Show the data for bandwidth, CPU, RAM
		function plot_pie(id, data){
			
			var plot = $.plot(id, data,{
				series: {
					pie: { 
						show: true,
						radius: 3/4,
						label: {
							show: false
						},
						stroke: { 
							width: 0.1,
							color: "#408080"
						}
					}
				},
				grid: {
					hoverable: true,
					clickable: true
				},
				legend: {
					show: false
				}
			});
			
			id.bind("plothover", pieHover);
		}
			
		function showTooltip(x, y, contents) {
			$(\'<div id="tooltip">\' + contents + \'</div>\').css( {
				position: "absolute",
				display: "none",
				top: y ,
				left: x + 20,
				border: "1px solid #CCCCCC",
				padding: "2px",
				"background-color": "#EFEFEF",
				opacity: 0.80
			}).appendTo("body").fadeIn(200);
		}

		var previousPoint = null;
		function pieHover(event, pos, obj){
			
			if (!obj){
				$("#tooltip").remove();
				previousPoint = null;
				return;
			}
			
			if (previousPoint != obj.seriesIndex) {
				previousPoint = obj.seriesIndex;
				$("#tooltip").remove();
			
				var tmp = ""+obj.series.data;
				var ram = tmp.split(",");
				
				var idOfPie = event.target.getAttribute(\'id\');
				idOfPie = idOfPie.split("_");
				var pieChartName = "";
				var pieChartSign = "";
				
				if(idOfPie[1] == "cpu"){
					pieChartName = "CPU";
					pieChartSign = "%";

				}else if(idOfPie[1] == "ram"){
					pieChartName = "RAM";
					pieChartSign = "MB";
					
				}else if(idOfPie[1] == "bandwidth"){
					pieChartName = "Bandwidth";
					pieChartSign = "GB";
				}
				
				showTooltip(pos.pageX, pos.pageY, obj.series.label+"<br /> "+pieChartName+" : "+ram[1]+" "+pieChartSign);		
			}		
		}
			
		function show_graph(id){
			
			vpsid = id;
			
			//CPU as default window
			change_monitor_tabs("cpuinfo");
			
			var dialogTitle = \'<img src="'.$theme['images'].'admin/graph.png">&nbsp;&nbsp; VPSID \'+id+\' &nbsp;&nbsp;<span id="img_loading"></span>\';
			
			$("#dialog").dialog({
				title: dialogTitle,
				resizable: true,
				modal: true,
				width: 900,
				maxHeight: 600
			}).prev(".ui-dialog-titlebar").css("background","#444444");
			
			//Only scroll on div is allowed not on page
			$( "#dialog" ).bind( "mousewheel DOMMouseScroll", function ( e ) {
				var e0 = e.originalEvent,
					delta = e0.wheelDelta || -e0.detail;
				
				this.scrollTop += ( delta < 0 ? 1 : -1 ) * 30;
				e.preventDefault();
			});
			
			//Display loading image
			$("#img_loading").html("<img src='.$theme['images'].'loading_35.gif width=26px height=26px/>");
			
			$.post("'.$globals['index'].'act=vps_stats&jsnohf=1&api=json&svs="+id+"", function(data){
					data = JSON.parse(data);
					
					if(data.vps_stats){
						show_monthly_graph(data.vps_stats, data.month);
					}else{
						var req_month = parseInt(data.month.current_month.toString().substring(4));
						$("#note_box").css("display","block");
						$("#for_month").html(monthFullNames[req_month - 1]);
					}
					$("#img_loading").html("");
			});
		};	
			
		// Show monthly graph
		function show_monthly_graph(monthly_data, month){
			
			if(monthly_data){
				
				$("#note_box").css("display","none");
			
				var cpu_data = new Array();
				var inode_data = new Array();
				var ram_data = new Array();
				var disk_data = new Array();
				var ntw_in_data = new Array();
				var ntw_out_data = new Array();
				var ntw_total_data = new Array();
				var io_read_data = new Array();
				var io_write_data = new Array();
				
				if(monthly_data){
				
					$.each(monthly_data,function(key,val){
						
						//Array is in format [0:vpsid, 1:time, 2:status, 3:disk, 4:inode, 5:ram,  6:cpu, 7:actual_cpu, 8:net_in, 9:net_out]
					
						cpu_data.push([val[1], val[7]]);
							
						inode_data.push([val[1], val[4]]);
							
						ram_data.push([val[1], val[5]]);	
						
						disk_data.push([val[1], (val[3]/1024)]);
						
						ntw_in_data.push([val[1], val[8]]);
						
						ntw_out_data.push([val[1], val[9]]);
						
						ntw_total_data.push([val[1], parseInt(val[8]) + parseInt(val[9])]);

						io_read_data.push([val[1], val[10]]);

						io_write_data.push([val[1], val[11]]);
					});
				
				}
				
				// LEFT TO check
				cpu_data.sort(function(a, b){ 
					return a[0]-b[0]; 
				});
				
				var cpu_graph = [
							{ label: "'.$l['cpu_utilization'].'",  data: cpu_data}
						];
						
				var inode_graph = [
						{ label: "'.$l['inode'].'",  data: inode_data, color: "#80b3ff"}
					];	
					
				var ram_graph = [
						{ label: "'.$l['ram_utilization'].'",  data: ram_data, color: "#ccff33"}
					];	
					
				var disk_graph = [
						{ label: "'.$l['disk'].'",  data: disk_data, color: "#ff6600"}
					];	

				var io_read_graph = [
					{ label: "'.$l['io_read_spd'].'",  data: io_read_data, color: "#0077FF"},
				];

				var io_write_graph = [
					{ label: "'.$l['io_write_spd'].'",  data: io_write_data, color: "#FF6600"},
				];
						
				var ntw_graph = [
						{ label: "'.$l['download_graph_title'].'",  data: ntw_in_data, color: "#0077FF"},
						{ label: "'.$l['upload_graph_title'].'",  data: ntw_out_data , color: "#0000A0"},
						{ label: "'.$l['total_graph_title'].'",  data: ntw_total_data}
					];	
				
				selection_zooming("cpu_plot", cpu_graph);
				live_resource_graph("cpu_plot", cpu_graph, flot_options("cpu_plot"), "% at ",true);

				selection_zooming("ram_plot", ram_graph);
				live_resource_graph("ram_plot", ram_graph, flot_options("ram_plot"), "MB at ",true);
				
				selection_zooming("disk_plot", disk_graph);
				live_resource_graph("disk_plot", disk_graph, flot_options("disk_plot"), "GB at ",true);

				selection_zooming("inode_plot", inode_graph);
				live_resource_graph("inode_plot", inode_graph, flot_options("inode_plot"), "Blocks at ",true);
			
				selection_zooming("io_read_plot", io_read_graph);
				live_resource_graph("io_read_plot", io_read_graph, flot_options("io_read_plot"), "Mb/s at ",true);
				
				selection_zooming("io_write_plot", io_write_graph);
				live_resource_graph("io_write_plot", io_write_graph, flot_options("io_write_plot"), "Mb/s at ",true);
				
				selection_zooming("ntw", ntw_graph);
				live_resource_graph("ntw", ntw_graph, flot_options("ntw"), "",true);
				
				var monthNames = ["'.$l['jan'].'", "'.$l['feb'].'", "'.$l['mar'].'", "'.$l['apr'].'", "'.$l['may'].'", "'.$l['jun'].'", "'.$l['jul'].'", "'.$l['aug'].'", "'.$l['sep'].'", "'.$l['oct'].'", "'.$l['nov'].'", "'.$l['dec'].'" ];
				
				var current_year = month.current_month.substring(0,4);
				var current_month = parseInt(month.current_month.substring(4));
				$("#month_holder2").html(monthNames[current_month - 1] +" "+ current_year);
				$("#next_month").html(\'<input id="next_stats" type="submit" class="form-control btn-primary" style="width:150px;height:30px;" onclick="search_result(\'+ month.next_month +\')" value="'.$l['next_month'].'" />\');
				$("#prev_month").html(\'<input id="next_stats" type="submit" class="form-control btn-primary" style="width:150px;height:30px;" onclick="search_result(\'+ month.prev_month +\')" value="'.$l['prev_month'].'" />\');
						
			}else{
				$("#note_box").css("display","block");
			}
		}
		
		function search_result(mon){
			
			//Display loading image
			$("#img_loading").html("<img src='.$theme['images'].'loading_35.gif width=26px height=26px/>");
			$.post("'.$globals['index'].'act=vps_stats&jsnohf=1&api=json&svs="+ vpsid +"&show=" + mon +"", function(data){
				data = JSON.parse(data);
				if(data.vps_stats){
					show_monthly_graph(data.vps_stats, data.month);
				}else{
					var req_month = parseInt(mon.toString().substring(4));
					$("#note_box").css("display","block");
					$("#for_month").html(monthFullNames[req_month - 1]);
				}
				$("#img_loading").html("");
			}); 
			
		};
			
		// Handle for selection and zooming
		function selection_zooming (id, data){

			$("#"+id).bind("plotselected", function (event, ranges) {
				if (ranges.xaxis.to - ranges.xaxis.from < 0.00001) {
					ranges.xaxis.to = ranges.xaxis.from + 0.00001;
				}
				if (ranges.yaxis.to - ranges.yaxis.from < 0.00001) {
					ranges.yaxis.to = ranges.yaxis.from + 0.00001;
				}
				options = flot_options(id);
				plot = $.plot("#"+id, data,
					$.extend(true, {}, options, {
						xaxis: { min: ranges.xaxis.from, max: ranges.xaxis.to },
						yaxis: { min: ranges.yaxis.from, max: ranges.yaxis.to }
					})
				);
				
				//Lets append zoom out button if its not present
				if($("#zoomOut_"+id) != undefined){
					$("#zoomOut_"+id).remove();
				}
				
				$("<input type=\'button\' style=\'position:absolute;right:15px;top:15px;opacity:0.5;\' id=\'zoomOut_\'"+ id +" value=\'Zoom Out\'>").appendTo($("#"+id)).click(function(e){
					e.preventDefault();
					options = flot_options(id);
					$.plot("#"+id, data, options);
					$("#zoomOut_"+id).remove();
				});	
			});
		};
		
		// Call for the options
		function flot_options(optionOf){
			var options = {
					grid: {
						borderWidth:0,
						labelMargin:0,
						axisMargin:0,
						minBorderMargin:0
					},
					legend: {
						show: true,
						noColumns: 3,
					},
					series: {
						lines: {
							show: true,
							lineWidth: 0.07,
							fill: true
						}
					},
					xaxis: {
						show:true,
						mode: "time",
						tickFormatter: function (v, axis) {
							return nDate(v,"m/d");
						},
						labelWidth: 25,
						axisLabelUseCanvas: true,
						axisLabelFontSizePixels: 12,
						axisLabelFontFamily: "Verdana, Arial",
						axisLabelPadding: 10,
					},
					yaxis: {
						show:true,
						min: 0,
						max: null,
						labelWidth: 50,
						axisLabelUseCanvas: true,
						axisLabelFontSizePixels: 12,
						axisLabelFontFamily: "Verdana, Arial",
					},
					selection: {
						mode: "x"
					},	
					grid: {
						borderWidth: 1,
						borderColor: "#FFF",
						hoverable: true,
					}
				};
				
			if(optionOf == "cpu_plot"){
			
				//Appending options for cpu
				options.yaxis.tickFormatter = function (v) {
					if(v <= 1024)
						return Math.round(v) + " %";
				};
				options.legend.container = $("#legend_cpu");
			
			}else if(optionOf == "ram_plot"){
				
				//Appending options for ram
				options.yaxis.tickFormatter = function (v) {
					if(v <= 1024)
						return Math.round(v) + " MB";
					if(v > 1024 && v < (1024*1024))
						return Math.round(v /1024) + " GB";
					if(v > (1024*1024))
						return Math.round(v / (1024*1024)) + " TB"
				};
				options.legend.container = $("#legend_ram");
				
			}else if(optionOf == "disk_plot"){
			
				//Appending options for Disk
				options.yaxis.tickFormatter = function (v) {
					if(v <= 1024)
						return Math.round(v) + " GB";
					if(v > 1024 && v < (1024*1024))
						return Math.round(v /1024) + " TB";
				};
				options.legend.container = $("#legend_disk");
			
			}else if(optionOf == "inode_plot"){
			
				//Appending option for INodes
				options.yaxis.tickFormatter = "";
				options.legend.container = $("#legend_inode");
			
			}else if(optionOf == "io_read_plot" || optionOf == "io_write_plot"){
			
				//Appending option for INodes
				options.yaxis.tickFormatter = function (v) {
					if(v <= 1024)
						return Math.round(v) + " B/s";
					if(v > 1024 && v < (1024*1024))
						return Math.round(v /1024) + " KB/s";
					if(v > (1024*1024) && v < (1024*1024*1024))
						return Math.round(v / (1024*1024)) + " MB/s"
					if(v > (1024*1024*1024))
						return Math.round(v / (1024*1024*1024)) + " GB/s"
				};
				options.legend.container = (optionOf == "io_read_plot" ? $("#legend_io_read") : $("#legend_io_write"));
			
			}else if(optionOf == "ntw"){
			
				//Appending  option for Network
				options.yaxis.tickFormatter = function (v) {
					if(v <= 1024)
						return Math.round(v) + " B/s";
					if(v > 1024 && v < (1024*1024))
						return Math.round(v / 1024) + " KB/s";
					if(v > (1024*1024) && v < (1024*1024*1024))
						return Math.round(v / (1024*1024)) + " MB/s";
					if(v > (1024*1024*1024))
						return Math.round(v / (1024*1024*1024)) + " GB/s";
				};
				options.legend.container = $("#legend_ntw");
				
			}
				
			return options;
		};
			
		function change_monitor_tabs(id){
			$("#cpuinfo_win, #raminfo_win, #diskinfo_win, #ntwinfo_win, #ioinfo_win").hide();
			$("#"+id+"_win").show();
		};
			
		$(document).ready(function(){
			
			cleartime = setTimeout(function(){
			   //window.location.reload(1);
			   refresh_stats();
			}, 30000);
			
			// Call the function to display RAM pie chart
			var data = ['.implode(', ', $values_ram).'];
			plot_pie($("#holder_ram"), data);
			
			// Call the function to display CPU pie chart
			var data = ['.implode(', ', $values_cpu).'];
			plot_pie($("#holder_cpu"), data);
	
			// Call the function to display Bandwidth pie chart
			var data = ['.implode(', ', $values_bandwidth).'];
			plot_pie($("#holder_bandwidth"), data);
			
			manage_sparkline();
			
		}); 
		
		function refresh_stats(){
			
			$.ajax({
				url: window.location.href +"&api=json",
				success: function(resp){
				
					resp = JSON.parse(resp);
					var total_cpu = 0;
					var total_band = 0;
					var v_ram = [];
					var v_cpu = [];
					var v_bandwidth = [];
					var table_data = "";
					var tmp = "";
					var status_td = "";
					
					$.each(resp.vps_data, function(i, t){
					
						v_ram.push({"label" : "VPS ID : "+t.vpsid+" VPS Name : "+t.vps_name, data: parseFloat(t.used_ram)});
							
						total_cpu += parseFloat(t.used_cpu);
						v_cpu.push({"label" : "VPS ID : "+t.vpsid+" VPS Name : "+t.vps_name, data: parseFloat(t.used_cpu)});
							
						total_band += parseFloat(t.used_bandwidth);
						v_bandwidth.push({"label" : "VPS ID : "+t.vpsid+" VPS Name : "+t.vps_name, data: parseFloat(t.used_bandwidth)});
							
						if(t.status == 1){
							tmp += "<td align=\"center\" id=\"spcpu_"+i+"\" ><div class=\"spcpu\">"+parseFloat(t.used_cpu)+","+(100 - t.used_cpu)+"</div><div class=\"small_txt\">"+parseFloat(t.used_cpu)+" %</div></td>";
							tmp += ((t.vps_ram_info) ? "<td align=\"center\" id=\"spram_"+i+"\" ><div class=\"spram\">"+parseInt(t.used_ram)+","+(parseInt(t.ram) - parseInt(t.used_ram))+"</div><div class=\"small_txt\">"+parseInt(t.used_ram)+" / "+parseInt(t.ram)+" MB</div></td>" : "<td align=\"center\" id=\"spram_"+i+"\" >N/A</td>");
							tmp += "<td align=\"center\" id=\"spdisk_"+i+"\"><div class=\"spdisk\">"+(t.used_disk)+","+(t.disk - t.used_disk)+"</div><div class=\"small_txt\">"+(t.used_disk.toFixed(2))+" / "+(t.disk)+" GB</div></td>";
							tmp += ((t.inode == "") ? "<td align=\"center\" id=\"spinode_"+i+"\"> N/A </td>" : "<td align=\"center\" id=\"spinode_"+i+"\"><div class=\"spinode\">"+t.used_inode+","+(t.inode - t.used_inode)+"</div><div class=\"small_txt\">"+t.used_inode+" / "+(t.inode)+"</div></td>");
								
							status_td = "<td align=\"center\" id=\"status_"+i+"\"><img title=\"'.$l['vps_stat_on'].'\" src=\"'.$theme['images'].'online.png\" /></td>";
								
						}else if(t.status == 2){
							status_td = "<td align=\"center\" id=\"status_"+i+"\"><img title=\"'.$l['vps_stat_suspend'].'\" src=\"'.$theme['images'].'suspended.png\" /></td>";
							tmp += "<td colspan=\"4\" style=\"text-align: center\" id=\"nodata_"+i+"\">'.$l['no_data'].'</td>";
						}else{
							status_td = "<td align=\"center\" id=\"status_"+i+"\"><img title=\"'.$l['vps_stat_off'].'\" src=\"'.$theme['images'].'offline.png\" /></td>";
							tmp += "<td colspan=\"4\" style=\"text-align: center\" id=\"nodata_"+i+"\">'.$l['no_data'].'</td>";
						}

						if(t.speed_cap == "0" || t.speed_cap == 0 || t.speed_cap == null){
							speedCap = "<td align=\"center\"><span><img src=\"'.$theme['images'].'offline.png\" title=\"'.$l['vps_speed_cap_disabled'].'\"></span></td>";
						}else{
							tmp_data = t.speed_cap.split(",");
							speedCap = "<td align=\"center\">"+tmp_data[1]+" KB <img src=\"'.$theme['images'].'admin/arrow_up.png\" title=\"'.$l['vps_upload_speed'].'\"><br>"+tmp_data[0]+" KB <img src=\"'.$theme['images'].'admin/arrow_down.png\" title=\"'.$l['vps_download_speed'].'\"></td>";
						}
							
						table_data += "<tr>";
						table_data += status_td;
						table_data += "<td id=\"vpsid_"+i+"\">"+t.vpsid+"</td>";
						table_data += "<td id=\"hostname_"+i+"\">"+t.hostname+" ("+t.vps_name+")</td>";
						table_data += tmp;
						table_data += "<td>";
						table_data += "<div class=\"spband\" align=\"center\" id=\"band_"+i+"\">";
						table_data += t.used_bandwidth+","+((t.bandwidth != "") ? (t.bandwidth - t.used_bandwidth).toFixed(2) : 9999999);
						table_data += "</div>";
						table_data += "<div class=\"small_txt\" align=\"center\" id=\"band2_"+i+"\">";
						table_data += t.used_bandwidth+"/"+((t.bandwidth != "") ? (t.bandwidth - t.used_bandwidth).toFixed(2) : "'.$l['unlimited'].'")+" GB";
						table_data += "</div>";
						table_data += "</td>";
						table_data += "<td align=\"center\" id=\"netin_"+i+"\">"+byte_convert(t.net_in, 2)+"</td>";
						table_data += "<td align=\"center\" id=\"netout_"+i+"\">"+byte_convert(t.net_out, 2)+"</td>";
						table_data += "<td align=\"center\" id=\"io_read_"+i+"\">"+byte_convert(t.io_read, 2)+"</td>";
						table_data += "<td align=\"center\" id=\"io_write_"+i+"\">"+byte_convert(t.io_write, 2)+"</td>";
						table_data += speedCap;
						table_data += "<td align=\"center\"><span onclick=\"show_graph("+t.vpsid+")\"><img style=\"cursor:hand;cursor:pointer;\" src=\"'.$theme['images'].'admin/graph.png\" title=\"'.$l['graph_display'].'\"></span></td>";
						table_data += "</tr>";
									
						tmp = "";
					});
						
					var pie_data = resp.pie_data;
						
					// Handle Server Ram
					$.each(pie_data.server_ram, function(i, t){
						if(i == 0 && (t.used_ram != "")){
							v_ram.push({"label" : "Server '.$l['free'].'&nbsp;'.$l['ram'].'", data: parseFloat(t.ram - t.used_ram)});
								
						// DOM-0 RAM for XEN and XCP
						}else if(i == -1 && (t.used_ram != "")){
							v_ram.push({"label" : "Dom0 '.$l['used'].'&nbsp;'.$l['ram'].'", data: parseFloat(t.used_ram)});
							v_ram.push({"label" : "Dom0 '.$l['free'].'&nbsp;'.$l['ram'].'", data: (parseFloat(t.ram) - parseFloat(t.used_ram))});
						}
					});
						
					// Handle Server CPU
					if(pie_data.server_cpu != ""){
						v_cpu.push({"label" : "Server '.$l['free'].'", data: parseFloat(pie_data.server_cpu.percent_free)});
							
						if(total_cpu < pie_data.server_cpu.percent){
							v_cpu.push({"label" : "Server '.$l['used'].'", data: (parseFloat(pie_data.server_cpu.percent) - parseFloat(total_cpu))});
						}
					}
						
					// Handle Server Bandwidth
					if(pie_data.server_bandwidth != ""){
							
						v_bandwidth.push({"label" : "Server '.$l['used'].'", data: parseFloat(pie_data.server_bandwidth.used_gb)});
							
						var free = 0; // Server Free GB
						// Show Free bandwidth if there is any
						if(pie_data.server_bandwidth.limit_gb > (total_band + pie_data.server_bandwidth.used_gb)){
							free = parseFloat(pie_data.server_bandwidth.limit_gb) - (total_band + parseFloat(pie_data.server_bandwidth.used_gb));
							v_bandwidth.push({"label" : "Server '.$l['free'].'", data: free});
						}
					}
						
					$("#servers_list_table > tbody").html("");
						
					$("#servers_list_table > tbody").html(table_data);
						
					// Call the function to display RAM pie chart
					plot_pie($("#holder_ram"), v_ram);
						
					// Call the function to display CPU pie chart
					plot_pie($("#holder_cpu"), v_cpu);
				
					// Call the function to display Bandwidth pie chart
					plot_pie($("#holder_bandwidth"), v_bandwidth);
						
					manage_sparkline();
					
					setTimeout(function(){
					   //window.location.reload(1);
					   refresh_stats();
					}, 30000);
				}
				
				
			});
		}
		
		function manage_sparkline(){
		
			// CPU Sparklines
			$("#servers_list_table .spcpu").sparkline("html", {
															type:"pie",
															sliceColors: ["#434348", "#f7a35c"],
															tooltipClassname: "spPieToolTip jqstooltip",
															tooltipFormatter: function(a, b, fields){
																return "<div style=\'padding:2px;\'>"+fields["value"]+" % "+(fields["offset"] == 0 ? "'.$l['used'].'" : "'.$l['free'].'")+"</div>";
															}
														}
													);
				
			// RAM Sparklines
			$("#servers_list_table .spram").sparkline("html", {
															type:"pie",
															sliceColors: ["#434348", "#90ed7d"],
															tooltipClassname: "spPieToolTip jqstooltip",
															tooltipFormatter: function(a, b, fields){
																return "<div style=\'padding:2px;\'>"+fields["value"]+" MB "+(fields["offset"] == 0 ? "'.$l['used'].'" : "'.$l['free'].'")+" ("+fields["percent"].toFixed(2)+"%)</div>";
															}
														}
													);
				
			// Disk Sparklines
			$("#servers_list_table .spdisk").sparkline("html", {
															type:"pie",
															sliceColors: ["#434348", "#7cb5ec"],
															tooltipClassname: "spPieToolTip jqstooltip",
															tooltipFormatter: function(a, b, fields){
																return "<div style=\'padding:2px;\'>"+fields["value"].toFixed(2)+" GB "+(fields["offset"] == 0 ? "'.$l['used'].'" : "'.$l['free'].'")+" ("+fields["percent"].toFixed(2)+"%)</div>";
															}
														}
													);
																
			// Inode Sparklines
			$("#servers_list_table .spinode").sparkline("html", {
														type:"pie",
															sliceColors: ["#434348", "#d90955"],
															tooltipClassname: "spPieToolTip jqstooltip",
															tooltipFormatter: function(a, b, fields){
																return "<div style=\'padding:2px;\'>"+fields["value"]+" "+(fields["offset"] == 0 ? "'.$l['used'].'" : "'.$l['free'].'")+" ("+fields["percent"].toFixed(2)+"%)</div>";
															}
														}
													);
																
			// Bandwidth Sparklines
			$(".spband").sparkline("html", {
										type:"pie",
										sliceColors: ["#434348", "#7cb5ec"],
										tooltipClassname: "spPieToolTip jqstooltip",
										tooltipFormatter: function(a, b, fields){
											return "<div style=\'padding:2px;\'>&nbsp;"+(fields["offset"] == 1 ? (fields["value"] == "9999999") ? a.values[0]+"/'.$l['unlimited'].'"+" GB"  : fields["value"]+" GB '.$l['free'].'" : fields["value"]+" GB '.$l['used'].'")+"("+((fields["offset"] == 1 && fields["value"] == "9999999") ? "0.01" : fields["percent"].toFixed(2))+" %)</div>";
										}
									}
								);
						
			$(".spcpu, .spram, .spdisk, .spinode, .spband").bind("sparklineClick", function(ev) {
				if(window.sparkClickClrTimer){
					 clearTimeout(window.sparkClickClrTimer);
				}	
				var elementPosition = $(ev.target).position();
				$(".jqstooltip").css({"left":(elementPosition.left+85)+\'px\'});
				$(".jqstooltip").css({"visibility": "visible"});
				window.sparkClickClrTimer = setTimeout(function(){ $(".jqstooltip").css({"visibility": "hidden"}); }, 3000);
			});
		
		}
			
		function byte_convert(bytes, round_upto = 0){
				
			// Is it in TBs
			if(bytes > (1024*1024*1024*1024)){
				return Math.round(bytes / (1024*1024*1024*1024), round_upto)+" TB";
			}
				
			// Is it in GBs
			if(bytes > (1024*1024*1024)){
				return Math.round(bytes / (1024*1024*1024), round_upto)+" GB";
			}
				
			// Is it in MBs
			if(bytes > (1024*1024)){
				return Math.round(bytes / (1024*1024), round_upto)+" MB";
			}
				
			// Is it in KBs
			if(bytes > 1024 && bytes < (1024*1024)){
				return Math.round(bytes /1024, round_upto)+" KB";
			}
				
			// Is it in GBs
			if(bytes <= 1024){
				return Math.round(bytes, round_upto)+" B";
			}
		}
		</script>
		<div class="bg">
			<center class="tit">
				<i class="icon icon-vs icon-head"></i>&nbsp; '.$l['page_head'].'<span style="float:right"><a href="javascript:showsearch();"></a></span>
			</center>
			<br />';
			
			server_select();
			
			echo '<br />
			<div class="row">
				<center>
					<div class="col-sm-4">
						<div class="row">
							<div id="holder_cpu" align="center" style="width:350px; height: 350px"></div>
						</div>
						<div class="row">
							<span style="padding:5px;font-size:20px;">'.$l['cpu_utilization'].'</span>
						</div>	
					</div>	
				</center>
				<center>
					<div class="col-sm-4">
						<div class="row">
							<div id="holder_ram" align="center" style="width:350px; height: 350px"></div>
						</div>	
						<div class="row">
							<span style="padding:5px;font-size:20px;">'.$l['ram_utilization'].'</span>
						</div>	
					</div>	
				</center>
				<center>	
					<div class="col-sm-4">
						<div class="row">
							<div id="holder_bandwidth" align="center" style="width:350px; height: 350px"></div>
						</div>	
						<div class="row">
							<span style="padding:5px;font-size:20px;">'.$l['bandwidth_utilization'].'</span>
						</div>	
					</div>
				</center>	
			</div><br/>
			<div class="notice" align="left" style="font-size:12px; line-height:150%"><img src="'.$theme['images'].'/notice.gif" align=left/> &nbsp; '.$l['resources_note'].'</div>
			<br /> 
			<center>
				<div class="row" style="width:100%">
					<table class="table table-hover tablesorter table-head" id="servers_list_table" width="60%">
						<thead>
							<tr>
								<th class="col-md-1">'.$l['status'].'</th>
								<th class="col-md-1">'.getSortHeader($l['vpsid'], $sort_column, $sort_column_by, 'vpsid').'</th>
								<th class="col-md-2">'.$l['hostname'].' (VID)</th>
								<th class="col-md-1">'.getSortHeader($l['cpu'], $sort_column, $sort_column_by, 'cpu').'</th>
								<th class="col-md-1">'.getSortHeader($l['ram'], $sort_column, $sort_column_by, 'ram').'</th>
								<th class="col-md-1">'.getSortHeader($l['disk'], $sort_column, $sort_column_by, 'disk').'</th>
								<th >'.getSortHeader($l['inode'], $sort_column, $sort_column_by, 'inode').'</th>
								<th class="col-md-2">'.getSortHeader($l['bandwidth'], $sort_column, $sort_column_by, 'bandwidth').'</th>
								<th >'.getSortHeader($l['net_in'], $sort_column, $sort_column_by, 'net_in').'</th>
								<th >'.getSortHeader($l['net_out'], $sort_column, $sort_column_by, 'net_out').'</th>
								<th >'.getSortHeader($l['io_read'], $sort_column, $sort_column_by, 'io_read').'</th>
								<th >'.getSortHeader($l['io_write'], $sort_column, $sort_column_by, 'io_write').'</th>
								<th class="col-md-1">'.getSortHeader($l['speed_cap'], $sort_column, $sort_column_by, 'speed_cap').'</th>
								<th ></th>
							</tr>
						</thead>
						'.$table_data.'
					</table>
				</div>
			</center>
		</div>'; // bg div End
		
		// Modal html for Individual VPS
		echo '<div id="dialog" style="display:none">
			<table border="0" cellpadding="0" cellspacing="3" align="center">
				<tr>
					<td class="form-horizontal" valign="top" >
						<center>
							<div class="row">
								<div class="col-sm-2 col-sm-offset-1">
									<a href="javascript:void(0);" class="tab" id="cpuinfo" onclick="change_monitor_tabs(this.id);"><img src="'.$theme['images'].'cpu.png" /></a>
									<label for="input text" class="help-block">'.$l['cpu'].'</label>
								</div>
								<div class="col-sm-2">
									<a href="javascript:void(0);" class="tab" id="raminfo" onclick="change_monitor_tabs(this.id);"><img src="'.$theme['images'].'ram.png" /></a>
									<label for="input text" class="help-block">'.$l['ram'].'</label>
								</div>
								<div class="col-sm-2">
									<a href="javascript:void(0);" class="tab" id="diskinfo" onclick="change_monitor_tabs(this.id);"><img src="'.$theme['images'].'disk.png" /></a>
									<label for="input text" class="help-block">'.$l['disk'].'/'.$l['inode'].'</label>
								</div>
								<div class="col-sm-2">
									<a href="javascript:void(0);" class="tab" id="ioinfo" onclick="change_monitor_tabs(this.id);"><img src="'.$theme['images'].'disk.png" /></a>
									<label for="input text" class="help-block">'.$l['disk_io'].'</label>
								</div>
								<div class="col-sm-2">
									<a href="javascript:void(0);" class="tab" id="ntwinfo" onclick="change_monitor_tabs(this.id);"><img src="'.$theme['images'].'ntw.png" /></a>
									<label for="input text" class="help-block">'.$l['header_ntw_speed'].'</label>
								</div>
							</div>

							<div class="row" id="by_month">
								<div id="prev_month" style="align:left" class="col-sm-4"></div>
								<div id="month_holder2" class="col-sm-4" style="text-align:center;font-size:21px;"></div>
								<div id="next_month" style="align:right" class="col-sm-4"></div>	
							</div><br/>
							
							<div class="row">
								<div class="col-sm-12">
									<div class="notice" id="note_box" style="display: none;">
										<img src="'.$theme['images'].'notice.gif" />&nbsp; '.$l['no_data'].'&nbsp;'.$l['for'].'&nbsp;<span id="for_month"></span>
									</div>
								</div>
							</div>
							<div class="row" id="cpuinfo_win">
								<div class="col-sm-12">
									<div id="legend_cpu" class="legend_container"></div>
									<div id="cpu_plot" style="width:800px; height:400px;">
									</div>
								</div>
							</div>
							
							<div id="raminfo_win" style="display:none" class="row">
								<div class="col-sm-12">
									<div id="legend_ram" class="legend_container"></div>
									<div id="ram_plot" style="width:800px; height:400px;">
									</div>
								</div>
							</div>
							
							<div class="row" id="diskinfo_win" style="display:none">
								<div class="row">
									<div class="col-sm-12">
										<div id="legend_disk" class="legend_container"></div>
										<div id="disk_plot" style="width:800px; height:400px;">
										</div>
									</div>
								</div>	
								<div class="row">
									<div class="col-sm-12" >
										<div id="legend_inode" class="legend_container"></div>
										<div id="inode_plot" style="width:800px; height:400px;">
										</div>
									</div>
								</div>	
							</div>
							
							<div id="ioinfo_win" style="display:none;" class="form-group">
								<div class="col-sm-12">
									<div id="legend_io_read" class="legend_container"></div>
									<div id="io_read_plot" style="width:720px; height:400px;">
									</div>
								</div>
								<div class="col-sm-12">
									<div id="legend_io_write" class="legend_container"></div>
									<div id="io_write_plot" style="width:720px; height:400px;">
									</div>
								</div>
							</div>
							
							<div id="ntwinfo_win" style="display:none;" class="row">
								<div class="col-sm-12">
									<div id="legend_ntw" class="legend_container"></div>
									<div id="ntw" style="width:800px; height:400px;">
									</div>
								</div>
							</div>
						</center>
					</td>
				</tr>
			</table>
		</div>';
	}
	
	softfooter();
}

//Function to convert given Bytes to KB, MB, GB or TB
function byte_convert($bytes, $round_upto = 0){
	
	// Is it in TBs
	if($bytes > (1024*1024*1024*1024)){
		return round($bytes / (1024*1024*1024*1024), $round_upto)." TB";
	}
	
	// Is it in GBs
	if($bytes > (1024*1024*1024)){
		return round($bytes / (1024*1024*1024), $round_upto)." GB";
	}
	
	// Is it in MBs
	if($bytes > (1024*1024)){
		return round($bytes / (1024*1024), $round_upto)." MB";
	}
	
	// Is it in KBs
	if($bytes > 1024 && $bytes < (1024*1024)){
		return round($bytes /1024, $round_upto)." KB";
	}
	
	// Is it in GBs
	if($bytes <= 1024){
		return round($bytes, $round_upto)." B";
	}
}
	
?>