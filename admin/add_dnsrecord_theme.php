<?php

//////////////////////////////////////////////////////////////
//===========================================================
// add_dnsrecord_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function add_dnsrecord_theme(){

global $theme, $globals, $kernel, $user, $l, $cluster, $error, $servers, $done, $ips, $pdns, $pdnsid, $zones, $editrecord;

softheader(empty($editrecord) ? $l['<add_title>'] : $l['<edit_title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-pdns icon-head"></i> '.(empty($editrecord) ? $l['add_dnsrecord'] : $l['edit_dnsrecord']).'</center>';

error_handle($error);

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
}
echo '<script language="javascript" type="text/javascript">

function recordtype(rtype){
	$(".rdns").hide();
	$(".dns").hide();
	if(rtype == "type_rdns"){
		$(".rdns").show();
	}else{
		$(".dns").show();
	}
}

function selectedzone(){
	zone = $("#zone_name option:selected").text();
	if(zone != ""){
		$("#domain_value").html("."+zone);
	}
}
		


addonload("selectedzone(\''.(empty($editrecord) ? POSTval('zone_name', '') : '').'\');");
addonload("recordtype(\''.(empty($editrecord) ? POSTval('dns_type', 'type_rdns') : $editrecord['dns_type']).'\');");

</script>';

echo '<div id="form-container">

<form accept-charset="'.$globals['charset'].'" name="add_dnsrecord" method="post" action="" class="form-horizontal">'
.(empty($editrecord) ? '
<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['dns_type'].'</label><br />
	</div>
	<div class="col-sm-6">
		<select class="form-control" name="dns_type" id="dns_type" onchange="recordtype(this.value)">
			<option id="type_rdns" value="type_rdns" '.ex_POSTselect('dns_type', 'type_rdns').'>'.$l['type_rdns'].'</option>
			<option id="type_dns" value="type_dns" '.ex_POSTselect('dns_type', 'type_dns').'>'.$l['type_dns'].'</option>
		</select>
	</div>
</div>' : '').'<br/>
<div class="row rdns">
	<div class="col-sm-6">
		<label class="control-label">'.$l['dns_ip'].'</label><br />
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="dns_ip" value="'.POSTval('dns_ip', rdns_name($editrecord['name'])).'" size="45">
	</div>
</div>
<br/>
<div class="row rdns">
	<div class="col-sm-6">
		<label class="control-label">'.$l['domain'].'</label><br />
		<span class="help-block">'.$l['domain_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="domain" value="'.POSTval('domain', $editrecord['content']).'" size="30">
	</div>
</div><br/>';
if(empty($editrecord)){
	echo '<div class="row dns">
	<div class="col-sm-6">
		<label class="control-label">'.$l['zone_name'].'</label><br />
	</div>
	<div class="col-sm-6">
		<select class="form-control" name="zone_name" id="zone_name" onchange="selectedzone();">';
		foreach($zones as $k => $v){
			echo '<option value="'.$v['domain_id'].'" '.POSTselect('zone_name', $v['domain_id']).' >'.$v['name'].'</option>';
		}
	echo '</select>
	</div>
</div>';
}
echo '
<br/>
<div class="row dns">
	<div class="col-sm-6">
		<label class="control-label">'.$l['host'].'</label><br />
	</div>
	<div class="col-sm-6">
		<div class="row">
			<div class="col-sm-6">
				<input type="text" class="form-control" name="host" value="'.POSTval('host', $editrecord['name']).'" size="30">
			</div>
			<div class="col-sm-6">
				<label id="domain_value" style="padding-top:5px; font-size:14px;" class="help-block"></label>
			</div>
		</div>
	</div>
</div>
<br/>
'.(empty($editrecord) ? '<div class="row dns">
	<div class="col-sm-6"><label class="control-label">Type</label></div>
	<div class="col-sm-6">
		<select class="form-control" name="record_type" id="record_type" value="'.POSTselect('record_type', '').'">
			<option name="a_record" id="a_record" value="A" '.ex_POSTselect('record_type', 'A').'>A (host)</option>
			<option name="cname_record" id="cname_record" value="CNAME" '.ex_POSTselect('record_type', 'CNAME').'>CNAME (Aliases)</option>
			<option name="mx_record" id="mx_record" value="MX" '.ex_POSTselect('record_type', 'MX').'>MX (Mail Exchange)</option>
			<option name="txt_record" id="txt_record" value="TXT" '.ex_POSTselect('record_type', 'TXT').'>TXT (Text)</option>
			<option name="ns_record" id="ns_record" value="NS" '.ex_POSTselect('record_type', 'NS').'>NS (Name Servers)</option>
			<option name="aaaa_record" id="aaaa_record" value="AAAA" '.ex_POSTselect('record_type', 'AAAA').'>AAAA (IPV6 Hosts)</option>
		</select>
	</div>
</div>' : '').'
<br/>
<div class="row dns">
	<div class="col-sm-6"><label class="control-label">Content</label></div>
	<div class="col-sm-6">
		<input id="content" type="text" class="form-control" name="content" value="'.POSTval('content', $editrecord['content']).'">
	</div>
</div>
<br/>
<div class="row dns">
	<div class="col-sm-6"><label class="control-label">Priority</label></div>
	<div class="col-sm-6"><input id="prio" type="text" class="form-control" name="prio" value="'.POSTval('prio', $editrecord['prio']).'"></div>		
</div>
</br>
<div class="row dns">
	<div class="col-sm-6"><label class="control-label">TTL</label></div>
	<div class="col-sm-6"><input id="ttl" type="text" class="form-control" name="ttl" value="'.POSTval('ttl', $editrecord['ttl']).'"></div>
</div>

<br /><br />
<center><input type="submit" value="'.$l['sub_but'].'" class="btn" name="add_dnsrecord" /></center>

</form>
</div>';

softfooter();

}

?>