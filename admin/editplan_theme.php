<?php

//////////////////////////////////////////////////////////////
//===========================================================
// editplan_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function editplan_theme(){

global $theme, $globals, $kernel, $user, $l , $error, $plan, $done, $_ubc, $mgs, $isos, $oslist, $vnc_keymap_array, $recipes, $backup_plans, $supported_nics, $servergroups, $ippools, $bus_driver_list;

softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-plans icon-head"></i> '.$l['edit_plan'].'</center>';

error_handle($error);

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
}

echo '<script language="javascript" type="text/javascript">

var servergroups = '.json_encode($servergroups).';

function toggle_advoptions(ele){
	
	var div_ele = $("#"+ele+"_advoptions");

	if (div_ele.is(":hidden")){
		$("#"+ele+"_advoptions_ind").text(" - ");
		div_ele.slideDown("slow");
	}
	else{
		$("#"+ele+"_advoptions_ind").text(" + ");
		div_ele.slideUp("slow");
	}
}
function adddnsrow(id, val){

var t = $_(id);
	var ele = document.createElement("div");
	ele.setAttribute("class","row");
	ele.innerHTML=\'<div class="col-sm-6"><input type="text" class="form-control" name="dns[]"  value="\'+(val || "")+\'"  size="20" /></div>\';
	t.appendChild(ele);
	var sp = document.createElement("span");
	sp.setAttribute("class","help-block");
	t.appendChild(sp);	
};

function ispvonhvm(plan){

	if($("#tr_pvonhvm").css("display") == "none"){
		return true;
	}
	
	var pv_on_hvm = false;
	if(!$_("pv_on_hvm")){
		pv_on_hvm = false;
	}else{
		pv_on_hvm = $_("pv_on_hvm").checked;
	}
	
	if($_("tr_viftype")){
		if(pv_on_hvm == true){
			$_("tr_viftype").style.display = "none";
		}else{
			$_("tr_viftype").style.display = "";
		}
	}
	
	return true;
};


function changeplan(plan){
	$(".openvz, .kvm, .xen, .xenhvm, .xcp, .xcphvm, .lxc, .vzk, .vzo, .proxk, .proxl, .proxo").hide();
	$("." + plan).show();
	
	$("#disk_space_col").attr("class", "");
	if(plan == "proxk"){
		$("#disk_space_col").attr("class", "col-sm-2");
		$("#disk_space").css("width", "50%");
	}else{
		$("#disk_space_col").attr("class", "col-sm-6");
		$("#disk_space").css("width", "");
	}
	
	$("#mgs option").each(function(){
		if($(this).attr("type") != plan){
			$(this).attr("disabled","disabled");
			$(this).css("display","none");
		}else{
			$(this).attr("disabled", false);
			$(this).css("display","block");
		}
	});	
};

function changepriority(priority){
	if(!$("#prior")){
		return;
	}
	if (priority != ""){
		$_("prior").value = priority;
	}
};

function ishvm(){
	var hvm = false;
	var tmp_hvm = "'.$plan['virt'].'";
	var n = tmp_hvm.search(/hvm/i); 
	if(n > 0){
		hvm = true;
	}
	
	if($_("isorow")){
		if(hvm == true || tmp_hvm == "kvm"){
			$_("isorow").style.display = "";
		}else{
			$_("isorow").style.display = "none";
		}
	}
	
	//alert(vpsid + " -- " + hvm)
	for(var i=0; i<$_("osid").options.length; i++){
		
		$_("osid").options[i].style.display = "";	
		if($_("osid").options[i].value < 1) continue;
		
		var cvirt = getAttributeByName($_("osid").options[i], "virt");
		
		if(getAttributeByName($_("osid").options[i], "hvm") == 1 && hvm == true && cvirt == tmp_hvm){
			$_("osid").options[i].disabled = false;
		}else if(getAttributeByName($_("osid").options[i], "hvm") != 1 && hvm == false && cvirt == tmp_hvm){
			$_("osid").options[i].style.display = "";
		}else{
			$_("osid").options[i].style.display = "none";
		}
	}
	return false;
};

function ubcEdit(){
	if($("#ubcsettings").prop("checked")){
		$("#ubc_advoptions input:text").prop("disabled", false);
	}else{
		$("#ubc_advoptions input:text").prop("disabled", true);
	}
}

function change_cpu_topology(){
	
	if($("#enable_cpu_topology").prop("checked")){
		$("#cpu_topology").css("display", "");
	}else{
		$("#cpu_topology").css("display", "none");
		$("#topology_sockets").val(0);
		$("#topology_cores").val(0);
		$("#topology_threads").val(0);
	}
}

function toggle_driver_num(driver_type){
	$(".ide, .sata, .virtio, .scsi").hide();
	$("." + driver_type).show();
};

var lang_no_limit = "'.$l['no_limit'].'";
addonload("changepriority(\''.POSTval('priority', $plan['io']).'\');change_cpu_topology();");
addonload("handle_capping(); fillspeedmbits(); changeplan(\''.$plan['virt'].'\');enable_accel();ishvm();ispvonhvm();ubcEdit();");
</script>

<form accept-charset="'.$globals['charset'].'" name="editplan" method="post" action="" class="form-horizontal">

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['plan_enable'].'</label>
		<span class="help-block">'.$l['plan_enable_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="checkbox" name="is_enabled" id="is_enabled" '.POSTchecked('is_enabled', $plan['is_enabled']).' />
	</div>
</div>
<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['planname'].'</label>
		<span class="help-block">'.$l['nameofplan'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control side_text" name="plan_name" id="plan_name" size="30" value="'.POSTval('plan_name', $plan['plan_name']).'" />
	</div>
</div>
<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['plantype'].'</label>
		<span class="help-block">&nbsp;</span>
	</div>
	<div class="col-sm-6">
		<img name="virt" data-virt="'.$plan['virt'].'" src="'.$theme['images'].'admin/'.$plan['virt'].'_100.gif" />
	</div>
</div>';

if(!empty($backup_plans)){
	
	echo '
<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['backup_plans'].'</label>
		<span class="help-block">'.$l['backup_plans_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<select class="form-control side_text" name="bpid" id="bpid">
		<option value=0 '.(POSTval('bpid') == 0 ? 'selected="selected"' : '').'>'.$l['select_backup_plan'].'</option>';
			foreach($backup_plans as $k => $v){
				echo '<option value="'.$k.'" '.(POSTval('bpid') == $k ? 'selected="selected"' : (empty($_POST) && $plan['bpid'] == $k ? 'selected="selected"' : '')).'>'.$v['plan_name'].'</option>';
			}
		echo '</select>
	</div>
</div>';

}

echo '
<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['vsos'].'</label>
		<span class="help-block">'.$l['vsos_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<select class="form-control side_text" id="osid" name="osid">
			<option value="0" '.(POSTval('osid')== 0 ?  'selected="selected"' :($plan['osid']== 0 ? 'selected="selected"' : '')).'>'.$l['select_os'].'</option>';
			foreach($oslist as $_virt => $vvvv){
			
				foreach($oslist[$_virt] as $kk => $vv){
				
					foreach($vv as $k => $v){
				
						echo '<option value="'.$k.'" '.(POSTval('osid') == $k ? 'selected="selected"' : ($plan['osid'] == $k ? 'selected="selected"' : '')).' '.(!empty($v['hvm']) ? 'hvm="1"' : '').' virt="'.$_virt.(!empty($v['hvm']) ? 'hvm' : '').'">'.(!empty($v['hvm']) ? 'HVM - ' : '').''.$v['name'].'</option>';
					
					}
					
				}
			}
	echo'</select>
	</div>
</div>';

if(!empty($isos)){
	echo '<div class="row kvm xenhvm xcphvm vzk proxk" id="isorow">
	<div class="col-sm-6">
		<label class="control-label">'.$l['vsiso'].'</label>
		<span class="help-block">'.$l['vsiso_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<select class="form-control side_text" name="iso">
		<option value="0" '.(POSTval('iso') == 0 ? 'selected="selected"' : '').'>'.$l['none'].'</option>';
		
		foreach($isos as $k => $v){	
			echo '<option value="'.$k.'" '.(POSTval('iso') == $k ? 'selected="selected"' : ($plan['iso'] == $k ? 'selected="selected"' : '')).'>'.$v['name'].'</option>';
		}
		
     echo'</select>
	</div>
</div>';

}

echo '
<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['dspace'].'</label>
		<span class="help-block">'.$l['hspaceallot'].'</span>
	</div>
	<div class="col-sm-2 proxk">
			<select class="form-control bus_driver" id = "bus_driver" name="bus_driver" onchange="toggle_driver_num(this.value)" >';
			
				foreach($bus_driver_list as $bdk => $bdv){
					echo '<option value="'.$bdk.'" '.(POSTval('bus_driver') == $bdk ? 'selected="selected"' : ($plan['bus_driver'] == $bdk ? 'selected="selected"' : '')).' >'.$bdv.'</option>';
				}
	echo   '</select>
		</div>	
			<div class="col-sm-2 proxk">	
					<select class="form-control bus_driver_num" name="bus_driver_num" >';
						echo '<option class="sata virtio scsi" value="0" '.(POSTval('bus_driver_num') == 0 ? 'selected="selected"' : ($plan['bus_driver_num'] == 0 ? 'selected="selected"' : '')).'>0</option>';
						echo '<option class="sata virtio scsi" value="1" '.(POSTval('bus_driver_num') == 1 ? 'selected="selected"' : ($plan['bus_driver_num'] == 1 ? 'selected="selected"' : '')).'>1</option>';
						echo '<option class="ide sata virtio scsi" value="2" '.(POSTval('bus_driver_num') == 2 ? 'selected="selected"' : ($plan['bus_driver_num'] == 2 ? 'selected="selected"' : '')).'>2</option>';
						echo '<option class="ide sata virtio scsi" value="3" '.(POSTval('bus_driver_num') == 3 ? 'selected="selected"' : ($plan['bus_driver_num'] == 3 ? 'selected="selected"' : '')).'>3</option>';
						echo '<option class="virtio scsi" value="4" '.(POSTval('bus_driver_num') == 4 ? 'selected="selected"' : ($plan['bus_driver_num'] == 4 ? 'selected="selected"' : '')).'>4</option>';
						echo '<option class="sata virtio scsi" value="5" '.(POSTval('bus_driver_num') == 5 ? 'selected="selected"' : ($plan['bus_driver_num'] == 5 ? 'selected="selected"' : '')).'>5</option>';
						echo '<option class="virtio scsi" value="6" '.(POSTval('bus_driver_num') == 6 ? 'selected="selected"' : ($plan['bus_driver_num'] == 6 ? 'selected="selected"' : '')).'>6</option>';
						echo '<option class="virtio scsi" value="7" '.(POSTval('bus_driver_num') == 7 ? 'selected="selected"' : ($plan['bus_driver_num'] == 7 ? 'selected="selected"' : '')).'>7</option>';
						echo '<option class="virtio scsi" value="8" '.(POSTval('bus_driver_num') == 8 ? 'selected="selected"' : ($plan['bus_driver_num'] == 8 ? 'selected="selected"' : '')).'>8</option>';
						echo '<option class="virtio scsi" value="9" '.(POSTval('bus_driver_num') == 9 ? 'selected="selected"' : ($plan['bus_driver_num'] == 9 ? 'selected="selected"' : '')).'>9</option>';
						echo '<option class="virtio scsi" value="10" '.(POSTval('bus_driver_num') == 10 ? 'selected="selected"' : ($plan['bus_driver_num'] == 10 ? 'selected="selected"' : '')).'>10</option>';
						echo '<option class="virtio scsi" value="11" '.(POSTval('bus_driver_num') == 11 ? 'selected="selected"' : ($plan['bus_driver_num'] == 11 ? 'selected="selected"' : '')).'>11</option>';
						echo '<option class="virtio scsi" value="12" '.(POSTval('bus_driver_num') == 12 ? 'selected="selected"' : ($plan['bus_driver_num'] == 12 ? 'selected="selected"' : '')).'>12</option>';
						echo '<option class="virtio scsi" value="13" '.(POSTval('bus_driver_num') == 13 ? 'selected="selected"' : ($plan['bus_driver_num'] == 13 ? 'selected="selected"' : '')).'>13</option>';
						echo '<option class="virtio" value="14" '.(POSTval('bus_driver_num') == 14 ? 'selected="selected"' : ($plan['bus_driver_num'] == 14 ? 'selected="selected"' : '')).'>14</option>';
						echo '<option class="virtio" value="15" '.(POSTval('bus_driver_num') == 15 ? 'selected="selected"' : ($plan['bus_driver_num'] == 15 ? 'selected="selected"' : '')).'>15</option>';
			echo	'
					</select>
	</div>
	<div id="disk_space_col" class="col-sm-2">
		<input type="text" class="form-control side_text" name="disk_space" id="disk_space" size="10" value="'.POSTval('disk_space', $plan['space']).'"/><label class="side_lbl">GB</label>
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['gram'].'</label><br />
		<span class="help-block">'.$l['gram_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control side_text" name="guaranteed_ram" id="guaranteed_ram" size="10" value="'.POSTval('guaranteed_ram', $plan['ram']).'" /><label class="side_lbl">MB</label>
	</div>	
</div>

<div class="row kvm xen xenhvm xcp xcphvm lxc vzk vzo proxk proxo proxl" id="swap">
	<div class="col-sm-6">
		<label class="control-label">'.$l['swap'].'</label>
		<span class="help-block">&nbsp;</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control side_text" name="swapram" id="swapram" size="10" value="'.POSTval('swapram', $plan['swap']).'" /><label class="side_lbl">MB</label>
		<span class="help-block"></span>
	</div>	
</div>

<div class="row openvz" id="burst">
	<div class="col-sm-6">
		<label class="control-label">'.$l['burst_ram'].'</label><br />
		<span class="help-block">'.$l['burst_ram_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control side_text" name="burst_ram" id="burst_ram" size="10" value="'.POSTval('burst_ram', $plan['burst']).'" /><label class="side_lbl">MB</label>
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['bwidth'].'</label><br />
		<span class="help-block">'.$l['balloc'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control side_text" name="bandwidth" id="bandwidth" size="10" value="'.POSTval('bandwidth', $plan['bandwidth']).'" onchange="handle_capping();" /><label class="side_lbl">GB</label>
	</div>	
</div>

<script type="text/javascript">
function netspeed(r){
	$_("network_speed").value = r;
	handle_capping();
}
function upspeed(r){
	$_("upload_speed").value = (r);
	handle_capping();
}

function enable_accel(){
	
	if($_("kvm_vga").checked == true){
		$_("enable_acceleration").style.display = "";
	}else{
		$_("enable_acceleration").style.display = "none";
	}

}

$("#hvm").change(function(data){
	if(data.target.checked){
		$("#tr_viftype").show();
	}
	else if(!data.target.checked){
		$("#tr_viftype").hide();
	}
	
});

$("#hvm").change(function(data){
	if(data.target.checked){
		$("#tr_pvonhvm").show();
	}
	else if(!data.target.checked){
		$("#tr_pvonhvm").hide();
	}
	
});
</script>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['network_speed'].'</label><br />
		<span class="help-block">'.$l['network_speed_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control side_text" name="network_speed" id="network_speed" size="8" value="'.POSTval('network_speed', $plan['network_speed']).'" style="width:30%" onchange="handle_capping();" /> <label class="side_lbl" style="float:left;">'.$l['net_kb'].'</label>
		<select class="form-control speedmbits" name="network_speed2" id="network_speed2" onchange="netspeed(this.value);" style="width:44%;"></select>
	 </div>
</div>

<div class="row" id="cpunits">
	<div class="col-sm-6">
		<label class="control-label">'.$l['cpu'].'</label><br />
		<span class="help-block">'.$l['cpalloc'].' <br />
		<a href="'.$globals['docs'].'Creating_A_VPS#CPU_Parameters" target="_blank">'.$l['need_info'].'</a>
		</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control side_text" name="cpu_units" id="cpu_units" size="10" value="'.POSTval('cpu_units', $plan['cpu']).'" /><label class="side_lbl">Units</label>
	</div>	
</div>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['cpucore'].'</label>
		<span class="help-block"><a href="'.$globals['docs'].'Creating_A_VPS#CPU_Parameters" target="_blank">'.$l['need_info'].'</a></span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control side_text" name="cpu_cores" id="cpu_cores" size="10" value="'.POSTval('cpu_cores', $plan['cores']).'" />
	</div>	
</div>

<div class="row" id="cpupercent">
	<div class="col-sm-6">
		<label class="control-label">'.$l['cpupercent'].'</label><br />
		<span class="help-block">'.$l['cpuperutil'].'<br /><a href="'.$globals['docs'].'Creating_A_VPS#CPU_Parameters" target="_blank">'.$l['need_info'].'</a></span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control side_text" name="percent_cpu" id="percent_cpu" size="10" value="'.POSTval('percent_cpu', $plan['cpu_percent']).'" /><label class="side_lbl">%</label>
	</div>
</div>

<div class="row openvz vzo vzk proxo" id="priority">
	<div class="col-sm-6">
		<label class="control-label">'.$l['ioprior'].'</label><br />
		<span class="help-block">'.$l['io0-7'].'</span>
	</div>
	<div class="col-sm-6">
		<select id="prior" name="priority" class="form-control side_text">
			<option value="0">0</option>
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
			<option value="4">4</option>
			<option value="5">5</option>
			<option value="6">6</option>
			<option value="7">7</option>
		</select>
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['plan_ips'].'</label><br />
		<span class="help-block">'.$l['plan_ips_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control side_text" name="ips" size="10" value="'.POSTval('ips', $plan['ips']).'" />
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['plan_ips6_subnet'].'</label><br />
		<span class="help-block">'.$l['plan_ips6_subnet_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control side_text" name="ips6_subnet" size="10" value="'.POSTval('ips6_subnet', $plan['ips6_subnet']).'" />
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['plan_ips6'].'</label><br />
		<span class="help-block">'.$l['plan_ips6_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control side_text" name="ips6" size="10" value="'.POSTval('ips6', $plan['ips6']).'" />
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['plan_ips_int'].'</label><br />
		<span class="help-block">'.$l['plan_ips_int_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control side_text" name="ips_int" size="10" value="'.POSTval('ips_int', $plan['ips_int']).'" />
	</div>
</div>
<div class="row kvm" id="virtio_settings">
	<div class="col-sm-6">
		<label class="control-label">'.$l['virtio'].'</label><br />
		<span class="help-block">'.$l['virtio_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="checkbox" class="ios" name="virtio" '.POSTchecked('virtio', $plan['virtio']).' />
	</div>
</div>
<div class="row vnc_div kvm xen xenhvm xcp xcphvm vzk vzo">
	<div class="col-sm-6">
		<label class="control-label">VNC</label>
		<span class="help-block">&nbsp;</span>
	</div>
	<div class="col-sm-6">
		<input type="checkbox" class="ios" name="vnc" id="vnc" '.POSTchecked('vnc', $plan['vnc']).' />
	</div>
</div>
<div class="row xenhvm" id="shadow_div">
	<div class="col-sm-6">
		<label class="control-label">'.$l['shadow'].'</label><br />
		<span class="help-block">'.$l['shadow_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control side_text" name="shadow" size="10" value="'.POSTval('shadow', $plan['shadow']).'" /><label class="side_lbl">MB</label>
	</div>
</div>
<div class="row openvz">
	<div class="col-sm-6">
		<label class="control-label">'.$l['ploop'].'</label>
		<span class="help-block">'.$l['ploop_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="checkbox" class="ios" name="ploop" value="1" '.POSTchecked('ploop', $plan['ploop']).' />
	</div>
</div>
<div class="row kvm_hvm_div kvm xenhvm xcphvm">
	<div class="col-sm-6">
		<label class="control-label">'.$l['acpi'].'</label>
		<span class="help-block"></span>
	</div>
	<div class="col-sm-6">
		<input type="checkbox" class="ios" name="acpi" '.POSTchecked('acpi', $plan['acpi']).'/>
	</div>
</div>
<div class="row kvm_hvm_div kvm xenhvm xcphvm">
	<div class="col-sm-6">
		<label class="control-label">'.$l['apic'].'</label>
		<span class="help-block"></span>
	</div>
	<div class="col-sm-6">
		<input type="checkbox" class="ios" name="apic" '.POSTchecked('apic', $plan['apic']).'/>
	</div>
</div>
<div class="row kvm_hvm_div kvm xenhvm xcphvm">
	<div class="col-sm-6">
		<label class="control-label">'.$l['pae'].'</label>
		<span class="help-block"></span>
	</div>
	<div class="col-sm-6">
		<input type="checkbox" class="ios" name="pae" '.POSTchecked('pae', $plan['pae']).'/>
	</div>
</div>
<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['mg'].'</label>
		<span class="help-block">'.$l['mg_exp'].'</span>
	</div>
	<div class="col-sm-5">
		<select class="form-control" name="mgs[]" id="mgs" multiple="multiple">';
			$tmp_mgs = empty($_POST['mgs']) ? $plan['mgs'] : $_POST['mgs'];
			foreach($mgs as $mk => $mv){
				echo '<option value="'.$mk.'" type="'.$mv['mg_type'].'" '.(in_array($mk, @$tmp_mgs) ? 'selected="selected"' : '').'>'.$mv['mg_name'].'</option>';
			}
			
		echo '</select>
	</div>
</div><br />
';

// UBC section starts here  for OpenVZ
echo '
<div class="openvz_ubc openvz" style="display:none;" id="ubc">
	<div class="roundheader" onclick="toggle_advoptions(\'ubc\');" style="cursor:pointer;"><label id="ubc_advoptions_ind" style="width:10px;">+</label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$l['ubcsettings'].'</div>
	<div id="ubc_advoptions" style="display:none; width:100%"><br />
		<div class="row" id="ubc">
			<div class="col-sm-6">
				<label class="control-label">'.$l['ubcsettings'].'</label><br />
				<span class="help-block">'.$l['exp_ubc'].'</span>
			</div>
			<div class="col-sm-6">
				<input type="checkbox" class="ios" name="ubcsettings" id="ubcsettings" value="1" onchange="ubcEdit();" '.POSTchecked('ubcsettings', (!empty($plan['ubc']) ? 1 : 0)).' />
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4"></div>
			<div class="col-sm-4">
				<label class="control-label">'.$l['barrier'].'</label>
			</div>
			<div class="col-sm-4">
				<label class="control-label">'.$l['limit'].'</label>
			</div>
		</div>';
	
		foreach($_ubc as $k){
			
			echo '<div class="row">
			<div class="col-sm-4">
					<label class="control-label">'.$l[$k].'</label></div>
				<div class="col-sm-4">
					<input type="text" class="form-control" name="b'.$k.'" id="b'.$k.'" size="30" value="'.POSTval('b'.$k, @$plan['ubc'][$k][0]).'" />
					<span class="help-block"></span>
				</div>
				<div class="col-sm-4">
					<input type="text" class="form-control" name="l'.$k.'" id="l'.$k.'" size="30" value="'.POSTval('l'.$k, @$plan['ubc'][$k][1]).'" />
				</div>
			</div>';	
		}
echo '</div>
</div>';// UBC section Ends here  for OpenVZ

// Network setting section
echo '<br />
<div class="roundheader" onclick="toggle_advoptions(\'network\');" style="cursor:pointer;"><label id="network_advoptions_ind" style="width:10px;">+</label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$l['networksettings'].'</div>
<div id="network_advoptions" style="display:none; width:100%"><br />
	<div class="row">
		<div class="col-sm-6">
			<label class="control-label">'.$l['upload_speed'].'</label>
			<span class="help-block">'.$l['upload_speed_exp'].'</span>
		</div>
		<div class="col-xs-8 col-sm-2">
			<input type="text" class="form-control" name="upload_speed" id="upload_speed" size="8" value="'.POSTval('upload_speed', $plan['upload_speed']).'" onchange="handle_capping();" />
		 </div>
		 <div class="col-xs-1" style="padding-top:8px">'.$l['net_kb'].'</div>
		 <div class="col-xs-8 col-sm-3 sm_space">
			<select class="form-control speedmbits" name="upload_speed2" id="upload_speed2" onchange="upspeed(this.value)"></select>
		 </div>
	</div>
	<div class="row kvm xen xenhvm vzk proxk" id="nic_settings">
		<div class="col-sm-6">
			<label class="control-label">'.$l['nic_type'].'</label><br />
			<span class="help-block">'.$l['nic_type_exp'].'</span>
		</div>
		<div class="col-sm-6">
			<select class="nic_type form-control side_text" name="nic_type" id="nic_type" >';
				
				foreach($supported_nics as $k => $v){
					
					echo '<option value="'.$v.'" '.ex_POSTselect('nic_type', $v, $plan['nic_type']).'>'.(!empty($l['nic_'.$v]) ? $l['nic_'.$v] : $v).'</option>';
				}
				
			echo'
			</select>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-10 col-sm-6">
			<label class="control-label">'.$l['band_suspend'].'</label>
			<span class="help-block">'.$l['exp_band_suspend'].'</span>
		</div>
		<div class="col-xs-2 col-sm-6">
				<input type="checkbox" class="ios" name="band_suspend" id="band_suspend" value="1" '.POSTchecked('band_suspend', $plan['band_suspend']).' onchange="handle_capping();" />
		</div>
	</div>
	<div id="speed_cap_limit">
		<div class="row">
			<div class="col-xs-10 col-sm-6">
				<label class="control-label" for="speed_cap_down">'.$l['speed_cap_down'].'</label>
				<span class="help-block">'.$l['exp_speed_cap_down'].'</span>
			</div>
			<div class="col-sm-2 col-xs-8">
				<input type="number" class="form-control" name="speed_cap_down" id="speed_cap_down" value="'.POSTval('speed_cap_down', $plan['speed_cap']['down']).'" onmouseout="blur();"/>			
			</div>
			<div class="col-sm-1" style="padding-top:8px">'.$l['net_kb'].'</div>
			<div class="col-sm-3 col-xs-8 sm_space">
				<select class="form-control speedmbits" name="speed_cap_down2" id="speed_cap_down2" onchange="$(\'#speed_cap_down\').val(this.value)"></select>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-10 col-sm-6">
				<label class="control-label" for="speed_cap_up">'.$l['speed_cap_up'].'</label>
				<span class="help-block">'.$l['exp_speed_cap_up'].'</span>
			</div>
			<div class="col-sm-2 col-xs-8">
				<input type="number" class="form-control" name="speed_cap_up" id="speed_cap_up" value="'.POSTval('speed_cap_up', $plan['speed_cap']['up']).'" onmouseout="blur();" />
			</div>
			<div class="col-sm-1" style="padding-top:8px">'.$l['net_kb'].'</div>
			<div class="col-sm-3 col-xs-8 sm_space">
				<select class="form-control speedmbits" name="speed_cap_up2" id="speed_cap_up2" onchange="$(\'#speed_cap_up\').val(this.value)"></select>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<label class="control-label">'.$l['dns'].'</label>
			<span class="help-block">'.$l['exp_dns'].'</span>
		</div>
		<div class="col-sm-6">
			<div class="row">
					<div class="col-sm-12" id="dnstable">';
				 
						$_dns = @(empty($_POST['dns']) ? $plan['dns_nameserver'] : $_POST['dns']);
						
						if(is_array($_dns) && !empty($_dns)){
							foreach($_dns as $d => $ds){
								if(empty($ds)){
									unset($_dns[$d]);
								}
							}
						}
						
						if(empty($_dns)){
							$_dns = array(NULL);
						}
					 
						foreach($_dns as $dn){
							echo '<div class="row">
								<div class="col-sm-6">
									<input type="text" class="form-control" name="dns[]" value="'.$dn.'"/>
								</div>
							</div>';
						}
						
					echo '
						<br />
					</div>
				</div>
			<input type="button" onclick="adddnsrow(\'dnstable\')" class="go_btn" value="'.$l['add_dns'].'"/>
			<span class="help-block"></span>
		</div>
	</div>
</div>';

// Other Additional Settings
echo '<br />
<div class="roundheader" onclick="toggle_advoptions(\'adv\');" style="cursor:pointer;"><label id="adv_advoptions_ind" style="width:10px;">+</label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$l['addvoption'].'</div>
<div id="adv_advoptions" style="display:none; width:100%"><br />
	<!--<div class="row">
		<div class="col-xs-10 col-sm-6">
			<label class="control-label">'.$l['cpupin'].'</label>
			<span class="help-block">'.$l['cpupin_exp'].'</span>
		</div>
		<div class="col-xs-2 col-sm-6">
			<label><input type="checkbox" class="ios" name="allcores" id="allcores" '.(!empty($_POST) ? (is_array($_POST['cpupin']) ? '' : 'checked="checked"') : 'checked="checked"').' value="-1" onchange="pincheck();" />&nbsp;Default</label>';
		echo '
		</div>
	</div>
	<div id="pincores" class="row">
		<div class="col-xs-10 col-sm-6">
			<label class="control-label">'.$l['cpupin_select'].'</label>
		</div>
		
		<div class="col-xs-2 col-sm-6">';
		for($i=0; $i < $resources['cpucores']; $i++){
			echo '<label><input type="checkbox" id="pin'.$i.'" name="cpupin['.$i.']" value="'.$i.'" '.(in_array($i, @$_POST['cpupin']) || in_array((string)$i, @$_POST['cpupin']) ? 'checked="checked"' : '').' />&nbsp;vCPU '.($i+1).'&nbsp;</label>';
		}
		
		echo '</div>
	</div>-->
	<div class="row">
		<div class="col-sm-6">
			<label class="control-label">'.$l['ip_pool'].'</label>
			<span class="help-block">'.$l['ip_pool_exp'].'</span>
		</div>
		<div class="col-sm-5">
			<select multiple="multiple" class="form-control" name="ippoolid[]" id="ippoolid">';
				foreach($ippools as $k => $v){
					echo '<option value="'.$k.'" '.(in_array($k, $plan['ippoolid']) ? 'selected="selected"' : '').'">'.$v['ippool_name'].(!empty($v['ipv6']) ? " (IPv6)" : (!empty($v['internal']) ? " (".$l['internal'].")" : " (IPv4)")).'</option>';
				}
			echo '</select>
		</div>
	</div>
	<br />
	<div class="row openvz" id="tuntap">
		<div class="col-xs-10 col-sm-6">
			<label class="control-label">'.$l['tuntap'].'</label>
			<span class="help-block">'.$l['exp_tuntap'].'</span>
		</div>
		<div class="col-xs-2 col-sm-6">
			<input type="checkbox" class="ios" name="tuntap" id="tuntap" value="1" '.POSTchecked('tuntap', $plan['tuntap']).'/>
		</div>
	</div><br/>
	<div class="row openvz" id="ppp">
		<div class="col-xs-10 col-sm-6">
			<label class="control-label">'.$l['ppp'].'</label>
			<span class="help-block">'.$l['exp_ppp'].'</span>
		</div>
		<div class="col-xs-2 col-sm-6">
			<input type="checkbox" class="ios" name="ppp" id="ppp" value="1" '.POSTchecked('ppp', $plan['ppp']).'/>
		</div>
	</div>
	<div class="row openvz">
		<div class="col-xs-10 col-sm-6">
			<label class="control-label">'.$l['fuse'].'</label><br />
			<span class="help-block">'.$l['fuse_exp'].'</span>
		</div>
		<div class="col-xs-2 col-sm-6">
			<input type="checkbox" value="1" name="fuse" '.POSTchecked('fuse', $plan['fuse']).' />
		</div>
	</div>
	<div class="row openvz">
		<div class="col-xs-10 col-sm-6">
			<label class="control-label">'.$l['ipip'].'</label><br />
			<span class="help-block">'.$l['ipip_exp'].'</span>
		</div>
		<div class="col-xs-2 col-sm-6">
			<input type="checkbox" value="1" name="ipip" '.POSTchecked('ipip', $plan['ipip']).' />
		</div>
	</div>
	<div class="row openvz">
		<div class="col-xs-10 col-sm-6">
			<label class="control-label">'.$l['ipgre'].'</label><br />
			<span class="help-block">'.$l['ipgre_exp'].'</span>
		</div>
		<div class="col-xs-2 col-sm-6">
			<input type="checkbox" value="1" name="ipgre" '.POSTchecked('ipgre', $plan['ipgre']).' />
		</div>
	</div>
	<div class="row openvz">
		<div class="col-xs-10 col-sm-6">
			<label class="control-label">'.$l['nfs'].'</label><br />
			<span class="help-block">'.$l['nfs_exp'].'</span>
		</div>
		<div class="col-xs-2 col-sm-6">
			<input type="checkbox" value="1" name="nfs" '.POSTchecked('nfs', $plan['nfs']).' />
		</div>
	</div>
	<div class="row openvz">
		<div class="col-sm-6">
			<label class="control-label">'.$l['quotaugidlimit'].'</label><br />
			<span class="help-block">'.$l['quotaugidlimit_exp'].'</span>
		</div>
		<div class="col-sm-6">
			<input type="text" class="form-control" name="quotaugidlimit" id="quotaugidlimit" size="30" value="'.POSTval('quotaugidlimit', $plan['quotaugidlimit']).'" />
		</div>
	</div>
	<div class="row openvz">
		<div class="col-sm-6">
			<label class="control-label">'.$l['iolimit'].'</label><br />
			<span class="help-block">'.$l['iolimit_exp'].'</span>
		</div>
		<div class="col-sm-6">
			<input type="text" class="form-control" name="iolimit" id="iolimit" size="30" value="'.POSTval('iolimit', $plan['iolimit']).'" />
		</div>
	</div>
	<div class="row openvz">
		<div class="col-sm-6">
			<label class="control-label">'.$l['iopslimit'].'</label><br />
			<span class="help-block">'.$l['iopslimit_exp'].'</span>
		</div>
		<div class="col-sm-6">
			<input type="text" class="form-control" name="iopslimit" id="iopslimit" size="30" value="'.POSTval('iopslimit', $plan['iopslimit']).'" />
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<label class="control-label">'.$l['control_panel'].'</label>
			<span class="help-block">'.$l['control_panel_exp'].'</span>
		</div>
		<div class="col-sm-6">
			<select class="form-control" name="control_panel" id="control_panel" >
				<option value="0" '.ex_POSTselect('control_panel', 'none', $plan['control_panel']).'>None</option>
				<option value="cpanel" '.ex_POSTselect('control_panel', 'cpanel', $plan['control_panel']).'>cPanel</option>
				<option value="webuzo" '.ex_POSTselect('control_panel', 'webuzo', $plan['control_panel']).'>Webuzo</option>
				<option value="plesk" '.ex_POSTselect('control_panel', 'plesk', $plan['control_panel']).'>Plesk</option>
				<option value="webmin" '.ex_POSTselect('control_panel', 'webmin', $plan['control_panel']).'>Webmin</option>
				<option value="interworx" '.ex_POSTselect('control_panel', 'interworx', $plan['control_panel']).'>Interworx</option>
				<option value="ispconfig" '.ex_POSTselect('control_panel', 'ispconfig', $plan['control_panel']).'>ISPConfig</option>
				<option value="cwp" '.ex_POSTselect('control_panel', 'cwp', $plan['control_panel']).'>CentOS Web Panel</option>
				<option value="vesta" '.ex_POSTselect('control_panel', 'vesta', $plan['control_panel']).'>VestaCP</option>
			</select>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<label class="control-label">'.$l['recipe'].'</label>
			<span class="help-block">'.$l['recipe_exp'].'</span>
		</div>
		<div class="col-sm-6">
			<select class="form-control" name="recipe" id="recipe" >
				<option value="0" '.ex_POSTselect('recipe', 'none', $plan['recipe']).'>None</option>';
				
				foreach($recipes as $k => $v){
					echo '<option value="'.$k.'" '.ex_POSTselect('recipe', $k, $plan['recipe']).'>'.$v['name'].'</option>';
				}
				
			echo '</select>
		</div>
	</div>
	<div class="kvm proxk xcp xcphvm">
		<div class="row xcp xcphvm">
			<div class="col-sm-6">
				<label class="control-label">'.$l['install_xentools'].'</label>
				<span class="help-block">&nbsp;</span>
			</div>
			<div class="col-sm-6">
				<input type="checkbox" class="ios" name="install_xentools" id="install_xentools"  '.POSTchecked('install_xentools',$plan['install_xentools']).' value="1" />	
			</div>
		</div>
		<div class="row" id="kvm_cpu_mode">
			<div class="col-sm-6">
				<label class="control-label">'.$l['cpu_mode'].'</label>
				<span class="help-block">&nbsp;</span>
			</div>
			<div class="col-sm-6">
				<select class="form-control" name="cpu_mode" id="cpu_mode" >
					<option value="default" '.ex_POSTselect('cpu_mode', 'default', $plan['cpu_mode']).'>Default</option>
					<option value="host-model" '.ex_POSTselect('cpu_mode', 'host-model', $plan['cpu_mode']).'>Host Model</option>
					<option class="kvm" value="host-passthrough" '.ex_POSTselect('cpu_mode', 'host-passthrough', $plan['cpu_mode']).'>Host Passthrough</option>
					<option class="proxk" value="486" '.ex_POSTselect('cpu_mode', '486', $plan['cpu_mode']).'>486</option>
					<option class="proxk" value="athlon" '.ex_POSTselect('cpu_mode', 'athlon', $plan['cpu_mode']).'>athlon</option>
					<option class="proxk" value="pentium" '.ex_POSTselect('cpu_mode', 'pentium', $plan['cpu_mode']).'>pentium</option>
					<option class="proxk" value="pentium2" '.ex_POSTselect('cpu_mode', 'pentium2', $plan['cpu_mode']).'>pentium2</option>
					<option class="proxk" value="pentium3" '.ex_POSTselect('cpu_mode', 'pentium3', $plan['cpu_mode']).'>pentium3</option>
					<option class="proxk" value="coreduo" '.ex_POSTselect('cpu_mode', 'coreduo', $plan['cpu_mode']).'>coreduo</option>
					<option class="proxk" value="core2duo" '.ex_POSTselect('cpu_mode', 'core2duo', $plan['cpu_mode']).'>core2duo</option>
					<option class="proxk" value="kvm32" '.ex_POSTselect('cpu_mode', 'kvm32', $plan['cpu_mode']).'>kvm32</option>
					<option class="proxk" value="qemu32" '.ex_POSTselect('cpu_mode', 'qemu32', $plan['cpu_mode']).'>qemu32</option>
					<option class="proxk" value="qemu64" '.ex_POSTselect('cpu_mode', 'qemu64', $plan['cpu_mode']).'>qemu64</option>
					<option class="proxk" value="phenom" '.ex_POSTselect('cpu_mode', 'phenom', $plan['cpu_mode']).'>phenom</option>
					<option class="proxk" value="Conroe" '.ex_POSTselect('cpu_mode', 'Conroe', $plan['cpu_mode']).'>Conroe</option>
					<option class="proxk" value="Penryn" '.ex_POSTselect('cpu_mode', 'Penryn', $plan['cpu_mode']).'>Penryn</option>
					<option class="proxk" value="Nehalem" '.ex_POSTselect('cpu_mode', 'Nehalem', $plan['cpu_mode']).'>Nehalem</option>
					<option class="proxk" value="Westmere" '.ex_POSTselect('cpu_mode', 'Westmere', $plan['cpu_mode']).'>Westmere</option>
					<option class="proxk" value="SandyBridge" '.ex_POSTselect('cpu_mode', 'SandyBridge', $plan['cpu_mode']).'>SandyBridge</option>
					<option class="proxk" value="IvyBridge" '.ex_POSTselect('cpu_mode', 'IvyBridge', $plan['cpu_mode']).'>IvyBridge</option>
					<option class="proxk" value="Haswell" '.ex_POSTselect('cpu_mode', 'Haswell', $plan['cpu_mode']).'>Haswell</option>
					<option class="proxk" value="Broadwell" '.ex_POSTselect('cpu_mode', 'Broadwell', $plan['cpu_mode']).'>Broadwell</option>
					<option class="proxk" value="Opteron_G1" '.ex_POSTselect('cpu_mode', 'Opteron_G1', $plan['cpu_mode']).'>Opteron_G1</option>
					<option class="proxk" value="Opteron_G2" '.ex_POSTselect('cpu_mode', 'Opteron_G2', $plan['cpu_mode']).'>Opteron_G2</option>
					<option class="proxk" value="Opteron_G3" '.ex_POSTselect('cpu_mode', 'Opteron_G3', $plan['cpu_mode']).'>Opteron_G3</option>
					<option class="proxk" value="Opteron_G4" '.ex_POSTselect('cpu_mode', 'Opteron_G4', $plan['cpu_mode']).'>Opteron_G4</option>
					<option class="proxk" value="Opteron_G5" '.ex_POSTselect('cpu_mode', 'Opteron_G5', $plan['cpu_mode']).'>Opteron_G5</option>
				</select>
			</div>
		</div>
		<div class="row" id="enable_cpu_topology_row">
			<div class="col-sm-6">
				<label class="control-label">'.$l['enable_cpu_topology'].'</label>
				<span class="help-block">'.$l['enable_cpu_topology_exp'].'</span>
			</div>
			<div class="col-sm-6">
				<input type="checkbox" class="ios" name="enable_cpu_topology" id="enable_cpu_topology" '.POSTchecked('enable_cpu_topology', $plan['topology_sockets']).' onchange="change_cpu_topology();" />
			</div>
		</div>
		<div id="cpu_topology">
			<div class="row">
				<div class="col-sm-6">
					<label class="control-label">Sockets</label>
					<span class="help-block">&nbsp;</span>
				</div>
				<div class="col-sm-6">
					<input type="text" name="topology_sockets" id="topology_sockets" class="form-control numbersonly" size="6" value="'.POSTval('topology_sockets', $plan['topology_sockets']).'" />
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<label class="control-label">Cores</label>
					<span class="help-block">&nbsp;</span>
				</div>
				<div class="col-sm-6">
					<input type="text" class="form-control numbersonly" name="topology_cores" id="topology_cores" size="6" value="'.POSTval('topology_cores', $plan['topology_cores']).'" />
				</div>
			</div>
			<div class="row kvm">
				<div class="col-sm-6">
					<label class="control-label">Threads</label>
					<span class="help-block">&nbsp;</span>
				</div>
				<div class="col-sm-6">
					<input type="text" class="form-control numbersonly" name="topology_threads" id="topology_threads" size="6" value="'.POSTval('topology_threads', $plan['topology_threads']).'" />
				</div>
			</div>
		</div>';
	
		if(!empty($isos)){
			echo '<div class="row" id="sec_iso_div">
				<div class="col-sm-6">
					<label class="control-label">'.$l['sec_vsiso'].'</label>
					<span class="help-block">'.$l['sec_vsiso_exp'].'</span>
				</div>
				<div class="col-sm-6">
			
				<select class="form-control" name="sec_iso">
				<option value="0" '.ex_POSTselect('sec_iso', 0, $plan['sec_iso']).'>'.$l['none'].'</option>';
			
				foreach($isos as $k => $v){	
					echo '<option value="'.$k.'" '.ex_POSTselect('sec_iso', $k, $plan['sec_iso']).' >'.$v['name'].'</option>';
				}
				
				 echo'</select>
				</div>
			</div>';
		}
	echo '</div>
	<div class="row kvm xen xenhvm xcp xcphvm" id="vnc_keymap_div">
		<div class="col-sm-6">
			<label class="control-label">'.$l['vnc_keymap'].'</label>
			<span class="help-block">&nbsp;</span>
		</div>
		<div class="col-sm-6">
			<select class="form-control" name="vnc_keymap" id="vnc_keymap" >';
				foreach($vnc_keymap_array as $kk => $vv){
					echo '<option value="'.$vv.'" '.ex_POSTselect('vnc_keymap', $vv, $plan['vnc_keymap']).'>'.$vv.'</option>';
				}
				echo '
			</select>
		</div>
	</div>
	<div class="kvm proxk">
		<div class="row" id="kvm_cache_div">
			<div class="col-sm-6">
				<label class="control-label">'.$l['kvm_cache'].'</label>
				<span class="help-block">'.$l['exp_kvm_cache'].'</span>
			</div>
			<div class="col-sm-6">
				<select class="form-control" name="kvm_cache" id="kvm_cache" >
					<option value="0" '.ex_POSTselect('kvm_cache', 'none', $plan['kvm_cache']).'>None</option>
					<option value="writeback" '.ex_POSTselect('kvm_cache', 'writeback', $plan['kvm_cache']).'>Writeback</option>
					<option value="writethrough" '.ex_POSTselect('kvm_cache', 'writethrough', $plan['kvm_cache']).'>Writethrough</option>
					<option value="default" '.ex_POSTselect('kvm_cache', 'default', $plan['kvm_cache']).'>Default</option>
					<option value="directsync" '.ex_POSTselect('kvm_cache', $plan['kvm_cache']).'>Direct Sync</option>
				</select>
			</div>
		</div><br/>

	
		<div class="row kvm" id="io_mode_div">
			<div class="col-sm-6">
				<label class="control-label">'.$l['io_mode'].'</label>
				<span class="help-block">&nbsp;</span>
			</div>
			<div class="col-sm-6">
				<select class="form-control" name="io_mode" id="io_mode" >
					<option value="0" '.ex_POSTselect('io_mode', 'default', $plan['io_mode']).'>Default</option>
					<option value="native" '.ex_POSTselect('io_mode', 'native', $plan['io_mode']).'>Native</option>
					<option value="threads" '.ex_POSTselect('io_mode', 'threads', $plan['io_mode']).'>Threads</option>
				</select>
			</div>
		</div>
		<div class="row kvm" id="kvm_vga_div">
			<div class="col-xs-10 col-sm-6">
				<label class="control-label">'.$l['kvm_vga'].'</label>
				<span class="help-block"></span>
			</div>
			<div class="col-xs-2 col-sm-6">
				<input type="checkbox" class="ios" name="kvm_vga" id="kvm_vga" onchange="enable_accel();" '.POSTchecked('kvm_vga', $plan['kvm_vga']).' />
			</div>
		</div>
		
		<div class="row proxk" id="proxmox_numa">
			<div class="col-sm-6">
				<label class="control-label">'.$l['usenuma'].'</label>
				<span class="help-block">&nbsp;</span>
			</div>
			<div class="col-sm-6">
				<input type="checkbox" class="ios" name="numa" id="numa" '.POSTchecked('numa', $plan['numa']).' />
			</div>
		</div>
		
		<div class="row kvm" id="enable_acceleration" style="display:none;">
			<div class="col-xs-10 col-sm-6">
				<label class="control-label">'.$l['acceleration'].'</label>
				<span class="help-block">'.$l['acceleration_exp'].'</span>
			</div>
			<div class="col-xs-2 col-sm-6">
				<input type="checkbox" class="ios" name="acceleration" id="acceleration" '.POSTchecked('acceleration', $plan['acceleration']).' />
			</div>
		</div>
		<div class="row kvm" id="total_iops_sec_div">
			<div class="col-sm-6">
				<label class="control-label">'.$l['total_iops_sec'].'</label>
				<span class="help-block">&nbsp;</span>
			</div>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="total_iops_sec" value="'.POSTval('total_iops_sec', $plan['total_iops_sec']).'" />
			</div>
		</div>
		<div class="row kvm" id="read_bytes_sec_div">
			<div class="col-sm-6">
				<label class="control-label">'.$l['read_bytes_sec'].'</label>
				<span class="help-block">&nbsp;</span>
			</div>
			<div class="col-sm-5">
				<input type="text" class="form-control" name="read_bytes_sec" value="'.POSTval('read_bytes_sec', $plan['read_bytes_sec']).'" />
			</div>
			<div class="col-sm-1">&nbsp; MB/s</div>
		</div>
		<div class="row kvm" id="write_bytes_sec_div">
			<div class="col-sm-6">
				<label class="control-label">'.$l['write_bytes_sec'].'</label>
				<span class="help-block">&nbsp;</span>
			</div>
			<div class="col-sm-5">
				<input type="text" class="form-control" name="write_bytes_sec" value="'.POSTval('write_bytes_sec', $plan['write_bytes_sec']).'" />
			</div>
			<div class="col-sm-1">&nbsp; MB/s</div>
		</div>
	</div>
	<div class="row kvm_hvm_div kvm xenhvm xcphvm vzk proxk" id="rdp">
		<div class="col-xs-10 col-sm-6">
			<label class="control-label">'.$l['rdp'].'</label>
			<span class="help-block">'.$l['exp_rdp'].'</span>
		</div>
		<div class="col-xs-2 col-sm-6">
			<input type="checkbox" class="ios" name="rdp" id="rdp" '.POSTchecked('rdp', $plan['rdp']).' />
		</div>
	</div>	
	<div id="tr_pvonhvm" class="row pv_on_hvm_div xenhvm">
		<div class="col-xs-10 col-sm-6">
			<label class="control-label">'.$l['pv_on_hvm'].'</label>
			<span class="help-block">'.$l['exp_pv_on_hvm'].'</span>
		</div>
		<div class="col-xs-2 col-sm-6">
			<input type="checkbox" class="ios" name="pv_on_hvm" id="pv_on_hvm" '.POSTchecked('pv_on_hvm', $plan['pv_on_hvm']).' onchange="ispvonhvm();" />
		</div>
   	</div>
	<div id="tr_viftype" class="row xenhvm">
		<div class="col-sm-6">
			<label class="control-label">'.$l['change_vif_type'].'</label>
			<span class="help-block">'.$l['exp_change_vif_type'].'</span>
		</div>
		<div class="col-sm-6">
			'.$l['viftype_netfront'].'<input type="radio" name="vif_type" id="vif_type"  value="netfront" '.POSTradio('vif_type', 'netfront', $plan['vif_type']).' />
			'.$l['viftype_ioemu'].'<input type="radio" name="vif_type" id="vif_type"  value="ioemu" '.POSTradio('vif_type', 'ioemu', $plan['vif_type']).' />
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<label class="control-label">'.$l['osreinstall'].'</label>
			<span class="help-block">'.$l['exp_osreinstall'].'</span>
		</div>
		<div class="col-sm-6">
			<input type="text" class="form-control" name="osreinstall_limit" id="osreinstall_limit"  value="'.POSTval('osreinstall_limit', $plan['osreinstall_limit']).'"/>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<label class="control-label">'.$l['admin_managed'].'</label>
			<span class="help-block">'.$l['exp_admin_managed'].'</span>
		</div>
		<div class="col-sm-6">
			<input type="checkbox" class="ios" name="admin_managed" id="admin_managed" '.POSTchecked('admin_managed', $plan['admin_managed']).'"/>
		</div>
	</div>
	<div class="row kvm xen xenhvm xcp xcphvm lxc vzk proxk proxl" id="disable_nw_config_row">
		<div class="col-sm-6">
			<label class="control-label">'.$l['disable_nw_config'].'</label>
			<span class="help-block">'.$l['exp_disable_nw_config'].'</span>
		</div>
		<div class="col-sm-6">
			<input type="checkbox" class="ios" name="disable_nw_config" id="disable_nw_config" '.POSTchecked('disable_nw_config', $plan['disable_nw_config']).'"/>
		</div>
	</div>
</div>';

if(!empty($globals['inhouse_billing'])){
	
echo '
<br />
<div class="roundheader" onclick="toggle_advoptions(\'pricing\');" style="cursor:pointer;"><label id="pricing_advoptions_ind" style="width:10px;" >+</label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$l['pricing'].'</div>
<div id="pricing_advoptions" style="display:none; width:100%" ><br />
	<div class="row text-center">
		<div class="col-sm-3"><label>'.$l['regions'].'</label></div>
		<div class="col-sm-8">
			<div class="col-sm-4">
				<label>'.$l['h_rate'].'</label>
			</div>
			<div class="col-sm-4">
				<label>'.$l['m_rate'].'</label>
			</div>
			<div class="col-sm-4">
				<label>'.$l['y_rate'].'</label>
			</div>
		</div>
	</div><hr />
	<div class="row" id="pricerowscontainer" style="padding-left: 20px; padding-right: 20px;">
		<div class="row" id="pricerow[-1]">
			<div class="col-sm-3" style="padding-bottom: 10px;">
				<label>'.$l['all_reg'].'</label>
			</div>
			<div class="col-sm-8">
				<div class="col-sm-4" style="padding-bottom: 10px;">
					<input type="text" class="form-control" name="h_rate[-1]" id="h_rate[-1]" value="'.POSTval('h_rate[-1]', $plan['h_rate']).'" />
				</div>
				<div class="col-sm-4" style="padding-bottom: 10px;">
					<input type="text" class="form-control" name="m_rate[-1]" id="m_rate[-1]" value="'.POSTval('m_rate[-1]', $plan['m_rate']).'" />
				</div>
				<div class="col-sm-4" style="padding-bottom: 10px;">
					<input type="text" class="form-control" name="y_rate[-1]" id="y_rate[-1]" value="'.POSTval('y_rate[-1]', $plan['y_rate']).'" />
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12 text-right" >
			<button type="button" class="btn" data-toggle1="modal" data-target1="#selectregionforpricing" onclick="selectregmod();" >'.$l['add_reg'].'</button>
		</div>
	</div>
	<div class="modal fade" id="selectregionforpricing" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<div class="modal-title" style="float:left;"><b>'.$l['sel_reg'].'</b></div>
					<div class="close" data-dismiss="modal" >X</div>
				</div>
				<div class="modal-body"></div>
			</div>
		</div>
	</div>
	<style>
		#selectregionforpricing .close{
			float: right;
			background-color: red;
			color: white;
			weight: bold;
			cursor: pointer;
			padding: 2px 10px;
			
		}
		#sglist{
		}
		.selregrowclass div{
			background-color: #eaeaea;			
			margin: 1px;
			padding: 5px;
			cursor: pointer;
		}
		.selregrowclass div:active{
			background-color: #a7e2ff;
		}
	</style>
</div>
<script>
	var ratetypes = ["h_rate", "m_rate", "y_rate"];
	
	// tmpsgs will act as a bucket, and store servergroups currently for which pricing is not added.
	var tmpsgs = JSON.parse(JSON.stringify(servergroups));
	
	// Fill and display modal with available servergroups
	function selectregmod(){
		
		if(JSON.stringify(tmpsgs) == "{}"){
			alert("'.$l['no_reg_add'].'");
			return;
		}
		
		var dv = "";
		dv += "<div class=\"row\" id=\"sglist\" style=\"max-height: 500px; overflow: auto; padding: 5px;\" >";
		
		for(var x in tmpsgs){
			if(tmpsgs.hasOwnProperty(x) && tmpsgs[x] != null){
				dv += "<div class=\"col-sm-6 selregrowclass\">";
					dv += "<div class=\"row\" id=\""+tmpsgs[x].sgid+"\" onclick=\"moreregionpricing(this); $(this).removeAttr(\'onclick\'); $(this).css(\'color\', \'#aeaeae\');\">";
						dv += "<div class=\"col-sm-1\">"+tmpsgs[x].sgid+"</div>";
						dv += "<div class=\"col-sm-10\">"+tmpsgs[x].sg_name+"</div>";
					dv += "</div>";
				dv += "</div>";
			}
		}
		dv += "</div>";
		$("#selectregionforpricing .modal-body").html(dv);
		$("#selectregionforpricing").modal({keyboard: true});
		$("#selectregionforpricing").modal({show:true});
	}
	
	// Add selected region (servergroup) pricing row
	function moreregionpricing(selreg, reload){
		
		// Return back if we do not have a valid servergroup to add or if we are not reloading old values back.
		if(reload != 1 && (selreg == "" || $(selreg).attr("id") == null)){
			return false;
		}
		
		// Extract sgid
		var rsgid = (reload == 1 ? selreg : $(selreg).attr("id"));
		
		// Construct div for this sgid:
		var prow = "";
		prow += "<div class=\"row\" id=\"pricerow"+rsgid+"\" style=\"margin1-bottom: 15px;\">";
		prow += "<div class=\"col-sm-3\" style=\"padding-bottom: 10px;\">";
		prow += "<label>"+servergroups[rsgid].sg_name+"</label>";
		prow += "</div>";
		prow += "<div class=\"col-sm-8\">";
		for(var i in ratetypes){
			prow += "<div class=\"col-sm-4\" style=\"padding-bottom: 10px;\">";
			prow += "<input type=\"text\" class=\"form-control\" name=\""+ratetypes[i]+"["+rsgid+"]\" id=\""+ratetypes[i]+"["+rsgid+"]\" value=\"\" />";
			prow += "</div>";
		}
		prow += "</div>";
		prow += "<div class=\"col-sm-1 text-right\">";
		prow += "<button type=\"button\" class=\"btn btn-danger\" style=\"background-color: #d9534f; border-color: #d43f3a; padding: 6px 12px;\" onclick=\"removepricing(this)\">-</button>";
		prow += "</div>";
		prow += "</div>";
		
		// Append this new div to pricerowcontainer div
		$("#pricerowscontainer").append(prow);
		
		// Remove this servergroup from the tmpsgs bucket
		delete tmpsgs[rsgid];
	}
	
	// Remove selected region pricing row
	function removepricing(id){
		// Remove selected pricing region
		$("#"+$(id).parents().eq(1).attr("id")).remove();
		
		// Get Pricing Region ID (ie id of servergroup in servergroups)
		var tmpsgid = parseInt(/\d+/.exec($(id).parents().eq(1).attr("id")));
		
		// If we did not got expected ID
		if(isNaN(tmpsgid)){
			alert("'.$l['delid_not_found'].'");
			return false;
		}
		
		// Add removed servergroup back to tmpsgs bucket
		tmpsgs[tmpsgid] = servergroups[tmpsgid];
	}
	
';
	
	if(empty($_POST)){
		$_POST['h_rate'] = $plan['h_rate'];
		$_POST['m_rate'] = $plan['m_rate'];
		$_POST['y_rate'] = $plan['y_rate'];
	}
	
	// Are the rates posted ? Then we must add them
	if(!empty($_POST['h_rate'])){
		
		echo '
$("#pricing_advoptions_ind").text(" - ");
$("#pricing_advoptions").slideDown("slow");
';
		
		foreach($_POST['h_rate'] as $kk => $vv){
			
			if($kk == -1){
				echo '
				$("#h_rate\\\[-1\\\]").val("'.$_POST['h_rate'][$kk].'");
				$("#m_rate\\\[-1\\\]").val("'.$_POST['m_rate'][$kk].'");
				$("#y_rate\\\[-1\\\]").val("'.$_POST['y_rate'][$kk].'");
				';
				continue;
			}
			
			echo '
			moreregionpricing('.$kk.', 1);
			$("#h_rate\\\["+'.$kk.'+"\\\]").val("'.$_POST['h_rate'][$kk].'");
			$("#m_rate\\\["+'.$kk.'+"\\\]").val("'.$_POST['m_rate'][$kk].'");
			$("#y_rate\\\["+'.$kk.'+"\\\]").val("'.$_POST['y_rate'][$kk].'");
			';
			
		}
		
	}
	
echo '</script>';

}

echo '
<br /><br />
<center><input type="submit" value="'.$l['sub_but'].'" class="btn" name="editplan"></center>
</form>
</div>';

softfooter();

}

