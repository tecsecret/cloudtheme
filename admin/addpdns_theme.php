<?php

//////////////////////////////////////////////////////////////
//===========================================================
// addpdns_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function addpdns_theme(){

global $theme, $globals, $kernel, $user, $l, $cluster, $error, $done;

softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-pdns icon-head"></i> '.$l['add_pdns'].'<span style="float:right;" ><a href="'.$globals['docs'].'Configure_PowerDNS" target="_blank" class="wiki_help" title="'.$l['wiki_help'].'"><i class="icon-help" ></i></a></span></center>';

error_handle($error);

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
}

echo '
<div id="form-container">

<form accept-charset="'.$globals['charset'].'" class="form-horizontal" name="addpdns" method="post" action="" class="form-horizontal">

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['name'].'</label><br />
		<span class="help-block">'.$l['exp_name'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="name" value="'.POSTval('name', '').'" size="30">
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['descr'].'</label><br />
		<span class="help-block">'.$l['exp_descr'].'</span>
	</div>
	<div class="col-sm-6">
		<textarea rows="5" cols="50" class="form-control" name="descr" value="'.POSTval('descr', '').'" ></textarea>
		<span class="help-block"></span>
	</div>
</div>
<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['ipaddress'].'</label><br />
		<span class="help-block">'.$l['exp_ipaddress'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="ipaddress" value="'.POSTval('ipaddress', '').'" size="30">
	</div>
</div>
<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['sql_port'].'</label><br />
		<span class="help-block">'.$l['exp_sql_port'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="sqlport" value="'.POSTval('sqlport', '').'" size="30">
	</div>
</div>
<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['sql_username']	.'</label><br />
		<span class="help-block">'.$l['exp_sql_username'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="sqlusername" value="'.POSTval('sqlusername', '').'" size="30">
	</div>
</div>
<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['sql_password']	.'</label><br />
	</div>
	<div class="col-sm-6">
		<input type="password" class="form-control" name="sqlpassword" value="'.POSTval('sqlpassword', '').'" size="30">
		<span class="help-block"></span>
	</div>
</div>
<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['encrypt_sql_pass'].'</label><br />
		<span class="help-block">'.$l['exp_encrypt_sql_pass'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="checkbox" value="1" class="ios" name="encrypt_sql_pass" '.POSTchecked('encrypt_sql_pass').'>
	</div>
</div>
<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['sql_db']	.'</label><br />
		<span class="help-block">'.$l['exp_sql_db'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="sqldb" value="'.POSTval('sqldb', 'powerdns').'" size="30">
	</div>
</div>
<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['use_ssl']	.'</label><br />
		<span class="help-block">'.$l['exp_use_ssl'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="checkbox" value="1" class="ios" name="use_ssl" '.POSTchecked('use_ssl').'>
	</div>
</div>

</div>
<br /><br />
<center><input type="submit" value="'.$l['sub_but'].'" class="btn" name="addpdns" /></center>

</form>
</div>';

softfooter();

}

?>