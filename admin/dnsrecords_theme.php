<?php

//////////////////////////////////////////////////////////////
//===========================================================
// dnsrecords_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function dnsrecords_theme(){

global $theme, $globals, $kernel, $user, $l, $cluster, $error, $servers, $done, $pdnsid, $dns_records;

softheader($l['<title>']);

echo '
<div class="bg" style="width:99%">
<center class="tit">
<i class="icon icon-pdns icon-head"></i>&nbsp; '.$l['dnsrecords'].'<span style="float:right"><a href="javascript:showsearch();"><img src="'.$theme['images'].'admin/search.gif" /></a></span></center>';

error_handle($error);

echo '<script language="javascript" type="text/javascript"><!-- // --><![CDATA[

function Deldnsrec(id){

	id = id || 0;
	
	// List of ids to delete
	var dnsrec_list = new Array();
	
	if(id < 1){
		
		if($("#dnsrec_task_select").val() != 1){
			alert("'.$l['no_action'].'");
			return false;
		}
		
		$(".ios:checked").each(function() {
			dnsrec_list.push($(this).val());
		});
		
	}else{
		
		dnsrec_list.push(id);
		
	}
	
	if(dnsrec_list.length < 1){
		alert("'.$l['nothing_selected'].'");
		return false;
	}
	
	var dnsrec_conf = confirm("'.$l['del_conf'].'");
	if(dnsrec_conf == false){
		return false;
	}
	
	var finalData = new Object();
	finalData["del"] = dnsrec_list.join(",");
	
	//alert(finalData);
	//return false;
	
	$("#progress_bar").show();
	
	$.ajax({
		type: "POST",
		url: "'.$globals['index'].'act=dnsrecords&pdnsid='.$pdnsid.'&api=json",
		data : finalData,
		dataType : "json",
		success: function(data){
			$("#progress_bar").hide();
			if("done" in data){
				alert("'.$l['action_completed'].'");
			}
			if("error" in data){
				alert(data["error"]);
			}
			location.reload(true);
		},
		error: function(data) {
			$("#progress_bar").hide();
			//alert(data.description);
			return false;
		}
	});
	
	return false;
};

// ]]></script>

<div id="showsearch" style="display:'.(optREQ('search') || (!empty($dns_records) && !empty($globals['showsearch'])) ? "" : "none").';">
<form accept-charset="'.$globals['charset'].'" name="dnsrecords" method="GET" action="" class="form-horizontal">
<input type="hidden" name="act" value="dnsrecords">
<input type="hidden" name="pdnsid" value="'.$pdnsid.'">
<div class="form-group_head">
<div class="row">
	<div class="col-sm-2"><label>'.$l['domain_id'].'</label></div>
    	<div class="col-sm-3"><input type="text" class="form-control" name="domain_id" id="domain_id" size="30" value="'.POSTval('domain_id', '').'" /></div>
	<div class="col-sm-2"><label>'.$l['dns_name'].'</label></div>
    	<div class="col-sm-3"><input type="text" class="form-control" name="dns_name" id="dns_name" size="30" value="'.POSTval('dns_name', '').'" /></div>
</div>
<div class="row">
    <div class="col-sm-2"><label>'.$l['dns_domain'].'</label></div>
    <div class="col-sm-3"><input type="text" class="form-control" name="dns_domain" id="dns_domain" size="30" value="'.POSTval('dns_domain', '').'" /></div>
    <div class="col-sm-2"><label>'.$l['record_type'].'</label></div>
    <div class="col-sm-3">
	<select name="record_type" class="form-control" id="record_type" value="'.POSTselect('record_type', '').'">
		<option value="0">---</option>
		<option name="soa_record" id="soa_record" value="SOA" '.ex_POSTselect('record_type', 'SOA').'>SOA</option>
		<option name="ptr_record" id="ptr_record" value="PTR" '.ex_POSTselect('record_type', 'PTR').'>PTR</option>
		<option name="a_record" id="a_record" value="A" '.ex_POSTselect('record_type', 'A').'>A (host)</option>
		<option name="cname_record" id="cname_record" value="CNAME" '.ex_POSTselect('record_type', 'CNAME').'>CNAME (Aliases)</option>
		<option name="mx_record" id="mx_record" value="MX" '.ex_POSTselect('record_type', 'MX').'>MX (Mail Exchange)</option>
		<option name="txt_record" id="txt_record" value="TXT" '.ex_POSTselect('record_type', 'TXT').'>TXT (Text)</option>
		<option name="ns_record" id="ns_record" value="NS" '.ex_POSTselect('record_type', 'NS').'>NS (Name Servers)</option>
		<option name="aaaa_record" id="aaaa_record" value="AAAA" '.ex_POSTselect('record_type', 'AAAA').'>AAAA (IPV6 Hosts)</option>
	</select>
     </div>
</div>
<div class="row">
   <div class="col-sm-10" style="text-align:center;">
		<input type="submit" name="search" value="'.$l['submit'].'" class="go_btn"/>
   </div>
</div>
  <br/>
</div>
</form>
<br />
<br />
</div>';

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
}

if(empty($dns_records)){

	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.(optREQ('search') ? $l['no_res'] : $l['no_rdns']).'</div>';
	
}
else{

page_links($globals['num_res'], $globals['cur_page'], $globals['reslen']);
echo '<br /><br />
<form accept-charset="'.$globals['charset'].'" name="multi_dnsrec" id="multi_dnsrec" method="post" action="" class="form-horizontal">
	<table align="center" cellpadding="10" cellspacing="1" border="0" width="95%" class="table table-hover tablesorter">
		<tr>
			<th align="center">'.$l['id'].'</th>
			<th align="center">'.$l['name'].'</th>
			<th align="center">'.$l['domain'].'</th>
			<th align="center">'.$l['type'].'</th>
			<th align="center">'.$l['ttl'].'</th>
			<th align="center">'.$l['priority'].'</th>
			<th align="center" width="10" colspan="2">'.$l['manage'].'</th>
			<th><input type="checkbox" class="select_all" name="select_all" id="select_all"></th>	
		<tr>';
	$i = 1;

	foreach($dns_records as $k => $v){
		
		echo '<tr>
			<td align="left">'.$v['id'].'</td>
			<td>'.$v['name'].'</td>
			<td>'.$v['content'].'</td>
			<td align="center">'.$v['type'].'</td>
			<td align="center">'.$v['ttl'].'</td>
			<td align="center">'.(empty($v['priority']) ? '0' : $v['priority']).'</td>
			<td align="center"><a href="'.$globals['index'].'act=add_dnsrecord&pdnsid='.$pdnsid.'&edit='.$v['id'].'" title="'.$l['edit_pdns'].'"><img src="'.$theme['images'].'admin/edit.png" /></a></td>
			<td align="center"><a class="delete" href="javascript:void(0);" onclick="return Deldnsrec('.$k.');" title="'.$l['del_rdns'].'"><img src="'.$theme['images'].'admin/delete.png" /></a></td>
			<td width="20" valign="middle" align="center">
				<input type="checkbox" class="ios" name="dnsrec_list[]" value="'.$k.'"/>
			</td>
		</tr>';
		$i++;
	}
echo '</table>

<div class="row">
		
	<div class="col-sm-7"></div>
	<div class="col-sm-5"><label style="float:left;padding: 8px;padding-left: 37px;">'.$l['with_selected'].'</label>
		<select name="dnsrec_task_select" id="dnsrec_task_select" class="form-control" style="width:35%;float:left;">
			<option value="0">---</option>
			<option value="1">'.$l['ms_delete'].'</option>
		</select>&nbsp;
		<input type="submit" id ="domains_submit" class="go_btn" name="domains_submit" value="Go" onclick="Deldnsrec(); return false;">
	</div>
</div>
</form>


<div id="progress_bar" style="height:125px; display:none">
	<br />
	<center>
		<font id="progress_txt" size="4" color="#222222">'.$l['action_msg'].'</font>
		<br>
		<br>
	</center>
	<table id="table_progress" width="500" height="28" cellspacing="0" cellpadding="0" border="0" align="center" style="border:1px solid #CCC; -moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px;background-color:#efefef;">
		<tbody>
			<tr>
				<td id="progress_color" width="100%" style="background-image: url(themes/default/images/bar.gif); -moz-border-radius: 4px; -webkit-border-radius: 4px; border-radius: 4px;"></td>
				<td id="progress_nocolor"> </td>
			</tr>
		</tbody>
	</table>
	<br>
	<center>
		'.$l['notify_msg'].'
	</center>
</div>

<br />
<center><a href="'.$globals['ind'].'act=add_dnsrecord&pdnsid='.$pdnsid.'" class="link_btn">'.$l['add_dnsrecord'].'</a></center>';
}

page_links($globals['num_res'], $globals['cur_page'], $globals['reslen']);

echo '</div>';
softfooter();

}

?>