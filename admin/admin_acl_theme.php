<?php

//////////////////////////////////////////////////////////////
//===========================================================
// admin_acl_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 2.2.5
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Julien
// Date:       19th July 2012
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function admin_acl_theme(){

global $theme, $globals, $kernel, $user, $l, $acls, $error, $done;

softheader($l['<title>']);

echo '
<div class="bg" style="width: 99%">
<center class="tit">
<i class="icon icon-users icon-head"></i>&nbsp; '.$l['admin_acl_tit'].'</center>';

error_handle($error);

if(!is_allowed('adminacl')){
	softfooter();
	return;
}

echo '<script language="javascript" type="text/javascript"><!-- // --><![CDATA[

function Delacl(aclid){

	aclid = aclid || 0;
	
	// List of ids to delete
	var acl_list = new Array();
	
	if(aclid < 1){
		
		if($("#acl_task_select").val() != 1){
			alert("'.$l['no_action'].'");
			return false;
		}
		
		$(".ios:checked").each(function() {
			acl_list.push($(this).val());
		});
		
	}else{
		
		acl_list.push(aclid);
		
	}
	
	if(acl_list.length < 1){
		alert("'.$l['nothing_selected'].'");
		return false;
	}
	
	var acl_conf = confirm("'.$l['del_conf'].'");
	if(acl_conf == false){
		return false;
	}
	
	var finalData = new Object();
	finalData["delete"] = acl_list.join(",");
	
	//alert(finalData);
	//return false;
	
	$("#progress_bar").show();
	
	$.ajax({
		type: "POST",
		url: "'.$globals['index'].'act=admin_acl&api=json",
		data : finalData,
		dataType : "json",
		success: function(data){
			$("#progress_bar").hide();
			if("done" in data){
				alert("'.$l['action_completed'].'");
			}
			if("error" in data){
				alert(data["error"]);
			}
			location.reload(true);
		},
		error: function(data) {
			$("#progress_bar").hide();
			//alert(data.description);
			return false;
		}
	});
	
	return false;
};

// ]]></script>';
if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp;'.$l['done'].'</div>';
}

if(empty($acls)){

	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['no_acls'].'</div>';
	
}else{
page_links($globals['num_res'], $globals['cur_page'], $globals['reslen']);
echo '<br /><br />
<form accept-charset="'.$globals['charset'].'" name="multi_acl" id="multi_acl" method="post" action="" class="form-horizontal">
<table class="table table-hover tablesorter">
<thead>
	<tr>
		<th align="center" width="50">'.$l['id'].'</th>
		<th align="center">'.$l['name'].'</th>
		<th colspan="2" width="10%">'.$l['manage'].'</th>
		<th width="20"><input type="checkbox" class="select_all" name="select_all" id="select_all"></th>
	</tr>
</thead><tbody>';

$i=1;	
foreach($acls as $k => $v){
	echo '<tr>
		<td align="left">'.$v['aclid'].'</td>
		<td>'.$v['acl_name'].'</td>
		<td align="center"><a href="?act=edit_admin_acl&aclid='.$v['aclid'].'" title="'.$l['edit'].'"><img src="'.$theme['images'].'admin/edit.png" /></a></td>
		<td align="center"><a href="javascript:void(0);" onclick="return Delacl('.$k.');" title="'.$l['delete'].'"><img src="'.$theme['images'].'admin/delete.png" /></a></td>
		<td valign="top" align="center">
			<input type="checkbox" class="ios" name="adminacl_list[]" value="'.$k.'"/>
		</td>
		</tr>';	
	$i++;
}

echo '</tbody></table>
<div class="row bottom-menu">			
	<div class="col-sm-7"></div>
	<div class="col-sm-5">
		<label>'.$l['with_selected'].'</label>
		<select name="acl_task_select" class="form-control" id="acl_task_select">
			<option value="0">---</option>
			<option value="1">'.$l['ms_delete'].'</option>
		</select>&nbsp;
		<button type="submit" class="go_btn" id ="adminacl_submit" name="adminacl_submit" onclick="Delacl(); return false;">'.$l['go'].'</button>
	</div>
</div>
<br/><br/>
</form>

<div id="progress_bar" style="height:125px; display:none">
	<br />
	<center>
		<font id="progress_txt" size="4" color="#222222">'.$l['action_msg'].'</font>
		<br>
		<br>
		<table id="table_progress" width="450" height="28" cellspacing="0" cellpadding="0" border="0" align="center" style="border:1px solid #CCC; -moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px;background-color:#efefef;">
			<tbody>
				<tr>
					<td id="progress_color" width="100%" style="background-image: url(themes/default/images/bar.gif); -moz-border-radius: 4px; -webkit-border-radius: 4px; border-radius: 4px;"></td>
					<td id="progress_nocolor"> </td>
				</tr>
			</tbody>
		</table>
		<br/>'.$l['notify_msg'].'
	</center>
</div>';
}
page_links($globals['num_res'], $globals['cur_page'], $globals['reslen']);
echo '<center><a href="'.$globals['ind'].'act=add_admin_acl"><button class="link_btn">'.$l['add_admin_acl'].'</button></a></center>';
echo '</div>';
softfooter();

}
?>
