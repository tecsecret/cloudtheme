<?php

//////////////////////////////////////////////////////////////
//===========================================================
// vsram_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function vsram_theme(){

global $theme, $globals, $cluster, $servers, $user, $l, $vpsusage, $available, $vpses;

softheader($l['<title>']);

echo '
<div class="bg" style="width: 99%">
<center class="tit"><i class="icon icon-ram icon-head"></i>&nbsp; '.$l['heading'].'<span style="float:right;" ><a href="'.$globals['docs'].'Server_RAM_Usage" target="_blank" class="wiki_help" title="'.$l['wiki_help'].'"><i class="icon-help" ></i></a></span></center>';

// Is it offline ?
$hypervisor_status = $cluster->statewise($globals['server']);
if($hypervisor_status == 0 || $hypervisor_status == 2){

	echo '<div class="e_notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['server_status_'.$hypervisor_status].'</div>';
	
}else{
	
	// Are there any VPSes ?
	if(count($vpses) < 1){
	
		echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
		
	}else{
	
		foreach($vpsusage as $k => $v){
			if($v['used'] <= 0){
				continue;
			}
			$values[$k] = '{"label" : "'.($k > 0 ? 'VPS ID : '.$k : '').' '.$v['label'].'", data: '.$v['used'].'}';
		}
		
		echo '<center><div id="holder" style="width:350px; height: 350px"></div><br /><br /></center>
<script type="text/javascript" charset="utf-8"><!-- // --><![CDATA[

$(function () {
	
	var data = ['.implode(', ', $values).'];
	
	// PIE
	$.plot($("#holder"), data, 
	{
		series: {
			pie: { 
				show: true,
				label: {
					show: false
				}
			}
		},
		grid: {
			hoverable: true
		},
		legend: {
			show: false
		}
	});
	$("#holder").bind("plothover", pieHover);	

	function showTooltip(x, y, contents) {
		$(\'<div id="tooltip">\' + contents + \'</div>\').css( {
			position: "absolute",
			display: "none",
			top: y ,
			left: x + 20,
			border: "1px solid #CCCCCC",
			padding: "2px",
			"background-color": "#EFEFEF",
			opacity: 0.80
		}).appendTo("body").fadeIn(200);
	}
	
	var previousPoint = null;
	function pieHover(event, pos, obj){
		
		if (!obj){
			$("#tooltip").remove();
			previousPoint = null;
			return;
		}
		
		if (previousPoint != obj.seriesIndex) {
			previousPoint = obj.seriesIndex;
			$("#tooltip").remove();
			
			var tmp = ""+obj.series.data;
			var ram = tmp.split(",");
			showTooltip(pos.pageX, pos.pageY, obj.series.label+"<br /> Ram : "+ram[1]+" MB");			
		}		
	}

});

$(document).ready(function(){
	
	$(".altrowstable tr").mouseover(function(){
		var old_class = $(this).attr("class");
		//alert(old_class);
		$(this).attr("class", "tr_bgcolor");
		
		$(this).mouseout(function(){
			$(this).attr("class", old_class);
		});
	});
	
});

// ]]></script>
	
	<table class="table table-hover tablesorter">
	<tr>
		<th align="center">'.$l['vpsid'].'</th>
		<th align="center">'.$l['vpsname'].'</th>
		<th align="center">'.$l['hostname'].'</th>
		<th align="center">'.$l['ram'].'</th>
		<th align="center">'.($cluster->virt(1) ? $l['burst'] : $l['swap']).'</th>
		<th align="center">'.$l['used'].'</th>
	</tr>';

	$i = 1;
	
	foreach($vpses as $k=>$v){
		
		echo '<tr>
			<td align="center">'.$v['vpsid'].'</td>
			<td align="center">'.$v['vps_name'].'</td>
			<td>'.$v['hostname'].'</td>
			<td align="center">'.$v['ram'].' MB</td>
			<td align="center">'.($cluster->virt(1) ? $v['burst'] : $v['swap']).' MB</td>
			<td align="center">'.$vpsusage[$k]['used'].'</td>
		</tr>';
	
		$i++;
	}
		
	echo '</table>';
	
	}
	
}
echo '</div>';
softfooter();

}

?>