<?php

//////////////////////////////////////////////////////////////
//===========================================================
// disk_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function disk_theme(){

global $theme, $globals, $cluster, $user, $l, $disk;

softheader($l['<title>']);

echo '
<div class="bg" style="width:99%">
<center class="tit"><i class="icon icon-disk icon-head"></i>&nbsp; '.$l['header'].'</center><br />';

// Is it offline ?
$hypervisor_status = $cluster->statewise($globals['server']);
if($hypervisor_status == 0 || $hypervisor_status == 2){

	echo '<div class="e_notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['server_status_'.$hypervisor_status].'</div>';
	
}else{

$i = 1;
foreach($disk['disk'] as $k => $v){

echo '
<div class="row">
	<div class="col-sm-6" style="border-right:1px solid #CCCCCC">
		<div class="roundheader">'.$l['diskinfo'].'</div>
		<table align="center" class="table table-hover" cellpadding="3" cellspacing="4" border="0" width="100%">
			<tr><td align="left" class="blue_td" width="30%">'.$l['mounted'].'</td><td class="fhead val">'.$k.'</td></tr>
			<tr><td align="left" class="blue_td">'.$l['totaldisk'].'</td><td class="val">'.$disk['disk'][$k]['limit_gb'].' GB</td></tr>
			<tr><td align="left" class="blue_td">'.$l['diskutilised'].'</td><td class="val">'.$disk['disk'][$k]['used_gb'].' GB</td></tr>
		        <tr><td align="left" class="blue_td">'.$l['diskused'].'</td><td class="val">'.$disk['disk'][$k]['actual_gb'].' GB</td></tr>
			<tr><td align="left" class="blue_td">'.$l['percentdisk'].'</td><td class="val">'.$disk['disk'][$k]['percent'].' %</td></tr>
		</table>
	</div>
	<div class="col-sm-6" align="center" width="50%">
		<div class="roundheader">'.$l['graphheader'].'</div>
		<br />
		<div id="disk'.$i.'_holder" style="width:130px; height:130px;"></div>
	</div>
</div>

<div class="row">
	<div class="col-sm-6" style="border-right:1px solid #CCCCCC">
		<div class="roundheader">'.$l['inodesinfo'].'</div>
		<table align="center" class="table table-hover" cellpadding="3" cellspacing="4" border="0" width="100%">
			<tr><td align="left" class="blue_td" width="30%">'.$l['inodestotal'].'</td><td class="val">'.$disk['inodes'][$k]['limit'].'</td></tr>
			<tr><td align="left" class="blue_td">'.$l['inodesutilised'].'</td><td class="val">'.$disk['inodes'][$k]['used'].'</td></tr>
			<tr><td align="left" class="blue_td">'.$l['percent_inodes'].'</td><td class="val">'.$disk['inodes'][$k]['percent'].' %</td></tr>
		</table>
	</div>
	<div class="col-sm-6" align="center" width="50%">
		<div class="roundheader">'.$l['inodesheader'].'</div>
		<br />
		<div id="inodes'.$i.'_holder" style="width:130px; height:130px;"></div>
	</div>
</div>
<hr>
<br />
<br />
<br />
<br />';
	
	$i++;
}

echo '<script type="text/javascript" charset="utf-8"><!-- // --><![CDATA[	

// Draw a Resource Graph
function resource_graph(id, data){

    $.plot($("#"+id), data, 
	{
		series: {
			pie: { 
				innerRadius: 0.7,
				radius: 1,
				show: true,
				label: {
					show: true,
					radius: 0,
					formatter: function(label, series){
						if(label != "Used") return "";
						return \'<div style="font-size:18px;text-align:center;padding:2px;color:black;">\'+Math.round(series.percent)+\'%</div><div style="font-size:10px;">\'+label+\'</div>\';	
					}
				}
			}
		},
		legend: {
			show: false
		}
	});
}

function drawpie(){';

$i = 1;
foreach($disk['disk'] as $k => $v){

	echo '	
	resource_graph("disk'.$i.'_holder", [
		{ label: "Used",  data: '.$disk['disk'][$k]['used_gb'].'},
		{ label: "Free",  data: '.$disk['disk'][$k]['free_gb'].'}
	]);
	
	resource_graph("inodes'.$i.'_holder", [
		{ label: "Used",  data: '.$disk['inodes'][$k]['used'].'},
		{ label: "Free",  data: '.$disk['inodes'][$k]['free'].'}
	]);
	';
	
	$i++;
}

echo '};

addonload("drawpie();");
// ]]></script>';

}

echo '</div>';
softfooter();

}

?>