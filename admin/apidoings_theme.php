<?php

//////////////////////////////////////////////////////////////
//===========================================================
// apidoings_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function apidoings_theme(){

global $theme, $globals, $kernel, $user, $l, $ipblocks , $cluster, $error, $saved, $serid, $serv;

softheader($l['<title>']);

echo '<center class="tit"><img src="'.$theme['images'].'admin/server.gif" />&nbsp; '.$l['page_head'].'</center>';

if(!empty($saved)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['saved'].'</div><br />';
}

error_handle($error);

echo '<form accept-charset="'.$globals['charset'].'" name="editserver" method="post" action="" class="form-horizontal">

<table cellspacing="0" cellpadding="8" border="0" width="95%" align="center">
<tr>
	<td width="40%">
		<span class="fhead">'.$l['server_name'].'</span><br />
		<span class="exp">'.$l['server_name_exp'].'</span></td>
	<td>
		<input type="text" name="server_name" id="server_name" size="30" value="'.POSTval('server_name', $serv['server_name']).'" />
	</td>	
</tr>

<tr>
	<td>
		<span class="fhead">'.$l['server_ip'].'</span><br />
		<span class="exp">'.$l['server_ip_exp'].'</span></td>
	<td>
		<input type="text" name="ip" id="ip" size="30" value="'.POSTval('ip', $serv['ip']).'" />
	</td>	
</tr>

<tr>
	<td>
		<span class="fhead">'.$l['server_pass'].'</span><br />
		<span class="exp">'.$l['server_pass_exp'].'</span></td>
	<td>
		<input type="text" name="pass" id="pass" size="30" value="'.POSTval('pass', $serv['pass']).'" />
	</td>
</tr>

</table>

<br /><br />
<center><input type="submit" name="editserver" class="submit" onmouseover="sub_but(this)" value="'.$l['sub_but'].'" /></center>

</form>';

softfooter();

}

?>