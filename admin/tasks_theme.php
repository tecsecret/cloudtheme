<?php

//////////////////////////////////////////////////////////////
//===========================================================
// addserver_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function getSortHeader($header_name, $sort_column, $sort_column_by, $column){

	global $theme, $globals;
		
		$str = '<table style="margin:auto;">
	<tr>
		<th rowspan="2" class="tablehead" style="background: none;border: none;color:black;padding-right:6px;">'.$header_name.'</th>
		<td width="10"><a href="'.$globals['index'].'act=tasks&sortcolumn='.$column.'&sortby=asc"><img src="'.$theme['images'].'admin/arrow_up.png"></a></td>
	</tr>
	<tr>
		<td><a href="'.$globals['index'].'act=tasks&sortcolumn='.$column.'&sortby=desc"><img src="'.$theme['images'].'admin/arrow_down.png"></a></td>
	</tr>
	</table>';
		
	return $str;
}

function tasks_theme(){

global $theme, $globals, $servers, $user, $l, $list, $cluster, $error, $done, $servergroups, $migrate_status, $cpu, $progress, $migrate_done, $migrate_run, $mprog, $tasks;

softheader($l['<title>']);

echo '
<div class="bg" style="width:99%">
<div class="modal fade" role="dialog" id="logs_modal">
	<div class="modal-dialog" style="width: 60%; height: 50%;">
		<div class="modal-content">
			<div class="modal-header text-center" id="logs_modal_head">
				<button class="close" data-dismiss="modal">&times;</button>
				<span class="fhead">'.$l['logs'].'</span>
			</div>
			<div class="modal-body" id="logs_modal_body" ></div>
		</div>
	</div>
</div>
<center class="tit"><i class="icon icon-tasks icon-head"></i>&nbsp; '.$l['<title>'].'<span style="float:right"><a href="javascript:showsearch();"><img src="'.$theme['images'].'admin/search.gif" /></a></span></center>';

error_handle($error);

echo '<script language="javascript" type="text/javascript"><!-- // --><![CDATA[
var prog = 0;
function make_progress(index, progress){
	var steps = 20;
	var start = $("#pbar"+index).text();
	start = (start.slice(0, -1));
	if(isNaN(start)){
		start = 0;
	}
	var step = (progress - start)/steps;
	if(step < 0){ step = 0;}
	var increment = function(){
		var val = $("#pbar"+index).text();
		val = Number(val.slice(0, - 1));
		prog = parseInt(val + Math.round(step));
		//alert("progr"+prog);
		if(prog > progress){ prog = progress;}
		if(prog < parseInt("100")){
			$("#pbar"+index).text(prog + "%");
			$("#progressbar"+index).width(prog + "%");
		}else{
			$("#progressbar"+index).width(100 + "%");
		}
		if (prog < progress){setTimeout(increment, 500);}
	};
	increment();
};

function make_table(id, data){
		
	try{
		data = JSON.parse(data);
	}catch(e){
		return;
	}
	
	var html = "<table>";
	$.each(data, function(i, item){
		if(i != "progress"){
			html += "<tr><td><b>"+i+"&nbsp;&nbsp;&nbsp;</b></td><td>"+item+"</td></tr>";
		}
	});
	
	html += "</table>";
	$(id).html(html);
};

function get_taskprogress(act){
	var task_ids = new Object();
	task_ids["actid"] = act.join(",");
	$.ajax({type: "GET",
		url: "'.$globals['index'].'&act=tasks&page='.optREQ('page').'&reslen='.optREQ('reslen').'&api=json",
		data: task_ids,
		dataType : "json",
		success:function(data){
			var tasks = data.tasks;
			$.each(tasks, function(index, value){
				if(tasks[index]["progress"] == 100){
					$("#status"+index).text("'.$l['completed'].'");
					$("#pbar"+index).text("100% '.$l['completed'].' !");
					$("#progressbar"+index).hide();
					$("#progressbar"+index).parent().css("border", "0px");
				}else if(tasks[index]["status"] == -1){
					$("#status"+index).html(tasks[index]["status_txt"]);
					$("#pbar"+index).text("'.$l['task_notcomplete'].'");
					$("#progressbar"+index).hide();
					$("#progressbar"+index).parent().css("border", "0px");
				}else{
					
					// We have to create the table in case of migrate and clone
					if(tasks[index]["action"] == "migrate2" || tasks[index]["action"] == "clone2" || tasks[index]["action"] == "multimigrateprog" || tasks[index]["action"] == "multicloneprog"){						
						make_table("#status"+index, tasks[index]["status_txt"]);
					}else{
						$("#status"+index).text(tasks[index]["status_txt"]);
					}
					
					if(isNaN(tasks[index]["progress"])){
						$("#pbar"+index).text(tasks[index]["progress"]);
						$("#progressbar"+index).hide();
					}else{
						make_progress(index, Number(tasks[index]["progress"]));
						$("#progressbar"+index).show();
					}
				}
				$("#started"+index).html(tasks[index]["started"]);
				$("#updated"+index).html(tasks[index]["updated"]);
				$("#ended"+index).html(tasks[index]["ended"]);
			});
		}
	});
	setTimeout(function(){get_taskprogress(act);}, 10000);
};

$(document).ready(function(){
	var act = new Array();';
	if(count($tasks) > 0){
		foreach($tasks as $k => $v){
			echo 'act.push('.$v['actid'].');';
		}
		echo 'get_taskprogress(act);';
	}
	echo '
	
});

// ]]></script>';

// Take this for display notice when no process available
echo '<div id="showsearch" style="display:'.(optREQ('search') || (!empty($tasks) && !empty($globals['showsearch'])) ? "" : "none").';">
<form accept-charset="'.$globals['charset'].'"  class="form-horizontal" name="tasks" method="GET" action="">
<input type="hidden" name="act" value="tasks">

<div class="form-group_head">
	<div class="row">
		<div class="col-sm-1"><label>'.$l['actid'].'</label></div>
		<div class="col-sm-3"><input type="text" class="form-control" name="actid" id="actid" size="15" value="'.REQval('actid','').'" /></div>
		<div class="col-sm-1"><label>'.$l['vpsid'].'</label></div>
		<div class="col-sm-3"><input type="text" class="form-control" name="vpsid" id="vpsid" size="15" value="'.REQval('vpsid','').'" /></div>
		<div class="col-sm-1"><label>'.$l['user'].'</label></div>
		<div class="col-sm-3"><input type="text" class="form-control" name="username" id="username" size="15" value="'.REQval('username','').'" /></div>
	</div>
	<div class="row">
		<div class="col-sm-1"><label>'.$l['action'].'</label></div>
		<div class="col-sm-3 server-select-lg">
			<select class="form-control virt-select" name="action" id="action" style="width:100%">
				<option value="0">---</option>
				<option value="addvs">'.$l['addvs'].'</option>
				<option value="addvs_restore">'.$l['addvs_restore'].'</option>
				<option value="rebuildvs">'.$l['rebuildvs'].'</option>
				<option value="vpsbackups">'.$l['vpsbackups'].'</option>
				<option value="restorevps">'.$l['restorevps'].'</option>
				<option value="migrate2">'.$l['migrateprog'].'</option>
				<option value="multimigrateprog">'.$l['multimigrateprog'].'</option>
				<option value="clone2">'.$l['cloneprog'].'</option>
				<option value="multicloneprog">'.$l['multicloneprog'].'</option>
				<option value="deletevs">'.$l['deletevs'].'</option>
				<option value="createtemplate">'.$l['createtemplate'].'</option>
				<option value="fstab_handle">'.$l['fstab_handle'].'</option>
				<option value="editxcpvs">'.$l['editxcpvs'].'</option>
				<option value="resizevps">'.$l['resizevps'].'</option>
				<option value="multivirt">'.$l['multivirt'].'</option>
				<option value="getos">'.$l['getos'].'</option>
				<option value="change_dnsnameserver">'.$l['change_dnsnameserver'].'</option>
				<option value="changepassword">'.$l['changepassword'].'</option>
				<option value="install_cp">'.$l['install_cp'].'</option>
				<option value="hostname">'.$l['hostname'].'</option>
				<option value="install_recipe">'.$l['install_recipe'].'</option>
				<option value="bandwidth_unsuspend">'.$l['bandwidth_unsuspend'].'</option>
				<option value="suspend_callback">'.$l['suspend_callback'].'</option>
				<option value="unsuspend_callback">'.$l['unsuspend_callback'].'</option>
				<option value="editvps_callback">'.$l['editvps_callback'].'</option>
				<option value="terminate_callback">'.$l['terminate_callback'].'</option>
				<option value="get_crt">'.$l['get_crt'].'</option>
				<option value="renew_crt">'.$l['renew_crt'].'</option>
				<option value="cron_crt">'.$l['cron_crt'].'</option>
				<option value="haproxy_cron">'.$l['haproxy_cron'].'</option>
				<option value="vpsbackups_plan">'.$l['vpsbackups_plan'].'</option>
				<option value="restorevps_plan">'.$l['restorevps_plan'].'</option>
				<option value="addsshkeys">'.$l['addsshkeys'].'</option>
				<option value="installxentools">'.$l['installxentools'].'</option>
				<option value="dbbackups">'.$l['dbbackups'].'</option>
				<option value="install_script">'.$l['install_script'].'</option>
			</select>
		</div>
		<div class="col-sm-1"><label>'.$l['status'].'</label></div>
		<div class="col-sm-3">
			<select class="form-control" name="status" id="status">
				<option value="">---</option>
				<option value="1">'.$l['inprogress'].'</option>
				<option value="2">'.$l['completed'].'</option>
				<option value="3">'.$l['notupdated'].'</option>
				<option value="-1">'.$l['error'].'</option>			
			</select>
		</div>
		<div class="col-sm-1"><label>'.$l['server'].'</label></div>
		<div class="col-sm-3">
				<select class="form-control" name="server" id="server" style="width:99%">';
					echo "<option value=''>---</option>";
					foreach ($servers as $serid => $server_name) {
						echo "<option value='$serid'>".$server_name['server_name']."</option>";
					}
		echo	'</select>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12" style="text-align: center;"><input type="submit" name="search" value="'.$l['submit'].'" class="go_btn"/></div>
	</div>
</div>


</form>
<br />
<br />
</div>';

// Take this for display notice when no process available
if(empty($tasks)){
	echo '<div class="notice" id="complete"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['no_tasks'].'</div>';
}else{

page_links($globals['num_res'], $globals['cur_page'], $globals['reslen']);

echo '<br /><br />
<table align="center" cellpadding="8" cellspacing="1" border="0" width="95%" class="table table-hover tablesorter tasks_table">
<tr>
	<th width="45">'.getSortHeader($l['actid'], $sort_column, $sort_column_by, 'actid').'</td>
	<th width="70">'.getSortHeader($l['vpsid'], $sort_column, $sort_column_by, 'vpsid').'</th>
	<th width="45">'.$l['user'].'</th>
	<th width ="90">'.getSortHeader($l['server'], $sort_column, $sort_column_by, 'server').'('.$l['id'].')</th>
	<th width="250">'.getSortHeader($l['started'], $sort_column, $sort_column_by, 'started').'</th>
	<th width="250">'.getSortHeader($l['updated'], $sort_column, $sort_column_by, 'updated').'</th>
	<th width="250">'.getSortHeader($l['ended'], $sort_column, $sort_column_by, 'ended').'</th>
	<th width="100">'.$l['action'].'</th>
	<th width="160">'.getSortHeader($l['status'], $sort_column, $sort_column_by, 'status').'</th>
	<th>'.$l['progress'].'</th>
	<th>'.$l['logs'].'</th>
</tr>';

$i = 1;
foreach($tasks as $k => $v){
	echo '<tr>
		<td align="center">'.$v['actid'].'</td>
		<td>'.$v['vpsid'].'</td>
		<td>'.$v['email'].'</td>
		<td>'.(!empty($v['server_name']) ? $v['server_name'].' ('.$v['serid'].')' : $l['unslaved']).'</td>
		<td><span id="started'.$v['actid'].'">'.$v['started'].'</span></td>
		<td><span id="updated'.$v['actid'].'">'.$v['updated'].'</span></td>
		<td><span id="ended'.$v['actid'].'">'.$v['ended'].'</span></td>
		<td>'.$l[$v['action']].'</td>
		<td id="status'.$v['actid'].'"></td>
		<td><div style="width:150px;" id="progress-cont'.$v['actid'].'">
			<center><div style="font-size:15px;text-align:center;" id="pbar'.$v['actid'].'">'.$v['progress'].'%</center>
			<div style="border-radius:4px;background-color: #ddd;border:1px solid #ccc;"><div style="width:0px;height:18px;background-image: url(themes/default/images/bar.gif); -moz-border-radius: 4px; -webkit-border-radius: 4px; border-radius: 4px;" id="progressbar'.$v['actid'].'"></div></div>
			</div>
		</td>
		<td>
			<a href="javascript:void(0);" onclick="return loadlogs('.$v['actid'].');" class="btn" style="padding:3px;">'.$l['show_logs'].'</a>
		</td>
	</tr>';
	$i++;
	}
	echo '</table>';
}

page_links($globals['num_res'], $globals['cur_page'], $globals['reslen']);

echo '</div>';

softfooter();
}

?>