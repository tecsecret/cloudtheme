<?php

//////////////////////////////////////////////////////////////
//===========================================================
// plans_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function getSortHeader($header_name, $sort_column, $sort_column_by, $column){

global $theme, $globals;
	
	$str = '<table style="margin:auto;">
<tr>
	<th rowspan="2" class="tablehead" style="background: none;border: none;color:black;padding-right:6px;">'.$header_name.'</th>
	<td width="10"><a href="'.$globals['index'].'act=user_plans&sortcolumn='.$column.'&sortby=asc"><img src="'.$theme['images'].'admin/arrow_up.png"></a></td>
</tr>
<tr>
	<td><a href="'.$globals['index'].'act=user_plans&sortcolumn='.$column.'&sortby=desc"><img src="'.$theme['images'].'admin/arrow_down.png"></a></td>
</tr>
</table>';
	
	return $str;
}

function user_plans_theme(){

global $theme, $globals, $kernel, $user, $l, $plans, $error, $done;

softheader($l['<title>']);

echo '
<div class="bg" style="width: 99%">
<center class="tit">
<i class="icon icon-plans icon-head"></i>&nbsp; '.$l['plans_tit'].'<span style="float:right"><a href="javascript:showsearch();"><img src="'.$theme['images'].'admin/search.gif" /></a><a href="'.$globals['docs'].'Plans" target="_blank" class="wiki_help" title="'.$l['wiki_help'].'"><i class="icon-help" ></i></a></span></center>';

error_handle($error);

echo '<script language="javascript" type="text/javascript"><!-- // --><![CDATA[

function Delplans(uplid){

	uplid = uplid || 0;
	
	// List of ids to delete
	var plan_list = new Array();
	
	if(uplid < 1){
		
		if($("#plans_task_select").val() != 1){
			alert("'.$l['no_action'].'");
			return false;
		}
		
		$(".ios:checked").each(function() {
			plan_list.push($(this).val());
		});
		
	}else{
		
		plan_list.push(uplid);
		
	}
	
	if(plan_list.length < 1){
		alert("'.$l['nothing_selected'].'");
		return false;
	}
	
	var plan_conf = confirm("'.$l['del_conf'].'");
	if(plan_conf == false){
		return false;
	}
	
	var finalData = new Object();
	finalData["delete"] = plan_list.join(",");
	
	//alert(finalData);
	//return false;
	
	$("#progress_bar").show();
	
	$.ajax({
		type: "POST",
		url: "'.$globals['index'].'act=user_plans&api=json",
		data : finalData,
		dataType : "json",
		success: function(data){
			$("#progress_bar").hide();
			if("done" in data){
				alert("'.$l['action_completed'].'");
				location.reload(true);
			}
		},
		error: function(data) {
			$("#progress_bar").hide();
			//alert(data.description);
			return false;
		}
	});
	
	return false;
};

// ]]></script>

<div id="showsearch" style="display:'.(optREQ('search') || (!empty($plans) && !empty($globals['showsearch'])) ? "" : "none").';">
<form accept-charset="'.$globals['charset'].'" name="plans" id="plans" method="GET" action="" class="form-horizontal">
<input type="hidden" name="act" value="plans">
		
<div class="form-group_head">
  <div class="row">
  <div class="col-sm-1"></div>
    <div class="col-sm-2"><label>'.$l['sbyplan'].'</label></div>
    <div class="col-sm-2"><input type="text" class="form-control" name="planname" id="planname" size="30" value="'.POSTval('planname','').'"/></div>
    <div class="col-sm-2"><label>'.$l['sbyplantype'].'</label></div>
    <div class="col-sm-2">
      <select name="ptype"  style="width:99%" class="form-control">
	      <option value="" '.(optPOST('ptype') == '' ? 'selected=selected' : '').'>All</option>
	      <option value="openvz" '.(optPOST('ptype') == 'openvz' ? 'selected=selected' : '').'>OpenVZ</option>
	      <option value="xen" '.(optPOST('ptype') == 'xen' ? 'selected=selected' : '').'>Xen</option>
	      <option value="kvm" '.(optPOST('ptype') == 'kvm' ? 'selected=selected' : '').'>KVM</option>
	      <option value="xcp" '.(optPOST('ptype') == 'xcp' ? 'selected=selected' : '').'>Xen Server</option>
	      <option value="lxc" '.(optPOST('ptype') == 'lxc' ? 'selected=selected' : '').'>LXC</option>
	      <option value="vzo" '.(optPOST('ptype') == 'vzo' ? 'selected=selected' : '').'>Virtuozzo OpenVZ</option>
	      <option value="vzk" '.(optPOST('ptype') == 'vzk' ? 'selected=selected' : '').'>Virtuozzo KVM</option>
      </select>
    </div>
    <div class="col-sm-2" style="text-align: center;"><button type="submit" name="search" class="go_btn" value="Search"/>'.$l['submit'].'</button></div>
  <div class="col-sm-1"></div>
  </div>
</div>
</form>
<br />
<br />
</div>';

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['saved'].'</div>';
}

if(empty($plans)){

	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.(optREQ('search') ? $l['no_res'] : $l['no_plans']).'</div>';
	
}else{

page_links($globals['num_res'], $globals['cur_page'], $globals['reslen']);
echo '<br /><br />
<form accept-charset="'.$globals['charset'].'" name="multi_plans" id="multi_plans" method="post" action="" class="form-horizontal">
<table class="table table-hover tablesorter">
<tr>
	<th align="center">'.getSortHeader($l['pl_id'], $sort_column, $sort_column_by, 'uplid').'</th>
	<th align="center">'.getSortHeader($l['pl_name'], $sort_column, $sort_column_by, 'plan_name').'</th>
	<th align="center">'.$l['pl_type'].'</th>
	<th align="center">'.getSortHeader($l['pl_space'], $sort_column, $sort_column_by, 'space').'</th>
	<th align="center">'.getSortHeader($l['pl_ram'], $sort_column, $sort_column_by, 'ram').'</th>
	<th align="center">'.$l['pl_burst'].'</th>
	<th align="center">'.getSortHeader($l['pl_cpu'], $sort_column, $sort_column_by, 'cpu').'</th>
	<th align="center">'.getSortHeader($l['pl_cores'], $sort_column, $sort_column_by, 'cores').'</th>
	<th align="center">'.getSortHeader($l['pl_band'], $sort_column, $sort_column_by, 'bandwidth').'</th>
	<th align="center" colspan="2">'.$l['manage'].'</th>
	<th><input type="checkbox" class="select_all" name="select_all" id="select_all"></th>
</tr>';
	$i = 1;
	foreach($plans as $k => $v){
echo '<tr>
	<td align="left">'.$v['uplid'].'</td>
	<td align="left">'.$v['plan_name'].'</td>
	<td align="center"><img src="'.$theme['images'].'admin/user_'.$v['type'].'.gif" /></td>
	<td align="center">'.$v['space'].' GB</td>
	<td align="center">'.$v['ram'].' MB</td>
	<td align="center">'.(!empty($v['swap']) ? $v['swap'] : $v['burst']).' MB</td>
	<td align="center">'. $v['cpu'].'</td>
	<td align="center">'.$v['cores'].'</td>
	<td align="center">'.(empty($v['bandwidth']) ? $l['unlimited'] : $v['bandwidth']).' GB</td>
	<td align="center" width="24">
		<a href="?act=edituser_plans&uplid='.$v['uplid'].'" title="Edit"><img src="'.$theme['images'].'admin/edit.png" /></a>
	</td>
	<td align="center" width="24">
		<a href="javascript:void(0);" onclick="return Delplans('.$k.');" title="Delete"><img src="'.$theme['images'].'admin/delete.png" />
	</td>
	<td width="20" valign="top" align="center">
		<input type="checkbox" class="ios" name="plans_list[]" value="'.$k.'"/>
	</td>
	</tr>';	
	$i++;
	}

echo '</table>

<div class="row bottom-menu">
		
	<div class="col-sm-7"></div>
	<div class="col-sm-5"><label>'.$l['with_selected'].'</label>
		<select class="form-control" name="plans_task_select" id="plans_task_select">
			<option value="0">---</option>
			<option value="1">'.$l['ms_delete'].'</option>
		</select>&nbsp;
		<input type="submit" id ="plans_submit" class="go_btn" name="plans_submit" value="'.$l['go'].'" onclick="Delplans(); return false;">
	</div>
</div>

</form>

<div id="progress_bar" style="height:125px; display:none">
	<br />
	<center>
		<font id="progress_txt" size="4" color="#222222">'.$l['action_msg'].'</font>
		<br>
		<br>
	</center>
	<table id="table_progress" width="500" height="28" cellspacing="0" cellpadding="0" border="0" align="center" style="border:1px solid #CCC; -moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px;background-color:#efefef;">
		<tbody>
			<tr>
				<td id="progress_color" width="100%" style="background-image: url(themes/default/images/bar.gif); -moz-border-radius: 4px; -webkit-border-radius: 4px; border-radius: 4px;"></td>
				<td id="progress_nocolor"> </td>
			</tr>
		</tbody>
	</table>
	<br>
	<center>
		'.$l['notify_msg'].'
	</center>
</div>

<br />
<center><a href="'.$globals['ind'].'act=adduser_plans" class="link_btn">Add User Plan</a></center>';

}

page_links($globals['num_res'], $globals['cur_page'], $globals['reslen']);
echo '</div>';
softfooter();

}
?>