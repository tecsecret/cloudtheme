<?php

//////////////////////////////////////////////////////////////
//===========================================================
// ips_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function ipranges_theme(){	

global $theme, $globals, $servers, $user, $l, $alerts, $ips, $error, $done;

softheader($l['<title>']);
	
echo '
<div class="bg" style="width: 99%">
<center class="tit">
<i class="icon icon-ippool icon-head"></i></i>&nbsp; '.$l['ip_list'].' 
<span style="float:right"><a href="javascript:showsearch();"><img src="'.$theme['images'].'admin/search.gif" /></a></span>
</center>';
	
error_handle($error);

echo '<script language="javascript" type="text/javascript"><!-- // --><![CDATA[

function show_confirm(ipid){
	
	ipid = ipid || 0;
	
	// List of ids to delete
	var ip_list = new Array();
	
	var selopts = $("#ipranges_task_select option");
	
	selectvals = new Array();
	
	for(i=0; i<selopts.length; i++){
		val = selopts.eq(i).val();
		
		if(val != 0){
			selectvals.push(val);
		}
	}
	
	confirmmsg = ["'.$l["del_conf"].'","'.$l["lockip_conf"].'","'.$l["unlockip_conf"].'"];
	donemsg = ["'.$l["done"].'","'.$l["done_lockip"].'","'.$l["done_unlockip"].'"];
	selectIndx = selectvals.indexOf($_("ipranges_task_select").value);
	
	if(ipid < 1){
		
		if(selectIndx == -1){
			alert("'.$l['no_action'].'");
			return false;
		}
		
		$(".ios:checked").each(function() {
			ip_list.push($(this).val());
		});
	
	}else{
		
		ip_list.push(ipid);
		selectIndx = 0;
		
	}
	
	var finalData = new Object();
	
	finalData["do"] = selectvals[selectIndx];
	finalData["ips"] = ip_list.join(",");
	
	if(selectvals[selectIndx] == "del"){
		finalData["delete"] = finalData["ips"];
	}
	
	if(ip_list.length < 1){
		alert("'.$l['nothing_selected'].'");
		return false;
	}
	
	var ip_conf = confirm(confirmmsg[selectIndx]);
	if(ip_conf == false){
		return false;
	}
	
	//alert(finalData);
	//return false;
	
	$("#progress_bar").show();
	
	$.ajax({
		type: "POST",
		url: "'.$globals['index'].'act=ipranges&api=json",
		data : finalData,
		dataType : "json",
		success: function(data){
			$("#progress_bar").hide();
			if("done" in data){
				alert(donemsg[selectIndx]);
			}
			if("error" in data){
				alert(data["error"]);
			}
			location.reload(true);
		},
		error: function(data) {
			$("#progress_bar").hide();
			//alert(data.description);
			alert("'.$l['error_occurred'].'");
			return false;
		}
	});
	
	return false;

};
// ]]></script>
<div id="showsearch" style="display:'.(optREQ('search') || (!empty($ips) && !empty($globals['showsearch'])) ? "" : "none").';">
<form accept-charset="'.$globals['charset'].'" name="ipranges" method="GET" action="" class="form-horizontal">
<input type="hidden" name="act" value="ipranges">
		
<div class="form-group_head">
  <div class="row">
    <div class="col-sm-1"><label>'.$l['sbyip'].'</label></div>
    <div class="col-sm-4"><input type="text" class="form-control" name="ipsearch" id="ipsearch" size="30" value="'.POSTval('ipsearch', '').'"/></div>
    <div class="col-sm-1"><label>'.$l['sbyippool'].'</label></div>
    <div class="col-sm-2"><input type="text" class="form-control" name="ippoolsearch" id="ippoolsearch" size="30" value="'.POSTval('ippoolsearch','').'"/></div>
    <div class="col-sm-2"><label>'.$l['locked_label'].'</label></div>
	<div class="col-sm-2">
		<select name="lockedsearch" id="lockedsearch" class="form-control">
			<option value="-1" '.(REQval('lockedsearch') == -1 ? 'selected="selected"' : '').'>'.$l['show_all'].'</option>
			<option value="showlocked" '.(REQval('lockedsearch') == "showlocked" ? 'selected="selected"' : '').'>'.$l['show_locked'].'</option>
			<option value="hidelocked" '.(REQval('lockedsearch') == "hidelocked" ? 'selected="selected"' : '').'>'.$l['show_unlocked'].'</option>
		</select>
	</div>
  </div>
  <div class="row text-center">
	<div class="col-sm-12" style="text-align: center;"><button type="submit" name="search" class="go_btn" value="Search"/>'.$l['submit'].'</button></div>
  </div>
</div>
		
<br />
<br />
</form>
</div>';

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
}

if(empty($ips)){

	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.(optREQ('search') ? $l['no_res'] : $l['no_ips']).'</div>';
	
}else{

page_links($globals['num_res'], $globals['cur_page'], $globals['reslen']);
echo '<br /><br />
<form accept-charset="'.$globals['charset'].'" name="multi_ipranges" id="multi_ipranges" method="post" action="" class="form-horizontal">
<table class="table table-hover tablesorter">
<tr>
	<th>ID</td>
	<th align="center" width="10px"></th>
	<th width="350">'.$l['sbyip'].'</td>
	<th width="100">'.$l['ip_pool'].'</th>
	<th width="300">'.$l['ip_mac'].'</th>
	<th width="150">'.$l['assigned_to'].'</th>
	<th colspan="2">'.$l['manage'].'</th>
	<th><input type="checkbox" class="select_all" name="select_all" id="select_all"></th>
</tr>';

$i = 1;

foreach($ips as $k => $v){	

	echo '<tr>
		<td align="left">'.$v['ipid'].'</td>
		<td>'.(!empty($v['locked']) ? '<span><img src="'.$theme['images'].'admin/lock.png" title="'.$l['locked_title'].'"></span> ' : '').'</td>
		<td class="ips">'.$v['ip'].'/'.$v['ipr_netmask'].'</td>
		<td>'.(empty($v['ippool_name']) ? '<i>'.$l['none'].'</i>' : $v['ippool_name']).'</td>
		<td class="ips_small">'.(empty($v['mac_addr']) ? '' : $v['mac_addr']).'</td>
		<td>'.(empty($v['vpsid']) ? '<em>'.$l['none'].'</em>' : $v['hostname'].' (<em>'.$v['vpsid'].'</em>)').'</td>
		<td>
			<a href="'.$globals['ind'].'act=editiprange&ipid='.$k.'" title="'.$l['edit_ip'].'"><img src="'.$theme['images'].'admin/edit.png" /></a>
		</td>
		<td class="manage-ico">
			<a href="javascript:void(0);" onclick="return show_confirm('.$k.');"  title="'.$l['del_ip'].'"><img src="'.$theme['images'].'admin/delete.png" /></a>
		</td>
		<td align="center">
			<input type="checkbox" class="ios" name="ipranges_list[]" value="'.$k.'"/>
		</td>
	</tr>';
	$i++;

}
echo '</table>

		
<div class="row bottom-menu">
		
	<div class="col-sm-7"></div>
	<div class="col-sm-5"><label>'.$l['with_selected'].'</label>
		<select name="ipranges_task_select" id="ipranges_task_select" class="form-control">
			<option value="0">---</option>
			<option value="del">'.$l['ms_delete'].'</option>
			<option value="lockip">'.$l['ms_lockip'].'</option>
			<option value="unlockip">'.$l['ms_unlockip'].'</option>
		</select>&nbsp;
		<input type="submit" id ="ipranges_submit" class="go_btn" name="ipranges_submit" value="Go" onclick="show_confirm(); return false;">
	</div>
</div>
</form>

<div id="progress_bar" style="height:125px; display:none">
	<br />
	<center>
		<font id="progress_txt" size="4" color="#222222">'.$l['action_msg'].'</font>
		<br>
		<br>
	</center>
	<table id="table_progress" width="500" height="28" cellspacing="0" cellpadding="0" border="0" align="center" style="border:1px solid #CCC; -moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px;background-color:#efefef;">
		<tbody>
			<tr>
				<td id="progress_color" width="100%" style="background-image: url(themes/default/images/bar.gif); -moz-border-radius: 4px; -webkit-border-radius: 4px; border-radius: 4px;"></td>
				<td id="progress_nocolor"> </td>
			</tr>
		</tbody>
	</table>
	<br>
	<center>
		'.$l['notify_msg'].'
	</center>
</div>';
	
}	

page_links($globals['num_res'], $globals['cur_page'], $globals['reslen']);

echo '<br />
<center><input type="button" value="'.$l['add_ips'].'" class="link_btn" onclick="window.location =\''.$globals['ind'].'act=addiprange\';"></center></div>
</div>
';
		
softfooter();

}

?>