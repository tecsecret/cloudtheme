<?php

//////////////////////////////////////////////////////////////
//===========================================================
// addmg_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function addmg_theme(){

global $theme, $globals, $kernel, $_user, $l, $error, $done;

softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-ostemplates icon-head"></i>&nbsp; '.$l['page_head'].'</center>';

error_handle($error);

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
}else{

echo '
<style>
.mgtype{
	padding: 10px 10px 10px 10px;
}
</style>

<script language="javascript" type="text/javascript">

function changetype(mg_type){	
	$_("mg_type").value = mg_type;
};

addonload("changetype(\''.POSTval('mg_type', 'openvz').'\');");

</script>

<div id="form-container">
<form accept-charset="'.$globals['charset'].'" class="form-horizontal" name="addmg" method="post" action="" class="form-horizontal">


<div class="row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['mg_name'].'</label><br />
		<span class="help-block">'.$l['mg_name_exp'].'</span>
	</div>
	<div class="col-sm-8">
	<input type="text" class="form-control" name="mg_name" id="mg_name" size="30" value="'.POSTval('mg_name', '').'" />
	</div>
</div>

<div class="row">
	<div class="col-sm-4">
	<label class="control-label">'.$l['mg_desc'].'</label><br />
		<span class="help-block">'.$l['mg_desc_exp'].'</span>
	</div>
	<div class="col-sm-8">
		<textarea class="form-control" name="mg_desc" id="mg_desc" cols="40">'.POSTval('mg_desc', '').'</textarea>
		<span class="help-block"></span>
	</div>
</div>

<div class="row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['mg_type'].'</label><br />
		<span class="help-block">'.$l['mg_type_exp'].'</span>
	</div>
	<div class="col-sm-8">
		<select class="form-control" id="mg_type" name="mg_type">
			<option value="openvz" id="openvz">OpenVZ</option>
			<option value="xen" id="xen">Xen</option>
			<option value="xenhvm" id="xenhvm">Xen - HVM</option>
			<option value="xcp" id="xcp">XCP</option>
			<option value="xcphvm" id="xcphvm">XCP - HVM</option>
			<option value="kvm" id="kvm">KVM</option>
			<option value="lxc" id="lxc">LXC</option>
			<option value="vzo" id="vzo">Virtuozzo OpenVZ</option>
			<option value="vzk" id="vzk">Virtuozzo KVM</option>
			<option value="proxk" id="proxk">Proxmox Qemu</option>
			<option value="proxl" id="proxl">Proxmox LXC</option>
			<option value="proxo" id="proxo">Proxmox OpenVZ</option>
		</select>
	</div>
</div>

</div>
<br /><br />
<center><input type="submit" name="addmg" value="'.$l['submit'].'" class="btn"></center>
</form>
</div>';

}

echo '</div>';
softfooter();

}

?>