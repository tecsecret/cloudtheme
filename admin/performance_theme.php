<?php

//////////////////////////////////////////////////////////////
//===========================================================
// performance_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function performance_theme(){

global $theme, $globals, $cluster, $user, $l, $perfomance;


// Is AJAX mode on
if(optGET('ajax')){	
	
	echo 'cpuusage.push('.$perfomance['cpu'].'); ramusage.push('.$perfomance['ram'].');';
	
	return true;

}

softheader($l['<title>']);

echo '
<div class="bg" style="width:99%">
<center class="tit">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="icon icon-performance icon-head"></i>&nbsp;'.$l['<title>'].'<span style="float:right;" ><a href="'.$globals['docs'].'Performance" target="_blank" class="wiki_help" title="'.$l['wiki_help'].'"><i class="icon-help" ></i></a></span></center>';

// Is it offline ?
$hypervisor_status = $cluster->statewise($globals['server']);
if($hypervisor_status == 0 || $hypervisor_status == 2){

	echo '<div class="e_notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['server_status_'.$hypervisor_status].'</div>';
	
}else{

echo'<center class="tit"><img src="'.$theme['images'].'cpu.png" />&nbsp;CPU</center><br />

<center><div id="cpuholder" style="width:500px; height: 250px;"></div></center>

<br />
<br />
<br />

<center class="tit"><img src="'.$theme['images'].'ram.png" />&nbsp;RAM</center><br />
<center><div id="ramholder" style="width:500px; height: 250px;"></div></center>';

echo '<script language="javascript" type="text/javascript"><!-- // --><![CDATA[
    
	function makedata(data){
		var fdata = [];
		i = 0;		
		for (x in data){
			fdata.push([i, data[x]]);
			i++;
		}	
		return fdata;		
	}
	
	var tmp = [];
	for (p = 0; p <= 300; p++){
		tmp.push(0);
	}	
	
	var cpuusage = tmp;
	var ramusage = tmp;
	
    // setup plot
    var _options = {
        series: { 
			shadowSize: 0, 
			color: "#4b66cb",
			lines: { show: true, fill: true, steps: false }
		},		
        yaxis: { min: 0, max: 100 },
        xaxis: { show: false },
		grid: {
			borderColor: "#CCC"
		}
    };
	
	var ram_options = {
        series: { 
			shadowSize: 0, 
			color: "#1F880D",
			lines: { show: true, fill: true, steps: false }
		},		
        yaxis: { min: 0, max: 100 },
        xaxis: { show: false },
		grid: {
			borderColor: "#CCC"
		}
    };
	
    var cpuplot = $.plot($("#cpuholder"), [makedata(cpuusage)], _options);
	var ramplot = $.plot($("#ramholder"), [makedata(ramusage)], ram_options);

    function _update(re) {
	
		if(re.length > 0){
			try{
				eval(re);
			}catch(e){ }
		}
	 	
		// CPU
		if (cpuusage.length > 300){
			cpuusage = cpuusage.slice(1);
		}
		
		cpuplot.setData([ makedata(cpuusage) ]);
		cpuplot.draw();
	 	
		// RAM
		if (ramusage.length > 300){
			ramusage = ramusage.slice(1);
		}
		
		ramplot.setData([ makedata(ramusage) ]);
		ramplot.draw();
    }
	
	function getusage(){
		if(AJAX("'.$globals['index'].'act=performance&ajax=true", "_update(re)")){
			return false;
		}else{
			return true;	
		}
	};

	function startusage(){
		ajaxtimer = setInterval("getusage()", 3000);
		_update("");
	};
	
    startusage();

// ]]></script>';

}

echo '</div>';
softfooter();

}

?>
