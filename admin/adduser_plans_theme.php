<?php

//////////////////////////////////////////////////////////////
//===========================================================
// adduser_plans_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function adduser_plans_theme(){

global $theme, $globals, $kernel, $_user, $l, $error, $done, $mgs, $servergroups, $dnsplans, $acls;

softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-users icon-head"></i>'.$l['add_user'].'<span style="float:right;" ><a href="'.$globals['docs'].'Add_User" target="_blank" class="wiki_help" title="'.$l['wiki_help'].'"><i class="icon-help" ></i></a></span></center>';

error_handle($error);

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
}else{

echo '<script language="javascript" type="text/javascript">
	
function changepriority(priority){
	$_("io_0").className = "col-sm-4 col-xs-4 io";
	$_("io_1").className = "col-sm-4 col-xs-4 io";
	$_("io_2").className = "col-sm-4 col-xs-4 io";
	
	if (priority != ""){
		$_("io_"+priority).className = "col-sm-4 col-xs-4 io_over";
		$_("prior").value = priority;
	}
	
	if(priority == 1){
		$("#tr_acl").show();
	}else{
		$("#tr_acl").hide();
	}
	
	$(".cloudfield").each(function(){
		if(priority == 2){
			$(this).show();
		}else{
			$(this).hide();
		}
	});

	update_max_cost();
};

// Update the media groups when selected
function updatemg(){	

	$("#mg option").each(function(){
	try{
		if(!$_($(this).attr("type")).checked){
			$(this).attr("disabled","disabled");
		}else{
			$(this).attr("disabled", false);
		}
	}catch(e){}
	});

}

function update_max_cost(){
	
	try{
	
	// Show max cost field
	if($_("inhouse_billing").checked){
		$("#max_cost_row").show();
	}else{
		$("#max_cost_row").hide();
	}
	
	}catch(e){}
	
}

addonload("changepriority(\''.POSTval('priority', 2).'\'); updatemg();");
</script>
<style>
input[type="text"]{
	width:90%;
	float:left;
}
input[type="password"]{
	width:90%;
	float:left;
}

.sideLabel label{
	padding:8px;
	float:left;
}
</style>
<div id="form-container">
<form accept-charset="'.$globals['charset'].'" name="adduser_plans" method="post" action="" class="form-horizontal">

<div class="row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['usertype'].'</label><br />
		<span class="help-block">'.$l['typeofuser'].'</span>
	</div>
	<div class="col-sm-8">
		<input type="hidden" id="prior" name="priority" value="">
		<div class="row" style="margin:0px; width:90%;">
			<div id="io_0" class="col-sm-4 col-xs-4 io"><a href="javascript:changepriority(\'0\');"><img src="'.$theme['images'].'admin/user_0.gif" /> '.$l['user_0'].'</a></div>
			<div id="io_2" class="col-sm-4 col-xs-4 io"><a href="javascript:changepriority(\'2\');"><img src="'.$theme['images'].'admin/user_2.gif" /> '.$l['user_2'].'</a></div>
			<div id="io_1" class="col-sm-4 col-xs-4 io"><a href="javascript:changepriority(\'1\');"><img src="'.$theme['images'].'admin/user_1.gif" /> '.$l['user_1'].'</a></div>
		</div>
	</div>
</div>

<div class="row" id="plan_name_row">
	<div class="col-sm-4">
	<label class="control-label">'.$l['plan_name'].'</label><br />
		<span class="help-block">'.$l['plan_name_exp'].'</span>
	</div>
	<div class="col-sm-8">
		<input type="text" class="form-control" name="plan_name" id="plan_name" size="15" value="'.POSTval('plan_name', '').'" />
	</div>
</div>

<div class="row" id="tr_acl">
	<div class="col-sm-4">
		<label class="control-label">'.$l['acl_plan'].'</label>
	</div>
	<div class="col-sm-8">
		<select class="form-control" name="acl_id" style="width:90%">
		<option value="0">'.$l['no_acl'].'</option>';
		foreach($acls as $k => $v){
			echo '<option value="'.$v['aclid'].'" '.POSTselect('acl_id', $v['aclid']).'>'.$v['acl_name'].'</option>';
		}
echo '</select>
<span class="help-block"></span>
	</div>
</div>

<div class="row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['dns_plan'].'</label><br />
		<span class="help-block">'.$l['dnsplan_exp'].'</span>
	</div>
	<div class="col-sm-8">
		<select class="form-control" name="dnsplan_id" style="width:90%">
		<option value="0">'.$l['no_plan'].'</option>';
		foreach($dnsplans as $k => $v){
			echo '<option value="'.$v['dnsplid'].'">'.$v['plan_name'].'</option>';
		}
echo '</select>
	</div>
</div>

<div class="row cloudfield" id="num_vs_row">
	<div class="col-sm-4">
	<label class="control-label">'.$l['num_vs'].'</label><br />
		<span class="help-block">'.$l['num_vs_exp'].'</span>
	</div>
	<div class="col-sm-8">
		<input type="text" class="form-control" name="num_vs" id="num_vs" size="15" value="'.POSTval('num_vs', '').'" />
	</div>
</div>
';

if(!empty($globals['inhouse_billing'])){
	
echo '
<div class="row cloudfield" id="inhouse_billing_row">
	<div class="col-xs-10 col-sm-4">
	<label class="control-label">'.$l['inhouse_billing'].'</label><br />
		<span class="help-block">'.$l['inhouse_billing_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-8">
		<input type="checkbox" class="ios" value="1" name="inhouse_billing" id="inhouse_billing" '.POSTchecked('inhouse_billing').' onchange="update_max_cost()" />
	</div>
</div>

<div class="row cloudfield" id="max_cost_row">
	<div class="col-sm-4">
	<label class="control-label">'.$l['max_cost'].'</label><br />
		<span class="help-block">'.$l['max_cost_exp'].'</span>
	</div>
	<div class="col-sm-8">
		<input type="text" class="form-control" name="max_cost" id="max_cost" size="15" value="'.POSTval('max_cost', '').'" />
	</div>
</div>';

}

echo '
<div class="row cloudfield" id="num_users_row">
	<div class="col-sm-4">
	<label class="control-label">'.$l['num_users'].'</label><br />
		<span class="help-block">'.$l['num_users_exp'].'</span>
	</div>
	<div class="col-sm-8">
		<input type="text" class="form-control" name="num_users" id="num_users" size="15" value="'.POSTval('num_users', '').'" />
	</div>
</div>

<div class="row cloudfield" id="space_row">
	<div class="col-sm-4">
	<label class="control-label">'.$l['space'].'</label><br />
		<span class="help-block">'.$l['space_exp'].'</span>
		<span class="help-block"></span>
	</div>
	<div class="col-sm-8 sideLabel">
		<input type="text" class="form-control" name="space" id="space" size="15" value="'.POSTval('space', '').'" /><label>GB</label>
	</div>
</div>

<div class="row cloudfield" id="ram_row">
	<div class="col-sm-4">
	<label class="control-label">'.$l['ram'].'</label><br />
		<span class="help-block">'.$l['ram_exp'].'</span>
		<span class="help-block"></span>
	</div>
	<div class="col-sm-8 sideLabel">
		<input type="text" class="form-control" name="ram" id="ram" size="15" value="'.POSTval('ram', '').'" /><label>MB</label>
	</div>
</div>

<div class="row cloudfield" id="burst_row">
	<div class="col-sm-4">
	<label class="control-label">'.$l['burst'].'</label><br />
		<span class="help-block">'.$l['burst_exp'].'</span>
		<span class="help-block"></span>
	</div>
	<div class="col-sm-8 sideLabel">
		<input type="text" class="form-control" name="burst" id="burst" size="15" value="'.POSTval('burst', '').'" /><label>MB</label>
	</div>
</div>

<div class="row cloudfield" id="bandwidth_row">
	<div class="col-sm-4">
	<label class="control-label">'.$l['bandwidth'].'</label><br />
		<span class="help-block">'.$l['bandwidth_exp'].'</span>
	</div>
	<div class="col-sm-8 sideLabel">
		<input type="text" class="form-control" name="bandwidth" id="bandwidth" size="15" value="'.POSTval('bandwidth', '').'" /><label>GB</label>
	</div>
</div>

<div class="row cloudfield" id="cpu_row">
	<div class="col-sm-4">
	<label class="control-label">'.$l['cpu'].'</label><br />
		<span class="help-block">'.$l['cpu_exp'].'</span>
		<span class="help-block"></span>
	</div>
	<div class="col-sm-8">
		<input type="text" class="form-control" name="cpu" id="cpu" size="15" value="'.POSTval('cpu', '').'" />
	</div>
</div>

<div class="row cloudfield" id="cores_row">
	<div class="col-sm-4">
	<label class="control-label">'.$l['cores'].'</label><br />
		<span class="help-block">'.$l['cores_exp'].'</span>
	</div>
	<div class="col-sm-8">
		<input type="text" class="form-control" name="cores" id="cores" size="15" value="'.POSTval('cores', '4').'" />
	</div>
</div>

<div class="row cloudfield" id="cpu_percent_row">
	<div class="col-sm-4">
	<label class="control-label">'.$l['cpu_percent'].'</label><br />
		<span class="help-block">'.$l['cpu_percent_exp'].'</span>
	</div>
	<div class="col-sm-8">
		<input type="text" class="form-control" name="cpu_percent" id="cpu_percent" size="15" value="'.POSTval('cpu_percent', '').'" />
	</div>
</div>

<div class="row cloudfield" id="num_cores_row">
	<div class="col-sm-4">
	<label class="control-label">'.$l['num_cores'].'</label><br />
		<span class="help-block">'.$l['num_cores_exp'].'</span>
	</div>
	<div class="col-sm-8">
	<input type="text" class="form-control" name="num_cores" id="num_cores" size="15" value="'.POSTval('num_cores', '0').'" />
	</div>
</div>

<div class="row cloudfield" id="num_ipv4_row">
	<div class="col-sm-4">
	<label class="control-label">'.$l['num_ipv4'].'</label><br />
		<span class="help-block">'.$l['num_ipv4_exp'].'</span>
		<span class="help-block"></span>
	</div>
	<div class="col-sm-8">
	<input type="text" class="form-control" name="num_ipv4" id="num_ipv4" size="15" value="'.POSTval('num_ipv4', '').'" />
	</div>
</div>

<div class="row cloudfield" id="num_ipv6_subnet_row">
	<div class="col-sm-4">
	<label class="control-label">'.$l['num_ipv6_subnet'].'</label><br />
		<span class="help-block">'.$l['num_ipv6_subnet_exp'].'</span>
		<span class="help-block"></span>
	</div>
	<div class="col-sm-8">
	<input type="text" class="form-control" name="num_ipv6_subnet" id="num_ipv6_subnet" size="15" value="'.POSTval('num_ipv6_subnet', '').'" />
	</div>
</div>

<div class="row cloudfield" id="num_ipv6_row">
	<div class="col-sm-4">
	<label class="control-label">'.$l['num_ipv6'].'</label><br />
		<span class="help-block">'.$l['num_ipv6_exp'].'</span>
		<span class="help-block"></span>
	</div>
	<div class="col-sm-8">
	<input type="text" class="form-control" name="num_ipv6" id="num_ipv6" size="15" value="'.POSTval('num_ipv6', '').'" />
	</div>
</div>

<script type="text/javascript">
function netspeed(r){
	$_("network_speed").value = r;
}
function upspeed(r){
	$_("upload_speed").value = r;
}
</script>';

$network_speed_values = array(128 => 1, 256 => 2, 384 => 3, 512 => 4, 640 => 5, 768 => 6, 896 => 7, 1024 => 8, 1152 => 9, 1280 => 10, 1920 => 15, 2560 => 20, 3849 => 30, 5120 => 40, 6400 => 50, 7680 => 60, 8960 => 70, 10240 => 80, 11520 => 90, 12800 => 100, 128000 => 1000, 1280000 => 10000);

echo '
<div class="row cloudfield" id="network_speed_row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['network_speed'].'</label><br />
		<span class="help-block">'.$l['network_speed_exp'].'</span>
	</div>
	<div class="col-sm-8 sideLabel">
		<input type="text" class="form-control" name="network_speed" id="network_speed" size="8" value="'.POSTval('network_speed', '').'"  style="width:30%;"/><label>'.$l['net_kb'].'</label>
		<select class="form-control" name="network_speed2" id="network_speed2" onchange="netspeed(this.value)" style="width:52%">
			<option value="0" selected="selected">'.$l['no_limit'].'</option>';
			foreach($network_speed_values as $k => $v){
				echo '<option value="'.$k.'">'.$k.' kb/s ('.$v.'mbit)</option>';
			}
		echo '
		</select>
	 </div>
</div>

<div class="row cloudfield" id="upload_speed_row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['upload_speed'].'</label><br />
		<span class="help-block">'.$l['upload_speed_exp'].'</span>
	</div>
	<div class="col-sm-8 sideLabel">
		<input type="text" class="form-control" name="upload_speed" id="upload_speed" size="8" value="'.POSTval('upload_speed', '-1').'" style="width:30%;"/><label>'.$l['net_kb'].'</label>
		<select class="form-control" name="upload_speed2" id="upload_speed2" onchange="upspeed(this.value)" style="width:52%">
		<option value="0" selected="selected">'.$l['no_limit'].'</option>';
			foreach($network_speed_values as $k => $v){
				echo '<option value="'.$k.'">'.$k.' kb/s ('.$v.'mbit)</option>';
			}
		echo '
		</select>
	 </div>
</div>
<div class="row cloudfield" id="band_suspend">
	<div class="col-sm-4">
		<label class="control-label">'.$l['band_suspend'].'</label><br />
		<span class="help-block">'.$l['band_suspend_exp'].'</span>
	</div>
	<div class="col-sm-8">
		<input type="checkbox" class="ios" name="band_suspend" id="ck_band_sus" '.POSTchecked('band_suspend').' />
	</div>
</div>
<div class="row cloudfield" id="service_period">
	<div class="col-sm-4">
		<label class="control-label">'.$l['service_period'].'</label><br />
		<span class="help-block">'.$l['service_period_exp'].'</span>
	</div>
	<div class="col-sm-8">
		<input type="number" style="width:45px;" min="0" max="31" id="txt_service_period" name="service_period" value="'.POSTval('service_period', '0').'" />
	</div>
</div>

<div class="row cloudfield" id="allowed_virts_row">
	<div class="col-xs-10 col-sm-4">
	<label class="control-label">'.$l['allowed_virts'].'</label><br />
		<span class="help-block">'.$l['allowed_virts_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-8">
		<select class="form-control" id="allowed_virts" multiple="multiple" name="allowed_virts[]" style="width:90%" onchange="updatemg()">';
		foreach($globals['virts'] as $k => $v){
			echo '<option value="'.$v.'" '.(@in_array($k, $_POST['allowed_virts']) ? 'selected="selected"' : '').'>'.$l['virt_'.$k].'</option>';
		}
		echo '
		</select>
		<span class="help-block"></span>
	</div>
</div>

<div class="row cloudfield" id="sg_row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['sg'].'</label><br />
		<span class="help-block">'.$l['sg_exp'].'</span>
	</div>
	<div class="col-sm-8">
		<select class="form-control" name="sgs[]" multiple="multiple" style="width:90%">';
			
			foreach($servergroups as $sk => $sv){
				echo '<option value="'.$sk.'" '.(in_array($sk, @$_POST['sgs']) ? 'selected="selected"' : '').'>'.$sv['sg_name'].'</option>';
			}
			
		echo '</select>
		<span class="help-block"></span>
	</div>
</div>

<div class="row cloudfield" id="mg_row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['mg'].'</label><br />
		<span class="help-block">'.$l['mg_exp'].'</span>
	</div>
	<div class="col-sm-8">
		<select class="form-control" name="mgs[]" id="mg" multiple="multiple" style="width:90%">';
			
			foreach($mgs as $mk => $mv){
				echo '<option value="'.$mk.'" type="'.$mv['mg_type'].'" '.(in_array($mk, @$_POST['mgs']) ? 'selected="selected"' : '').'>'.$mv['mg_name'].'</option>';
			}
			
		echo '</select>
	</div>
</div>

<span class="help-block"></span>

<br /><br />
<center><input type="submit" name="adduser_plans" value="'.$l['submit'].'" class="btn"></center>
<br /><br />
</form>
';

}

echo '</div>';
softfooter();

}

?>