<?php

//////////////////////////////////////////////////////////////
//===========================================================
// editiso_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function editiso_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $cluster, $mgs, $isos, $isouuid, $done;

softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-ostemplates icon-head"></i> &nbsp; '.$l['editiso'].'</center>';

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div><br />';
}

error_handle($error);

echo '<form accept-charset="'.$globals['charset'].'" name="editiso" method="post" action="" class="form-horizontal">

	<div class="row">
		<div class="col-sm-6"><label class="control-label">'.$l['isofile'].'</label></div>
		<div class="col-sm-6">
			<input type="text" class="form-control" name="filename" id="base" size="40" value="'.$isos[$isouuid]['filename'].'" disabled="disabled" />
			<span class="help-block"></span>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6"><label class="control-label">'.$l['media_groups'].'</label></div>
		<div class="col-sm-6">
			<select class="form-control" name="mgs[]" multiple="multiple">';
				
				foreach($mgs as $mk => $mv){
					if(!$kernel->features('iso_support', $mv['mg_type'])) continue;
					echo '<option value="'.$mk.'" '.(in_array($mk, $isos[$isouuid]['mg']) ? 'selected="selected"' : '').'>'.$mv['mg_name'].'</option>';
				}
				
			echo '</select>
		</div>
	</div>

<br />
<br />
<center><input type="submit" name="editiso" value="'.$l['submit'].'" class="btn"></center>
	
</form>
</div>
';

softfooter();

}

?>