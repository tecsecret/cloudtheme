<?php

//////////////////////////////////////////////////////////////
//===========================================================
// createtemplate_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function createtemplate_theme(){

global $theme, $globals, $kernel, $user, $l, $info, $error, $vs, $oslist, $ostemplates, $oses, $done, $actid, $flag_createtemplate, $distros;

if($flag_createtemplate){
	echo $actid;
	$flag_createtemplate = 0;
	return;
}

if(optGET('done')){		
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
}

softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-ostemplates icon-head"></i> &nbsp; '.$l['page_title'].'<span style="float:right;" ><a href="'.$globals['docs'].'Create_OS_Template" target="_blank" class="wiki_help" title="'.$l['wiki_help'].'"><i class="icon-help" ></i></a></span></center>';

error_handle($error);

if(!empty($done)){
	
}

echo '<script language="javascript" type="text/javascript">

	var actid = null;
	var progress = 0;
	var progress_update = "";
	
	$(document).ready(function(){
		progress_onload();
		$(".wiki_help").tipTip({delay:100, defaultPosition:"left"});
		$("#createtemplate-form").submit(function(){
		var params = $("#createtemplate-form").serialize();
			
			$.ajax({type: "POST",
				url: "'.$globals['index'].'jsnohf=1&act=createtemplate",
				data:params+"&createtemp=1",
				success:function(response){
					if(!isNaN(response)){
						actid = response;
						setTimeout("get_progress(\'createtemplate\')", 1000);
					}
					else{
						$("#softcontent").html(response);
						changeplan();
					}
				}
			});
			
			$("#error_box").remove();
			$("#form-container").hide();
			$("#progress-cont").show();
			$("#pbar").html("'.$l['checking_data'].'" + " ( 0% ) ");
			window.scrollTo(0, 0);
			$("#progressbar").progressbar({value: 0});
			
			return false;
		});
		
	});

function changeplan(){
try{
	var vpsid = $_("vpsid").value;
	var plan = getAttributeByName($_("vs"+vpsid), "type");
	var distro = getAttributeByName($_("vs"+vpsid), "distro");
}catch(e){ };

	if(plan == "undefined" || !plan){
		plan = "openvz";
	}
	
	if(distro == "undefined" || !distro){
		distro = "others";
	}
	
	$_("openvz").style.display = "none";
	$_("xen").style.display = "none";
	$_("xenhvm").style.display = "none";
	$_("kvm").style.display = "none";
	$_("xcp").style.display = "none";
	$_("xcphvm").style.display = "none";
	$_("lxc").style.display = "none";
	$_("vzo").style.display = "none";
	$_("vzk").style.display = "none";
	$_("proxo").style.display = "none";
	$_("proxl").style.display = "none";
	$_("proxk").style.display = "none";
	$_(plan).style.display = "";	
	
	$("[id^=\'temp_\']").each(function() {
		$(this).hide();
	});		
	$_("temp_"+distro).style.display = "";	
	
	$_("pygrub").style.display = "none";
	$_("extrac").style.display = "none";
	$_("drive").style.display = "none";
	$_("fstype").style.display = "none";
	
	$_("noresizefs").style.display = "none";
	$_("perf_ops").style.display = "none";
	
	if(plan == "xen"){
		$_("pygrub").style.display = "";
		$_("extrac").style.display = "";
		$_("drive").style.display = "";
		$_("fstype").style.display = "";
	}else if(plan == "xenhvm" || plan == "kvm" || plan == "vzk" || plan == "proxk"){
		$_("extrac").style.display = "";
		$_("noresizefs").style.display = "";
		$_("perf_ops").style.display = "";
	}else if(plan == "xcp" || plan == "xcphvm"){
		$_("noresizefs").style.display = "";
		$_("perf_ops").style.display = "";
	}else if(plan == "lxc"){
		$_("perf_ops").style.display = "";
	}
};

addonload("changeplan();");

</script>';

echo '<div id="form-container">
<form id="createtemplate-form" accept-charset="'.$globals['charset'].'" action="" method="post" name="createtmp" class="form-horizontal">

	<div class="row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['selectvps'].'</label><br />
		<span class="help-block">'.$l['selectvps_exp'].'</span>
	</div>
	<div class="col-sm-8 server-select-lg" valign="top">';
		
		echo '<select class="form-control virt-select" name="vpsid" id="vpsid" onchange="changeplan();">
		<option value="0">'.$l['please_choose'].'</option>';
			
 		foreach($vs as $k => $v){  
	    	echo '<option id="vs'.$v['vpsid'].'" type="'.$v['virt'].(empty($v['hvm']) ? '' : 'hvm').'" distro="'.$v['os_distro'].'" '.(POSTval('vpsid') == $v['vpsid'] ? 'selected="selected"' : ''). ' value="'.$v['vpsid'].'">'.$v['vpsid'].' - '.$v['hostname'].'</option>';
		}
		
		echo '</select>
	</div>
</div>

<div class="row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['tmpname'].'</label><br />
		<span class="help-block">'.$l['tmpname_exp'].'</span>
	</div>
	<div class="col-sm-8">
		<input type="text" class="form-control" name="tmpname" id="tmpname" size="16" value="'.POSTval('tmpname', '').'" />
	</div>
</div>

<div class="row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['type'].'</label>
	</div>
	<div class="col-sm-6">
			<div class="row">
				<div class="col-sm-1" id="openvz" align="left"><img src="'.$theme['images'].'admin/openvz_100.gif" /></div>
				<div class="col-sm-1" id="xen" align="left"><img src="'.$theme['images'].'admin/xen_100.gif" /></div>
				<div class="col-sm-1" id="xenhvm" align="left"><img src="'.$theme['images'].'admin/xenhvm_100.gif" /></div>
				<div class="col-sm-1" id="kvm" align="left"><img src="'.$theme['images'].'admin/kvm_100.gif" /></div>
				<div class="col-sm-1" id="xcp" align="left"><img src="'.$theme['images'].'admin/xcp_100.gif" /></div>
				<div class="col-sm-1" id="xcphvm" align="left"><img src="'.$theme['images'].'admin/xcphvm_100.gif" /></div>
				<div class="col-sm-1" id="lxc" align="left"><img src="'.$theme['images'].'admin/lxc_100.gif" /></div>
				<div class="col-sm-1" id="vzo" align="left"><img src="'.$theme['images'].'admin/vzo_100.gif" /></div>
				<div class="col-sm-1" id="vzk" align="left"><img src="'.$theme['images'].'admin/vzk_100.gif" /></div>
				<div class="col-sm-1" id="proxo" align="left"><img src="'.$theme['images'].'admin/proxo_100.gif" /></div>
				<div class="col-sm-1" id="proxk" align="left"><img src="'.$theme['images'].'admin/proxk_100.gif" /></div>
				<div class="col-sm-1" id="proxl" align="left"><img src="'.$theme['images'].'admin/proxl_100.gif" /></div>
			</div>
	</div>
</div>
<div class="row"><br/>
</div>	
<div class="row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['vps_os_type'].'</label><br />
		<span class="help-block">'.$l['vps_os_type_exp'].'</span>
	</div>
	<input type="hidden" id="template" name="template" value="0">
	<div class="col-sm-8" id="tmp_distro" align="left">';
	foreach($distros as $k => $v){
		echo '<span id="temp_'.$k.'" align="left"><img src="'.distro_logo($k).'" /></span>';
	}
	
	echo '<span id="temp_others" align="left"><img src="'.distro_logo('others').'" /></span>
	</div>
</div>

<div class="row" id="pygrub">
	<div class="col-sm-4">
		<label class="control-label">'.$l['pygrub'].'</label>
		<span class="help-block"></span>
	</div>
	<div class="col-sm-8"><input type="checkbox" class="ios" name="pygrub" '.POSTchecked('pygrub').'/></div>
</div>
<div class="row" id="fstype">
	<div class="col-sm-4">
		<label class="control-label">'.$l['fstype'].'</label><br />
		<span class="help-block">'.$l['fstype_exp'].'</span>
	</div>
	<div class="col-sm-8"><input type="checkbox" class="ios" name="fstype" '.POSTchecked('fstype').'/></div>
</div>
<div class="row" id="drive">
	<div class="col-sm-4">
		<label class="control-label">'.$l['drive'].'</label><br />
		<span class="help-block">'.$l['drive_exp'].'</span>
	</div>
	<div class="col-sm-8"><input type="text" class="form-control" name="drive" id="driveid" value="'.POSTval('drive', '').'" size="10" /></div>
</div>

<div class="row" id="noresizefs">
	<div class="col-sm-4">
		<label class="control-label">'.$l['noresizefs'].'</label>
		<span class="help-block"></span>
	</div>
	<div class="col-sm-8"><input type="checkbox" class="ios" name="noresizefs" '.POSTchecked('noresizefs').'/></div>
	<span class="help-block"></span>
</div>

<div class="row" id="perf_ops">
	<div class="col-sm-4">
		<label class="control-label">'.$l['perf_ops'].'</label><br />
		<span class="help-block">'.$l['perf_ops_exp'].'</span>
	</div>
	<div class="col-sm-8"><input type="checkbox" class="ios" name="perf_ops" '.POSTchecked('perf_ops').'/></div>
</div>

<div class="row" id="extrac">
	<div class="col-sm-4"><label class="control-label">'.$l['extra'].'</label></div>
	<div class="col-sm-8"><textarea class="form-control" name="extra" rows="3" cols="30">'.POSTval('extra', '').'</textarea></div>
</div>	

<br />
<br />
<center>
<input type="submit" value="'.$l['submit_button'].'" name="createtemp" class="btn" />
</center>
<br />
<br />
</form>
</div>
<div id="progress_onload"></div>';

softfooter();

}

?>