<?php

//////////////////////////////////////////////////////////////
//===========================================================
// sslcert_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function sslcert_theme(){

global $theme, $globals, $cluster, $user, $l, $info, $cacertconf, $done, $error;

softheader($l['<title>']);

echo '<center class="tit"><img src="'.$theme['images'].'/admin/sslbig.gif" />&nbsp; '.$l['<title>'].'</center>';

error_handle($error);

// Is it offline ?
$hypervisor_status = $cluster->statewise($globals['server']);
if($hypervisor_status == 0 || $hypervisor_status == 2){

	echo '<div class="e_notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['server_status_'.$hypervisor_status].'</div>';
	
}else{

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
}

echo'<form accept-charset="'.$globals['charset'].'" name="sslcert" method="post" action="" class="form-horizontal">
<table cellspacing="0" cellpadding="8" border="0" width="95%" align="center">
<tr>
	<td valign="top">
		<span class="fhead">'.$l['install_key'].'</span><br />
		<span class="exp">'.$l['install_key_desc'].'</span>
	</td>
	<td>
		<textarea style="color: #333333; border: 1px solid #CCCCCC; font-family:monospace" cols="70" name="cacert_crt" rows="15">'.POSTval('cacert_crt', $cacertconf).'</textarea>
	</td>
</tr>
</table>
<center><input type="submit" name="savesslcert" value="'.$l['submit'].'" class="submit" onmouseover="sub_but(this)"></center>
</form>';

}

softfooter();

}

?>