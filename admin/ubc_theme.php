<?php

//////////////////////////////////////////////////////////////
//===========================================================
// ubc_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function ubc_theme(){

global $theme, $globals, $kernel, $ubc, $l, $error, $done, $vps, $plan, $curubc, $_ubc;

softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-config icon-head"></i>&nbsp; '.$l['ubcsetting'].'</center>';

error_handle($error);

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.(!empty($plan) ? $l['done'] : $l['done_vps']).'</div>';
}

echo '
<form accept-charset="'.$globals['charset'].'" class="form-horizontal" name="ubcsettings" method="post" action="" class="form-horizontal">

<br />
<center><div class="roundheader">'.(!empty($plan) ? $l['plan_name'].' : '.$plan['plan_name'].' (<i>'.$l['id'].' '.$plan['plid'].'</i>)' : 
$l['vps_name'].' : '.$vps['vps_name'].' (<i>'.$l['id'].' '.$vps['vpsid'].'</i>)').'</div></center>

<br />

<div class="row">
	<div class="col-sm-4"></div>
	<div class="col-sm-4">
		<label class="control-label">'.$l['barrier'].'</label>
	</div>
	<div class="col-sm-4">
		<label class="control-label">'.$l['limit'].'</label>
	</div>
</div>';

foreach($_ubc as $k){
	
	echo '<div class="row">
	<div class="col-sm-4">
		<label class="control-label">'.$l[$k].'</label></div>
	<div class="col-sm-4">
		<input type="text" class="form-control" name="b'.$k.'" id="b'.$k.'" size="30" value="'.POSTval('b'.$k, @$curubc[$k][0]).'" />
		<span class="help-block"></span>
	</div>
	<div class="col-sm-4">
		<input type="text" class="form-control" name="l'.$k.'" id="l'.$k.'" size="30" value="'.POSTval('l'.$k, @$curubc[$k][1]).'" />
	</div>
</div>';
	
}

echo '

<br /><br />

<center><input type="submit" value="'.$l['saveubc'].'" class="btn" name="saveubc"></center>

</form>
</div>
';

softfooter();

}
?>