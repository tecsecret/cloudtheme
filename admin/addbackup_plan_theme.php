<?php

//////////////////////////////////////////////////////////////
//===========================================================
// addbackup_plan_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 2.8.1
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Husain
// Date:       11th November 2015
// Time:       16:15 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function addbackup_plan_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $cluster, $done, $backupservers, $servers, $servergroups;
	
softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-plans icon-head"></i>&nbsp; '.$l['addbackup_plan'].'<span style="float:right;" ><a href="'.$globals['docs'].'VPS_Backups" target="_blank" class="wiki_help" title="'.$l['wiki_help'].'"><i class="icon-help" ></i></a></span></center>';

error_handle($error);
if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
}

	echo '
	<script language="javascript" type="text/javascript">
				
	$(document).ready(function(){
	
		$("input:radio").change(function(event){
			if(event.target.value == "weekly"){
				$("#backupday_tr").show();
				$("#backupdate_tr").hide();
				$("#tr_time").show();
				$("#backuphourly_tr").hide();
			}else if(event.target.value == "monthly"){
				$("#backupdate_tr").show();
				$("#backupday_tr").hide();
				$("#tr_time").show();
				$("#backuphourly_tr").hide();
			}else if(event.target.value == "daily"){
				$("#backupdate_tr").hide();
				$("#backupday_tr").hide();
				$("#tr_time").show();
				$("#backuphourly_tr").hide();
			}else if(event.target.value == "hourly"){
				$("#backupdate_tr").hide();
				$("#backupday_tr").hide();
				$("#tr_time").hide();
				$("#backuphourly_tr").show();
			}
		});

		$("#type").change(function(data){
			if(data.target.value == "LOCAL"){
				$("#tr_backup_servers").hide();
			}else{
				$("#tr_backup_servers").show();
				if(data.target.value == "FTP"){
					$(".FTP").show();
					$(".SSH").hide();
				}else{
					$(".SSH").show();
					$(".FTP").hide();
				}
			}

		});

	});
	
	function toggle_advoptions(ele){
			
		var div_ele = $("#"+ele+"_advoptions");
		
		if (div_ele.is(":hidden")){
			$("#"+ele+"_advoptions_ind").text(" - ");
			div_ele.slideDown("slow");
		}
		else{
			$("#"+ele+"_advoptions_ind").text(" + ");
			div_ele.slideUp("slow");
		}
	}
	
	</script>
	
	<div id="form-container">
	<form accept-charset="'.$globals['charset'].'" action="" method="post" name="vpsbackups" class="form-horizontal">
		<div class="row">
			<div class="col-sm-6">
				<label class="control-label">'.$l['backup_disabled'].'</label><br />
				<span class="help-block">'.$l['exp_backup_disabled'].'</span>
			</div>
			<div class="col-sm-6">
				<input type="checkbox" id="disabled" class="ios" name="disabled" '.POSTchecked('disabled').'/>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<label class="control-label">'.$l['planname'].'</label>
				<span class="help-block">'.$l['nameofplan'].'</span>
			</div>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="plan_name" id="plan_name" value="'.POSTval('plan_name', '').'" />
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<label class="control-label">'.$l['sel_type'].'</label><br />
			</div>
			<div class="col-sm-6">
				<select id="type" class="form-control" name="type" id="type">
					<option value="LOCAL" '.(POSTselect('type', 'LOCAL')).'>Local</option>
					<option value="FTP" '.(POSTselect('type', 'FTP')).'>FTP</option>
					<option value="SSH" '.(POSTselect('type', 'SSH')).'>SSH</option>
				</select>
			</div>
		</div>
		<br/>
		<div class="row" id="tr_backup_servers" style="display:none">
			<div class="col-sm-6">
				<label class="control-label">'.$l['sel_backupserver'].'</label><br />
				<span class="help-block">'.$l['exp_sel_backupserver'].'</span>
			</div>
			<div class="col-sm-6">
				<select class="form-control" name="id">';
				foreach($backupservers as $k => $v){
					echo '<option class="'.$v['type'].'" value="'.$v['bid'].'" '.POSTselect('id', $v['bid']).'>'.$v['name'].'</option>';
				}
			echo '</select>
			</div>
		</div>					
		<div class="row" id="tr_dir">
			<div class="col-sm-6">
				<label class="control-label">'.$l['local_dir'].'</label><br />
				<span class="help-block">'.$l['exp_local_dir'].'</span>
			</div>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="dir" id="dir" value="'.POSTval('dir', '').'" size="40"/>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<label class="control-label">'.$l['backup_freq'].'</label><br />
				<span class="help-block">'.$l['exp_backup_freq'].'</span>
			</div>
			<div class="col-sm-6">
				<input type="radio" id="freqh" name="freq" value="hourly" '.POSTradio('freq', 'hourly').'/>
				<label for="freqh">'.$l['freq_hourly'].'</label>
				<input type="radio" id="freqd" name="freq" value="daily" '.POSTradio('freq', 'daily').'/>
				<label for="freqd">'.$l['freq_daily'].'</label>
				<input type="radio" id="freqw" name="freq" value="weekly" '.POSTradio('freq', 'weekly').'/>
				<label for="freqw">'.$l['freq_weekly'].'</label>
				<input type="radio" id="freqm" name="freq" value="monthly" '.POSTradio('freq', 'monthly').'/>
				<label for="freqm">'.$l['freq_monthly'].'</label>
			</div>
		</div>
		<div class="row" id="backuphourly_tr" style="display:none">
			<div class="col-sm-6">
				<label class="control-label">'.$l['backup_hourly'].'</label><br />
				<span class="help-block">'.$l['exp_backup_hourly'].'</span>
			</div>
			<div class="col-sm-6">
				<select class="form-control" name="hourly_freq" style="width:88%;float:left">';
					for($i=0;$i<24;$i++){
						echo '<option value="'.($i < 10 ? '0'.$i : $i).'" '.POSTselect('hourly_freq', $i).' >'.($i < 10 ? '0'.$i : $i).'</option>';
					}
			echo'</select><label class="side_lbl">'.$l['hrs'].'</label>
			</div>
		</div>		
		<div class="row" id="backupday_tr" style="display:none;">
			<div class="col-sm-6">
				<label class="control-label">'.$l['backup_day'].'</label><br />
				<span class="help-block">'.$l['exp_backup_day'].'</span>
			</div>
			<div class="col-sm-6">
				<select class="form-control" name="day">
					<option value="1" '.POSTselect('day', "1").' >Monday</option>
					<option value="2" '.POSTselect('day', "2").' >Tuesday</option>
					<option value="3" '.POSTselect('day', "3").' >Wednesday</option>
					<option value="4" '.POSTselect('day', "4").' >Thursday</option>
					<option value="5" '.POSTselect('day', "5").' >Friday</option>
					<option value="6" '.POSTselect('day', "6").' >Saturday</option>
					<option value="7" '.POSTselect('day', "7").' >Sunday</option>
				</select>
			</div>
		</div>
		<div class="row" id="backupdate_tr" style="display:none;">
			<div class="col-sm-6">
				<label class="control-label">'.$l['backup_date'].'</label><br />
				<span class="help-block">'.$l['exp_backup_date'].'</span>
			</div>
			<div class="col-sm-6">
				<select name="date" class="form-control">';
				for($i=1;$i<32;$i++){
					echo '<option value="'.$i.'" '.POSTselect('date', $i).' >'.$i.'</option>';
				}
			echo '</select>
			</div>
		</div>
		<div class="row" id="tr_time">
			<div class="col-sm-6">
				<label class="control-label">'.$l['backup_time'].'</label><br />
				<span class="help-block">'.$l['exp_backup_time'].'</span>
			</div>
			<div class="col-sm-6">
				<select class="form-control" name="hrs" style="width:37%;float:left">';
				for($i=0;$i<24;$i++){
					echo '<option value="'.($i < 10 ? '0'.$i : $i).'" '.POSTselect('hrs', $i).' >'.($i < 10 ? '0'.$i : $i).'</option>';
				}
			echo '</select>
				<label class="side_lbl">'.$l['hrs'].'</label>
				<select class="form-control" name="min" style="width:37%;float:left">';
					for($i=0;$i<60;$i++){
						echo '<option value="'.($i < 10 ? '0'.$i : $i).'" '.POSTselect('min', $i).' >'.($i < 10 ? '0'.$i : $i).'</option>';
					}
			echo '</select><label class="side_lbl">'.$l['min'].'</label>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<label class="control-label">'.$l['backup_rotation'].'</label><br />
				<span class="help-block">'.$l['exp_backup_rotation'].'</span>
			</div>
			<div class="col-sm-6">
				<select class="form-control" name="rotation">';
				for($i = 1; $i < 11; $i++){
					echo '<option value="'.$i.'" '.POSTselect('rotation', $i).' >'.$i.'</option>';
				}
				
				echo '<option value="0" '.POSTselect('rotation', 0).' >'.$l['unlimited'].'</option>';
				
			echo '</select>
			</div>
		</div>
		<div class="row" id="tr_backup_limit">
			<div class="col-sm-6">
				<label class="control-label">'.$l['backup_limit'].'</label><br />
				<span class="help-block">'.$l['exp_backup_limit'].'</span>
			</div>
			<div class="col-sm-6">
				<select class="form-control" name="backup_limit" id="backup_limit">';
				
				echo '<option value="0" '.POSTselect('backup_limit', 0).' >'.$l['disabled'].'</option>';
				
				for($i = 1; $i < 11; $i++){
					echo '<option value="'.$i.'" '.POSTselect('backup_limit', $i).' >'.$i.'</option>';
				}
				
				echo '<option value="-1" '.POSTselect('backup_limit', -1).' >'.$l['unlimited'].'</option>';
				
			echo '</select>
			</div>
		</div>
		<div class="row" id="tr_restore_limit">
			<div class="col-sm-6">
				<label class="control-label">'.$l['restore_limit'].'</label><br />
				<span class="help-block">'.$l['exp_restore_limit'].'</span>
			</div>
			<div class="col-sm-6">
				<select class="form-control" name="restore_limit" id="restore_limit">';
				
				echo '<option value="0" '.POSTselect('restore_limit', 0).' >'.$l['disabled'].'</option>';
				
				for($i = 1; $i < 11; $i++){
					echo '<option value="'.$i.'" '.POSTselect('restore_limit', $i).' >'.$i.'</option>';
				}
				
				echo '<option value="-1" '.POSTselect('restore_limit', -1).' >'.$l['unlimited'].'</option>';
				
			echo '</select>
			</div>
		</div>
		<!-- <div class="row">
			<div class="col-sm-6">
				<label class="control-label">'.$l['enable_enduser_backup_servers'].'</label><br />
				<span class="help-block">'.$l['exp_enable_enduser_backup_servers'].'</span>
			</div>
			<div class="col-sm-6">
				<input type="checkbox" id="enable_enduser_backup_servers" class="ios" name="enable_enduser_backup_servers" '.POSTchecked('enable_enduser_backup_servers').'/>
			</div>
		</div> -->
		<div class="row">
			<div class="col-sm-6">
				<label class="control-label">'.$l['nice'].'</label><br />
				<span class="help-block">'.$l['nice_exp'].'</span>
			</div>
			<div class="col-sm-6">
				<select class="form-control" name="nice" id="nice">';
				for($i=-20; $i<20; $i++){
					echo '<option value="'.$i.'" '.POSTselect('nice', $i).'>'.$i.'</option>';
				}
			echo '</select>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<label class="control-label">'.$l['ionice_prio'].'</label><br />
				<span class="help-block">'.$l['ionice_prio_exp'].'</span>
			</div>
			<div class="col-sm-6">
				<select class="form-control" name="ionice_prio" id="ionice_prio">';
				for($i=0; $i<8; $i++){
					echo '<option value="'.$i.'" '.POSTselect('ionice_prio', $i).'>'.$i.'</option>';
				}
		echo '</select>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<label class="control-label">'.$l['ionice_class'].'</label><br />
				<span class="help-block">'.$l['ionice_class_exp'].'</span>
			</div>
			<div class="col-sm-6">
				<select class="form-control" name="ionice_class" id="ionice_class">
					<option value="1" '.POSTselect('ionice_class', 1).' >Real Time</option>
					<option value="2" '.POSTselect('ionice_class', 2).'>Best Effort</option>
					<option value="3" '.POSTselect('ionice_class', 3).'>Idle</option>
				</select>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<label class="control-label">'.$l['io_limit'].'</label><br />
				<span class="help-block">'.$l['exp_io_limit'].'</span>
			</div>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="io_limit" value="'.POSTval('io_limit', '0').'" />
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<label class="control-label">'.$l['backup_compression'].'</label><br />
				<span class="help-block">'.$l['exp_backup_compression'].'</span>
			</div>
			<div class="col-sm-6">
				<input type="checkbox" id="compression" class="ios" name="compression" '.POSTchecked('compression').'/>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<label class="control-label">'.$l['check_dir_permissions'].'</label><br />
				<span class="help-block">'.$l['exp_check_dir_permissions'].'</span>
			</div>
			<div class="col-sm-6">
				<input type="checkbox" id="check_dir_permissions" class="ios" name="check_dir_permissions" '.POSTchecked('check_dir_permissions', true).'/>
			</div>
		</div>
		
		<div class="roundheader hide_for_plan" onclick="toggle_advoptions(\'adv\');" style="cursor:pointer;"><label id="adv_advoptions_ind"  style="width:10px;">+</label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$l['addvoption'].'</div>
		<div id="adv_advoptions" style="display:none; width:100%" class="hide_for_plan">
			<div class="col-sm-12 bgaddv">
				<div class="row">
					<div class="col-sm-12">
						<label class="control-label">'.$l['backup_server_per_region'].'</label>
						<span class="help-block">'.$l['backup_server_per_region_exp'].'</span>
					</div>
					<div class="col-sm-12">
						<div class="scrollbar-virt" style="max-height: 50px; height: 50px;">
							<table class="table table-bordered">
								<tr class="active">
									<th>'.$l['server_group_header'].'</th>
									<th>'.$l['sel_backupserver'].'</th>
									<th>'.$l['local_dir'].'</th>
								</tr>';
								
								foreach($servergroups as $sgid => $servergroup) {
									
									echo '<tr>
										<th style="vertical-align:middle">[Group] '.$servergroup['sg_name'].'</th>
										<th><select class="virt-select" name="bs_sg'.$sgid.'" style="width:100%">
											<option value="0" '.(POSTselect('bs_sg'.$sgid, 0)).'>'.$l['default'].'</option>
											<option value="-1" '.(POSTselect('bs_sg'.$sgid, -1)).'>LOCAL</option>
											<optgroup label="SSH">';
											
												foreach($backupservers as $bid => $bs) {
													if($bs['type'] == 'SSH') {
														echo '<option value="'.$bid.'" '.(POSTselect('bs_sg'.$sgid, $bid)).'>'.$bs['name'].'</option>';
													}
												}
										
											echo '</optgroup>
											<optgroup label="FTP">';
											
												foreach($backupservers as $bid => $bs) {
													if($bs['type'] == 'FTP') {
														echo '<option value="'.$bid.'" '.(POSTselect('bs_sg'.$sgid, $bid)).'>'.$bs['name'].'</option>';
													}
												}
										
											echo '</optgroup>
										</select></th>
										<th><input type="text" class="form-control" name="dir_sg'.$sgid.'" value="'.POSTval('dir_sg'.$sgid, '').'" placeholder="'.$l['default_dir'].'" size="40"/></th>
									</tr>';
									
									foreach($servers as $serid => $server) {
										
										if($server['sgid'] != $sgid) {
											continue;
										}
										
										echo '<tr>
											<td style="vertical-align:middle"> - '.$server['server_name'].' ('.$server['ip'].')</td>
											<th><select class="virt-select" name="bs_s'.$serid.'" style="width:100%">
												<option value="0" '.(POSTselect('bs_s'.$serid, 0)).'>'.$l['default'].'</option>
												<option value="-1" '.(POSTselect('bs_s'.$serid, -1)).'>LOCAL</option>
												<optgroup label="SSH">';
												
													foreach($backupservers as $bid => $bs) {
														if($bs['type'] == 'SSH') {
															echo '<option value="'.$bid.'" '.(POSTselect('bs_s'.$serid, $bid)).'>'.$bs['name'].'</option>';
														}
													}
											
												echo '</optgroup>
												<optgroup label="FTP">';
												
													foreach($backupservers as $bid => $bs) {
														if($bs['type'] == 'FTP') {
															echo '<option value="'.$bid.'" '.(POSTselect('bs_s'.$serid, $bid)).'>'.$bs['name'].'</option>';
														}
													}
											
												echo '</optgroup>
											</select></th>
											<th><input type="text" class="form-control" name="dir_s'.$serid.'" value="'.POSTval('dir_s'.$serid, '').'"  placeholder="'.$l['default_dir'].'" size="40"/></th>
										</tr>';
										
									}
									
								}
								
								echo '</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<br /><br /><br />
		<center><input type="submit" name="addbackup_plan"  id="addbackup_plan" value="'.$l['submit'].'" class="btn"/></center>

		<br /><br />
	</form>
	</div>';

echo '</div>';
softfooter();

}

?>