<?php

//////////////////////////////////////////////////////////////
//===========================================================
// vs_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}


function getSortHeader($header_name, $sort_column, $sort_column_by, $column){

global $theme, $globals;
	
	$str = '<table style="margin:auto;">
<tr>
	<th rowspan="2" class="tablehead" style="background: none;border: none;color:black;padding-right:6px;">'.$header_name.'</th>
	<td width="10"><a href="'.$globals['index'].'act=vs&sortcolumn='.$column.'&sortby=asc"><img src="'.$theme['images'].'admin/arrow_up.png"></a></td>
</tr>
<tr>
	<td><a href="'.$globals['index'].'act=vs&sortcolumn='.$column.'&sortby=desc"><img src="'.$theme['images'].'admin/arrow_down.png"></a></td>
</tr>
</table>';
	
	return $str;
}

function __make_li($carr, $item){
	return $carr .= '<li>'.$item.'</li>';
}


function vs_theme(){

global $theme, $globals, $kernel, $cluster, $user, $oslist, $ostemplates, $distros, $l, $error, $vs, $servers, $done, $SESS, $servergroups, $plans, $backup_plans;
global $flag_deletevps, $actid, $vsop, $vpsstatus, $owners, $sort_column, $sort_column_by,  $flag_band_reset, $error_msg;

if($flag_deletevps){
	echo $actid;
	$flag_deletevps = 0;
	return false;
}

// Is the rescue mode error set ? Are we in ajax mode ? 
if(!empty($error_msg) && optGET('ajaxprogress') == 1){
	echo json_encode(array('error' => $error_msg));
	return false;
}

if(!empty($vsop)){
	
	if(!empty($_GET['apikey'])){
		return $vsop['status'][$vsop['id']];	
	}

	if($vsop['action'] == 'start'){
		
		if($vsop['status'][$vsop['id']] != 0){
			echo 'custom_valert("'.$l['started'].'"); changevpsstatus('.$vsop['id'].', '.$vsop['serid'].', '.$vsop['status'][$vsop['id']].', '.(!empty($vs[$vsop['id']]['nw_suspended']) ? 1 : -1).');';
		}else{
			echo 'custom_valert("<div>'.$l['start_failed'].'</div><div style=\'overflow-y:scroll;margin-top:10px;max-height:200px;background-color:#FAFBD9;\'><p>'.preg_replace("/[\n\"]/",'',nl2br($vsop['output'])).'</p></div>","Error");';
		}
		
	}elseif($vsop['action'] == 'stop'){		
	
		if($vsop['status'][$vsop['id']] != 1){
			echo 'custom_valert("'.$l['stopped'].'"); changevpsstatus('.$vsop['id'].', '.$vsop['serid'].', '.$vsop['status'][$vsop['id']].', '.(!empty($vs[$vsop['id']]['nw_suspended']) ? 1 : -1).');';
		}else{
			echo 'custom_valert("'.$l['stop_sent'].'");';
		}		
		
	}elseif($vsop['action'] == 'poweroff'){		
	
		if($vsop['status'][$vsop['id']] != 1){
			echo 'custom_valert("'.$l['poweroff'].'"); changevpsstatus('.$vsop['id'].', '.$vsop['serid'].', '.$vsop['status'][$vsop['id']].', '.(!empty($vs[$vsop['id']]['nw_suspended']) ? 1 : -1).');';
		}else{
			echo 'custom_valert("'.$l['poweroff'].'");';
		}	
		
	}elseif($vsop['action'] == 'restart'){
	
		echo 'custom_valert("'.$l['restart'].'");';
		
	}
	
	echo 'false;';
	echo 'getvpsstatus();';
	
	return true;
	
}

// Check the status of Virtual Servers
if(optGET('vs_status')){
	
	if(empty($GLOBALS['status'])){
		echo 'false;';
	}
	
	foreach($GLOBALS['status'] as $k => $v){
		
		// As the status is empty network is unsuspended
		$ntw_status = (empty($GLOBALS['network_status'][$k])) ? 0 : 1; 
		
		$_used_cpu = empty($v['used_cpu']) ? 0 : $v['used_cpu'];
		$_used_disk = empty($v['used_disk']) ? 0 : $v['used_disk'];
		$_disk = empty($v['disk']) ? 0 : $v['disk'];
		$_used_bandwidth = empty($v['used_bandwidth']) ? 0 : $v['used_bandwidth'];
		$_bandwidth = empty($v['bandwidth']) ? 0 : $v['bandwidth'];
		$_used_ram = empty($v['used_ram']) ? 0 : $v['used_ram'];
		$_ram = empty($v['ram']) ? 0 : $v['ram'];
		$_status = empty($v['status']) ? 0 : $v['status'];
		$_virt = empty($v['virt']) ? 0 : $v['virt'];
		
		// changevpsstatus('vpsid', 'serid', 'status', 'ntw_status', [[0] => ['used_cpu', 'cpu_free'], [1] => ['used_ram','ram_free'], [2] => ['used_disk', 'disk_free'], [3] => [used_bandwidth, bandwidth_free], [4] => 'virt'])
		// 0.01 for bandwidth parameter to show unlimited value 
		echo 'changevpsstatus('.$k.', '.$GLOBALS['vs_serid'][$k].', '.$_status.', '.$ntw_status.', [['.$_used_cpu.', '.(100 - $_used_cpu).'], ['.$_used_ram.', '.($_ram - $_used_ram).'], ['.$_used_disk.', '.($_disk - $_used_disk).'], ['.$_used_bandwidth.', '.(!empty($_bandwidth) ? $_bandwidth - $_used_bandwidth : 9999999).'], "'.$_virt.'"]);';
	}
	
	// Change vps status is called after document.ready so we have to call function to display the sparkline
	echo 'render_sparkline()';
	
	return true;
}

softheader($l['<title>']);


echo '<script language="javascript" type="text/javascript"><!-- // --><![CDATA[

// Action related JS
var vps_list = new Array();
var done_vps_list = new Array();
var removed = re = new Object();
var multi_action = "";
var nextvpsid = "";
var vpsid = "";
var delvpsid = 0;
var task_lang = new Array();
task_lang["delete"] = "'.$l['multi_delete'].'";
task_lang["suspend"] = "'.$l['multi_suspend'].'";
task_lang["unsuspend"] = "'.$l['multi_unsuspend'].'";
task_lang["suspend_net"] = "'.$l['multi_suspend_net'].'";
task_lang["unsuspend_net"] = "'.$l['multi_unsuspend_net'].'";
task_lang["poweroff"] = "'.$l['multi_poweroff'].'";
task_lang["vs_netrestrict"] = "'.$l['multi_vs_netrestrict'].'";
task_lang["start"] = "'.$l['multi_start'].'";
task_lang["stop"] = "'.$l['multi_stop'].'";
task_lang["restart"] = "'.$l['multi_restart'].'";

function ucfirst(str) {
  str += "";
  var f = str.charAt(0)
    .toUpperCase();
  return f + str.substr(1);
}

function show_confirm(vpsid, cur_action){
	
	vpsid = vpsid || 0;
	cur_action = cur_action || "";
	vps_list = new Array();
	done_vps_list = new Array();
	removed = new Object();
	var selected_list = "";

	// Single VPS select
	if(vpsid > 0){
		
		vps_list[0] = vpsid;
		multi_action = cur_action;
		selected_list += "\n'.$l['vpsid_list'].'"+vpsid+" ["+$("#tr_hostname"+vpsid).text()+"]"+" '.$l['of']." ".$l['vserid'].' "+$("#tr_server"+vpsid).text();
		
	// Multi select
	}else{
	
		multi_action = $_("multi_options").value;
		if(multi_action == "0"){
			return false;
		}

		// Build the list of Installations to remove
		var field = document.getElementsByName(\'vps_list[]\');
		for(i = 0; i < field.length; i++){
			if(field[i].checked){
				vps_list[vps_list.length] = field[i].value;
				selected_list += "\n'.$l['vpsid_list'].'"+field[i].value+" ["+$("#tr_hostname"+field[i].value).text()+"]"+" '.$l['of']." ".$l['vserid'].' "+$("#tr_server"+field[i].value).text();
			}
		}
		
	}
	
	if(vps_list.length == 0){
		alert("'.$l['no_sel_vps'].'");
		return false;
	}
	var confirm_net = "";
	if(multi_action == "suspend_net"){	
		confirm_net = "'.$l["ms_suspend_net"].'"+" '.$l['of'].'";
	}else if(multi_action == "unsuspend_net"){	
		confirm_net = "'.$l["ms_unsuspend_net"].'"+" '.$l['of'].'";
	}else if(multi_action == "vs_netrestrict"){	
		confirm_net = "'.$l["ms_vs_netrestrict"].'"+" '.$l['of'].'";
	}
	var confirm_message = "";
	confirm_message = "'.$l['cnf_action'].'" + ((!empty(confirm_net))? confirm_net : ucfirst(multi_action))+"'.$l['cnf_action_1'].'"+selected_list;
	var r = confirm(confirm_message);
	if(r != true){
		return false;
	}
	
	if(r == true && multi_action == "delete"){
		var y = confirm("'.$l['conf_del'].'"+selected_list+"\n'.$l['final_conf'].'");
		
		// He confirmed it last time
		if(y != true){
			return false;
		}
	}
	
	action_by_id(vps_list[0], "", 0);
	
	return false; // So that A hrefs are taken care of
	
};

function IsJsonString(str) {
    try {
        $.parseJSON(str);
    } catch (e) {
        return false;
    }
    return true;
}

function action_by_id(vpsid, re){
	
	vpsid = vpsid || 0;
	re = re || "";
	
	if(vpsid > 0){
		try{
			re = $.parseJSON(re);
			if("error" in re && (typeof re["error"] != "undefined"))
				alert(re["error"]+ " " + vpsid)
		}catch(e){
			//Do not do anything!
		}
	}
	
	//alert(multi_action +" -- "+ re)

	nextvpsid = 0;
	
	// Find the next INSTALLATION to remove
	for(i = 0; i < vps_list.length; i++){
		nextvpsid = vps_list[i];
		//alert("Val of I : "+ i)
		vps_list.splice(i, 1);
		break;
	}
	
	//alert(nextvpsid +" -- "+ vps_list+" -- "+ vps_list.length);
	//alert(" Deleted list "+done_vps_list);
	
	// All tasks done
	if(nextvpsid == 0){
		
		// If an action is to Delete, We have to remove the <TR> as well.
		if(multi_action == "delete"){
			for(x in done_vps_list){
				//alert(x +" --done_vps_list-- "+done_vps_list[x]);
				$("#trid_"+done_vps_list[x]).html("");
				$("#trid_"+done_vps_list[x]).remove();
			}
		
			$("#progress-cont").hide();
		}
		
		// If the action is to suspend or unsuspend, we have to update the status to display.
		if(multi_action == "suspend" || multi_action == "unsuspend" || multi_action == "start" || multi_action == "stop" || multi_action == "poweroff" || multi_action == "restart" || multi_action == "suspend_net" || multi_action == "unsuspend_net" || multi_action == "vs_netrestrict" ){
			$_("rem_div").innerHTML = "";
			getvpsstatus();
		}
		
		return true;
	}
	
	///////////////////////
	// Lets do the actions
	///////////////////////
	
	url = "'.$globals['index'].'act=vs&"+multi_action+"="+nextvpsid+"&ajaxprogress=1&random="+Math.random();
	//alert(vpsid +" - "+nextvpsid+" -- "+url);
	
	// Delete Action
	if(multi_action == "delete"){
		
		// Set some variables
		actid = null;
		progress = 0;
		
		$.ajax({type: "POST",
			url: url,
			data:null,
			success:function(response){
				if(!isNaN(response)){
					actid = response;
					delvpsid = nextvpsid;
					timer = setTimeout("get_progress(\'vs\')", 1000);
				}else{
					//$("#softcontent").html(response);
				}
				
				response = JSON.parse(response);
				if(response.error != undefined){
					custom_valert(response.error, "Error");
					$("#progress-cont").hide();
				}
			}
		});
		
		$("#progress-cont").show();
		$("#pbar").html("'.$l['prog_begin'].'" + "(0%) ");
		$("#pbar_id").html(task_lang[multi_action] + " " + nextvpsid + " (" + $(\'#tr_hostname\'+nextvpsid).attr(\'val\') + ")");
		$("#progressbar").progressbar({value: 0});
	}
	
	// For suspend and unsuspend we dont get any response so we have to make another call.
	if(multi_action != "delete"){
		try{
			if(multi_action == "start" || multi_action == "stop" || multi_action == "poweroff" || multi_action == "restart" || multi_action == "vs_netrestrict"){				
				AJAX("'.$globals['index'].'act=vs&action="+multi_action+"&vpsid="+nextvpsid+"&api=json&random="+Math.random(), "multi_vpsboothandle(re)");
			}else{
				AJAX("'.$globals['index'].'act=vs&"+multi_action+"="+nextvpsid+"&api=json&random="+Math.random(), "action_by_id(\'"+nextvpsid+"\', re)");
			}
			
			$_("rem_div").innerHTML = "<br /><br /><p align=\"center\"><img src=\"'.$theme['images'].'ajax_remove.gif\"> <br />" + task_lang[multi_action] + " " + nextvpsid + " (" + $(\'#tr_hostname\'+nextvpsid).attr(\'val\') + ") <br /></p>";
			//return true;
		}catch(e){
			return false;
		}
	}
	
};

// Delete Prgoress Bar
var actid = null;
var progress = 0;
var progress_update = "";
var timer = null;
var oldProgress = 0;
var animateover = true;
var url = "";

// ]]></script>';

echo '<div class="bg" style="padding:5px;margin:1px auto;">
<center class="tit">
<i class="icon icon-vs icon-head"></i>&nbsp; '.$l['page_head'].'<span style="float:right"><a href="javascript:showsearch();"><img src="'.$theme['images'].'admin/search.gif" /></a></span></center>';
if(optGET('done')){
	$actid = (int) optGET('actid');
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.(optGET('suspend') ? $l['done_sus'] : (optGET('unsuspend') ? $l['done_unsus'] : $l['done'])).'</div>';
}

error_handle($error);

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.(optGET('suspend') ? $l['done_sus'] : (optGET('unsuspend') ? $l['done_unsus'] : $l['done'])).'</div>';
}

if(!empty($flag_band_reset)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done_bwreset'].'</div>';
}

$vnc_vm = array();
foreach($vs as $vpsid => $vpsdata){
	if(!empty($vpsdata['vnc'])){
		$vnc_vm[] = $vpsid;
	}
}
echo '<script language="javascript" type="text/javascript"><!-- // --><![CDATA[
																			   
var menu_vpsid, menu_serverid, menu_content;
var vnc_vm = ['.implode(',', $vnc_vm).'];


function conf_del(ele){
	var vpsid = $(ele).attr("vpsid");
	return show_confirm(vpsid, "delete");
};

function conf_band_reset(res){
	return confirm("'.$l['conf_bwreset'].'");
}

function conf_sus(vpsid){
	return show_confirm(vpsid, "suspend");
};

function conf_unsus(vpsid){
	return show_confirm(vpsid, "unsuspend");
};

// suspending network
function conf_susnetwork(vpsid){
	return show_confirm(vpsid, "suspend_net");
};

function conf_unsusnetwork(vpsid){
	return show_confirm(vpsid, "unsuspend_net");
};

function toggle_sort_img(){
	var sort_column_by = $("#sort_column_by");
	if(sort_column_by.is(":checked")){
		sort_column_by.parent().find("img").attr("src","'.$theme['images'].'admin/sort_column_by_desc.png");
	}else{
		sort_column_by.parent().find("img").attr("src","'.$theme['images'].'admin/sort_column_by_asc.png");
	}
}

function showmanagevpsmenu(serid, vpsid, status, ntw_status){
	menu_vpsid = vpsid;
	menu_serverid = serid;
	var position = $("#action_"+vpsid).children("img").position();
	
	$_("managevpsmenu").style.left = position.left-90+\'px\';
	$_("managevpsmenu").style.top = position.top-3+\'px\';
	$_("managevpsmenu").style.display = "block";
	$("#managevpsmenu").html(\'<div class="vpsmanagemenubg" id="managevps_\'+vpsid+\'">\'+((status == "0") ? \'<a href="#"><img src="'.$theme['images'].'blank_page.gif" alt="pause network"/></a>\' : ((status == "1") ? \'<a href="'.$globals['ind'].'act=vs&suspend=\'+vpsid+\'" onclick="return conf_sus(\'+vpsid+\');" title="'.$l['suspend_vs'].'"><img src="'.$theme['images'].'admin/suspend.png" height="16" width="16"/></a>\' : \' <a href="'.$globals['ind'].'act=vs&unsuspend=\'+vpsid+\'" onclick="return conf_unsus(\'+vpsid+\');" title="'.$l['unsuspend_vs'].'"><img src="'.$theme['images'].'admin/unsuspend.png" height="16" width="16"/></a>\'))+ \'<span>&nbsp;</span>\'+((status == "0" || status == "2") ? \'<a href="#"><img  src="'.$theme['images'].'blank_page.gif" alt="pause network" height="16" width="16"/></a>\' : ((ntw_status == 0) ? \'<a href="javascript:void(0);" onclick="return conf_susnetwork(\'+vpsid+\');" title="'.$l['suspend_ntw'].'"><img src="'.$theme['images'].'admin/disconnect.png" height="16" width="16"/></a>\' : \'<a href="javascript:void(0);" onclick="return conf_unsusnetwork(\'+vpsid+\');" title="'.$l['unsuspend_ntw'].'"><img src="'.$theme['images'].'admin/connect.png" height="16" width="16"/></a>\'))+\'<span>&nbsp;</span><a vpsid="\'+vpsid+\'" href="'.$globals['ind'].'act=vs&delete=\'+vpsid+\'" onclick="return conf_del(this);"><img src="'.$theme['images'].'admin/delete.png" height="16" width="16"/></a></div>\');
}

function hidemanagevpsmenu(){

	timerId = null;
		
	if($("#managevpsmenu").is(":hover")){	
		$(\'#managevpsmenu\').on(\'mouseenter\', this, function () {
			$(this).css("display", "block");
		}).on(\'mouseleave\', this, function () {
			setTimeout("hidemanagemenu()", 1000);
		});
	}else{
		setTimeout("hidemanagemenu()", 3000);
	}	
}

function hidemanagemenu(){
	if($("#managevpsmenu").is(":hover")){
		hidemanagevpsmenu();
	}else{
		$("#managevpsmenu").css("display", "none");
	}	
}

function showvpsmenu(serid, vpsid){
	try{
		clearTimeout(menuhider);
		if(menu_content.length > 0){
			$_("vpsmenubg").innerHTML = menu_content;
		}
	}catch(e){}
	menu_vpsid = vpsid;
	menu_serverid = serid;
	if(vnc_vm.indexOf(menu_vpsid) > -1){
		$("#novncURL.vncButton, #java_vnc.vncButton").show();
	}else{
		$("#novncURL.vncButton, #java_vnc.vncButton").hide();
	}
	//var position = findelpos($_("stat_"+vpsid));
	var position = $("#stat_"+vpsid).position();
	
	//$_("vpsmenu").style.left = position[0]+30+\'px\';
	//$_("vpsmenu").style.top = position[1]+1+\'px\';
	
	$("#vpsmenu").css("left",  (position.left +30)+"px");
	$("#vpsmenu").css("top",  (position.top + 20)+"px");
	$_("vpsmenu").style.display = "block";
};
	
function hidemenu(){
	menuhider = setTimeout("hidevpsmenu()", 500);
};

function hidevpsmenu(){
	$_("vpsmenu").style.display = "none";
};

function vpsactions(todo){
	
	// Change to loading
	menu_content = $_("vpsmenubg").innerHTML;
	$_("vpsmenubg").innerHTML = \'<img title="'.$l['vpsloading'].'" src="'.$theme['images'].'admin/vpsloading.gif" style="margin-top:6px;" />\';
	
	if(AJAX("'.$globals['index'].'act=vs&action="+todo+"&serid="+menu_serverid+"&vpsid="+menu_vpsid+"", "vpsboothandle(re)")){
		return false;
	}else{
		return true;	
	}
};

function changevpsstatus(id, serid, status, ntw_status, stats){
	
	stats = stats || "";
	
	$("#action_edit_"+id).show();
	/*$("#action_del_"+id).show(); */
		
	$_("action_"+id).innerHTML = \'<img title="'.$l['manage_vps'].'" src="'.$theme['images'].'admin/manage.png" width="20" onmouseover="showmanagevpsmenu(\'+serid+\', \'+id+\', \'+status+\', \'+ntw_status+\')" onmouseout="hidemanagevpsmenu()" />&nbsp;&nbsp;<a href="'.$globals['ind'].'act=editvs&vpsid=\'+id+\'" ><img src="'.$theme['images'].'admin/edit.png" /></a>&nbsp;&nbsp;<a href="'.$globals['ind'].'act=managevps&vpsid=\'+id+\'" id="action_vpsmanage_\'+id+\'"><img title="'.$l['manage_vps'].'" src="'.$theme['images'].'admin/managevps.png" width="18" /></a>\';
	
	// Appending the icon to show network is suspended
	if(ntw_status == 1 && ($(\'#ntw_suspended\'+id).length == 0)){
		$(\'#tr_hostname\'+id).append(\'<img style="float:right;margin:5px;" title="'.$l['status_ntwsuspended'].'" width="15" id="ntw_suspended\'+id+\'" src="'.$theme['images'].'admin/disconnect.png" >\');
		custom_valert("'.$l['status_ntwsuspended'].'");
				
	}else if(ntw_status == 0 && ($(\'#ntw_suspended\'+id).length == 1)){
		$(\'#ntw_suspended\'+id).remove();	
		custom_valert("'.$l['status_ntwunsuspended'].'");
	}
	
	if(status == 0){
		$_("stat_"+id).innerHTML = \'<img title="'.$l['vps_id_stat_off'].'" src="'.$theme['images'].'offline.png" onmouseover="showvpsmenu(\'+serid+\', \'+id+\')" onmouseout="hidemenu()" />\';
	
	}else if(status == 1){
		$_("stat_"+id).innerHTML = \'<img title="'.$l['vps_id_stat_on'].'" src="'.$theme['images'].'online.png" onmouseover="showvpsmenu(\'+serid+\', \'+id+\')" onmouseout="hidemenu()" />\';
	
	}else if(status == 2){
		$_("stat_"+id).innerHTML = \'<img title="'.$l['vps_id_stat_off'].'" src="'.$theme['images'].'suspended.png" onmouseover="showvpsmenu(\'+serid+\', \'+id+\')" onmouseout="hidemenu()" />\';
	
	}

	// stats = [[0] => [actual_cpu, cpu_free], [1] => [ram_used, ram_free], [2] => [disk_used, disk_free], [3] => [used_bandwidth, bandwidth_free], [4] => virt]
	// If the ram or cpu is empty dont any action
	if(stats != ""){
		
		var ram_div = "";
		// We have to show the RAM Info only for OpenVZ and LXC
		if(stats[4] == "openvz" || stats[4] == "lxc" || stats[4] == "vzo" || stats[4] == "vzk"){
			ram_div = stats[1][0]+","+stats[1][1];
		}else{
			ram_div = "";
		}
		
		$("#bw_stats_"+id).html(stats[3][0]+","+stats[3][1]);
		$("#cpu_stats_"+id).html(stats[0][0]+","+stats[0][1]);
		$("#disk_stats_"+id).html(stats[2][0]+","+stats[2][1]);
		$("#ram_stats_"+id).html(ram_div);
	}
	
};

function custom_valert(output_msg, title_msg)
{
	if (!title_msg)
		title_msg = "Success!";

	if (!output_msg)
		output_msg = "No Message to Display.";

	$("<div></div>").html(output_msg).dialog({
		title: title_msg,
		resizable: true,
		buttons: {
            "Close": function() 
            {
                $(this).dialog( "close");
            }
        },
		modal: true,
		width: 350,
		maxHeight: 250
	});
}

function vpsboothandle(re){
	if(re.length > 0){
		try{
			if(menu_content.length > 0){
				$_("vpsmenubg").innerHTML = menu_content;
			}
			eval(re);
		}catch(e){ }
	}
	
};

function multi_vpsboothandle(re){
	action_by_id(re);	
};

function launchvnc(type){
	if(type === 1){
		var thisURL = window.location.href;
		thisURL = thisURL.toString();
		thisURL = thisURL.replace("http:", "https:");
		thisURL = thisURL.replace(":4084", ":4085");
		window.open("'.$globals['index'].'act=vnc&novnc="+menu_vpsid, "_blank", "height=400,width=720");
	}else if(type === 2){
		AJAX("'.$globals['index'].'act=vnc&ajax="+menu_vpsid, "vnchandle(re)");
	}
};

function vnchandle(resp){
	$("#vnc_launcher").html(resp);
}
									
function render_sparkline(){
	// CPU Sparklines
	$(".spcpu").sparkline("html", {
								type:"pie",
								sliceColors: ["#434348", "#f7a35c"],
								tooltipClassname: "spPieToolTip jqstooltip",
								tooltipFormatter: function(a, b, fields){
									return "<div style=\'padding:2px;\'>'.$l['cpu'].'&nbsp;"+fields["value"]+" % "+(fields["offset"] == 0 ? "'.$l['used'].'" : "'.$l['free'].'")+"</div>";
								}
							}
						);

	// RAM Sparklines
	$(".spram").sparkline("html", {
								type:"pie",
								sliceColors: ["#434348", "#90ed7d"],
								tooltipClassname: "spPieToolTip jqstooltip",
								tooltipFormatter: function(a, b, fields){
									return "<div style=\'padding:2px;\'>'.$l['ram'].'&nbsp;"+fields["value"]+" MB "+(fields["offset"] == 0 ? "'.$l['used'].'" : "'.$l['free'].'")+" ("+fields["percent"].toFixed(2)+"%)</div>";
								}
							}
						);

	// Disk Sparklines
	$(".spdisk").sparkline("html", {
								type:"pie",
								sliceColors: ["#434348", "#d90955"],
								tooltipClassname: "spPieToolTip jqstooltip",
								tooltipFormatter: function(a, b, fields){
									return "<div style=\'padding:2px;\'>'.$l['disk'].'&nbsp;"+fields["value"].toFixed(2)+" GB "+(fields["offset"] == 0 ? "'.$l['used'].'" : "'.$l['free'].'")+" ("+fields["percent"].toFixed(2)+"%)</div>";
								}
							}
						);
	
	// Bandwidth Sparklines
	$(".spband").sparkline("html", {
								type:"pie",
								sliceColors: ["#434348", "#7cb5ec"],
								tooltipClassname: "spPieToolTip jqstooltip",
								tooltipFormatter: function(a, b, fields){
									return "<div style=\'padding:2px;\'>'.$l['lv_bandwidth'].'&nbsp;"+(fields["offset"] == 1 ? (fields["value"] == "9999999") ? a.values[0]+"/'.$l['unlimited'].'"+" GB"  : fields["value"]+" GB '.$l['free'].'" : fields["value"]+" GB '.$l['used'].'")+"("+((fields["offset"] == 1 && fields["value"] == "9999999") ? "0.01" : fields["percent"].toFixed(2))+" %)</div>";
								}
							}
						);

	$(".spcpu, .spram, .spdisk, .spband").bind("sparklineClick", function(ev) {
		if($(this).is(":hover")){
			return false;			
		}
		if(window.sparkClickClrTimer){
			 clearTimeout(window.sparkClickClrTimer);
		}	
		var elementPosition = $(ev.target).position();
		$(".jqstooltip").css({"left":(elementPosition.left+80)+\'px\'});
		$(".jqstooltip").css({"visibility": "visible"});
		window.sparkClickClrTimer = setTimeout(function(){ $(".jqstooltip").css({"visibility": "hidden"}); }, 3000);
	});						
}

$(document).ready(function(){

	// Server Filter Multiple Select
	$("#server-option").on("change", function() {
		var serverValues = [];
		serverValues.push($("#server-option").val());
		$("#vserid").val(serverValues.toString());
		if($("#vserid").val() == ""){
			$("#vserid").val(-1);
		}
	});

	// Server Group Filter Multiple Select
	$("#group-option").on("change", function() {
		var sgroupValues = [];
		sgroupValues.push($("#group-option").val());
		$("#vsgid").val(sgroupValues.toString());
		if($("#vsgid").val() == ""){
			$("#vsgid").val(-1);
		}
	});

	// Plan Filter Multiple Select
	$("#plan-option").on("change", function() {
		var planValues = [];
		planValues.push($("#plan-option").val());
		$("#plid").val(planValues.toString());
		if($("#plid").val() == ""){
			$("#plid").val(-1);
		}
	});

	// Backup Plan Filter Multiple Select
	$("#bplan-option").on("change", function() {
		var bplanValues = [];
		bplanValues.push($("#bplan-option").val());
		$("#bpid").val(bplanValues.toString());
		if($("#bpid").val() == ""){
			$("#bpid").val(-1);
		}
	});

});

// ]]></script>
<div id="vnc_launcher" style="position:absolute;width:0px;left:-100%"></div>
<div id="vpsmenu" class="vpsmenu" onmouseover="clearTimeout(menuhider);" onmouseout="hidemenu()">
<div class="vpsmenubg" id="vpsmenubg">
<a href="javascript:void(0)"><img src="'.$theme['images'].'admin/start.gif" title="'.$l['vpmenu_start'].'" onclick="vpsactions(\'start\')"/></a>
<a href="javascript:void(0)"><img src="'.$theme['images'].'admin/stop.gif" title="'.$l['vpmenu_stop'].'" onclick="vpsactions(\'stop\')"/></a>
<a href="javascript:void(0)"><img src="'.$theme['images'].'admin/restart.gif" title="'.$l['vpmenu_restart'].'"  onclick="vpsactions(\'restart\')"/></a>
<a href="javascript:void(0)"><img src="'.$theme['images'].'admin/poweroff.gif" title="'.$l['vpmenu_poweroff'].'"  onclick="vpsactions(\'poweroff\')"/></a>'
.(!empty($globals['novnc']) ? '<a href="javascript:void(0)" id="novncURL" class="vncButton" onclick="launchvnc(1)"/><img style="padding-left:4px" src="'.$theme['images'].'admin/listvs_vnc.gif" title="'.$l['vpmenu_novnc'].'"/></a>' : (empty($globals['disable_java_vnc']) ? '<a href="javascript:void(0)" id="java_vnc" class="vncButton" onclick="launchvnc(2)"/><img style="padding-left:4px" src="'.$theme['images'].'admin/listvs_vnc.gif" title="'.$l['vpmenu_javavnc'].'"/></a>' : '')).'
</div>
</div>

<!-- Code for showing the manage vps menu -->
<div id="managevpsmenu" class="vpsmanagemenu"></div>

<div id="showsearch" style="display:'.(optREQ('search') || (!empty($vs) && !empty($globals['showsearch'])) ? "" : "none").';">
<form accept-charset="'.$globals ['charset'].'" name="ippool" method="get" action="" id="ippoolform" class="form-horizontal">
<input type="hidden" name="act" value="vs">
<div class="form-group_head">
	<div class="row">
	    <div class="col-xs-3 col-sm-1"><label>'.$l['vpsid'].'</label></div>
	    <div class="col-xs-3 col-sm-2"><input class="form-control" type="text" name="vpsid" id="vpsid" size="15" value="'.REQval('vpsid','').'"/></div>
	    <div class="col-xs-3 col-sm-1"><label>'.$l['vpsname'].'</label></div>
		<div class="col-xs-3 col-sm-2"><input type="text" class="form-control" name="vpsname" id="vpsname" size="30" value="'.REQval('vpsname', '').'" /></div>
	    <div class="col-xs-3 col-sm-1"><label>'.$l['vpsip'].'</label></div>
	    <div class="col-xs-3 col-sm-2"><input type="text" class="form-control" name="vpsip" id="vpsip" size="25" value="'.REQval('vpsip','').'"/></div>
		<div class="col-xs-3 col-sm-1"><label>'.$l['vpshostname'].'</label></div>
	    <div class="col-xs-3 col-sm-2"><input type="text" class="form-control" name="vpshostname" id="vpshostname" size="25" value="'.REQval('vpshostname','').'"/></div>
  	</div>
  	<div class="row">
	    <div class="col-xs-3 col-sm-1"><label>'.$l['vsstatus'].'</label></div>
		<div class="col-xs-3 col-sm-2">
	    	<select name="vsstatus" id="vsstatus" class="form-control">
				<option value="0" '.(REQval('vsstatus') == 0 ? 'selected="selected"' : '').'>'.$l['status_none'].'</option>
				<option value="s" '.(REQval('vsstatus') == 's' ? 'selected="selected"' : '').'>'.$l['status_suspended'].'</option>
				<option value="u" '.(REQval('vsstatus') == 'u' ? 'selected="selected"' : '').'>'.$l['status_unsuspended'].'</option>
			</select>
	    </div>
		<div class="col-xs-3 col-sm-1"><label>'.$l['vstype'].'</label></div>
		<div class="col-xs-3 col-sm-2">
			<select name="vstype" style="width:99%" id="vstype" class="form-control">
				<option value="0" '.(REQval('vstype') == 0 ? 'selected="selected"' : '').'>'.$l['status_none'].'</option>
				<option value="xen" '.(REQval('vstype') == 'xen' ? 'selected="selected"' : '').'>'.$l['vsxen'].'</option>
				<option value="xenhvm" '.(REQval('vstype') == 'xenhvm' ? 'selected="selected"' : '').'>'.$l['vsxenhvm'].'</option>
				<option value="xcp" '.(REQval('vstype') == 'xcp' ? 'selected="selected"' : '').'>'.$l['vsxcp'].'</option>
				<option value="xcphvm" '.(REQval('vstype') == 'xcphvm' ? 'selected="selected"' : '').'>'.$l['vsxcphvm'].'</option>
				<option value="openvz" '.(REQval('vstype') == 'openvz' ? 'selected="selected"' : '').'>'.$l['vsopenvz'].'</option>
				<option value="kvm" '.(REQval('vstype') == 'kvm' ? 'selected="selected"' : '').'>'.$l['vskvm'].'</option>
				<option value="lxc" '.(REQval('vstype') == 'lxc' ? 'selected="selected"' : '').'>'.$l['vslxc'].'</option>
				<option value="vzo" '.(REQval('vstype') == 'vzo' ? 'selected="selected"' : '').'>'.$l['vsvzo'].'</option>
				<option value="vzk" '.(REQval('vstype') == 'vzk' ? 'selected="selected"' : '').'>'.$l['vsvzk'].'</option>
				<option value="proxo" '.(REQval('vstype') == 'proxo' ? 'selected="selected"' : '').'>'.$l['vsproxo'].'</option>
				<option value="proxk" '.(REQval('vstype') == 'proxk' ? 'selected="selected"' : '').'>'.$l['vsproxk'].'</option>
				<option value="proxl" '.(REQval('vstype') == 'proxl' ? 'selected="selected"' : '').'>'.$l['vsproxl'].'</option>
			</select>
		</div>
	    <div class="col-xs-3 col-sm-1">
			<label class="form-check-label" for="speedcap">'.$l['lv_speedcap'].'</label>
		</div>
		<div class="col-xs-3 col-sm-2">
			<select name="speedcap" id="speedcap" class="form-control">
				<option value="0" '.(REQval('speedcap') == 0 ? 'selected="selected"' : '').'>'.$l['status_none'].'</option>
				<option value="1" '.(REQval('speedcap') == 1 ? 'selected="selected"' : '').'>'.$l['lv_enabled'].'</option>
				<option value="2" '.(REQval('speedcap') == 2 ? 'selected="selected"' : '').'>'.$l['lv_disabled'].'</option>';
		echo '</select>	
		</div>
		<div class="col-xs-3 col-sm-1"><label>'.$l['lv_user'].'</label></div>
	    <div class="col-xs-3 col-sm-2"><input type="text" class="form-control" name="user" size="25" value="'.REQval('user','').'"/></div>
  	</div>
	<div class="row">
		<div class="col-xs-3 col-sm-1" style="text-align: center;"><label>'.$l['servergroup'].'</label></div>
		<div class="col-xs-3 col-sm-2">';
			@$tmpgroupid = explode(',', REQval('vsgid'));
		echo '<input type="hidden" id="vsgid" name="vsgid" value="'.(isset($_REQUEST['vsgid']) ? REQval('vsgid') : -1).'">
			<select class="chosen-select" id="group-option" multiple data-placeholder="'.$l['servergroup'].'">';
					foreach($servergroups as $key => $id) {
						echo '<option value="'.$key.'" '.(isset($_REQUEST['vsgid']) && in_array($key, $tmpgroupid) ? 'selected="selected"' : '').'>'.$id['sg_name'].'</option>';
					}
		
	echo 	'</select>
		</div>
		<div class="col-xs-3 col-sm-1"><label>'.$l['vserid'].'</label></div>
		<div class="col-xs-3 col-sm-2">';
			@$tmpvserid = explode(',', REQval('vserid'));
		echo '<input type="hidden" id="vserid" name="vserid" value="'.(isset($_REQUEST['vserid']) ? REQval('vserid') : -1).'">
	    	<select class="chosen-select" id="server-option" multiple data-placeholder="'.$l['vserid'].'">';
				foreach($servers as $key => $id) {
					echo '<option value="'.$key.'" '.(isset($_REQUEST ['vserid']) && in_array($key, $tmpvserid) ? 'selected="selected"' : '').'>'.$id ['server_name'].'</option>';
				}
	
	echo 	'</select>
	    </div>
		<div class="col-xs-3 col-sm-1">
			<label class="form-check-label" for="plid">'.$l['lv_plan'].'</label>
		</div>
		<div class="col-xs-3 col-sm-2">';
			@$tmpplanid = explode(',', REQval('plid'));
		echo '<input type="hidden" id="plid" name="plid" value="'.(isset($_REQUEST['plid']) ? REQval('plid') : -1).'">
			<select class="chosen-select" id="plan-option" multiple data-placeholder="'.$l['lv_plan'].'">';
				foreach($plans as $key => $id) {
					echo '<option value="'.$key.'" '.(isset($_REQUEST['plid']) && in_array($key, $tmpplanid) ? 'selected="selected"' : '').'>'.$id.'</option>';
				}
		echo '</select>
		</div>
		<div class="col-xs-3 col-sm-1">
			<label class="form-check-label" for="bpid">'.$l['lv_backup_plan'].'<img class="wiki_help" title="'.$l['backup_help'].'" style="margin-left: 0;height:22px; width:20px;" src="'.$theme['images'].'admin/information.gif" /></label>
		</div>
		<div class="col-xs-3 col-sm-2">';
			@$tmpbplanid = explode(',', REQval('bpid'));
		echo '<input type="hidden" id="bpid" name="bpid" value="'.(isset($_REQUEST['bpid']) ? REQval('bpid') : -1).'">
			<select class="chosen-select" id="bplan-option" multiple data-placeholder="'.$l['lv_backup_plan'].'">';
				foreach($backup_plans as $key => $id) {
					echo '<option value="'.$key.'" '.(isset($_REQUEST['bpid']) && in_array($key, $tmpbplanid) ? 'selected="selected"' : '').'>'.$id.'</option>';
				}
		echo '</select>
		</div>
		<div class="col-sm-12" style="text-align: center;"><button type="submit" name="search" class="btn go_btn" value="Search"/>'.$l['submit'].'</button></div>
	</div>
	
</div>
</form>
<br/>			
</div>';
	
	if (empty($vs)) {
		
		echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.(optREQ('search') ? $l['no_res'] : $l['no_vs']).'</div>';
	} else {
		
		echo '<script language="javascript" type="text/javascript"><!-- // --><![CDATA[

// Get the VPS Status
function getvpsstatus(){
	
	$_("refresh_status").src = "'.$theme['images'].'admin/vpsloading.gif";
	
	$.ajax({type: "GET",
		url: "'.$globals['index'].'act=vs&vs_status='.implode(',', array_keys($vs)).'",
		data:null,
		success:function(response){
			//alert(response);
			eval(response);
			$_("refresh_status").src = "'.$theme['images'].'refresh.png";
		}
	});
}

$(document).ready(function(){
	getvpsstatus();
});

$(document).ready(function(){
	
	progress_onload(1);
	
	$("a.vpsinfotool").hover(function(e){
		$(this).find("span",0).fadeIn(250);
	},function(e){
		$(this).find("span",0).fadeOut(10);
	});
	
	$("a.bandinfo").hover(function(e){
		$(this).find("span").fadeIn(250);
	},function(e){
		$(this).find("span").fadeOut(10);
	});
	
	$("span.ip_count").hover(function(){
		var ul_ip_lists = $(this).next().find("ul.ip_lists");
		var H = ul_ip_lists.height() + 14; // 14 is total padding
		if(H < 200){
			ul_ip_lists.css({ "overflow-y" : "hidden"}).fadeIn(200);
		}else{
			ul_ip_lists.fadeIn();
		}
	},function(){});
	
	$("ul.ip_lists").hover(function(){},function(){
		$("ul.ip_lists").fadeOut("fast");
	});
	
});

// ]]></script>';

page_links($globals['num_res'], $globals['cur_page'], $globals['reslen']);
echo '<br /><br />
<table class="table table-hover tablesorter">
<tr>
	<th height="30"><a href="javascript:void(0)" onclick="getvpsstatus();"><img src="'.$theme['images'].'refresh.png" id="refresh_status" /></a></th>
	<th>'.getSortHeader($l['lv_id'], $sort_column, $sort_column_by, 'vpsid').'</th>
	<th>'.getSortHeader($l['lv_cid'], $sort_column, $sort_column_by, 'vps_name').'</th>
	<th><span>'.$l['lv_type'].'</span></th>
	<th><span>'.$l['lv_os'].'</span></th>
	<th>'.getSortHeader($l['lv_hostname'], $sort_column, $sort_column_by, 'hostname').'</th>
	<th><span>'.$l['lv_ip'].'</span></th>
	<th><span>'.$l['lv_server'].'</span></th>
	<th colspan="4"><span>'.$l['lv_stats'].'</span></th>
	<th width="50">'.getSortHeader($l['lv_user'], $sort_column, $sort_column_by, 'email').'</th>
	<th align="center" colspan="3" width="75">'.$l['action'].'</th>
	<th style="padding:8px 3px !important"><input type="checkbox" name="select_all" id="select_all" class="select_all"></th>
</tr>';
		
		$i = 1;
		
		foreach($vs as $k => $v) {
			
			@reset($vs [$k] ['ips']);
			
			$v['bandwidth_percent'] =(float) ($v['used_bandwidth'] / $v['bandwidth']);
			
			echo '<tr id="trid_'.$k.'">
	<td style="vertical-align:middle;" id="stat_'.$k.'" align="center">
		<a href="javascript:void(0)" onmouseover="showvpsmenu('.$v['serid'].', '.$k.')" onmouseout="hidemenu()">
			<img src="'.$theme['images'].'progress.gif" width="16" height="16" />
		</a>
	</td>
	<td style="vertical-align:middle;"><a href="'.$globals ['ind'].'act=editvs&vpsid='.$k.'" class="vpsinfotool">'.$k.'
		<span>
		<table align="center" cellpadding="5" cellspacing="1" border="0" width="100%" class="vpsinfo">
			<tr>
				<th align="center" class="">'.$l['lv_space'].'</th>
				<th align="center" class="">'.$l['lv_core'].'</th>
				'.($v['virt'] == 'kvm' ? '' : '<th align="center" class="">'.$l['lv_cpup'].'</th>').'
				'.($v['virt'] == 'openvz' ? '' : '<th align="center" class="">'.$l['lv_vnc'].'</th>').'
				<th align="center" class="">'.$l['lv_burst'].'</th>
				<th align="center" class="">'.$l['lv_ram'].'</th>
				<th align="center" class="">'.$l['lv_netspeed'].'</th>
				<th align="center" class="">'.$l['lv_bandwidth'].'</th>
				<th align="center" class="">'.$l['lv_speedcap'].'</th>
				<th align="center" class="">'.$l['creation_date'].'</th>
				<th align="center" class="">'.$l['last_edited'].'</th>
			</tr>
			<tr>
				<td align="center">'.$v['space'].' '.$l['lv_gb'].'</td>
				<td align="center">'.$v['cores'].'</td>
				'.($v['virt'] == 'kvm' ? '' : '<td align="center">'.$v['cpu_percent'].'</td>').'
				'.($v['virt'] == 'openvz' ? '' : '<td align="center">'.($v['vnc'] ? $l['lv_enabled'] : $l['lv_disabled']).'</td>').'
				<td align="center">'.(! empty($v['swap']) ? $v['swap'] : $v['burst']).' '.$l['lv_mb'].'</td>
				<td align="center">'.$v['ram'].' '.$l['lv_mb'].'</td>
				<td align="center">'.(empty($v['network_speed']) ? $l['unlimited'] : $v['network_speed']).'</td>
				<td align="center">'.(empty($v['bandwidth']) ? $l['unlimited'] : round($v['used_bandwidth'],1).'/'.$v['bandwidth']).' '.$l['lv_gb'].'
				<br /><br /><a onclick="return conf_band_reset(this);" href="'.$globals ['ind'].'act=vs&bwreset='.$k.'" class="link_btn">'.$l['band_reset'].'</a><br/><br/></td>
				<td align="center">'.(empty($v['speed_cap']) ? 'N/A' : $v['speed_cap']['up'].' KB UL<br/>'.$v['speed_cap']['down'].' KB DL<br/>').'</td>
				<td align="center">'.(empty($v['time']) ? 'N/A' : datify($v['time'])).'</td>
				<td align="center">'.(empty($v['edittime']) ? 'N/A' : datify($v['edittime'])).'</td>
			</tr>
		</table>
		</span>
	</a></td>
	<td style="vertical-align:middle;">'.$v['vps_name'].' <a href="https://'.$globals ['HTTP_HOST'].':4083/'.$SESS ['token_key'].'/index.php?#!svs='.$k.'" target="_blank" title="'.$l['svs_adm_login_title'].'"><img src="'.$theme['images'].'admin/svs_login.gif" /></a></td>
	<td align="center" style="vertical-align:middle;"><img src="'.$theme['images'].'admin/'.$v['virt'].(empty($v['hvm']) ? '' : 'hvm').'_42.gif" /></td>
	<td align="center" style="vertical-align:middle;"><img src="'.distro_logo($v['os_distro']).'" title="'.$v['os_name'].'" /></td>
	<td style="vertical-align:middle;" class="'.($v['rescue'] == 1 ? "rescuecolor" : "none").'" id="tr_hostname'.$k.'" val="'.$v['hostname'].'">'.$v['hostname'].($v['rescue'] == 0 ? '' : '<a href="#" class="rescuetip" style="float:right;"><img src="'.$theme['images'].'rescue.png" width="20"><span>'.$l['rescue_tip'].'</span>').(empty($v['nw_suspended']) ? '' : '<a href="#" class="rescuetip" style="float:right;"><img src="'.$theme['images'].'admin/disconnect.png" width="15" id="ntw_suspended'.$k.'" style="float:right;margin:5px;" title="'.$l['status_ntwsuspended'].'">').'</td>';
			$current_ip = @current($v['ips']);
			echo '<td style="vertical-align:middle;"><span '.(strlen($current_ip) > 15 ? 'style="font-size:9px"' : '').'>'.$current_ip.'</span>'.(count($v['ips']) > 1 ? '<span class="ip_count">'.count($v['ips']).'</span>'.'<div style="position:relative;"><ul class="ip_lists">'.array_reduce($v['ips'], '__make_li', '').'</ul></div>' : '').'</td>
	<td style="vertical-align:middle;" id="tr_server'.$k.'"  >'.$v['server_name'].'</td>
	<td align="center" style="vertical-align:middle;" id="bw_stats_'.$k.'" class="pad4 spband"></td>
	<td align="center" style="vertical-align:middle;" id="cpu_stats_'.$k.'" class="pad4 spcpu"></td>
	<td align="center" style="vertical-align:middle;" id="disk_stats_'.$k.'" class="pad4 spdisk"></td>
	<td align="center" style="vertical-align:middle;" id="ram_stats_'.$k.'" class="pad4 spram"></td>
	<td style="vertical-align:middle;">'.(empty($v['email']) ? '<em>'.$l['no_info'].'</em>' : $v['email'].' <a href="https://'.$globals['HTTP_HOST'].':4083/'.$SESS['token_key'].'/index.php?suid='.$v['uid'].'" target="_blank"><img src="'.$theme['images'].'admin/svs_login.gif" /></a>').($v['type'] == 2 ? '<img src="'.$theme['images'].'admin/cloud_user.png" alt="'.$l['is_cloud_user'].'" title="'.$l['is_cloud_user'].'" style="display: block; float:right;" />' : '').(! empty($v['pid']) ? '<span class="ip_count"><img src="'.$theme['images'].'admin/user_icon.png" width="10px" height="10px"  /></span>'.'<div style="position:relative;"><ul class="ip_lists"><a href="https://'.$globals['HTTP_HOST'].':4083/'.$SESS['token_key'].'/index.php?suid='.$v['pid'].'" target="_blank" style="color:#b4b4b4;">'.$owners[$v['pid']]['email'].'</a>'.'</ul></div>' : '').'</td>
	<td id="action_'.$k.'" width="10" align="center" style="padding:8px 3px !important" colspan="3">
		<img title="'.$l['vps_id_stat_off'].'" src="'.$theme['images'].'admin/manage.png" width="20" onmouseover="showmanagevpsmenu('.$v['serid'].', '.$v['id'].' ,'.(!empty($v['status']) ? (empty($v['suspended']) ? '1' : '2') : '0').','.(!empty($v['nw_suspended'])? '1' : '0').')" onmouseout="hidemanagevpsmenu()" />&nbsp;
		<a href="'.$globals['ind'].'act=editvs&vpsid='.$k.'"><img src="'.$theme['images'].'admin/edit.png" /></a>&nbsp;
		<a href="'.$globals['ind'].'act=managevps&vpsid='.$k.'" id="action_vpsmanage_'.$k.'"><img title="'.$l['manage_vps'].'" src="'.$theme['images'].'admin/managevps.png" width="18" /></a>
	</td>

	<td align="center" style="padding:8px 3px !important"><input type="checkbox" class="ios" name="vps_list[]" value="'.$k.'"/></td>
	</tr>';
			$i++;
		}
		
		echo '</table>
<div class="row bottom-menu">
	<div class="col-sm-6"></div>
	<div class="col-sm-6">
		<label>'.$l['with_selected'].'</label>
		<select name="multi_options" id="multi_options" class="form-control">
			<option value="0">---</option>
			<option value="vs_netrestrict">'.$l['ms_vs_netrestrict'].'</option>
			<option value="start">'.$l['ms_start'].'</option>
			<option value="stop">'.$l['ms_stop'].'</option>
			<option value="restart">'.$l['ms_restart'].'</option>
			<option value="poweroff">'.$l['ms_poweroff'].'</option>
			<option value="suspend">'.$l['ms_suspend'].'</option>
			<option value="unsuspend">'.$l['ms_unsuspend'].'</option>
			<option value="suspend_net">'.$l['ms_suspend_net'].'</option>
	                <option value="unsuspend_net">'.$l['ms_unsuspend_net'].'</option>
			<option value="delete">'.$l['ms_delete'].'</option>
		</select>&nbsp;
		<input type="button" value="'.$l['go'].'" onclick="show_confirm()" class="go_btn"/></div>
	</div>
	<div id="rem_div"></div>
	<div id="progress_onload"></div>
<br />
<center>
<a href="'.$globals ['ind'].'act=addvs" class="link_btn">'.$l['add_vs'].'</a>
</center><br />';
}
	
page_links($globals ['num_res'], $globals ['cur_page'], $globals ['reslen']);
echo '<div class="notice" align="left" style="font-size:12px; line-height:150%"><img src="'.$theme['images'].'/notice.gif" align=left/> &nbsp; '.$l['resources_note'].'</div></div>';
softfooter ();
}

?>