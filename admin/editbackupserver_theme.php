<?php

//////////////////////////////////////////////////////////////
//===========================================================
// editbackupserver_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function editbackupserver_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $done, $backup_server;

softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-servers icon-head"></i>&nbsp; '.$l['edit_backupserver'].'</center>';

error_handle($error);

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
}

echo '<div id="form-container">

<form accept-charset="'.$globals['charset'].'" name="editbackupserver" method="post" action="" class="form-horizontal">

  <div class="row">
    <div class="col-sm-5">
      <label class="control-label">'.$l['name'].'</label>
      <span class="help-block">'.$l['exp_name'].'</span>
    </div>
    <div class="col-sm-6">
      <input type="text" class="form-control" name="name" id="name" size="30" value="'.POSTval('name', $backup_server['name']).'" />
    </div>
  </div>
  <div class="row">
    <div class="col-sm-5">
      <label class="control-label">'.$l['hostname'].'</label>
      <span class="help-block">'.$l['exp_hostname'].'</span>
    </div>
    <div class="col-sm-6">
      <input type="text" class="form-control" name="hostname" id="hostname" size="30" value="'.POSTval('hostname', $backup_server['hostname']).'" />
    </div>
  </div>
  <div class="row">
    <div class="col-sm-5">
      <label class="control-label">'.$l['type'].'</label>
      	<span class="help-block">&nbsp;</span>
    </div>
    <div class="col-sm-6">
      <select class="form-control"name="type" id="type">
			<option value="FTP" '.ex_POSTselect('type', "FTP", $backup_server['type']).'>FTP</option>
			<option value="SSH" '.ex_POSTselect('type', "SSH", $backup_server['type']).'>SSH</option>
      </select>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-5">
      	<label class="control-label">'.$l['username'].'</label>
		<span class="help-block">&nbsp;</span>
    </div>
    <div class="col-sm-6">
      <input type="text" class="form-control" name="username" id="username" size="30" value="'.POSTval('username', $backup_server['username']).'" />
    </div>
  </div>
  <div class="row">
    <div class="col-sm-5">
      <label class="control-label">'.$l['password'].'</label>
      	<span class="help-block">&nbsp;</span>
    </div>
    <div class="col-sm-6">
      <input type="password" class="form-control" name="password" id="password" size="30" value="'.POSTval('password').'" />
    </div>
  </div>
  <div class="row">
    <div class="col-sm-5">
      <label class="control-label">'.$l['port'].'</label>
      	<span class="help-block">&nbsp;</span>
    </div>
    <div class="col-sm-6">
      <input type="text" class="form-control" name="port" id="port" size="30" value="'.POSTval('port', $backup_server['port']).'" />
    </div>
  </div>
		
</div>

<br /><br />
<center><input type="submit" class="btn" name="editbackupserver" value="'.$l['submit'].'"></center>

</form>

</div>';

echo '</div>';
softfooter();

}

?>
