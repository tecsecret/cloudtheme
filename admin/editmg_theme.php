<?php

//////////////////////////////////////////////////////////////
//===========================================================
// editmg_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function editmg_theme(){

global $theme, $globals, $kernel, $_user, $l, $error, $done, $mg;

softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-ostemplates icon-head"></i> &nbsp; '.$l['page_head'].'</center>';

error_handle($error);

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
}else{

echo '<script language="javascript" type="text/javascript">

function changetype(mg_type){	
	$_("openvz").className = "";
	$_("xen").className = "";
	$_("xenhvm").className = "";
	$_("kvm").className = "";
	$_(mg_type).className = "server_over";
	$_("mg_type").value = mg_type;
};

addonload("changetype(\''.POSTval('mg_type', $mg['mg_type']).'\');");

</script>
<form accept-charset="'.$globals['charset'].'" name="editmg" method="post" action="" class="form-horizontal">

<div class="row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['mg_name'].'</label><br />
		<span class="help-block">'.$l['mg_name_exp'].'</span>
	</div>
	<div class="col-sm-8">
		<input type="text" class="form-control" name="mg_name" id="mg_name" size="30" value="'.POSTval('mg_name', $mg['mg_name']).'" />
	</div>
</div>

<div class="row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['mg_desc'].'</label><br />
		<span class="help-block">'.$l['mg_desc_exp'].'</span>
	</div>
	<div class="col-sm-8">
		<textarea class="form-control" name="mg_desc" id="mg_desc" cols="40">'.POSTval('mg_desc', $mg['mg_desc']).'</textarea>
	</div>
</div>

<div class="row">
<br/>
</div>

<div class="row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['mg_type'].'</label><br />
		<span class="help-block">'.$l['mg_type_exp'].'</span>
	</div>
	<div class="col-sm-8">
		<img src="'.$theme['images'].'admin/'.$mg['mg_type'].'_100.gif" />
	</div>
</div>
<br />
<center><input type="submit" name="editmg" value="'.$l['submit'].'" class="btn"></center>
</form>
';

}
echo '</div>';
softfooter();

}

?>