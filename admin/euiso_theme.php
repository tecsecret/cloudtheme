<?php

//////////////////////////////////////////////////////////////
//===========================================================
// editiso_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function euiso_theme(){

global $theme, $globals, $users, $l, $error, $isos, $isoname, $done, $user, $userisos, $euisos, $isoid;

// Is Ajax Mode on?

softheader($l['<title>']);
echo '
<div class="bg">
<center class="tit"><i class="icon icon-ostemplates icon-head"></i> &nbsp; '.$l['euiso'].'
<span style="float:right"><span id="vdf_search_link" onclick="showsearch();"><i class="icon-search" style="padding: 0px; margin: 0px 5px; font-size: 25px; cursor: pointer;" ></i></span>
</span></center>';
error_handle($error);

echo '<script language="javascript" type="text/javascript">

function Deliso(data){
	data = data || 0;	
	var isoinfo = new Object();
	var ids = [];
	// Go Button pressed
	if(data == 0){
		
		$(".isorow").each(function(){
			if($(this).is(":checked")){
				ids.push($(this).val());				
			}
		});
	
	// Direct X button
	}else{
		ids.push(data);	
	}
	
	if(ids.length <= 0){
		alert("'.$l['eu_noiso'].'");
		return;
	}
	
	var fids = new Object();
	fids["act"]="euiso";
	fids["del"] = ids.join(",");
	
	if(confirm("'.$l['eu_confirm_del'].'") == true){
		if(AJAX("'.$globals['index'].'api=json&"+$.param(fids),"response(re)")){
			return false;
		}else{
			return true;
		}
	}
};

function response(re){
	data = $.parseJSON(re);	
	if( "done" in data){
		alert("'.$l['eu_done'].'");
		location.reload();
	}else{
		alert("'.$l['eu_error'].'");
	}
};

function checkbox_select_all(el){
	var checked = $(el).is(":checked");
	$(".ios").each(function(){
		$(this).prop("checked", checked);
	});
};

$(document).ready(function(){
	$(".chosen").chosen({width: "100%", no_results_text: "'.$l['eu_noresult'].'"});	
});

</script>';

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['eu_done'].'</div><br />';
}

//-------------- Search Block Starts ---------------------
echo '<div id="showsearch" style="display:'.(optREQ('euisosearch') || (!empty($globals['showsearch'])) ? "" : "none").';">
<form accept-charset="'.$globals ['charset'].'" name="euisosearchform" method="get" action="" id="euisosearchform" class="form-horizontal">
<input type="hidden" name="act" value="euiso">
<div class="form-group_head">
	<div class="row">	    
		<div class="col-sm-5">
			<div class="col-xs-12"><label>'.$l['eu_isouser'].'</label></div>
			<div class="col-xs-12">
				<select name="uid" id="isouser" class="form-control chosen">
					<option value="-1" '.(REQval('isouser') == - 1 ? 'selected="selected"' : '').'>'.$l['eu_none'].'</option>';
					foreach($users as $key => $user) {
						echo '<option value="'.$key.'" '.(isset($_REQUEST['isouser']) && REQval('isouser') == $key ? 'selected="selected"' : '').'>'.$user['email'].'</option>';
					}
		echo 	'</select>
			</div>
		</div>
		<div class="col-sm-5">
			<div class="col-xs-12"><label>'.$l['eu_isofile'].'</label></div>
			<div class="col-xs-12">
				<input class="form-control" type="text" name="iso" id="isofile" value="'.REQval('iso', '-1').'" />
			</div>
		</div>
		<div class="col-sm-2">
			<div class="col-xs-12"><label></label></div>
			<div class="col-xs-12" style="text-align: center;"><button type="submit" name="euisosearch" class="go_btn" value="1" />'.$l['search'].'</button></div>
		</div>
  	</div>
</div>
</form>
</div>
<br/>';
//-------------- Search Block Ends ---------------------
page_links($globals['num_res'], $globals['cur_page'], $globals['reslen']);
echo '<div class="row">
		<table class="table table-hover tablesorter" id="euisolist">
		<tr>
			<th align="center" width="100">'.$l['eu_uuid'].'</th>
			<th align="center">'.$l['eu_filename'].'</th>
			<th align="center">'.$l['eu_isosize'].'</th>
			<th align="center" width="80">'.$l['eu_uemail'].'</th>
			<th width="20" align="center">'.$l['eu_isodel'].'</th>
			<th><input type="checkbox" class="select_all" name="select_all" id="select_all"></th>
		</tr>';
		foreach($euisos as $k => $v){
			echo'<tr>
				<td align="center">'.$k.'</td>
				<td>'.$v['iso'].'</td>
				<td align="center">'.round($v['size']/1024/1024).' MB</td>
				<td align="center"><a href="'.$globals['ind'].'act=edituser&uid='.$v['uid'].'">'.$v['email'].'</a></td>
				<td align="center"><a href="javascript:void(0);" onclick="return Deliso(\''.$k.'\');"  title="'.$l['del_os'].'"><img src="'.$theme['images'].'admin/delete.png" /></a></td>
				<td width="20" align="center">
					<input type="checkbox" class="ios isorow" name="iso_list[]" value="'.$v['uuid'].'" data-uid="'.$v['uid'].'"/>
				</td>
			</tr>';
		}
	echo '</table>
	</div>';
	
	echo '<div class="row bottom-menu">
		<div class="col-sm-7"></div>
		<div class="col-sm-5"><label style="font-size:12px">'.$l['with_selected'].'</label>
			<select name="iso_task_select" class="form-control" id="iso_task_select">
				<option value="1">'.$l['ms_delete'].'</option>
			</select>&nbsp;
			<input type="button" id ="iso_submit" class="go_btn" name="iso_submit" value="Go" onclick="Deliso();">
		</div>
	</div>
</div>';

softfooter();

}

?>