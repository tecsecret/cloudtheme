<?php

//////////////////////////////////////////////////////////////
//===========================================================
// ftpservers_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Julien
// Date:       19th July 2012
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function backupservers_theme(){

global $theme, $globals, $kernel, $user, $l, $backupservers, $error, $done, $done_test, $request_test;

softheader($l['<title>']);

if(!empty($request_test)){
	echo $done_test;
	return true;
}

echo '
<div class="bg" style="width: 99%">
<center class="tit">
<i class="icon icon-servers icon-head"></i>&nbsp; '.$l['plans_tit'].'<span style="float:right"><a href="javascript:showsearch();"><img src="'.$theme['images'].'admin/search.gif" /></a></span></center>';

error_handle($error);

echo '<script language="javascript" type="text/javascript"><!-- // --><![CDATA[



function Delbservers(bid){

	bid = bid || 0;
	
	// List of ids to delete
	var bserver_list = new Array();
	
	if(bid < 1){
		
		if($("#bserver_task_select").val() != 1){
			alert("'.$l['no_action'].'");
			return false;
		}
		
		$(".ios:checked").each(function() {
			bserver_list.push($(this).val());
		});
		
	}else{
		
		bserver_list.push(bid);
		
	}
	
	if(bserver_list.length < 1){
		alert("'.$l['nothing_selected'].'");
		return false;
	}
	
	var bserver_conf = confirm("'.$l['del_conf'].'");
	if(bserver_conf == false){
		return false;
	}
	
	var finalData = new Object();
	finalData["delete"] = bserver_list.join(",");
	
	//alert(finalData);
	//return false;
	
	$("#progress_bar").show();
	
	$.ajax({
		type: "POST",
		url: "'.$globals['index'].'act=backupservers&api=json",
		data : finalData,
		dataType : "json",
		success: function(data){
			$("#progress_bar").hide();
			if("done" in data){
				alert("'.$l['action_completed'].'");
			}
			if("error" in data){
				alert(data["error"]);
			}
			location.reload(true);
		},
		error: function(data) {
			$("#progress_bar").hide();
			//alert(data.description);
			return false;
		}
	});
	
	return false;
};

$(document).ready(function(){
	$(".test-button").each(function(index){
		$(this).click(function(){	
			var parent = $(this).parent();
			var id =  parent.attr("id");
			$(this).find("img").attr("src", "'.$theme['images'].'progress_bar.gif");
			$.post("'.$globals['index'].'act=backupservers&jsnohf=1&test="+id, function(data){
				if(data == "1"){
					$(".backupserver-test").find("img").attr("src", "'.$theme['images'].'admin/connection.png");
					alert("'.$l['success_connect'].'");
				}
				else{
					$(".backupserver-test").find("img").attr("src", "'.$theme['images'].'admin/connection.png");
					alert("'.$l['unable_connect'].'");
				}
			});
			
			return false;
		});
	});
});

// ]]></script>

<div id="showsearch" style="display:'.(optREQ('search') || (!empty($backupservers) && !empty($globals['showsearch'])) ? "" : "none").';">
<form accept-charset="'.$globals['charset'].'" name="bservers" method="GET" action="" class="form-horizontal">
<input type="hidden" name="act" value="backupservers">
		
<div class="form-group_head">
  <div class="row">
	<div class="col-sm-1"></div>
    <div class="col-sm-1"><label>'.$l['sbyname'].'</label></div>
    <div class="col-sm-2"><input type="text" class="form-control" name="name" id="name" size="30" value="'.POSTval('name','').'"/></div>
    <div class="col-sm-1"><label>'.$l['sbyhostname'].'</label></div>
    <div class="col-sm-2"><input type="text" class="form-control" name="hostname" id="hostname" size="30" value="'.POSTval('hostname','').'"/></div>
    <div class="col-sm-1"><label>'.$l['sbytype'].'</label></div>
    <div class="col-sm-2"><input type="text" class="form-control" name="type" id="type" size="30" value="'.POSTval('type','').'"/></div>
	<div class="col-sm-1"><button type="submit" name="search" class="go_btn" value="Search"/>'.$l['submit'].'</button></div>
  </div>
</div>
</form>
<br />
<br />
</div>';

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['saved'].'</div>';
}

if(empty($backupservers)){

	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.(optREQ('search') ? $l['no_res'] : $l['no_backupservers']).'</div>';
	
}else{

page_links($globals['num_res'], $globals['cur_page'], $globals['reslen']);
echo '<br /><br />
<form accept-charset="'.$globals['charset'].'" name="multi_bserver" id="multi_bserver" method="post" action="" class="form-horizontal">
<table class="table table-hover tablesorter">
<tr>
	<th align="center">'.$l['id'].'</th>
	<th align="center">'.$l['name'].'</th>
	<th align="center">'.$l['type'].'</th>
	<th align="center">'.$l['hostname'].'</th>
	<th align="center">'.$l['username'].'</th>
	<th align="center">'.$l['port'].'</th>
	<th align="center">'.$l['test'].'</th>
	<th colspan="2">'.$l['manage'].'</th>
	<th><input type="checkbox" class="select_all" name="select_all" id="select_all"></th>
</tr>';
$i = 1;
foreach($backupservers as $k => $v){
	echo '<tr>
		<td align="left">'.$v['bid'].'</td>
		<td>'.$v['name'].'</td>
		<td  align="center">'.$v['type'].'</td>
		<td>'.$v['hostname'].'</td>
		<td>'.$v['username'].'</td>
		<td align="center">'.$v['port'].'</td>
		<td class="backupserver-test manage-ico" style="cursor:pointer;" id="'.$v['bid'].'" align="center"><a class="test-button" title="'.$l['test_con'].'"><img src="'.$theme['images'].'admin/connection.png" width="20" /></a></td>
		<td align="center" class="manage-ico"><a href="?act=editbackupserver&id='.$v['bid'].'" title="Edit"><img src="'.$theme['images'].'admin/edit.png" /></a></td>
		<td align="center" class="manage-ico"><a href="javascript:void(0);" onclick="return Delbservers('.$k.');" title="'.$l['delete'].'"><img src="'.$theme['images'].'admin/delete.png" /></a></td>
		<td width="20" valign="middle" align="center">
			<input type="checkbox" class="ios" name="backupserver_list[]" value="'.$k.'"/>
		</td>
		</tr>';
	$i++;
}
echo '</table>

<div class="row bottom-menu">

	<div class="col-sm-7"></div>
	<div class="col-sm-5"><label>'.$l['with_selected'].'</label>
		<select class="form-control" name="bserver_task_select" id="bserver_task_select">
			<option value="0">---</option>
			<option value="1">'.$l['ms_delete'].'</option>
		</select>&nbsp;
		<input type="submit" id ="bserver_submit" class="go_btn" name="bserver_submit" value="Go" onclick="Delbservers(); return false;">
	</div>
</div>
<div id="progress_bar" style="height:125px; display:none">
	<br />
	<center>
		<font id="progress_txt" size="4" color="#222222">'.$l['action_msg'].'</font>
		<br>
		<br>
	</center>
	<table id="table_progress" width="500" height="28" cellspacing="0" cellpadding="0" border="0" align="center" style="border:1px solid #CCC; -moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px;background-color:#efefef;">
		<tbody>
			<tr>
				<td id="progress_color" width="100%" style="background-image: url(themes/default/images/bar.gif); -moz-border-radius: 4px; -webkit-border-radius: 4px; border-radius: 4px;"></td>
				<td id="progress_nocolor"> </td>
			</tr>
		</tbody>
	</table>
	<br>
	<center>
		'.$l['notify_msg'].'
	</center>
</div>

<br/>

<center>
<input type="button" value="'.$l['add_backupserver'].'" class="link_btn" onclick="window.location =\''.$globals['ind'].'act=addbackupserver\';">
</center>
';
}

page_links($globals['num_res'], $globals['cur_page'], $globals['reslen']);

echo '</div>';

softfooter();

}
?>
