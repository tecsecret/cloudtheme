<?php

//////////////////////////////////////////////////////////////
//===========================================================
// users_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function users_theme(){

global $theme, $globals, $kernel, $users, $l, $error, $done, $SESS;

softheader($l['<title>']);

echo '
<div class="bg" style="width: 99%">
<center class="tit">
<i class="icon icon-users icon-head"></i>&nbsp; '.$l['usr_tit'].'<span style="float:right"><a href="javascript:showsearch();"><img src="'.$theme['images'].'admin/search.gif" /></a></span></center>';

error_handle($error);

echo '<script language="javascript" type="text/javascript"><!-- // --><![CDATA[

function action_chosen(action, uid){
	
	action = action || "";
	uid = uid || 0;
	
	if(action == ""){
		if($("#user_task_select").val() == 0){
				alert("'.$l['no_action'].'");
				return false;
		}else{
			action = $("#user_task_select").val();
		}
	}
	
	// List of ids to delete
	var usr_list = new Array();
	
	if(uid < 1){
		
		$(".ios:checked").each(function(){
			usr_list.push($(this).val());
		});
		
	}else{
		
		usr_list.push(uid);
		
	}
	
	if(usr_list.length < 1){
		alert("'.$l['nothing_selected'].'");
		return false;
	}
	
	// To delete the user
	if(action == "delete"){
		
		var usr_conf = confirm("'.$l['del_conf'].'");
		if(usr_conf == false){
			return false;
		}
		
		var finalData = new Object();
		finalData["delete"] = usr_list.join(",");
	
	// To suspend the user
	}else if(action == "suspend"){
		
		var usr_conf = confirm("'.$l['suspend_conf'].'");
	
		if(usr_conf == false){
			return false;
		}
		
		var finalData = new Object();
		finalData["suspend"] = usr_list.join(",");
	
	// To unsuspend the user	
	}else if(action == "unsuspend"){
		 
		var usr_conf = confirm("'.$l['unsuspend_conf'].'");
	
		if(usr_conf == false){
			return false;
		}
	
		var finalData = new Object();
		finalData["unsuspend"] = usr_list.join(",");			
	
	// To Deactivate 2FA
	}else if(action == "deactivate_2fa"){
		
		var usr_conf = confirm("'.$l['deactivate_2fa_conf'].'");
	
		if(usr_conf == false){
			return false;
		}
	
		var finalData = new Object();
		finalData["deactivate_2fa"] = usr_list.join(",");	

	// To Activate User Account
	}else if(action == "activate_account"){
		
		var usr_conf = confirm("'.$l['activate_account_conf'].'");
	
		if(usr_conf == false){
			return false;
		}
	
		var finalData = new Object();
		finalData["activate_account"] = usr_list.join(",");		
	
	// To Deactivate User Account
	}else if(action == "deactivate_account"){
		
		var usr_conf = confirm("'.$l['deactivate_account_conf'].'");
	
		if(usr_conf == false){
			return false;
		}
	
		var finalData = new Object();
		finalData["deactivate_account"] = usr_list.join(",");		

	// Not a valid action 
	}else{
		alert("'.$l['no_action'].'");
		return false;
	}
	
	perform_action(finalData);
	return ;
}

function perform_action(finalData){
	
	//alert(finalData);
	//return false;
	
	$("#progress_bar").show();
	
	$.ajax({
		type: "POST",
		url: "'.$globals['index'].'act=users&api=json",
		data : finalData,
		dataType : "json",
		success: function(data){
			$("#progress_bar").hide();
			if("done" in data){
				alert("'.$l['action_completed'].'");
				location.reload(true);
			}
			
			if("error" in data){
				alert(data["error"]);
				location.reload(true);
			}
		},
		error: function(data) {
			$("#progress_bar").hide();
			//alert(data.description);
			return false;
		}
	});
	
	return false;
}

// ]]></script>

<div id="showsearch" style="display:'.(optREQ('search') || (!empty($users) && !empty($globals['showsearch'])) ? "" : "none").';">
<form accept-charset="'.$globals['charset'].'" name="users" method="GET" action="" class="form-horizontal">
<input type="hidden" name="act" value="users">
<div class="form-group_head">
	<div class="row">
		<div class="col-xs-12 col-sm-1">
			<label for="input text">'.$l['search_id'].'</label>
		</div>
		<div class="col-xs-12 col-sm-2">
			<input type="text" class="form-control" name="uid" id="uid" size="15" value="'.REQval('uid','').'" />
		</div>
		<div class="col-xs-12 col-sm-1">
			<label for="input text" class="control-label">'.$l['search_email'].'</label>
		</div>
		<div class="col-xs-12 col-sm-3">
			<input type="text" class="form-control" name="email" id="email" size="30" value="'.REQval('email', '').'" style="float:left; width:85%;" /> &nbsp;<img class="wiki_help" title="'.$l['wiki_help'].'" style="height:22px; margin-top:6px; width:20px;" src="'.$theme['images'].'admin/information.gif" /><br/>
		</div>
		<div class="col-xs-12 col-sm-1">
			<label for="input text" class="control-label">'.$l['search_type'].'</label>
		</div>
		<div class="col-xs-12 col-sm-2">
			<select name="type" class="form-control" id="user_type">
				<option value="-1">---</option>
				<option value="0">'.$l['User'].'</option>
				<option value="1">'.$l['Admin'].'</option>
				<option value="2">'.$l['Cloud'].'</option>
			</select>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12" style="text-align: center;">
			<button type="submit" name="search" class="go_btn sm_space" value="Search">'.$l['submit'].'</button>
		</div>
	</div>
</div>
</form>
<br />
<br />
</div>';

if($done){
	echo '<center class="head"><img src="'.$theme['images'].'notice.gif" /> &nbsp; <span class="h3">'.$l['saved'].'</span></center>';
}

if(empty($users)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp;'.(optREQ('search') ? $l['no_res'] : $l['no_users']).'</div>';
}
else{

page_links($globals['num_res'], $globals['cur_page'], $globals['reslen']);
echo '<br /><br />
<form accept-charset="'.$globals['charset'].'" name="multi_users" id="multi_users" method="post" action="" class="form-horizontal">
<table class="table table-hover tablesorter">
<thead>
	<tr>
		<th width="50">'.$l['usr_id'].'</th>
		<th>'.$l['search_email'].'</th>
		<th width="100">'.$l['usr_type'].'</th>
		<th width="100">'.$l['no_of_vps'].'</th>
		<th width="150">'.$l['bw_used'].'</th>
		<th width="20">'.$l['status'].'</th>
		<th width="70">'.$l['2fa_status'].'</th>
		<th colspan="3">'.$l['manage'].'</th>
		<th><input type="checkbox" class="select_all" name="select_all" id="select_all"></th>
	</tr>
</thead>';

$i = 1;

foreach($users as $k => $v){	
	
	echo '<tbody><tr>
			<td align="left">'.$v['uid'].'</td>
			<td>'.$v['email'].'</td>
			<td align="left">
				<img src="'.$theme['images'].'admin/user_'.$v['type'].'.gif" height="30" />
				<span class="usr_small">'.(empty($l['user_'.$v['type']])?'null':$l['user_'.$v['type']]).'</span>
				<a href="https://'.$globals['HTTP_HOST'].':4083/'.$SESS['token_key'].'/index.php?suid='.$k.'" target="_blank"><img src="'.$theme['images'].'admin/svs_login.gif" /></a>
			</td>
			<td align="center">'.(empty($v['numvps']) ? 0 : $v['numvps']).'</td>
			<td align="center">'.(empty($v['bw_used']) ? 0 : $v['bw_used']).' GB</td>';

			echo '<td align="center"><img src="'.$theme['images'].(($v['act_status'] != 1) ? "offline.png" : "online.png").'" title="'.(($v['act_status'] != 1) ? $l['status_inactive'] : $l['status_active']).'"></i></td>';

			echo '<td align="center"><i class="icon icon-ssl" style="font-size:20px;color:'.(empty($v['2fa_status']) ? '#FF0000' : '#80C343').';" title="'.(empty($v['2fa_status']) ? $l['2fa_status_off'] : $l['2fa_status_on']).'"></i></td>';			
			
			echo '<td width="20" valign="middle" align="center">
				<a href="'.$globals['ind'].'act=edituser&uid='.$k.'" title="'.$l['edit_usr'].'"><img src="'.$theme['images'].'admin/edit.png" /></a>
			</td>						
			<td width="20" valign="middle" align="center">
				'.(empty($v['suspended']) ? '<a href="javascript:void(0);" onclick="return action_chosen(\'suspend\', '.$k.');"  title="'.$l['suspend_user'].'"><img src="'.$theme['images'].'admin/suspend.png" /></a>' : '<a href="javascript:void(0);" onclick="return action_chosen(\'unsuspend\', '.$k.');"  title="'.$l['unsuspend_user'].'"><img src="'.$theme['images'].'admin/unsuspend.png" /></a>').'
			</td>
			<td width="20" valign="middle" align="center">
				<a href="javascript:void(0);" onclick="return action_chosen(\'delete\', '.$k.');"  title="'.$l['del_usr'].'"><img src="'.$theme['images'].'admin/delete.png" /></a>
			</td>
			<td width="20" valign="middle" align="center">
				<input type="checkbox" class="ios" name="user_list[]" value="'.$k.'"/>
			</td>
		</tr></tbody>';
	
	$i++;
}

echo '</table>

<div class="row bottom-menu">			
<div class="col-sm-7"></div>
<div class="col-sm-5"><label>'.$l['with_selected'].'</label>
<select name="user_task_select" class="form-control" id="user_task_select">
	<option value="0">---</option>
	<option value="delete">'.$l['ms_delete'].'</option>
	<option value="suspend">'.$l['ms_suspend'].'</option>
	<option value="unsuspend">'.$l['ms_unsuspend'].'</option>
	<option value="deactivate_2fa">'.$l['ms_deactivate_2fa'].'</option>
	<option value="activate_account">'.$l['activate'].'</option>
	<option value="deactivate_account">'.$l['deactivate'].'</option>
</select>&nbsp;
<button type="submit" class="go_btn" id="user_submit" name="user_submit" onclick="action_chosen(); return false;">Go</button></div>
</div>
</form>
<div id="progress_bar" style="height:125px; display:none">
	<br />
	<center class="col-sm-11">
		<font id="progress_txt" size="4" color="#222222">'.$l['action_msg'].'</font>
		<br>
		<br>
		<table id="table_progress" width="450" height="28" cellspacing="0" cellpadding="0" border="0" align="center" style="border:1px solid #CCC; -moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px;background-color:#efefef;">
			<tbody>
				<tr>
					<td id="progress_color" width="100%" style="background-image: url(themes/default/images/bar.gif); -moz-border-radius: 4px; -webkit-border-radius: 4px; border-radius: 4px;"></td>
					<td id="progress_nocolor"> </td>
				</tr>
			</tbody>
		</table>
		<br/>'.$l['notify_msg'].'
	</center>
</div>
';

}// End of else

page_links($globals['num_res'], $globals['cur_page'], $globals['reslen']);

echo '<center><div class="col-sm-11"><a href="'.$globals['ind'].'act=adduser"><button class="link_btn">'.$l['add_usr'].'</button></a></div></center><br/><br/>
<br/><br/>
<div class="notice" align="left" style="font-size:12px; line-height:150%"><img src="'.$theme['images'].'/notice.gif" align=left/> &nbsp; '.$l['clous_user_band_note'].'</div>

</div>';
softfooter();

}
?>