<?php

//////////////////////////////////////////////////////////////
//===========================================================
// letsencrypt_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function letsencrypt_theme(){
    
global $theme, $globals, $cluster, $l, $crt_details, $site_domain, $crt_buts, $crt_config_options, $crt_select_opts, $done, $error, $actid, $flag_task, $servers;

// If flag_task flag is set, then send back actid of the task
if($flag_task){
	echo 'actid = '.$actid;
	$flag_task = 0;
	return;
}

softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><img src="'.$theme['images'].'admin/le.png" /> &nbsp; <span style="float:right;" ><a href="'.$globals['docs'].'LetsEncrypt" target="_blank" class="wiki_help" title="'.$l['wiki_help'].'"><i class="icon-help" ></i></a></span></center>';

error_handle($error);
echo '<div id="ajax_err"></div>';

// Is it offline ?
$hypervisor_status = $cluster->statewise($globals['server']);
if($hypervisor_status == 0 || $hypervisor_status == 2){

	echo '<div class="e_notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['server_status_'.$hypervisor_status].'</div>';
	
}else{
	
	echo '<script>
		function load_form(frm){
			if(frm === undefined){
				return false;
			}
			$.each(frm, function(k, v){
				switch(v.type){
					case "text":
						$("#"+k).val(v.value);
						break;
					case "select":
						$("#"+v.value).prop("selected", true);
						break;
					case "checkbox":
						$("#"+k).prop("checked", v.value);
						break;
					default:
						break;
				}
			});
		}
			
		function show_form(){
		
			$("#processing").css("display", "block");
			$("#get_crt_id").css("background-color", "#2C3C69");
			';
			$config_contents = '';
			foreach($crt_config_options['crt_save_cfg_frm']['inputs'] as $k => $v){
				
				$config_contents .= '<div class="form-group" data-toggle="tooltip" data-placement="bottom" title="'.$l[$k.'_exp'].'"><div class="col-sm-5"><label class="control-label" for="'.$k.'">'.$l[$k].'</label><span class="help-block">'.$l[$k.'_exp'].'</span></div>';
				
				switch($v['type']){
					
					case 'text':
						$config_contents .= '<div class="col-sm-7"><input type="text" id="'.$k.'" name="'.$k.'" class="form-control" value="'.$v['value'].'"/></div>';
						break;
						
					case 'select':
						$config_contents .='<div class="col-sm-7" ><select id="'.$k.'" name="'.$k.'" class="form-control">';
						if(isset($crt_select_opts[$k])){
							foreach($crt_select_opts[$k] as $vv){
								$config_contents .= '<option id="'.$vv.'" name="'.$vv.'" value="'.$vv.'" '.($v['value'] == $vv ? 'selected="selected"' : '').'>'.$vv.'</option>';
							}
						}
						$config_contents .= '</select></div>';
						break;
						
					case 'checkbox':
						$config_contents .= '<div class="col-xs-7"><input type="checkbox" id="'.$k.'" name="'.$k.'" '.($v['value'] == true ? 'checked="checked"' : '').' /></div>';
						break;
						
					default:
						break;
				}
				
				$config_contents .= '</div>';
			}
			$config_contents .= '<div class="form-group"><div class="col-xs-12 text-center" ><button type="submit" id="save_cfg_id" class="btn btn-sm" onclick="process_req(\\\'save_cfg\\\');" >'.$l['proceed'].'</button></div><div class="col-xs-12"><div class="text-center" id="processing_form" style="display: none;"><img src="themes/default/images/ajax_remove.gif" /></div></div></div>';
			
			echo 'var content = \'<form id="crt_save_cfg_frm" name="crt_save_cfg_frm" class="form-horizontal" role="form" action="" method="POST" onsubmit="return false;">'.$config_contents.'</form>\';
					
			$.ajax({
				url: "'.$globals['index'].'act=letsencrypt&api=json",
				data: "",
				dataType : "json",
				type: "post",
				success: function(response){
					load_form(response.crt_config_options.crt_save_cfg_frm.inputs);
					$("#conf_modal").modal({keyboard: true});
					$("#conf_modal").modal({show:true});
					$("#processing").css("display", "none");
					$("#get_crt_id").css("background-color", "#00A0D2");
				}
			});
			
			$("#conf_modal_body").html(content);
		}
	
		$(document).ready(function(){
			$("[data-toggle=\"tooltip\"]").tooltip();
		});
	</script>
	
	<div class="modal fade" role="dialog" id="conf_modal">
		<div class="modal-dialog" style="width: 60%;">
			<div class="modal-content">
				<div class="modal-header text-center" id="conf_modal_head">
					<button class="close" data-dismiss="modal">&times;</button>
					<span class="fhead">'.$l['configuration'].'</span>
				</div>
				<div class="modal-body" id="conf_modal_body" ></div>
			</div>
		</div>
	</div>
	<div class="modal fade" role="dialog" id="logs_modal">
		<div class="modal-dialog" style="width: 60%; height: 50%;">
			<div class="modal-content">
				<div class="modal-header text-center" id="logs_modal_head">
					<button class="close" data-dismiss="modal">&times;</button>
					<span class="fhead">'.$l['logs'].'</span>
				</div>
				<div class="modal-body" id="logs_modal_body" ></div>
			</div>
		</div>
	</div>';
	echo '<div class="text-center" ><span id="site_domain" class="domain_header">'.(!empty($site_domain) ? $site_domain : '').'</span></div><br />';
	echo '
	<div class="row">
		<div class="col-sm-12 text-center">
			<div class="btn-group" id="btn-grp1">';
			foreach($crt_buts as $k => $v){
				echo '<button type="button" class="btn btn-primary btn-lg" id="'.$k.'_id" onclick="'.($k == 'get_crt' ? 'show_form()' : 'process_req(\''.$k.'\')').'" >'.$l[$v].'</button>';    
			}
			echo '
			</div>
		</div>
		<div class="text-center" id="processing" style="display: none;"><img src="themes/default/images/ajax_remove.gif" /></div>
	</div>
						
	<div id="progress_onload"></div><br />
	
	<table class="table table-hover" id="crt_details_tbl">
		<tr id="crt_details_tbl_hd">
			<th colspan=2>'.$l['le_crt_details'].'</th>
		</tr>
		<tr>
			<td style="width: 120px;" class="fhead">'.$l['server'].'</td>
			<td><span class="server_name" >'.$servers[$globals['server']]['server_name'].'</span></td>
		</tr>';
		foreach($crt_details as $k => $v){
			echo '
			<tr>
				<td style="width: 120px;" class="fhead">'.$l[$k].'</td>
				<td id= "'.$k.'" style="text-align: left;">'.$v.'</td>
			</tr>';
		}
	echo '</table>';
}

echo '</div>
	<script>
		var regexp = /^actid/;
		var actid = null;
		var progress = 0;
		var progress_update = "";
		var timer = null;
		var sel_act = "";
		
		$(document).ready(function(){
			progress_onload();';
			if(empty($site_domain)){
				echo '
					$("#site_domain").removeClass("domain_header");
					$("#site_domain").html("<b>'.$l['no_site_domain'].'</b>");
					$("#site_domain").addClass("no_domain");
				';
			}
		echo '	
		});
		
		function process_req(sel){
			
			var dos = ["get_crt", "renew_crt", "save_cfg", "show_logs"]; 
			$("#progress-cont").hide();
			
			if(dos.indexOf(sel) < 0){
				alert("'.$l['err_invalid_opt'].'");
				return;
			}
			
			// Disable buttons till certificat is being install
			for(x in dos){
				//alert(dos[x]);
				$("#"+dos[x]+"_id").attr("disabled", "disabled");
			}
			
			$("#"+sel+"_id").css("background-color", "#2C3C69");
			$("#processing").css("display", "block");
			var data = "";
			if(sel == "save_cfg"){
				$("#processing_form").css("display", "block");
				data = "opt=save_cfg&"+$("#crt_save_cfg_frm").serialize();
			}else if(sel == "show_logs"){
				data = "opt="+sel;
			}else{
				if(sel != "get_crt" && confirm("'.$l['confirm_process'].'") == false){
					for(x in dos){
						//alert(dos[x]);
						$("#"+dos[x]+"_id").removeAttr("disabled");
					}
					$("#"+sel+"_id").css("background-color", "#00A0D2");
					$("#processing").css("display", "none");
					return false;
				}
				progress = 0;
				data = "opt="+sel;
				sel_act = sel;
			}
			
			$.ajax({
				url: "'.$globals['index'].'act=letsencrypt"+((sel == "save_cfg" || sel == "show_logs") ? "&api=json" : "&jsnohf=1")'.(!empty($_REQUEST['debug']) ? '+"&debug=died"' : '').',
				data: data,
				type: "post",
				success: function(response){
					$("#processing").css("display", "none");
					$("#processing_form").css("display", "none");
					$("#"+sel+"_id").css("background-color", "#00A0D2");
					if(sel == "get_crt" || sel == "renew_crt"){
						$("#progress-cont").show();
						if(regexp.exec(response) != null){
							eval(response);
							setTimeout("get_progress(\'letsencrypt\')", 1000);
						}else{
							var error_box = $(response).find("#error_box"); 
							$("#ajax_err").html(error_box);
							$("#progress-cont").hide();
						}
					}else{
						response = JSON.parse(response);
						if(response.done){
							
							if(sel == "save_cfg"){
								
								for(var x in response.crt_details){
									$("#"+x).html(response.crt_details[x]);
								}
								
								load_form(response.crt_config_options.crt_save_cfg_frm.inputs);
								$("#conf_modal").modal("hide");
								$("#site_domain").html($("#primary_domain").val());
								$("#site_domain").removeClass("no_domain");
								$("#site_domain").addClass("domain_header");
								process_req("get_crt");
								
							}else if(sel == "show_logs"){
								$("#logs_modal_body").html("<pre style=\"max-height: 500px;\" >"+response.logs+"</pre>");
								$("#logs_modal").modal({keyboard: true});
								$("#logs_modal").modal({show:true});
								$("#show_logs_id").css("background-color", "#00A0D2");
							}
						}else if(response.error){
							var err = "";
							for(var x in response.error){
								err += response.error[x]+"\n"; 
							}
							alert(err);
						}
					}
					
					for(x in dos){
						//alert(dos[x]);
						$("#"+dos[x]+"_id").removeAttr("disabled");
					}
				}
			});
			if(sel == "get_crt" || sel == "renew_crt"){
				$("#error_box").remove();
				$("#pbar").html("'.$l['initializing'].' ( 0% )");			
			}
			$("#progressbar").progressbar({value: 0});
		}
		
		function donecert(){
		
			if(sel_act == "get_crt"){
				alert("'.lang_vars_name($l['done_req_crt'], array('req_crt' => $l['get_crt'])).'");
			}else if(sel_act == "renew_crt"){
				alert("'.lang_vars_name($l['done_req_crt'], array('req_crt' => $l['renew_crt'])).'");
			}
			$("#progress-cont").hide();';
			
			echo '
			$.ajax({
				url: "'.$globals['index'].'act=letsencrypt&api=json"'.(!empty($_REQUEST['debug']) ? '+"&debug=died"' : '').',
				type: "post",
				success: function(response){
					response = JSON.parse(response);
					if(response.crt_details){
						for(var x in response.crt_details){
							$("#"+x).html(response.crt_details[x]);
						}
					}
				}
			});';
			
			// If the request was for master server then reload the page.
			if($globals['server'] == 0){
				echo 'location.reload(true);';
			}
			
		echo '}

		$(window).resize(function(){
		   if($(window).width() < 750){
			   $("#btn-grp1 button").removeClass("btn-lg");
			   $("#btn-grp1 button").addClass("btn-sm");
		   }else{
			   $("#btn-grp1 button").removeClass("btn-sm");
			   $("#btn-grp1 button").addClass("btn-lg");
		   }
		   
		});
	</script>
'; 
softfooter();

}

?>