<?php

//////////////////////////////////////////////////////////////
//===========================================================
// import_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function import_default_theme(){

global $theme, $globals, $kernel, $user, $ippools, $l, $error, $oslist, $done, $vpsid;

softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-import icon-head"></i>&nbsp; '.$l['importvps'].'</center>';

error_handle($error);

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.lang_vars($l['done'], array($vpsid)).'</div>';
}

echo '<form accept-charset="'.$globals['charset'].'" name="importvps" method="post" action="" class="form-horizontal">

<table border="0" width="95%" align="center" class="class="table table-hover tablesorter"">
<tr>
	<td width="40%">
		<span class="fhead">'.$l['vpsname'].'</span><br />
		<span class="exp">'.$l['exp_vpsname'].'</span></td>
	<td>
		<input type="text" name="vpsname" id="vpsname" value="'.POSTval('vpsname', '').'" class="form-control" />
	</td>
</tr>
<tr>
	<td>
		<span class="fhead">'.$l['vpsbandwidth'].'</span><br />
		<span class="exp">'.$l['exp_vpsbandwidth'].'</span></td>
	<td>
		<input type="text" name="vpsbandwidth" id="vpsbandwidth" value="'.POSTval('vpsbandwidth', '').'" class="form-control"/>
	</td>
</tr>
</table>

<br />
<center><input type="submit" name="importvps" value="'.$l['submit'].'" class="btn"></center>
</form>
</div>';

softfooter();

}

// Import Virtuozzo / OpenVZ
function openvz_import_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $oslist, $done, $vpsid, $orphan, $users;

softheader($l['<title>']);

$sa = optREQ('sa');

$tmp_virt = 'OpenVZ';
if($sa == 'vzo'){
	$tmp_virt = 'Virtuozzo OpenVZ';
}

echo '
<div class="bg">
<center class="tit"><i class="icon icon-import icon-head"></i>&nbsp; '.$tmp_virt.' '.$l['importvps'].'<span style="float:right;" ><a href="'.$globals['docs'].'Importing_OpenVZ_VPS" target="_blank" class="wiki_help" title="'.$l['wiki_help'].'"><i class="icon-help" ></i></a></span></center>';

error_handle($error);

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done_openvz'].'</div>';
}else{

// Are there any orphans
if(empty($orphan)){

echo '<div class="e_notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['no_orphan'].'</div>';

}else{

echo '<form accept-charset="'.$globals['charset'].'" name="importvps" method="post" action="" class="form-horizontal">

<table border="0" width="95%" align="center" class="table table-hover tablesorter">
<tr>
	<th class="fhead">'.$l['vpsname'].'</th>
	<th class="fhead">'.$l['vpsbandwidth'].'</th>
	<th class="fhead">&nbsp;</th>
	<th class="fhead">'.$l['vpsuser'].'</th>
</tr>';

foreach($orphan as $k => $v){
	echo '<tr>
	<td width="20%">'.$v.'</td>
	<td><input type="text" name="vsbw_'.$v.'" size="8" value="'.POSTval('vsbw_'.$v, '0').'" class="form-control"/></td><td> GB</td>
	<td>
		<select name="vsuser_'.$v.'" class="form-control">
		<option value="">'.$l['none'].'</option>';
	
		foreach($users as $uk => $uv){
			echo '<option value="'.$uk.'">'.$uv['email'].'</option>';
		}
	
echo '</select>
	</td>
</tr>';
}

echo '</table>

<br />
<center><input type="submit" name="importvps" value="'.$l['submit'].'" class="btn"></center>
</form>';

}// End of IF !EMPTY Orphan

}
echo '</div>';
softfooter();

}

// Import KVM
function kvm_import_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $oslist, $done, $vpsid, $orphan, $users;

softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-import icon-head"></i>&nbsp; KVM '.$l['importvps'].'</center>';

error_handle($error);

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done_openvz'].'</div>';
}else{

// Are there any orphans
if(empty($orphan)){

echo '<div class="e_notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['no_orphan'].'</div>';

}else{

echo '<form accept-charset="'.$globals['charset'].'" name="importvps" method="post" action="" class="form-horizontal">

<table border="0" width="95%" align="center" class="table table-hover tablesorter">
<tr>
	<th class="fhead">'.$l['vpsname'].'</th>
	<th class="fhead">'.$l['vpsbandwidth'].'</th>
	<th>&nbsp;</th>
	<th class="fhead">'.$l['vpsuser'].'</th>
</tr>';

foreach($orphan as $k => $v){
	echo '<tr>
	<td>'.$v.'</td>
	<td><input type="text" name="vsbw_'.$v.'" size="8" value="'.POSTval('vsbw_'.$v, '0').'" class="form-control"/></td><td> GB</td>
	<td>
		<select name="vsuser_'.$v.'" class="form-control">
		<option value="">'.$l['none'].'</option>';
	
		foreach($users as $uk => $uv){
			echo '<option value="'.$uk.'">'.$uv['email'].'</option>';
		}
	
echo '</select>
	</td>
</tr>';
}

echo '</table>

<br />
<center><input type="submit" name="importvps" value="'.$l['submit'].'" class="btn"></center>
</form>';

}// End of IF !EMPTY Orphan

}
echo '<br /></div>';
softfooter();

}

// Import xen
function xen_import_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $oslist, $done, $vpsid, $orphan, $users;

softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-import icon-head"></i>&nbsp; Xen '.$l['importvps'].'</center>';

error_handle($error);

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done_openvz'].'</div>';
}else{

// Are there any orphans
if(empty($orphan)){

echo '<div class="e_notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['no_orphan'].'</div>';

}else{

echo '<form accept-charset="'.$globals['charset'].'" name="importvps" method="post" action="" class="form-horizontal">

<table border="0" width="95%" align="center" class="table table-hover tablesorter">
<tr>
	<th class="fhead">'.$l['vpsname'].'</th>
	<th class="fhead">'.$l['vpsbandwidth'].'</th>
	<th>&nbsp;</th>
	<th class="fhead">'.$l['vpsuser'].'</th>
</tr>';

foreach($orphan as $k => $v){
	echo '<tr>
	<td>'.$v.'</td>
	<td><input type="text" name="vsbw_'.$v.'" size="8" value="'.POSTval('vsbw_'.$v, '0').'" class="form-control"/></td><td> GB</td>
	<td>
		<select name="vsuser_'.$v.'" class="form-control">
		<option value="">'.$l['none'].'</option>';
	
		foreach($users as $uk => $uv){
			echo '<option value="'.$uk.'">'.$uv['email'].'</option>';
		}
	
echo '</select>
	</td>
</tr>';
}

echo '</table>

<br />
<center><input type="submit" name="importvps" value="'.$l['submit'].'" class="btn" ></center>
</form>';

}// End of IF !EMPTY Orphan

}
echo '<br /></div>';
softfooter();

}

// Import xen
function xcp_import_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $oslist, $done, $vpsid, $orphan, $users;

softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-import icon-head"></i>&nbsp; XenServer '.$l['importvps'].'</center>';

error_handle($error);

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done_openvz'].'</div>';
}else{

// Are there any orphans
if(empty($orphan)){

echo '<div class="e_notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['no_orphan'].'</div>';

}else{

echo '<form accept-charset="'.$globals['charset'].'" name="importvps" method="post" action="" class="form-horizontal">

<table border="0" width="95%" align="center" class="table table-hover tablesorter">
<tr>
	<th class="fhead">'.$l['vpsname'].'</th>
	<th class="fhead">'.$l['vpsbandwidth'].'</th>
	<th>&nbsp;</th>
	<th class="fhead">'.$l['vpsuser'].'</th>
</tr>';

foreach($orphan as $k => $v){
	echo '<tr>
	<td>'.$v.'</td>
	<td><input type="text" name="vsbw_'.$v.'" size="8" value="'.POSTval('vsbw_'.$v, '0').'" class="form-control"/></td><td> GB</td>
	<td>
		<select name="vsuser_'.$v.'" class="form-control">
		<option value="">'.$l['none'].'</option>';
	
		foreach($users as $uk => $uv){
			echo '<option value="'.$uk.'">'.$uv['email'].'</option>';
		}
	
echo '</select>
	</td>
</tr>';
}

echo '</table>

<br />
<center><input type="submit" name="importvps" value="'.$l['submit'].'" class="btn" ></center>
</form>';

}// End of IF !EMPTY Orphan

}
echo '<br /></div>';
softfooter();

}

function  hypervm_import_theme(){
	global $theme, $globals, $user, $l, $error, $done;
	
	softheader($l['<title>']);

	echo '
<div class="bg">
	<center class="tit"><i class="icon icon-import icon-head"></i>&nbsp; '.$l['hypervm_import'].'</center>';
	
	error_handle($error);

	echo '
<table cellspacing="0" cellpadding="8" border="0" width="400" align="center">
<tr>
<td>
	<a href="'.$globals['ind'].'act=import&sa=hypervm&ta=nodes" class="solus_opt"><img src="'.$theme['images'].'admin/servers.gif"> &nbsp;'.$l['hypervm_nodes_link'].'</a><br>
	<a href="'.$globals['ind'].'act=import&sa=hypervm&ta=plans" class="solus_opt"><img src="'.$theme['images'].'admin/plans.gif"> &nbsp;'.$l['hypervm_plans_link'].'</a><br>
	<a href="'.$globals['ind'].'act=import&sa=hypervm&ta=users" class="solus_opt"><img src="'.$theme['images'].'admin/users.gif"> &nbsp;'.$l['hypervm_users_link'].'</a><br>
	<a href="'.$globals['ind'].'act=import&sa=hypervm&ta=ips" class="solus_opt"><img src="'.$theme['images'].'admin/ippool.gif"> &nbsp;'.$l['hypervm_ips_link'].'</a><br>
	<a href="'.$globals['ind'].'act=import&sa=hypervm&ta=os" class="solus_opt"><img src="'.$theme['images'].'admin/ostemplates.gif"> &nbsp;'.$l['hypervm_os_link'].'</a><br>
	<a href="'.$globals['ind'].'act=import&sa=hypervm&ta=vps" class="solus_opt"><img src="'.$theme['images'].'admin/vs.gif"> &nbsp;'.$l['hypervm_vps_link'].'</a>
</td>
</tr>
</table>
	<br><br>';
	
	echo '<center><span class="notice">'.$l['hypervm_import_note'].'</span></center><br /></div>';

	softfooter();

}

// Import HyperVm Nodes
function hypervm_nodes_import_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $servers, $oslist, $done, $vpsid, $orphan, $users, $hypervm_nodes, $virt_hypervm_servers;
	
	softheader($l['<title>']);

	echo '
	<div class="bg">
	<center class="tit"><i class="icon icon-import icon-head"></i>&nbsp; '.$l['hypervm_nodes'].'</center>';
	
	error_handle($error);
	
	if(!empty($done)){
		echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done_hypervm_nodes'].'</div>';
	}else{
			
		echo '<table border="0" width="95%" align="center" class="table table-hover tablesorter">
		<tr>
			<th class="fhead">'.$l['hypervm_nodes_id'].'</th>
			<th class="fhead">'.$l['hypervm_nodes_name'].' </th>
			<th class="fhead">'.$l['hypervm_nodes_ip'].' </th>
			<th class="fhead">'.$l['hypervm_nodes_virt'].'</th>
		</tr>';	
				
		foreach($hypervm_nodes as $k => $v){

			echo '<tr '.(isset($virt_hypervm_servers[$k]) ? 'style="background-color: #C4FDB5"' : '').'>
			<td>'.$k.'</td>
			<td>'.$k.'</td>
			<td>'.$v['ipaddr'].'</td>
			<td>'.(!isset($virt_hypervm_servers[$k]) ? '--' : $servers[$virt_hypervm_servers[$k]]['server_name'].' (ID : '.$virt_hypervm_servers[$k].')').'</td>
			</tr>';
		}
		
		echo '</table>
		<br /><br />
		<center><a href="'.$globals['index'].'act=import&sa=hypervm" class="link_btn">'.$l['hypervm_back_to_wizard'].'</a></center>
		<br /><br />
		<center><span style="background-color: #C4FDB5; padding:8px;">'.$l['hypervm_green_nodes'].'</span></center>';

	}
echo '<br /></div>';
softfooter();

}


// Import HyperVm Plans
function hypervm_plans_import_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $servers, $done, $v_plans, $v_plans_matches, $hypervm_plans;
	
	softheader($l['<title>']);

	echo '
	<div class="bg">
	<center class="tit"><i class="icon icon-import icon-head"></i>&nbsp; '.$l['hypervm_plans'].'</center>';
	
	error_handle($error);
	
	if(!empty($done)){
		echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done_hypervm_plans'].'</div>';
	}else{
		
		// Show the list to be imported
		if(!empty($hypervm_plans)){
			
			echo '<form accept-charset="'.$globals['charset'].'" name="importplans" method="post" action="" class="form-horizontal">
			
			<table border="0" width="95%" align="center" class="table table-hover tablesorter">
			<tr>
				<th class="fhead" width="10%">'.$l['hypervm_plan_id'].'</th>
				<th class="fhead">'.$l['hypervm_plan_name'].' </th>
				<th class="fhead">'.$l['hypervm_plan_ram'].' </th>
				<th class="fhead">'.$l['hypervm_plan_disk'].' </th>
				<th class="fhead">'.$l['hypervm_plan_bandwidth'].' </th>
				<th class="fhead">'.$l['hypervm_plan_virt'].' </th>
			</tr>';
			
			foreach($hypervm_plans as $k => $v){
				
				$plid = -1;
				
				// Is there a matching Plan ?
				if(isset($v_plans_matches[$v['name']])){
					$plid = $v_plans_matches[$v['name']];
				}
				
				echo '<tr '.($plid == -1 ? 'style="background-color: #C4FDB5"' : '').'>
					<td>'.$v['nname'].'</td>
					<td>'.$v['name'].'</td>
					<td>'.$v['ram'].' MB</td>
					<td>'.$v['space'].' GB</td>
					<td>'.$v['bandwidth'].' GB</td>
					<td>'.($plid == -1 ? '--' : $v_plans[$plid]['name'].' (ID : '.$plid.')').'</td>
				</tr>';
				
			}

			echo '
			</table>
			<br /><br />
			<center><input type="submit" name="hypervm_plans" value="'.$l['submit'].'" class="btn"></center>
			</form>
			<br /><br />
			<center><a href="'.$globals['index'].'act=import&sa=hypervm" class="link_btn">'.$l['hypervm_back_to_wizard'].'</a></center>
			<br /><br />
			<center><span style="background-color: #C4FDB5; padding:8px;">'.$l['hypervm_green_plans'].'</span></center>';
	
		// There are no users
		}else{
			echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['solus_no_ips'].'</div>';
		}

	}
echo '<br /></div>';
softfooter();

}


// Import HyperVm users
function hypervm_users_import_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $oslist, $done, $vpsid, $orphan, $users, $hypervm_users, $u_hyper_users, $v_users, $inserted, $info, $no_users;
	
	softheader($l['<title>']);

	echo '
	<div class="bg">
	<center class="tit"><i class="icon icon-import icon-head"></i>&nbsp; HyperVm '.$l['importusers'].'</center>';
	
	error_handle($error);
	
	if(!empty($done)){
		echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done_hypervm_users'].'</div>';
	}else{
		
		// Show the list to be imported
		if(!empty($hypervm_users)){
			
			echo '<form accept-charset="'.$globals['charset'].'" name="importvps" method="post" action="" class="form-horizontal">
			
			<table border="0" width="95%" align="center" class="table table-hover tablesorter">
			<tr>
				<th class="fhead">'.$l['vpsuser'].'</th>
				<th class="fhead">'.$l['email'].' </th>
				<th class="fhead">'.$l['type_user'].' </th>
				<th class="fhead">'. $l['import_no'] .'</th>
			</tr>';
			
			foreach($hypervm_users as $k => $v){
					if(!empty($v['parent_clname'])){
						$parent = explode("-", $v['parent_clname']);
					}
				echo '<tr>
					<td width="40%">'.$v['nname'].'</td>
					<td>'.(empty($v['contactemail']) ? '---' : $v['contactemail']).' </td>
					<td>'.$l['hypervm_user_type_0'].' </td>
					<td>'. (empty($v['contactemail']) ? '<font color="red">'.$l['no'].'</font>'. '<br />' .$l['reason'] .': ' .$l['empty_email'] : (in_array($v['contactemail'], $v_users) ? '<font color="red">'.$l['no'] .'</font>'.'<br />' .$l['reason'] . ': ' .$l['users_exists']  : $e = '<font color="green">'.$l['yes'] .'</font>' )) . '
					
					</td>
				</tr>';
				
			}
			
			/*if(isset($e)){
	
				echo '<tr>
						<td>
							'.$l['send_email'].'
						</td>
						<td>
							<input type="checkbox" name="send_email">
						</td>
					</tr>';
			}*/

			echo '
			</table>
			
			<br /><br />
			<center><input type="submit" name="hypervm_users" value="'.$l['submit'].'" class="btn" '.(!isset($e) ? 'disabled="1"' : '' ).'></center>
			</form>
			<br /><br />
			<center><a href="'.$globals['index'].'act=import&sa=hypervm" class="link_btn">'.$l['hypervm_back_to_wizard'].'</a></center>';
	
		// There are no users
		}else{
			echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['no_users'].'</div>';
		}

	}
echo '<br /></div>';
softfooter();

}

// Import HyperVM Ip Blocks
function hypervm_ips_import_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $servers, $oslist, $done, $vpsid, $orphan, $hypervm_nodes, $virt_hypervm_servers, $v_ippools, $v_ipmatches, $hypervm_ipblocks, $hypervm_ipblocknodes;

	softheader($l['<title>']);

	echo '
<div class="bg">
	<center class="tit"><i class="icon icon-import icon-head"></i>&nbsp; HyperVm '.$l['hypervm_import_ips'].'</center>';
	
	error_handle($error);
	
	if(!empty($done)){
		echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done_hypervm_ips'].'</div>';
	}else{
		
		// Show the list to be imported
		if(!empty($hypervm_ipblocks)){
			
			echo '<form accept-charset="'.$globals['charset'].'" name="importips" method="post" action="" class="form-horizontal">
			
			<table border="0" width="95%" align="center" class="table table-hover tablesorter">
			<tr>
				<th class="fhead" width="10">ID</th>
				<th class="fhead" width="10%">'.$l['hypervm_ips_name'].'</th>
				<th class="fhead">'.$l['hypervm_ips_type'].' </th>
				<th class="fhead">'.$l['hypervm_ips_gw'].' </th>
				<th class="fhead">'.$l['hypervm_ips_netmask'].' </th>
				<th class="fhead">'.$l['hypervm_ips_servers'].' </th>
			</tr>';
			
			foreach($hypervm_ipblocks as $k => $v){
						
				// Is there a matching IP pool ?
				$ippid = @$v_ipmatches[$v['nname'].'_'.$v['networkgateway'].'/'.$v['networknetmask']];
				
				echo '<tr>
					<td>'.$v['noid'].'</td>
					<td '.(empty($ippid) ? 'style="background-color: #C4FDB5"' : '').'>'.$v['nname'].'</td>
					<td>'. 'IPv4' .'</td>
					<td>'.$v['networkgateway'].'</td>
					<td>'.$v['networknetmask'].'</td>
					<td>';
					
					// List the Block Nodes
					if(!empty($hypervm_ipblocknodes[$k])){
						
						echo '<table border="0" width="95%" align="center" class="table table-hover tablesorter">
						<tr>
						<th>HyperVm Node</th>
						<th>Virtualizor Server</th>
						</tr>';
						
						foreach($hypervm_ipblocknodes[$k] as $nk => $nv){
							echo '<tr '.(empty($v_ippools[$ippid]['servers'][$virt_hypervm_servers[$nk]]) ? ' style="background-color: #C4FDB5"' : '').'>
							<td>'.$hypervm_nodes[$nk]['nname'].'</td>
							<td>'.(empty($servers[$virt_hypervm_servers[$nk]]['server_name']) ? '--' : $servers[$virt_hypervm_servers[$nk]]['server_name']).'</td>
							</tr>';
						}
						
						echo '</table>';
					}
					
					echo '</td>
				</tr>';
				
			}

			echo '
			</table>
			
			<br /><br />
			<center><input type="submit" name="hypervm_ips" value="'.$l['submit'].'" class="btn"></center>
			</form>
			<br /><br />
			<center><a href="'.$globals['index'].'act=import&sa=hypervm" class="link_btn">'.$l['hypervm_back_to_wizard'].'</a></center>
			<br /><br />
			<center><span style="background-color: #C4FDB5; padding:8px;">'.$l['solus_green_ips'].'</span></center>';
	
		// There are no IPs
		}else{
			echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['solus_no_ips'].'</div>';
		}

	}
echo '<br /></div>';
softfooter();

}

// Import HyperVm Templates
function hypervm_os_import_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $servers, $done, $ostemplates, $oslist, $os_map, $hypervm_templates;
	
	softheader($l['<title>']);

	echo '
	<div class="bg">
	<center class="tit"><i class="icon icon-import icon-head"></i>&nbsp; '.$l['hypervm_os'].'</center>';
	
	error_handle($error);
	
	if(!empty($done)){
		echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done_hypervm_os'].'</div>';
	}else{
		
		// Show the list to be imported
		if(!empty($hypervm_templates)){
			
			echo '<form accept-charset="'.$globals['charset'].'" name="importos" method="post" action="" class="form-horizontal">
			
			<table border="0" width="95%" align="center" class="table table-hover tablesorter">
			<tr>
				<th class="fhead" width="10%">'.$l['hypervm_os_name'].'</th>
				<th class="fhead">'.$l['hypervm_os_filename'].' </th>
				<th class="fhead">'.$l['hypervm_os_type'].' </th>
				<th class="fhead">'.$l['hypervm_os_virt'].' </th>
			</tr>';
			
			foreach($hypervm_templates as $type => $type_v){
				
				foreach($type_v as $k => $v){
				
					echo '<tr '.(empty($os_map[$v['ttype']][$v['_filename']]) ? 'style="background-color: #C4FDB5"' : '').'>
					<td>'.$v['ostemplate'].'</td>
					<td>'.$v['_filename'].'</td>
					<td>'.ucfirst($v['ttype']).'</td>
					<td>';
					
					if($v['ttype'] == 'openvz' || $v['ttype'] == 'xen'){
						echo (empty($os_map[$v['ttype']][$v['_filename']]) ? '--' : $ostemplates[ $os_map[$v['ttype']][$v['_filename']] ]['name'].' (ID : '.$os_map[$v['ttype']][$v['_filename']].')');
					}else{
						
						echo '<select name="'.$v['ttype'].'_'.$v['ostemplate'].'" class="form-control">
						<option value="0">Select</option>';
						
						foreach($ostemplates as $ok => $ov){
							
							$ov['type'] = ($ov['type'] == 'xen' && !empty($ov['hvm'])) ? 'xenhvm' : $ov['type'];
							
							if($ov['type'] != $v['type']){
								continue;
							}
							
							echo '<option value="'.$ok.'" '.($os_map[$v['ttype']][$v['_filename']] == $ok ? 'selected="selected"' : '').'>'.$ov['name'].'</option>';
							
						}
						
						echo '</select>';
						
					}
					
					echo '</td>
				</tr>';
				
				}
				
			}

			echo '
			</table>
			
			<br /><br />
			<center><input type="submit" name="hypervm_os" value="'.$l['submit'].'" class="btn"></center>
			</form>
			<br /><br />
			<center><a href="'.$globals['index'].'act=import&sa=hypervm" class="link_btn">'.$l['hypervm_back_to_wizard'].'</a></center>
			<br /><br />
			<center><span style="background-color: #C4FDB5; padding:8px;">'.$l['hypervm_green_os'].'</span></center>
			<br /><br />
			<center><span class="notice">'.$l['hypervm_os_used_only'].'</span></center>';
	
		// There are no users
		}else{
			echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['hypervm_no_os'].'</div>';
		}

	}
echo '<br /></div>';
softfooter();

}

// Import HyperVm VPS
function hypervm_vps_import_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $servers, $done, $v_vps, $v_vps_matches, $hypervm_vps, $hypervm_users;
	
	softheader($l['<title>']);

	echo '
	<div class="bg">
	<center class="tit"><i class="icon icon-import icon-head"></i>&nbsp; '.$l['hypervm_vps'].'</center>';
	
	error_handle($error);
	
	if(!empty($done)){
		echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done_hypervm_vps'].'</div>';
	}else{
		
		// Show the list to be imported
		if(!empty($hypervm_vps)){
			
			echo '<form accept-charset="'.$globals['charset'].'" name="importos" method="post" action="" class="form-horizontal">
			
			<table border="0" width="95%" align="center" class="table table-hover tablesorter">
			<tr>
				<th class="fhead" width="10%">'.$l['hypervm_vps_id'].'</th>
				<th class="fhead">'.$l['hypervm_vps_name'].' </th>
				<th class="fhead">'.$l['hypervm_vps_hostname'].' </th>
				<th class="fhead">'.$l['hypervm_vps_type'].' </th>
				<th class="fhead">'.$l['hypervm_vps_user'].' </th>
				<th class="fhead">'.$l['hypervm_vps_virt'].' </th>
			</tr>';
			
			foreach($hypervm_vps as $k => $v){
				
				$vpsid = empty($v_vps_matches[$v['nname']]) ? 0 : $v_vps_matches[$v['nname']];
				$name = explode("-", $v['parent_clname']);
				echo '<tr '.(empty($vpsid) ? 'style="background-color: #C4FDB5"' : '').'>
					<td>'.$v['nname'].'</td>
					<td>'.$v['iid'].'</td>
					<td>'.$v['hostname'].'</td>
					<td>'.ucfirst($v['ttype']).'</td>
					<td>'.$hypervm_users[$name[1]]['contactemail'].'</td>
					<td>'.(empty($vpsid) ? '--' : $v_vps[$v_vps_matches[$v['iid']]]['vpsid'].' (ID : '.$v_vps_matches[$v['iid']].')').'</td>
				</tr>';
				
			}

			echo '
			</table>
			
			<br /><br />
			<center><input type="submit" name="hypervm_vps" value="'.$l['submit'].'" class="btn"></center>
			</form>
			<br /><br />
			<center><a href="'.$globals['index'].'act=import&sa=hypervm" class="link_btn">'.$l['solus_back_to_wizard'].'</a></center>
			<br /><br />
			<center><span style="background-color: #C4FDB5; padding:8px;">'.$l['hypervm_green_vps'].'</span></center>
			<br /><br />
			<center><span class="notice">'.$l['hypervm_vps_server'].'</span></center>';
	
		// There are no users
		}else{
			echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['hypervm_no_vps'].'</div>';
		}

	}
echo '<br /></div>';
softfooter();


}


// Redundant previous code
function hypervm_import_theme_copy(){
	
	global $theme, $globals, $kernel, $user, $l, $error, $oslist, $done, $vpsid, $orphan, $users, $hypervm_users, $v_users, $inserted, $info, $db1, $row0;
	
	softheader($l['<title>']);

	echo '
	<div class="bg">
	<center class="tit"><i class="icon icon-import icon-head"></i>&nbsp; HyperVM '.$l['importusers'].'</center>';
	
	error_handle($error);
	
	if(!empty($done)){
		echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done_openvz'].'</div>';
	}else{
		if(!empty($inserted) && !empty($info))
		{
			$str = '';
			foreach($info as $k => $v)
			{
				$str .= $v . ' ';
			}
			$str = substr($str, 0, -2);
			
			echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$str . $l['import_ut'] .' <a href="index.php?act=import">'.$l['here'].'</a></div>';
		
		}
		else if(!empty($hypervm_users))
		{			
			echo '<form accept-charset="'.$globals['charset'].'" name="importvps" method="post" action="" class="form-horizontal">
			
			<table border="0" width="95%" align="center" class="table table-hover tablesorter">
			<tr>
				<th class="fhead">'.$l['vpsuser'].'</th>
				<th class="fhead">'.$l['email'].' </th>
				<th class="fhead">'. $l['import_no'] .'</th>
			</tr>';
			
			foreach($hypervm_users as $k => $v)
			{
				echo '<tr>
					<td width="40%">'.$k.'</td>
					<td>'. (empty($v['contactemail']) ? '---' : $v['contactemail'] ) .' </td>
					<td>'. (empty($v['contactemail']) ? '<font color="red">'.$l['no'] .'</font><br />' .$l['reason'] .': ' .$l['empty_email'] : (in_array($v['contactemail'], $v_users) ? '<font color="red">'.$l['no'] .'</font><br />' .$l['reason'] .': ' .$l['users_exists'] :  $e = '<font color="green">'.$l['yes'] .'</font>' )) . '
					</td>
				</tr>';
			}
			
			if(isset($e))
			{
				echo '<tr>
						<td>
						'.$l['send_email'].'
						</td>
						<td><input type="checkbox" name="send_email" class="ios">
						</td>
					</tr>';
			}
			
		echo '
		</table>
		
		<br /><br />
		<center><input type="submit" name="hypvm_users" value="'.$l['submit'].'" class="btn" '.(!isset($e) ? ' disabled="1"' : '' ). '></center>
		</form>';
	
		}
		else if(!empty($no_users))
		{
			echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['no_users'].'</div>';
		}
		
		
	} // else ends
echo '<br /></div>';
softfooter();

}


function veportal_import_theme(){

	global $theme, $globals, $kernel, $user, $l, $error, $oslist, $done, $vpsid, $orphan, $users, $veportal_users, $u_vep_users, $v_users, $inserted, $info, $no_users;
	
	softheader($l['<title>']);

	echo '
	<div class="bg">
	<center class="tit"><i class="icon icon-import icon-head"></i>&nbsp; VePortal '.$l['importusers'].'</center>';
	
	error_handle($error);
	
	if(!empty($done))
	{
		echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done_openvz'].'</div>';
	}
	else
	{
		if(!empty($inserted) && !empty($info))
		{
			$str = '';
			foreach($info as $k => $v)
			{
				$str .= $v . ' ';
			}
			$str = substr($str, 0, -2);
			
			echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$str . $l['import_ut'].' <a href="index.php?act=import">'.$l['here'].'</a></div>';
		
		}
		else if(!empty($veportal_users))
		{
			echo '<form accept-charset="'.$globals['charset'].'" name="importvps" method="post" action="" class="form-horizontal">
			
			<table border="0" width="95%" align="center" class="table table-hover tablesorter">
			<tr>
				<th class="fhead">'.$l['vpsuser'].'</th>
				<th class="fhead">'.$l['email'].' </th>
				<th class="fhead">'. $l['import_no'] .'</th>
			</tr>';
			
			foreach($veportal_users as $k => $v)
			{
				echo '<tr>
				<td width="40%">'.$k.'</td>
					<td>'.(empty($v['email_address']) ? '---' : $v['email_address']).' </td>
					<td>'. (empty($v['email_address']) ? '<font color="red">'.$l['no'].'</font>'. '<br />' .$l['reason'] .': ' .$l['empty_email'] : (in_array($v['email_address'], $v_users) ? '<font color="red">'.$l['no'] .'</font>'.'<br />' .$l['reason'] .': ' .$l['users_exists']  : $e = '<font color="green">'.$l['yes'] .'</font>' )) . 
					
					'</td>
				</tr>';
			}
		
			if(isset($e))
			{
				echo '<tr>
						<td>
							'.$l['send_email'].'
						</td>
						<td>
							<input type="checkbox" name="send_email" class="ios">
						</td>
					</tr>';
			}
		
		echo '
		</table>
		
		<br /><br />
		<center><input type="submit" name="veportal_users" value="'.$l['submit'].'" class="btn" '.(!isset($e) ? ' disabled="1"' : '' ). '></center>
		</form>';
	
		}
		else if(!empty($no_users))
		{
			echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['no_users'].'</div>';
		}
		
//}// End of IF !EMPTY Orphan

	}
echo '<br /></div>';
softfooter();

}

// The default theme to show the options
function solusvm_import_theme(){

global $theme, $globals, $user, $l, $error, $done;
	
	softheader($l['<title>']);

	echo '
	<div class="bg">
	<center class="tit"><i class="icon icon-import icon-head"></i>&nbsp; '.$l['solusvm_import'].'</center>';
	
	error_handle($error);

	echo '
<table cellspacing="0" cellpadding="8" border="0" width="400" align="center">
<tr>
<td>
	<a href="'.$globals['ind'].'act=import&sa=solusvm&ta=nodes" class="solus_opt"><img src="'.$theme['images'].'admin/servers.gif"> &nbsp;'.$l['solus_nodes_link'].'</a><br>
	<a href="'.$globals['ind'].'act=import&sa=solusvm&ta=nodegroups" class="solus_opt"><img src="'.$theme['images'].'admin/servers.gif"> &nbsp;'.$l['solus_ng_link'].'</a><br>
	<a href="'.$globals['ind'].'act=import&sa=solusvm&ta=plans" class="solus_opt"><img src="'.$theme['images'].'admin/plans.gif"> &nbsp;'.$l['solus_plans_link'].'</a><br>
	<a href="'.$globals['ind'].'act=import&sa=solusvm&ta=users" class="solus_opt"><img src="'.$theme['images'].'admin/users.gif"> &nbsp;'.$l['solus_users_link'].'</a><br>
	<a href="'.$globals['ind'].'act=import&sa=solusvm&ta=ips" class="solus_opt"><img src="'.$theme['images'].'admin/ippool.gif"> &nbsp;'.$l['solus_ips_link'].'</a><br>
	<a href="'.$globals['ind'].'act=import&sa=solusvm&ta=os" class="solus_opt"><img src="'.$theme['images'].'admin/ostemplates.gif"> &nbsp;'.$l['solus_os_link'].'</a><br>
	<a href="'.$globals['ind'].'act=import&sa=solusvm&ta=vps" class="solus_opt"><img src="'.$theme['images'].'admin/vs.gif"> &nbsp;'.$l['solus_vps_link'].'</a>
</td>
</tr>
</table>
<br><br><br>
<center><span class="notice">'.$l['solus_import_note'].'</span></center><br><br></div>';

	softfooter();

}

// Import SolusVM Nodes
function solusvm_nodes_import_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $servers, $oslist, $done, $vpsid, $orphan, $users, $solusvm_nodes, $virt_solus_servers;
	
	softheader($l['<title>']);

	echo '
	<div class="bg">
	<center class="tit"><i class="icon icon-import icon-head"></i>&nbsp; '.$l['solus_nodes'].'</center>';
	
	error_handle($error);
	
	if(!empty($done)){
		echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done_solusvm_nodes'].'</div>';
	}else{
			
		echo '<table border="0" width="95%" align="center" class="table table-hover tablesorter">
		<tr>
			<th class="fhead">'.$l['solus_nodes_id'].'</th>
			<th class="fhead">'.$l['solus_nodes_name'].' </th>
			<th class="fhead">'.$l['solus_nodes_ip'].' </th>
			<th class="fhead">'.$l['solus_nodes_virt'].'</th>
		</tr>';	
						
		foreach($solusvm_nodes as $k => $v){
			echo '<tr '.(isset($virt_solus_servers[$k]) ? 'style="background-color: #C4FDB5"' : '').'>
			<td>'.$v['nodeid'].'</td>
			<td>'.$v['name'].'</td>
			<td>'.$v['ip'].'</td>
			<td>'.(!isset($virt_solus_servers[$k]) ? '--' : $servers[$virt_solus_servers[$k]]['server_name'].' (ID : '.$virt_solus_servers[$k].')').'</td>
			</tr>';
		}
		
		echo '</table>
			
		<br /><br />
		<center><a href="'.$globals['index'].'act=import&sa=solusvm" class="link_btn">'.$l['solus_back_to_wizard'].'</a></center>
		<br /><br />
		<center><span style="background-color: #C4FDB5; padding:8px;">'.$l['solus_green_nodes'].'</span></center><br />';

	}
echo '</div>';
softfooter();

}

// Import SolusVM Node Groups
function solusvm_nodegroups_import_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $servers, $done, $v_server_groups, $v_sg_matches, $solusvm_nodes, $solusvm_nodegroups;
	
	softheader($l['<title>']);

	echo '
	<div class="bg">
	<center class="tit"><i class="icon icon-import icon-head"></i>&nbsp; '.$l['solus_nodegroups'].'</center>';
	
	error_handle($error);
	
	if(!empty($done)){
		echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done_solusvm_nodegroups'].'</div>';
	}else{
		
		// Show the list to be imported
		if(!empty($solusvm_nodegroups)){
			
			echo '<form accept-charset="'.$globals['charset'].'" name="importnodegroups" method="post" action="" class="form-horizontal">
			
			<table border="0" width="95%" align="center" class="table table-hover tablesorter">
			<tr>
				<th class="fhead" width="10%">'.$l['solus_ips_name'].'</th>
				<th class="fhead">'.$l['solus_ng_id'].' </th>
				<th class="fhead">'.$l['solus_ng_name'].' </th>
				<th class="fhead">'.$l['solus_ng_nodes'].' </th>
				<th class="fhead">'.$l['solus_ng_virt'].' </th>
			</tr>';
			
			foreach($solusvm_nodegroups as $k => $v){
				
				$sgid = -1;
				
				// Is there a matching Group ?
				if(isset($v_sg_matches[$v['name']])){
					$sgid = $v_sg_matches[$v['name']];
				}
				
				echo '<tr>
					<td '.($sgid == -1 ? 'style="background-color: #C4FDB5"' : '').'>'.$v['name'].'</td>
					<td>'.$v['id'].'</td>
					<td>'.$v['name'].'</td>
					<td>';
					$nodes = array();
					foreach($solusvm_nodes as $nk => $nv){
						if($k == $nv['groupid']){
							$nodes[$nk] = $nv['name'];
						}
					}
					
					echo implode(', ', $nodes).'</td>
					<td>'.($sgid == -1 ? '--' : $v_server_groups[$sgid]['sg_name'].' (ID : '.$sgid.')').'</td>
				</tr>';
				
			}

			echo '
			</table>
			
			<br /><br />
			<center><input type="submit" name="solusvm_ng" value="'.$l['submit'].'" class="btn"></center>
			</form>
			<br /><br />
			<center><a href="'.$globals['index'].'act=import&sa=solusvm" class="link_btn">'.$l['solus_back_to_wizard'].'</a></center>
			<br /><br />
			<center><span style="background-color: #C4FDB5; padding:8px;">'.$l['solus_green_ng'].'</span></center>';
	
		// There are no users
		}else{
			echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['solus_no_ips'].'</div>';
		}

	}
echo '<br /></div>';
softfooter();

}

// Import SolusVM Plans
function solusvm_plans_import_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $servers, $done, $v_plans, $v_plans_matches, $solusvm_plans;
	
	softheader($l['<title>']);

	echo '
	<div class="bg">
	<center class="tit"><i class="icon icon-import icon-head"></i>&nbsp; '.$l['solus_plans'].'</center>';
	
	error_handle($error);
	
	if(!empty($done)){
		echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done_solusvm_plans'].'</div>';
	}else{
		
		// Show the list to be imported
		if(!empty($solusvm_plans)){
			
			echo '<form accept-charset="'.$globals['charset'].'" name="importplans" method="post" action="" class="form-horizontal">
			
			<table border="0" width="95%" align="center" class="table table-hover tablesorter">
			<tr>
				<th class="fhead" width="10%">'.$l['solus_plan_id'].'</th>
				<th class="fhead">'.$l['solus_plan_name'].' </th>
				<th class="fhead">'.$l['solus_plan_type'].' </th>
				<th class="fhead">'.$l['solus_plan_ram'].' </th>
				<th class="fhead">'.$l['solus_plan_disk'].' </th>
				<th class="fhead">'.$l['solus_plan_bandwidth'].' </th>
				<th class="fhead">'.$l['solus_plan_virt'].' </th>
			</tr>';
			
			foreach($solusvm_plans as $k => $v){
				
				$plid = -1;
				
				// Is there a matching Plan ?
				if(isset($v_plans_matches[$v['type'].'-'.$v['name']])){
					$plid = $v_plans_matches[$v['type'].'-'.$v['name']];
				}
				
				echo '<tr '.($plid == -1 ? 'style="background-color: #C4FDB5"' : '').'>
					<td>'.$v['planid'].'</td>
					<td>'.$v['name'].'</td>
					<td>'.ucfirst($v['type']).'</td>
					<td>'.$v['ram'].' MB</td>
					<td>'.$v['disk'].' GB</td>
					<td>'.$v['bandwidth'].' GB</td>
					<td>'.($plid == -1 ? '--' : $v_plans[$plid]['name'].' (ID : '.$plid.')').'</td>
				</tr>';
				
			}

			echo '
			</table>
			
			<br /><br />
			<center><input type="submit" name="solusvm_plans" value="'.$l['submit'].'" class="btn" ></center>
			</form>
			<br /><br />
			<center><a href="'.$globals['index'].'act=import&sa=solusvm" class="link_btn">'.$l['solus_back_to_wizard'].'</a></center>
			<br /><br />
			<center><span style="background-color: #C4FDB5; padding:8px;">'.$l['solus_green_plans'].'</span></center>';
	
		// There are no users
		}else{
			echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['solus_no_ips'].'</div>';
		}

	}
echo '<br /></div>';
softfooter();

}

// Import SolusVM users
function solusvm_users_import_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $oslist, $done, $vpsid, $orphan, $users, $solusvm_users, $u_sol_users, $v_users, $inserted, $info, $no_users;
	
	softheader($l['<title>']);

	echo '
	<div class="bg">
	<center class="tit"><i class="icon icon-import icon-head"></i>&nbsp; SolusVM '.$l['importusers'].'</center>';
	
	error_handle($error);
	
	if(!empty($done)){
		echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done_solusvm_users'].'</div>';
	}else{
		
		// Show the list to be imported
		if(!empty($solusvm_users)){
			
			echo '<form accept-charset="'.$globals['charset'].'" name="importvps" method="post" action="" class="form-horizontal">
			
			<table border="0" width="95%" align="center" class="table table-hover tablesorter">
			<tr>
				<th class="fhead">'.$l['vpsuser'].'</th>
				<th class="fhead">'.$l['email'].' </th>
				<th class="fhead">'.$l['type_user'].' </th>
				<th class="fhead">'. $l['import_no'] .'</th>
			</tr>';
			
			foreach($solusvm_users as $k => $v){
				
				echo '<tr>
					<td width="40%">'.$v['username'].'</td>
					<td>'.(empty($v['emailaddress']) ? '---' : $v['emailaddress']).' </td>
					<td>'.(!empty($v['resellerid']) ? ($v['resellerid'] == $v['clientid'] ? $l['solus_user_type_1'] : $l['solus_user_type_2']) : $l['solus_user_type_0']).' </td>
					<td>'. (empty($v['emailaddress']) ? '<font color="red">'.$l['no'].'</font>'. '<br />' .$l['reason'] .': ' .$l['empty_email'] : (in_array($v['emailaddress'], $v_users) ? '<font color="red">'.$l['no'] .'</font>'.'<br />' .$l['reason'] . ': ' .$l['users_exists']  : $e = '<font color="green">'.$l['yes'] .'</font>' )) . '
					
					</td>
				</tr>';
				
			}
			
			/*if(isset($e)){
	
				echo '<tr>
						<td>
							'.$l['send_email'].'
						</td>
						<td>
							<input type="checkbox" name="send_email">
						</td>
					</tr>';
			}*/

			echo '
			</table>
			
			<br /><br />
			<center><input type="submit" name="solusvm_users" value="'.$l['submit'].'" class="btn" '.(!isset($e) ? ' disabled="1"' : '' ). '></center>
			</form>
			<br /><br />
			<center><a href="'.$globals['index'].'act=import&sa=solusvm" class="link_btn">'.$l['solus_back_to_wizard'].'</a></center>';
	
		// There are no users
		}else{
			echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['no_users'].'</div>';
		}

	}
echo '<br /></div>';
softfooter();

}

// Import SolusVM Ip Blocks
function solusvm_ips_import_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $servers, $oslist, $done, $vpsid, $orphan, $solusvm_nodes, $virt_solus_servers, $v_ippools, $v_ipmatches, $solusvm_ipblocks, $solusvm_ipblocknodes, $show_solusvm_int_ips;
	
	softheader($l['<title>']);

	echo '
	<div class="bg">
	<center class="tit"><i class="icon icon-import icon-head"></i>&nbsp; SolusVM '.$l['solus_import_ips'].'</center>';
	
	error_handle($error);
	
	if(!empty($done)){
		echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done_solusvm_ips'].'</div>';
	}else{
		
		// Show the list to be imported
		if(!empty($solusvm_ipblocks)){
			
			echo '<form accept-charset="'.$globals['charset'].'" name="importips" method="post" action="" class="form-horizontal">
			
			<table border="0" width="95%" align="center" class="table table-hover tablesorter">
			<tr>
				<th class="fhead" width="10">ID</th>
				<th class="fhead" width="10%">'.$l['solus_ips_name'].'</th>
				<th class="fhead">'.$l['solus_ips_type'].' </th>
				<th class="fhead">'.$l['solus_ips_gw'].' </th>
				<th class="fhead">'.$l['solus_ips_netmask'].' </th>
				<th class="fhead">'.$l['solus_ips_ns'].' </th>
				<th class="fhead">'.$l['solus_ips_servers'].' </th>
			</tr>';
			
			foreach($solusvm_ipblocks as $k => $v){
						
				// Is there a matching IP pool ?
				$ippid = @$v_ipmatches[$v['name'].'_'.$v['gateway'].'/'.$v['mask']];
				
				echo '<tr>
					<td>'.$k.'</td>
					<td '.(empty($ippid) ? 'style="background-color: #C4FDB5"' : '').'>'.$v['name'].'</td>
					<td>'.($v['iptype'] == 4 ? 'IPv4' : 'IPv6').'</td>
					<td>'.$v['gateway'].'</td>
					<td>'.$v['mask'].'</td>
					<td>'.$v['nameserver1'].'<br />'.$v['nameserver2'].'</td>
					<td>';
					
					// List the Block Nodes
					if(!empty($solusvm_ipblocknodes[$k])){
						
						echo '<table border="0" width="95%" align="center" class="table table-hover tablesorter">
						<tr>
						<th>SolusVM Node</th>
						<th>Virtualizor Server</th>
						</tr>';
						
						foreach($solusvm_ipblocknodes[$k] as $nk => $nv){
							echo '<tr '.(empty($v_ippools[$ippid]['servers'][$virt_solus_servers[$nk]]) ? ' style="background-color: #C4FDB5"' : '').'>
							<td>'.$solusvm_nodes[$nk]['name'].'</td>
							<td>'.(empty($servers[$virt_solus_servers[$nk]]['server_name']) ? '--' : $servers[$virt_solus_servers[$nk]]['server_name']).'</td>
							</tr>';
						}
						
						echo '</table>';
					}
					
					echo '</td>
				</tr>';
				
			}
			
			// For internal IPs. This will not be there
			if(!empty($show_solusvm_int_ips)){
				
				echo '<tr>
					<td>'.$k++.'</td>
					<td style="background-color: #C4FDB5">SolusVM_internal_'.$globals['server'].'</td>
					<td>Internal</td>
					<td>'.$show_solusvm_int_ips['gateway'].'</td>
					<td>'.$show_solusvm_int_ips['netmask'].'</td>
					<td>'.$show_solusvm_int_ips['nameserver1'].'<br />'.$show_solusvm_int_ips['nameserver2'].'</td>
					<td>';
			}
			
			echo '
			</table>
			
			<br /><br />
			<center><input type="submit" name="solusvm_ips" value="'.$l['submit'].'" class="btn" ></center>
			</form>
			<br /><br />
			<center><a href="'.$globals['index'].'act=import&sa=solusvm" class="link_btn">'.$l['solus_back_to_wizard'].'</a></center>
			<br /><br />
			<center><span style="background-color: #C4FDB5; padding:8px;">'.$l['solus_green_ips'].'</span></center>';
	
		// There are no IPs
		}else{
			echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['solus_no_ips'].'</div>';
		}

	}
echo '<br /></div>';
softfooter();

}

// Import SolusVM Templates
function solusvm_os_import_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $servers, $done, $ostemplates, $oslist, $os_map, $solusvm_templates;
	
	softheader($l['<title>']);

	echo '
	<div class="bg">
	<center class="tit"><i class="icon icon-import icon-head"></i>&nbsp; '.$l['solus_os'].'</center>';
	
	error_handle($error);
	
	if(!empty($done)){
		echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done_solusvm_os'].'</div>';
	}else{
		
		// Show the list to be imported
		if(!empty($solusvm_templates)){
			
			echo '<form accept-charset="'.$globals['charset'].'" name="importos" method="post" action="" class="form-horizontal">
			
			<table border="0" width="95%" align="center" class="table table-hover tablesorter">
			<tr>
				<th class="fhead" width="10%">'.$l['solus_os_id'].'</th>
				<th class="fhead">'.$l['solus_os_name'].' </th>
				<th class="fhead">'.$l['solus_os_type'].' </th>
				<th class="fhead">'.$l['solus_os_virt'].' </th>
			</tr>';
			
			foreach($solusvm_templates as $type => $type_v){
				
				foreach($type_v as $k => $v){
				
					echo '<tr '.(empty($os_map[$v['type']][$v['_filename']]) ? 'style="background-color: #C4FDB5"' : '').'>
					<td>'.$v['templateid'].'</td>
					<td>'.$v['filename'].'</td>
					<td>'.ucfirst($v['type']).'</td>
					<td>';
					
					if($v['type'] == 'openvz' || $v['type'] == 'xen'){
						echo (empty($os_map[$v['type']][$v['_filename']]) ? '--' : $ostemplates[ $os_map[$v['type']][$v['_filename']] ]['name'].' (ID : '.$os_map[$v['type']][$v['_filename']].')');
					}else{
						
						echo '<select name="'.$v['type'].'_'.$v['templateid'].'" class="form-control">
						<option value="0">Select</option>';
						
						foreach($ostemplates as $ok => $ov){
							
							$ov['type'] = ($ov['type'] == 'xen' && !empty($ov['hvm'])) ? 'xenhvm' : $ov['type'];
							
							if($ov['type'] != $v['type']){
								continue;
							}
							
							echo '<option value="'.$ok.'" '.($os_map[$v['type']][$v['_filename']] == $ok ? 'selected="selected"' : '').'>'.$ov['name'].'</option>';
							
						}
						
						echo '</select>';
						
					}
					
					echo '</td>
				</tr>';
				
				}
				
			}

			echo '
			</table>
			
			<br /><br />
			<center><input type="submit" name="solusvm_os" value="'.$l['submit'].'" class="btn" ></center>
			</form>
			<br /><br />
			<center><a href="'.$globals['index'].'act=import&sa=solusvm" class="link_btn">'.$l['solus_back_to_wizard'].'</a></center>
			<br /><br />
			<center><span style="background-color: #C4FDB5; padding:8px;">'.$l['solus_green_os'].'</span></center>
			<br /><br />
			<center><span class="notice">'.$l['solus_os_used_only'].'</span></center>';
	
		// There are no users
		}else{
			echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['solus_no_os'].'</div>';
		}

	}
echo '<br /></div>';
softfooter();

}

// Import SolusVM VPS
function solusvm_vps_import_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $servers, $done, $v_vps, $v_vps_matches, $solusvm_vps, $solusvm_users;
	
	softheader($l['<title>']);

	echo '
	<div class="bg">
	<center class="tit"><i class="icon icon-import icon-head"></i>&nbsp; '.$l['solus_vps'].'</center>';
	
	error_handle($error);
	
	if(!empty($done)){
		echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done_solusvm_vps'].'</div>';
	}else{
		
		// Show the list to be imported
		if(!empty($solusvm_vps)){
			
			echo '<form accept-charset="'.$globals['charset'].'" name="importos" method="post" action="" class="form-horizontal">
			
			<table border="0" width="95%" align="center" class="table table-hover tablesorter">
			<tr>
				<th class="fhead" width="10%">'.$l['solus_vps_id'].'</th>
				<th class="fhead">'.$l['solus_vps_name'].' </th>
				<th class="fhead">'.$l['solus_vps_hostname'].' </th>
				<th class="fhead">'.$l['solus_vps_type'].' </th>
				<th class="fhead">'.$l['solus_vps_user'].' </th>
				<th class="fhead">'.$l['solus_vps_virt'].' </th>
			</tr>';
			
			foreach($solusvm_vps as $k => $v){
				
				$vpsid = empty($v_vps_matches[$v['ctid']]) ? 0 : $v_vps_matches[$v['ctid']];
				
				echo '<tr '.(empty($vpsid) ? 'style="background-color: #C4FDB5"' : '').'>
					<td>'.$v['vserverid'].'</td>
					<td>'.$v['ctid'].'</td>
					<td>'.$v['hostname'].'</td>
					<td>'.ucfirst($v['type']).'</td>
					<td>'.$solusvm_users[$v['clientid']]['emailaddress'].'</td>
					<td>'.(empty($vpsid) ? '--' : $v_vps[$v_vps_matches[$v['ctid']]]['vps_name'].' (ID : '.$v_vps_matches[$v['ctid']].')').'</td>
				</tr>';
				
			}

			echo '
			</table>
			
			<br /><br />
			<center><input type="submit" name="solusvm_vps" value="'.$l['submit'].'" class="btn"></center>
			</form>
			<br /><br />
			<center><a href="'.$globals['index'].'act=import&sa=solusvm" class="link_btn">'.$l['solus_back_to_wizard'].'</a></center>
			<br /><br />
			<center><span style="background-color: #C4FDB5; padding:8px;">'.$l['solus_green_vps'].'</span></center>
			<br /><br />
			<center><span class="notice">'.$l['solus_vps_server'].'</span></center>';
	
		// There are no users
		}else{
			echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['solus_no_vps'].'</div>';
		}

	}
echo '<br /></div>';
softfooter();


}

// The default theme to show the options
function feathur_import_theme(){

global $theme, $globals, $user, $l, $error, $done;
	
	softheader($l['<title>']);

	echo '
	<div class="bg">
	<center class="tit"><i class="icon icon-import icon-head"></i>&nbsp; '.$l['feathur_import'].'</center>';
	
	error_handle($error);

	echo '
<table cellspacing="0" cellpadding="8" border="0" width="400" align="center">
<tr>
<td>
	<a href="'.$globals['ind'].'act=import&sa=feathur&ta=nodes" class="solus_opt"><img src="'.$theme['images'].'admin/servers.gif"> &nbsp;'.$l['solus_nodes_link'].'</a><br>
	<a href="'.$globals['ind'].'act=import&sa=feathur&ta=users" class="solus_opt"><img src="'.$theme['images'].'admin/users.gif"> &nbsp;'.$l['solus_users_link'].'</a><br>
	<a href="'.$globals['ind'].'act=import&sa=feathur&ta=ips" class="solus_opt"><img src="'.$theme['images'].'admin/ippool.gif"> &nbsp;'.$l['solus_ips_link'].'</a><br>
	<a href="'.$globals['ind'].'act=import&sa=feathur&ta=os" class="solus_opt"><img src="'.$theme['images'].'admin/ostemplates.gif"> &nbsp;'.$l['solus_os_link'].'</a><br>
	<a href="'.$globals['ind'].'act=import&sa=feathur&ta=vps" class="solus_opt"><img src="'.$theme['images'].'admin/vs.gif"> &nbsp;'.$l['solus_vps_link'].'</a>
</td>
</tr>
</table>
	
	<br><br><br>';
	
	echo '<center><span class="notice">'.$l['feathur_import_note'].'</span></center></div>';

	softfooter();

}


// Import Feathur Nodes
function feathur_nodes_import_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $servers, $oslist, $done, $vpsid, $orphan, $users, $feathur_nodes, $virt_feathur_servers;

//r_print($feathur_nodes);
	
	softheader($l['<title>']);

	echo '
	<div class="bg">
	<center class="tit"><i class="icon icon-import icon-head"></i>&nbsp; '.$l['feathur_nodes'].'</center>';
	
	error_handle($error);
	
	if(!empty($done)){
		echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done_solusvm_nodes'].'</div>';
	}else{
			
		echo '<table border="0" width="95%" align="center" class="table table-hover tablesorter">
		<tr>
			<th class="fhead">'.$l['feathur_nodes_id'].'</th>
			<th class="fhead">'.$l['feathur_nodes_name'].' </th>
			<th class="fhead">'.$l['feathur_nodes_ip'].' </th>
			<th class="fhead">'.$l['feathur_nodes_virt'].'</th>
		</tr>';	
						
		foreach($feathur_nodes as $k => $v){
			echo '<tr '.(isset($virt_feathur_servers[$k]) ? 'style="background-color: #C4FDB5"' : '').'>
			<td>'.$v['id'].'</td>
			<td>'.$v['name'].'</td>
			<td>'.$v['ip_address'].'</td>
			<td>'.(!isset($virt_feathur_servers[$k]) ? '--' : $servers[$virt_feathur_servers[$k]]['server_name'].' (ID : '.$virt_feathur_servers[$k].')').'</td>
			</tr>';
		}
		
		echo '</table>
			
		<br /><br />
		<center><a href="'.$globals['index'].'act=import&sa=feathur" class="link_btn">'.$l['feathur_back_to_wizard'].'</a></center>
		<br /><br />
		<center><span style="background-color: #C4FDB5; padding:8px;">'.$l['feathur_green_nodes'].'</span></center>';

	}
echo '<br /></div>';
softfooter();

}


// Import feathur users
function feathur_users_import_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $oslist, $done, $vpsid, $orphan, $users, $feathur_users, $u_feathur_users, $v_users, $inserted, $info, $no_users;
	
	softheader($l['<title>']);

	echo '
	<div class="bg">
	<center class="tit"><i class="icon icon-import icon-head"></i>&nbsp; Feathur '.$l['importusers'].'</center>';
	
	error_handle($error);
	
	if(!empty($done)){
		echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done_feathur_users'].'</div>';
	}else{
		
		// Show the list to be imported
		if(!empty($feathur_users)){
			
			echo '<form accept-charset="'.$globals['charset'].'" name="importvps" method="post" action="" class="form-horizontal">
			
			<table border="0" width="95%" align="center" class="table table-hover tablesorter">
			<tr>
				<th class="fhead">'.$l['vpsuser'].'</th>
				<th class="fhead">'.$l['email'].' </th>
				<th class="fhead">'.$l['type_user'].' </th>
				<th class="fhead">'. $l['import_no'] .'</th>
			</tr>';
			
			foreach($feathur_users as $k => $v){
				
				echo '<tr>
					<td width="40%">'.$v['username'].'</td>
					<td>'.(empty($v['email_address']) ? '---' : $v['email_address']).' </td>
					<td>'.(!empty($v['resellerid']) ? ($v['resellerid'] == $v['id'] ? $l['solus_user_type_1'] : $l['solus_user_type_2']) : $l['solus_user_type_0']).' </td>
					<td>'. (empty($v['email_address']) ? '<font color="red">'.$l['no'].'</font>'. '<br />' .$l['reason'] .': ' .$l['empty_email'] : (in_array($v['email_address'], $v_users) ? '<font color="red">'.$l['no'] .'</font>'.'<br />' .$l['reason'] . ': ' .$l['users_exists']  : $e = '<font color="green">'.$l['yes'] .'</font>' )) . '
					
					</td>
				</tr>';
				
			}
			
			/*if(isset($e)){
	
				echo '<tr>
						<td>
							'.$l['send_email'].'
						</td>
						<td>
							<input type="checkbox" name="send_email">
						</td>
					</tr>';
			}*/

			echo '
			</table>
			
			<br /><br />
			<center><input type="submit" name="feathur_users" value="'.$l['submit'].'" class="btn" '.(!isset($e) ? ' disabled="1"' : '' ).'></center>
			</form>
			<br /><br />
			<center><a href="'.$globals['index'].'act=import&sa=feathur" class="link_btn">'.$l['solus_back_to_wizard'].'</a></center>';
	
		// There are no users
		}else{
			echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['no_users'].'</div>';
		}

	}
echo '<br /></div>';
softfooter();

}


// Import feathur Ip Blocks
function feathur_ips_import_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $servers, $oslist, $done, $vpsid, $orphan, $feathur_nodes, $virt_feathur_servers, $v_ippools, $v_ipmatches, $feathur_ipblocks, $feathur_ipblocknodes;
	
	softheader($l['<title>']);

	echo '
	<div class="bg">
	<center class="tit"><i class="icon icon-import icon-head"></i>&nbsp;Feathur '.$l['solus_import_ips'].'</center>';
	
	error_handle($error);
	
	if(!empty($done)){
		echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done_feathur_ips'].'</div>';
	}else{
		
		// Show the list to be imported
		if(!empty($feathur_ipblocks)){
			
			echo '<form accept-charset="'.$globals['charset'].'" name="importips" method="post" action="" class="form-horizontal">
			
			<table border="0" width="95%" align="center" class="table table-hover tablesorter">
			<tr>
				<th class="fhead" width="10">ID</th>
				<th class="fhead" width="10%">'.$l['solus_ips_name'].'</th>
				<th class="fhead">'.$l['solus_ips_type'].' </th>
				<th class="fhead">'.$l['solus_ips_gw'].' </th>
				<th class="fhead">'.$l['solus_ips_netmask'].' </th>
				<th class="fhead">'.$l['solus_ips_servers'].' </th>
			</tr>';
			
			foreach($feathur_ipblocks as $k => $v){
						
				// Is there a matching IP pool ?
				$ippid = @$v_ipmatches[$v['name'].'_'.$v['gateway'].'/'.$v['netmask']];
				
				echo '<tr>
					<td>'.$k.'</td>
					<td '.(empty($ippid) ? 'style="background-color: #C4FDB5"' : '').'>'.$v['name'].'</td>
					<td>'.($v['ipv6'] == 0 ? 'IPv4' : 'IPv6').'</td>
					<td>'.$v['gateway'].'</td>
					<td>'.$v['netmask'].'</td>
					<td>';
					
					// List the Block Nodes
					if(!empty($feathur_ipblocknodes[$k])){
						
						echo '<table border="0" width="95%" align="center" class="table table-hover tablesorter">
						<tr>
						<th>Feathur Node</th>
						<th>Virtualizor Server</th>
						</tr>';
						
						foreach($feathur_ipblocknodes[$k] as $nk => $nv){
							echo '<tr '.(empty($v_ippools[$ippid]['servers'][$virt_feathur_servers[$nk]]) ? ' style="background-color: #C4FDB5"' : '').'>
							<td>'.$feathur_nodes[$nk]['name'].'</td>
							<td>'.(empty($servers[$virt_feathur_servers[$nk]]['server_name']) ? '--' : $servers[$virt_feathur_servers[$nk]]['server_name']).'</td>
							</tr>';
						}
						
						echo '</table>';
					}
					
					echo '</td>
				</tr>';
				
			}

			echo '
			</table>
			
			<br /><br />
			<center><input type="submit" name="feathur_ips" value="'.$l['submit'].'" class="btn"></center>
			</form>
			<br /><br />
			<center><a href="'.$globals['index'].'act=import&sa=feathur" class="link_btn">'.$l['solus_back_to_wizard'].'</a></center>
			<br /><br />
			<center><span style="background-color: #C4FDB5; padding:8px;">'.$l['solus_green_ips'].'</span></center>';
	
		// There are no IPs
		}else{
			echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['solus_no_ips'].'</div>';
		}

	}
echo '<br /></div>';
softfooter();

}

// Import SolusVM Templates
function feathur_os_import_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $servers, $done, $ostemplates, $oslist, $os_map, $feathur_templates;
	
	softheader($l['<title>']);

	echo '
	<div class="bg">
	<center class="tit"><i class="icon icon-import icon-head"></i>&nbsp; '.$l['feathur_os'].'</center>';
	
	error_handle($error);
	
	if(!empty($done)){
		echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done_feathur_os'].'</div>';
	}else{
		
		// Show the list to be imported
		if(!empty($feathur_templates)){
			
			echo '<form accept-charset="'.$globals['charset'].'" name="importos" method="post" action="" class="form-horizontal">
			
			<table border="0" width="95%" align="center" class="table table-hover tablesorter">
			<tr>
				<th class="fhead" width="10%">'.$l['solus_os_id'].'</th>
				<th class="fhead">'.$l['solus_os_name'].' </th>
				<th class="fhead">'.$l['solus_os_type'].' </th>
				<th class="fhead">'.$l['solus_os_virt'].' </th>
			</tr>';
			
			foreach($feathur_templates as $type => $type_v){
				
				foreach($type_v as $k => $v){
				
					echo '<tr '.(empty($os_map[$v['type']][$v['_path']]) ? 'style="background-color: #C4FDB5"' : '').'>
					<td>'.$v['id'].'</td>
					<td>'.$v['path'].'</td>
					<td>'.ucfirst($v['type']).'</td>
					<td>';
					
					if($v['type'] == 'openvz' || $v['type'] == 'xen'){
						echo (empty($os_map[$v['type']][$v['_path']]) ? '--' : $ostemplates[ $os_map[$v['type']][$v['_path']] ]['name'].' (ID : '.$os_map[$v['type']][$v['_path']].')');
					}else{
						
						echo '<select name="'.$v['type'].'_'.$v['id'].'" class="form-control">
						<option value="0">Select</option>';
						
						foreach($ostemplates as $ok => $ov){
							
							$ov['type'] = ($ov['type'] == 'xen' && !empty($ov['hvm'])) ? 'xenhvm' : $ov['type'];
							
							if($ov['type'] != $v['type']){
								continue;
							}
							
							echo '<option value="'.$ok.'" '.($os_map[$v['type']][$v['_path']] == $ok ? 'selected="selected"' : '').'>'.$ov['name'].'</option>';
							
						}
						
						echo '</select>';
						
					}
					
					echo '</td>
				</tr>';
				
				}
				
			}

			echo '
			</table>
			
			<br /><br />
			<center><input type="submit" name="feathur_os" value="'.$l['submit'].'" class="btn"></center>
			</form>
			<br /><br />
			<center><a href="'.$globals['index'].'act=import&sa=feathur" class="link_btn">'.$l['solus_back_to_wizard'].'</a></center>
			<br /><br />
			<center><span style="background-color: #C4FDB5; padding:8px;">'.$l['solus_green_os'].'</span></center>
			<br /><br />
			<center><span class="notice">'.$l['solus_os_used_only'].'</span></center>';
	
		// There are no users
		}else{
			echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['feathur_no_os'].'</div>';
		}

	}
echo '<br /></div>';
softfooter();

}


// Import Feathur VPS
function feathur_vps_import_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $servers, $done, $v_vps, $v_vps_matches, $feathur_vps, $feathur_users;
	
	softheader($l['<title>']);

	echo '
	<div class="bg">
	<center class="tit"><i class="icon icon-import icon-head"></i>&nbsp; '.$l['feathur_vps'].'</center>';
	
	error_handle($error);
	
	if(!empty($done)){
		echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done_feathur_vps'].'</div>';
	}else{
		
		// Show the list to be imported
		if(!empty($feathur_vps)){
			
			echo '<form accept-charset="'.$globals['charset'].'" name="importos" method="post" action="" class="form-horizontal">
			
			<table border="0" width="95%" align="center" class="table table-hover tablesorter">
			<tr>
				<th class="fhead" width="10%">'.$l['solus_vps_id'].'</th>
				<th class="fhead">'.$l['solus_vps_name'].' </th>
				<th class="fhead">'.$l['solus_vps_hostname'].' </th>
				<th class="fhead">'.$l['solus_vps_type'].' </th>
				<th class="fhead">'.$l['solus_vps_user'].' </th>
				<th class="fhead">'.$l['solus_vps_virt'].' </th>
			</tr>';
			
			foreach($feathur_vps as $k => $v){
				
					$vpsid = empty($v_vps_matches[$v['container_id']]) ? 0 : $v_vps_matches[$v['container_id']];
				
				echo '<tr '.(empty($vpsid) ? 'style="background-color: #C4FDB5"' : '').'>
					<td>'.$v['id'].'</td>
					<td>'.$v['container_id'].'</td>
					<td>'.$v['hostname'].'</td>
					<td>'.ucfirst($v['type']).'</td>
					<td>'.$feathur_users[$v['user_id']]['email_address'].'</td>
					<td>'.(empty($vpsid) ? '--' : $v_vps[$v_vps_matches[$v['container_id']]]['vps_name'].' (ID : '.$v_vps_matches[$v['container_id']].')').'</td>
				</tr>';
				
			}

			echo '
			</table>
			
			<br /><br />
			<center><input type="submit" name="feathur_vps" value="'.$l['submit'].'" class="btn"></center>
			</form>
			<br /><br />
			<center><a href="'.$globals['index'].'act=import&sa=feathur" class="link_btn">'.$l['feathur_back_to_wizard'].'</a></center>
			<br /><br />
			<center><span style="background-color: #C4FDB5; padding:8px;">'.$l['solus_green_vps'].'</span></center>
			<br /><br />
			<center><span class="notice">'.$l['solus_vps_server'].'</span></center>';
	
		// There are no users
		}else{
			echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['feathur_no_vps'].'</div>';
		}

	}

echo '<br /></div>';
softfooter();


}

// The default theme to show the options
function proxmox_import_theme(){

global $theme, $globals, $user, $l, $error, $done;
	
	softheader($l['<title>']);

	echo '
	<div class="bg">
	<center class="tit"><i class="icon icon-import icon-head"></i>&nbsp; '.$l['proxmox_import'].'</center>';
	
	error_handle($error);

	echo '
<table cellspacing="0" cellpadding="8" border="0" width="400" align="center">
<tr>
<td>
	<a href="'.$globals['ind'].'act=import&sa=proxmox&ta=nodes" class="solus_opt"><img src="'.$theme['images'].'admin/servers.gif"> &nbsp;'.$l['solus_nodes_link'].'</a><br>
	<a href="'.$globals['ind'].'act=import&sa=proxmox&ta=users" class="solus_opt"><img src="'.$theme['images'].'admin/users.gif"> &nbsp;'.$l['solus_users_link'].'</a><br>
	<a href="'.$globals['ind'].'act=import&sa=proxmox&ta=storages" class="solus_opt"><img src="'.$theme['images'].'admin/users.gif"> &nbsp;'.$l['proxmox_storages_link'].'</a><br>
	<a href="'.$globals['ind'].'act=import&sa=proxmox&ta=vps" class="solus_opt"><img src="'.$theme['images'].'admin/vs.gif"> &nbsp;'.$l['solus_vps_link'].'</a>
</td>
</tr>
</table>
	
	<br><br><br>';
	
	echo '<center><span class="notice">'.$l['proxmox_import_note'].'</span></center></div>';

	softfooter();

}

// Import Feathur Nodes
function proxmox_nodes_import_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $servers, $oslist, $done, $vpsid, $orphan, $users, $proxmox_nodes, $virt_proxmox_servers;

//r_print($proxmox_nodes);
//r_print($virt_proxmox_servers);
	
	softheader($l['<title>']);

	echo '
	<div class="bg">
	<center class="tit"><i class="icon icon-import icon-head"></i>&nbsp; '.$l['proxmox_nodes'].'</center>';
	
	error_handle($error);
	
	if(!empty($done)){
		echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done_solusvm_nodes'].'</div>';
	}else{
			
		echo '<table border="0" width="95%" align="center" class="table table-hover tablesorter">
		<tr>
			<th class="fhead">'.$l['proxmox_nodes_id'].'</th>
			<th class="fhead">'.$l['proxmox_nodes_name'].' </th>
			<th class="fhead">'.$l['proxmox_nodes_virt'].'</th>
		</tr>';	
						
		foreach($proxmox_nodes[0] as $k => $v){
			echo '<tr '.(isset($virt_proxmox_servers[$k]) ? 'style="background-color: #C4FDB5"' : '').'>
			<td>'.$v['id'].'</td>
			<td>'.$v['node'].'</td>
			<td>'.(!isset($virt_proxmox_servers[$k]) ? '--' : $servers[$virt_proxmox_servers[$k]]['server_name'].' (ID : '.$virt_proxmox_servers[$k].')').'</td>
			</tr>';
		}
		
		echo '</table>
			
		<br /><br />
		<center><a href="'.$globals['index'].'act=import&sa=proxmox" class="link_btn">'.$l['proxmox_back_to_wizard'].'</a></center>
		<br /><br />
		<center><span style="background-color: #C4FDB5; padding:8px;">'.$l['proxmox_green_nodes'].'</span></center>';

	}
echo '<br /></div>';
softfooter();

}

// Import proxmox users
function proxmox_users_import_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $oslist, $done, $vpsid, $orphan, $users, $proxmox_users, $u_sol_users, $v_users, $inserted, $info, $no_users;
	
	softheader($l['<title>']);

	echo '
	<div class="bg">
	<center class="tit"><i class="icon icon-import icon-head"></i>&nbsp; Proxmox '.$l['importusers'].'</center>';
	
	error_handle($error);
	
	if(!empty($done)){
		echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done_solusvm_users'].'</div>';
	}else{
		
		// Show the list to be imported
		if(!empty($proxmox_users)){
			
			echo '<form accept-charset="'.$globals['charset'].'" name="importvps" method="post" action="" class="form-horizontal">
			
			<table border="0" width="95%" align="center" class="table table-hover tablesorter">
			<tr>
				<th class="fhead">'.$l['vpsuser'].'</th>
				<th class="fhead">'.$l['email'].' </th>
				<th class="fhead">'.$l['type_user'].' </th>
				<th class="fhead">'. $l['import_no'] .'</th>
			</tr>';
			
			foreach($proxmox_users as $k => $v){
				
				echo '<tr>
					<td width="40%">'.$v['userid'].'</td>
					<td>'.(empty($v['email']) ? '---' : $v['email']).' </td>
					<td>'.(!empty($v['resellerid']) ? ($v['resellerid'] == $v['clientid'] ? $l['solus_user_type_1'] : $l['solus_user_type_2']) : $l['solus_user_type_0']).' </td>
					<td>'. (empty($v['email']) ? '<font color="red">'.$l['no'].'</font>'. '<br />' .$l['reason'] .': ' .$l['empty_email'] : (in_array($v['email'], $v_users) ? '<font color="red">'.$l['no'] .'</font>'.'<br />' .$l['reason'] . ': ' .$l['users_exists']  : $e = '<font color="green">'.$l['yes'] .'</font>' )) . '
					
					</td>
				</tr>';
				
			}
			
			/*if(isset($e)){
	
				echo '<tr>
						<td>
							'.$l['send_email'].'
						</td>
						<td>
							<input type="checkbox" name="send_email">
						</td>
					</tr>';
			}*/

			echo '
			</table>
			
			<br /><br />
			<center><input type="submit" name="proxmox_users" value="'.$l['submit'].'" class="btn" '.(!isset($e) ? ' disabled="1"' : '' ). '></center>
			</form>
			<br /><br />
			<center><a href="'.$globals['index'].'act=import&sa=proxmox" class="link_btn">'.$l['solus_back_to_wizard'].'</a></center>
			<br /><br />
			<center>'.$l['proxmox_users_server'].'</center>';
	
		// There are no users
		}else{
			echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['no_users'].'</div>';
		}

	}
echo '<br /></div>';
softfooter();

}


// Import proxmox Storages
function proxmox_storages_import_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $oslist, $done, $vpsid, $orphan, $storages, $proxmox_storages, $u_sol_users, $v_storages, $inserted, $info, $no_users;
	
	softheader($l['<title>']);

	echo '
	<div class="bg">
	<center class="tit"><i class="icon icon-import icon-head"></i>&nbsp; Proxmox '.$l['importstorages'].'</center>';
	
	error_handle($error);
	
	if(!empty($done)){
		echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done_proxmox_storages'].'</div>';
	}else{
		
		// Show the list to be imported
		if(!empty($proxmox_storages)){
			
			echo '<form accept-charset="'.$globals['charset'].'" name="importvps" method="post" action="" class="form-horizontal">
			
			<table border="0" width="95%" align="center" class="table table-hover tablesorter">
			<tr>
				<th class="fhead">'.$l['name'].'</th>
				<th class="fhead">'.$l['type'].' </th>
				<th class="fhead">'.$l['import_no'].' </th>
			</tr>';
			
			foreach($proxmox_storages as $k => $v){
				
				echo '<tr>
					<td>'.(empty($v['storage']) ? '---' : $v['storage']).' </td>
					<td>'.(empty($v['type']) ? '---' : $v['type']).' </td>
					<td>'. (empty($v['storage']) ? '<font color="red">'.$l['no'].'</font>'. '<br />' .$l['reason'] .': ' .$l['empty_email'] : (in_array($v['storage'], $v_storages) ? '<font color="red">'.$l['no'] .'</font>'.'<br />' .$l['reason'] . ': ' .$l['storages_exists']  : $e = '<font color="green">'.$l['yes'] .'</font>' )) . '
					</td>
				</tr>';	
			}
			
			echo '
			</table>
			
			<br /><br />
			<center><input type="submit" name="proxmox_storages" value="'.$l['submit'].'" class="btn" '.(!isset($e) ? ' disabled="1"' : '' ). '></center>
			</form>
			<br /><br />
			<center><a href="'.$globals['index'].'act=import&sa=proxmox" class="link_btn">'.$l['solus_back_to_wizard'].'</a></center>
			<br /><br />
			<center>'.$l['proxmox_storage_server'].'</center>';
	
		// There are no storages
		}else{
			echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['no_storages'].'</div>';
		}

	}
echo '<br /></div>';
softfooter();

}


// Import Proxmox VPS
function proxmox_vps_import_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $servers, $done, $v_vps, $v_vps_matches, $proxmox_vps, $proxmox_users, $users, $importVm;
	
	softheader($l['<title>']);

	echo '
	<div class="bg">
	<center class="tit"><i class="icon icon-import icon-head"></i>&nbsp; '.$l['proxmox_vps'].'</center>';
	
	error_handle($error);
	
	if(!empty($done)){
		echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done_proxmox_vps'].'</div>';
	}else{
		
		// Show the list to be imported
		if(!empty($importVm)){
			
			echo '<form accept-charset="'.$globals['charset'].'" name="importos" method="post" action="" class="form-horizontal">
			
			<table border="0" width="95%" align="center" class="table table-hover tablesorter">
			<tr>
				<th class="fhead">'.$l['solus_vps_name'].' </th>
				<th class="fhead">'.$l['solus_vps_hostname'].' </th>
				<th class="fhead">'.$l['solus_vps_type'].' </th>
				<th class="fhead">'.$l['vpsuser'].' </th>
			</tr>';
			
			foreach($importVm as $k => $v){
				
				echo '<tr>
					<td>'.$v['vmid'].'</td>
					<td>'.$v['name'].'</td>
					<td>'.ucfirst($v['type'] == 'proxk' ? 'qemu' : ($v['type'] == 'proxl' ? 'lxc' : $v['type'])).'</td>
					<td>
						<select name="vsuser_'.$k.'" class="form-control">
						<option value="">'.$l['none'].'</option>';
					
						foreach($users as $uk => $uv){
							echo '<option value="'.$uk.'">'.$uv['email'].'</option>';
						}
					
				echo '</select>
					</td>
				</tr>';
				
			}

			echo '
			</table>
			
			<br /><br />
			<center><input type="submit" name="proxmox_vps" value="'.$l['submit'].'" class="btn"></center>
			</form>
			<br /><br />
			<center><a href="'.$globals['index'].'act=import&sa=proxmox" class="link_btn">'.$l['proxmox_back_to_wizard'].'</a></center>
			<br /><br />
			<center><span class="notice">'.$l['solus_vps_server'].'</span></center>';
	
		// There are no users
		}else{
			echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['proxmox_no_vps'].'</div>';
		}

	}

echo '<br /></div>';
softfooter();


}




?>
