<?php

//////////////////////////////////////////////////////////////
//===========================================================
// logs_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function logs_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $logs;

softheader($l['<title>']);

echo '
<div class="bg" style="width: 99%">
<center class="tit"><i class="icon icon-logs icon-head"></i> &nbsp; '.$l['heading'].'<span style="float:right;"><a href="javascript:showsearch();"><img src="'.$theme['images'].'admin/search.gif" /></a><a href="'.$globals['docs'].'Logs" target="_blank" class="wiki_help" title="'.$l['wiki_help'].'"><i class="icon-help" ></i></a></span></center>
<div id="showsearch" style="display:'.(optREQ('search') || (!empty($logs) && !empty($globals['showsearch'])) ? "" : "none").';" class="form-horizontal">
<form accept-charset="'.$globals['charset'].'" name="logs" method="get" action="">
<input type="hidden" name="act" value="logs">
		
<div class="form-group_head">
  <div class="row">
    <div class="col-sm-2"></div>
    <div class="col-sm-1"><label>'.$l['id'].'</label></div>
    <div class="col-sm-2"><input type="text" class="form-control" name="id" id="id" size="30" value="'.REQval('id','').'"/></div>
    <div class="col-sm-1"><label>'.$l['email'].'</label></div>
    <div class="col-sm-2"><input type="text" class="form-control" name="email" id="email" size="30" value="'.REQval('email','').'"/></div>
    <div class="col-sm-2" style="text-align: center;"><button type="submit" name="search" class="go_btn" value="Search"/>'.$l['search'].'</button></div>
    <div class="col-sm-2"></div>
  </div>
</div>
		
</form>
<br /><br />
</div>';

if(empty($logs)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['no_logs'].'</div>';
}
else{
	page_links($globals['num_res'], $globals['cur_page'], $globals['reslen']);
	echo '<br /><br />
	<table class="table table-hover tablesorter">
	<tr>
		<th align="center">'.$l['user'].'</th>
		<th align="center">'.$l['vpsid'].'</th>
		<th align="center">'.$l['date'].'</th>
		<th align="center">'.$l['act_id'].'</th>
		<th align="center">'.$l['act'].'</th>
		<th align="center">'.$l['status'].'</th>
		<th align="center">'.$l['ip'].'</th>	
	<tr>';
	$i = 0;
	foreach($logs as $k => $v){
		
	echo '<tr>
			<td>'.$v['email'].'</td>
			<td align="left">'.$v['id'].'</td>
			<td>'.datify($v['time']).'</td>
			<td>'.$v['actid'].'</td>
			<td>'.action_text($v['action'], $v['data']).'</td>
			<td align="center">'.($v['status'] == 1 ? 'Successful' : '<font color="#FF0000">Failed</font>').'</td>
			<td>'.$v['ip'].'</td>
		  </tr>';
		  $i++;
	}
	
	echo '</table><br />
	<form accept-charset="'.$globals['charset'].'" name="logs" method="post" action="" class="form-horizontal">
	<center><input type="submit" name="delete" value="'.$l['delete'].'" class="btn"></center>
	</form>
	<br /><br />';
	page_links($globals['num_res'], $globals['cur_page'], $globals['reslen']);
}

echo '</div>';
softfooter();

}

?>