<?php

//////////////////////////////////////////////////////////////
//===========================================================
// vscpu_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function serverloads_theme(){

global $theme, $globals, $cluster, $user, $l, $vpsusage, $available, $vpses;

softheader($l['<title>']);

echo '<script language="javascript" src="'.js_url('Chart.min.js').'" type="text/javascript"></script>
	<div class="bg" style="width: 99%">
	<center class="tit"><i class="icon icon-serverloads icon-head"></i>&nbsp; '.$l['heading'].'</center><br />';

// Is it offline ?
$hypervisor_status = $cluster->statewise($globals['server']);
if($hypervisor_status == 0 || $hypervisor_status == 2){

	echo '<div class="e_notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['server_status_'.$hypervisor_status].'</div>';
	
}else{

	if(count($vpses) < 1){
		echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
		
	}else{
	
foreach($vpsusage as $k => $v){
	$data[1][$k] = $v[1];
	$data[5][$k] = $v[5];
	$data[15][$k] = $v[15];
}
	
	echo '<center><canvas id="canvas" height="450" width="1000"></canvas><div id="barLegend"></div><br /><br /></center>
		

<script type="text/javascript" charset="utf-8"><!-- // --><![CDATA[
	
$(function () {

	function legend(parent, data) {
		parent.className = "legend";
		parent.style.position = "absolute";
		parent.style.top = document.getElementById("canvas").offsetTop+20 + "px";
		parent.style.left = document.getElementById("canvas").offsetLeft+250 + "px";
		
		var datas = data.hasOwnProperty("datasets") ? data.datasets : data;

		datas.forEach(function(d) {
			var title = document.createElement("span");
			title.className = "title";
			title.style.borderColor = "black";
			title.style.borderStyle = "solid";
			title.style.borderWidth = "1px";
			title.style.background = d.hasOwnProperty("fillColor") ? d.fillColor : d.color;
			parent.appendChild(title);
			var text = document.createTextNode(d.title);
			title.appendChild(text);
		});
	}
	
	
	var barChartData = {
			labels : ['.implode(",",array_keys($vpsusage)).'],
			datasets : [
				{
					fillColor : "rgba(175,216,248,0.5)",
					strokeColor : "rgba(175,216,248,1)",
					data : ['.implode(",",$data[1]).'],
					title : " 1 min "
				},
				{
					fillColor : "rgba(237,194,64,0.5)",
					strokeColor : "rgba(237,194,64,1)",
					data : ['.implode(",",$data[5]).'],
					title : " 5 min "
				},
				{
					fillColor : "rgba(75,75,75,0.2)",
					strokeColor : "rgba(75,75,75,0.5)",
					data : ['.implode(",",$data[15]).'],
					title : " 15 min "
				}
			]
			
		}
		var myLine = new Chart(document.getElementById("canvas").getContext("2d")).Bar(barChartData, {scaleStartValue : 0,scaleFontSize : 15});
		legend(document.getElementById("barLegend"), barChartData);
});

$(document).ready(function(){
	
	$(".altrowstable tr").mouseover(function(){
		var old_class = $(this).attr("class");
		//alert(old_class);
		$(this).attr("class", "tr_bgcolor");
		
		$(this).mouseout(function(){
			$(this).attr("class", old_class);
		});
	});
	
});
	
// ]]></script>	
	
	<table class="table table-hover tablesorter">
	<tr>
		<th align="center">'.$l['vpsid'].'</th>
		<th align="center">'.$l['vpsname'].'</th>
		<th align="center">'.$l['hostname'].'</th>
		<th align="center">'.$l['1min'].' </th>
		<th align="center">'.$l['5min'].' </th>
		<th align="center">'.$l['15min'].' </th>
	</tr>';

	$i = 1;
	
	foreach($vpses as $k => $v){
		
		echo '<tr>
			<td align="center">'.$v['vpsid'].'</td>
			<td align="center">'.$v['vps_name'].'</td>
			<td>'.$v['hostname'].'</td>
			<td align="center">'.$vpsusage[$k][1].'</td>
			<td align="center">'.$vpsusage[$k][5].'</td>
			<td align="center">'.$vpsusage[$k][15].'</td>
		</tr>';
	
		$i++;
	}
		
	echo '</table>';
	
	}

}
echo '</div>';
softfooter();

}

?>
