<?php

//////////////////////////////////////////////////////////////
//===========================================================
// config_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function config_theme(){

global $theme, $globals, $info, $kernel, $user, $l, $error, $langs, $skins, $done, $cluster, $virt, $centos5, $val, $master, $servers;

softheader($l['<title>']);

echo '
<div class="bg" style="width:99%">
<center class="tit"><i class="icon icon-config icon-head"></i> &nbsp;'.$l['heading'].'</center>';

error_handle($error);

if(!$master) {
	server_select();
}

// Is it offline ?
$hypervisor_status = $cluster->statewise($globals['server']);
if($hypervisor_status == 0 || $hypervisor_status == 2){

	echo '<div class="e_notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['server_status_'.$hypervisor_status].'</div>';
	
}else{

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
}

if($master){
	echo '<div class="notebox" style="width:95%;">'.$l['master_setting_note'].'</div>';
}else{
	echo '<div class="notebox" style="width:95%;">'.$l['slave_setting_note'].'</div>';
}

echo '
<style>
.side_lbl{
	padding:10px;
}
</style>

<script language="javascript" type="text/javascript"><!-- // --><![CDATA[
	
	function reset_hash(){
		
		$("#resethash").attr("src", "'.$theme['images'].'admin/vpsloading.gif");
		
		$.ajax({
			type: "POST",
			url: "'.$globals['ind'].'act=config&reset_cb_hash=1&api=json",
			dataType : "json",
			success:function(response){
				if("new_hash" in response){
					$("#cb_hash").html(response["new_hash"]);
				}
				$("#resethash").attr("src", "'.$theme['images'].'refresh.png");
			},
			error:function(){
				$("#resethash").attr("src", "'.$theme['images'].'refresh.png");
			}
		});
		return;
	}
	
	$(document).ready(function(){
		
		$("#haproxy_src_ips").on("change", function(){
			alert("'.$l['haproxy_src_ips_change'].'");
		});
		
	})
// ]]></script>

<div class="divroundshad">
<div class="roundheader">'.$l['gen_set'].'</div>
<br/>
<form accept-charset="'.$globals['charset'].'" action="" method="post" name="config" class="form-horizontal">';

if($master){
	
echo '<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['sitename'].'</label><br />
		<span class="help-block">'.$l['sitename_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="sn" id="sitename" size="30" value="'.POSTval('sn', $info['sn']).'" />
	</div>
</div>
<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['site_domain'].'</label><br />
		<span class="help-block">'.$l['site_domain_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="site_domain" size="30" value="'.POSTval('site_domain', $info['site_domain']).'" />
	</div>
</div>
<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['logo_url'].'</label><br />
		<span class="help-block">'.$l['logo_url_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="logo_url" size="30" value="'.POSTval('logo_url', $info['logo_url']).'" />
	</div>
</div>
<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['logo_login_url'].'</label><br />
		<span class="help-block">'.$l['logo_login_url_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="logo_login_url" size="30" value="'.POSTval('logo_login_url', $info['logo_login_url']).'" />
	</div>
</div>
<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['support'].'</label><br />
		<span class="help-block">'.$l['support_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="support" size="30" value="'.POSTval('support', $info['support']).'"/> 
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['copy_right'].'</label><br />
		<span class="help-block">'.$l['copy_right_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="copyright" id="copyright" size="30" value="'.POSTval('copyright', @$info['copyright']).'" />
	</div>
</div>

<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['comp_output'].'</label><br />
		<span class="help-block">'.$l['comp_output_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" value="1" class="ios" name="gzip" '.POSTchecked('gzip', $info['gzip']).' />
	</div>
</div>';
}

if(empty($master)){
echo '<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['cookie_name'].'</label><br />
		<span class="help-block">'.$l['cook_name_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="cookie_name" id="cookie_name" size="30" value="'.POSTval('cookie_name', $info['cookie_name']).'" />
	</div>
</div>';
}

echo '<div class="row">
	<div class="col-sm-6">
	<label class="control-label">'.$l['soft_email'].'</label><br />
	<span class="help-block">'.$l['soft_email_exp'].'</span>
	</div>
	<div class="col-sm-6">
	<input type="text" class="form-control" name="soft_email" size="30" value="'.POSTval('soft_email', $info['soft_email']).'" />
	</div>
</div>';

if($master){
echo '<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['sess_len'].'</label><br />
		<span class="help-block">'.$l['sess_len_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="sess_len" id="sess_len" size="30" value="'.POSTval('sess_len', $info['sess_len']).'" />
	</div>
</div>
<div class="row">
<div class="col-sm-6">
<label class="control-label">'.$l['choose_lang'].'</label><br />
<span class="help-block">'.$l['choose_lang_exp'].'</span>
</div>
<div class="col-sm-6">
<select class="form-control" name="language">';
	
	foreach($langs as $k => $v){

		echo '<option value="'.$v.'" '.(empty($_POST['language']) && $info['language'] == $v ? 'selected="selected"' : (@trim($_POST['language']) == $v ? 'selected="selected"' : '') ).'>'._ucfirst($v).'</option>';
		
	}
	
	echo '</select>
</div>
</div>

<div class="row">
<div class="col-sm-6">
<label class="control-label">'.$l['choose_theme'].'</label><br />
<span class="help-block">'.$l['choose_theme_exp'].'</span>
</div>
<div class="col-sm-6">
<select class="form-control" name="theme_folder">';
	
	foreach($skins as $k => $v){
		
		echo '<option value="'.$v.'" '.(empty($_POST['theme_folder']) && $info['theme_folder'] == $v ? 'selected="selected"' : (trim($_POST['theme_folder']) == $v ? 'selected="selected"' : '') ).'>'._ucfirst($v).'</option>';
	}
	
	echo '</select>
</div>
</div>';
}

echo '<div class="row">
	<div class="col-sm-6"><label class="control-label">'.$l['timezone'].'</label></div>
	<div class="col-sm-6" valign="top">
		<select class="form-control" name="timezone" style="font-size:13px">
		<option value="-12" '.(POSTval('timezone') == '-12' ? 'selected="selected"' : ($info['timezone'] == -12 ? 'selected="selected"' : '')).' >(GMT -12:00) Eniwetok, Kwajalein</option>
		<option value="-11" '.(POSTval('timezone') == '-11' ? 'selected="selected"' : ($info['timezone'] == -11 ? 'selected="selected"' : '')).' >(GMT -11:00) Midway Island, Samoa</option>
		<option value="-10" '.(POSTval('timezone') == '-10' ? 'selected="selected"' : ($info['timezone'] == -10 ? 'selected="selected"' : '')).' >(GMT -10:00) Hawaii</option>
		<option value="-9" '.(POSTval('timezone') == '-9' ? 'selected="selected"' : ($info['timezone'] == -9 ? 'selected="selected"' : '')).' >(GMT -9:00) Alaska</option>
		<option value="-8" '.(POSTval('timezone') == '-8' ? 'selected="selected"' : ($info['timezone'] == -8 ? 'selected="selected"' : '')).' >(GMT -8:00) Pacific Time (US &amp; Canada)</option>
		<option value="-7" '.(POSTval('timezone') == '-7' ? 'selected="selected"' : ($info['timezone'] == -7 ? 'selected="selected"' : '')).' >(GMT -7:00) Mountain Time (US &amp; Canada)</option>
		<option value="-6" '.(POSTval('timezone') == '-6' ? 'selected="selected"' : ($info['timezone'] == -6 ? 'selected="selected"' : '')).' >(GMT -6:00) Central Time (US &amp; Canada), Mexico City</option>
		<option value="-5" '.(POSTval('timezone') == '-5' ? 'selected="selected"' : ($info['timezone'] == -5 ? 'selected="selected"' : '')).' >(GMT -5:00) Eastern Time (US &amp; Canada), Bogota, Lima</option>
		<option value="-4" '.(POSTval('timezone') == '-4' ? 'selected="selected"' : ($info['timezone'] == -4 ? 'selected="selected"' : '')).' >(GMT -4:00) Atlantic Time (Canada), Caracas, La Paz</option>
		<option value="-3.5" '.(POSTval('timezone') == '-3.5' ? 'selected="selected"' : ($info['timezone'] == -3.5 ? 'selected="selected"' : '')).' >(GMT -3:30) Newfoundland</option>
		<option value="-3" '.(POSTval('timezone') == '-3' ? 'selected="selected"' : ($info['timezone'] == -3 ? 'selected="selected"' : '')).' >(GMT -3:00) Brazil, Buenos Aires, Georgetown</option>
		<option value="-2" '.(POSTval('timezone') == '-2' ? 'selected="selected"' : ($info['timezone'] == -2 ? 'selected="selected"' : '')).' >(GMT -2:00) Mid-Atlantic</option>
		<option value="-1" '.(POSTval('timezone') == '-1' ? 'selected="selected"' : ($info['timezone'] == -1 ? 'selected="selected"' : '')).' >(GMT -1:00 hour) Azores, Cape Verde Islands</option>
		<option value="0" '.(POSTval('timezone') == '0' ? 'selected="selected"' : ($info['timezone'] == 0 ? 'selected="selected"' : '')).' >(GMT) Western Europe Time, London, Lisbon, Casablanca</option>
		<option value="1" '.(POSTval('timezone') == '1' ? 'selected="selected"' : ($info['timezone'] == 1 ? 'selected="selected"' : '')).' >(GMT +1:00 hour) Brussels, Copenhagen, Madrid, Paris</option>
		<option value="2" '.(POSTval('timezone') == '2' ? 'selected="selected"' : ($info['timezone'] == 2 ? 'selected="selected"' : '')).' >(GMT +2:00) Kaliningrad, South Africa</option>
		<option value="3" '.(POSTval('timezone') == '3' ? 'selected="selected"' : ($info['timezone'] == 3 ? 'selected="selected"' : '')).' >(GMT +3:00) Baghdad, Riyadh, Moscow, St. Petersburg</option>
		<option value="3.5" '.(POSTval('timezone') == '3.5' ? 'selected="selected"' : ($info['timezone'] == 3.5 ? 'selected="selected"' : '')).' >(GMT +3:30) Tehran</option>
		<option value="4" '.(POSTval('timezone') == '4' ? 'selected="selected"' : ($info['timezone'] == 4 ? 'selected="selected"' : '')).' >(GMT +4:00) Abu Dhabi, Muscat, Baku, Tbilisi</option>
		<option value="4.5" '.(POSTval('timezone') == '4.5' ? 'selected="selected"' : ($info['timezone'] == 4.5 ? 'selected="selected"' : '')).' >(GMT +4:30) Kabul</option>
		<option value="5" '.(POSTval('timezone') == '5' ? 'selected="selected"' : ($info['timezone'] == 5 ? 'selected="selected"' : '')).' >(GMT +5:00) Ekaterinburg, Islamabad, Karachi, Tashkent</option>
		<option value="5.5" '.(POSTval('timezone') == '5.5' ? 'selected="selected"' : ($info['timezone'] == 5.5 ? 'selected="selected"' : '')).' >(GMT +5:30) Bombay, Calcutta, Madras, New Delhi</option>
		<option value="6" '.(POSTval('timezone') == '6' ? 'selected="selected"' : ($info['timezone'] == 6 ? 'selected="selected"' : '')).' >(GMT +6:00) Almaty, Dhaka, Colombo</option>
		<option value="6.5" '.(POSTval('timezone') == '6.5' ? 'selected="selected"' : ($info['timezone'] == 6.5 ? 'selected="selected"' : '')).' >(GMT +6:30) Yangon, Myanmar</option>
		<option value="7" '.(POSTval('timezone') == '7' ? 'selected="selected"' : ($info['timezone'] == 7 ? 'selected="selected"' : '')).' >(GMT +7:00) Bangkok, Hanoi, Jakarta</option>
		<option value="8" '.(POSTval('timezone') == '8' ? 'selected="selected"' : ($info['timezone'] == 8 ? 'selected="selected"' : '')).' >(GMT +8:00) Beijing, Perth, Singapore, Hong Kong</option>
		<option value="9" '.(POSTval('timezone') == '9' ? 'selected="selected"' : ($info['timezone'] == 9 ? 'selected="selected"' : '')).' >(GMT +9:00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk</option>
		<option value="9.5" '.(POSTval('timezone') == '9.5' ? 'selected="selected"' : ($info['timezone'] == 9.5 ? 'selected="selected"' : '')).' >(GMT +9:30) Adelaide, Darwin</option>
		<option value="10" '.(POSTval('timezone') == '10' ? 'selected="selected"' : ($info['timezone'] == 10 ? 'selected="selected"' : '')).' >(GMT +10:00) Eastern Australia, Guam, Vladivostok</option>
		<option value="11" '.(POSTval('timezone') == '11' ? 'selected="selected"' : ($info['timezone'] == 11 ? 'selected="selected"' : '')).' >(GMT +11:00) Magadan, Solomon Islands, New Caledonia</option>
		<option value="12" '.(POSTval('timezone') == '12' ? 'selected="selected"' : ($info['timezone'] == 12 ? 'selected="selected"' : '')).' >(GMT +12:00) Auckland, Wellington, Fiji, Kamchatka</option>
		</select>
	</div>
</div>
<br/>';

// CPU model edit option only available on kvm
if($master || (empty($master) && server_virt($globals['server'], 'kvm') == 'kvm')){
	echo '<div class="row">
		<div class="col-xs-10 col-sm-6">
			<label class="control-label">'.$l['cpu_nm'].'</label><br />
			<span class="help-block">'.$l['cpu_nm_exp'].'</span>
		</div>
		<div class="col-xs-2 col-sm-6">
			<input type="checkbox" value="1" class="ios" name="cpu_nm" '.POSTchecked('cpu_nm', $info['cpu_nm']).' />
		</div>
	</div>';
}

if(empty($master)){
echo '<div class="row" id="overcommitrow">
	<div class="col-sm-6">
		<label class="control-label">'.$l['overcommit'].'</label><br />
		<span class="help-block">'.$l['overcommit_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="overcommit" id="overcommit" size="20" value="'.POSTval('overcommit', $info['overcommit']).'" />
	</div>
</div>';
}

if(empty($master) && !$cluster->virt(0)){
echo '<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['vnc_ip'].'</label><br />
		<span class="help-block">'.$l['vnc_ip_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="vnc_ip" id="vnc_ip" size="20" value="'.POSTval('vnc_ip', $info['vnc_ip']).'" />
	</div>
</div>';
}

if($master){
echo '
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['enable_rdns'].'</label><br />
		<span class="help-block">'.$l['enable_rdns_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" value="1" class="ios" name="enable_rdns" '.POSTchecked('enable_rdns', @$info['enable_rdns']).' />
	</div>
</div>
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['auto_add_zone'].'</label><br />
		<span class="help-block">'.$l['auto_add_zone_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" value="1" class="ios" name="auto_add_zone" '.POSTchecked('auto_add_zone', @$info['auto_add_zone']).' />
	</div>
</div>
<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['curl_timeout'].'</label><br />
		<span class="help-block">'.$l['curl_timeout_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="curl_timeout" value="'.POSTval('curl_timeout', $info['curl_timeout']).'" />
	</div>
</div>
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['del_novps_user'].'</label><br />
		<span class="help-block">'.$l['del_novps_user_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" value="1" class="ios" name="del_novps_user" '.POSTchecked('del_novps_user', $info['del_novps_user']).' />
	</div>
</div>
<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['pass_strength'].'</label><br />
		<span class="help-block">'.$l['pass_strength_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="pass_strength" value="'.POSTval('pass_strength', $info['pass_strength']).'" />
	</div>
</div>';
}

if($master){
echo '<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['turnon_virtdf'].'</label><br />
		<span class="help-block">'.$l['turnon_virtdf_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" class="ios" name="turnon_virtdf" '.POSTchecked('turnon_virtdf', $info['turnon_virtdf']).'/>
	</div>
</div>';
}

echo '<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['del_slave_ostemplate'].'</label><br />
		<span class="help-block">'.$l['del_slave_ostemplate_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" value="1" class="ios" name="del_slave_ostemplate" '.POSTchecked('del_slave_ostemplate', $info['del_slave_ostemplate']).' />
	</div>
</div>
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['keep_orphan_template'].'</label><br />
		<span class="help-block">'.$l['keep_orphan_template_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" value="1" class="ios" name="keep_orphan_template" '.POSTchecked('keep_orphan_template', $info['keep_orphan_template']).' />
	</div>
</div>
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label" for="disable_virttop" >'.$l['disable_virttop'].'</label><br />
		<span class="help-block">'.$l['disable_virttop_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" value="1" id="disable_virttop" name="disable_virttop" '.POSTchecked('disable_virttop', $info['disable_virttop']).' />
	</div>
</div>
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['showsearch'].'</label><br />
		<span class="help-block">'.$l['showsearch_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" value="1" class="ios" name="showsearch" '.POSTchecked('showsearch', $info['showsearch']).' />
	</div>
</div>';

if($master){

echo '
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['enable_idsort'].'</label><br />
		<span class="help-block">'.$l['enable_idsort_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" value="1" class="ios" name="enable_idsort" '.POSTchecked('enable_idsort', $info['enable_idsort']).' />
	</div>
</div>

<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['default_sg_name'].'</label><br />
		<span class="help-block">'.$l['default_sg_name_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="text" class="form-control" name="default_sg_name" value="'.POSTval('default_sg_name', $info['default_sg_name']).'" />
	</div>
</div>

<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['default_sg_reseller_name'].'</label><br />
		<span class="help-block">'.$l['default_sg_reseller_name_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="text" class="form-control" name="default_sg_reseller_name" value="'.POSTval('default_sg_reseller_name', $info['default_sg_reseller_name']).'" />
	</div>
</div>

<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['default_sg_desc'].'</label><br />
		<!-- <span class="help-block">'.$l['default_sg_desc_exp'].'</span> -->
	</div>
	<div class="col-xs-2 col-sm-6">
		<textarea class="form-control" name="default_sg_desc" >'.POSTval('default_sg_desc', $info['default_sg_desc']).'</textarea>
	</div>
</div>
<br />';
}

if(empty($master) && $cluster->virt(0, 0, 0, 0, 0, 0, 0, 1)){
	echo '
<div  class="divroundshad">
<div class="roundheader">'.$l['proxmox_settings'].'</div>
<br/>
<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['proxmox_server_name'].'</label><br />
		<span class="help-block">'.$l['proxmox_server_name_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="proxmox_server_name" id="proxmox_server_name" size="30" value="'.POSTval('proxmox_server_name', $info['proxmox_server_name']).'" />
	</div>
</div>
<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['proxmox_server_pass'].'</label><br />
		<span class="help-block">'.$l['proxmox_server_pass_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="proxmox_server_pass" id="proxmox_server_pass" size="30" value="'.POSTval('proxmox_server_pass', '').'" />
	</div>
</div>

</div>
<br /><br />';

}

// Network Settings
echo '<div class="divroundshad">
<div class="roundheader">'.$l['nw_settings'].'</div>
<br/>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['interface'].'</label><br />
		<span class="help-block">'.$l['interface_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="interface" id="interface" size="30" value="'.POSTval('interface', $info['interface']).'" />
	</div>
</div>
<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['node_bandwidth'].'</label><br />
		<span class="help-block">'.$l['node_bandwidth_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="node_bandwidth" id="node_bandwidth" size="20" value="'.POSTval('node_bandwidth', $info['node_bandwidth']).'" />
	</div>
</div>
<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['vps_bandwidth_threshold'].'</label><br />
		<span class="help-block">'.$l['vps_bandwidth_threshold_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="vps_bandwidth_threshold" id="vps_bandwidth_threshold" size="20" value="'.POSTval('vps_bandwidth_threshold', $info['vps_bandwidth_threshold']).'" />
	</div>
</div>
<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['ping_ip'].'</label><br />
		<span class="help-block">'.$l['ping_ip_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="ping_ip" id="ping_ip" size="20" value="'.POSTval('ping_ip', $info['ping_ip']).'" />
	</div>
</div>';
if($master){
	echo '
	<div class="row">
		<div class="col-xs-10 col-sm-6">
			<label class="control-label">'.$l['band_suspend'].'</label><br />
			<span class="help-block">'.$l['band_suspend_exp'].'</span>
		</div>
		<div class="col-xs-2 col-sm-6">
			<input type="checkbox" value="1" class="ios" name="band_suspend" '.POSTchecked('band_suspend', $info['band_suspend']).' />
		</div>
	</div>
	<div class="row">
		<div class="col-xs-10 col-sm-6">
			<label class="control-label">'.$l['band_calc_creation'].'</label><br />
			<span class="help-block">'.$l['band_calc_creation_exp'].'</span>
		</div>
		<div class="col-xs-2 col-sm-6">
			<input type="checkbox" value="1" class="ios" name="band_calc_creation" '.POSTchecked('band_calc_creation', $info['band_calc_creation']).' />
		</div>
	</div>';
}
if(empty($master)){
	echo '
		<div class="row">
		<div class="col-xs-10 col-sm-6">
			<label class="control-label">'.$l['disable_nw_config'].'</label><br />
			<span class="help-block">'.$l['exp_disable_nw_config'].'</span>
		</div>
		<div class="col-xs-2 col-sm-6">
			<input type="checkbox" value="1" class="ios" name="disable_nw_config" '.POSTchecked('disable_nw_config', $info['disable_nw_config']).' />
		</div>
	</div>';
}
echo '
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label" for="speed_cap_disable" >'.$l['speed_cap_disable'].'</label><br />
		<span class="help-block">'.$l['speed_cap_disable_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" value="1" class="ios" id="speed_cap_disable" name="speed_cap_disable" '.POSTchecked('speed_cap_disable', $info['speed_cap_disable']).' />
	</div>
</div>';

echo '</div>'; // End of network setting DIV

echo '</div>
<div class="divroundshad">
<div class="roundheader">'.$l['haproxy_settings'].'</div>
<br/>
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label" for="haproxy_enable">'.$l['haproxy_enable'].'</label><br />
		<span class="help-block">'.$l['haproxy_enable_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" value="1" class="ios" id="haproxy_enable" name="haproxy_enable" '.POSTchecked('haproxy_enable', $info['haproxy_enable']).' />
	</div>
</div>';

if(empty($master)){
	echo '

<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label" for="haproxy_src_ips">'.$l['haproxy_src_ips'].'</label><br />
		<span class="help-block">'.$l['haproxy_src_ips_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="text" class="form-control haproxy" id="haproxy_src_ips" name="haproxy_src_ips" value="'.POSTval('haproxy_src_ips', (!empty($info['haproxy_src_ips']) ? $info['haproxy_src_ips'] : server_publicip($globals['serid']))).'" />
	</div>
</div>';
}

echo '
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label" for="haproxy_reservedports">'.$l['haproxy_reservedports'].'</label><br />
		<span class="help-block">'.$l['haproxy_reservedports_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="text" class="form-control haproxy" id="haproxy_reservedports" name="haproxy_reservedports" value="'.POSTval('haproxy_reservedports', $info['haproxy_reservedports']).'" />
	</div>
</div>
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label" for="haproxy_reservedports_http">'.$l['haproxy_reservedports_http'].'</label><br />
		<span class="help-block">'.$l['haproxy_reservedports_http_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="text" class="form-control haproxy" id="haproxy_reservedports_http" name="haproxy_reservedports_http" value="'.POSTval('haproxy_reservedports_http', (empty($info['haproxy_reservedports_http']) ? '80,443' : $info['haproxy_reservedports_http'])).'" />
	</div>
</div>
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label" for="haproxy_allowedports">'.$l['haproxy_allowedports'].'</label><br />
		<span class="help-block">'.$l['haproxy_allowedports_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="text" class="form-control haproxy" id="haproxy_allowedports" name="haproxy_allowedports" value="'.POSTval('haproxy_allowedports', $info['haproxy_allowedports']).'" />
	</div>
</div>
</div>

<div class="divroundshad">
<div class="roundheader">'.$l['novnc_settings'].'</div>
<br/>';

if($master){
echo '
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['novnc'].'</label><br />
		<span class="help-block">'.$l['novnc_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" value="1" class="ios" name="novnc" '.POSTchecked('novnc', $info['novnc']).' />
	</div>
</div>
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['novnc_master_only'].'</label><br />
		<span class="help-block">'.$l['novnc_master_only_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" value="1" class="ios" name="novnc_master_only" '.POSTchecked('novnc_master_only', $info['novnc_master_only']).' />
	</div>
</div>

<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['disable_java_vnc'].'</label><br />
		<span class="help-block">'.$l['disable_java_vnc_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" value="1" class="ios" name="disable_java_vnc" '.POSTchecked('disable_java_vnc', $info['disable_java_vnc']).' />
	</div>
</div>';
}

echo '
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['novnc_server_name'].'</label><br />
		<span class="help-block">'.$l['novnc_server_name_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" value="1" class="ios" name="novnc_server_name" '.POSTchecked('novnc_server_name', $info['novnc_server_name']).' />
	</div>
</div>
</div>
<br />';

if($master || (empty($master) && $cluster->virt(0, 1, 1, 0, 1))){

echo '<div class="divroundshad">
<div class="roundheader">'.$l['xen_settings'].'</div>
<br/>
<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['xen_dom0mem'].'</label><br />
		<span class="help-block">'.$l['xen_dom0mem_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="xen_dom0mem" id="xen_dom0mem" size="30" value="'.POSTval('xen_dom0mem', $info['xen_dom0mem']).'" style="width:50%;float:left;"/><label class="side_lbl">MB</label>
	</div>
</div>
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['pv_on_hvm'].'</label><br />
		<span class="help-block">'.$l['pv_on_hvm_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" value="1" class="ios" name="pv_on_hvm" '.POSTchecked('pv_on_hvm', $info['pv_on_hvm']).' />
	</div>
</div>';

if(empty($master)){

echo '<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['xen_pvbridge'].'</label><br />
		<span class="help-block">'.$l['xen_pvbridge_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="xen_pvbridge" id="xen_pvbridge" size="30" value="'.POSTval('xen_pvbridge', $info['xen_pvbridge']).'" />
	</div>
</div>
<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['xen_hvmbridge'].'</label><br />
		<span class="help-block">'.$l['xen_hvmbridge_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="xen_hvmbridge" id="xen_hvmbridge" size="30" value="'.POSTval('xen_hvmbridge', $info['xen_hvmbridge']).'" />
	</div>
</div>
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['use_xl'].'</label><br />
		<span class="help-block">'.$l['use_xl_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" value="1" class="ios" name="use_xl" '.POSTchecked('use_xl', $info['use_xl']).' />
	</div>
</div>
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['xen_dm_version'].'</label><br />
		<span class="help-block">'.$l['xen_dm_version_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="radio" name="xen_dm_version" value="0" '.(POSTval('xen_dm_version', $info['xen_dm_version']) == '0' ? 'checked="checked"' : '').' /> '.$l['xen_dm_version_traditional'].' &nbsp;&nbsp; 
		<input type="radio" name="xen_dm_version" value="1" '.(POSTval('xen_dm_version', $info['xen_dm_version']) == '1' ? 'checked="checked"' : '').' /> '.$l['xen_dm_version_qemu'].' <br />
	</div>
</div>';
}

echo '
</div>';
}// End of XEN

if($master || (empty($master) && $cluster->virt(1))){
	
echo '
<br />
<div  class="divroundshad">
<div class="roundheader">'.$l['openvz_settings'].'</div>
<br/>
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['vswap'].'</label><br />
		<span class="help-block">'.$l['vswap_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" value="1" class="ios" name="vswap" disabled '.POSTchecked('vswap', $info['vswap']).' />
	</div>
</div>
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['ploop'].'</label><br />
		<span class="help-block">'.$l['ploop_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" value="1" class="ios" name="ploop" '.POSTchecked('ploop', $info['ploop']).' />
	</div>
</div>
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['enable_ppp_cp'].'</label><br />
		<span class="help-block">'.$l['enable_ppp_cp_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" value="1" class="ios" name="enable_ppp_cp" '.POSTchecked('enable_ppp_cp', $info['enable_ppp_cp']).' />
	</div>
</div>
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['enable_tuntap_cp'].'</label><br />
		<span class="help-block">'.$l['enable_tuntap_cp_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" value="1" class="ios" name="enable_tuntap_cp" '.POSTchecked('enable_tuntap_cp', $info['enable_tuntap_cp']).' />
	</div>
</div>
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['enable_fuse_cp'].'</label><br />
		<span class="help-block">'.$l['enable_fuse_cp_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" value="1" class="ios" name="enable_fuse_cp" '.POSTchecked('enable_fuse_cp', $info['enable_fuse_cp']).' />
	</div>
</div>
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['enable_ipip_cp'].'</label><br />
		<span class="help-block">'.$l['enable_ipip_cp_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" value="1" class="ios" name="enable_ipip_cp" '.POSTchecked('enable_ipip_cp', $info['enable_ipip_cp']).' />
	</div>
</div>
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['enable_ipgre_cp'].'</label><br />
		<span class="help-block">'.$l['enable_ipgre_cp_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" value="1" class="ios" name="enable_ipgre_cp" '.POSTchecked('enable_ipgre_cp', $info['enable_ipgre_cp']).' />
	</div>
</div>
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['enable_nfs_cp'].'</label><br />
		<span class="help-block">'.$l['enable_nfs_cp_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" value="1" class="ios" name="enable_nfs_cp" '.POSTchecked('enable_nfs_cp', $info['enable_nfs_cp']).' />
	</div>
</div>
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['enable_quotaugidlimit_cp'].'</label><br />
		<span class="help-block">'.$l['enable_quotaugidlimit_cp_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" value="1" class="ios" name="enable_quotaugidlimit_cp" '.POSTchecked('enable_quotaugidlimit_cp', $info['enable_quotaugidlimit_cp']).' />
	</div>
</div>
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['enable_iolimit_cp'].'</label><br />
		<span class="help-block">'.$l['enable_iolimit_cp_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" value="1" class="ios" name="enable_iolimit_cp" '.POSTchecked('enable_iolimit_cp', $info['enable_iolimit_cp']).' />
	</div>
</div>
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['enable_iopslimit_cp'].'</label><br />
		<span class="help-block">'.$l['enable_iopslimit_cp_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" value="1" class="ios" name="enable_iopslimit_cp" '.POSTchecked('enable_iopslimit_cp', $info['enable_iopslimit_cp']).' />
	</div>
</div>';

if($master){
echo '<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['enable_console'].'</label><br />
		<span class="help-block">'.$l['enable_console_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" value="1" class="ios" name="enable_console" '.POSTchecked('enable_console', $info['enable_console']).' />
	</div>
</div>';
}

echo '<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['openvz_suspend_load'].'</label><br />
		<span class="help-block">'.$l['openvz_suspend_load_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="openvz_suspend_load" id="openvz_suspend_load" size="30" value="'.POSTval('openvz_suspend_load', $info['openvz_suspend_load']).'" />
	</div>
</div>';

}

if($master){

echo '<br /><div  class="divroundshad">
<div class="roundheader">'.$l['enduser_settings'].'</div>
<br/>

<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['disable_icons_monitor'].'</label><br />
		<span class="help-block">'.$l['disable_icons_monitor_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" value="1" class="ios" name="disable_icons_monitor" '.POSTchecked('disable_icons_monitor', $info['disable_icons_monitor']).' />
	</div>
</div>

<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['disable_icons_cp'].'</label><br />
		<span class="help-block">'.$l['disable_icons_cp_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" value="1" class="ios" name="disable_icons_cp" '.POSTchecked('disable_icons_cp', $info['disable_icons_cp']).' />
	</div>
</div>

<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['enable_enduser_vnc'].'</label><br />
		<span class="help-block">'.$l['enable_enduser_vnc_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" value="1" class="ios" name="enable_enduser_vnc" '.POSTchecked('enable_enduser_vnc', $info['enable_enduser_vnc']).' />
	</div>
</div>
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['show_server'].'</label><br />
		<span class="help-block">'.$l['show_server_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" value="1" class="ios" name="show_server" '.POSTchecked('show_server', $info['show_server']).' />
	</div>
</div>
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['disable_apicredential'].'</label><br />
		<span class="help-block">'.$l['exp_disable_apicredential'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" class="ios" name="disable_apicredential" id="disable_apicredential" '.POSTchecked('disable_apicredential', @$info['disable_apicredential']).' />
	</div>	
</div>

<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['disable_recipes'].'</label><br />
		<span class="help-block">'.$l['disable_recipes_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" class="ios" name="disable_recipes" '.POSTchecked('disable_recipes', $info['disable_recipes']).' />
	</div>
</div>

<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['eu_allow_virtio'].'</label><br />
		<span class="help-block">'.$l['eu_allow_virtio_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" class="ios" name="eu_allow_virtio" '.POSTchecked('eu_allow_virtio', $info['eu_allow_virtio']).' />
	</div>
</div>

<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['disable_rescue'].'</label><br />
		<span class="help-block">'.$l['disable_rescue_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" class="ios" name="disable_rescue" '.POSTchecked('disable_rescue', $info['disable_rescue']).' />
	</div>
</div>

<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['disable_change_hostname'].'</label><br />
		<span class="help-block">'.$l['disable_change_hostname_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" class="ios" name="disable_change_hostname" '.POSTchecked('disable_change_hostname', $info['disable_change_hostname']).' />
	</div>
</div>

<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['disable_change_password'].'</label><br />
		<span class="help-block">'.$l['disable_change_password_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" class="ios" name="disable_change_password" '.POSTchecked('disable_change_password', $info['disable_change_password']).' />
	</div>
</div>

<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['disable_vps_config'].'</label><br />
		<span class="help-block">'.$l['disable_vps_config_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" class="ios" name="disable_vps_config" '.POSTchecked('disable_vps_config', $info['disable_vps_config']).' />
	</div>
</div>

<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['disable_os_reinstall'].'</label><br />
		<span class="help-block">'.$l['disable_os_reinstall_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" class="ios" name="disable_os_reinstall" '.POSTchecked('disable_os_reinstall', $info['disable_os_reinstall']).' />
	</div>
</div>

<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['disable_ssh'].'</label><br />
		<span class="help-block">'.$l['disable_ssh_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" class="ios" name="disable_ssh" '.POSTchecked('disable_ssh', $info['disable_ssh']).' />
	</div>
</div>
</div>
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['disable_suspend_email'].'</label><br />
		<span class="help-block">'.$l['disable_suspend_email_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" class="ios" name="disable_suspend_email" '.POSTchecked('disable_suspend_email', $info['disable_suspend_email']).' />
	</div>
</div>
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['disable_self_shutdown'].'</label><br />
		<span class="help-block">'.$l['disable_self_shutdown_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" class="ios" name="disable_self_shutdown" '.POSTchecked('disable_self_shutdown', $info['disable_self_shutdown']).' />
	</div>
</div>
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['disable_server_location'].'</label><br />
		<span class="help-block">'.$l['disable_server_location_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" class="ios" name="disable_server_location" '.POSTchecked('disable_server_location', $info['disable_server_location']).' />
	</div>
</div>
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['disable_enduser_sshkeys'].'</label><br />
		<span class="help-block">'.$l['disable_enduser_sshkeys_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" class="ios" name="disable_enduser_sshkeys" '.POSTchecked('disable_enduser_sshkeys', $info['disable_enduser_sshkeys']).' />
	</div>
</div>
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['disable_vga'].'</label><br />
		<span class="help-block">'.$l['disable_vga_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" class="ios" name="disable_vga" '.POSTchecked('disable_vga', $info['disable_vga']).' />
	</div>
</div>
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['enable_nic'].'</label><br />
		<span class="help-block">'.$l['enable_nic_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" class="ios" name="enable_nic" '.POSTchecked('enable_nic', $info['enable_nic']).' />
	</div>
</div>
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['enable_2fa'].'</label><br />
		<span class="help-block">'.$l['enable_2fa_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" class="ios" name="enable_2fa" '.POSTchecked('enable_2fa', $info['enable_2fa']).' />
	</div>
</div>
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['2fa_email_otp'].'</label><br />
		<span class="help-block">'.$l['2fa_email_otp_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" class="ios" name="2fa_email_otp" '.POSTchecked('2fa_email_otp', $info['2fa_email_otp']).' />
	</div>
</div>
<br />

<br />
<div  class="divroundshad">
<div class="roundheader">'.$l['eu_iso_settings'].'</div>
<br/>
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['enable_eu_iso'].'</label><br />
		<span class="help-block">'.$l['enable_eu_iso_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" value="1" class="ios" name="enable_eu_iso" id="enable_eu_iso" '.POSTchecked('enable_eu_iso', $info['enable_eu_iso']).' />
	</div>
</div>
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['eu_iso_size'].'</label><br />
		<span class="help-block">'.$l['eu_iso_size_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="number" class="form-control" style="width:50%;float:left;" name="eu_iso_size" id="eu_iso_size" value="'.POSTval('eu_iso_size', (empty($info['eu_iso_size']) ? '5120' : $info['eu_iso_size'])).'"/><label class="side_lbl">MB</label>
	</div><br />
</div>
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['eu_iso_limit'].'</label><br />
		<span class="help-block">'.$l['eu_iso_limit_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="number" class="form-control" style="width:50%;float:left;" name="eu_iso_limit" id="eu_iso_limit" value="'.POSTval('eu_iso_limit', (empty($info['eu_iso_limit']) ? '3' : $info['eu_iso_limit'])).'"/>
	</div><br />
</div>
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['eu_iso_space'].'</label><br />
		<span class="help-block">'.$l['eu_iso_space_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="number" class="form-control" style="width:50%;float:left;" name="eu_iso_space" id="eu_iso_space" value="'.POSTval('eu_iso_space', (empty($info['eu_iso_space']) ? '5120' : $info['eu_iso_space'])).'"/><label class="side_lbl">MB</label>
	</div><br />
</div>

<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['eu_iso_del_hr'].'</label><br />
		<span class="help-block">'.$l['eu_iso_del_hr_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="number" class="form-control" style="width:50%;float:left;" name="eu_iso_del_hr" id="eu_iso_del_hr" value="'.POSTval('eu_iso_del_hr', (empty($info['eu_iso_del_hr']) ? 0 : $info['eu_iso_del_hr'])).'"/><label class="side_lbl">Hr</label>
	</div><br />
</div>

</div>
</div>
<br />

<div  class="divroundshad">
<div class="roundheader">'.$l['cloud_user_settings'].'</div>
<br/>
<!-- <div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['disable_cloud_suspend_options'].'</label><br />
		<span class="help-block">'.$l['disable_cloud_suspend_options_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" class="ios" name="disable_cloud_suspend_options" '.POSTchecked('disable_cloud_suspend_options', $info['disable_cloud_suspend_options']).' />
	</div>
</div>-->
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['cloud_allow_virtio'].'</label><br />
		<span class="help-block">'.$l['cloud_allow_virtio_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" class="ios" name="cloud_allow_virtio" '.POSTchecked('cloud_allow_virtio', @$info['cloud_allow_virtio']).' />
	</div>
</div>
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['disable_login_logo'].'</label><br />
		<span class="help-block">'.$l['exp_disable_login_logo'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" class="ios" name="disable_login_logo" id="disable_login_logo" '.POSTchecked('disable_login_logo', @$info['disable_login_logo']).' />
	</div>	
</div>
<br /><div  class="divroundshad">
<div class="roundheader">'.$l['cb_settings'].'</div>
<br/>
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['enable_cb'].'</label><br />
		<span class="help-block">'.$l['enable_cb_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" value="1" class="ios" name="enable_cb" '.POSTchecked('enable_cb', $info['enable_cb']).' />
	</div>
</div>
<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['cb_url'].'</label><br />
		<span class="help-block">'.$l['cb_url_exp'].'</span>
	</div>
	<div class="col-sm-6 form-gorup">
		<select name="cb_use_https" id="cb_use_https" class="form-gorup">
			<option value="0" '.ex_POSTselect('cb_use_https', '0', $info['cb_use_https']).'>http://</option>
			<option value="1" '.ex_POSTselect('cb_use_https', '1', $info['cb_use_https']).'>https://</option>
		</select>
		<input type="text" class="form-gorup" name="cb_url" id="cb_url" size="55" value="'.POSTval('cb_url', $info['cb_url']).'" />
	</div>
</div>
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['cb_hash'].'</label>
		<span class="help-block">'.$l['cb_hash_exp'].'</span>
	</div>
	<div class="col-xs-6 col-sm-6">
		<span class="val" id="cb_hash">'.$info['cb_hash'].'</span><a onclick="return reset_hash();" style="cursor:pointer;position:absolute;margin-top:3px;margin-left:5px;"><img id="resethash" src="'.$theme['images'].'refresh.png" title="'.$l['reset'].'" height="20" width="20"/></a>
	</div>
</div>
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['cb_suspend'].'</label>
		<span class="help-block">'.$l['cb_suspend_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" value="1" class="ios" name="cb_suspend" '.POSTchecked('cb_suspend', $info['cb_suspend']).' />
	</div>
</div>
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['cb_unsuspend'].'</label>
		<span class="help-block">'.$l['cb_unsuspend_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" value="1" class="ios" name="cb_unsuspend" '.POSTchecked('cb_unsuspend', $info['cb_unsuspend']).' />
	</div>
</div>
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['cb_hostname'].'</label>
		<span class="help-block">'.$l['cb_hostname_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" value="1" class="ios" name="cb_hostname" '.POSTchecked('cb_hostname', $info['cb_hostname']).' />
	</div>
</div>
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['cb_terminate'].'</label>
		<span class="help-block">'.$l['cb_terminate_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" value="1" class="ios" name="cb_terminate" '.POSTchecked('cb_terminate', $info['cb_terminate']).' />
	</div>
</div>
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['cb_changeips'].'</label>
		<span class="help-block">'.$l['cb_changeips_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" value="1" name="cb_changeips" '.POSTchecked('cb_changeips', $info['cb_changeips']).' />
	</div>
</div>
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['cb_login_whmcs_user'].'</label>
		<span class="help-block">'.$l['cb_login_whmcs_user_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" value="1" name="cb_login_whmcs_user" '.POSTchecked('cb_login_whmcs_user', $info['cb_login_whmcs_user']).' />
	</div>
</div>
<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['cb_invoice_whmcs_user'].'</label>
		<span class="help-block">'.$l['cb_invoice_whmcs_user_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" value="1" name="cb_invoice_whmcs_user" '.POSTchecked('cb_invoice_whmcs_user', $info['cb_invoice_whmcs_user']).' />
	</div>
</div>';
}

echo '<br />
<div  class="divroundshad">
<div class="roundheader">'.$l['security_settings'].'</div>
<br/>
';

if($master){
	
echo '<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['login_attempts'].'</label><br />
		<span class="help-block">'.$l['login_attempts_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="login_attempts" id="login_attempts" size="30" value="'.POSTval('login_attempts', $info['login_attempts']).'" />
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['login_ban_time'].'</label><br />
		<span class="help-block">'.$l['login_ban_time_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="login_ban_time" id="login_ban_time" size="30" value="'.POSTval('login_ban_time', $info['login_ban_time']).'" />
	</div>
</div>

<div class="row">
<div class="col-sm-6">
	<label class="control-label">'.$l['secure_allowed_ips'].'</label><br />
	<span class="help-block">'.$l['secure_allowed_ips_exp'].'</span>
</div>
<div class="col-sm-6">
	<input type="text" class="form-control" name="secure_allowed_ips" id="secure_allowed_ips" size="30" value="'.POSTval('secure_allowed_ips', $info['secure_allowed_ips']).'" />
</div>
</div>';

}

if(empty($master)){
echo '<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['change_ssh_port'].'</label><br />
		<span class="help-block">'.$l['change_ssh_port_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="change_ssh_port" id="change_ssh_port" size="30" value="'.POSTval('change_ssh_port', $info['change_ssh_port']).'" />
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['max_ssh_login'].'</label><br />
		<span class="help-block">'.$l['max_ssh_login_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="max_ssh_login" id="max_ssh_login" size="30" value="'.POSTval('max_ssh_login', $info['max_ssh_login']).'" />
	</div>
</div>';
}

echo '
</div>
<br />';

if(empty($master)){
	
	echo '<div  class="divroundshad">
		<div class="roundheader">'.$l['server_location_set'].'</div>
		<br/>
		<div class="row">
			<div class="col-sm-6">
				<label class="control-label">'.$l['location_lat'].'</label><br />
				<span class="help-block">'.$l['location_lat_exp'].'</span>
			</div>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="server_latitude" size="10" value="'.POSTval('server_latitude', $info['server_latitude']).'" />
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<label class="control-label">'.$l['location_long'].'</label><br />
				<span class="help-block">'.$l['location_long_exp'].'</span>
			</div>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="server_longitude" size="10" value="'.POSTval('server_longitude', $info['server_longitude']).'" />
			</div>
		</div>
	</div>';
}

echo '<div  class="divroundshad">
	<div class="roundheader">'.$l['addvs_io_cpu_settings'].'</div>
	<br/>
	';
	
	if($master){
		echo '<div class="row">
			<div class="col-sm-6">
				<label class="control-label">'.$l['queue_creation'].'</label><br />
				<span class="help-block">'.$l['queue_creation_exp'].'</span>
			</div>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="queue_creation" size="10" value="'.POSTval('queue_creation', @$info['queue_creation']).'" />
			</div>
		</div>
		<div class="row">
			<div class="col-xs-10 col-sm-6">
				<label class="control-label">'.$l['disable_master_vpsname'].'</label><br />
				<span class="help-block">'.$l['disable_master_vpsname_exp'].'</span>
			</div>
			<div class="col-xs-2 col-sm-6">
				<input type="checkbox" value="1" class="ios" name="disable_master_vpsname" '.POSTchecked('disable_master_vpsname', @$info['disable_master_vpsname']).' />
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<label class="control-label">'.$l['custom_mac'].'</label><br />
				<span class="help-block">'.$l['custom_mac_exp'].'</span>
			</div>
			<div class="col-sm-6">
				<div class="row">
					<div class="col-xs-12">
						<input type="text" class="form-control" id="custom_mac" name="custom_mac" placeholder="eg. 9a:f8:15" value="'.POSTval('custom_mac', $info['custom_mac']).'" />			
					</div>';
				echo '
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<label class="control-label" for="set_def_hvm" >'.$l['set_def_hvm'].'</label><br />
				<span class="help-block">'.$l['set_def_hvm_exp'].'</span>
			</div>
			<div class="col-xs-2 col-sm-6">
				<input type="checkbox" value="1" class="ios" name="set_def_hvm" id="set_def_hvm" '.POSTchecked('set_def_hvm', @$info['set_def_hvm']).' />
			</div>
		</div><br />';
	}
	
	if(empty($master)){
		echo '<div class="row">
			<div class="col-sm-6">
				<label class="control-label">'.$l['vps_limit'].'</label><br />
				<span class="help-block">'.$l['vps_limit_exp'].'</span>
			</div>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="vpslimit" id="vpslimit" size="30" value="'.POSTval('vpslimit', $info['vpslimit']).'" />
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<label class="control-label">'.$l['vcores_limit'].'</label><br />
				<span class="help-block">'.$l['vcores_limit_exp'].'</span>
			</div>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="vcores" id="vcores" size="30" value="'.POSTval('vcores', $info['vcores']).'" />
			</div>
		</div>';
	}
		
	echo '<div class="row">
			<div class="col-sm-6">
				<label class="control-label">'.$l['nice'].'</label><br />
				<span class="help-block">'.$l['nice_exp'].'</span>
			</div>
			<div class="col-sm-6">
				<select class="form-control" name="addvs_cpu_prio" id="addvs_cpu_prio">';
					echo '<option value="" '.($info['addvs_cpu_prio'] == '' ? 'selected="selected"' : '').'>None</option>';
					if($info['addvs_cpu_prio'] == ''){
						$info['addvs_cpu_prio'] = 'none';
					}
					for($i=-20; $i<20; $i++){
						echo '<option value="'.$i.'" '.ex_POSTselect('addvs_cpu_prio', "$i", $info['addvs_cpu_prio']).'>'.$i.'</option>';
					}
					
				echo '</select>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<label class="control-label">'.$l['ionice_prio'].'</label><br />
				<span class="help-block">'.$l['ionice_prio_exp'].'</span>
			</div>
			<div class="col-sm-6">
				<select class="form-control" name="addvs_io_prio" id="addvs_io_prio">';
					echo '<option value="" '.($info['addvs_io_prio'] == '' ? 'selected="selected"' : '').'>None</option>';
					if($info['addvs_io_prio'] == ''){
						$info['addvs_io_prio'] = 'none';
					}
					for($i=0; $i<8; $i++){
						echo '<option value="'.$i.'" '.ex_POSTselect('addvs_io_prio', "$i", $info['addvs_io_prio']).' >'.$i.'</option>';
					}
					
				echo '</select>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<label class="control-label">'.$l['ionice_class'].'</label><br />
				<span class="help-block">'.$l['ionice_class_exp'].'</span>
			</div>
			<div class="col-sm-6">
				<select class="form-control" name="addvs_io_class" id="addvs_io_class">';
						echo '<option value="" '.(POSTval('addvs_io_class') == '' ? 'selected="selected"' : ($info['addvs_io_class'] == '' ? 'selected="selected"' : '')).'>None</option>';
						echo'<option value="1" '.(POSTval('addvs_io_class') == 1 ? 'selected="selected"' : ($info['addvs_io_class'] == 1 ? 'selected="selected"' : '')).'>Real Time</option>
						<option value="2"  '.(POSTval('addvs_io_class') == 2 ? 'selected="selected"' : ($info['addvs_io_class'] == 2 ? 'selected="selected"' : '')).'>Best Effort</option>
						<option value="3"  '.(POSTval('addvs_io_class') == 3 ? 'selected="selected"' : ($info['addvs_io_class'] == 3 ? 'selected="selected"' : '')).'>Idle</option>
				</select>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12" align="center">
				'.$l['addvs_io_cpu_note'].'
			</div>
		</div>
</div>
<br />';

if($master){
	echo '<div  class="divroundshad">
	<div class="roundheader">'.$l['backup_settings'].'</div><br/>
		<div class="row">
			<div class="col-sm-6">
				<label class="control-label">'.$l['backup_queue'].'</label><br />
				<span class="help-block">'.$l['backup_queue_exp'].'</span>
			</div>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="backup_queue" size="10" value="'.POSTval('backup_queue', @$info['backup_queue']).'" />
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<label class="control-label">'.$l['restore_queue'].'</label><br />
				<span class="help-block">'.$l['restore_queue_exp'].'</span>
			</div>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="restore_queue" size="10" value="'.POSTval('restore_queue', @$info['restore_queue']).'" />
			</div>
		</div>
		<div class="row">
			<div class="col-xs-10 col-sm-6">
				<label class="control-label">'.$l['disable_backup_cp'].'</label><br />
				<span class="help-block">'.$l['disable_backup_cp_exp'].'</span>
			</div>
			<div class="col-xs-2 col-sm-6">
				<input type="checkbox" value="1" class="ios" name="disable_backup_cp" '.POSTchecked('disable_backup_cp', $info['disable_backup_cp']).' />
			</div>
		</div>
		<div class="row">
			<div class="col-xs-10 col-sm-6">
				<label class="control-label">'.$l['backup_email'].'</label><br />
				<span class="help-block">'.$l['backup_email_exp'].'</span>
			</div>
			<div class="col-xs-2 col-sm-6">
				<input type="text" class="form-control" name="backup_email" size="10" value="'.POSTval('backup_email', $info['backup_email']).'" />
			</div>
		</div>
		<div class="row">
			<div class="col-xs-10 col-sm-6">
				<label class="control-label">'.$l['backup_disable_success_mail'].'</label><br />
				<span class="help-block">'.$l['backup_disable_success_mail_exp'].'</span>
			</div>
			<div class="col-xs-2 col-sm-6">
				<input type="checkbox" value="1" class="ios" name="backup_disable_success_mail" '.POSTchecked('backup_disable_success_mail', $info['backup_disable_success_mail']).' />
			</div>
		</div>
	</div>';
}

echo '<div  class="divroundshad">
<div class="roundheader">'.$l['update_settings'].'</div>
<br/>
<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['auto_upd_virtualizor'].'</label><br />
		<span class="help-block">'.$l['auto_upd_virtualizor_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="radio" name="update" value="0" '.(POSTval('update', $info['update']) == '0' ? 'checked="checked"' : '').' /> '.$l['never_update'].' <br />
		<input type="radio" name="update" value="1" '.(POSTval('update', $info['update']) == '1' ? 'checked="checked"' : '').' /> '.$l['stable'].' <br />
		<input type="radio" name="update" value="2" '.(POSTval('update', $info['update']) == '2' ? 'checked="checked"' : '').' /> '.$l['release_cand'].'<br /><br />
	</div>
</div>

<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['email_upd_soft'].'</label><br />
		<span class="help-block">'.$l['email_upd_soft_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" class="ios" name="email_update" '.POSTchecked('email_update', @$info['email_update']).' />
	</div>
</div>';

if($master){
echo '<div class="row">
	<div class="col-xs-10 col-sm-6">
		<label class="control-label">'.$l['email_upd_soft_client'].'</label><br />
		<span class="help-block">'.$l['email_upd_soft_client_exp'].'</span>
	</div>
	<div class="col-xs-2 col-sm-6">
		<input type="checkbox" value="1" class="ios" name="email_update_client" '.POSTchecked('email_update_client', @$info['email_update_client']).' />
	</div>
</div>';
}

if(empty($master)){
echo '<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['cron_time'].'</label><br />
		<span class="help-block">'.$l['cron_time_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" name="cron_time" class="form-control" id="cron_time" size="30" value="'.POSTval('cron_time', $info['cron_time']).'" />
	</div>
</div>
<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['emps_cron_time'].'</label><br />
		<span class="help-block">'.$l['emps_cron_time_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" name="emps_cron_time" class="form-control" id="emps_cron_time" size="30" value="'.POSTval('emps_cron_time', $info['emps_cron_time']).'" />
	</div>
</div>';
}

echo '
</div>
<br />';

if($master){
	
echo '<div  class="divroundshad">
<div class="roundheader">'.$l['logs_settings'].'</div>
<br/>
<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['admin_logs'].'</label><br />
		<span class="help-block">'.$l['admin_logs_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="admin_logs" id="admin_logs" size="30" value="'.POSTval('admin_logs', $info['admin_logs']).'" />
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['vps_logs'].'</label><br />
		<span class="help-block">'.$l['vps_logs_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="vps_logs" id="vps_logs" size="30" value="'.POSTval('vps_logs', $info['vps_logs']).'" />
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['login_logs'].'</label><br />
		<span class="help-block">'.$l['login_logs_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="login_logs" id="login_logs" size="30" value="'.POSTval('login_logs', $info['login_logs']).'" />
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['system_logs'].'</label><br />
		<span class="help-block">'.$l['system_logs_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="system_logs" id="system_logs" size="30" value="'.POSTval('system_logs', $info['system_logs']).'" />
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['status_logs'].'</label><br />
		<span class="help-block">'.$l['status_logs_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="status_logs" id="status_logs" size="30" value="'.POSTval('status_logs', $info['status_logs']).'" />
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['bandwidth_logs'].'</label><br />
		<span class="help-block">'.$l['bandwidth_logs_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="bandwidth_logs" id="bandwidth_logs" size="30" value="'.POSTval('bandwidth_logs', $info['bandwidth_logs']).'" />
	</div>
</div>
<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['haproxy_cron_logs'].'</label><br />
		<span class="help-block">'.$l['haproxy_cron_logs_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="haproxy_cron_logs" id="haproxy_cron_logs" size="30" value="'.POSTval('haproxy_cron_logs', $info['haproxy_cron_logs']).'" />
	</div>
</div>
<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['del_tasks'].'</label><br />
		<span class="help-block">'.$l['del_tasks_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="del_tasks" id="del_tasks" size="30" value="'.POSTval('del_tasks', $info['del_tasks']).'" />
	</div>
</div>

</div>
<br />';
}

echo '
<div  class="divroundshad">
<div class="roundheader">'.$l['notify_settings'].'</div>
<br/>
<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['alert_load_limit'].'</label><br />
		<span class="help-block">'.$l['alert_load_limit_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="alert_load_limit" id="alert_load_limit" size="30" value="'.POSTval('alert_load_limit', $info['alert_load_limit']).'" />
	</div>
</div>
<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['alert_disk_limit'].'</label><br />
		<span class="help-block">'.$l['alert_disk_limit_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="alert_disk_limit" id="alert_disk_limit" size="30" value="'.POSTval('alert_disk_limit', $info['alert_disk_limit']).'" />
	</div>
</div>

</div>
<br /><br />
	
<center>
<input type="submit" name="editsettings" value="'.$l['saveconfig'].'" class="btn"/>
</center>
<br /><br />
</form>';

}

echo '</div>';

softfooter();

}

?>