<?php

//////////////////////////////////////////////////////////////
//===========================================================
// editserver_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function editserver_theme(){

global $theme, $globals, $kernel, $user, $l, $ipblocks , $cluster, $error, $saved, $serid, $serv, $servers, $servergroups;

softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-servers icon-head"></i>&nbsp; '.$l['page_head'].'</center>';

if(!empty($saved)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['saved'].'</div><br />';
}

error_handle($error);

echo '<form accept-charset="'.$globals['charset'].'" name="editserver" method="post" action="" class="form-horizontal">


<div class="row">
	<div class="col-sm-5">
		<label class="control-label">'.$l['server_name'].'</label>
		<span class="help-block">'.$l['server_name_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="server_name" id="server_name" size="30" '.(empty($serid) ? "disabled='disabled'" : '').' value="'.((!empty($serv))? $serv['server_name']: $serv['server_name']).'" />
	</div>
</div>

<div class="row">
	<div class="col-sm-5">
		<label class="control-label">'.$l['server_ip'].'</label>
		<span class="help-block">'.$l['server_ip_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="ip" id="ip" size="30" '.(empty($serid) ? "disabled='disabled'" : '').' value="'.((!empty($serv))? $serv['ip']: $serv['ip']).'" />
	</div>
</div>

<div class="row">
	<div class="col-sm-5">
		<label class="control-label">'.$l['server_pass'].'</label>
		<span class="help-block">'.$l['server_pass_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="pass" id="pass" size="30" '.(empty($serid) ? "disabled='disabled'" : '').' value="'.POSTval('pass', @$serv['pass']).'" />
	</div>
</div>

<div class="row">
	<div class="col-sm-5">
		<label class="control-label">'.$l['internal_ip'].'</label>
		<span class="help-block">'.$l['internal_ip_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="text" class="form-control" name="internal_ip" id="internal_ip" size="30" value="'.POSTval('internal_ip', @$serv['internal_ip']).'" />
	</div>
</div>

<div class="row">
	<div class="col-sm-5">
		<label class="control-label">'.$l['server_lock'].'</label>
		<span class="help-block">'.$l['server_lock_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="checkbox" class="ios" name="locked" '.POSTchecked('locked', $serv['locked']).' />
	</div>
</div>
<div class="row">
';

if(!empty($serid)){
	echo '<div class="col-sm-5">
			<label class="control-label">'.$l['server_group'].'</label>
		</div>
		<div class="col-sm-6">
			<select class="form-control" name="sgid" id="sgid" />';
			foreach($servergroups as $k => $v){
				echo '<option value="'.$v['sgid'].'" '.ex_POSTselect('sgid', $v['sgid'], $serv['sgid']).'>'.$v['sg_name'].'</option>';
			}
			
		echo'<br/></select><span class="help-block"></span></div>';
	}

echo '</div>
<center><input type="submit" name="editserver" class="btn" value="'.$l['sub_but'].'" /></center>
</form>
</div>';
softfooter();

}

?>