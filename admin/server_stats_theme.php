<?php

//////////////////////////////////////////////////////////////
//===========================================================
// server_stats_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function server_stats_theme(){

global $theme, $globals, $ckernel, $user, $l, $server_stats, $month, $servers;

softheader($l['<title>']);

$monthly_data = json_encode($server_stats);
$month = json_encode($month);

echo '
<script language="javascript" type="text/javascript"><!-- // --><![CDATA[

function graph_display(id){

	var res= id.split("_");

	if($(\'#\'+res[1]+\'_body\').is(\':visible\')){

		$(\'#\'+res[1]+\'_sign\').html(\'+\');
		$(\'#\'+res[1]+\'_body\').slideUp("fast");

	}else{

		$(\'#\'+res[1]+\'_sign\').html(\'-\');
		$(\'#\'+res[1]+\'_body\').slideDown("fast");

	}
}

//Show monthly graph
function show_monthly_graph(monthly_data, month){

	$("#note_box").css("display", "none");

	if(!empty(monthly_data)){

		var cpu_data = new Array();
		var inode_data = new Array();
		var ram_data = new Array();
		var disk_data = new Array();
		var ntw_in_data = new Array();
		var ntw_out_data = new Array();
		var ntw_total_data = new Array();
		var avg_download = 0;
		var avg_upload = 0;
		var count = 0;

		$.each(monthly_data,function(key,val){

			//Array is in format [vpsid, time, status, disk, inode, ram, cpu, actual_cpu, net_in, net_out]
			cpu_data.push([val[1], val[6]]);

			inode_data.push([val[1], val[4]]);

			ram_data.push([val[1], val[5]]);

			disk_data.push([val[1], (val[3]/1024)]);

			ntw_in_data.push([val[1], val[8]]);

			ntw_out_data.push([val[1], val[9]]);

			ntw_total_data.push([val[1], parseInt(val[8]) + parseInt(val[9])]);

			avg_download += parseInt(val.net_in);
			avg_upload += parseInt(val.net_out);
			count++;
		});

		//cpu_data.sort(function(a,b){return a-b});
		cpu_data.sort(function(a, b){
			return a[0]-b[0];
		});

		var cpu_graph = [
					{ label: "'.$l['cpu_graph_title'].'",  data: cpu_data}
				];

		var inode_graph = [
				{ label: "'.$l['inode_graph_title'].'",  data: inode_data, color: "#80b3ff"}
			];

		var ram_graph = [
				{ label: "'.$l['ram_graph_title'].'",  data: ram_data, color: "#ccff33"}
			];

		var disk_graph = [
				{ label: "'.$l['disk_graph_title'].'",  data: disk_data, color: "#ff6600"}
			];

		var ntw_graph = [
				{ label: "'.$l['download_graph_title'].'",  data: ntw_in_data, color: "#0077FF"},
				{ label: "'.$l['upload_graph_title'].'",  data: ntw_out_data, color: "#7D0096"},
				{ label: "'.$l['total_graph_title'].'",  data: ntw_total_data}
			];

		selection_zooming("cpu", cpu_graph);
		live_resource_graph("cpu", cpu_graph, flot_options("cpu"), "% at",true);
		
		selection_zooming("ram", ram_graph);
		live_resource_graph("ram", ram_graph, flot_options("ram"), "MB at",true);
		
		selection_zooming("disk", disk_graph);
		live_resource_graph("disk", disk_graph, flot_options("disk"), "GB at",true);

		selection_zooming("inodes", inode_graph);
		live_resource_graph("inodes", inode_graph, flot_options("inodes"), " at", true);

		selection_zooming("ntw", ntw_graph);
		live_resource_graph("ntw", ntw_graph, flot_options("ntw"), "", true);

		$("#data_div").css("display", "block");

	}else{
		$("#note_box").css("display", "block");
	}

	var current_year = month.current_month.substring(0,4);
	var current_month = parseInt(month.current_month.substring(4));
	var monthNames = ["'.$l['jan'].'", "'.$l['feb'].'", "'.$l['mar'].'", "'.$l['apr'].'", "'.$l['may'].'", "'.$l['jun'].'", "'.$l['jul'].'", "'.$l['aug'].'", "'.$l['sep'].'", "'.$l['oct'].'", "'.$l['nov'].'", "'.$l['dec'].'" ];

	$("#month_holder2").html(monthNames[current_month - 1] +" "+ current_year);
	$("#next_month").html(\'<a id="next_stats" class="form-control btn-primary" href="'.$globals['index'].'act=server_stats&show=\'+ month.next_month +\'">'.$l['next_month'].'</a>\');
	$("#prev_month").html(\'<a id="prev_stats" class="form-control btn-primary" href="'.$globals['index'].'act=server_stats&show=\'+ month.prev_month +\'">'.$l['prev_month'].'</a>\');

}

//lets check for selection and zooming
function selection_zooming (id, data){
	
	$("#"+id).bind("plotselected", function (event, ranges) {
		if (ranges.xaxis.to - ranges.xaxis.from < 0.00001) {ranges.xaxis.to = ranges.xaxis.from + 0.00001;}
		if (ranges.yaxis.to - ranges.yaxis.from < 0.00001) {ranges.yaxis.to = ranges.yaxis.from + 0.00001;}
		options = flot_options(id);
		plot = $.plot("#"+id, data,
			$.extend(true, {}, options, {
				xaxis: { min: ranges.xaxis.from, max: ranges.xaxis.to },
				yaxis: { min: ranges.yaxis.from, max: ranges.yaxis.to }
			})
		);
		
		// if the button is there remove and append 
		if($("#zoomOut_"+id) != undefined){
			$("#zoomOut_"+id).remove();
		}
		
		$("<input type=\'button\' style=\'position:absolute;right:15px;top:15px;opacity:0.5;\' id=\'zoomOut_\'"+ id +" value=\'Zoom Out\'>").appendTo($("#"+id)).click(function(e){
			e.preventDefault();
			options = flot_options(id);
			$.plot("#"+id, data, options);
			$("#zoomOut_"+id).remove();
		});
	});
};

function flot_options(optionOf){
	var options = {
			grid: {
				borderWidth:0,
				labelMargin:0,
				axisMargin:0,
				minBorderMargin:0
			},
			legend: {
				show: true,
				noColumns: 3,
			},
			series: {
				lines: {
					show: true,
					lineWidth: 0.07,
					fill: true
				}
			},
			xaxis: {
				show:true,
				mode: "time",
				tickFormatter: function (v, axis) {
					return nDate(v,"m/d");
				},
				axisLabelUseCanvas: true,
				axisLabelFontSizePixels: 12,
				axisLabelFontFamily: \'Verdana, Arial\',
				axisLabelPadding: 10,
			},
			yaxis: {
				show:true,
				min: 0,
				max: null,
				axisLabelUseCanvas: true,
				axisLabelFontSizePixels: 12,
				axisLabelFontFamily: \'Verdana, Arial\',
			},
			selection: {
				mode: "x"
			},	
			grid: {
				borderWidth: 1,
				borderColor: \'#FFF\',
				hoverable: true,
			}
		};
		
		if(optionOf == "cpu"){
		
			//Appending options for cpu
			options.yaxis.tickFormatter = function (v) {
				if(v <= 1024)
					return Math.round(v) + " %";
			};
			options.legend.container = $("#legend_cpu");
		
		}else if(optionOf == "ram"){
			
			//Appending options for ram
			options.yaxis.tickFormatter = function (v) {
				if(v <= 1024)
					return Math.round(v) + " MB";
				if(v > 1024 && v < (1024*1024))
					return Math.round(v /1024) + " GB";
				if(v > (1024*1024))
					return Math.round(v / (1024*1024)) + " TB"
			};
			options.legend.container = $("#legend_ram");
			
		}else if(optionOf == "disk"){
		
			//Appending options for Disk
			options.yaxis.tickFormatter = function (v) {
				if(v <= 1024)
					return Math.round(v) + " GB";
				if(v > 1024 && v < (1024*1024))
					return Math.round(v /1024) + " TB";
			};
			options.legend.container = $("#legend_disk");
		
		}else if(optionOf == "inodes"){
		
			//Appending option for INodes
			options.yaxis.tickFormatter = "";
			options.legend.container = $("#legend_inodes");
		
		}else if(optionOf == "ntw"){
		
			//Appending  option for Network
			options.yaxis.tickFormatter = function (v) {
				if(v <= 1024)
					return Math.round(v) + " B/s";
				if(v > 1024 && v <= (1024*1024))
					return Math.round(v / 1024) + " KB/s";
				if(v > (1024*1024) && v <= (1024*1024*1024))
					return Math.round(v / (1024*1024)) + " MB/s";
				if(v > (1024*1024*1024))
					return Math.round(v / (1024*1024*1024)) + " GB/s";
			};
			options.legend.container = $("#legend_ntw");
			
		}
		
		return options;
};

function search_result(mon){

	//Display loading image
	$("#img_loading").html("<img class=\'form-label\' src='.$theme['images'].'loading_35.gif />");
	$("#note_box").css("display", "none");

	$.post("'.$globals['index'].'act=server_stats&jsnohf=1&api=json&svs='.$globals['server'].'&show=" + mon +"", function(data){
		data = JSON.parse(data);
		if(data.server_stats){
			show_monthly_graph(data.server_stats, data.month);
			$("#data_div").css("display", "block");
		}else{
			$("#note_box").css("display", "block");
		}
		$("#img_loading").html("");
	});

};

$(document).ready(function() {

	show_monthly_graph('.$monthly_data.', '.$month.');

});

</script>
<br />

<div class="col-sm-12 bg">
	<center class="tit"><i class="icon icon-server_stats icon-head"></i>&nbsp; '.$l['page_head'].'</center>
	<br />';
	server_select();
	
	echo '<div class="row">
		<div class="col-sm-12">
			<div class="notice" id="note_box" style="display:none;"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['no_data'].'</div>
		</div>
	</div>

	<div class="form-group" id="per_month_display">
		<div id="prev_month" style="align:left" class="col-sm-4"></div>
		<div id="month_holder2" class="col-sm-4" style="text-align:center;font-size:30px;"></div>
		<div id="next_month" style="align:right" class="col-sm-4"></div>
	</div> <br /><br /><br />

	<div id="data_div" style="display:none;">
		<div class="row">
			<div class="col-sm-12">
				<div>
					<div id="flip_cpu" class="roundheader" onclick="graph_display(this.id)">
						<label id="cpu_sign">-</label>
						<label>'.$l['cpu_label'].'</label>
					</div>
					<br />
					<center>
						<div id="cpu_body" class="box_shadow" style="padding:1%;">
							<div class="legend_container" id="legend_cpu"></div>
							<div id="cpu" style="width:100%; height:300px; padding:6px;"></div>
						</div>
					</center>
				</div><br />
				<div>
					<div id="flip_ram" class="roundheader"  onclick="graph_display(this.id)">
						<label id="ram_sign">-</label>
						<label>'.$l['ram_label'].'</label>
					</div>
					<br />
					<center>
						<div id="ram_body" class="box_shadow" style="padding:1%">
							<div class="legend_container" id="legend_ram"></div>
							<div id="ram" style="width:100%; height:300px; padding:6px;"></div>
						</div>
					</center>
				</div><br />
				<div>
					<div id="flip_disk" class="roundheader"  onclick="graph_display(this.id)">
						<label id="disk_sign">-</label>
						<label>'.$l['disk_label'].'</label>
					</div><br />

					<center>
						<div id="disk_body" class="box_shadow" style="padding:1%">
							<div class="legend_container" id="legend_disk"></div>
							<div id="disk" style="width:100%; height:300px; padding:6px;"></div>
							<br />
							<div class="legend_container" id="legend_inodes"></div>
							<div id="inodes" style="width:100%; height:300px; padding:6px;"></div>
						</div>
					</center>
				</div><br />

				<div>
					<div id="flip_ntw" class="roundheader" onclick="graph_display(this.id)">
						<label id="ntw_sign">-</label>
						<label >'.$l['ntw_label'].'</label>
					</div>
					<br />
					<center>
						<div id="ntw_body" class="box_shadow" style="padding:1%">
							<div class="legend_container" id="legend_ntw"></div>
							<div id="ntw" style="width:100%; height:300px; padding:6px;"></div>
						</div>
					</center>
				</div>
			</div>
		</div>
	</div>
</div>';

softfooter();

}

?>
