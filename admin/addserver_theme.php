<?php

//////////////////////////////////////////////////////////////
//===========================================================
// addserver_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function addserver_theme(){

global $theme, $globals, $kernel, $user, $l, $ipblocks , $cluster, $error, $saved, $servergroups, $servers;

softheader($l['<title>']);

echo '
<div class="bg">		
<center class="tit"><i class="icon icon-servers icon-head"></i>&nbsp; '.$l['add_server'].'<span style="float:right;" ><a href="'.$globals['docs'].'Add_Server" target="_blank" class="wiki_help" title="'.$l['wiki_help'].'"><i class="icon-help" ></i></a></span></center>';

if(!empty($saved)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['added'].'</div><br />';
}

error_handle($error);

foreach($servergroups as $k => $v){
	$sg_ha[$k] = $v['sg_ha'];
	
	if(!empty($sg_ha[$k])){
		$sg_ha_on[$k] = $v['sg_ha'];
	}
}

echo '<script language="javascript" type="text/javascript">
	
	function isHa(){
		
		var sgid = parseInt(document.getElementById("sgid").value);
		
		var sg_ha = ['.implode(',', array_keys($sg_ha_on)).'];
		if(sg_ha.indexOf(sgid) != -1){
			alert("'.$l['ha_install'].'");
		}
	}
</script>


<div id="form-container">


<form accept-charset="'.$globals['charset'].'" name="addippool" method="post" action="" class="form-horizontal">

  <div class="row">
    <div class="col-sm-5">
      <label class="control-label">'.$l['server_name'].'</label>
      <span class="help-block">'.$l['server_name_exp'].'</span>
    </div>
    <div class="col-sm-6">
      <input type="text" class="form-control" name="server_name" id="server_name" size="30" value="'.POSTval('server_name','').'"/>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-5">
      <label class="control-label">'.$l['server_ip'].'</label>
      <span class="help-block">'.$l['server_ip_exp'].'</span>
    </div>
    <div class="col-sm-6">
      <input type="text" class="form-control" name="ip" id="ip" size="30" value="'.POSTval('ip', '').'" />
    </div>
  </div>
  <div class="row">
    <div class="col-sm-5">
      <label class="control-label">'.$l['server_pass'].'</label>
      <span class="help-block">'.$l['server_pass_exp'].'</span>
    </div>
    <div class="col-sm-6">
      <input type="text" class="form-control" name="pass" id="pass" size="30" value="'.POSTval('pass', '').'" />
    </div>
  </div>
  <div class="row">
    <div class="col-sm-5">
      <label class="control-label">'.$l['internal_ip'].'</label>
      <span class="help-block">'.$l['internal_ip_exp'].'</span>
    </div>
    <div class="col-sm-6">
      <input type="text" class="form-control" name="internal_ip" id="internal_ip" size="30" value="'.POSTval('internal_ip', @$serv['internal_ip']).'" />
    </div>
  </div>
  <div class="row">
    <div class="col-sm-5 col-xs-10">
      <label class="control-label">'.$l['server_lock'].'</label>
      <span class="help-block">'.$l['server_lock_exp'].'</span>
    </div>
    <div class="col-sm-6 col-xs-2">
      <input type="checkbox" class="ios" name="locked" '.POSTchecked('locked').' />
    </div>
  </div>
  <div class="row">
    <div class="col-sm-5">
      <label class="control-label">'.$l['server_group'].'</label>
      <span class="help-block">'.$l['server_group_exp'].'</span>
    </div>
    <div class="col-sm-6 server-select-lg">
      <select name="sgid" id="sgid" class="form-control virt-select" style="width:100%" onchange="isHa();">';
        foreach($servergroups as $k => $v){
          echo '<option value="'.$v['sgid'].'">'.$v['sg_name'].'</option>';
        }
      echo'</select>
    </div>
  </div>
<br /><br />
<center><input type="submit" name="addserver" class="btn" value="'.$l['sub_but'].'" /></center>	
</form>	
</div>
</div>';
softfooter();

}

?>