<?php

//////////////////////////////////////////////////////////////
//===========================================================
// add_distro_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function add_distro_theme(){

global $theme, $globals, $kernel, $user, $l, $oslist , $error, $done, $mgs, $distros, $editdistro;

softheader(empty($editdistro) ? $l['<title_add>'] : $l['<title_edit>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-ostemplates icon-head"></i> &nbsp; '.(empty($editdistro) ? $l['tit_add_distro'] : $l['tit_edit_distro']).'</center>';

error_handle($error);

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
}

echo '<div id="form-container">
<form accept-charset="'.$globals['charset'].'" id="add_distro" name="add_distro" method="post" action="" class="form-horizontal">
<div class="row">
	<div class="col-sm-5"><label class="control-label">'.$l['distro'].'</label></div>
	<div class="col-sm-6">
		<input type="text" id="distro" class="form-control" name="distro" value="'.(!empty($editdistro) ? @$distros[$editdistro]['distro'] : POSTval('distro', @$distros[$editdistro]['distro'])).'" '.(!empty($editdistro) ? 'disabled="disabled"' : '').'/>
		<span class="help-block"></span>
	</div>
</div>
<div class="row">
	<div class="col-sm-5"><label class="control-label">'.$l['distro_name'].'</label><br />
	<span class="help-block">'.$l['distro_name_exp'].'</span></div>
	<div class="col-sm-6">
		<input type="text" id="distro_name" class="form-control" name="distro_name" value="'.POSTval('distro_name', @$distros[$editdistro]['name']).'">
		<span class="help-block"></span>
	</div>
</div>
<div class="row" id="distro_desc">
	<div class="col-sm-5">
		<label class="control-label">'.$l['distro_desc'].'</label><br />
	</div>
	<div class="col-sm-6">
		<textarea class="form-control" name="distro_desc" rows="4" cols="50">'.POSTval('distro_desc', @$distros[$editdistro]['desc']).'</textarea>
		<span class="help-block"></span>
	</div>
</div>
<div class="row">
	<div class="col-sm-5">
		<label class="control-label">'.$l['distro_logo'].'</label><br />
		<span class="help-block">'.$l['distro_logo_exp'].'</span>
	</div>
	<div class="col-sm-6"><input type="text" class="form-control" name="distro_logo" id="distro_logo" size="40" value="'.POSTval('distro_logo', @$distros[$editdistro]['logo']).'"/></div>
</div>
<div class="row">
	<div class="col-sm-5">
		<label class="control-label">'.$l['distro_screenshot'].'</label><br />
		<span class="help-block">'.$l['distro_screenshot_exp'].'</span>
	</div>
	<div class="col-sm-6"><input type="text" class="form-control" name="distro_screenshot" id="distro_screenshot" size="40" value="'.POSTval('distro_screenshot', @$distros[$editdistro]['screenshot']).'"/></div>
</div>
<br /><br />
<center><input type="submit" name="add_distro" value="'.(empty($editdistro) ? $l['submit_add'] : $l['submit_edit']).'" class="btn"></center>
<br /><br />
</form>
</div>';

softfooter();

}

?>