<?php

//////////////////////////////////////////////////////////////
//===========================================================
// filemanager_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////


if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function filemanager_theme(){	

global $theme, $globals, $cluster, $servers, $user, $l, $files;
	
// Is AJAX mode on
if(optGET('ajax')){	
	
	echo 'var $files = new Array();';
	
	$i = 0;
	foreach($files as $k => $v){
		echo '$files['.$i.'] = new Object();
		$files['.$i.'].name = "'.$v['name'].'";
		$files['.$i.'].path = "'.urlencode($k).'";
		$files['.$i.'].dir = "'.$v['dir'].'";
		$files['.$i.'].sname = "'.stringLimit($v['name'], 15).'";';
		$i++;
	}
	
	return true;

}

// Normal File mode
softheader($l['<title>']);

echo '
<div class="bg" style="width:99%">
<center class="tit"><i class="icon icon-filemanager icon-head"></i>&nbsp; '.$l['<title>'].'<span style="float:right;" ><a href="'.$globals['docs'].'Server_File_Manager" target="_blank" class="wiki_help" title="'.$l['wiki_help'].'"><i class="icon-help" ></i></a></span></center>';

// Is it offline ?
$hypervisor_status = $cluster->statewise($globals['server']);
if($hypervisor_status == 0 || $hypervisor_status == 2){

	echo '<div class="e_notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['server_status_'.$hypervisor_status].'</div>';
	
}else{

	$path = trim(optGET('path'), '/');
	echo 'Path : <input type="text" size="30" id="cur_path" value="/'.$path.'" /> &nbsp; &nbsp;<input type="button" onclick="loaddir($_(\'cur_path\').value);" value="Go" /> &nbsp; 
	<a href="'.$globals['ind'].'act=filemanager&path='.urlencode(dirname($path)).'" onclick="return updir();"><img src="'.$theme['images'].'up.gif" /></a>
	<br /><br />';
	echo '<div class="filemanager fileicon" id="files">';
	
	if(count($files) > 0){
	
		//r_print($files);
		foreach($files as $k => $v){
			if(!empty($v['dir'])){
				echo '<a href="'.$globals['ind'].'act=filemanager&path='.urlencode($k).'" title="'.$v['name'].'" onclick="return loaddir(\''.urlencode($k).'\')">';
			}else{
				echo '<a href="javascript:void(0);" title="'.$v['name'].'">';
			}
			
			echo '<img src="'.$theme['images'].(empty($v['dir']) ? 'file.gif' : 'folder.gif').'" /><br />
			'.stringLimit($v['name'], 15).'
			</a>';
		}
		
	}else{
		
		echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['nothing_in'].'</div>';
		
	}
	
	echo '</div>';
	
	echo '<script language="javascript" type="text/javascript"><!-- // --><![CDATA[
function loaddir(path){
	if(AJAX("'.$globals['index'].'act=filemanager&ajax=true&path="+path, "updatediv(re)")){
		$_("cur_path").value = unescape(path);
		return false;
	}else{
		return true;	
	}
}

function updatediv(re){
	eval(re);
	var str = "";
	if($files.length > 0){
		for(x in $files){
			if($files[x].dir == 1){
				str += \'<a href="'.$globals['ind'].'act=filemanager&path=\'+$files[x].path+\'" title="\'+$files[x].name+\'" onclick="return loaddir(\\\'\'+$files[x].path+\'\\\')">\';
			}else{
				str += \'<a href="javascript:void(0);" title="\'+$files[x].name+\'">\';
			}
			
			str += \'<img src="'.$theme['images'].'\'+($files[x].dir == "1" ? "folder.gif" : "file.gif")+\'" /><br />\';
			str += $files[x].sname;
			str += \'</a>\';
		}
	}else{
		str = \'<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['nothing_in'].'</div>\';
	}
	$_("files").innerHTML = str;
}

function updir(){
	path = $_("cur_path").value;
	if(path != "/"){
		path = path.replace(/\\\\/g,"/").replace(/\\/[^\\/]*$/, "");
		path = escape(path);
		if(path.length == 0){
			path = "/";
		}
		loaddir(path);
	}
	return false;
};

// ]]></script>';

}

echo '</div>';

softfooter();
 
}

?>