<?php

//////////////////////////////////////////////////////////////
//===========================================================
// addvs_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function addvs_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $cluster, $servers, $oslist, $done;
global $plans, $ips, $ips6, $ips6_subnet, $ips_int, $ipools, $servers, $users, $isos, $_virt, $virt, $hvm, $newvs, $resources;
global $actid, $flag_createvps, $flag_ubcsettings, $done_data, $mgs, $servergroups, $scripts, $iscripts, $storages, $stid, $disk_space, $os_check, $disks, $dnsplans, $recipes, $supported_nics, $cpu_modes, $backup_plans;
global $iscripts_allowed, $webuzo_apps, $isfree, $webuzo_templates, $allowed_scripts, $webuzo_done, $bus_driver, $ha_enabled;

if($flag_createvps){
	
	echo 'actid='.$actid.';
	ubc_redirect='.$flag_ubcsettings.';';
	$flag_createvps = 0;
	return;
}

//Is Ajax Mode on?
if(optGET('ajax')){

	$planid = optGET('plan');
	$uid  = (int) optGET('uid');
	if(!empty($uid)){
		// Make the ISOs list as per user
		useriso($uid);
		$options_eu_iso = $options_iso = $options = '';
		$options = '<option value="0">'.$l['none'].'</option>';
		foreach($isos as $k => $v){
			if(!empty($v['isuseriso'])){
				$options_eu_iso .= '<option value="'.$k.'">'.$v['name'].'</option>';
			}else{
				$options_iso .= '<option value="'.$k.'">'.$v['name'].'</option>';
			}
		}

		if(!empty($options_iso)){
			$options .= '<optgroup label="'.$l['admin_iso'].'">'.$options_iso.'</optgroup>';
		}
		
		if(!empty($options_eu_iso)){
			$options .= '<optgroup label="'.$l['eu_iso'].'">'.$options_eu_iso.'</optgroup>';
		}
		echo $options;
		return;
	}
	
	echo 'var $plan = new Object();';
	
	foreach($plans[$planid] as $k => $v){
		
		if($k == 'dns_nameserver'){
			$tmp_dns = unserialize($v);
			echo '$plan["dns_nameserver"] = '.json_encode($tmp_dns).';';
			continue;
		}
		
		if($k == 'mgs'){
			$tmp_mgs = cexplode(',', $v);
			echo '$plan["mgs"] = '.json_encode($tmp_mgs).';';
			continue;
		}
		
		if($k == 'speed_cap' && !empty($v)){
			$v = _unserialize($v);
			echo '$plan["speed_cap_down"] = '.$v['down'].';
				$plan["speed_cap_up"] = '.$v['up'].';
			';
			continue;
		}		
	
		if($k == 'ippoolid'){
			echo '$plan["plan_ips"] = '.json_encode($ips).';';
			echo '$plan["internal_ips"] = '.json_encode($ips_int).';';
			continue;
		}
		
		echo '$plan["'.$k.'"] = "'.str_replace('"', '\"', $v).'";';
	}
	
	return true;
}

if(optGET('done')){

	$done = $done_data['newvs']['vpsid'];
	$vps_name = $done_data['newvs']['vps_name'];
	$root_pass = $done_data['newvs']['pass'];
	$vnc = $done_data['newvs']['vnc_details'];
	$vnc_pass = $done_data['newvs']['vncpass'];
	$ips = $done_data['newvs']['ips'];
	$ipsv6 = $done_data['newvs']['ipv6'];
	$ipsv6_subnet = $done_data['newvs']['ipv6_subnet'];

	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
	
	echo '<div style="background-color: #FAFBD9;">
	<div class="row">
		<div class="col-sm-12" style="padding:5px">
			<div class="col-sm-2"></div>
			<div class="col-sm-4">
				<span class="fhead">'.$l['newvs_vpsid'].'</span>
			</div>
			<div class="col-sm-4">'.$done.'</div>
			<div class="col-sm-2"></div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12" style="padding:5px">
			<div class="col-sm-2"></div>
			<div class="col-sm-4">
				<span class="fhead">'.$l['newvs_vps_name'].'</span>
			</div>
			<div class="col-sm-4">'.$vps_name.'</div>
			<div class="col-sm-2"></div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12" style="padding:5px">
			<div class="col-sm-2"></div>
			<div class="col-sm-4">
				<span class="fhead">'.$l['newvs_rootpass'].'</span>
			</div>
			<div class="col-sm-4" id="newvs_rootpass">'.$root_pass.'</div>
			<div class="col-sm-2"></div>
		</div>
	</div>';

	if($vnc){

		echo '<div class="row">
			<div class="col-sm-12" style="padding:5px">
				<div class="col-sm-2"></div>
				<div class="col-sm-4">
					<span class="fhead">'.$l['newvs_vnc'].'</span>
				</div>
				<div class="col-sm-4">'.$vnc.'</div>
				<div class="col-sm-2"></div>
			</div>
		</div>';
	
		if(!empty($vnc_pass)){
			echo '
			<div class="row">
				<div class="col-sm-12" style="padding:5px">
					<div class="col-sm-2"></div>
					<div class="col-sm-4">
						<span class="fhead">'.$l['newvs_vncpass'].'</span>
					</div>
					<div class="col-sm-4" id="newvs_vncpass">'.$vnc_pass.'</div>
					<div class="col-sm-2"></div>
				</div>
			</div>';
		}
	}

	echo '
	<div class="row">
		<div class="col-sm-12" style="padding:5px">
			<div class="col-sm-2"></div>
			<div class="col-sm-4">
				<span class="fhead">'.$l['newvs_ips'].'</span>
			</div>
			<div class="col-sm-4">'.implode(', ', $ips).'</div>
			<div class="col-sm-2"></div>
		</div>
	</div>';

	if(!empty($ipsv6)){

		echo '<div class="row">
			<div class="col-sm-12" style="padding:5px">
				<div class="col-sm-2"></div>
				<div class="col-sm-4">
					<span class="fhead">'.$l['newvs_ips6'].'</span>
				</div>
				<div class="col-sm-4">'.implode(', ', $ipsv6).'</div>
				<div class="col-sm-2"></div>
			</div>
		</div>';
	}

	if(!empty($ipsv6_subnet)){
		echo '
		<div class="row">
			<div class="col-sm-12" style="padding:5px">
				<div class="col-sm-2"></div>
				<div class="col-sm-4">
					<span class="fhead">'.$l['newvs_ips6_subnet'].'</span>
				</div>
				<div class="col-sm-4">'.implode(', ', $ipsv6_subnet).'</div>
				<div class="col-sm-2"></div>
			</div>
		</div>';
	}

	echo '</div>';
	return true;
}

softheader($l['<title>']);
	
// Is it offline ?
$hypervisor_status = $cluster->statewise($globals['server']);
if($hypervisor_status == 0 || $hypervisor_status == 2){
	echo '<div class="bg">';
	echo '<center class="tit"><i class="icon icon-vs icon-head"></i>&nbsp; '.$l['add_vs'].'</center>';
	server_select();
	echo '<div class="e_notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['server_status_'.$hypervisor_status].'</div>';
	echo '</div>';
}else{

if(!empty($done)){

	echo '<div class="bg"><div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
	
	echo '<div style="background-color: #FAFBD9;">
	<div class="row">
		<div class="col-sm-12" style="padding:5px">
			<div class="col-sm-2"></div>
			<div class="col-sm-4">
				<span class="fhead">'.$l['newvs_vpsid'].'</span>
			</div>
			<div class="col-sm-4">'.$done.'</div>
			<div class="col-sm-2"></div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12" style="padding:5px">
			<div class="col-sm-2"></div>
			<div class="col-sm-4">
				<span class="fhead">'.$l['newvs_vps_name'].'</span>
			</div>
			<div class="col-sm-4">'.$newvs['vps_name'].'</div>
			<div class="col-sm-2"></div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12" style="padding:5px">
			<div class="col-sm-2"></div>
			<div class="col-sm-4">
				<span class="fhead">'.$l['newvs_rootpass'].'</span>
			</div>
			<div class="col-sm-4" id="newvs_rootpass">'.$newvs['pass'].'</div>
			<div class="col-sm-2"></div>
		</div>
	</div>';

	if($newvs['vnc']){

		echo '<div class="row">
			<div class="col-sm-12" style="padding:5px">
				<div class="col-sm-2"></div>
				<div class="col-sm-4">
					<span class="fhead">'.$l['newvs_vnc'].'</span>
				</div>
				<div class="col-sm-4">'.$servers[$globals['server']]['ip'].':'.vncPort($newvs).'</div>
				<div class="col-sm-2"></div>
			</div>
		</div>';
	
		if(!empty($vnc_pass)){
			echo '
			<div class="row">
				<div class="col-sm-12" style="padding:5px">
					<div class="col-sm-2"></div>
					<div class="col-sm-4">
						<span class="fhead">'.$l['newvs_vncpass'].'</span>
					</div>
					<div class="col-sm-4">'.$newvs['vncpass'].'</div>
					<div class="col-sm-2"></div>
				</div>
			</div>';
		}
	}

	echo '
	<div class="row">
		<div class="col-sm-12" style="padding:5px">
			<div class="col-sm-2"></div>
			<div class="col-sm-4">
				<span class="fhead">'.$l['newvs_ips'].'</span>
			</div>
			<div class="col-sm-4">'.implode(', ', $newvs['ips']).'</div>
			<div class="col-sm-2"></div>
		</div>
	</div>';

	if(!empty($newvs['ipv6'])){

		echo '<div class="row">
			<div class="col-sm-12" style="padding:5px">
				<div class="col-sm-2"></div>
				<div class="col-sm-4">
					<span class="fhead">'.$l['newvs_ips6'].'</span>
				</div>
				<div class="col-sm-4">'.implode(', ', $newvs['ipv6']).'</div>
				<div class="col-sm-2"></div>
			</div>
		</div>';

	}

	echo '</div></div>';	

}else{

echo '<div class="bg">
<center class="tit"><i class="icon icon-vs icon-head"></i>&nbsp; '.$l['add_vs'].'<span style="float:right;" ><a href="'.$globals['docs'].'Creating_A_VPS" target="_blank" class="wiki_help" title="'.$l['wiki_help'].'"><i class="icon-help" ></i></a></span></center>';
error_handle($error);

echo '<script language="javascript" type="text/javascript"><!-- // --><![CDATA[
	var regexp = /^actid/;
	var actid = null;
	var progress = 0;
	var progress_update = "";
	var timer = null;
	var newvs_rootpass = "";
	var newvs_vncpass = "";
	//var ubc_redirect = 0;
	
	function change_checkbox(id){	
		if(is_checked(id)){
			$("#hidden_"+id).val(1);
		}else{
			$("#hidden_"+id).val(0);
		}
	}
	
	function show_webuzo(cp){
		if(cp != "webuzo"){
			$("#show_webuzo_tr").hide();
			$("#show_webuzo_tr input[name=webuzo_os]").prop("disabled", true);
			return false;
		}
		
		$("#show_webuzo_tr").show();
		$("#show_webuzo_tr input[name=webuzo_os]").prop("disabled", false);
		ishvm();
	}
	

	function webuzo_apps(stack){
		stack = stack || 0;
		
		if($("#stack1").data("demo") === "1"){
			$("#webuzo_stack_tr").hide();
			return;
		}
		$("#webuzo_stack_tr").slideDown("slow");	
		if(stack == \'lamp\'){
			$("#webuzo_webserver_tr").show();
		}else{
			$("#webuzo_webserver_tr").hide();
		}
	}
	
	function script_req(sid){
	
		if(sid == 0 || $("#script_isfree").val() > 0){
			return;
		}
		$("#stack1, #stack2, #stack3").prop("checked", false);
		
		processing_symb(1);
		
		if(AJAX("'.$globals['index'].'act=addvs&api=json&scriptreqid="+sid, "scriptdetails(re, true)")){
			return false;
		}else{
			return true;
		}
	};

	function scriptdetails(re, scriptreq){
		
		processing_symb();
		var re = $.parseJSON(re);
		scriptreq = scriptreq || false;
		
		if("error" in re){
			alert(re["error"]);
			$("#webuzo_operation").hide();
			$("#webuzo_unsupport").show().text(re["error"]);
		}else{
			$("#webuzo_operation").show();
			$("#webuzo_unsupport").hide().text("");
		}
			
		if("webuzo_apps" in re["webuzo_done"]){
			for(x in re["webuzo_done"]["webuzo_apps"]){
				var id = "webuzo_"+x;
				var str = "";
				var val = re["webuzo_done"]["webuzo_apps"][x];
				for(k in val){
					str += \'<div class="col-sm-4"><table><tr><td><input type="radio" name="\'+x+\'" id="serverver_\'+k+\'" value="\'+val[k][\'softname\']+\'" /></td><td width="12%"></td><td><label class="_label" for="serverver_\'+k+\'">\'+val[k][\'fullname\']+\'</label></td></tr></table></div>\';
				}
				
				$("#"+id).html(str);
			}
		}
		
		if("isfree" in re["webuzo_done"]){
			$("#stack2_tr, #stack3_tr").hide();
			$("#stack1").prop("checked", true).data("demo", "1");
			$("#script_isfree").val(1);
		}else{
			$("#stack2_tr, #stack3_tr").show();
			$("#stack1").data("demo", "0");
		}
		
		if(scriptreq == true){
			return;
		}
		
		var options = \'<option value="0">'.$l['none'].'</option>\';
		if("allowed_scripts" in re["webuzo_done"]){	
			for(x in re["webuzo_done"][\'allowed_scripts\']){
				options += \'<optgroup label="\'+x+\'">\';
				var val = re["webuzo_done"][\'allowed_scripts\'][x];
				for(k in val){
					options += \'<option value="\'+k+\'">\'+val[k][\'name\']+\'</option>\';
				}
				options += \'</optgroup>\';
			}
		}
		$("#webuzo_scriptlist").html(options).trigger("chosen:updated");
	}
	
	$(document).ready(function(){
		$("#addvsform .chosen").chosen({width: "100%"});
		$("#hvm").change(function(data){
			if(data.target.checked){
				$("#tr_viftype").show();
			}
			else if(!data.target.checked){
				$("#tr_viftype").hide();
			}
			
		});
		
		$("#hvm").change(function(data){
			if(data.target.checked){
				$("#tr_pvonhvm").show();
			}
			else if(!data.target.checked){
				$("#tr_pvonhvm").hide();
			}
			
		});';
		
		if($virt != 'xenhvm'){
			echo '$("#tr_viftype").hide();';
			echo '$("#tr_pvonhvm").hide();';
		}
		
	
		echo '$("#addvsform").submit(function(){
			
			var space = new Array();
			var size = new Array();
			var storage = new Array();
			var tmp_space = new Object();
			
			size = $(".size").map(function(){
				return this.value;
			}).get();
			
			storage = $(".storages").map(function(){
				return this.value;
			}).get();
			
			if($(".bus_driver")){
				bus_driver = $(".bus_driver").map(function(){
					return this.value;
				}).get();
			
				bus_driver_num = $(".bus_driver_num").map(function(){
					return this.value;
				}).get();
			}		
			
			 for(i = 0; i < $(".size").length; i++){
				 
		 
				if($(".bus_driver")){
					tmp_space[i] = {size: size[i], st_uuid: storage[i], bus_driver: bus_driver[i], bus_driver_num: bus_driver_num[i]};
				}else{
					tmp_space[i] = {size: size[i], st_uuid: storage[i]};
				}
				
				space.push(tmp_space[i]);
			}
			
			var json_data = JSON.stringify(space);
			
			//Reset this to zero on every new submit
			progress = 0;
			var params = $("#addvsform").serialize();
			newvs_vncpass = $("#vncpass").val();
			newvs_rootpass = $("#rootpass").val();
			
			$.ajax({type: "POST",
				url: "'.$globals['index'].'jsnohf=1&act=addvs'.(!empty($_REQUEST['stid']) ? '&stid='.optREQ('stid') : '').''.(!empty($_REQUEST['virt']) ? '&virt='.optREQ('virt') : '').''.(!empty($_REQUEST['debug']) ? '&debug='.optREQ('debug') : '').'",
				data:params+"&addvps=1&space="+json_data,
				success:function(response){
						if(regexp.exec(response) != null){
						
							eval(response);
							setTimeout("get_progress(\'addvs\')", 2000);
						}else{
							
							var error_box = $(response).find("#error_box"); 
							$("#form-container").prepend(error_box);
							$("#form-container").show();
							$("#progress-cont").hide();
						}
				}
			});
			
			$("#error_box").remove();
			$("#form-container").hide();
			$("#progress-cont").show();
			$("#pbar").html("'.$l['checking_data'].'" + " ( 0% ) ");
			window.scrollTo(0, 0);
			$("#progressbar").progressbar({value: 0});
			
			return false;
		});
		';
		if(!empty($globals['set_def_hvm']) && !isset($_POST['hvm'])){
			echo '
				$("#hvm").prop("checked", true);
				$("#hvm").trigger("change");
			';
		}
		
		echo '
	});
	
// ]]></script>';

echo '<script language="javascript" type="text/javascript">

var ips = new Object();
var virt = "'.$virt.'";';

foreach($ips as $k => $v){
	echo 'ips['.$k.'] = "'.$v['ip'].'";';
}

echo 'function addrow(id, val){
	var t = $_(id);
	var lastRow = (t.rows.length);
	var x= t.insertRow(lastRow);
	var y = x.insertCell(0);
	y.innerHTML = \'<input type="text" style="width:90%;float:left" class="form-control" name="ips[]" value="\'+(val || "")+\'" onblur="checkippool()" size="20" /></td><td><a class="delip" title="'.$l['rem_from_ips'].'"><img  src="'.$theme['images'].'admin/delete.png" width="12"/></a><br/><span class="help-block">&nbsp;</span>\';
	
	ipdel();	
};

function changevif(){
	var name = $("#osid option:selected").html();
	if (name.match(/windows/i)) {
		$("input[name=vif_type][value=\'ioemu\']").prop("checked", true);
	}
};

function add_storage_list(id, val){
	
	var y = id.insertCell(2);
	var z = id.insertCell(3);

	var str = \'<select class="form-control storages" name="storages[]">\';';
	
	foreach($storages as $sk => $sv){
		echo 'str += \'<option value="'.$sv['st_uuid'].'" '.($sv['st_uuid'] == $storages[$stid]['st_uuid'] ? 'selected="selected"' : '').'>'.$sv['name'].'&nbsp;('.$sv['disk_space'].' GB '.(empty($sv['oversell']) ? $l['free'] : $l['oversell_free']).')</option>\';';
	}
	
	echo 'str += \'</select>\';
	y.innerHTML = str;
	y.style.paddingTop = "10px";
	z.style.paddingTop = "10px";
	z.innerHTML = \'<a class="delstorage" title="'.$l['rem_storage'].'"><img  src="'.$theme['images'].'admin/delete.png" width="12"/></a>\';
	storagedel();
};

function add_storage(id, val){
	var t = $_(id);
	var lastRow = (t.rows.length);
	var x = t.insertRow(lastRow);
	var y = x.insertCell(0);
	var z = x.insertCell(1);

	y.innerHTML = \'<input name="size" class="form-control size" type="text" size="30" value="" />\';
	z.innerHTML = \'&nbsp;'.$l['space_gb'].'\';

	y.style.paddingTop = "10px";
	z.style.paddingTop = "10px";
	add_storage_list(x);
	
};

function add_storage_bus(id, val){
	var t = $_(id);
	var lastRow = (t.rows.length);
	var x = t.insertRow(lastRow);
	var y = x.insertCell(0);
	var z = x.insertCell(1);
	var p = x.insertCell(2);
	var q = x.insertCell(3);
	var r = x.insertCell(4);
	var s = x.insertCell(5);
	var j = x.insertCell(6);
	var k = x.insertCell(7);
	
	var str1 = \'<select class="form-control bus_driver" name="bus_driver[]" onchange="toggle_driver_num(this.value)" >\';';
						
	foreach($bus_driver as $bdk => $bdv){
		echo 'str1 += \'<option value="'.$bdk.'">'.$bdv.'</option>\';';
	}
							
	echo 'str1 += \'</select>\';
	

	z.innerHTML = \'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp\';
	z.style.paddingTop = "10px";
	var str2 = \'<select class="form-control bus_driver_num" name="bus_driver_num[]">\';';
	
	
	echo 'str2 += \'<option class = "sata virtio scsi" value="0" >0</option>\';';
	echo 'str2 += \'<option class = "sata virtio scsi" value="1" >1</option>\';';
	echo 'str2 += \'<option class = "ide sata virtio scsi" value="2" >2</option>\';';
	echo 'str2 += \'<option class = "ide sata virtio scsi" value="3" >3</option>\';';
	echo 'str2 += \'<option class = "virtio scsi" value="4" >4</option>\';';
	echo 'str2 += \'<option class = "sata virtio scsi" value="5" >5</option>\';';
	echo 'str2 += \'<option class = "virtio scsi" value="6" >6</option>\';';
	echo 'str2 += \'<option class = "virtio scsi" value="7" >7</option>\';';
	echo 'str2 += \'<option class = "virtio scsi" value="8" >8</option>\';';
	echo 'str2 += \'<option class = "virtio scsi" value="9" >9</option>\';';
	echo 'str2 += \'<option class = "virtio scsi" value="10" >10</option>\';';
	echo 'str2 += \'<option class = "virtio scsi" value="11" >11</option>\';';
	echo 'str2 += \'<option class = "virtio scsi" value="12" >12</option>\';';
	echo 'str2 += \'<option class = "virtio scsi" value="13" >13</option>\';';
	echo 'str2 += \'<option class = "virtio" value="14" >14</option>\';';
	echo 'str2 += \'<option class = "virtio" value="15" >15</option>\';';

	echo 'str2 += \'</select>\';
	
	var str3 = \'<span class="help-block">&nbsp;</span>\';
	
	r.innerHTML = \'<input name="size" class="form-control size" type="text" size="30" value="" />\';
	s.innerHTML = \'&nbsp;'.$l['space_gb'].'\';
	
	r.style.paddingTop = "10px";
	s.style.paddingTop = "10px";
					
	var str4 = \'<select class="form-control storages" name="storages[]">\';';
	
	foreach($storages as $sk => $sv){
		echo 'str4 += \'<option value="'.$sv['st_uuid'].'" '.($sv['st_uuid'] == $storages[$stid]['st_uuid'] ? 'selected="selected"' : '').'>'.$sv['name'].'&nbsp;('.$sv['disk_space'].' GB '.(empty($sv['oversell']) ? $l['free'] : $l['oversell_free']).')</option>\';';
	}
	
	echo 'str4 += \'</select>\';
	
	y.innerHTML = str1;
	y.style.paddingTop = "10px";
	p.innerHTML = str2;
	p.style.paddingTop = "10px";
	q.innerHTML = str3;
	q.style.paddingTop = "10px";
	j.innerHTML = str4;
	j.style.paddingTop = "10px";
	k.style.paddingTop = "10px";
	k.innerHTML = \'<a class="delstorage" title="'.$l['rem_storage'].'"><img  src="'.$theme['images'].'admin/delete.png" width="12"/></a>\';
	toggle_driver_num($("#bus_driver option:first").val());
	storagedel();
	
};

function toggle_driver_num(driver_type){
	$(".ide, .sata, .virtio, .scsi").hide();
	$("." + driver_type).show();
};

function storagedel(){

	$(".delstorage").each(function(){
		$(this).unbind("click");
		$(this).click(function(){
			var parent = $(this).parent();
			var granparent = parent.parent();
			parent.remove();
			granparent.remove();
		});
	});	
};

function ipdel(){
	$(".delip").each(function(){
		$(this).unbind("click");
		$(this).click(function(){
			var parent = $(this).parent();
			parent.remove();
			checkippool();
		});
	});
}

$(document).ready(function(){
	progress_onload();
	ipdel();
	storagedel();
	toggle_driver_num($("#bus_driver option:first").val());
});

function adddnsrow(id, val){

	var t = $_(id);
	var ele = document.createElement("div");
    ele.setAttribute("class","row");
    ele.innerHTML=\'<div class="col-sm-6"><input type="text" class="form-control" name="dns[]"  value="\'+(val || "")+\'"  size="20" /></div>\';
    t.appendChild(ele);

    var sp = document.createElement("span");
    sp.setAttribute("class","help-block");
    t.appendChild(sp);
};

function checkippool(){
	try{
	// Clear the Select
	while($_("iplist").options.length != 0){
		$_("iplist").remove(0);
	}
	
	vpsips = curips();
	
	for(x in ips){
		is_cur = 0;	
		for(xx in vpsips){
			if(vpsips[xx] == ips[x]){
				is_cur = 1;
			}
		}
		if(is_cur == 0){
			createOption(x, ips[x]);
		}
	}
	}catch(e){	}
};

function addtoips(){
	try{
	for(i=0; i < $_("iplist").options.length; i++) {
		if ($_("iplist").options[i].selected) {
			addrow("iptable", ips[$_("iplist").options[i].value]);
		}
	}
	
	checkippool();
	
	// Make the Table again
	while($_("iptable").rows.length != 0){
		$_("iptable").deleteRow(0);
	}
	
	for(x in vpsips){
		addrow("iptable", vpsips[x]);
	}
	}catch(e){	}
};

function createOption(val, txt){
	var opt = document.createElement("option");
	opt.text = txt;
	opt.value = val;
	
	if(document.all && !window.opera){
		$_("iplist").add(opt);
	}else{
		$_("iplist").add(opt, null);
	}
};

function curips(){
	var vpsips = new Object();
	var cur_ips = document.getElementsByName("ips[]");
	for(i=0; i < cur_ips.length; i++){
		if(cur_ips.item(i).value != ""){
			vpsips[i] = cur_ips.item(i).value;
		}
	}
	return vpsips;
};

function planips(num){
	if(num > 0){
		try{
			for(i=0; i < num; i++) {
				$_("iplist").options[i].selected = true;
			}
		}catch(e){	}
		addtoips();
	}
};

function loadplan(planid){
	
	if(planid == 0){
		return false;
	}
	
	if(AJAX("'.$globals['index'].'act=addvs&ajax=true'.(!empty($_REQUEST['virt']) ? '&virt='.optREQ('virt') : '').'&plan="+planid+"&plid="+planid, "plandetails(re)")){
		return false;
	}else{
		return true;	
	}
};

function plandetails(re){
	eval(re);
	var topology_enabled = 0;
	$.each($plan, function( index, value ){
		//alert(index  + " -- "+ value)
		if(index == "io") index = "priority";
		if(index == "swap") index = "swapram";
		
		if(index == "mgs"){
			
			var i = 0;
			var size = value.length;
			$options = $("#mg option");
			$("#mg option:selected").removeAttr("selected");
			for(i; i < size; i++){
				// filter the options with the specific value and select them
				if(value[i] != ""){
					$options.filter("[value="+value[i]+"]").prop("selected", true);
				}
			}
			
		}else if(index == "dns_nameserver" && (typeof value == "object")){
			$("#dnstable").html("");
			$.each(value, function( ind, val ){
				adddnsrow(\'dnstable\', val);
			});			
		}else if(index == "plan_ips" && (typeof value == "object")){
			$("#iplist").html("");
			ips = [];
			$.each(value, function( ind, val ){
				let row = "<option value="+ind+">"+val.ip+"</option>";
				$("#iplist").append(row);
				ips[ind] = val.ip;
			});			
		}else if(index == "internal_ips" && (typeof value == "object")){
			$("#ips_int").html("");
			ips_int = [];
			$.each(value, function( ind, val ){
				let row = "<option value="+val.ip+">"+val.ip+"</option>";
				$("#ips_int").append(row);
				ips_int[ind] = val.ip;
			});			
		}else if(index == "bpid"){
			// Do nothing since bpid should remain as -1
		}else{
			var ischeckbox = 0;
			if($("#addvsform").find("[name=show_"+ index +"]").prop("type") == "checkbox"){
				ischeckbox = index;
				index = "show_"+index;
			}
			
			$("#addvsform").field( index, value);
			if(!empty(ischeckbox)){
				change_checkbox(ischeckbox);
			}
			
			if((index == "topology_sockets" || index == "topology_cores" || index == "cpu_threads") && (!empty(value)) && (empty(topology_enabled))){
				$("#enable_cpu_topology").prop("checked", true);
				topology_enabled = 1;
			}
		}
	});
	
	$_("hdd_0").value = $plan["space"];';
	
	if(!empty($bus_driver)){
		echo '
		$_("bus_driver").value = $plan["bus_driver"];
		$_("bus_driver_num").value = $plan["bus_driver_num"];';
	}
	
	echo 'ipchoose("ips_int", $plan["ips_int"]);
	
	try{
		var currentIPs = curips();		
		var i;
		var num_ips = 0;
		for (i in currentIPs) {
			if (currentIPs.hasOwnProperty(i)) {
				num_ips++;
			}
		}
	
		if(num_ips < $plan["ips"]){		
			var diff = $plan["ips"] - num_ips;
			planips(diff);
		}
		$_("ipv6count").value = $plan["ips6"];
		$_("ipv6subnetcount").value = $plan["ips6_subnet"];
		
		if(document.getElementsByName("ips_int[]").length < $plan["ips_int"]){
			multiselect("ips_int[]", $plan["ips_int"]);
		}
		
	}catch(e){
		// Nothing to do here
	}
	
	ishvm();
	checkvnc();
	change_cpu_topology();
	handle_capping();
};

function loadiso(uid){
	
	if(uid == 0){
		return false;
	}
	
	if(AJAX("'.$globals['index'].'act=addvs&ajax=true&uid="+uid, "isodetails(re)")){
		return false;
	}else{
		return true;
	}	
};

// Fill iso where it is needed
function isodetails(re){
	$("#addvsform #iso #sec_iso").empty().append(re).trigger("chosen:updated");	
};

function changepriority(priority){
	
	if(!$("#prior")){
		return false;
	}
	
	if (priority != ""){
		$("#prior").val(priority);
	}
};

function adduser(){
	var uid = parseInt($_("uid").value);
	
	if(uid < 1){
		$(".user_details_f").show();
		$(".user_details").show();
	}else{
		$(".user_details").hide();
		$(".user_details_f").hide();
	}
	
	loadiso(uid);
	
}

function checkvnc(){
	if(!$("#vnc")){
		return false;
	}
	if(is_checked("vnc")){
		$("#vncpassrow").show();
	}else{
		$("#vncpassrow").hide();
	}
	change_checkbox("vnc");	
};

function pincheck(){
	if(!$("#allcores")){
		return false;
	}
	
	if(is_checked("allcores")){';
		for($i=0; $i < $resources['cpucores']; $i++){
			echo '$_("pin'.$i.'").checked = 0;';
		}
		echo '$("#pincores").hide();
	}else{
		$("#pincores").show();
	}
};

function addvbutton(){
	
	if($_("macaddress").style.display == "none"){
		$_("macaddress").style.display = ""; 
		$_("addv_opt_label").innerHTML = "'.$l['addvhide'].'";
		$_("addv_opt_button").value = "'.$l['addvhide'].'";     
    } else {
		$_("macaddress").style.display = "none";
		$_("addv_opt_label").innerHTML = "'.$l["addvoption"].'";  
		$_("addv_opt_button").value = "'.$l["addvoption"].'";  	    
	}
}

function toggle_advoptions(ele){
	
	var div_ele = $("#"+ele+"_advoptions");

	if (div_ele.is(":hidden")){
		$("#"+ele+"_advoptions_ind").text(" - ");
		div_ele.slideDown("slow");
	}
	else{
		$("#"+ele+"_advoptions_ind").text(" + ");
		div_ele.slideUp("slow");
	}
}

function plus_onmouseover(){
	$("#plus").attr("src", "'.$theme['images'].'admin/plus_hover.gif");
}

function plus_onmouseout(){
	$("#plus").attr("src", "'.$theme['images'].'admin/plus.gif");
}

function enable_accel(){
	
	if($_("kvm_vga").checked == true){
		$_("enable_acceleration").style.display = "";
	}else{
		$_("enable_acceleration").style.display = "none";
	}
	
	change_checkbox("kvm_vga");
}


function ishvm(){
	
	var hvm = false;
	if(!$_("hvm")){
		hvm = false;
	}else{
		hvm = $_("hvm").checked;
	}
	
	if($_("isorow")){
		if(hvm == true || virt == "kvm" || virt == "vzk" || virt == "proxk"){
			$_("isorow").style.display = "";
		}else{
			$_("isorow").style.display = "none";
		}
	}
	
	if($_("enable_rdp")){
		if(hvm == true || virt == "kvm" || virt == "vzk" || virt == "proxk"){
			$_("enable_rdp").style.display = "";
		}else{
			$_("enable_rdp").style.display = "none";
		}		
	}
	
	if($("#add_storage_btn")){
		if(hvm == true || virt == "kvm" || virt == "proxk"){
			$("#add_storage_btn").show();
		}else{
			$("#add_storage_btn").hide();
		}
	}
	
	if($_("shadowrow")){
		if(hvm == true){
			$_("shadowrow").style.display = "";
		}else{
			$_("shadowrow").style.display = "none";
		}
	}
		
	if($_("osid")){
		
		var sid = $("#sid").val() || 0;
		
		for(var i=0; i<$_("osid").options.length; i++){

			// The NONE Value
			if($_("osid").options[i].value == 0){
				if(hvm == true){
					$_("osid").options[i].disabled = false;
				}else{
					$_("osid").options[i].disabled = true;
				}
				continue;
			}
			
			if(getAttributeByName($_("osid").options[i], "hvm") == 1 && hvm == true){
				if(sid > 0 && getAttributeByName($_("osid").options[i], "distro") != "webuzo"){
					$_("osid").options[i].disabled = true;
				}else{
					$_("osid").options[i].disabled = false;	
				}
			}else if(getAttributeByName($_("osid").options[i], "hvm") != 1 && hvm == false){
				if(sid > 0 && getAttributeByName($_("osid").options[i], "distro") != "webuzo"){
					$_("osid").options[i].disabled = true;
				}else{
					$_("osid").options[i].disabled = false;	
				}
			}else{
				$_("osid").options[i].disabled = true;
			}
		}
	}
	
	// if there is webuzo templates available in advance section
	if($(\'input[name="webuzo_os"]\')){
		
		$(\'input[name="webuzo_os"]\').each(function(){
			if(hvm == true && $(this).attr("hvm") == 1){
				$(this).removeAttr("disabled");
			}else if(hvm == false && $(this).attr("hvm") != 1){
				$(this).removeAttr("disabled");
			}else{
				$(this).attr("disabled", "true")
			}
		})
	}
	
	if($_("mg")){
		
		$("#mg option").each(function() {
			if(hvm){
				if($(this).attr("type").indexOf("hvm") > 0){
					$(this).css("display", "");
				}else{
					$(this).css("display", "none");
				}
			}else{
				if($(this).attr("type").indexOf("hvm") == -1){
					$(this).css("display", "");
				}else{
					$(this).css("display", "none");
				}
			}
		});
	}
    
	change_checkbox("hvm");
	
	$(".virt-select").select2();
	
	return true;
};

function ispvonhvm(){
	
	var pv_on_hvm = false;
	if(!$_("pv_on_hvm")){
		pv_on_hvm = false;
	}else{
		pv_on_hvm = $_("pv_on_hvm").checked;
	}
	
	if($_("tr_viftype")){
		if(pv_on_hvm == true){
			$_("tr_viftype").style.display = "none";
		}else{
			$_("tr_viftype").style.display = "";
		}
	}
	change_checkbox("pv_on_hvm");
	return true;
};


function loadvirt(virt){

	window.location = "'.$globals['index'].'act=addvs&virt="+virt;
	
}

var lang = Array();
lang["bad"] = "'.$l['bad'].'";
lang["good"] = "'.$l['good'].'";
lang["strong"] = "'.$l['strong'].'";
lang["short"] = "'.$l['short'].'";
lang["strength_indicator"] = "'.$l['strength_indicator'].'";

function change_cpu_topology(){
	
	if($("#enable_cpu_topology").prop("checked")){
		$("#cpu_topology").css("display", "");
	}else{
		$("#cpu_topology").css("display", "none");
		$("#topology_sockets").val(0);
		$("#topology_cores").val(0);
		$("#topology_threads").val(0);
	}
}

var lang_no_limit = "'.$l['no_limit'].'";

addonload("handle_capping(); fillspeedmbits(); planips(0); checkippool(); adduser(); changepriority(\''.POSTval('priority', 3).'\'); checkvnc(); ishvm(); pincheck(); check_pass_strength(\'rootpass\');change_cpu_topology();");

</script>';

// Is it loaded into the correct kernel
if(strlen($resources['check_kernel']) > 1){
	echo '<div class="e_notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$resources['check_kernel'].'</div>';
}

echo '
<div id="form-container">
<form id="addvsform"  accept-charset="'.$globals['charset'].'" action="" method="post" name="addvs" class="form-horizontal">
		
<div class="row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['vs_server'].'</label>
		<span class="help-block">'.$l['exp_server'].'</span>
	</div>
	<div class="col-sm-4 server-select-lg">
		<select id="server-select" class="server-select" style="width:100%">';
					
			foreach($servers as $k => $v) {
				echo '<option value="'.$k.'" data-icon="'.$theme['images'].'admin/'.$v['virt'].'_28.png" '.($k == $globals['server'] ? 'selected' : '').'><span><img src="'.$theme['images'].'admin/'.$v['virt'].'_28.png" />'.$v['server_name'].' ('.$v['ip'].')</span></option>';
			}
			
		echo '</select>
	</div>
	<div class="col-sm-4"></div>
</div>
';
// Do we have multiple virt
if(count($servers[$globals['server']]['virts']) > 1){

echo '<div class="row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['vs_virt'].'</label>
		<span class="help-block">'.$l['vs_virt_exp'].'</span>
	</div>
	<div class="col-sm-4">
		<select class="form-control" name="virt" onchange="loadvirt(this.value)">';
	 foreach($servers[$globals['server']]['virts'] as $k => $v){		 
		echo '<option value="'.$v.'" '.(REQval('virt') == $v ? 'selected="selected"' : '').'>'.$l['virt_'.$v].'</option>';
	}
	
echo '
		</select>
	</div>
	<div class="col-sm-4"></div>
</div>';
	
}

echo  '<div class="row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['vs_user'].'</label>
		<span class="help-block">'.$l['vs_user_exp'].'</span>
	</div>
	<div class="col-sm-4 server-select-lg">
		<select class="form-control virt-select" name="uid" id="uid" onchange="adduser()" style="width:100%">
			<option value="0" '.(POSTval('uid') == 0 ? 'selected="selected"' : '').'>'.$l['add_user'].'</option>';	
			foreach($users as $k => $v){		 
				echo '<option value="'.$k.'" '.(POSTval('uid') == $k ? 'selected="selected"' : '').'>'.$v['email'].'</option>';
			}
	
	echo'</select>
	</div>
</div>

<div class="row">
	<div class="col-sm-9">
		<fieldset class="user_details_f">
			<legend class="user_details_f">'.$l['user_details'].'</legend>
			<div class="row user_details" style="display:none">
				<div class="col-sm-5">
					<label class="control-label">'.$l['user_email'].'</label>
				</div>
				<div class="col-sm-6">
					<input type="text" class="form-control" name="user_email" size="30" value="'.POSTval('user_email', '').'" />
					<span class="help-block"></span>
				</div>
				<div class="col-sm-1"></div>
				<span class="help-block">&nbsp;</span>
			</div>
			<div class="row user_details" style="display:none">
				<div class="col-sm-5">
					<label class="control-label">'.$l['user_pass'].'</label>
				</div>
				<div class="col-sm-6">
					<input type="text" class="form-control" name="user_pass" size="30" value="'.POSTval('user_pass', '').'" id="rand_user_pass"/>
					<span class="help-block"></span>
				</div>
				<div class="col-sm-1" style="padding-top: 8px;">
					<a href="javascript: void(0);" onclick="$_(\'rand_user_pass\').value=rand_pass(12);check_pass_strength(\'rand_user_pass\');return false;" title="'.$l['randpass'].'"><img src="'.$theme['images'].'randpass.gif" /></a>
				</div>
			</div>
			
			<div class="row user_details" style="display:none">
				<div class="col-sm-5">
					<label class="control-label">'.$l['fname'].'</label>
					<span class="help-block">&nbsp;</span>
				</div>
				<div class="col-sm-6">
					<input type="text" class="form-control" name="fname" size="30" value="'.POSTval('fname', '').'" />
				</div>
				<div class="col-sm-1"></div>
			</div>
			
			<div class="row user_details" style="display:none">
				<div class="col-sm-5">
					<label class="control-label">'.$l['lname'].'</label>
					<span class="help-block">&nbsp;</span>
				</div>
				<div class="col-sm-6">
					<input type="text" class="form-control" name="lname" size="30" value="'.POSTval('lname', '').'" />
				</div>
				<div class="col-sm-1"></div>
			</div>
		</fieldset>
	</div>
	<div class="col-sm-3"></div>
</div>
<div class="row"> 
	<div class="col-sm-4">
		<label class="control-label">'.$l['vs_plan'].'</label>
		<span class="help-block">'.$l['plan_exp'].'</span>
	</div>
	<div class="col-sm-4 server-select-lg">
		<select class="form-control virt-select" name="plid" onchange="loadplan(this.value)" style="width:100%">';
		 foreach($plans as $k => $v){
			 
			if(str_replace('hvm', '', $v['virt']) != $virt && !empty($k)) continue;
			
			echo '<option value="'.$k.'" '.(POSTval('plid') == $k ? 'selected="selected"' : '').'>'.$v['plan_name'].'</option>';
		}
 		echo'</select>
	</div>
	<div class="col-sm-4"></div>

</div>';

if(($virt == 'xen' || $virt == 'xcp') && !empty($hvm)){
	echo '<div class="row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['hvm'].'</label>
		<span class="help-block">'.$l['hvm_exp'].'</span>
	</div>
	<div class="col-sm-4">
		<input type="checkbox" class="ios" name="show_hvm" id="hvm" '.POSTchecked('hvm').' onchange="ishvm()"/>
		<input type="hidden" name="hvm" id="hidden_hvm">
	</div>
	<div class="col-sm-4"></div>
</div>';
}
	
echo '<div class="row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['vsos'].'</label>
		<span class="help-block">'.$l['vsos_exp'].'</span>
	</div>
	<div class="col-sm-4 server-select-lg">
	
	<select class="form-control virt-select" name="osid" id="osid" onchange="changevif();" style="width:100%">
	'.'<option value="0" '.(POSTval('osid') == 0 ? 'selected="selected"' : '').'>'.$l['none'].'</option>';

if(!empty($oslist)){
	foreach($oslist[$virt] as $kk => $vv){		
		foreach($vv as $k => $v){
			echo '<option value="'.$k.'" '.(POSTval('osid') == $k ? 'selected="selected"' : '').' '.(!empty($v['hvm']) ? 'hvm="1"' : '').' distro="'.$kk.'">'.(!empty($v['hvm']) ? 'HVM - ' : '').''.$v['name'].'</option>';
		}
	}
}
		
     echo'</select>
	</div>
	<div class="col-sm-4"></div>
</div>';

if(!empty($isos)){
	echo '<div class="row" id="isorow">
	<div class="col-sm-4">
		<label class="control-label">'.$l['vsiso'].'</label>
		<span class="help-block">'.$l['vsiso_exp'].'</span>
	</div>
	<div class="col-sm-4">
	
	<select class="form-control chosen" name="iso" id="iso">
	<option value="0" '.(POSTval('iso') == 0 ? 'selected="selected"' : '').'>'.$l['none'].'</option>
	<optgroup label="'.$l['admin_iso'].'">';
	foreach($isos as $k => $v){
		echo '<option value="'.$k.'" '.(POSTval('iso') == $k ? 'selected="selected"' : '').'>'.$v['name'].'</option>';
	}
		
     echo'</optgroup></select>
	</div>
	<div class="col-sm-4"></div>
</div>';

}

echo '<div class="row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['hostname'].'</label>
		<span class="help-block">'.$l['hostname_exp'].'</span>
	</div>
	<div class="col-sm-4">
		<input type="text" class="form-control" name="hostname" id="hostname" size="30" value="'.POSTval('hostname', '').'" />
	</div>
	<div class="col-sm-4"></div>
</div>

<div class="row">
	<div class="col-sm-4">
		<label class="control-label">'.$l['rootpass'].'</label>
		<span class="help-block">'.$l['rootpass_exp'].'</span>
	</div>
	<div class="col-sm-4">
		<input type="text" class="form-control" name="rootpass" id="rootpass" size="30" onkeyup="check_pass_strength(\'rootpass\');" value="'.POSTval('rootpass', generateRandPwd(12)).'" autocomplete="off" /><div id="rootpass_pass-strength-result" class="">'.$l['strength_indicator'].'</div>
	</div>
	<div class="col-sm-4">
		<a href="javascript: void(0);" onclick="$_(\'rootpass\').value=randstr(12, 1, '.(!empty($globals['pass_strength']) ? $globals['pass_strength'] : 0).');check_pass_strength(\'rootpass\');return false;" title="'.$l['randpass'].'"><img src="'.$theme['images'].'randpass.gif" /></a>
	</div>
</div>


<div class="row hide_for_plan">
	<div class="col-sm-4">
		<span class="help-block">&nbsp;</span>
		<label class="control-label">'.$l['ip'].'</label>
		<span class="help-block">'.$l['ips_exp'].'</span>
	</div>
	<div class="col-sm-4">
		<span class="help-block">&nbsp;</span>
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr>
			<td valign="top">
				<table cellpadding="3" cellspacing="0" style="width:100%" id="iptable">';
		 
				$_ips = @$_POST['ips'];
				
				if(is_array($_ips) && !empty($_ips)){
					foreach($_ips as $k => $ip){
						if(empty($ip)){
							unset($_ips[$k]);
						}
					}
				}
				
				if(empty($_ips)){
					$_ips = array(NULL);
				}
			 
				foreach($_ips as $ip){
					echo '<tr>
						<td>
							<input type="text" class="form-control" style="width:90%;float:left;" name="ips[]" value="'.$ip.'" onblur="checkippool();" size="20" />
							<a class="delip" title="'.$l['rem_from_ips'].'"><img src="'.$theme['images'].'admin/delete.png" width="12"/></a>
							<br/><span class="help-block">&nbsp;</span>
						</td>
					</tr>';
				}
		 		
			echo '</table>
				
				<input type="button" class="go_btn" onclick="javascript:addrow(\'iptable\');" value="'.$l['add_ip'].'"/>
			</td>
		</tr>
		</table>
	</div>
	<div class="col-sm-4"><span class="help-block">&nbsp;</span>';
		if(!empty($ips)){
			
			echo '<select class="form-control" id="iplist" size="4" multiple="multiple">';
			foreach($ips as $k => $v){	 
				echo '<option value="'.$k.'">'.$v['ip'].'</option>';
			}		
			echo'</select>
			<br/>
			<input type="button" class="go_btn" onclick="javascript:addtoips(\'iptable\');" value="'.$l['add_to_ips'].'"/>
			<br />';
				
			}else{
				'&nbsp;';
			}
	echo '
		</div>
	</div>';

if(!empty($ips6_subnet)){

echo '<div class="row hide_for_plan">
	<div class="col-sm-4">
		<label class="control-label">'.$l['ips6_subnet'].'</label>
		<span class="help-block">'.$l['ips6_subnet_exp'].'</span>
	</div>
	<div class="col-sm-4">
		<input type="text" class="form-control" name="num_ips6_subnet" id="ipv6subnetcount" size="10"/>';
			
echo '</div>
</div>';

}

if(!empty($ips6)){
echo '<div class="row hide_for_plan">
	<div class="col-sm-4">
		<span class="help-block">&nbsp;</span>
		<label class="control-label">'.$l['ips6'].'</label>
		<span class="help-block">'.$l['ips6_exp'].'</span>
	</div>
	<div class="col-sm-4">
		<span class="help-block">&nbsp;</span>
		<input type="text" class="form-control" name="num_ips6" id="ipv6count" size="10" />
	</div>
	<div class="col-sm-4"></div>
</div>';
}

if($bus_driver){
	echo '<div class="row hide_for_plan">
		<div class="col-sm-4">
			<span class="help-block">&nbsp;</span>
			<label class="control-label">'.$l['hdd'].'</label>
			<span class="help-block">'.$l['exp_hdd'].'</span>
		</div>
		<div class="col-sm-8">
			<span class="help-block">&nbsp;</span>
			<table name="storages_table" id="storages_table" style="width:100%;" cellpadding="2" cellspacing="0" border="0">
			<tr>
					<td>
						<select class="form-control bus_driver" id = "bus_driver" name="bus_driver[]" onchange="toggle_driver_num(this.value)">';
						
							foreach($bus_driver as $bdk => $bdv){
								echo '<option value="'.$bdk.'" '.POSTselect('bus_driver', $bdv).'>'.$bdv.'</option>';
							}
							
				echo   '</select>
					</td>
					<td>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</td>
					<td>
						<select class="form-control bus_driver_num" name="bus_driver_num[]" id="bus_driver_num">';
							echo '<option class="sata virtio scsi" value="0" '.POSTselect('bus_driver_num', 0).'>0</option>';
							echo '<option class="sata virtio scsi" value="1" '.POSTselect('bus_driver_num', 1).'>1</option>';
							echo '<option class="ide sata virtio scsi" value="2" '.POSTselect('bus_driver_num', 2).'>2</option>';
							echo '<option class="ide sata virtio scsi" value="3" '.POSTselect('bus_driver_num', 3).'>3</option>';
							echo '<option class="virtio scsi" value="4" '.POSTselect('bus_driver_num', 4).'>4</option>';
							echo '<option class="sata virtio scsi" value="5" '.POSTselect('bus_driver_num', 5).'>5</option>';
							echo '<option class="virtio scsi" value="6" '.POSTselect('bus_driver_num', 6).'>6</option>';
							echo '<option class="virtio scsi" value="7" '.POSTselect('bus_driver_num', 7).'>7</option>';
							echo '<option class="virtio scsi" value="8" '.POSTselect('bus_driver_num', 8).'>8</option>';
							echo '<option class="virtio scsi" value="9" '.POSTselect('bus_driver_num', 9).'>9</option>';
							echo '<option class="virtio scsi" value="10" '.POSTselect('bus_driver_num', 10).'>10</option>';
							echo '<option class="virtio scsi" value="11" '.POSTselect('bus_driver_num', 11).'>11</option>';
							echo '<option class="virtio scsi" value="12" '.POSTselect('bus_driver_num', 12).'>12</option>';
							echo '<option class="virtio scsi" value="13" '.POSTselect('bus_driver_num', 13).'>13</option>';
							echo '<option class="virtio" value="14" '.POSTselect('bus_driver_num', 14).'>14</option>';
							echo '<option class="virtio" value="15" '.POSTselect('bus_driver_num', 15).'>15</option>';
				echo	'
						</select>
					</td>
					<td>
						<span class="help-block">&nbsp;</span>
					</td>';
						
					if(empty($disks)){
						$disks[0]['st_uuid'] = $storages[$stid]['st_uuid'];
					}
					
					foreach($disks as $k => $v){

						echo '
							<td width="25%">
								<input name="size[]" class="form-control size" type="text" id="hdd_'.$k.'" size="30" value="'.@$v['size'].'" />
							</td>
							<td>
								&nbsp;&nbsp;'.$l['space_gb'].'&nbsp;&nbsp;&nbsp;
							</td>
							<td>
								<select class="form-control storages" name="storages[]">';
									foreach($storages as $sk => $sv){
										echo '<option value="'.$sv['st_uuid'].'" '.(isset($_POST['storages']) ? ($sv['st_uuid'] == $v['st_uuid'] ? 'selected="selected"' : '') : ($sv['st_uuid'] == $storages[$stid]['st_uuid'] ? 'selected="selected"' : '')).'>'.$sv['name'].'&nbsp;('.$sv['disk_space'].' GB '.(empty($sv['oversell']) ? $l['free'] : $l['oversell_free']).')</option>';
									}
								echo '</select>
							</td>
						';
					}	
				echo '</tr>
					</table>
				<br/>
				<input id="add_storage_btn" type="button" class="go_btn" onclick="add_storage_bus(\'storages_table\');" value="'.$l['add_ip'].'"/>
			<span class="help-block">&nbsp;</span>	
		</div>
	</div>';
	
}else{
	echo '<div class="row hide_for_plan">
		<div class="col-sm-4">
			<span class="help-block">&nbsp;</span>
			<label class="control-label">'.$l['hdd'].'</label>
			<span class="help-block">'.$l['exp_hdd'].'</span>
		</div>
		<div class="col-sm-8">
			<span class="help-block">&nbsp;</span>
			<table name="storages_table" id="storages_table" style="width:100%;" cellpadding="2" cellspacing="0" border="0">';
				
			if(empty($disks)){
				$disks[0]['st_uuid'] = $storages[$stid]['st_uuid'];
			}
			
			foreach($disks as $k => $v){

				echo '<tr>
					<td width="25%">
						<input name="size[]" class="form-control size" type="text" id="hdd_'.$k.'" size="30" value="'.@$v['size'].'" />
					</td>
					<td>
						&nbsp;&nbsp;'.$l['space_gb'].'&nbsp;&nbsp;&nbsp;
					</td>
					<td>
						<select class="form-control storages" name="storages[]">';
							foreach($storages as $sk => $sv){
								echo '<option value="'.$sv['st_uuid'].'" '.(isset($_POST['storages']) ? ($sv['st_uuid'] == $v['st_uuid'] ? 'selected="selected"' : '') : ($sv['st_uuid'] == $storages[$stid]['st_uuid'] ? 'selected="selected"' : '')).'>'.$sv['name'].'&nbsp;('.$sv['disk_space'].' GB '.(empty($sv['oversell']) ? $l['free'] : $l['oversell_free']).')</option>';
							}
						echo '</select>
					</td>
				</tr>';
			}
			
		echo '</table>
			<br>
			<input id="add_storage_btn" type="button" class="go_btn" onclick="add_storage(\'storages_table\');" value="'.$l['add_ip'].'"/>
			<span class="help-block">&nbsp;</span>	
		</div>
	</div>';	
}

echo '<div class="row hide_for_plan">
	<div class="col-sm-4">
		<label class="control-label">'.$l['gram'].'</label>
		<span class="help-block">'.$l['exp_gram'].'</span>
	</div>
	<div class="col-sm-4">
		<input type="text" class="form-control" name="ram" id="gram" size="30" value="'.POSTval('ram', '').'" />		
	</div>
	<div class="col-sm-4" style="padding-top: 8px;">
		'.$l['ram_mb'].' (<span class="fhead">'.(empty($resources['overcommit']) ? $resources['ram'] : ($resources['overcommit'] - $servers[$globals['server']]['alloc_ram'])).' MB</span> '.(empty($resources['overcommit']) ? $l['free'] : $l['overcomit_free']).')
	</div>
</div>';

if($virt == 'openvz' || $virt == 'proxo'){

echo '<div id="burst" class="row hide_for_plan">
	<div class="col-sm-4">
		<label class="control-label">'.$l['bram'].'</label>
		<span class="help-block">'.$l['exp_bram'].'</span>
	</div>
	<div class="col-sm-4">
		<input type="text" class="form-control" name="burst" id="bram" size="30" value="'.POSTval('burst', '').'" />
	</div>
	<div class="col-sm-4" style="padding-top:8px;">
		 '.$l['ram_mb'].'
	</div>
</div>';

}

if($virt != 'openvz' && $virt != 'proxo'){

echo '<div id="swappedram" class="row hide_for_plan">
	<div class="col-sm-4">
		<label class="control-label">'.$l['swap'].'</label>
		<span class="help-block">'.$l['exp_swap'].'&nbsp;</span>
	</div>
	<div class="col-sm-4">
		<input type="text" class="form-control" name="swapram" id="swap" size="30" value="'.POSTval('swapram', '').'" />
	</div>
	<div class="col-sm-4">'.$l['ram_mb'].'</div>
</div>';

}

echo '<div class="row hide_for_plan">
	<div class="col-sm-4">
		<label class="control-label">'.$l['band'].'</label>
		<span class="help-block">'.$l['exp_band'].'</span>
	</div>
	<div class="col-sm-4">
		<input type="text" class="form-control" name="bandwidth" id="band" size="30" value="'.POSTval('bandwidth', '').'" onchange="handle_capping();" />
	</div>
	<div class="col-sm-4" style="padding-top:8px;">'.$l['band_gb'].'</div>
</div>

<script type="text/javascript">
function netspeed(r){
	$_("network_speed").value = (r);
	handle_capping();
}
function upspeed(r){
	$_("upload_speed").value = (r);
	handle_capping();
}
</script>

<div class="row hide_for_plan">
	<div class="col-sm-4">
		<label class="control-label">'.$l['network_speed'].'</label>
		<span class="help-block">'.$l['network_speed_exp'].'</span>
	</div>
	<div class="col-sm-4">
<input type="text" class="form-control" name="network_speed" id="network_speed" size="8" value="'.POSTval('network_speed', '').'" />
	 </div>
	 <div class="col-sm-1" style="padding-top:8px;">'.$l['net_kb'].'</div>
	 <div class="col-sm-3 server-select-lg">
	 	<select class="form-control speedmbits virt-select" name="network_speed2" id="network_speed2" onchange="netspeed(this.value)" style="width:100%"></select>
	 </div>
</div>';

// CPU Units HTML Output
//-------------------------------------------------------------------------------------------------------------------------------
if($virt != 'virtualbox'){
echo '<div class="row hide_for_plan">
	<div class="col-sm-4">
		<label class="control-label">'.$l['cpunit'].'</label>
		<span class="help-block">'.$l['cpalloc'].'<br/>
	        <a href="'.$globals['docs'].'Creating_A_VPS#CPU_Parameters" target="_blank">'.$l['need_info'].'</a></span>
	</div>
	<div class="col-sm-4">
		<input type="text" class="form-control" name="cpu" id="cpunit" size="30" value="'.POSTval('cpu', '').'" />
	</div>
	<div class="col-sm-4" style="padding-top:8px;">'.$l['units'].'</div>
</div>';
}

echo '<div class="row hide_for_plan">
	<div class="col-sm-4">
		<label class="control-label">'.$l['cores'].'</label>
		<span class="help-block"><a href="'.$globals['docs'].'Creating_A_VPS#CPU_Parameters" target="_blank">'.$l['need_info'].'</a></span>
	</div>
	<div class="col-sm-4">
		<input type="text" class="form-control" name="cores" id="cores" size="30" value="'.POSTval('cores', '').'" />
	</div>
	<div class="col-sm-4"></div>
</div>';

if($virt != 'lxc'){
	// CPU Percent HTML Output
	echo '<div id="cpupercent" class="row hide_for_plan">
		<div class="col-sm-4">
			<label class="control-label">'.$l['percent'].'</label>
			<span class="help-block">'.$l['cpuperutil'].'<br />
			<a href="'.$globals['docs'].'Creating_A_VPS#CPU_Parameters" target="_blank">'.$l['need_info'].'</a>
			</span>
		</div>
		<div class="col-sm-4">
			<input type="text" class="form-control" name="cpu_percent" id="percent" size="30" value="'.POSTval('cpu_percent', '').'" />
		</div>
		<div class="col-sm-4" style="padding-top:8px;">%</div>
	</div>';
}

// IO Priority
if($kernel->features('io_priority', $virt)){
	echo '<div class="row hide_for_plan">
			<div class="col-sm-4">
				<label class="control-label">'.$l['io'].'</label>
				<span class="help-block">'.$l['io0-7'].'</span>
			</div>
			<div class="col-sm-4">
				<select id="prior" name="priority" class="form-control">
					<option value="0">0</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
				</select>
			</div>
	</div>';
}

if($virt == 'openvz'){
if(!distro_check(1)){
	echo '<div class="row hide_for_plan">
		<div class="col-sm-4">
			<label class="control-label">'.$l['ploop'].'</label>
			<span class="help-block">'.$l['ploop_exp'].'</span>
		</div>
		<div class="col-sm-4">
			<input type="checkbox" class="ios" id="ploop" name="show_ploop" '.POSTchecked('ploop').' '.($globals['ploop'] ? 'checked="checked" disabled="disabled"' : '').' onchange="change_checkbox(\'ploop\');"/>
			<input type="hidden" name="ploop" id="hidden_ploop">
		</div>
	<div class="col-sm-4"></div>
</div>';
}
}

if($virt == 'xen' && !empty($hvm)){
	
echo '<div id="shadowrow" class="row hide_for_plan">
	<div class="col-sm-4">
		<label class="control-label">'.$l['shadow'].'</label>
		<span class="help-block">'.$l['shadow_exp'].'</span>
	</div>
	<div class="col-sm-4">
		<input type="text" class="form-control" name="shadow" id="shadow" size="30" value="'.POSTval('shadow', 8).'" />
	</div>
	<div class="col-sm-4" style="padding-top:8px;">MB</div>
</div>';

}

if($kernel->features('vnc_support', $virt)){

$vncpasslen = $kernel->features('vncpasslen', $virt);

echo '<div class="row hide_for_plan">
	<div class="col-sm-4">
		<label class="control-label">VNC</label>
	</div>
	<div class="col-sm-4">
		<input type="checkbox" class="ios" name="show_vnc" id="vnc" '.POSTchecked('vnc').' onchange="checkvnc();"/>
		<input type="hidden" name="vnc" id="hidden_vnc">
	</div>
	<div class="col-sm-4"></div>
</div>
<br/>
<div id="vncpassrow" class="row hide_for_plan">
	<div class="col-sm-4">
		<label class="control-label">'.$l['vncpass'].'</label>
		<span class="help-block">'.$l['vncpass_exp'].'</span>
	</div>
	<div class="col-sm-4">
		<input type="text" class="form-control" name="vncpass" id="vncpass" size="30" value="'.POSTval('vncpass', generateRandStr($vncpasslen)).'" autocomplete="off"/> &nbsp; 
	</div>
	<div class="col-sm-4">
		<a href="javascript: void(0);" onclick="$_(\'vncpass\').value=randstr('.$vncpasslen.');return false;" title="'.$l['vncpass'].'"><img src="'.$theme['images'].'vncpass.gif" /></a>
	</div>
</div><br/>';

}

if($kernel->features('ha', $virt) && array_key_exists($globals['server'], $ha_enabled)){
	echo '<div class="row">
			<div class="col-sm-4">
				<label class="control-label">'.$l['ha'].'</label>
				<span class="help-block">'.$l['ha_exp'].'</span>
			</div>
			<div class="col-sm-4">
				<input type="checkbox" class="ios" name="show_ha" id="ha" '.POSTchecked('ha', 1).' onchange="change_checkbox(\'ha\');" value="1"/>
				<input type="hidden" name="ha" id="hidden_ha" value="1">
			</div>
	</div>';
}

echo '<div class="row hide_for_plan">
		<div class="col-sm-12"></div>
			<div id="macaddress"  style="display:none">			
		</div>
	</div>
<br />
<div class="roundheader" onclick="toggle_advoptions(\'network\');" style="cursor:pointer;"><label id="network_advoptions_ind" style="width:10px;">+</label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$l['networksettings'].'</div>
<div id="network_advoptions" style="display:none; width:100%">
	<div class="bgaddv">';

	// Internal IP
	if(!empty($ips_int)){
	
	echo '<div class="row">
		<div class="col-sm-6">
			<label class="control-label">'.$l['ips_int'].'</label>
		<span class="help-block">'.$l['ips_int_exp'].'</span>
		</div>
		<div class="col-sm-6">';
			 
			$_ips_int = @$_POST['ips_int'];
			
			if(is_array($_ips_int) && !empty($_ips_int)){
				foreach($_ips_int as $k => $ip){
					if(empty($ip)){
						unset($_ips_int[$k]);
					}
				}
			}
			
			if(empty($_ips_int)){
				$_ips_int = array(NULL);
			}
			
			if(!empty($ips_int)){
			
				echo '<select id="ips_int" class="form-control" name="ips_int[]" size="5" multiple="multiple">';
				foreach($ips_int as $k => $v){	 
					echo '<option value="'.$v['ip'].'" '.(@in_array($v['ip'], $_POST['ips_int']) ? 'selected="selected"' : '').'>'.$v['ip'].'</option>';
				}		
				echo'</select>';
				
			}else{
				'&nbsp;';
			}		
				
	echo '</div>
	</div><br/>';
	}
	
echo '
	<div class="row">
		<div class="col-sm-6">
			<label class="control-label">'.$l['upload_speed'].'</label>
			<span class="help-block">'.$l['upload_speed_exp'].'</span>
		</div>
		<div class="col-xs-8 col-sm-2">
			<input type="text" class="form-control" name="upload_speed" id="upload_speed" size="8" value="'.POSTval('upload_speed', '-1').'" onchange="handle_capping();" />
		 </div>
		 <div class="col-xs-1" style="padding-top:8px">'.$l['net_kb'].'</div>
		 <div class="col-xs-8 col-sm-3 sm_space">
			<select class="form-control speedmbits" name="upload_speed2" id="upload_speed2" onchange="upspeed(this.value)"></select>
		 </div>
	</div><br/>
	
	<div class="row">
		<div class="col-xs-10 col-sm-6">
			<label class="control-label">'.$l['band_suspend'].'</label>
			<span class="help-block">'.$l['exp_band_suspend'].'</span>
		</div>
		<div class="col-xs-2 col-sm-6">
			<input type="checkbox" class="ios" name="show_band_suspend" id="band_suspend" '.POSTchecked('band_suspend').' onchange="change_checkbox(\'band_suspend\'); handle_capping();"/>
				<input type="hidden" name="band_suspend" id="hidden_band_suspend">
		</div>
	</div>
	<div id="speed_cap_limit">
		<div class="row">
			<div class="col-xs-10 col-sm-6">
				<label class="control-label" for="speed_cap_down">'.$l['speed_cap_down'].'</label>
				<span class="help-block">'.$l['exp_speed_cap_down'].'</span>
			</div>
			<div class="col-sm-2 col-xs-8">
				<input type="number" class="form-control" name="speed_cap_down" id="speed_cap_down" onmouseout="blur();" />			
			</div>
			<div class="col-sm-1" style="padding-top:8px">'.$l['net_kb'].'</div>
			<div class="col-sm-3 col-xs-8 sm_space">
				<select class="form-control speedmbits" name="speed_cap_down2" id="speed_cap_down2" onchange="$(\'#speed_cap_down\').val(this.value)"></select>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-10 col-sm-6">
				<label class="control-label" for="speed_cap_up">'.$l['speed_cap_up'].'</label>
				<span class="help-block">'.$l['exp_speed_cap_up'].'</span>
			</div>
			<div class="col-sm-2 col-xs-8">
				<input type="number" class="form-control" name="speed_cap_up" id="speed_cap_up" onmouseout="blur();" />
			</div>
			<div class="col-sm-1" style="padding-top:8px">'.$l['net_kb'].'</div>
			<div class="col-sm-3 col-xs-8 sm_space">
				<select class="form-control speedmbits" name="speed_cap_up2" id="speed_cap_up2" onchange="$(\'#speed_cap_up\').val(this.value)"></select>
			</div>
		</div>
	</div>';
	
	if(!empty($supported_nics)){
		
		echo '<div class="row">
			<div class="col-sm-6">
				<label class="control-label">'.$l['changenic'].'</label>
				<span class="help-block">'.$l['exp_changenic'].'</span>
			</div>
			<div class="col-sm-6">
				<select class="form-control" name="nic_type" id="nic_type" >';
				
					foreach($supported_nics as $k => $v){
						
						echo '<option value="'.$v.'" '.POSTselect('nic_type', $v).'>'.(!empty($l['nic_'.$v]) ? $l['nic_'.$v] : $v).'</option>';
					}
				echo '</select>
			</div>
		</div><br/>';
	}
	
	
	// All kernel. DNS Nameserver 
	echo '<div class="row">
			<div class="col-sm-6">
				<label class="control-label">'.$l['dns'].'</label>
				<span class="help-block">'.$l['exp_dns'].'</span>
			</div>
			<div class="col-sm-6">
				<div class="row">
					<div class="col-sm-12" id="dnstable">';
				 
						$_dns = @$_POST['dns'];
						
						if(is_array($_dns) && !empty($_dns)){
							foreach($_dns as $d => $ds){
								if(empty($ds)){
									unset($_dns[$d]);
								}
							}
						}
						
						if(empty($_dns)){
							$_dns = array(NULL);
						}
					 
						foreach($_dns as $dn){
							echo '<div class="row">
								<div class="col-sm-6">
									<input type="text" class="form-control" name="dns[]" value="'.$dn.'"/>
								</div>
							</div>';
						}
						
					echo '<br />
					</div>
				</div>
				<input type="button" onclick="adddnsrow(\'dnstable\')" class="go_btn" value="'.$l['add_dns'].'"/>
				<span class="help-block"></span>
			</div>
		</div><br/>';
// End of network settings
echo '</div>
</div>

<br />
<div class="roundheader hide_for_plan" onclick="toggle_advoptions(\'adv\');" style="cursor:pointer;"><label id="adv_advoptions_ind"  style="width:10px;">+</label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$l['addvoption'].'</div>
<div id="adv_advoptions" style="display:none; width:100%" class="hide_for_plan">
	<div class="col-sm-12 bgaddv">
		<div class="row">
			<div class="col-sm-6">
				<label class="control-label">'.$l['control_panel'].'</label>
				<span class="help-block">'.$l['control_panel_exp'].'</span>
			</div>
			<div class="col-sm-6">
				<select class="form-control" name="control_panel" id="control_panel" onchange="show_webuzo(this.value);">
					<option value="0" '.POSTselect('control_panel', 'none').'>None</option>
					<option value="cpanel" '.POSTselect('control_panel', 'cpanel').'>cPanel</option>
					<option value="webuzo" '.POSTselect('control_panel', 'webuzo').'>Webuzo</option>
					<option value="plesk" '.POSTselect('control_panel', 'plesk').'>Plesk</option>
					<option value="webmin" '.POSTselect('control_panel', 'webmin').'>Webmin</option>
					<option value="interworx" '.POSTselect('control_panel', 'interworx').'>Interworx</option>
					<option value="ispconfig" '.POSTselect('control_panel', 'ispconfig').'>ISPConfig</option>
					<option value="cwp" '.POSTselect('control_panel', 'cwp').'>CentOS Web Panel</option>
					<option value="vesta" '.POSTselect('control_panel', 'vesta').'>VestaCP</option>
				</select>
			</div>
		</div><br/>
		<div id="show_webuzo_tr" class="col-sm-12" style="display:none;">
			<div id="webuzo_content">
				<span class="help-block">'.$l['webuzo_exp'].'</span><br />
				<div class="row">
					<div class="col-sm-4">
						<label class="control-label">'.$l['webuzo_spasswd'].'</label><br />
					</div>
					<div class="col-sm-6">							
						<input class="form-control" type="text" name="webuzo_spasswd" id="webuzo_spasswd" onkeyup="check_pass_strength(\'webuzo_spasswd\');"  placeholder="'.$l['webuzo_spasswd_p'].'" 
						value="'.POSTval('webuzo_spasswd', '').'">
						<div id="webuzo_spasswd_pass-strength-result" class="">'.$l['strength_indicator'].'</div>							
					</div>
					<div class="col-sm-2">
						<a href="javascript: void(0);" onclick="$_(\'webuzo_spasswd\').value=randstr(12, 1, '.(!empty($globals['pass_strength']) ? $globals['pass_strength'] : 0).');check_pass_strength(\'webuzo_spasswd\');return false;" title="'.$l['randpass'].'"><img src="'.$theme['images'].'randpass.gif" /></a>
					</div>
				</div><br />
				<div class="row">
					<div class="col-sm-4">
						<label class="control-label">'.$l['webuzo_pd'].'</label><br />
					</div>
					<div class="col-sm-6">							
						<input class="form-control" type="textbox" name="webuzo_pd" id="webuzo_pd" placeholder="'.$l['webuzo_pd_p'].'" 
						value="'.POSTval('webuzo_pd', '').'">			
					</div>
					<div class="col-sm-2"></div>
				</div><br />
				<div class="row">
					<div class="col-sm-4">
						<label class="control-label">'.$l['webuzo_scriptlist'].'</label><br />
					</div>
					<div class="col-sm-6">						
						<select id="webuzo_scriptlist" class="chosen" onchange="script_req(this.value);" name="webuzo_script">
						<option value="0">'.$l['none'].'</option>';
						foreach($allowed_scripts as $k => $v){
							echo '<optgroup label="'.$k.'">';
							$val = $v;
							foreach($val as $vk => $vv){
								echo '<option value="'.$vk.'">'.$vv['name'].'</option>';
							}
							echo '</optgroup>';
						}
						echo ';
						</select>
					</div>
					<div class="col-sm-2"></div>
				</div><br />
				<div class="row">
					<div class="col-sm-4">
						<label class="control-label">'.$l['webuzo_os'].'</label><br />
					</div>
					<div class="col-sm-8">';
					
						foreach($webuzo_templates as $k => $v){
							echo '
							<div class="col-sm-12">
								<input for="input radio" type="radio" name="webuzo_os" id="webuzoos_'.$k.'" value="'.$k.'" '.(!empty($v['hvm']) ? 'hvm="1"' : '').' distro="'.$v['distro'].'"/>
								<label for="webuzoos_'.$k.'">'.$v['name'].'</label>
							</div>';
						}
					echo '</div>
				</div><br />
				<div class="row">
					<div class="col-sm-4">
						<label class="control-label">'.$l['webuzo_appstack'].'</label><br />
					</div>
					<div class="col-sm-8">
						<div class="col-sm-3" id="stack1_tr">
							<input type="radio" name="webuzo_stack" id="stack1" value="lamp" onclick="webuzo_apps(this.value);" checked="checked"/>
							<label for="stack1">LAMP</label>
						</div>
						<div class="col-sm-3" id="stack2_tr" style="display:'.(!empty($webuzo_done['isfree']) ? 'none' : '').';">
							<input type="radio" name="webuzo_stack" id="stack2" value="lemp" onclick="webuzo_apps(this.value);" />
							<label for="stack2">LEMP</label>
						</div>
						<div class="col-sm-3" id="stack3_tr" style="display:'.(!empty($webuzo_done['isfree']) ? 'none' : '').';">
							<input type="radio" name="webuzo_stack" id="stack3" value="llmp" onclick="webuzo_apps(this.value);" />
							<label for="stack3">LLMP</label>
						</div>
					</div>
				</div><br />
				<div class="adv_border" id="webuzo_stack_tr" style="padding:1.2%;display:'.(!empty($webuzo_done['isfree']) ? 'none' : '').';">
					<input type="hidden" id="script_isfree" value="0" />
					<div class="row" id="webuzo_webserver_tr">
						<div class="col-sm-4">
							<label class="control-label" style="margin:8px;">'.$l['webuzo_apache'].'</label><br />
						</div>
						<div class="col-sm-8" id="webuzo_webserver">';
							
							foreach($webuzo_apps['webserver'] as $wk => $wv){
								echo '<div class="col-sm-4">
										<table>
											<tr>
												<td>
													<input type="radio" name="webserver" id="serverver_'.$wk.'" value="'.$wv['softname'].'" />
												</td>
												<td width="12%"></td>
												<td>
													<label class="_label" for="serverver_'.$wk.'">'.$wv['fullname'].'</label>
												</td>
											</tr>
										</table>
									</div>';
							}
						echo '</div>
					</div><br />
					<div class="row">
						<div class="col-sm-4">
							<label class="control-label" style="margin:8px;">'.$l['webuzo_mysql'].'</label><br />
						</div>						
						<div class="col-sm-8" id="webuzo_mysql">';
							
							foreach($webuzo_apps['mysql'] as $wk => $wv){
								echo '<div class="col-sm-4">
										<table>
											<tr>
												<td>
													<input type="radio" name="mysql" id="serverver_'.$wk.'" value="'.$wv['softname'].'" />
												</td>
												<td width="12%"></td>
												<td>
													<label class="_label" for="serverver_'.$wk.'">'.$wv['fullname'].'</label>
												</td>
											</tr>
										</table>
									</div>';
							}
						echo '</div>				
					</div><br />
					<div class="row">
						<div class="col-sm-4">
							<label style="margin:8px;" class="control-label">'.$l['webuzo_php'].'</label><br />
						</div>	
						<div class="col-sm-8" id="webuzo_php">';
							
							foreach($webuzo_apps['php'] as $wk => $wv){
								echo '<div class="col-sm-4">
										<table>
											<tr>
												<td>
													<input type="radio" name="php" id="serverver_'.$wk.'" value="'.$wv['softname'].'" />
												</td>
												<td width="12%"></td>
												<td>
													<label class="_label" for="serverver_'.$wk.'">'.$wv['fullname'].'</label>
												</td>
											</tr>
										</table>
									</div>';
							}
						echo '</div>
					</div><br />
				</div><br />
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<label class="control-label">'.$l['recipe'].'</label>
				<span class="help-block">'.$l['recipe_exp'].'</span>
			</div>
			<div class="col-sm-6 server-select-lg">
				<select class="form-control virt-select" name="recipe" id="recipe" style="width:100%">
					<option value="0" '.POSTselect('recipe', 'none').'>None</option>';
					
					foreach($recipes as $k => $v){
						echo '<option value="'.$k.'" '.POSTselect('recipe', $k).'>'.$v['name'].'</option>';
					}
					
				echo '</select>
			</div>
		</div><br/>';
		
		// CPU Affinity. (CPU PIN)
		if($kernel->features('cpu_pinning', $virt)){
			
			echo '<div class="row">
				<div class="col-xs-10 col-sm-6">
					<label class="control-label">'.$l['cpupin'].'</label>
					<span class="help-block">'.$l['cpupin_exp'].'</span>
				</div>
				<div class="col-xs-2 col-sm-6">
					<label><input type="checkbox" class="ios" name="allcores" id="allcores" '.(!empty($_POST) ? (is_array($_POST['cpupin']) ? '' : 'checked="checked"') : 'checked="checked"').' value="-1" onchange="pincheck();" />&nbsp;Default</label>';
				echo '
				</div>
			</div>
			<div id="pincores" class="row">
				<div class="col-xs-10 col-sm-6">
					<label class="control-label">'.$l['cpupin_select'].'</label>
				</div>
				
				<div class="col-xs-2 col-sm-6">';
				for($i=0; $i < $resources['cpucores']; $i++){
					echo '<label><input type="checkbox" id="pin'.$i.'" name="cpupin['.$i.']" value="'.$i.'" '.(in_array($i, @$_POST['cpupin']) || in_array((string)$i, @$_POST['cpupin']) ? 'checked="checked"' : '').' />&nbsp;vCPU '.($i+1).'&nbsp;</label>';
				}
				
				echo '</div>
			</div><br/>';
		}
	
		if($kernel->features('numa', $virt)){
			echo '
			<div class="row">
				<div class="col-sm-6">
					<label class="control-label">'.$l['usenuma'].'</label>
					<span class="help-block">&nbsp;</span>
				</div>
				<div class="col-sm-6">
					<input type="checkbox" class="ios" name="numa" value="1" id="numa" '.POSTchecked('numa').' />
				</div>
			</div>';
		}
		 
		if($kernel->features('virtio', $virt)){
			echo '
			<div class="row">
				<div class="col-xs-10 col-sm-6">
					<label class="control-label">'.$l['usevirtio'].'</label>
					<span class="help-block">'.$l['exp_usevirtio'].'</span>
				</div>
				<div class="col-xs-2 col-sm-6">
					<input type="checkbox" class="ios" name="virtio" value="1" id="virtio" '.POSTchecked('virtio', 1).' />
				</div>
			</div>';
		}
		
		if($kernel->features('cpu_mode', $virt)){
			echo '
			<div class="row">
				<div class="col-sm-6">
					<label class="control-label">'.$l['cpu_mode'].'</label>
					<span class="help-block">&nbsp;</span>
				</div>
				<div class="col-sm-6">
					<select class="form-control" name="cpu_mode" id="cpu_mode">';
					
						foreach($cpu_modes as $ck => $cv){
							echo '<option value="'.$ck.'" '.POSTselect('cpu_mode', $ck).'>'.$cv.'</option>';
						}
						
					echo '
					</select>
				</div>
			</div>';
		}
			
		if($kernel->features('cpu_topology', $virt)){
			echo '
		<div class="row" id="enable_cpu_topology_row">
			<div class="col-sm-6">
				<label class="control-label">'.$l['enable_cpu_topology'].'</label>
				<span class="help-block">'.$l['enable_cpu_topology_exp'].'</span>
			</div>
			<div class="col-sm-6">
				<input type="checkbox" class="ios" name="enable_cpu_topology" id="enable_cpu_topology" '.POSTchecked('enable_cpu_topology').' onchange="change_cpu_topology();" />
			</div>
		</div>
		<div id="cpu_topology" style="display:none;">
			<div class="row">
				<div class="col-sm-6">
					<label class="control-label">Sockets</label>
					<span class="help-block">&nbsp;</span>
				</div>
				<div class="col-sm-6">
					<input type="text" name="topology_sockets" id="topology_sockets" class="form-control numbersonly" value="'.POSTval('topology_sockets').'" />
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<label class="control-label">Cores</label>
					<span class="help-block">&nbsp;</span>
				</div>
				<div class="col-sm-6">
					<input type="text" class="form-control numbersonly" name="topology_cores" id="topology_cores" value="'.POSTval('topology_cores').'" />
				</div>
			</div>';	
			if($kernel->features('cpu_threads', $virt)){
				echo '
			<div class="row">
				<div class="col-sm-6">
					<label class="control-label">Threads</label>
					<span class="help-block">&nbsp;</span>
				</div>
				<div class="col-sm-6">
					<input type="text" class="form-control numbersonly" name="topology_threads" id="topology_threads" value="'.POSTval('topology_threads').'" />
				</div>
			</div>';
			}
			echo '
		</div>';
		}
		
		if(!empty($isos) && $kernel->features('sec_iso_support', $virt)){
			
			echo '
		<div class="row">
			<div class="col-sm-6">
				<label class="control-label">'.$l['sec_vsiso'].'</label>
				<span class="help-block">'.$l['sec_vsiso_exp'].'</span>
			</div>
			<div class="col-sm-6">
				<select class="form-control chosen" name="sec_iso" id="sec_iso">
				<option value="0" '.(POSTval('sec_iso') == 0 ? 'selected="selected"' : '').'>'.$l['none'].'</option><optgroup label="'.$l['admin_iso'].'">';
				
				foreach($isos as $k => $v){
					echo '<option value="'.$k.'" '.(POSTval('sec_iso') == $k ? 'selected="selected"' : '').'>'.$v['name'].'</option>';
				}
				
			 echo'</optgroup></select>
			</div>
		</div><br/>';
		}
		
		if($kernel->features('kvm_cache', $virt)){
			echo '
		<div class="row">
			<div class="col-sm-6">
				<label class="control-label">'.$l['kvm_cache'].'</label>
				<span class="help-block">'.$l['exp_kvm_cache'].'</span>
			</div>
			<div class="col-sm-6">
				<select class="form-control" name="kvm_cache" id="kvm_cache" >
					<option value="0" '.POSTselect('kvm_cache', 'none').'>None</option>
					<option value="writeback" '.POSTselect('kvm_cache', 'writeback').'>Writeback</option>
					<option value="writethrough" '.POSTselect('kvm_cache', 'writethrough').'>Writethrough</option>
					<option value="directsync" '.POSTselect('kvm_cache', 'directsync').'>Direct Sync</option>
					<option value="default" '.POSTselect('kvm_cache', 'default').'>Default</option>
				</select>
			</div>
		</div><br/>';
		}
			
		if($kernel->features('io_mode', $virt)){
			echo'
		<div class="row">
			<div class="col-sm-6">
				<label class="control-label">'.$l['io_mode'].'</label>
				<span class="help-block">&nbsp;</span>
			</div>
			<div class="col-sm-6">
				<select class="form-control" name="io_mode" id="io_mode" >
					<option value="0" '.POSTselect('io_mode', 'default').'>Default</option>
					<option value="native" '.POSTselect('io_mode', 'native').'>Native</option>
					<option value="threads" '.POSTselect('io_mode', 'threads').'>Threads</option>
				</select>
			</div>
		</div>';
			if(!distro_check(0, 0, 0, 0, 1)){
				echo'		
			<div class="row">
				<div class="col-xs-10 col-sm-6">
					<label class="control-label">'.$l['kvm_vga'].'</label>
					<span class="help-block"></span>
				</div>
				<div class="col-xs-2 col-sm-6">
					<input type="checkbox" class="ios" name="show_kvm_vga" id="kvm_vga" onchange="enable_accel();" '.POSTchecked('kvm_vga').' />
					<input type="hidden" name="kvm_vga" id="hidden_kvm_vga">
				</div>
			</div>
			<div class="row" id="enable_acceleration" style="display:none;">
				<div class="col-xs-10 col-sm-6">
					<label class="control-label">'.$l['acceleration'].'</label>
					<span class="help-block">'.$l['acceleration_exp'].'</span>
				</div>
				<div class="col-xs-2 col-sm-6">
					<input type="checkbox" class="ios" name="show_acceleration" id="acceleration" '.POSTchecked('acceleration').' onchange="change_checkbox(\'acceleration\');"/>
					<input type="hidden" name="acceleration" id="hidden_acceleration">
				</div>
			</div>
			<br/>';
				
			}
		}
		
		if(!empty($os_check)){
			echo '
		<div class="row">
			<div class="col-sm-6">
				<label class="control-label">'.$l['total_iops_sec'].'</label>
			</div>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="total_iops_sec" value="'.POSTval('total_iops_sec', '').'" />
			</div>
		</div><br/>
		<div class="row">
			<div class="col-sm-6">
				<label class="control-label">'.$l['read_bytes_sec'].'</label>
			</div>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="read_bytes_sec" value="'.POSTval('read_bytes_sec', '').'" />&nbsp; MB/s
			</div>
		</div><br/>
		<div class="row">
			<div class="col-sm-6">
				<label class="control-label">'.$l['write_bytes_sec'].'</label>
			</div>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="write_bytes_sec" value="'.POSTval('write_bytes_sec', '').'" />&nbsp; MB/s
			</div>
		</div><br/>';
		}
		
		if($kernel->features('ebtables_support', $_virt)){
					
			echo '<div class="row">
				<div class="col-sm-6">
					<label class="control-label">'.$l['disable_ebtables'].'</label>
					<span class="help-block">&nbsp;</span>
				</div>
				<div class="col-sm-6">
					<input type="checkbox" class="ios" name="show_disable_ebtables" id="disable_ebtables" '.POSTchecked('disable_ebtables').' onchange="change_checkbox(\'disable_ebtables\');"/>
					<input type="hidden" name="disable_ebtables" id="hidden_disable_ebtables">
				</div>
			</div><br />';
					
		}
		
		if($kernel->features('xenserver_tools', $_virt)){
		
			echo '<div class="row">
				<div class="col-sm-6">
					<label class="control-label">'.$l['install_xentools'].'</label>
					<span class="help-block">&nbsp;</span>
				</div>
				<div class="col-sm-6">
					<input type="checkbox" value="1" class="ios" name="install_xentools" id="install_xentools" '.POSTchecked('install_xentools').' />					
				</div>
			</div><br />';
		
		}
		
		if($kernel->features('win_support', $_virt)){
			
			echo '<div id="enable_rdp" class="row">
				<div class="col-xs-10 col-sm-6">
					<label class="control-label">'.$l['rdp'].'</label>
					<span class="help-block">'.$l['exp_rdp'].'</span>
				</div>
				<div class="col-xs-2 col-sm-6">
					<input type="checkbox" class="ios" name="show_rdp" id="rdp" '.POSTchecked('rdp').' onchange="change_checkbox(\'rdp\');"/>
					<input type="hidden" name="rdp" id="hidden_rdp">
				</div>
			</div><br/>';
		}
		
		if($kernel->features('vnc_key_map', $_virt)){
			
			echo '<div class="row">
				<div class="col-sm-6">
					<label class="control-label">'.$l['vnc_keymap'].'</label>
				</div>
				<div class="col-sm-6 server-select-lg">
					<select class="form-control virt-select" name="vnc_keymap" id="vnc_keymap" style="width:100%">
						<option value="en-us" '.POSTselect('vnc_keymap', 'en-us').'>en-us</option>
						<option value="de-ch" '.POSTselect('vnc_keymap', 'de-ch').'>de-ch</option>
						<option value="ar" '.POSTselect('vnc_keymap', 'ar').'>ar</option>
						<option value="da" '.POSTselect('vnc_keymap', 'da').'>da</option>
						<option value="et" '.POSTselect('vnc_keymap', 'et').'>et</option>
						<option value="fo" '.POSTselect('vnc_keymap', 'fo').'>fo</option>
						<option value="fr-be" '.POSTselect('vnc_keymap', 'fr-be').'>fr-be</option>
						<option value="fr-ch" '.POSTselect('vnc_keymap', 'fr-ch').'>fr-ch</option>
						<option value="hu" '.POSTselect('vnc_keymap', 'hu').'>hu</option>
						<option value="it" '.POSTselect('vnc_keymap', 'it').'>it</option>
						<option value="lt" '.POSTselect('vnc_keymap', 'lt').'>lt</option>
						<option value="mk" '.POSTselect('vnc_keymap', 'mk').'>mk</option>
						<option value="nl" '.POSTselect('vnc_keymap', 'nl').'>nl</option>
						<option value="no" '.POSTselect('vnc_keymap', 'no').'>no</option>
						<option value="pt" '.POSTselect('vnc_keymap', 'pt').'>pt</option>
						<option value="ru" '.POSTselect('vnc_keymap', 'ru').'>ru</option>
						<option value="sv" '.POSTselect('vnc_keymap', 'sv').'>sv</option>
						<option value="tr" '.POSTselect('vnc_keymap', 'tr').'>tr</option>
						<option value="de" '.POSTselect('vnc_keymap', 'de').'>de</option>
						<option value="en-gb" '.POSTselect('vnc_keymap', 'en-gb').'>en-gb</option>
						<option value="es" '.POSTselect('vnc_keymap', 'es').'>es</option>
						<option value="fi" '.POSTselect('vnc_keymap', 'fi').'>fi</option>
						<option value="fr" '.POSTselect('vnc_keymap', 'fr').'>fr</option>
						<option value="fr-ca" '.POSTselect('vnc_keymap', 'fr-ca').'>fr-ca</option>
						<option value="hr" '.POSTselect('vnc_keymap', 'hr').'>hr</option>
						<option value="is" '.POSTselect('vnc_keymap', 'is').'>is</option>
						<option value="ja" '.POSTselect('vnc_keymap', 'ja').'>ja</option>
						<option value="lv" '.POSTselect('vnc_keymap', 'lv').'>lv</option>
						<option value="nl-be" '.POSTselect('vnc_keymap', 'nl-be').'>nl-be</option>
						<option value="pl" '.POSTselect('vnc_keymap', 'pl').'>pl</option>
						<option value="pt-br" '.POSTselect('vnc_keymap', 'pt-br').'>pt-br</option>
						<option value="sl" '.POSTselect('vnc_keymap', 'sl').'>sl</option>
						<option value="th" '.POSTselect('vnc_keymap', 'th').'>th</option>
					</select>
				</div>
			</div>
			<br/>';
		}
				
		// Only available for XEN-HVM
		if(($virt == 'xen' && !empty($hvm)) && distro_check(0, 1, 1, 0, 1)){
			
			// PV-on-HVM drivers
			
			echo '<div id="tr_pvonhvm" class="row">
				<div class="col-xs-10 col-sm-6">
					<label class="control-label">'.$l['pv_on_hvm'].'</label>
					<span class="help-block">'.$l['exp_pv_on_hvm'].'</span>
				</div>
				<div class="col-xs-2 col-sm-6">
					<input type="checkbox" class="ios" name="show_pv_on_hvm" id="pv_on_hvm" '.POSTchecked('pv_on_hvm').' onchange="ispvonhvm();" />
					<input type="hidden" name="pv_on_hvm" id="hidden_pv_on_hvm">
				</div>
		   </div><br/>';
		}	
			
		if($virt == 'xen'){
			echo '<div id="tr_viftype" class="row">
				<div class="col-sm-6">
					<label class="control-label">'.$l['change_vif_type'].'</label>
					<span class="help-block">'.$l['exp_change_vif_type'].'</span>
				</div>
				<div class="col-sm-6">
					'.$l['viftype_netfront'].'<input type="radio" name="vif_type" id="vif_type"  value="netfront" '.POSTradio('vif_type', 'netfront', 'netfront').' />
					'.$l['viftype_ioemu'].'<input type="radio" name="vif_type" id="vif_type"  value="ioemu" '.POSTradio('vif_type', 'ioemu').' />
				</div>
			</div><br/>';
		}
				
		// TUN/TAP
		if($virt == 'openvz' || $virt == 'vzo' || $virt == 'proxo'){ 
		
			echo '
			<div class="row">
				<div class="col-xs-10 col-sm-6">
					<label class="control-label">'.$l['tuntap'].'</label>
					<span class="help-block">'.$l['exp_tuntap'].'</span>
				</div>
				<div class="col-xs-2 col-sm-6">
					<input type="checkbox" class="ios" name="show_tuntap" id="tuntap" '.POSTchecked('tuntap').' onchange="change_checkbox(\'tuntap\');"/>
					<input type="hidden" name="tuntap" id="hidden_tuntap">
				</div>
			</div><br/>
			<div class="row">
				<div class="col-xs-10 col-sm-6">
					<label class="control-label">'.$l['ppp'].'</label>
					<span class="help-block">'.$l['exp_ppp'].'</span>
				</div>
				<div class="col-xs-2 col-sm-6">
					<input type="checkbox" class="ios" name="show_ppp" id="ppp" '.POSTchecked('ppp').' onchange="change_checkbox(\'ppp\');"/>
					<input type="hidden" name="ppp" id="hidden_ppp">
				</div>
			</div><br/>
			<div class="row">
				<div class="col-xs-10 col-sm-6">
					<label class="control-label">'.$l['fuse'].'</label><br />
					<span class="help-block">'.$l['fuse_exp'].'</span>
				</div>
				<div class="col-xs-2 col-sm-6">
					<input type="checkbox" value="1" name="show_fuse" '.POSTchecked('fuse').' onchange="change_checkbox(\'fuse\');" />
					<input type="hidden" name="fuse" id="hidden_fuse">
				</div>
			</div>
			<div class="row">
				<div class="col-xs-10 col-sm-6">
					<label class="control-label">'.$l['ipip'].'</label><br />
					<span class="help-block">'.$l['ipip_exp'].'</span>
				</div>
				<div class="col-xs-2 col-sm-6">
					<input type="checkbox" value="1" name="show_ipip" '.POSTchecked('ipip').' onchange="change_checkbox(\'ipip\');"/>
					<input type="hidden" name="ipip" id="hidden_ipip">
				</div>
			</div>
			<div class="row">
				<div class="col-xs-10 col-sm-6">
					<label class="control-label">'.$l['ipgre'].'</label><br />
					<span class="help-block">'.$l['ipgre_exp'].'</span>
				</div>
				<div class="col-xs-2 col-sm-6">
					<input type="checkbox" value="1" name="show_ipgre" '.POSTchecked('ipgre').' onchange="change_checkbox(\'ipgre\');"/>
					<input type="hidden" name="ipgre" id="hidden_ipgre">
				</div>
			</div>
			<div class="row">
				<div class="col-xs-10 col-sm-6">
					<label class="control-label">'.$l['nfs'].'</label><br />
					<span class="help-block">'.$l['nfs_exp'].'</span>
				</div>
				<div class="col-xs-2 col-sm-6">
					<input type="checkbox" value="1" name="show_nfs" '.POSTchecked('nfs').' onchange="change_checkbox(\'nfs\');"/>
					<input type="hidden" name="nfs" id="hidden_nfs">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<label class="control-label">'.$l['quotaugidlimit'].'</label><br />
					<span class="help-block">'.$l['quotaugidlimit_exp'].'</span>
				</div>
				<div class="col-sm-6">
					<input type="text" class="form-control" name="quotaugidlimit" id="quotaugidlimit" size="30" value="'.POSTval('quotaugidlimit').'" />
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<label class="control-label">'.$l['iolimit'].'</label><br />
					<span class="help-block">'.$l['iolimit_exp'].'</span>
				</div>
				<div class="col-sm-6">
					<input type="text" class="form-control" name="iolimit" id="iolimit" size="30" value="'.POSTval('iolimit').'" />
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<label class="control-label">'.$l['iopslimit'].'</label><br />
					<span class="help-block">'.$l['iopslimit_exp'].'</span>
				</div>
				<div class="col-sm-6">
					<input type="text" class="form-control" name="iopslimit" id="iopslimit" size="30" value="'.POSTval('iopslimit').'" />
				</div>
			</div>';
		}
					
		// OS Reinstall
		echo '<div class="row">
				<div class="col-sm-6">
					<label class="control-label">'.$l['osreinstall'].'</label>
					<span class="help-block">'.$l['exp_osreinstall'].'</span>
				</div>
				<div class="col-sm-6">
					<input type="text" class="form-control" name="osreinstall_limit" id="osreinstall_limit"  value="'.POSTval('osreinstall_limit', '').'"/>
				</div>
			</div><br/>
			<div class="row hide_for_plan">
				<div class="col-sm-6">
					<label class="control-label">'.$l['admin_managed'].'</label><br />
					<span class="help-block">'.$l['exp_admin_managed'].'</span>
				</div>
				<div class="col-sm-6">
					<input type="checkbox" class="ios" name="show_admin_managed" id="admin_managed" '.POSTchecked('admin_managed').' onchange="change_checkbox(\'admin_managed\');"/>
					<input type="hidden" name="admin_managed" id="hidden_admin_managed">
				</div>
			</div>';
			
			if($kernel->features('nw_config', $_virt)){
				
				echo '<div class="row">
					<div class="col-sm-6">
						<label class="control-label">'.$l['disable_nw_config'].'</label>
						<span class="help-block">'.$l['exp_disable_nw_config'].'</span>
					</div>
					<div class="col-sm-6">
						<input type="checkbox" class="ios" name="show_disable_nw_config" id="disable_nw_config" '.POSTchecked('disable_nw_config', $vps['disable_nw_config']).' onchange="change_checkbox(\'disable_nw_config\');"/>
						<input type="hidden" name="disable_nw_config" id="hidden_disable_nw_config" value="'.$vps['disable_nw_config'].'">
					</div>
				</div>';
			}
			
			if(!empty($backup_plans)){
			
				echo '
			<div class="row">
				<div class="col-sm-6">
					<label class="control-label">'.$l['backup_plan'].'</label>
					<span class="help-block">'.$l['backup_plan_exp'].'</span>
				</div>
				<div class="col-sm-6 server-select-lg">
					<select class="form-control virt-select" name="bpid" style="width:100%">
							<option value="0" '.POSTselect('bpid', 0).'>'.$l['none'].'</option>
							<option value="-1" '.POSTselect('bpid', -1, true).'>'.$l['same_as_vps_plan'].'</option>';
					foreach($backup_plans as $bk => $bv){		 
							echo '
							<option value="'.$bk.'" '.POSTselect('bpid', $bk).'>'.$bv['plan_name'].'</option>';
						}
					echo '
					</select>
				</div>
			</div>';
			
			}
			
	echo '
			<div class="row user_details" style="display:none">
				<div class="col-sm-6">
					<label class="control-label">'.$l['dnsplid'].'</label>
					<span class="help-block">'.$l['dnsplid_exp'].'</span>
				</div>
				<div class="col-sm-6 server-select-lg">		
					<select class="form-control virt-select" name="dnsplid" style="width:100%">';
						 foreach($dnsplans as $dk => $dv){		 
							echo '<option value="'.$dk.'" '.(POSTval('dnsplid') == $dk ? 'selected="selected"' : '').'>'.$dv['plan_name'].'</option>';
						}
				
					echo '</select>
				</div>
			</div>
	</div>
</div>

<div class="col-sm-12">
<span class="help-block"></span><br>
<div class="row hide_for_plan">
	<div class="col-sm-6">
		<label class="control-label">'.$l['mg'].'</label>
		<span class="help-block">'.$l['mg_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<select class="form-control" name="mgs[]" id="mg" multiple="multiple">';
			
			foreach($mgs as $mk => $mv){
				if(strpos($_virt, $mv['mg_type']) === false) continue;
				echo '<option value="'.$mk.'" type="'.$mv['mg_type'].'" '.(in_array($mk, @$_POST['mgs']) ? 'selected="selected"' : '').'>'.$mv['mg_name'].'</option>';
			}
			
		echo '</select>
	</div>
</div><br /><br />
</div>
<center><input type="submit" class="btn" name="addvps" value="'.$l['submit'].'"/></center>


<br /><br />
</form>
</div>
<div id="progress_onload"></div>';

}

}// End of Server Offline if

echo '</div>';
softfooter();
echo '
<div id="processing_symb"><img src="'.$theme['images'].'loader.gif" /></div>
';

}

?>