<?php

//////////////////////////////////////////////////////////////
//===========================================================
// addsg_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function addsg_theme(){

global $theme, $globals, $kernel, $user, $l , $cluster, $error, $done;

softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-servers icon-head"></i>&nbsp; '.$l['add_sg'].'<span style="float:right;" ><a href="'.$globals['docs'].'Server_Groups" target="_blank" class="wiki_help" title="'.$l['wiki_help'].'"><i class="icon-help" ></i></a></span></center>';

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['added'].'</div><br />';
}else{

error_handle($error);

echo '
<div id="form-container">

<form accept-charset="'.$globals['charset'].'" name="addsg" method="post" action="" class="form-horizontal">
	<div class="row">
		<div class="col-sm-5">
			<label class="control-label">'.$l['sg_name'].'</label>
			<span class="help-block">'.$l['sg_name_exp'].'</span>
		</div>
		<div class="col-sm-6">
			<input type="text" class="form-control" name="sg_name" size="30" value="'.POSTval('sg_name', '').'" />
		</div>
	</div>
	<div class="row">
		<div class="col-sm-5">
			<label class="control-label">'.$l['sg_reseller_name'].'</label>
			<span class="help-block">'.$l['sg_reseller_name_exp'].'</span>
		</div>
		<div class="col-sm-6">
			<input type="text" class="form-control" name="sg_reseller_name" size="30" value="'.POSTval('sg_reseller_name', '').'" />
		</div>
	</div>
	<div class="row">
		<div class="col-sm-5">
			<label class="control-label">'.$l['sg_desc'].'</label>
			<span class="help-block">&nbsp;</span>
		</div>
		<div class="col-sm-6">
			<textarea class="form-control" name="sg_desc" rows="5" cols="40" >'.POSTval('sg_desc', '').'</textarea>
		</div>
	</div>
	<br/>	
	<!--<div class="row">
		<div class="col-sm-5">
			<label class="control-label">'.$l['sg_select'].'</label>
			<span class="help-block">&nbsp;</span>
		</div>
		<div class="col-sm-6">
			<input type="radio" name="sg_select" value="0" '.POSTradio('sg_select', '0', 0).' /> '.$l['least_util'].' &nbsp; &nbsp; <input type="radio" name="sg_select" value="1" '.POSTradio('sg_select', '1', 0).' /> '.$l['first_avl'].'
		</div>
	</div>-->
	<div class="row">
		<div class="col-sm-5 col-xs-10">
			<label class="control-label">'.$l['sg_ha_enable'].'</label>
			<span class="help-block">'.$l['sg_ha_enable_exp'].'</span>
		</div>
		<div class="col-sm-6 col-xs-2">
			<input type="checkbox" name="sg_ha" '.POSTchecked('sg_ha').'  value="1" />
		</div>
	</div>		
</div>

<br /><br />
<center><input type="submit" class="btn" name="addsg" value="'.$l['sub_but'].'" /></center>


</form>
</div>';

}

echo "</div>";
softfooter();

}

?>