<?php

//////////////////////////////////////////////////////////////
//===========================================================
// ssl_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function ssl_theme(){

global $theme, $globals, $cluster, $user, $l, $info, $keyconf, $crtconf, $csrconf, $cert_bundle, $done, $error;

softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-ssl icon-head"></i> &nbsp; '.$l['<title>'].'<span style="float:right;" ><a href="'.$globals['docs'].'Add_SSL_Certificate" target="_blank" class="wiki_help" title="'.$l['wiki_help'].'"><i class="icon-help" ></i></a></span></center>';

error_handle($error);

// Is it offline ?
$hypervisor_status = $cluster->statewise($globals['server']);
if($hypervisor_status == 0 || $hypervisor_status == 2){

	echo '<div class="e_notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['server_status_'.$hypervisor_status].'</div>';
	
}else{

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
}

echo '<form accept-charset="'.$globals['charset'].'" name="ssl" method="post" action="" class="form-horizontal">
<div class="row">
	<div class="col-sm-3">
		<label class="control-label">'.$l['sim_key'].'</label><br />
		<span class="help-block">'.$l['sim_key_desc'].'</span>
	</div>
	<div class="col-sm-9">
		<textarea style="color: #333333; border: 1px solid #CCCCCC; font-family:monospace" class="form-control" name="cert_key" rows="15">'.POSTval('cert_key', $keyconf).'</textarea>
	</div>
	
</div>

<br/>
<div class="row">
	<div class="col-sm-3">
		<label class="control-label">'.$l['sim_crt'].'</label><br />
		<span class="help-block">'.$l['sim_crt_desc'].'</span>
	</div>
	<div class="col-sm-9">
		<textarea style="color: #333333; border: 1px solid #CCCCCC; font-family:monospace" class="form-control" name="cert_crt" rows="15">'.POSTval('cert_crt', $crtconf).'</textarea>
	</div>	
</div>

<br/>
<div class="row">
	<div class="col-sm-3">
		<a name="csr"></a>
		<label class="control-label">'.$l['sim_bundle'].'</label><br />
		<span class="help-block">'.$l['sim_bundle_desc'].'</span>
	</div>
	<div class="col-sm-9">
		<textarea style="color: #333333; border: 1px solid #CCCCCC; font-family:monospace" class="form-control" name="cert_bundle" rows="15">'.POSTval('cert_bundle', $cert_bundle).'</textarea>
	</div>	
</div>

<br/>
<div class="row">
	<div class="col-sm-3">
		<a name="csr"></a>
		<label class="control-label">'.$l['sim_csr'].'</label><br />
		<span class="help-block">'.$l['sim_csr_desc'].'</span>
	</div>
	<div class="col-sm-9">
		<textarea style="color: #333333; border: 1px solid #CCCCCC; font-family:monospace" class="form-control" name="cert_csr" rows="15">'.$csrconf.'</textarea>
	</div>	
</div>

<br/>
<center><input type="submit" name="savessl" value="'.$l['submit'].'" class="btn"></center>
<br />
<br />
</form>';

}

echo '</div>';
softfooter();

}

?>