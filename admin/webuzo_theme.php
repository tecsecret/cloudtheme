<?php

//////////////////////////////////////////////////////////////
//===========================================================
// webuzo_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function webuzo_theme(){

global $theme, $globals, $info, $kernel, $user, $l, $error, $langs, $skins, $done, $cluster, $virt, $centos5, $val, $master, $servers, $iscripts, $scripts, $soft_scripts;

softheader($l['<title>']);

echo '<div class="bg" style="width:99%">';
echo '<center class="tit"><img src="'.$theme['images'].'webuzo_50.gif" />&nbsp; '.$l['page_head'].'</center>';

error_handle($error);

if(!empty($done)){
	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
}
echo '<link href="https://images.softaculous.com/sprites/20.css" rel="stylesheet" type="text/css" />
<script language="javascript" type="text/javascript">

var ids1 = new Array('.implode(', ', array_values($iscripts)).');
var cats = new Array("'.implode('","', array_keys($soft_scripts)).'");
var selected = new Array('.implode(', ', array_values($info['enabled_scripts'])).');
function toggle_option(option){
	if($("#"+option+"_softwares").is(":checked")){
		$("#"+option).slideDown("fast");
		$("#"+option+"_toggle_indicator").text(" - ");
		return;
	}
	
	if ($("#"+option).is(":hidden")){
		$("#"+option).slideDown("fast");
		$("#"+option+"_toggle_indicator").text(" - ");		
	}else{
		$("#"+option).slideUp("fast");
		$("#"+option+"_toggle_indicator").text(" + ");
	}
};

function checkallsoftware(checker){	
	for(x in ids1){		
		$_("scripts_"+ids1[x]).checked = checker.checked;
		//change td background color
		changebg(ids1[x],"scripts_");
	}
	for(x in cats){
		if(checker.checked){
			$("#"+cats[x]).slideDown("fast");
			$("#"+cats[x]+"_toggle_indicator").text(" - ");
		}else{
			$("#"+cats[x]).slideUp("fast");
			$("#"+cats[x]+"_toggle_indicator").text(" + ");
		}
	}
};

function checksoftwares(id){
	var ele = id.substr(0,(id.indexOf("_")));
	
	// Show all elements first
	$("#" + ele).find(".search").each(function(){
		$(this).show();
	});
	
	$("."+ele).each(function(){
		$(this).prop("checked", $("#"+id).is(":checked"));
	});
	
	if($("#"+id).is(":checked")){
		$(".script_"+ele).addClass("script_selected");
	}else{
		$(".script_"+ele).removeClass("script_selected");
	}
}

function selectchkbox(id,type,cat){
	if($_(type+id).checked){
		$_(type+id).checked = false;		
		$_(cat+"_softwares").checked = false;		
	}else{
		$_(type+id).checked = true;
	}
	
	//change td background color
	changebg(id,type);	
};

function changebg(id,type){
	try{
		if($_(type+id).checked){
			
			$("#td_"+id).addClass("script_selected");
			
		}else{
			
			$("#td_"+id).removeClass("script_selected");
		}
	}catch(e){}
};

function show_sel(id, show){
	if(show){
		$("#"+id+"_softwares").show();
	}else{
		$("#"+id+"_softwares").hide();
	}
	
}

function checkall(){
	
	// for users
	for(x in ids){	
		changebg(ids[x],"users_");
	}
	
	// for scripts
	for(x in ids1){	
		changebg(ids1[x],"scripts_");
	}
	
	// for scripts
	for(x in ids2){	
		changebg(ids2[x],"resellers_");
	}
	// for scripts
	for(x in ids3){	
		changebg(ids3[x],"cpplan_");
	}
};

function searchsoftware(val){
	val = val || 0;	
	if(val != 0){
		val = val.toUpperCase();		
		// hide all boxes
		for(x in cats){
			$("#"+cats[x]+"_block").hide();
		}
		// Search the keyword
		for(x in cats){
			$("#" + cats[x]).find(".search").each(function(){
				var appname = $(this).data("for");
				appname = appname.toUpperCase();
				if(appname.indexOf(val) > -1 ){
					console.log(appname);
					$("#"+cats[x]+"_block").show();
					$(this).css("display","").parent().removeClass("script_selected");
					return;
				}else{
					$(this).css("display","none").parent().removeClass("script_selected");
				}
			});
			$("#" + cats[x]).slideDown("fast");
			$("#"+cats[x]+"_toggle_indicator").text(" - ");
		}
		
	}else{
		// Reset all
		for(x in cats){
			$("#"+cats[x]+"_block").show();			
			$("#" + cats[x]).slideUp("fast");
			$("#"+cats[x]+"_toggle_indicator").text(" + ");			
			$("#" + cats[x]).find(".search").each(function(){
				$(this).show();
			});
		}
	}
}

$(document).ready(function(){
	
	for(x in cats){
		toggle_option(cats[x]);
	}
	
	// Check all if select all is enabled	
	for(x in selected){
		changebg(selected[x],"scripts_");
	}
	
	$("#softwaresearch").keyup(function(){		
		searchsoftware($(this).val());
	});
	
	/* for(x in cats){
		$.each($("#"+cats[x]), function(k, v));
	} */
});

</script>';

echo '<form accept-charset="'.$globals['charset'].'" name="webuzo_form" method="post" action="" class="form-horizontal" id="webuzo_form">
<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['disable_webuzo'].'</label><br />
		<span class="help-block">'.$l['disable_webuzo_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input type="checkbox" name="disable_webuzo" id="disable_webuzo" '.POSTchecked('disable_webuzo', $info['disable_webuzo']).' />
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['noc_apikey'].'</label><br />
		<span class="help-block">'.$l['noc_apikey_exp'].'</span>
	</div>
	<div class="col-sm-6">		
		<input class="form-control" type="text" name="noc_apikey" id="noc_apikey" size="30" value="'.POSTval('noc_apikey', $info['noc_apikey']).'" />
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<label class="control-label">'.$l['noc_apipass'].'</label><br />
		<span class="help-block">'.$l['noc_apipass_exp'].'</span>
	</div>
	<div class="col-sm-6">
		<input class="form-control" type="password" name="noc_apipass" id="noc_apipass" value="'.POSTval('noc_apipass', $info['noc_apipass']).'" />
	</div>
</div>';

echo '<div class="row">
<div class="col-sm-12 col-xs-12">
	<div class="roundheader" style="cursor:pointer;height:30px;">
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '.$l['script_assign'].'
	</div><br /><br/>
<div class="row">
	<div class="col-sm-3">
		<label class="control-label">'.$l['scripts_all'].'</label><br />
	</div>
	<div class="col-sm-2">
		<input type="checkbox" name="select_all" id="select_all" '.POSTchecked('select_all', $info['select_all']).' onclick="checkallsoftware(this);" value="-1" />
	</div>
	<div class="col-sm-7">
		<input type="text" class="form-control" id="softwaresearch" placeholder="'.$l['search_script'].'" />
	</div>
</div><br /><br/>
<div class="row">
	<div class="col-sm-12 col-xs-12">
		<center><h5><label>'.$l['scripts'].'</label></h5></center>		
	</div>
</div>
<hr />';
foreach($soft_scripts as $k => $v){
	echo '<div id="'.$k.'_block" onmouseover="show_sel(\''.$k.'\',true);" onmouseleave="show_sel(\''.$k.'\',false);">
			<div class="col-sm-12 col-xs-12">
				<div class="roundheader" onclick="toggle_option(\''.$k.'\');" style="cursor:pointer;height:30px;"><label id="'.$k.'_toggle_indicator" style="width:10px;">-</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp'.$l['cat_php_'.$k].'<input type="checkbox" id="'.$k.'_softwares" class="softwares" onclick="checksoftwares(this.id);" style="display:none;" /></div>
			<div id="'.$k.'" class="bgaddv">
				<div class="row">';
				$cellwidth = 100;
				$cellwidth = (count($v) < 2) ? 25 : ((count($v) < 3) ? 50 : $cellwidth);		
				
				echo '<table class="table-condensed appcell" width="'.$cellwidth.'%">';
					$i = 0;
					foreach($v as $appk => $appv){
						if(is_int($i/4)){
							echo '<tr>';
						}
						echo '<td width="20%" class="script_choose script_'.$k.'" id="td_'.$appk.'">
							<table width="95%" style="margin-top:4px;" class="search" data-for="'.$appv['name'].'">
							<tr>
							<td width="12%">
								<input class="'.$k.'" type="checkbox" id="scripts_'.$appk.'" name="sel_scripts[]" '.(in_array($appk, $info['enabled_scripts']) ? 'checked="checked"' : "").'  onclick="changebg(\''.$appk.'\',\'scripts_\');" value="'.$appk.'" />
							</td>
							<td width="12%">
								<div class="sp20_'.$appv['softname'].'"></div>
							</td>
							<td>
								<a href="javascript:void(0)" style="text-decoration: none; " onclick="selectchkbox(\''.$appk.'\',\'scripts_\', \''.$k.'\');" ><font class="sai_bboxtxt">'.$appv['name'].'</font></a>
							</td>
							</tr></table>
							</td>';
						$i++;
						if(is_int($i/4)){
							echo '</tr>';
						}
					}
					echo '</table></div>
			</div><br />
		</div>
	</div>';
}

echo '</div></div>';

echo '<input type="hidden" name="hidden_selected" id="hidden_selected" />
<input type="hidden" name="webuzo" id="webuzo" value="1" />
<br />
<center><input id="webuzo_settings" type="submit" name="webuzo_button" value="'.$l['submit'].'" class="btn"/></center>
</form>';

softfooter();

}

?>