<?php

//////////////////////////////////////////////////////////////
//===========================================================
// backup_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function databackup_theme(){

global $theme, $globals, $kernel, $user, $l, $error, $done, $vps_name, $vpslist, $filename, $tablesname, $checkval, $temp, $backup_settings, $backup_servers;


softheader($l['<title>']);

echo '
<div class="bg">
<center class="tit"><i class="icon icon-databackup icon-head"></i> &nbsp;'.$l['page_dbbackup_head'].'</center>';

error_handle($error);

$email = !empty($globals['dbbackup_email']) ? $globals['dbbackup_email'] : $globals['soft_email'];

echo '	
	<script language="javascript" type="text/javascript">
				
		$(document).ready(function(){';
		echo '				
		$("#dbtabs ul a").click(function(evt){
			evt.preventDefault();
			evt.stopImmediatePropagation();
			window.location.hash = $(this).attr("href");
			
			if($(this).attr("href")=="#cron"){
				$("#cron_tab").addClass("active");
				$("#immediate_tab").removeClass("active");
			}else{
				$("#immediate_tab").addClass("active");
				$("#cron_tab").removeClass("active");
			}
			
			$("#dbtabs ul a").removeClass("active");
			$(this).addClass("active");
			
			$("#dbtabs div.tab").hide();
			$("#dbtabs div"+$(this).attr("href")).show();
			
		});
		
		$("#type").change(dbbackup_type_change);
		
		dbbackup_type_change();
			
		';

		if(empty($backup_settings['vpsids'])){
			 echo '$("#vpses").each(function(){
					$("#vpses option").attr("selected", "selected");
			  });';
		}
		
	echo '
	});
	
	function Deldbback(id){

		id = id || 0;
		
		// List of ids to delete
		var dbback_list = new Array();
		
		if(id < 1){
			
			if($("#dbback_task_select").val() != 1){
				alert("'.$l['no_action'].'");
				return false;
			}
			
			$(".ios:checked").each(function() {
				dbback_list.push($(this).val());
			});
		
		}else{
			
			dbback_list.push(id);
			
		}
		
		if(dbback_list.length < 1){
			alert("'.$l['nothing_selected'].'");
			return false;
		}
		
		var dbback_conf = confirm("'.$l['del_conf'].'");
		if(dbback_conf == false){
			return false;
		}
		
		var finalData = new Object();
		finalData["id"] = dbback_list.join(",");
		
		//alert(finalData);
		//return false;
		
		$("#progress_bar").show();
		
		$.ajax({
			type: "POST",
			url: "'.$globals['index'].'act=databackup&api=json",
			data : finalData,
			dataType : "json",
			success: function(data){
				$("#progress_bar").hide();
				if("done" in data){
					alert("'.$l['action_completed'].'");
				}
				location.reload(true);
				
			},
			error: function(data) {
				$("#progress_bar").hide();
				//alert(data.description);
				return false;
			}
		});
		
		return false;
	};
	
	function dbbackup_type_change() {
		var val = $("#type").val();
		
		var backup_server = '.((int) POSTval('dbbackup_server', $globals['dbbackup_server'])).';
		
		if(val == "EMAIL") {
			$("#dbbackup_server_row, #dbbackup_server_dir_row").hide();
			$("#dbbackup_email_row").show();
		} else if(val == "SSH") {
			$("#dbbackup_server_row, #dbbackup_server_dir_row").show();
			$("#dbbackup_email_row").hide();
			
			$("#dbbackup_server option.ssh").show(); // Show all SSH servers
			$("#dbbackup_server option.ftp").hide(); // Hide all FTP servers
		} else if(val == "FTP") {
			$("#dbbackup_server_row, #dbbackup_server_dir_row").show();
			$("#dbbackup_email_row").hide();
			
			$("#dbbackup_server option.ssh").hide(); // Hide all SSH servers
			$("#dbbackup_server option.ftp").show(); // Show all FTP servers
		}
		
		var $selected_server = $("#dbbackup_server option[value="+backup_server+"]");
		
		if($selected_server.is(":disabled")) {
			$("#dbbackup_server").val(0);
		} else {
			$("#dbbackup_server").val(backup_server);
		}
	}
	
</script>
	
	
<div id="dbtabs">
	<ul class="nav nav-tabs">
		<li class="col-xs-6" id="cron_tab"><a href="#cron"><label class="control-label">'.$l['cron_backup'].'</label></a></li>
		<li class="col-xs-6" id="immediate_tab"><a href="#immediate"><label class="control-label">'.$l['immediate_backup'].'</label></a></li>
	</ul>
<br/>
<div id="cron" class="tab">
	<form accept-charset="'.$globals['charset'].'" action="" method="post" name="databasebackups" class="form-horizontal">
	<div class="row">
		<div class="col-sm-4">
			<label class="control-label">'.$l['sel_type'].'</label>
			<span class="help-block">'.$l['sel_type_exp'].'</span>
		</div>
		<div class="col-sm-8">
			<select id="type" class="form-control" name="type">
				<option value="EMAIL" '.ex_POSTselect('type', 'EMAIL', $globals['dbbackup_type']).'>'.$l['email'].'</option>
				<option value="SSH" '.ex_POSTselect('type', 'SSH', $globals['dbbackup_type']).'>'.$l['ssh'].'</option>
				<option value="FTP" '.ex_POSTselect('type', 'FTP', $globals['dbbackup_type']).'>'.$l['ftp'].'</option>
			</select>
		</div>
	</div>
	<div class="row" id="dbbackup_email_row">
		<div class="col-sm-4">
			<label class="control-label">'.$l['backup_email'].'</label>
			<span class="help-block">'.$l['backup_email_exp'].'</span>
		</div>
		<div class="col-sm-8">
			<input type="text" id="email" size="40" class="form-control" name="email" value="'.POSTval('email', $email).'"/>
		</div>
	</div>
	<div class="row" id="dbbackup_server_row">
		<div class="col-sm-4">
			<label class="control-label">'.$l['dbbackup_server'].'</label>
		<span class="help-block">'.$l['dbbackup_server_exp'].'</span>
		</div>
		<div class="col-sm-8">
			<select id="dbbackup_server" name="dbbackup_server" class="form-control">
				<option value="0">'.$l['select'].'</option>';
				
				foreach($backup_servers as $bs) {
					echo '<option class="'._strtolower($bs['type']).'" value="'.$bs['bid'].'" '.ex_POSTselect('dbbackup_server', $bs['bid'], $globals['dbbackup_server']).'>'.$bs['name'].'</option>';
				}
				
			echo '</select>
		</div>
	</div>
	<div class="row" id="dbbackup_server_dir_row">
		<div class="col-sm-4">
			<label class="control-label">'.$l['dbbackup_server_dir'].'</label>
		<span class="help-block">'.$l['dbbackup_server_dir_exp'].'</span>
		</div>
		<div class="col-sm-8">
			<input type="text" id="dbbackup_server_dir" size="40" class="form-control" name="dbbackup_server_dir" value="'.POSTval('dbbackup_server_dir', $globals['dbbackup_server_dir']).'"/>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-4">
			<label class="control-label">'.$l['dbbackup_cron'].'</label>
			<span class="help-block">'.$l['dbbackup_cron_exp'].'</span>
		</div>
		<div class="col-sm-8">
			<input type="text" id="dbbackup_cron" size="40" class="form-control" name="dbbackup_cron" value="'.POSTval('dbbackup_cron', $globals['dbbackup_cron']).'"/>
		</div>
	</div>
	<br /><br />
	<center>
		<input type="submit" class="btn" name="databasebackups"  id="databasebackups" value="'.$l['submit'].'"/>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="submit" name="databasebackups_delete"  id="databasebackups_delete" value="'.$l['delete_dbbackup_submit'].'" class="btn"/>
	</center>';
	if(!empty($done['cron_set'])){
		echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>';
	}
	echo'<br />
	</form>
	<div class="notebox"><font class="bboxtxt"><b>'.$l['note'].'</b> : '.$l['cron_master_note'].'</font></div>
</div>

<div id="immediate" class="tab">';
				
	if(!empty($filename)){
		
		echo '<br><form accept-charset="'.$globals['charset'].'" name="multi_dbback" id="multi_dbback" method="post" action="">
			<table align="center" cellpadding="5" cellspacing="1" border="0" width="100%" class="table table-hover tablesorter">
			 <tr>
				 <th align="center">'.$l['db_table_col1'].'</th>
				 <th colspan="2" align="center">'.$l['db_table_col2'].'</th>
				 <th><input type="checkbox" class="select_all" name="select_all" id="select_all" value="1"></th>
			</tr>';
			foreach($filename as $k =>$v){
				echo '<tr>
				<td>'.$v.'</td>
				<td width="20" ><center>			
					<a href="'.$globals['index'].'act=databackup&download='.$v.'" title="'.$l['title_download'].'" ><img src="'.$theme['images'].'admin/download.png" width="18" /></a></td>
				<td width="20" >
				<a href="javascript:void(0);" onclick="Deldbback(\''.rawurlencode($v).'\'); return false;"><img src="'.$theme['images'].'admin/delete.png" /></a>
				</cecnter>
			 </td>
			 <td width="20" valign="middle" align="center">
				<input type="checkbox" class="ios" name="dbback_list[]" value="'.rawurlencode($v).'"/>
			</td></tr>';
			}
		echo' </table>
		<div class="row bottom-menu">			
		<div class="col-sm-7"></div>
		<div class="col-sm-5"><label>'.$l['with_selected'].'</label>
		<select name="user_task_select" class="form-control" id="dbback_task_select">
			<option value="0">---</option>
			<option value="1">'.$l['ms_delete'].'</option>
		</select>&nbsp;
		<button type="submit" class="go_btn" id="dbback_submit" name="dbback_submit" onclick="Deldbback(); return false;">Go</button></div>
		</div>
		</form>
		<div id="progress_bar" class="form-group" style="height:125px; display:none">
			<br />
			<center class="col-sm-11">
				<font id="progress_txt" size="4" color="#222222">'.$l['action_msg'].'</font>
				<br>
				<br>
				<table id="table_progress" width="450" height="28" cellspacing="0" cellpadding="0" border="0" align="center" style="border:1px solid #CCC; -moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px;background-color:#efefef;">
					<tbody>
						<tr>
							<td id="progress_color" width="100%" style="background-image: url(themes/default/images/bar.gif); -moz-border-radius: 4px; -webkit-border-radius: 4px; border-radius: 4px;"></td>
							<td id="progress_nocolor"> </td>
						</tr>
					</tbody>
				</table>
				<br/>'.$l['notify_msg'].'
			</center>
		</div>';
	}
	echo '<form accept-charset="'.$globals['charset'].'" action="" method="post" name="databackup" onsubmit="dbsubmitval()" class="form-horizontal"><br />
	<center>
		<label><input type="checkbox" name="email_db" /> &nbsp;'.$l['email_backup'].' &nbsp;&nbsp;&nbsp;&nbsp;</label>
		<input type="submit" name="dbdown" value="'.$l['testbackup'].'" class="btn" />
		<!--<a href="'.$globals['index'].'act=databackup&backup=yes" title="'.$l['testbackup'].'" class="abut">'.$l['testbackup'].'</a>-->
	</center>
	</form>
</div>
</div>

<br /><div class="notebox"><font class="bboxtxt"><b>'.$l['note'].'</b> : '.$l['restore_note'].'</font></div>
<script>

if(window.location.hash == ""){
	$("#dbtabs ul a[href=#cron]").addClass("active");				
	$("#dbtabs div.tab").hide();
	$("#dbtabs div#cron").show();
	$("#cron_tab").addClass("active");
}else{
	$("#dbtabs div.tab").hide();
	$("#dbtabs ul a[href="+window.location.hash+"]").addClass("active");
	$("#dbtabs "+window.location.hash).show();
	if(window.location.hash=="#cron"){
		$("#cron_tab").addClass("active");
		$("#immediate_tab").removeClass("active");
	}else{
		$("#immediate_tab").addClass("active");
		$("#cron_tab").removeClass("active");
	}
}
</script>

</div>';

softfooter();

}

?>