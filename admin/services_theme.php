<?php

//////////////////////////////////////////////////////////////
//===========================================================
// services_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function services_theme(){

global $theme, $globals, $cluster, $user, $l, $services, $autostart, $running;

softheader($l['<title>']);

echo '<script type="text/javascript">

function multi_action(n) {
	
	var op = $("#multi_options_"+n).val();
	
	var conf_lang = new Array();
	conf_lang["start"] = "'.$l['conf_start'].'";
	conf_lang["stop"] = "'.$l['conf_stop'].'";
	conf_lang["restart"] = "'.$l['conf_restart'].'";	
	var r = confirm(conf_lang[op]);
	if(r != true){
		return false;
	}
	
	$("#action").val(op);
	$("form#services").submit();
}

</script>';

echo '
<div class="bg" style="width:99%">
<center class="tit"><i class="icon icon-services icon-head"></i>&nbsp; '.$l['<title>'].'<span style="float:right;" ><a href="'.$globals['docs'].'Services" target="_blank" class="wiki_help" title="'.$l['wiki_help'].'"><i class="icon-help" ></i></a></span></center>';
	
// Is it offline ?
$hypervisor_status = $cluster->statewise($globals['server']);
if($hypervisor_status == 0 || $hypervisor_status == 2){

	echo '<div class="e_notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['server_status_'.$hypervisor_status].'</div>';
	
}elseif(empty($services)){

	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['cant_load'].'</div>';

}else{

echo '<form accept-charset="'.$globals['charset'].'" id="services" name="services" method="post" action="" class="form-horizontal">

	<input type="hidden" id="action" name="action" value="0" />
	
	<div class="row bottom-menu">
		<div class="col-sm-6"></div>
		<div class="col-sm-6">
			<label>'.$l['with_selected'].'</label>
			<select id="multi_options_1" class="form-control">
				<option value="0">---</option>
				<option value="start">'.$l['sstart'].'</option>
				<option value="stop">'.$l['sstop'].'</option>
				<option value="restart">'.$l['srestart'].'</option>
			</select>&nbsp;
			<input type="button" value="'.$l['go'].'" onclick="multi_action(1)" class="go_btn"/>
		</div>
	</div>
	
	<br/>
	
	<table class="table table-hover tablesorter">
		<tr>
			<th align="center">'.$l['<title>'].'</th>
			<th align="center">'.$l['status'].'</th>
			<th align="center">'.$l['autostart'].'</th>
			<th align="center"><input type="checkbox" class="select_all" name="select_all" id="select_all"></th>
		</tr>';
		
	foreach($services as $k=>$v){
		echo '<tr>
			<td>'.$v.'</td>		
			<td align="center">'.(in_array($v, $running) ? '&nbsp;&nbsp;'.$l['statrun'].'': '&nbsp;&nbsp;'.$l['statoff'].'').'</td>
			<td align="center">'.(in_array($v, $autostart) ? '&nbsp;&nbsp;'.$l['staton'].'' : '&nbsp;&nbsp;'.$l['statoff'].'').'</td>
			<td align="center"><input type="checkbox" class="ios" name="sel_serv[]" value="'.$v.'"></td>
		</tr>';
	}
	
	echo'</table>
	
	<div class="row bottom-menu">
		<div class="col-sm-6"></div>
		<div class="col-sm-6">
			<label>'.$l['with_selected'].'</label>
			<select id="multi_options_2" class="form-control">
				<option value="0">---</option>
				<option value="start">'.$l['sstart'].'</option>
				<option value="stop">'.$l['sstop'].'</option>
				<option value="restart">'.$l['srestart'].'</option>
			</select>&nbsp;
			<input type="button" value="'.$l['go'].'" onclick="multi_action(2)" class="go_btn"/>
		</div>
	</div>
	
	</form>
	<br/><br/>
	';
	
}

echo '</div>';
softfooter();

}

?>