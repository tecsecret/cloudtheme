<?php

//////////////////////////////////////////////////////////////
//===========================================================
// ostemplates_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function downloadiso_theme(){

global $theme, $globals, $kernel, $user, $l, $oslist, $offlist, $ostemplates, $error, $done, $isos, $newiso;

if(optGET('ajax')){
	if(!empty($error)){
		$err = str_replace("\n", '', implode('<br />', $error));
		echo $err;
	}elseif(!empty($done)){
		echo '1';
	}else{
		echo '0';
	}
	
	return true;
}

if(optGET('downsize')){
	$size = (float) @vfilesize($globals['isos'].'/'.$newiso['filename']);
	echo $size;
	return true;
}

softheader($l['<title>']);

echo '
<div class="bg" style="width=80%">
<center class="tit"><img src="'.$theme['images'].'/admin/addiso.gif" /> &nbsp; '.$l['tit_iso'].'</center>';

error_handle($error);

if(empty($error)){

echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['done'].'</div>
	
<script language="javascript" type="text/javascript">
var oses = new Object()
oses[1] = new Array(0, 0, 1, "'.$newiso['size'].'", "'.$newiso['filename'].'", 0);

function start_download(){

	for(x in oses){
		if(oses[x][0] == 0){
			$_("doing_div").innerHTML = $_("doing_div").innerHTML + \'<table width="100%" cellpadding="4" id="table_\'+oses[x][2]+\'"><tr><td>File : \'+oses[x][4]+\'</td><td>Size : \'+Math.round(oses[x][3]/1024/1024)+\' MB</td><td id="prog_\'+oses[x][2]+\'" class="val" width="100">0 %</td></tr></table><br />\';
			AJAX(window.location+"&ajax="+oses[x][2], "resp_handle("+oses[x][2]+", re)");
			setTimeout("not_started("+oses[x][2]+");", 10000);
			AJAX(window.location+"&downsize="+oses[x][2], "down_size("+oses[x][2]+", re)");
			return true;
		}
	}
	
	$_("done_notice").style.display = "";
};

function show_error(osid, err){
	oses[osid][0] = -1;
	$_("prog_"+osid).innerHTML = \'<img src="'.$theme['images'].'admin/softerror.gif" />\';
	var t = $_("table_"+osid);
	var lastRow = (t.rows.length);
	var x = t.insertRow(lastRow);
	var y = x.insertCell(0);
	y.colSpan = 3;
	y.innerHTML = err;
	y.style.backgroundColor = "#FEE4CD";
};

function not_started(osid){
	if(oses[osid][1] == 0 && oses[osid][0] != -1 && oses[osid][0] != 1){
		show_error(osid, "'.$l['err_down'].' "+oses[osid][4]);
		start_download();
	}
};

function down_size(osid, resp){
	// If completed
	if(oses[osid][0] == 1){
		return true;
	}
	// If there is no error
	if(oses[osid][0] == -1){
		return false;
	}
	oses[osid][1] = resp;
	AJAX(window.location+"&downsize="+oses[osid][2], "down_size("+oses[osid][2]+", re)");
	progressbar(oses[osid][3], oses[osid][1], oses[osid][2]);
};

function progressbar(s, d, id){
	s = parseInt(s);
	d = parseInt(d);
	var cent = 1;
	if(d <= s){
		cent = (d/s*100);
	}
	if(cent > 0 && !isNaN(cent)){
		cent = parseInt(cent);
		slowprog(id, cent);
	}
};

function slowprog(osid, dest){
	if(oses[osid][5] > dest){
		return true;
	}
	oses[osid][5] = oses[osid][5] + 1;
	if(oses[osid][0] == 1){
		oses[osid][5] = 100;
	}else{
		$_("prog_"+osid).innerHTML = oses[osid][5]+" %";
		setTimeout("slowprog("+osid+", "+dest+")", 10);
	}
};

function resp_handle(osid, resp){ //alert(resp);
	if(resp != "1"){
		show_error(osid, resp);
	}else{
		oses[osid][0] = 1;
		$_("prog_"+osid).innerHTML = \'<img src="'.$theme['images'].'admin/softok.gif" />\';
		progressbar(100, 100, oses[osid][2]);
	}
	start_download();
};

addonload("start_download();");

</script>

<div id="doing_div"></div>
<br />
<div class="notice" style="display:none;" id="done_notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['down_done'].'</div>';
		
}// End of IF for $error

echo '</div>';
softfooter();

}

?>