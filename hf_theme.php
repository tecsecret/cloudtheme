<?php

//////////////////////////////////////////////////////////////
//===========================================================
// hf_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

function js_url(){	
	$js['givejs'] = func_get_args();
	return $GLOBALS['theme']['url'].'/js/givejs.php?'.http_build_query($js).'&'.$GLOBALS['globals']['version'];
}

function css_url(){	
	$r['givecss'] = func_get_args();
	return $GLOBALS['theme']['url'].'/css/givecss.php?'.http_build_query($r).'&'.$GLOBALS['globals']['version'];
}

function softheader($title = '', $leftbody = true){

global $theme, $globals, $ckernel, $user, $l, $error, $SESS;

	if(optGET('jsnohf')){
		return true;
	}
	
	$title = ((empty($title)) ? $globals['sn'] : $title);
	
	//Lets echo the top headers
	echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset='.$globals['charset'].'" />
	<meta name="keywords" content="softaculous, software" />
	<title>'.$title.'</title>
	<link rel="stylesheet" type="text/css" href="'.css_url('style.css', 'jquery-ui.css', 'dataTables-jui.css', 'common.css').'" />
	<link rel="shortcut icon" href="'.$theme['images'].'favicon.ico" />
	<script language="javascript" src="'.js_url('jquery-1.7.1.js', 'jquery-ui.js', 'tiptip.js', 'universal.js', 'dataTables.js', 'jquery.flot.min.js', 'jquery.flot.navigate.min.js', 'jquery.flot.pie.min.js', 'jquery.flot.stack.js').'" type="text/javascript"></script>
	<script language="javascript" id="lazyload" type="text/javascript"></script>
<script language="javascript" type="text/javascript"><!-- // --><![CDATA[

	var clickstate = false;
	var browserType = isIE;
	var browserVersion = IE_version;
	
	function nav(newUrl){
		$("#contwrapper").html(\'<div id="pbar" style=""><img style="position:absolute;top:40%;left:60%;margin-top:-18px;margin-left:-18px;" src="'.$theme['images'].'progress.gif" /></div>\');
		$.ajax({
			url: newUrl+"&random="+Math.random()+"&jsnohf=1&spi=1",
			error: function(){
				alert("Error Loading Page. Please reload the page");
			},
			success: function(data){
				var newhtml = data;
				$("#contwrapper").append(\'<div id="cont"></div>\');

				if(browserType && browserVersion < 9){
					$("#cont").html(newhtml);
					$("#cont").css("display", "none");
				}
				else{
					$("#cont").css("display", "none");
					$("#cont").html(newhtml);
				}
				
				var newhtmlHeight = $("#cont").height();
				$("#contwrapper").animate({"opacity": "1", "height": newhtmlHeight}, 500, function(){
						$("#pbar").hide();
						$("#cont").show();
						setTimeout(\'$("#contwrapper").height($("#cont").height());\', 500);
						$("#contwrapper").css("overflow", "");
						clickstate = false;
						/*$("#cont").show("slide", {direction: "right"}, 500, function(){	
							
						});*/
				});
			}
		});
	};
	
function re_height(){								
	var newhtmlHeight = $("#cont").height();
	$("#contwrapper").animate({"opacity": "1", "height": newhtmlHeight}, 500, function(){
			$("#pbar").hide();
			$("#cont").show();
			setTimeout(\'$("#contwrapper").height($("#cont").height());\', 500);
			$("#contwrapper").css("overflow", "");
			clickstate = false;
	});	
};

var stateManager = {
		
	update: function(silent)
	{
			var animDuration = silent ? 0 : 600;
			var url = window.location.href.replace("#!","");
			if(queryString("svs")){
				url = url + "&act=vpsmanage";
				nav(url);
			}
			else{
				if(queryString("act")){
					nav(url);
				}
				else{
					url = url + "&act=listvs";
					nav(url);
				}
			}
	}
}

	var vindex = "'.$globals['index'].'";
	var urlWithoutHash = window.location.href.replace(window.location.hash,"");
	var urlbits = urlWithoutHash.split("/");
	if (urlbits[urlbits.length-1] !== vindex) {
		urlbits[urlbits.length-1] = "";
		vindex = vindex.replace("&", "");
		var remainder = window.location.href.indexOf(vindex);
		if(remainder > 0){
			var remainder = remainder + vindex.length;
			remainder = window.location.href.substr(remainder);
			if(remainder.indexOf("login") == -1){
				window.location.href = vindex + "#!" + remainder;
				$(function(){ stateManager.update(true); });
			}
		}else{
			window.location.href = vindex + window.location.hash + "#!";
		}
	}else if(window.location.hash != ""){
		$(document).ready(function(){
			stateManager.update(true);
		});
	}
	else{
		window.location.href = window.location.href + window.location.hash + "#!";
		$(document).ready(function(){
			stateManager.update(true);
		});
	}

$(document).ready(function(){
    $("body").fadeIn(1000);
	
	$(".left_menu a, a.eu_nav, .header_menu a").click(function(){
		if(!clickstate){
			clickstate = true;
			var remainder = this.href.indexOf(vindex);
			if(remainder > 0){
				var remainder = remainder + vindex.length;
				remainder = this.href.substr(remainder);
				window.location.hash = "!" + remainder;
				$(function(){ stateManager.update(true); });
				return false;
			}else{
				return true;	
			}
		}
		else{
			return false;
		}
	});
});

function queryString(parameter) { 
  var loc = location.hash.substring(2, location.hash.length);
  var param_value = false;
  var params = loc.split("&");
  for (i=0; i<params.length;i++) {
      param_name = params[i].substring(0,params[i].indexOf("="));
      if (param_name == parameter) {
          param_value = params[i].substring(params[i].indexOf("=")+1)
      }
  }
  
  if (param_value) {
      return true;
  }
  else {
      return false; //Here determine return if no parameter is found
  }
};

function jqueryvpsboot(todo){
	var currentStateImage = null;
	if(todo == "restart"){
		$("#restartimg").attr("src", "'.$theme['images'].'progress_bar.gif");
	}
	else if(todo == "poweroff"){
		$("#poweroffimg").attr("src", "'.$theme['images'].'progress_bar.gif");
	}
	else if(todo == "start" || todo == "stop"){
		currentStateImageSource = $("#startstopimg").attr("src");
		currentStateCaption = $("#startstopcaption").html();
		$("#startstopimg").attr("src", "'.$theme['images'].'progress_bar.gif");
	}
	$.get("index.php?svs="+vpsid_t+"&act="+todo+"&window=true&do=1", function(data){
					   //Error Situation Checking
						if(data.indexOf("not") != -1){
							 $("#startstopimg").attr("src", currentStateImageSource);
							 $("#startstopcaption").html(currentStateCaption);
							 $("#restartimg").attr("src", "'.$theme['images'].'restart.gif");
							 $("#restartcaption").html("Restart");
						}
						//Hack around & getting url-encoded in url
						eval(data.replace(\'amp;\', \'\'));    
	});
	return false;
};

function setvpsstatus(stat){
	try{
		onstatuschange(stat);
	}catch(e){ }
	vpsstatus = stat;
};

function vs_loc(val){
	var newurl = location.href;
	if(!newurl.match(/\?/gi)){
		newurl = newurl+"?";
	}
	window.location = newurl.replace(/(\&?)svs=(\d*)/gi, "")+"&svs="+val
}

// ]]></script>
	</head>
	<body style="display:none;" onload="bodyonload();">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">	
                <tr>
                    <td colspan="3" style="background: url('.$theme['images'].'loginhead.gif);background-repeat: repeat-x;background-size: contain;">
                        <table border="0" cellpadding="0" cellspacing="0" width="950" align="center">	
                            <tr>
                                <td padding="10" width="100%" height="43" valign="middle"><a href="'.$globals['ind'].'"><img src="'.(empty($globals['logo_url']) ? $theme['images'].'loginlogo.gif' : $globals['logo_url']).'" alt="" /></a></td>';
								
								if(!empty($user['name'])){
									echo '<td>
										<table style="float:right;" border="0" cellpadding="5" cellspacing="1" align="center">	
											<tr>
												<td><span style="color:white;">'.$l['welcome'].',</span>	</td>
												<td><span style="color:white;"><b>'.@$user['name'].'</b></span>	</td>
												<td><a style="text-decoration:none; color:white;" href="'.$globals['ind'].'act=logout"><b>['.$l['logout'].']</b></a></td>
											</tr>
										</table>
									</td>';
								}
								
                            echo '</tr>
                        </table>
                    </td>
                </tr>
            </table>
	<table width="980" align="center" cellpadding="0" cellspacing="0">
		<tr><td width="100%">
		<br /><input id="theme_path" type="hidden" value="'.$theme['images'].'"/> ';
	//The Menus of softwares
	if(!empty($leftbody)){
	
		$theme['leftbody'] = $leftbody;
		
		echo '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="softwares">	
	<tr>
		<td align="left" valign="top" width="180" class="left_menu" id="left_menu">';
			
		// If this is the ADMIN logged in as enduser
		if(isset($SESS['og_uid'])){
			echo '<center class="notice" style="width:178px; margin-bottom:3px;">'.$l['you_are_admin'].'</center>';
		}	
			
		echo '<a id="listvs" href="index.php?act=listvs"><img src="'.$theme['images'].'vs.gif" /> &nbsp; '.$l['lm_vs'].'</a>';
			
			// Cloud User Menu
			if(@$user['type'] == 2){
				echo '<a id="create" href="index.php?act=create"><img src="'.$theme['images'].'addvs.png" /> &nbsp; '.$l['lm_addvs'].'</a>
				<a id="cloudres" href="index.php?act=cloudres"><img src="'.$theme['images'].'cloud_resources.png" /> &nbsp; '.$l['lm_resources'].'</a>
				<a id="users" href="index.php?act=users"><img src="'.$theme['images'].'/admin/users.gif" /> &nbsp; '.$l['lm_users'].'</a>';
			}
			
			echo '<a id="tasks" href="index.php?act=ctasks"><img src="'.$theme['images'].'tasks.gif" /> &nbsp; '.$l['lm_tasks'].'</a>
			<a id="profile" href="index.php?act=profile"><img src="'.$theme['images'].'/admin/userprofile.gif" /> &nbsp; '.$l['lm_profile'].'</a>
			<a id="usettings" href="index.php?act=usersettings"><img src="'.$theme['images'].'lmsettings.gif" /> &nbsp; '.$l['lm_usr_settings'].'</a>
			<a id="upassword" href="index.php?act=userpassword"><img src="'.$theme['images'].'changepass.gif" /> &nbsp; '.$l['lm_changepass'].'</a>'.
			(empty($globals['disable_apicredential']) ? '<a id="apikey" href="index.php?act=apikey"><img src="'.$theme['images'].'apikey.gif" /> &nbsp; '.$l['lm_apikey'].'</a>' : '').
			(!empty($user['dnsplan']['pdnsid']) ? '<a id="pdns" href="index.php?act=pdns"><img src="'.$theme['images'].'dns24.gif" /> &nbsp; '.$l['lm_pdns'].'</a>' : '').
			(!empty($user['dnsplan']['pdnsid']) || !empty($globals['enable_rdns']) ?'<a id="rdns" href="index.php?act=rdns"><img src="'.$theme['images'].'dns24.gif" /> &nbsp; '.$l['lm_rdns'].'</a>' : '').'
			'.(!empty($globals['support'])? '<a href="'.$globals['support'].'" target="_blank"><img height="24" src="'.$theme['images'].'support.png"/> &nbsp; '.$l['lm_support'].'</a>' : '').'
		</td>
		<td valign="top" style="padding-left:15px;">
		<td valign="top" style="padding-left:15px;">';
	
	}
	
	//Everything else will go here
	echo '<div id="softcontent" style="padding:0px; margin:0px;">';
	
}


function softfooter(){

global $theme, $globals, $ckernel, $user, $l, $error, $end_time, $start_time;

if(optGET('jsnohf')){
	return true;
}

echo '</div>';

if(!empty($theme['leftbody'])){
	
	echo '</td>
	</tr>
	</table>';

}

echo '<br />';

echo '<br /><div align="center">'.$l['times_are'].(empty($globals['pgtimezone']) ? '' : ' '.($globals['pgtimezone'] > 0 ? '+' : '').$globals['pgtimezone']).'. '.$l['time_is'].' '.datify(time(), false).'.</div>';

$pageinfo = array();

if(!empty($globals['showntimetaken'])){

	$pageinfo[] = $l['page_time'].':'.substr($end_time-$start_time,0,5);

}

echo '<br />
<table width="100%" cellpadding="8" cellspacing="1" class="footer">
<tr>
<td align="left">'.copyright().'&nbsp;</td>'.(empty($pageinfo) ? '' : '<td align="right">'.implode('&nbsp;&nbsp;|&nbsp;&nbsp;', $pageinfo).'</td>').'
</tr>
</table><br />';

if(!empty($theme['copyright'])){

	echo unhtmlentities($theme['copyright']);

}

echo '<script language="javascript" type="text/javascript"><!-- // --><![CDATA[
function bodyonload(){
	if(aefonload != \'\'){		
		eval(aefonload);
	}
	'.(empty($onload) ? '' : 'eval(\''.implode(';', $onload).'\');').'
};
// ]]></script>';

echo '</td>
</tr>
</table>
</body>
</html>';
}

function error_handle($error, $table_width = '100%', $center = false, $ret = false){

global $l;
	
	$str = '';
	
	//on error call the form
	if(!empty($error)){
		
		$str = '<table width="'.$table_width.'" cellpadding="2" cellspacing="1" class="error" '.(($center) ? 'align="center"' : '' ).'>
			<tr>
			<td>
			'.$l['following_errors_occured'].' :
			<ul type="square">';
		
		foreach($error as $ek => $ev){
		
			$str .= '<li>'.$ev.'</li>';
		
		}
		
		
		$str .= '</ul>
			</td>
			</tr>
			</table>'.(($center) ? '</center>' : '' ).'
			<br />';
		
		if(empty($ret)){
			echo $str;
		}else{
			return $str;
		}
		
	}

}


//This will just echo that everything went fine
function success_message($message, $table_width = '100%', $center = false){

global $l;

	//on error call the form
	if(!empty($message)){
		
		echo '<table width="'.$table_width.'" cellpadding="2" cellspacing="1" class="error" '.(($center) ? 'align="center"' : '' ).'>
			<tr>
			<td>
			'.$l['following_message'].' :
			<ul type="square">';
		
		foreach($message as $mk => $mv){
		
			echo '<li>'.$mv.'</li>';
		
		}
		
		
		echo '</ul>
			</td>
			</tr>
			</table>'.(($center) ? '</center>' : '' ).'
			<br />';
		
		
	}

}


function majorerror($title, $text, $heading = ''){

global $theme, $globals, $user, $l;

softheader(((empty($title)) ? $l['fatal_error'] : $title), false);

?>
<br /><br /><br />
<table width="70%" cellpadding="2" cellspacing="1" class="cbor" align="center">
	
	<tr>
	<td class="cbg" align="left">
	<b><?php echo ((empty($heading)) ? $l['following_fatal_error'].':' : $heading);?></b>
	</td>
	</tr>
	
	<tr>
	<td colspan="2" align="center">
	<img src="<?php echo $theme['images'];?>error.gif" alt="" />
	</td>
	</tr>
	
	<tr>
	<td class="error" align="left"><?php echo $text;?><br />
	</td>
	</tr>

</table>
<br /><br /><br />

<?php

softfooter();

//We must return
return true;

}

function message($title, $heading = '', $icon, $text){

global $theme, $globals, $user, $l;

softheader(((empty($title)) ? $l['soft_message'] : $title), false);

?>

<table width="70%" cellpadding="2" cellspacing="1" class="cbor" align="center" >
	
	<tr>
	<td class="cbg" align="left"  >
	<b><?php echo ((empty($heading)) ? $l['following_soft_message'].':' : $heading);?></b>
	</td>
	</tr>
	
	<tr>
	<td class="bg" colspan="2" align="center">
	<img src="<?php echo $theme['images'].(empty($icon)?'info.gif':$icon);?>" alt="" />
	</td>
	</tr>
	
	<tr>
	<td class="error" align="left"><?php echo $text;?><br />
	</td>
	</tr>

</table>
<br /><br /><br />

<?php

softfooter();

//We must return
return true;

}

?>
