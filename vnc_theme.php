<?php

//////////////////////////////////////////////////////////////
//===========================================================
// vnc_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       8th Mar 2010
// Time:       23:00 hrs
// Site:       https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

// function ssh_theme(){
function vnc_theme(){

global $theme, $globals, $ckernel, $user, $l, $info, $SESS, $cluster, $servers;

softheader($l['<title>']);

if(empty($user['vps']['vnc'])){

	echo '<div class="notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['not_enabled'].'</div>';
	
}else{

// is it no vnc mode ?
if(optGET('novnc') && !empty($globals['novnc'])){
	
	$vpsid = $globals['vpsid'];
	
	if(empty($info['port'])){
		echo '<div class="e_notice"><img src="'.$theme['images'].'notice.gif" /> &nbsp; '.$l['vps_offline'].'</div>';
		return false;
	}
	
	$novnc_viewer = file_get_contents(dirname(__FILE__).'/novnc/vnc_auto_virt.html');

	// The noVNC serid
	$novnc_serid = $user['vps']['serid'];

	if(!empty($globals['novnc_master_only'])){
		$novnc_serid = 0;
	}
	
	$proto = 'http';
	$port = 4081;
	$websockify = 'websockify';
	if(!empty($_SERVER['HTTPS'])){
		$proto = 'https';
		if($_SERVER['SERVER_PORT'] == '443'){
			$port = 443;
		}else{
			$port = 4083;
		}
		$websockify = 'novnc/';
	}
	
	if($user['vps']['virt'] == 'xcp'){
		$vnc_token = $user['vps']['vpsid'].'-'.$user['vps']['vnc_passwd'];
	}else{
		$vnc_token = $user['vps']['vpsid'];
	}
	
	$novnc_viewer = lang_vars_name($novnc_viewer, array('HOST' => (!empty($globals['novnc_server_name']) ? server_hostname($novnc_serid) : server_vncip($novnc_serid)),
														'PORT' => $port,
														'PROTO' => $proto,
														'WEBSOCKET' => $websockify,
														'TOKEN' => $vnc_token,
														'PASSWORD' => $info['password']));
		
	echo $novnc_viewer;
	
	return true;
	
}

if(isset($_REQUEST['launch'])){
	
	if(vps_virt_text() == 'xcp'){
		
		echo '<APPLET ARCHIVE="'.$theme['url'].'/java/vnc/TightVncViewer.jar" CODE="com.tightvnc.vncviewer.VncViewer" WIDTH="1" HEIGHT="1">
		<PARAM NAME="SOCKETFACTORY" VALUE="com.tightvnc.vncviewer.SshTunneledSocketFactory">
		<PARAM NAME="SSHHOST" VALUE="'.$info['ip'].'">
		<PARAM NAME="HOST" VALUE="localhost">
		<PARAM NAME="PORT" VALUE="'.$info['port'].'">
		<PARAM NAME="Open New Window" VALUE="yes">
		</APPLET>';
	}else{

		echo '<APPLET ARCHIVE="https://s2.softaculous.com/a/virtualizor/files/VncViewer.jar" CODE="com.tigervnc.vncviewer.VncViewer" WIDTH="1" HEIGHT="1">
		<PARAM NAME="HOST" VALUE="'.$info['ip'].'">
		<PARAM NAME="PORT" VALUE="'.$info['port'].'">
		<PARAM NAME="PASSWORD" VALUE="'.$info['password'].'">
		<PARAM NAME="Open New Window" VALUE="yes">
		</APPLET>';
		
	}
	
	return true;
	
}

//JQuery UI Dialog and AJAX Code------------------------------------------------------------------------------------
echo '<script language="javascript" type="text/javascript"><!-- // --><![CDATA[

function launchvnc(){
	AJAX("/'.$SESS['token_key'].'/index.php?act=vnc&launch=1&jsnohf=1&svs='.$user['vps']['vpsid'].'", "vnchandle(re)");
};

function vnchandle(resp){
	$_("vnc_launcher").innerHTML = resp;
}

function novnc_https(ele){
	var thisURL = ele.href;
	thisURL = thisURL.toString();
	thisURL = thisURL.replace("http:", "https:");
	thisURL = thisURL.replace(":4082", ":4083");
	ele.href = thisURL;
	return false;
};
			
      // ]]></script>';
//----------------------------------------------------------------------------------------------------------------

echo '<br /><br />

<div class="heading">'.$l['vnc_info'].'</div>
<br />
<table align="center" cellpadding="6" cellspacing="0" border="0">
	<tr>
		<td width="15%">&nbsp;&nbsp;&nbsp;'.$l['vnc_ip'].'</td>
		<td class="val" style="border-right:1px solid #CCCCCC" width="35%">'.$info['ip'].'</td>

		<td width="15%" align="right">&nbsp;&nbsp;&nbsp;'.$l['vnc_port'].'</td>
		<td class="val" width="35%" align="right">&nbsp;&nbsp;'.$info['port'].'</td>
	</tr>
</table>

<br /><br /><br /><br />

<center>'.(!empty($globals['novnc']) ? '<a href="'.$globals['ind'].'act=vnc&novnc=1&jsnohf=1&svs='.$globals['vpsid'].'" target="blank" class="abut">'.$l['novnc_button'].'</a>' : '').' &nbsp; <a href="javascript:void(0)" id="vnc_button" class="abut" onclick="launchvnc();">'.$l['launch_vnc'].'</a></center>

<div id="vnc_launcher"></div>';	

}

softfooter();

}

?>