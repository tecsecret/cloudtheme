<?php

//////////////////////////////////////////////////////////////
//===========================================================
// invoices_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:	   8th Mar 2010
// Time:	   23:00 hrs
// Site:	   https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

// function ssh_theme(){
function invoices_theme(){

global $theme, $globals, $ckernel, $user, $l, $info, $SESS, $cluster, $servers, $invoice;

	if(optREQ('download')){
		ob_start();
	}

	echo '
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>'.$globals['sn'].' - '.$l['bill_invoice'].' #'.$invoice['invoid'].'</title>
	<!-- CSS -->
	<link href="themes/'.$globals['theme_folder'].'/css2/givecss.php?[[version]]&[[patch]]" rel="stylesheet">
	<script src="themes/default/js2/jquery.min.js"></script>
	<script src="themes/default/js2/html2pdf.bundle.min.js"></script>

</head>
<body class="invoice-body">

<div class="container-fluid invoice-container">		
	<div id="invoice">
		<div class="row">
			<div class="col-sm-12">
				<h2>'.$globals['sn'].'</h2>
				<h3>'.$l['bill_invoice'].' #'.$invoice['invoid'].'</h3>
			</div>
		</div>

		<hr>
			
		<div class="row">
			<div class="col-sm-6">
				<strong>'.$l['bill_pay_to'].':</strong>
				<address class="small-text">
					'.$globals['billing_comp'].'<br />
					'.$globals['billing_addr1'].'<br />
					'.$globals['billing_addr2'].'
				</address>
			</div>
			<div class="col-sm-6 text-right">
				<strong>'.$l['bill_inv_to'].':</strong>
				<address class="small-text">
					'.(empty($user['company']) ? '' : $user['company'].'<br />').'
					'.(empty($user['fname']) ? $user['email'] : $user['fname'].' '.$user['lname']).'<br />
					'.(empty($user['address']) ? '' : $user['address'].'<br />');
					
					if(!empty($user['city'])){
						$arr[] = $user['city'];
					}
					
					if(!empty($user['state'])){
						$arr[] = $user['state'];
					}
					
					if(!empty($user['country'])){
						$arr[] = $user['country'];
					}
					
					echo (empty($arr) ? '' : implode(', ', $arr).'<br />').'
					'.(empty($user['zip']) ? '' : 'Zip Code : '.$user['zip'].'<br />').'
				</address>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-6">
				<h3 class="invoice_status_'.$invoice['status'].'">'.$invoice['status_txt'].'</h3>
			</div>
			<div class="col-sm-6 text-right">
				<strong>'.$l['bill_invodate'].':</strong><br>
				<span class="small-text">
					'.datetime($invoice['invodate']).'<br><br>
				</span>
			</div>
		</div>
		<br />	
		
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><strong>'.$l['bill_item'].'</strong></h3>
			</div>
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-condensed">
						<thead>
							<tr>
								<td><strong>'.$l['bill_desc'].'</strong></td>
								<td width="20%" class="text-center"><strong>'.$l['bill_net'].'</strong></td>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>'.$invoice['item'].'</td>
								<td class="text-center">'.$globals['billing_symbol'].''.$invoice['amt'].' '.$globals['billing_currency'].'</td>
							</tr>
							<tr>
								<td class="total-row text-right"><strong>'.$l['bill_subtotal'].'</strong></td>
								<td class="total-row text-center">'.$globals['billing_symbol'].''.$invoice['amt'].' '.$globals['billing_currency'].'</td>
							</tr>
							<tr>
								<td class="total-row text-right"><strong>'.$l['bill_disc'].'</strong></td>
								<td class="total-row text-center">'.$globals['billing_symbol'].''.$invoice['disc'].' '.$globals['billing_currency'].'</td>
							</tr>
							<tr>
								<td class="total-row text-right"><strong>'.$l['bill_netamt'].'</strong></td>
								<td class="total-row text-center">'.$globals['billing_symbol'].''.$invoice['net'].' '.$globals['billing_currency'].'</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>';
	
	
	/*if(!empty($invoice['transactions'])){
	
		echo '
	<div class="transactions-container small-text">
		<div class="table-responsive">
			<table class="table table-condensed">
				<thead>
					<tr>
						<td class="text-center"><strong>'.$l['bill_tr_date'].'</strong></td>
						<td class="text-center"><strong>'.$l['bill_tr_gateway'].'</strong></td>
						<td class="text-center"><strong>'.$l['bill_tr_token'].'</strong></td>
						<td class="text-center"><strong>'.$l['bill_tr_amt'].'</strong></td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="text-center">12/01/2017</td>
						<td class="text-center">PayPal</td>
						<td class="text-center">10H07953W24191451</td>
						<td class="text-center">$24.00 USD</td>
					</tr>
					<tr>
						<td class="text-right" colspan="3"><strong>Balance</strong></td>
						<td class="text-center">$0.00 USD</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>';
		
	}*/

	echo'<div class="pull-right btn-group btn-group-sm hidden-print">
			<a href="javascript:window.print()" class="btn btn-default"><i class="fa fa-print"></i> '.$l['bill_print'].'</a>
			<a href="#" onclick="print_invoice()" class="btn btn-default"><i class="fa fa-download"></i> '.$l['bill_download'].'</a>
		</div>
	</div>

	
</div>
<script>
function print_invoice(){
	$(".hidden-print").hide();
	var data = document.getElementById("invoice");
	var parameters = {
		margin:	0,
		filename: "'.$invoice['invoid'].'_invoice.pdf",
		image: { type: "jpeg", quality: 1 },
		html2canvas: { dpi: 300, letterRendering: true, scale: 2 },
		jsPDF: {unit: "mm", format: "a4", orientation: "portrait"}
	};
	html2pdf().from(data).set(parameters).save();
}
</script>
</body>
</html>
';

}


