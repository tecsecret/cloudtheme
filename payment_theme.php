<?php

//////////////////////////////////////////////////////////////
//===========================================================
// payment_theme.php
//===========================================================
// SOFTACULOUS VIRTUALIZOR
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:	   8th Mar 2010
// Time:	   23:00 hrs
// Site:	   https://www.virtualizor.com/ (SOFTACULOUS VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at https://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Ltd.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('VIRTUALIZOR')){

	die('Hacking Attempt');

}

// function payment_theme(){
function payment_theme(){

global $theme, $globals, $ckernel, $user, $l, $info, $error, $cluster, $servers, $done;
	
	echo '
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>'.$globals['sn'].' - '.$l['pay_payment'].'</title>
	<!-- CSS -->
	<link href="themes/'.$globals['theme_folder'].'/css2/givecss.php?[[version]]&[[patch]]" rel="stylesheet">

</head>
<body class="invoice-body">

<div class="container-fluid invoice-container">		
	<div id="invoice">
		<div class="row">
			<div class="col-sm-12">
				<h2>'.$globals['sn'].'</h2>
			</div>
		</div>';
		
		if(!empty($error)){
			
			echo $l['following_errors_occured'].' :<br /><br />';
			
			foreach($error as $ek => $ev){		
					echo '
				<div class="alert alert-danger">
					'.$ev.'
				</div>';
			}
			
		}
		
		if(!empty($done)){
			
			echo '
		<div class="alert alert-success">
			'.$done['msg'].'
		</div>';
		
		}

		echo '<div class="pull-right btn-group btn-group-sm hidden-print">
			<a href="'.full_cp_url().'#act=billing" class="btn btn-default"><i class="fa fa-print"></i> '.$l['pay_panel'].'</a>
			<a href="javascript:window.print()" class="btn btn-default"><i class="fa fa-print"></i> '.$l['bill_print'].'</a>
		</div>
	</div>
</div>

</body>
</html>
';
	
}


