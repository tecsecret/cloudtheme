<?php

/**
 * Combine all JavaScript Files and give.
 * 
 * @package  ampps
 */

//////////////////////////////////////////////////////////////
//===========================================================
// editremote.php
//===========================================================
// SOFTACULOUS 
// Version : 1.1
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       10th Jan 2009
// Time:       21:00 hrs
// Site:       http://www.softaculous.com/ (SOFTACULOUS)
// ----------------------------------------------------------
// Please Read the Terms of use at http://www.softaculous.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Inc.
//===========================================================
//////////////////////////////////////////////////////////////

// We are zipping if possible
if(function_exists('ob_gzhandler')){
	ob_start('ob_gzhandler');
}

header("Content-type: text/css; charset: UTF-8");

//Caching
header("Cache-Control: must-revalidate");
header("Expires: " . gmdate("D, d M Y H:i:s", time() + (60*60*24*365)) . " GMT");

foreach($_GET['givecss'] as $k => $v){
	
	// Prevent hacking or ECHOING of any other file which is above this folder !
	$name = str_replace(array('/', '../', chr(0)), array('', '', ''), $v);
	
	if(file_exists($name) && basename($name) != basename(__FILE__)){
		echo file_get_contents($name)."\n\n\n";
	}
}

?>